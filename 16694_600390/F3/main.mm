
//
//  main.m
//  Net_Information
//
//  Created by huqinghe on 11-6-5.
//  Copyright 2011 __MyCompanyName__.com. All rights reserved.
//

#import <UIKit/UIKit.h>

///////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
	int retVal = UIApplicationMain(argc, argv, nil, @"ZSTF3ClientAppDelegate");
    [pool release];
    
    return retVal;
}
