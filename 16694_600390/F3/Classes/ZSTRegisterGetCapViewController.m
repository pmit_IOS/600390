//
//  ZSTRegisterGetCapViewController.m
//  F3
//
//  Created by pmit on 15/8/13.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTRegisterGetCapViewController.h"
#import "ZSTAgreementViewController.h"
#import "ZSTCapInputViewController.h"

@interface ZSTRegisterGetCapViewController ()

@property (strong,nonatomic) UITextField *phoneNumTF;
@property (strong,nonatomic) UIButton *hasRegisterBtn;
@property (strong,nonatomic) UIButton *serviceBtn;

@end

@implementation ZSTRegisterGetCapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"完善账号资料", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    [self buildPhoneLabel];
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildPhoneLabel
{
    UIView *phoneView = [[UIView alloc] initWithFrame:CGRectMake(30, HEIGHT * 0.1, WIDTH - 60, 50)];
    phoneView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:phoneView];
    
    UIImageView *iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 30)];
    iconIV.image = ZSTModuleImage(@"user.png");
    iconIV.contentMode = UIViewContentModeScaleAspectFit;
    [phoneView addSubview:iconIV];
    
    self.phoneNumTF = [[UITextField alloc] initWithFrame:CGRectMake(40, 15, phoneView.bounds.size.width - 40, 30)];
    self.phoneNumTF.delegate = self;
    self.phoneNumTF.tag = 1;
    self.phoneNumTF.placeholder = @"手机号";
    self.phoneNumTF.font = [UIFont systemFontOfSize:16.0f];
    self.phoneNumTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.phoneNumTF.keyboardType = UIKeyboardTypeNumberPad;
    [phoneView addSubview:self.phoneNumTF];
    
    CALayer *bottomLine = [CALayer layer];
    bottomLine.backgroundColor = RGBA(244, 244, 244, 1).CGColor;
//    bottomLine.backgroundColor = [UIColor blackColor].CGColor;
    bottomLine.frame = CGRectMake(0, phoneView.bounds.size.height - 1, phoneView.bounds.size.width, 1);
    [phoneView.layer addSublayer:bottomLine];
    
    UIButton *getCapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    getCapBtn.frame = CGRectMake(30, HEIGHT * 0.1 + 80 + 20, WIDTH - 60, 50);
    getCapBtn.backgroundColor = RGBA(244, 125, 54, 1);
    [getCapBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [getCapBtn addTarget:self action:@selector(getCap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:getCapBtn];
    
    UIButton *serviceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    serviceBtn.frame = CGRectMake(30, getCapBtn.frame.origin.y + getCapBtn.bounds.size.height + 20, WIDTH - 60, 30);
    [serviceBtn setTitle:@"服务条款和隐私政策" forState:UIControlStateNormal];
    [serviceBtn setTitleColor:RGBA(65, 84, 248, 1) forState:UIControlStateNormal];
    [serviceBtn addTarget:self action:@selector(showPrivateService:) forControlEvents:UIControlEventTouchUpInside];
    serviceBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.view addSubview:serviceBtn];
    self.serviceBtn = serviceBtn;
    serviceBtn.hidden = NO;
    
    UIButton *hasRegisterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [hasRegisterBtn setTitle:@"该手机号已注册,点击直接去绑定>" forState:UIControlStateNormal];
    [hasRegisterBtn setTitleColor:RGBA(124, 124, 124, 1) forState:UIControlStateNormal];
    hasRegisterBtn.frame = CGRectMake(30, getCapBtn.frame.origin.y + getCapBtn.bounds.size.height + 20, WIDTH - 60, 30);
    [hasRegisterBtn addTarget:self action:@selector(goToBind:) forControlEvents:UIControlEventTouchUpInside];
    hasRegisterBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    self.hasRegisterBtn = hasRegisterBtn;
    [self.view addSubview:hasRegisterBtn];
    hasRegisterBtn.hidden = YES;
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.phoneNumTF.tag == 1 && self.phoneNumTF == textField) {
        NSInteger maxLength = 11;
        NSInteger strLength = textField.text.length - range.length + string.length;
        
        return (strLength <= maxLength);
    }
    
    return YES;
}

// 回收软键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (void)getCap:(UIButton *)sender
{
    [self.phoneNumTF resignFirstResponder];
    if ([self.phoneNumTF.text isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:@"您还没填写电话号码呀" withImage:nil];
    }
    else if (![self validateMobile:self.phoneNumTF.text])
    {
        [TKUIUtil alertInView:self.view withTitle:@"您的电话号码不符合规则哦" withImage:nil];
    }
    else
    {
        [self.engine checkPhoneNumIsExist:self.phoneNumTF.text];
    }
}

- (void)showPrivateService:(UIButton *)sender
{
    ZSTAgreementViewController *controller = [[ZSTAgreementViewController alloc] init];
    controller.source = NO;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)checkPhoneNumIsExistSuccess:(NSDictionary *)response
{
    if ([[response safeObjectForKey:@"code"] integerValue] == 1)
    {
        self.hasRegisterBtn.hidden = NO;
        self.serviceBtn.hidden = YES;
    }
    else
    {
        [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在请求验证码,请稍等一会哦", nil)];
        [self.engine getVerificationCodeWithMsisdn:self.phoneNumTF.text OperType:1];
        
    }
}

- (BOOL)validateMobile:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,181,189,1700,1709,1705
     */
    NSString * MOBILE = @"^1(3[0-9]|4[0-9]|5[0-35-9]|7[0-9]|8[0125-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349|700|705|709)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)getVerificationCodeDidSucceed:(NSString*)response
{
    [TKUIUtil hiddenHUD];
    ZSTCapInputViewController *capInputVC = [[ZSTCapInputViewController alloc] init];
    capInputVC.mobilePhone = self.phoneNumTF.text;
    [self.navigationController pushViewController:capInputVC animated:YES];
    
}

- (void)getVerificationCodeDidFailed:(NSString*)message
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:message withImage:nil];
    ZSTCapInputViewController *capInputVC = [[ZSTCapInputViewController alloc] init];
    capInputVC.mobilePhone = self.phoneNumTF.text;
    [self.navigationController pushViewController:capInputVC animated:YES];
}

- (void)goToBind:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
