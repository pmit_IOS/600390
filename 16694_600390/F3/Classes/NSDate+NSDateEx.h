//
//  NSDate+NSDateEx.h
//  star_letv
//
//  Created by Yunfei Bai on 11-12-26.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (NSDateEx)
+(NSInteger) minutesSinceMidnight: (NSDate *)date;
+(NSInteger) secondsSinceMidnight: (NSDate *)date;
+(NSString *) dateStringWithTimeInterval:(NSTimeInterval) timeInterval;
+(NSString *) dateStringWithDate:(NSDate*) date;
+ (NSString *)stringFromDate:(NSDate *)date;
+ (NSDate *)dateFromString:(NSString *)dateString;
@end
