//
//  ZSTCapInputViewController.m
//  F3
//
//  Created by pmit on 15/8/13.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTCapInputViewController.h"
#import "ZSTAMSettingPassVC.h"

@interface ZSTCapInputViewController ()

@property (strong,nonatomic) UITextField *validTF;
@property (strong,nonatomic) UIButton *resendBtn;
@property (assign,nonatomic) NSInteger timeCount;

@end

@implementation ZSTCapInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"完善账号资料", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    [self buildTip];
    [self buildTipsView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildTip
{
    UILabel *phoneLB = [[UILabel alloc] initWithFrame:CGRectMake(30, HEIGHT * 0.05, WIDTH - 60, 40)];
    phoneLB.textColor = [UIColor blackColor];
    NSString *phoneLBString = [NSString stringWithFormat:@"您的手机号: %@",self.mobilePhone];
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:phoneLBString];
    [nodeString addAttributes:@{UITextAttributeTextColor:RGBA(135, 135, 135, 1)} range:NSMakeRange(0, 6)];
    phoneLB.attributedText = nodeString;
    phoneLB.textAlignment = NSTextAlignmentLeft;
    phoneLB.font = [UIFont systemFontOfSize:18.0f];
    [self.view addSubview:phoneLB];
    
}

- (void)buildTipsView
{
    UIImageView *iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(30, HEIGHT * 0.05 + 40 + 20, 20, 20)];
    iconIV.image = ZSTModuleImage(@"a.png");
    [self.view addSubview:iconIV];
    
    UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(55, HEIGHT * 0.05 + 40 + 10, WIDTH - 55 - 20, 40)];
    tipLB.font = [UIFont systemFontOfSize:14.0f];
    tipLB.textColor = RGBA(135, 135, 135, 1);
    tipLB.text = @"您将会收到一条带有验证码的短信,请输入验证码!";
    tipLB.numberOfLines = 2;
    [self.view addSubview:tipLB];
    
    UIView *phoneView = [[UIView alloc] initWithFrame:CGRectMake(30, HEIGHT * 0.05 + 100, WIDTH - 60, 50)];
    phoneView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:phoneView];
    
    UIImageView *iconsIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 15, 20, 20)];
    iconsIV.image = ZSTModuleImage(@"lock.png");
    iconsIV.contentMode = UIViewContentModeScaleAspectFit;
    [phoneView addSubview:iconsIV];
    
    self.validTF = [[UITextField alloc] initWithFrame:CGRectMake(30, 10,phoneView.bounds.size.width - 30, 30)];
    self.validTF.delegate = self;
    self.validTF.tag = 1;
    self.validTF.placeholder = @"请输入手机收到的验证码";
    self.validTF.font = [UIFont systemFontOfSize:16.0f];
    self.validTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.validTF.keyboardType = UIKeyboardTypeNumberPad;
    [phoneView addSubview:self.validTF];
    
    CALayer *bottomLine = [CALayer layer];
    bottomLine.backgroundColor = RGBA(244, 244, 244, 1).CGColor;
    //    bottomLine.backgroundColor = [UIColor blackColor].CGColor;
    bottomLine.frame = CGRectMake(0, phoneView.bounds.size.height - 1, phoneView.bounds.size.width, 1);
    [phoneView.layer addSublayer:bottomLine];
    
    UIButton *getCapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    getCapBtn.frame = CGRectMake(30, phoneView.frame.origin.y + phoneView.bounds.size.height + 20, WIDTH - 60, 50);
    getCapBtn.backgroundColor = RGBA(244, 125, 54, 1);
    [getCapBtn setTitle:@"提交验证码" forState:UIControlStateNormal];
    [getCapBtn addTarget:self action:@selector(submitValid:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:getCapBtn];
    
    UIButton *serviceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    serviceBtn.frame = CGRectMake(30, getCapBtn.frame.origin.y + getCapBtn.bounds.size.height + 20, WIDTH - 60, 30);
    [serviceBtn setTitle:@"重新发送验证码" forState:UIControlStateNormal];
    [serviceBtn setTitleColor:RGBA(124, 124, 124, 1) forState:UIControlStateNormal];
    [serviceBtn addTarget:self action:@selector(repeatValid:) forControlEvents:UIControlEventTouchUpInside];
    serviceBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    self.resendBtn = serviceBtn;
    [self.view addSubview:serviceBtn];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.validTF.tag == 1 && self.validTF == textField) {
        NSInteger maxLength = 6;
        NSInteger strLength = textField.text.length - range.length + string.length;
        
        return (strLength <= maxLength);
    }
    
    return YES;
}

// 回收软键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (void)submitValid:(UIButton *)sender
{
    if ([self.validTF.text isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您还没有输入验证码哦", nil) withImage:nil];
    }
    else
    {
        [self.engine checkVerificationCodeWithMsisdn:self.mobilePhone verificationCode:self.validTF.text OperType:1];
    }
}

- (void)checkVerificationCodeDidSucceed:(NSString*)response
{
    ZSTAMSettingPassVC *passVC = [[ZSTAMSettingPassVC alloc] init];
    passVC.mobilePhone = self.mobilePhone;
    passVC.verifyCode = self.validTF.text;
    [self.navigationController pushViewController:passVC animated:YES];
}

- (void)checkVerificationCodeDidFailed:(NSString*)message
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:message withImage:nil];
}

- (void)repeatValid:(UIButton *)sender
{
    self.timeCount = 60;
    [sender setTitle:[NSString stringWithFormat:@"重新发送验证码(%@)",@(60)] forState:UIControlStateNormal];
    sender.enabled = NO;
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(countDown:) userInfo:nil repeats:YES];
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在请求验证码,请稍等一会哦", nil)];
    [self.engine getVerificationCodeWithMsisdn:self.mobilePhone OperType:1];
}

- (void)getVerificationCodeDidSucceed:(NSString*)response
{
    [TKUIUtil hiddenHUD];
}

- (void)getVerificationCodeDidFailed:(NSString*)message
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:message withImage:nil];
}

- (void)countDown:(NSTimer *)timer
{
    if (self.timeCount == 0)
    {
        [timer invalidate];
        [self.resendBtn setTitle:@"重新发送验证码" forState:UIControlStateNormal];
        self.resendBtn.enabled = YES;
    }
    else
    {
        self.timeCount--;
        [self.resendBtn setTitle:[NSString stringWithFormat:@"重新发送验证码(%@)",@(self.timeCount)] forState:UIControlStateNormal];
    }
}

@end
