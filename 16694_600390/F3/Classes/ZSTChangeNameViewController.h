//
//  ZSTChangeNameViewController.h
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-30.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTTextView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ZSTPersonalInfo.h"

typedef  void (^changeBlocked)(NSString *nickname);

@interface ZSTChangeNameViewController : UIViewController<UITextViewDelegate,ZSTF3EngineDelegate>
{
    changeBlocked _changeInfoBlocked;
}

@property (nonatomic, retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (nonatomic, retain) UIView *bgView;
@property (nonatomic, retain) ZSTTextView *textView;

@property (nonatomic, retain) UserInfo *userInfo;

@property (nonatomic, assign) int type;

@property(nonatomic, retain) ZSTF3Engine *engine;

-(void)setBlock:(changeBlocked) btnClickedBlock;

@end
