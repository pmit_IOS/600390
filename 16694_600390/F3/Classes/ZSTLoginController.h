//
//  ZSTLoginController.h
//  F3
//
//  Created by pmit on 15/8/3.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTLoginControllerDelegate <NSObject>

@optional

- (void)loginDidFinish;

- (void)loginDidCancel;

- (void)loginDidSucceed;

@end

@interface ZSTLoginController : UIViewController <ZSTF3EngineDelegate,UITextFieldDelegate>

@property(nonatomic, assign) BOOL isFromSetting;

@property(nonatomic, retain) ZSTF3Engine *engine;

@property (nonatomic, assign) id<ZSTLoginControllerDelegate> delegate;


@end
