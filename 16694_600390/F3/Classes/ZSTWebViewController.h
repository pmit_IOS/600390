//
//  ZSTWebViewController.h
//  F3
//
//  Created by 9588 on 3/6/12.
//  Copyright 2012 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZSTScrollToolBarView.h"
#import "ZSTModuleBaseViewController.h"
#import "JSBridgeWebView.h"
#import "PMRepairButton.h"

@protocol ZSTWebViewControllerDelegate;
@class ZSTScrollToolBarView;

@interface ZSTWebViewController : ZSTModuleBaseViewController<UIWebViewDelegate, ZSTScrollToolBarViewDelegate,JSBridgeWebViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    JSBridgeWebView *_webView;
    NSString *_url;
    NSURL *_loadingURL;
    id<ZSTWebViewControllerDelegate> _delegate;
    
    BOOL webCanGoBack;
    BOOL webCanGoForward;
    
    PMRepairButton *_backButton;
    PMRepairButton *_forwardButton;
    
    ZSTScrollToolBarView *_scrollToolBarView;
    UIImagePickerController *pickerLibrary;

}

@property (nonatomic, assign)   id<ZSTWebViewControllerDelegate> delegate;
@property(nonatomic) BOOL webCanGoBack;
@property(nonatomic) BOOL webCanGoForward;
@property (nonatomic, retain)ZSTScrollToolBarView *scrollToolBarView;


@property(retain,nonatomic) NSString *uploadUrl;
@property(retain,nonatomic) NSString *uploadImageName;
@property(nonatomic,assign) float dWidth;
@property(nonatomic,assign) float dHeight;
@property(nonatomic,assign) float quality;
@property(retain,nonatomic) NSString *callBackFunc;
@property(retain,nonatomic) NSString *resultStr;
@property (retain,nonatomic) NSString *titleString;
@property (assign,nonatomic) CGPoint scrollPoint;
@property (nonatomic, assign) NSInteger type;

- (void)setURL:(NSString *)url;

@end


/**
 * The web controller delegate, similar to UIWebViewDelegate, but prefixed with controller
 */
@protocol ZSTWebViewControllerDelegate <NSObject>

@optional
- (BOOL)webController:(ZSTWebViewController *)controller webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
       navigationType:(UIWebViewNavigationType)navigationType;
- (void)webController:(ZSTWebViewController *)controller webViewDidStartLoad:(UIWebView *)webView;
- (void)webController:(ZSTWebViewController *)controller webViewDidFinishLoad:(UIWebView *)webView;
- (void)webController:(ZSTWebViewController *)controller webView:(UIWebView *)webView
 didFailLoadWithError:(NSError *)error;

@end