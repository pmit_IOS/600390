//
//  ECMobileClientParams.h
//  F3
//
//  Created by xuhuijun on 12-4-17.
//  Copyright 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZSTECMobileClientParams : NSObject 
    
@property(nonatomic, assign) int MCRegistType;
@property(nonatomic, copy) NSString *MCRegistDescription;
@property(nonatomic, copy) NSString *MCRegistChangeDescription;
@property(nonatomic, assign) int IPPushFlag;
@property(nonatomic, copy) NSString *HTTPPollTimeRule;
@property(nonatomic, assign) int SMSHookFlag;

@end
