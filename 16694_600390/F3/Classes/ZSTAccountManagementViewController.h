//
//  ZSTAccountManagementViewController.h
//  F3
//
//  Created by xuhuijun on 13-7-9.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModuleBaseViewController.h"
#import "ZSTRegisterViewController.h"

@interface ZSTAccountManagementViewController : ZSTModuleBaseViewController <UITableViewDelegate,UITableViewDataSource,RegisterDelegate>

{
    UITableView *_tableView;
    NSMutableArray *accountItems;
    NSMutableArray *settingItems;

}

@end
