//
//  HHCommentController.h
//  YouYun
//
//  Created by luo bin on 12-6-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKPlaceHolderTextView.h"
#import "ZSTModuleBaseViewController.h"
#import "ZSTF3Preferences.h"

@protocol ZSTHHSinaShareControllerDelegate;

@interface ZSTHHSinaShareController : ZSTModuleBaseViewController<UITextViewDelegate>

@property (nonatomic, retain) TKPlaceHolderTextView *textView;

@property (nonatomic, retain) UILabel *countLabel;

@property (nonatomic, retain) UIImageView *backgroundView;

@property (nonatomic, assign) id<ZSTHHSinaShareControllerDelegate> delegate;

@property (nonatomic, retain) NSString * shareString;

@property (nonatomic ,assign) BOOL isLogin;

@property (nonatomic ,assign) BOOL isHaveImage;

@property (nonatomic, retain) UIImage * shareImage;

@property (nonatomic, retain) UIImageView * shareImageView;

@property (assign, nonatomic) ShareType shareType;




- (void)sendButtonAction:(id)sender;

@end

@protocol ZSTHHSinaShareControllerDelegate <NSObject>

@optional

- (void)shareDidFinish:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options;

- (void)shareDidFail:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options;


@end
