//
//  ZSTPersonalSexCell.m
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-18.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonalSexCell.h"

@implementation ZSTPersonalSexCell

- (void)awakeFromNib
{
//    _sexSwitch = [[ZSTCustomSwitch alloc] initWithFrame:CGRectMake(225, 15, 49, 24)];
    _sexSwitch = [[PMCustomSwitch alloc] initWithFrame:CGRectMake(225, 15, 49, 24)];
    _sexSwitch.delegate = self;
//    _sexSwitch.arrange = ZSTCustomSwitchArrangeOFFLeftONRight;
//    _sexSwitch.onImage = ZSTModuleImage(@"module_personal_female.png");
//    _sexSwitch.offImage = ZSTModuleImage(@"module_personal_male.png");
    [self.contentView addSubview:_sexSwitch];
    
    UIImageView *rightImgView = [[UIImageView alloc] initWithFrame:CGRectMake(285, (self.frame.size.height - 12)/2.0, 7, 12)];
    rightImgView.image = ZSTModuleImage(@"module_setting_icon_right.png");
    [self addSubview:rightImgView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

# pragma mark - customSwitch delegate
-(void)zstcustomSwitchSetStatus:(PMCustomSwitchStatus)status
{
    switch (status) {
        case PMCustomSwitchStatusOn:
            if ([_delegate respondsToSelector:@selector(cell:sexSelection:)]) {
                [_delegate cell:self sexSelection:female];
            }
            break;
        case PMCustomSwitchStatusOff:
            if ([_delegate respondsToSelector:@selector(cell:sexSelection:)]) {
                [_delegate cell:self sexSelection:male];
            }
            break;
        default:
            break;
    }
}


@end
