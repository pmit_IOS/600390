//
//  ZSTMineViewController.h
//  F3
//
//  Created by LiZhenQu on 14-10-27.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTLoginViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ZSTLoginController.h"

@interface ZSTSettingItems : NSObject {
}

+(ZSTSettingItems *)itemWithTitle:(NSString *)title type:(NSString *)type;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, retain) NSString *type;

@end

@interface ZSTMineViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,ZSTF3EngineDelegate,ZSTLoginControllerDelegate,UIAlertViewDelegate>
{
    UITableView *_tableView;
    
    NSMutableArray *_tableViewDataArray;
    
    NSMutableArray *_settingItems;
    
    NSMutableArray *_urlSettingItems;
    
    NSMutableDictionary *_urlSettingItemDict;
    
    UIButton *button;
    
    NSString * _clientVersionUrl;
    UITextField *_phoneNumberTextField;
}

@property (nonatomic, retain) IBOutlet UIView *unLoginView;
@property (nonatomic, retain) IBOutlet UIButton *loginbtn;

@property (nonatomic, retain) IBOutlet UIView *loginView;
//@property (nonatomic, retain) TKAsynImageView *avaterImgView;
@property (retain, nonatomic) IBOutlet UIImageView *avaterImage;

@property (nonatomic,retain) UIImageView *avaterImgView;
@property (nonatomic, retain) IBOutlet UILabel *userNameLabe;
@property (nonatomic, retain) IBOutlet UILabel *accountLabel;
@property (nonatomic, retain) IBOutlet UILabel *pointLabel;
@property (nonatomic, retain) IBOutlet UIButton *signbtn;

@property (nonatomic, retain) IBOutlet UIButton *showbtn;

@property (nonatomic, retain) IBOutlet UIButton *clickbtn;

@property (nonatomic, retain) IBOutlet UIButton *registerbtn;
@property (nonatomic, retain) IBOutlet UILabel *linkLabel;

@property(nonatomic, retain) ZSTF3Engine *engine;

@end
