//
//  BusinessAppDao.m
//  F3
//
//  Created by xuhuijun on 12-3-8.
//  Copyright 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTBusinessAppDao.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"

@implementation ZSTBusinessAppDao

+ (ZSTBusinessAppDao *)shareBusinessAppDao
{
    static ZSTBusinessAppDao *dao;
    if (dao == nil) {
        @synchronized(self) {
            if (dao == nil) {
                dao = [[ZSTBusinessAppDao alloc] init];
            }
        }
    }
    return dao;
}

- (void)deleteBusinessAppInfo:(NSString *)appId
{    
    // 从数据库中删除
    NSString * sql =  @"Delete from BusinessAppInfo where AppID = ?";
    [ZSTSqlManager executeUpdate:sql,appId];
}

- (void)deleteAllBusinessAppInfo
{
    NSString * sql =  @"Delete from BusinessAppInfo";
    [ZSTSqlManager executeUpdate:sql];
}

- (BOOL)insertBusinessAppInfo:(ZSTBusinessAppInfo *)info
{
//       NSLog(@"insertBusinessAppInfo.appId = %@ %@ %@ %@ %@ %@ %@ %d %@ %@",info.AppID,info.Name,info.ECECCID,info.IconKey,info.Introduce,info.Version,info.Status,info.appOrder,info.Url,info.Local);
    
    NSString *sql = @"Insert into BusinessAppInfo (AppID, Name, ECECCID, IconKey, Introduce, Version, Status, appOrder, Url, Local,ModuleId, ModuleType,InterfaceUrl)"
    " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
    NSError *error = nil;
    [ZSTSqlManager executeUpdate:sql,
                                              info.AppID ,
                                              info.Name,
                                              info.ECECCID,
                                              info.IconKey,
                                              [ZSTUtils objectForArray:info.Introduce],
                                              info.Version,
                                              info.Status,
                                              @(info.appOrder),
                                              info.Url,   
                                              [ZSTUtils objectForArray:info.Local],
                                              @(info.ModuleID),
                                              @(info.ModuleType),
                                              info.InterfaceUrl];
    return error == nil;
}

- (BOOL)insertOrReplaceBusinessAppInfo:(ZSTBusinessAppInfo *)info
{    
//    NSLog(@"insertOrReplaceBusinessAppInfo info.appId = %@ %@ %@ %@ %@ %@ %@ %d %@ %@",info.AppID,info.Name,info.ECECCID,info.IconKey,info.Introduce,info.Version,info.Status,info.appOrder,info.Url,info.Local);

    NSString *sql = @"Insert or replace into BusinessAppInfo (AppID, Name, ECECCID, IconKey, Introduce, Version, Status, appOrder, Url, Local,ModuleId , ModuleType, InterfaceUrl)"
    " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
    return [ZSTSqlManager executeUpdate:sql,
                                                     info.AppID ,
                                                     info.Name,
                                                     info.ECECCID,
                                                     info.IconKey,
                                                     [ZSTUtils objectForArray:info.Introduce],
                                                     info.Version,
                                                     info.Status,
                                                     @(info.appOrder),
                                                     info.Url,   
                                                     [ZSTUtils objectForArray:info.Local],
                                                     @(info.ModuleID),
                                                     @(info.ModuleType),
                                                     info.InterfaceUrl
                                                     ];
}

- (ZSTBusinessAppInfo *)_populateMsgTypeInfoFromResultSet: (NSDictionary *)resultSet
{
    ZSTBusinessAppInfo *info = [[ZSTBusinessAppInfo alloc] init];         
    info.ID = [[resultSet safeObjectForKey:@"ID"] integerValue];
    info.AppID = [resultSet safeObjectForKey:@"AppID"];
    info.Name = [resultSet safeObjectForKey:@"Name"];
    info.ECECCID = [resultSet safeObjectForKey:@"ECECCID"];
    info.IconKey = [resultSet safeObjectForKey:@"IconKey"];
    info.Introduce = [resultSet safeObjectForKey: @"Introduce"];
    info.Version = [resultSet safeObjectForKey:@"Version"];
    info.Status = [resultSet safeObjectForKey:@"Status"];
    info.appOrder = [[resultSet safeObjectForKey:@"appOrder"] integerValue];
    info.Url = [resultSet safeObjectForKey:@"Url"];
    info.Local = [resultSet safeObjectForKey: @"Local"];
    if ([resultSet safeObjectForKey:@"ModuleID"]) {
        info.ModuleID = [[resultSet safeObjectForKey:@"ModuleID"] integerValue];
    } else {
        info.ModuleID = 9999;
    }
    info.ModuleType = [[resultSet safeObjectForKey:@"ModuleType"] integerValue];
    info.InterfaceUrl = [resultSet safeObjectForKey:@"InterfaceUrl"];
    return [info autorelease];
}

-(NSArray *) getBusinessAppnfos
{
    NSMutableArray *businessAppnfos = [NSMutableArray array];
    
    NSString *sql = @"Select ID, AppID, Name, ECECCID, IconKey, Introduce, Version, Status, appOrder, Url, Local ,ModuleId, ModuleType, InterfaceUrl from BusinessAppInfo order by appOrder DESC, AppID DESC";
    
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql];
    
    for (NSDictionary *rs in resultSet) {
        [businessAppnfos addObject: [self _populateMsgTypeInfoFromResultSet:rs]];
    }
    return businessAppnfos;
    
}

- (ZSTBusinessAppInfo *)getBusinessAppInfoByAppId:(NSString *)appId
{
    NSString *sql = @"Select ID, AppID, Name, ECECCID, IconKey, Introduce, Version, Status, appOrder, Url, Local,ModuleId, ModuleType, InterfaceUrl from BusinessAppInfo where AppID = ?";
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql, appId];
    for (NSDictionary *rs in resultSet)
    {
        return [self _populateMsgTypeInfoFromResultSet:rs];
    }
    return nil;
}

- (BOOL)setBusinessAppInfo:(NSString *)appId shielded:(BOOL)isShielded
{
    NSString *sql = @"Update BusinessAppInfo Set Status = ? where AppID = ? ";
    return [ZSTSqlManager executeUpdate:sql, [NSNumber numberWithBool:isShielded], appId];
}

- (NSData *)getIcon:(NSString *)appId
{
    NSString *sql = @"select data from fileinfo, BusinessAppInfo where fileinfo.key = BusinessAppInfo.IconKey and BusinessAppInfo.IconKey != '' and AppID = ?";
    return [ZSTSqlManager dataForQuery:sql, appId];
}
@end
