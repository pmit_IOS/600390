//
//  PMRepairButton.h
//  F3
//
//  Created by pmit on 15/5/9.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMRepairButton : UIButton
@property(retain,nonatomic)UIImage *norIm;
@property(retain,nonatomic)UIImage *hiIm;
@property(retain,nonatomic)UIImage *bgIm;
@end
