//
//  ContactData.m
//  Phone
//
//  Created by angel li on 10-9-20.
//  Copyright 2010 Lixf. All rights reserved.
//

#import "ContactData.h"

#define KPHONELABELDICDEFINE		@"KPhoneLabelDicDefine"
#define KPHONENUMBERDICDEFINE	@"KPhoneNumberDicDefine"
#define KPHONENAMEDICDEFINE	@"KPhoneNameDicDefine"

@implementation ContactData
+ (ABAddressBookRef) addressBook
{
    //pmit修改 时间2015-5-5
    //IOS6.0以上
    //必须先请求用户的同意，如果未获得同意或者用户未操作，此通讯录的内容为空
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);//等待同意后向下执行
    //为了保证用户同意后在进行操作，此时使用多线程的信号量机制，创建信号量，信号量的资源数0表示没有资源，调用dispatch_semaphore_wait会立即等待。若对此处不理解，请参看GCD信号量同步相关内容。
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);//发出访问通讯录的请求
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error){
        //如果用户同意，才会执行此block里面的方法
        //此方法发送一个信号，增加一个资源数
        dispatch_semaphore_signal(sema);});
    //如果之前的block没有执行，则sema的资源数为零，程序将被阻塞
    //当用户选择同意，block的方法被执行， sema资源数为1；
    dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    dispatch_release(sema);
    
	return addressBook;
}
//从Address Book里得到所有联系人
+ (NSArray *) contactsArray
{
    ABAddressBookRef addressBook = [ContactData addressBook];
	NSArray *thePeople = (NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
	NSMutableArray *array = [NSMutableArray arrayWithCapacity:thePeople.count];
	for (id person in thePeople)
		[array addObject:[ABContact contactWithRecord:(ABRecordRef)person]];
	[thePeople release];
    CFRelease(addressBook);
	return array;
}


//以号码来检测通讯录内是否已包含该联系人
+ (NSDictionary *) hasContactsExistInAddressBookByPhone:(NSString *)phone{
	NSString *PhoneNumber = nil;
	NSString *PhoneLabel = nil;
	NSString *PhoneName = nil;
	NSArray *contactarray = [ContactData contactsArray];
	for(int i=0; i<[contactarray count]; i++)
	{
		ABContact *contact = [contactarray objectAtIndex:i];
		NSArray *phoneCount = [ContactData getPhoneNumberAndPhoneLabelArray:contact];
		if([phoneCount count] > 0)
		{
			NSDictionary *PhoneDic = [phoneCount objectAtIndex:0];
			PhoneNumber = [ContactData getPhoneNumberFromDic:PhoneDic];
			PhoneLabel = [ContactData getPhoneLabelFromDic:PhoneDic];
			PhoneName = contact.contactName;
			if([PhoneNumber isEqualToString:phone])
			{
				NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:PhoneName,KPHONENAMEDICDEFINE,PhoneNumber,KPHONENUMBERDICDEFINE,PhoneLabel,KPHONELABELDICDEFINE,nil ];
				return dic;
			}
		}
	}
	return nil;
}


//通过号码得到该联系人
+(ABContact *) byPhoneNumberAndLabelToGetContact:(NSString *)phone withLabel:(NSString *)label{
	NSArray *array = [ContactData contactsArray];
	for(ABContact * contast in array)
	{
		NSArray *phoneArray = [ContactData getPhoneNumberAndPhoneLabelArray:contast];
		if(phoneArray == nil)
			return nil;
		for(NSDictionary *dic in phoneArray)
		{
			NSString *aPhone = [ContactData getPhoneNumberFromDic:dic];
			NSString *aLabel = [ContactData getPhoneLabelFromDic:dic];
			if([aPhone isEqualToString:phone] && [aLabel isEqualToString:label])
				return (ABContact *)contast;
		}
	}
	return nil;
}

//通过姓名与号码得到该联系人
+(ABContact *) byPhoneNumberAndNameToGetContact:(NSString *)name withPhone:(NSString *)phone{
	NSArray *array = [ContactData contactsArray];
	for(ABContact * contast in array)
	{
		NSArray *phoneArray = [ContactData getPhoneNumberAndPhoneLabelArray:contast];
		if(phoneArray == nil)
			return nil;
		for(NSDictionary *dic in phoneArray)
		{
			NSString *aPhone = [ContactData getPhoneNumberFromDic:dic];
		//	NSString *aLabel = [ContactData getPhoneLabelFromDic:dic];
			if([aPhone isEqualToString:phone] && [name isEqualToString:contast.contactName])
				return (ABContact *)contast;
		}
	}
	return nil;
}


//通过姓名得到该联系人
+(ABContact *) byNameToGetContact:(NSString *)name{
	NSArray *array = [ContactData contactsArray];
	for(ABContact * contast in array)
	{
		if([contast.contactName isEqualToString:name])
			return (ABContact *)contast;
	}
	return nil;
}


//通过号码得到该联系人
+(ABContact *) byPhoneNumberlToGetContact:(NSString *)phone withLabel:(NSString *)label{
	NSArray *array = [ContactData contactsArray];
	for(ABContact * contast in array)
	{
		NSArray *phoneArray = [ContactData getPhoneNumberAndPhoneLabelArray:contast];
		if(phoneArray == nil)
			return nil;
		for(NSDictionary *dic in phoneArray)
		{
			NSString *aPhone = [ContactData getPhoneNumberFromDic:dic];
			if([aPhone isEqualToString:phone] && [label isEqualToString:NSLocalizedString(@"未知", @"")])
				return (ABContact *)contast;
		}
	}
	return nil;
}


//得到联系人的号码组与Label组
+(NSArray *) getPhoneNumberAndPhoneLabelArray:(ABContact *) contact
{
	NSMutableArray *phoneArray = [[[NSMutableArray alloc] init] autorelease];
	ABMutableMultiValueRef phoneMulti = ABRecordCopyValue(contact.record, kABPersonPhoneProperty);
    if (phoneMulti == nil) {
        return phoneArray;
    }
	int i;
	for (i = 0;  i < ABMultiValueGetCount(phoneMulti);  i++) {
		NSString *phone = [(NSString*)ABMultiValueCopyValueAtIndex(phoneMulti, i) autorelease];
		NSString *label =  [(NSString*)ABMultiValueCopyLabelAtIndex(phoneMulti, i) autorelease];
		NSMutableDictionary *phoneDic = [[NSDictionary dictionaryWithObjectsAndKeys:contact.contactName,KPHONENAMEDICDEFINE,phone,KPHONENUMBERDICDEFINE,label,KPHONELABELDICDEFINE,nil] mutableCopy];
		[phoneArray addObject:phoneDic];
	}
    CFRelease(phoneMulti);
	return phoneArray;
	
}

//得到联系人的号码组与Label组
+(NSArray *) getPhoneNumberAndPhoneLabelArrayFromABRecodID:(ABRecordRef)person withABMultiValueIdentifier:(ABMultiValueIdentifier)identifierForValue
{
	NSString *nameStr = (NSString *)ABRecordCopyCompositeName(person);
	NSMutableArray *phoneArray = [[[NSMutableArray alloc] init] autorelease];
	ABMutableMultiValueRef phoneMulti = ABRecordCopyValue(person, kABPersonPhoneProperty);
	NSString *phone = [(NSString*)ABMultiValueCopyValueAtIndex(phoneMulti, identifierForValue) autorelease];
	NSString *label =  [(NSString*)ABMultiValueCopyLabelAtIndex(phoneMulti, identifierForValue) autorelease];
	NSMutableDictionary *phoneDic = [[NSDictionary dictionaryWithObjectsAndKeys:nameStr,KPHONENAMEDICDEFINE,phone,KPHONENUMBERDICDEFINE,label,KPHONELABELDICDEFINE,nil] mutableCopy];
	[phoneArray addObject:phoneDic];
	CFRelease(phoneMulti);
    [nameStr release];
	return phoneArray;
}


//从所存的辞典中得到当前联系人的电话号码
+(NSString *) getPhoneNumberFromDic:(NSDictionary *) Phonedic
{
	NSString * phoneNumber = [Phonedic safeObjectForKey:KPHONENUMBERDICDEFINE];
	return [ContactData getPhoneNumberFomat:phoneNumber];
}

//从所存的辞典中得到当前联系人的姓名
+(NSString *) getPhoneNameFromDic:(NSDictionary *) Phonedic
{
	NSString * phoneName = [Phonedic safeObjectForKey:KPHONENAMEDICDEFINE];
	return phoneName;
}


//从所存的辞典中得到当前联系人的Label
+(NSString *) getPhoneLabelFromDic:(NSDictionary *) Phonedic
{
	NSString * PhoneLabel = [Phonedic objectForKey:KPHONELABELDICDEFINE];
	if([PhoneLabel isEqualToString:@"_$!<Mobile>!$_"])
		PhoneLabel = @"移动电话";
	else if([PhoneLabel isEqualToString:@"_$!<Home>!$_"])
		PhoneLabel = @"住宅";
	else if([PhoneLabel isEqualToString:@"_$!<Work>!$_"])
		PhoneLabel = @"工作";
	else if([PhoneLabel isEqualToString:@"_$!<Main>!$_"])
		PhoneLabel = @"主要";
	else if([PhoneLabel isEqualToString:@"_$!<HomeFAX>!$_"])
		PhoneLabel = @"住宅传真";
	else if([PhoneLabel isEqualToString:@"_$!<WorkFAX>!$_"])
		PhoneLabel = @"工作传真";
	else if([PhoneLabel isEqualToString:@"_$!<Pager>!$_"])
		PhoneLabel = @"传呼";
	else if([PhoneLabel isEqualToString:@"_$!<Other>!$_"])
		PhoneLabel = @"其它";
	return PhoneLabel;
}


//向当前联系人表中插入一条电话记录
+ (BOOL)addPhone:(ABContact *)contact phone:(NSString*)phone{
    ABMutableMultiValueRef multi = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    CFErrorRef anError = NULL;
    
    // The multivalue identifier of the new value isn't used in this example,
    // multivalueIdentifier is just for illustration purposes.  Real-world
    // code can use this identifier to do additional work with this value.
    ABMultiValueIdentifier multivalueIdentifier;
    
    if (!ABMultiValueAddValueAndLabel(multi, (CFStringRef)phone, kABPersonPhoneMainLabel, &multivalueIdentifier)){
        CFRelease(multi);
        return NO;
    }
	
    if (!ABRecordSetValue(contact.record, kABPersonPhoneProperty, multi, &anError)){
        CFRelease(multi);
        return NO;
    }
    CFRelease(multi);
    return YES;
}


//号码显示格式
+ (NSString *)getPhoneNumberFomat:(NSString *)phone{
	if([phone length] <1)
		return nil;
	NSString* telNumber = @"";
	for (int i=0; i<[phone length]; i++) {
		NSString* chr = [phone substringWithRange:NSMakeRange(i, 1)];
		if([ContactData doesStringContain:@"0123456789" Withstr:chr]) {
			/*if([telNumber length] == 3 || [telNumber length] == 8)
			 telNumber = [telNumber stringByAppendingFormat:@"-%@", chr];
			 else
			 telNumber = [telNumber stringByAppendingFormat:@"%@", chr];*/
			telNumber = [telNumber stringByAppendingFormat:@"%@", chr];
		}
	}
	return telNumber;
}

//检测字符
+ (BOOL)doesStringContain:(NSString* )string Withstr:(NSString*)charcter{
	if([string length] < 1)
		return FALSE;
	for (int i=0; i<[string length]; i++) {
		NSString* chr = [string substringWithRange:NSMakeRange(i, 1)];
		if([chr isEqualToString:charcter])
			return TRUE;
	}
	return FALSE;
}


+(NSString *)equalContactByAddressBookContacts:(NSString *)name withPhone:(NSString *)phone withLabel:(NSString *)label PhoneOrLabel:(BOOL)isPhone withFavorite:(BOOL)isFavorite
{
	ABContact *contact = nil;
	NSArray *array;
	NSString *phoneNumber = @"";
	NSString *phoneLabel = @"";
	if(isFavorite)
		contact = [ContactData byNameToGetContact:name];
	if(!contact)
		contact = [ContactData byPhoneNumberAndLabelToGetContact:phone withLabel:label];
	if(!contact)
		contact = [ContactData byPhoneNumberAndNameToGetContact:name withPhone:phone];
	if([label isEqualToString:NSLocalizedString(@"未知", @"")] && contact == nil)
		contact = [ContactData byPhoneNumberlToGetContact:phone withLabel:label];
	if(contact)
	{
		array = [ContactData getPhoneNumberAndPhoneLabelArray:contact];
	}
	if(contact == nil)
		return nil;
	if([array count] == 1)
	{
		NSDictionary *PhoneDic = [array objectAtIndex:0];
		phoneNumber = [ContactData getPhoneNumberFromDic:PhoneDic];
		phoneLabel = [ContactData getPhoneLabelFromDic:PhoneDic];
	}else  if([array count] > 1)
	{
		for(NSDictionary *dic in array)
		{
			NSString *aPhone = [ContactData getPhoneNumberFromDic:dic];
			NSString *aLabel = [ContactData getPhoneLabelFromDic:dic];
			if([phone isEqualToString:aPhone] && [label isEqualToString:aLabel])
			{
				phoneNumber = aPhone;
				phoneLabel = aLabel;
				break;
			}
		}
	}
	if(isPhone)
		return phoneNumber;
	else
		return phoneLabel;
}


+(NSString *)getContactsNameByPhoneNumberAndLabel:(NSString *)phone withLabel:(NSString *)label{
	NSArray *array = [ContactData contactsArray];
	for(ABContact * contast in array)
	{
		NSArray *phoneArray = [ContactData getPhoneNumberAndPhoneLabelArray:contast];
		if(phoneArray == nil)
			return nil;
		for(NSDictionary *dic in phoneArray)
		{
			NSString *aPhone = [ContactData getPhoneNumberFromDic:dic];
			NSString *aLabel = [ContactData getPhoneLabelFromDic:dic];
			if([aPhone isEqualToString:phone] && [aLabel isEqualToString:label])
				return contast.contactName;
		}
	}
	return nil;	
}


// 从通讯录中删除联联人
+(BOOL) removeSelfFromAddressBook:(ABContact *)contact withErrow:(NSError **) error
{
    ABAddressBookRef addressBook = [ContactData addressBook];
	if (!ABAddressBookRemoveRecord(addressBook, contact.record, (CFErrorRef *) error)) 
    {
        CFRelease(addressBook);
        return NO;
    }
	BOOL success = ABAddressBookSave(addressBook,  (CFErrorRef *) error);
    CFRelease(addressBook);
    return success;
}

+(BOOL)searchResult:(NSString *)contactName searchText:(NSString *)searchT{
	NSComparisonResult result = [contactName compare:searchT options:NSCaseInsensitiveSearch
											   range:NSMakeRange(0, searchT.length)];
	if (result == NSOrderedSame)
		return YES;
	else
		return NO;
}
@end
