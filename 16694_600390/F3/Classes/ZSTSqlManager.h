//
//  SqlViewRender.h
//  CCBClient
//
//  Created by 谢伟 on 10-9-25.
//
//  Updated by luobin on 12-9-27.
//  
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlausibleDatabase.h"


@interface ZSTSqlManager : NSObject

// 数据库文件是否存在
+(BOOL) dbExists;

+(void) openDatabase;
+(void) closeDatabase;

+(void) lock;
+(void) unlock;

+(void) executeSqlWithSqlStrings: (NSString *) sqlStrings;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*query, select data*/
+ (NSArray *)executeQuery:(NSString *)sql, ...;
+ (NSArray *)executeQuery:(NSString *)sql arguments:(NSArray *)args;

/*update methos, update/insert/delete data*/
+ (BOOL)executeUpdate:(NSString *)sql, ...;
+ (BOOL)executeUpdate:(NSString *)sql arguments:(NSArray *)args;

/**
 * @brief 执行插入sql语句专用，返回插入记录的rowId，sql执行失败则返回－1
 */
+(NSInteger)executeInsert:(NSString *)sql, ...;
+(NSInteger)executeInsert:(NSString *)sql arguments:(NSArray *)args;

/*transaction*/
+ (BOOL)commit;
+ (BOOL)rollback;
+ (BOOL)beginTransaction;
+ (BOOL)beginDeferredTransaction;

/*helper methods*/
+ (BOOL)tableExists:(NSString*)tableName;
+ (BOOL)indexExists:(NSString*)indexName;

+ (NSString*)stringForQuery:(NSString*)sql, ...;
+ (int)intForQuery:(NSString*)sql, ...;
+ (long)longForQuery:(NSString*)sql, ...;
+ (BOOL)boolForQuery:(NSString*)sql, ...;
+ (double)doubleForQuery:(NSString*)sql, ...;
+ (NSData*)dataForQuery:(NSString*)sql, ...;

@end
