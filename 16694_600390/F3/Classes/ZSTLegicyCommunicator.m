

//
//  TKCommunicator.m
//
//
//  Created by luobin on 12-3-8.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTLegicyCommunicator.h"
#import "ASIFormDataRequest.h"
#import "TKUtil.h"
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "ElementParser.h"
#import "ZSTLegacyResponse.h"
#import "ZSTLogUtil.h"

#define kTKRequestUserInfo @"kTKRequestUserInfo"

@interface ZSTLegicyCommunicator ()
- (NSData *)postDataFromTemplate:(NSString *)Template
                  replacedParams:(NSDictionary *)params;

- (void)requestFormPostURL:(NSString *)url
                    params:(NSDictionary *)params
                  fileData:(NSData *)data
                  delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
          progressDelegate:(id)progress;

- (void)requestDidFetchFailed:(ZSTLegacyResponse *)response;

- (ZSTLegacyResponse *)requestFetchFailed:(ASIHTTPRequest *)request;
//Connection has occurred error!
- (ZSTLegacyResponse *)requestFetchComplete:(ASIHTTPRequest *)request;
@end

@implementation ZSTLegicyCommunicator
@synthesize prefixParams;
@synthesize baseURL;

SINGLETON_IMPLEMENTATION(ZSTLegicyCommunicator);

- (id)init {
    self = [super init];
    if (self) {
        networkQueue = [[ASINetworkQueue alloc] init];
        [networkQueue setRequestDidFinishSelector:@selector(requestFetchComplete:)];
        [networkQueue setRequestDidFailSelector:@selector(requestFetchFailed:)];
        [networkQueue setDelegate:self];
        [networkQueue setMaxConcurrentOperationCount:1]; // ASI default set = 4.
        [networkQueue go];
        uploadRequestsSet = [[NSMutableSet alloc] init];
        self.baseURL = @"";
    }
    return self;
}

- (void)dealloc {
    [networkQueue reset];
    TKRELEASE(baseURL);
    TKRELEASE(networkQueue);
    TKRELEASE(uploadRequestsSet);
    TKRELEASE(prefixParams);
    [super dealloc];
}

#pragma mark - URL Request method
// ############################ URL request methods ############################

- (ZSTLegacyResponse *)uploadFileSynToMethod:(NSString *)method
                                        path:(NSString *)path
                                      params:(NSDictionary *)params
                            progressDelegate:(id)progress
{
    NSString *url = nil;
    if ([self.baseURL length] == 0) {
        url = method;
    } else {
        url = [self.baseURL stringByAppendingPathComponent:method];
    }
    url = [url stringByAddingQuery:params];
    TKDPRINT(@"Will post url address : %@", url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url]];
    [request setTimeOutSeconds:180];
    request.postBodyFilePath = path;
    request.shouldStreamPostDataFromDisk = YES;
    if (progress) {
        [request setUploadProgressDelegate:progress];
        [request setShowAccurateProgress:YES];
    }
    [uploadRequestsSet addObject:request];
    [request startSynchronous];
    if (request.error) {
        return [self requestFetchFailed:request];
    } else {
        return [self requestFetchComplete:request];
    }
}

- (ZSTLegacyResponse *)uploadFileSynToMethod:(NSString *)method
                                        data:(NSData *)data
                                      params:(NSDictionary *)params
                            progressDelegate:(id)progress
{
    NSString *url = nil;
    if ([self.baseURL length] == 0) {
        url = method;
    } else {
        url = [self.baseURL stringByAppendingPathComponent:method];
    }
    url = [url stringByAddingQuery:params];
    TKDPRINT(@"Will post url address : %@", url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url]];
    request.postBody = (NSMutableData *)data;
    [request setTimeOutSeconds:180];
    if (progress) {
        [request setUploadProgressDelegate:progress];
        [request setShowAccurateProgress:YES];
    }
    [uploadRequestsSet addObject:request];
    [request startSynchronous];
    if (request.error) {
        return [self requestFetchFailed:request];
    } else {
        return [self requestFetchComplete:request];
    }
}

- (void)uploadFileToMethod:(NSString *)method
                      path:(NSString *)path
                    params:(NSDictionary *)params
                  delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
          progressDelegate:(id)progress {
    NSString *url = nil;
    if ([self.baseURL length] == 0) {
        url = method;
    } else {
        url = [self.baseURL stringByAppendingPathComponent:method];
    }
    url = [url stringByAddingQuery:params];
    TKDPRINT(@"Will post url address : %@", url);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url]];
    [request setTimeOutSeconds:180];
    request.postBodyFilePath = path;
    request.shouldStreamPostDataFromDisk = YES;
    if (progress) {
        [request setUploadProgressDelegate:progress];
        [request setShowAccurateProgress:YES];
    }
    [request setDidFinishSelector:@selector(requestFetchComplete:)];
    [request setDidFailSelector:@selector(requestFetchFailed:)];
    [request setDelegate:self];
    
    if (delegate) {
        TKCommunicatorUserInfo *userInfo = [[[TKCommunicatorUserInfo alloc] init] autorelease];
        userInfo.delegate = delegate;
        [request setUserInfo:[NSDictionary dictionaryWithObject:userInfo forKey:kTKRequestUserInfo]];
    }
    
    [uploadRequestsSet addObject:request];
    [request startAsynchronous];
}

- (void)uploadFileToMethod:(NSString *)method
                      data:(NSData *)data
                    params:(NSDictionary *)params
                  delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
          progressDelegate:(id)progress {
    NSString *url = nil;
    if ([self.baseURL length] == 0) {
        url = method;
    } else {
        url = [self.baseURL stringByAppendingPathComponent:method];
    }
    url = [url stringByAddingQuery:params];
    [self requestFormPostURL:url params:params fileData:data delegate:delegate progressDelegate:progress];
}

-(BOOL)downloadFileSynToMethod:(NSString *)method
                        params:(NSDictionary *)params
               destinationFile:(NSString *)destinationFile
                timeOutSeconds:(NSTimeInterval)timeOutSeconds;
{
    NSString *url = nil;
    if ([self.baseURL length] == 0) {
        url = method;
    } else {
        url = [self.baseURL stringByAppendingPathComponent:method];
    }
    url = [url stringByAddingQuery:params];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];
    [request setRequestMethod:@"GET"];
    request.downloadDestinationPath = destinationFile;
    [request setTimeOutSeconds:timeOutSeconds];
    [request startSynchronous];
    
    return !request.error;
}

-(ZSTLegacyResponse *)openSynAPIGetToMethod:(NSString *)method
                                     params:(NSDictionary *)params
                             timeOutSeconds:(NSTimeInterval)timeOutSeconds
{
    NSString *url = nil;
    if ([self.baseURL length] == 0) {
        url = method;
    } else {
        url = [self.baseURL stringByAppendingPathComponent:method];
    }
    url = [url stringByAddingQuery:params];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    url = [url stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: version?version:@"", @"version", nil]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];
    [request setRequestMethod:@"GET"];
    //    [request addRequestHeader:@"Content-Type" value:@"application/xml"];
    [request setTimeOutSeconds:timeOutSeconds];
    [request startSynchronous];
    
    if (request.error) {
        return [self requestFetchFailed:request];
    } else {
        return [self requestFetchComplete:request];
    }
}

-(ZSTLegacyResponse *)openSynAPIPostToMethod:(NSString *)method
                                        body:(NSData *)body
                              timeOutSeconds:(NSTimeInterval)timeOutSeconds
{
    NSURL *url = nil;
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    if ([self.baseURL length] == 0) {
        url = [NSURL URLWithString:[method stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: version?version:@"", @"version", nil]]];
    } else {
        url = [NSURL URLWithString:[[self.baseURL stringByAppendingPathComponent:method] stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: version?version:@"", @"version", nil]]];
    }
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"POST"];
    [request setTimeOutSeconds:timeOutSeconds];
    [request appendPostData:body];
    [request startSynchronous];
    NSString *info = [NSString stringWithFormat:@"Will post URL:%@, body:%@", url, [[[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding] autorelease]];
    [ZSTLogUtil logSysInfo:info];
    
    if (request.error) {
        return [self requestFetchFailed:request];
    } else {
        return [self requestFetchComplete:request];
    }
}

-(ZSTLegacyResponse *)openSynAPIPostToMethod:(NSString *)method
                              replacedParams:(NSDictionary *)params
                              timeOutSeconds:(NSTimeInterval)timeOutSeconds
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"Legacy.bundle/Templates/%@", [[method lastPathComponent] stringByDeletingPathExtension]] ofType:@"xml"];
    NSString *Template = [NSString stringWithContentsOfFile: filePath encoding: NSUTF8StringEncoding error: nil];
    return [self openSynAPIPostToMethod:method Template:Template replacedParams:params timeOutSeconds:timeOutSeconds];
}

-(ZSTLegacyResponse *)openSynAPIPostToMethod:(NSString *)method
                              replacedParams:(NSDictionary *)params
{
    return [self openSynAPIPostToMethod:method replacedParams:params timeOutSeconds:30];
}

-(ZSTLegacyResponse *)openSynAPIPostToMethod:(NSString *)method
                                    Template:(NSString *)Template
                              replacedParams:(NSDictionary *)params
                              timeOutSeconds:(NSTimeInterval)timeOutSeconds
{
    NSLog(@"%@",params);
    NSData *body = [self postDataFromTemplate:Template replacedParams:params];
    return [self openSynAPIPostToMethod:method body:body timeOutSeconds:timeOutSeconds];
}

- (void)openAPIPostToMethod:(NSString *)method
                   Template:(NSString *)Template
             replacedParams:(NSDictionary *)params
               asynchronous:(BOOL)flag
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector
                   userInfo:(id)theUserInfo
{
    NSLog(@"params = %@",params);
    NSURL *url = nil;
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    if ([self.baseURL length] == 0) {
        url = [NSURL URLWithString:[method stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: version?version:@"", @"version", nil]]];
    } else {
        url = [NSURL URLWithString:[[self.baseURL stringByAppendingPathComponent:method] stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: version?version:@"", @"version", nil]]];
    }
    TKDPRINT(@"Will post url address : %@", url);
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"POST"];
    //    [request addRequestHeader:@"Content-Type" value:@"application/xml"];
    [request setTimeOutSeconds:20];
    NSData *body = [self postDataFromTemplate:Template replacedParams:params];
    [request appendPostData:body];
    
    TKCommunicatorUserInfo *userInfo = [[TKCommunicatorUserInfo alloc] init];
    userInfo.delegate = delegate;
    userInfo.succeedSelector = succeedSelector;
    userInfo.failSelector = failSelector;
    userInfo.userInfo = theUserInfo;
    
    [request setUserInfo:[NSMutableDictionary dictionaryWithObject:userInfo forKey:kTKRequestUserInfo]];
    [userInfo release];
    
    if (flag) {
        [networkQueue addOperation:request];
    } else {
        [request startSynchronous];
    }
}

- (void)openAPIPostToMethod:(NSString *)method
             replacedParams:(NSDictionary *)params
               asynchronous:(BOOL)flag
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector
                   userInfo:(id)theUserInfo
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"Legacy.bundle/Templates/%@", [[method lastPathComponent] stringByDeletingPathExtension]] ofType:@"xml"];
    NSString *Template = [NSString stringWithContentsOfFile: filePath encoding: NSUTF8StringEncoding error: nil];
    //    NSAssert1([template length] != 0 ,@"No such file at Path: %@.", filePath);
    [self openAPIPostToMethod:method Template:Template replacedParams:params asynchronous:flag delegate:delegate failSelector:failSelector succeedSelector:succeedSelector userInfo:theUserInfo];
}

- (void)openAPIPostToMethod:(NSString *)method
             replacedParams:(NSDictionary *)params
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector
                   userInfo:(id)theUserInfo
{
    [self openAPIPostToMethod:method replacedParams:params asynchronous:YES delegate:delegate failSelector:failSelector succeedSelector:succeedSelector userInfo:theUserInfo];
}

- (void)openAPIPostToMethod:(NSString *)method
             replacedParams:(NSDictionary *)params
               asynchronous:(BOOL)flag
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
                   userInfo:(id)userInfo
{
    [self openAPIPostToMethod:method replacedParams:params asynchronous:flag delegate:delegate failSelector:nil succeedSelector:nil userInfo:userInfo];
}

- (void)openAPIPostToMethod:(NSString *)method
             replacedParams:(NSDictionary *)params
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
                   userInfo:(id)userInfo
{
    [self openAPIPostToMethod:method replacedParams:params delegate:delegate failSelector:nil succeedSelector:nil userInfo:userInfo];
}

- (void)openAPIPostToMethod:(NSString *)method
             replacedParams:(NSDictionary *)params
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
{
    [self openAPIPostToMethod:method replacedParams:params delegate:delegate failSelector:nil succeedSelector:nil userInfo:nil];
}

// ############################ URL request methods ############################

#pragma mark - Helper
- (NSData *)postDataFromTemplate:(NSString *)Template
                  replacedParams:(NSDictionary *)params;
{
    NSRange range = NSMakeRange(0, [Template length]);
    NSMutableString *str = [[Template mutableCopy] autorelease];
    for (NSString *key in params) {
        range.length = [str length];
        NSString *value;
        id obj = [params objectForKey:key];
        
        if (obj == [NSNull null]) {
            value = @"";
        } else if (![obj isKindOfClass:[NSString class]]) {
            value = [obj description];
            
        } else
        {
            value = obj;
        }
        [str replaceOccurrencesOfString:[NSString stringWithFormat:@"${%@}", key] withString:value options:0 range:range];
    }
    TKDINFO(@"Will post xml param:%@", str);
    return [str dataUsingEncoding:NSUTF8StringEncoding];
}

// Form upload file
- (void)requestFormPostURL:(NSString *)url
                    params:(NSDictionary *)params
                  fileData:(NSData *)data
                  delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
          progressDelegate:(id)progress {
    
    url = [TKUtil buildURL:url params:params];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    url = [url stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: version?version:@"", @"version", nil]];
    
    TKDPRINT(@"Will post url address : %@", url);
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url]];
    request.postBody = (NSMutableData *)data;
    [request setTimeOutSeconds:180];
    //    [request setShouldContinueWhenAppEntersBackground:NO];
    if (progress) {
        [request setUploadProgressDelegate:progress];
        [request setShowAccurateProgress:YES];
    }
    [request setDidFinishSelector:@selector(requestFetchComplete:)];
    [request setDidFailSelector:@selector(requestFetchFailed:)];
    [request setDelegate:self];
    TKCommunicatorUserInfo *userInfo = [[TKCommunicatorUserInfo alloc] init];
    userInfo.delegate = delegate;
    
    //    [request addPostValue:sid forKey:kRequestUID];
    //    for (id key in [params allKeys]) {
    //        [request addPostValue:[params objectForKey:key] forKey:key];
    //    }
    //    [request addPostValue:[TKUtil md5Data:data] forKey:@"md5"];
    [request setUserInfo:[NSDictionary dictionaryWithObject:userInfo forKey:kTKRequestUserInfo]];
    
    [request startAsynchronous];
    [uploadRequestsSet addObject:request];
    [userInfo release];
}

#pragma mark - ASI delegate
- (void)requestDidFetchFailed:(ZSTLegacyResponse *)response{
    TKCommunicatorUserInfo *userInfo = [[response.request userInfo] objectForKey:kTKRequestUserInfo];
    
    if (userInfo.delegate) {
        if (!userInfo.failSelector || ![userInfo.delegate respondsToSelector:userInfo.failSelector]) {
            userInfo.failSelector = @selector(requestDidFail:);
        }
        if ([userInfo.delegate respondsToSelector:userInfo.failSelector]) {
            [userInfo.delegate performSelector:userInfo.failSelector withObject:response];
        }
    }
}

- (ZSTLegacyResponse *)requestFetchFailed:(ASIHTTPRequest *)request {
    TKDPRINT(@"request requestFetchFailed fetch has failed....... \n and error is : %@", [request error]);
    
    ZSTLegacyResponse *response = [ZSTLegacyResponse responseWithRequest:request];
    
    [uploadRequestsSet removeObject:request];
    [self requestDidFetchFailed:response];
    
    return response;
}

//Connection has occurred error!
- (ZSTLegacyResponse *)requestFetchComplete:(ASIHTTPRequest *)request {
    
    TKCommunicatorUserInfo *userInfo = [[request userInfo] objectForKey:kTKRequestUserInfo];
    [uploadRequestsSet removeObject:request];
    TKDINFO(@"The requestFetchComplete response : [%@]", [request responseString]);
    
    ZSTLegacyResponse *response = [ZSTLegacyResponse responseWithRequest:request];
    if (response.error) {
        [self requestDidFetchFailed:response];
    } else {
        if (userInfo.delegate) {
            if (!userInfo.succeedSelector || ![userInfo.delegate respondsToSelector:userInfo.succeedSelector]) {
                userInfo.succeedSelector = @selector(requestDidSucceed:);
            }
            if ([userInfo.delegate respondsToSelector:userInfo.succeedSelector]) {
                [userInfo.delegate performSelector:userInfo.succeedSelector
                                        withObject:response];
            }
        }
    }
    return response;
}

- (void)cancelByDelegate:(id)delegate {
    @synchronized(self) {
        TKCommunicatorUserInfo *userInfo;
        for (ASIHTTPRequest *request in [networkQueue operations]) {
            userInfo = [[request userInfo] objectForKey:kTKRequestUserInfo];
            if (userInfo.delegate == delegate) {
                [request clearDelegatesAndCancel];
                userInfo.delegate = nil;
            }
        }
    }
}

- (void)cancelUploadByDelegate:(id)delegate {
    @synchronized(self) {
        TKCommunicatorUserInfo *userInfo;
        for (ASIHTTPRequest *request in uploadRequestsSet) {
            userInfo = [[request userInfo] objectForKey:kTKRequestUserInfo];
            if (userInfo.delegate == delegate) {
                TKDPRINT(@"The cancel upload delegate is : %@", delegate);
                [request clearDelegatesAndCancel];
                userInfo.delegate = nil;
            }
        }
    }
}

@end

#pragma mark -
@implementation TKCommunicatorUserInfo

@synthesize delegate;
@synthesize failSelector;
@synthesize succeedSelector;
@synthesize userInfo;

- (id)init {
    self = [super init];
    if (self) {
        uid = [[NSString alloc] init];
    }
    return self;
}

- (void)dealloc {
    self.userInfo = nil;
    delegate = nil;
    failSelector = nil;
    succeedSelector = nil;
    TKRELEASE(uid);
    [super dealloc];
}
@end
