//
//  ZSTShareView.h
//  F3
//
//  Created by pmit on 15/8/17.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WXApi.h>
#import <TencentOpenAPI/TencentOAuth.h>

@protocol ZSTShareViewDelegate <NSObject>

- (void)dismissShareView;

@end

@interface ZSTShareView : UIView <TencentSessionDelegate>
{
     enum WXScene _scene;
}

@property (weak,nonatomic) id<ZSTShareViewDelegate> delegate;
@property (copy,nonatomic) NSString *shareString;
@property (strong,nonatomic) TencentOAuth *tencentOAuth;
@property (copy,nonatomic) NSString *imageView;

- (UIView *)createUIWithFrame:(CGRect)frame;

@end
