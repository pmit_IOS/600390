//
//  ZSTAMBindingMobileVC.h
//  F3
//
//  Created by P&M on 15/8/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTAMBindingMobileVC : UIViewController <UITextFieldDelegate, ZSTF3EngineDelegate>

@property (strong, nonatomic) UIView *firstView;
@property (strong, nonatomic) UIView *secondView;

@property (strong, nonatomic) UITextField *mobileTF;
@property (strong, nonatomic) UITextField *passTF;

@property (strong, nonatomic) ZSTF3Engine *engine;

@end
