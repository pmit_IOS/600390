//
//  ZSTPersonalViewController.m
//  F3
//
//  Created by LiZhenQu on 14-10-28.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonalViewController.h"
#import "ZSTPersonalHeaderCell.h"
#import "UIImage+Compress.h"
#import "ZSTChangeNameViewController.h"
#import "ZSTPersonalInfo.h"
#import "ZSTPersonalAddressTableViewCell.h"
#import "ZSTUtils.h"

#define kZSTPersonalItem_Avatar                 0
#define kZSTPersonalItem_Name                   1
#define kZSTPersonalItem_PhoneNum               2
#define kZSTPersonalItem_Birthday               3
#define kZSTPersonalItem_Sex                    4
#define kZSTPersonalItem_Address                5
#define kZSTPersonalItem_Signature              6

#define currentMonth [currentMonthString integerValue]

@interface ZSTPersonalViewController ()
{
    NSString *timeString;
    
    NSMutableArray *yearArray;
    NSArray *monthArray;
    NSMutableArray *DaysArray;
    NSArray *amPmArray;
    NSArray *hoursArray;
    NSMutableArray *minutesArray;
    
    NSString *currentMonthString;
    
    NSInteger selectedYearRow;
    NSInteger selectedMonthRow;
    NSInteger selectedDayRow;
    
    BOOL firstTimeLoad;
    
    UserInfo *userInfo;
}

@end

@implementation ZSTPersonalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"个人信息", @"")];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    firstTimeLoad = YES;
    self.datePicker.hidden = YES;
    self.toolbarCancelDone.hidden = YES;
    
    NSDate *date = [NSDate date];
    
    // Get Current Year
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy"];
    
    NSString *currentyearString = [NSString stringWithFormat:@"%@",[formatter stringFromDate:date]];
    
    // Get Current  Month
    [formatter setDateFormat:@"MM"];
    
    currentMonthString = [NSString stringWithFormat:@"%@",@([[formatter stringFromDate:date]integerValue])];
    
    // Get Current  Date
    [formatter setDateFormat:@"dd"];
    NSString *currentDateString = [NSString stringWithFormat:@"%@",[formatter stringFromDate:date]];
    
    /*一下这些貌似没有地方用得到
    // Get Current  Hour
    [formatter setDateFormat:@"hh"];
    NSString *currentHourString = [NSString stringWithFormat:@"%@",[formatter stringFromDate:date]];
    
    // Get Current  Minutes
    [formatter setDateFormat:@"mm"];
    NSString *currentMinutesString = [NSString stringWithFormat:@"%@",[formatter stringFromDate:date]];
    
    // Get Current  AM PM
    [formatter setDateFormat:@"a"];
    NSString *currentTimeAMPMString = [NSString stringWithFormat:@"%@",[formatter stringFromDate:date]];
    */
    
    // PickerView -  Years data
    yearArray = [[NSMutableArray alloc]init];
    
    for (int i = 1970; i <= 2050 ; i++)
    {
        [yearArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    // PickerView -  Months data
    monthArray = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12"];
    
    // PickerView -  Hours data
    hoursArray = @[@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12"];
    
    // PickerView -  Hours data
    minutesArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < 60; i++)
    {
        [minutesArray addObject:[NSString stringWithFormat:@"%02d",i]];
    }
    
    // PickerView -  AM PM data
    amPmArray = @[@"上午",@"下午"];
    
    // PickerView -  days data
    DaysArray = [[NSMutableArray alloc]init];
    
    for (int i = 1; i <= 31; i++)
    {
        if (i < 10) {
            [DaysArray addObject:[NSString stringWithFormat:@"0%d",i]];
        } else {
            [DaysArray addObject:[NSString stringWithFormat:@"%d",i]];
            
        }
    }
    
    // PickerView - Default Selection as per current Date
    [self.datePicker selectRow:[yearArray indexOfObject:currentyearString] inComponent:0 animated:YES];
    [self.datePicker selectRow:[monthArray indexOfObject:currentMonthString] inComponent:1 animated:YES];
    [self.datePicker selectRow:[DaysArray indexOfObject:currentDateString] inComponent:2 animated:YES];
    
    _scrollView.layer.borderWidth = 1;
    _scrollView.layer.cornerRadius = 4;
    _scrollView.layer.borderColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1].CGColor;
    _scrollView.layer.masksToBounds = YES;
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [TKUIUtil showHUD:self.view];
    [self.engine getUserInfoWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0)
    {
        selectedYearRow = row;
        [self.datePicker reloadAllComponents];
    }
    else if (component == 1)
    {
        selectedMonthRow = row;
        [self.datePicker reloadAllComponents];
    }
    else if (component == 2)
    {
        selectedDayRow = row;
        
        [self.datePicker reloadAllComponents];
    }
}

#pragma mark - UIPickerViewDatasource

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row
          forComponent:(NSInteger)component
           reusingView:(UIView *)view {
    
    // Custom View created for each component
    UILabel *pickerLabel = (UILabel *)view;
    
    if (pickerLabel == nil) {
        CGRect frame = CGRectMake(0.0, 0.0, 50, 60);
        pickerLabel = [[UILabel alloc] initWithFrame:frame];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont systemFontOfSize:15.0f]];
    }
    
    if (component == 0)
    {
        pickerLabel.text =  [yearArray objectAtIndex:row]; // Year
    }
    else if (component == 1)
    {
        pickerLabel.text =  [monthArray objectAtIndex:row];  // Month
    }
    else if (component == 2)
    {
        pickerLabel.text =  [DaysArray objectAtIndex:row]; // Date
        
    }
    else if (component == 3)
    {
        pickerLabel.text =  [hoursArray objectAtIndex:row]; // Hours
    }
    else if (component == 4)
    {
        pickerLabel.text =  [minutesArray objectAtIndex:row]; // Mins
    }
    else
    {
        pickerLabel.text =  [amPmArray objectAtIndex:row]; // AM/PM
    }
    
    return pickerLabel;
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return [yearArray count];
    }
    else if (component == 1)
    {
        return [monthArray count];
    }
    else if (component == 2)
    { // day
        
        if (firstTimeLoad)
        {
            if (currentMonth == 1 || currentMonth == 3 || currentMonth == 5 || currentMonth == 7 || currentMonth == 8 || currentMonth == 10 || currentMonth == 12)
            {
                return 31;
            }
            else if (currentMonth == 2)
            {
                int yearint = [[yearArray objectAtIndex:selectedYearRow]intValue ];
                
                if(((yearint %4==0)&&(yearint %100!=0))||(yearint %400==0)){
                    
                    return 29;
                }
                else
                {
                    return 28; // or return 29
                }
            }
            else
            {
                return 30;
            }
        }
        else
        {
            if (selectedMonthRow == 0 || selectedMonthRow == 2 || selectedMonthRow == 4 || selectedMonthRow == 6 || selectedMonthRow == 7 || selectedMonthRow == 9 || selectedMonthRow == 11)
            {
                return 31;
            }
            else if (selectedMonthRow == 1)
            {
                int yearint = [[yearArray objectAtIndex:selectedYearRow]intValue ];
                
                if(((yearint %4==0)&&(yearint %100!=0))||(yearint %400==0)){
                    return 29;
                }
                else
                {
                    return 28; // or return 29
                }
            }
            else
            {
                return 30;
            }
        }
    }
    else if (component == 3)
    { // hour
        
        return 12;
        
    }
    else if (component == 4)
    { // min
        return 60;
    }
    else
    { // am/pm
        return 2;
    }
}

-(void)setBlock:(saveInfoBlocked) btnClickedBlock
{
    _btnSaveBlock = [btnClickedBlock copy];
}

#pragma mark - UITableViewDatasource

 - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *headeridentifier = @"ZSTPersonalHeaderCell";
    static NSString *variableidentifier = @"ZSTPersonalVariableCell";
    static NSString *sexidentifier = @"ZSTPersonalSexCell";
    static NSString *birthdayidentifier = @"ZSTPersonalBirthDayTableViewCell";
    static NSString *addressidentifier = @"ZSTPersonalAddressTableViewCell";
    
    if (indexPath.row == kZSTPersonalItem_Avatar) {
        ZSTPersonalHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:headeridentifier];
        
        if (!cell) {
            
            NSArray* array = [[UINib nibWithNibName:headeridentifier bundle:nil] instantiateWithOwner:self options:nil];
            cell = [array objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.avatarbtn addTarget:self action:@selector(changeAvatar:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        cell.tag = indexPath.row;
        [cell.avatarImgView setImageWithURL:[NSURL URLWithString:userInfo.iconUrl]];
//        [cell.avatarImgView clear];
//        cell.avatarImgView.url = [NSURL URLWithString:userInfo.iconUrl];
//        [cell.avatarImgView loadImage];
    
        
        return cell;
    } else if (indexPath.row == kZSTPersonalItem_Name) {
        
        ZSTPersonalVariableCell *cell = [tableView dequeueReusableCellWithIdentifier:variableidentifier];
        
        if (!cell) {
            
            NSArray* array = [[UINib nibWithNibName:variableidentifier bundle:nil] instantiateWithOwner:self options:nil];
            cell = [array objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        cell.tag = indexPath.row;
        cell.nameLabel.text = userInfo.userName;
        
        return cell;
    } else if (indexPath.row == kZSTPersonalItem_Sex) {
        
        ZSTPersonalSexCell *cell = [tableView dequeueReusableCellWithIdentifier:sexidentifier];
        
        if (!cell) {
            
            NSArray* array = [[UINib nibWithNibName:sexidentifier bundle:nil] instantiateWithOwner:self options:nil];
            cell = [array objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        }
        
        if (userInfo.sex == 1) {
            
            cell.sexSwitch.status = PMCustomSwitchStatusOff;
        } else {
            
            cell.sexSwitch.status = PMCustomSwitchStatusOn;
        }
        
        return cell;
    } else if (indexPath.row == kZSTPersonalItem_Birthday) {
        
        ZSTPersonalBirthDayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:birthdayidentifier];
        
        if (!cell) {
            
            NSArray* array = [[UINib nibWithNibName:birthdayidentifier bundle:nil] instantiateWithOwner:self options:nil];
            cell = [array objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.titleLabel.text = @"生日";
        }
        
        cell.delegate = self;
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM-dd"];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:userInfo.birthday];
        NSString *dateStr = [formatter stringFromDate:date];
        
        if(userInfo.birthday == 0) {
            
            dateStr = @"未设置";
        }
        
        cell.contentField.text = dateStr;
        cell.contentField.enabled = NO;
        
//        if (userInfo.days < 30) {
//            
//            cell.remindLabel.hidden = NO;
//            if (userInfo.days == 0) {
//                
//                cell.remindLabel.text = [NSString stringWithFormat:@"生日快乐"];
//            } else {
//                cell.remindLabel.text = [NSString stringWithFormat:@"%d天后生日",userInfo.days];
//            }
//        } else {
//            
//            cell.remindLabel.hidden = YES;
//        }
        
        cell.tag = indexPath.row;
        
        return cell;
    } else if (indexPath.row == kZSTPersonalItem_PhoneNum) {
        
        ZSTPersonalVariableCell *cell = [tableView dequeueReusableCellWithIdentifier:variableidentifier];
        
        if (!cell) {
            
            NSArray* array = [[UINib nibWithNibName:variableidentifier bundle:nil] instantiateWithOwner:self options:nil];
            cell = [array objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.rightImgView.hidden = YES;
            cell.titleLabel.text = @"账号";
        }
        
        cell.nameLabel.text = userInfo.Msisdn;
        cell.nameLabel.textColor = RGBACOLOR(0, 0, 0, 0.26);
        
        cell.tag = indexPath.row;
        
        return cell;
    } else if (indexPath.row == kZSTPersonalItem_Address) {
        
        ZSTPersonalAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:addressidentifier];
        
        if (!cell) {
            
            NSArray* array = [[UINib nibWithNibName:addressidentifier bundle:nil] instantiateWithOwner:self options:nil];
            cell = [array objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.titleLabel.text = @"地址";
        }
        
        [cell configCell:userInfo.address];
        cell.contentLabel.textColor = RGBACOLOR(0, 0, 0, 0.26);
        
        cell.backgroundColor = [UIColor clearColor];
        
        CGRect frame = cell.frame;
        if (cell.cellHeight < 54) {
            
            cell.cellHeight = 54;
        }
        frame.size.height = cell.cellHeight;
        cell.frame = frame;
        cell.tag = indexPath.row;
        
        return cell;
    } else if (indexPath.row == kZSTPersonalItem_Signature) {
        
        ZSTPersonalAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:variableidentifier];
        
        if (!cell) {
            
            NSArray* array = [[UINib nibWithNibName:addressidentifier bundle:nil] instantiateWithOwner:self options:nil];
            cell = [array objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.titleLabel.text = @"个性签名";
        }
        
        cell.contentLabel.textColor = RGBACOLOR(0, 0, 0, 0.26);
        [cell configCell:userInfo.signature];
        
        cell.backgroundColor = [UIColor clearColor];
        
        CGRect frame = cell.frame;
        if (cell.cellHeight < 54) {
            
            cell.cellHeight = 54;
        }
        
        frame.size.height = cell.cellHeight;
        cell.frame = frame;
        cell.tag = indexPath.row;
        
        return cell;
    }

    return nil;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == kZSTPersonalItem_Address) {
        
        ZSTPersonalAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZSTPersonalAddressTableViewCell"];
        
        if (!cell) {
            
            NSArray* array = [[UINib nibWithNibName:@"ZSTPersonalAddressTableViewCell" bundle:nil] instantiateWithOwner:self options:nil];
            cell = [array objectAtIndex:0];
        }
    
        [cell configCell:userInfo.address];
        
        return cell.cellHeight;
    } else if (indexPath.row == kZSTPersonalItem_Signature) {
        
        ZSTPersonalAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZSTPersonalAddressTableViewCell"];
        
        if (!cell) {
            
            NSArray* array = [[UINib nibWithNibName:@"ZSTPersonalAddressTableViewCell" bundle:nil] instantiateWithOwner:self options:nil];
            cell = [array objectAtIndex:0];
        }
        
        [cell configCell:userInfo.signature];
        
        return cell.cellHeight;
    }
    return 54;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失
    
    ZSTChangeNameViewController *controller = [[ZSTChangeNameViewController alloc] init];
    controller.userInfo = userInfo;
    
    [controller setBlock:^(NSString *nickname) {
        
        _btnSaveBlock(nickname,2);
    }];
    
    if (indexPath.row == kZSTPersonalItem_Name) {
        
        controller.type = kZSTPersonalItem_Name;
        [self.navigationController pushViewController:controller animated:YES];
        
    } else if (indexPath.row == kZSTPersonalItem_Birthday) {
        
        ZSTPersonalBirthDayTableViewCell *cell = (ZSTPersonalBirthDayTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        [self textFieldCellDidBeginEditing:cell];
        
    } else if (indexPath.row == kZSTPersonalItem_Address) {
        
        controller.type = kZSTPersonalItem_Address;
        [self.navigationController pushViewController:controller animated:YES];
    } else if (indexPath.row == kZSTPersonalItem_Signature) {
       
        controller.type = kZSTPersonalItem_Signature;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void) changeAvatar:(id)sender
{
    UIActionSheet *actionSheet = nil;
    //是否支持拍照
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"获取图片"
                                                  delegate:self
                                         cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"拍照", @"本地图片", nil];
    } else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"获取图片"
                                                  delegate:self
                                         cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"本地图片", nil];
    }
    
    [actionSheet showInView:self.view];
    
}

- (IBAction)actionCancel:(id)sender
{
    [self datePickerResignFirstResponder];
}

- (void) datePickerResignFirstResponder
{
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         self.datePicker.hidden = YES;
                         self.toolbarCancelDone.hidden = YES;
                     }
                     completion:^(BOOL finished){
                         
                         
                     }];
}

- (IBAction)actionDone:(id)sender
{
    [self datePickerResignFirstResponder];
    
    timeString = [NSString stringWithFormat:@"%@-%@-%@",[yearArray objectAtIndex:[self.datePicker selectedRowInComponent:0]],[monthArray objectAtIndex:[self.datePicker selectedRowInComponent:1]],[DaysArray objectAtIndex:[self.datePicker selectedRowInComponent:2]]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:timeString];
    long timeSp = (long)[date timeIntervalSince1970];
    
     [dateFormatter setDateFormat:@"MM-dd"];
     NSString *dateStr = [dateFormatter stringFromDate:date];
    
    NSDate *nowdate = [NSDate date];
    long nowtimeSp = (long)[nowdate timeIntervalSince1970];
    
    if (nowtimeSp < timeSp) {
        
        [TKUIUtil alertInWindow:@"亲～，你穿越了吗" withImage:nil];
        return;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:kZSTPersonalItem_Birthday inSection:0];
    
    ZSTPersonalBirthDayTableViewCell *cell = (ZSTPersonalBirthDayTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.contentField.text = dateStr;
    
    [self.engine updateUserInfoWith:userInfo.Msisdn userId:[ZSTF3Preferences shared].UserId iconUrl:userInfo.iconUrl  name:userInfo.userName birthday:timeSp sex:userInfo.sex address:userInfo.address signature:userInfo.signature phonePublic:[ZSTF3Preferences shared].snsaStates];
}

- (void)getUserInfoDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    userInfo = [UserInfo userInfoWithdic:[response safeObjectForKey:@"data"]];
    
    [self.tableView reloadData];
    
    CGSize size = [userInfo.address sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(150, 80) lineBreakMode:NSLineBreakByCharWrapping];
    CGFloat tmpHeight = size.height + 32;
    size = [userInfo.signature sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(150, 80) lineBreakMode:NSLineBreakByCharWrapping];
    tmpHeight += size.height + 32;
    
    CGFloat cellheight = 16;
    if ([userInfo.address isEqualToString:@"未填写"] || [userInfo.signature isEqualToString:@"未填写"]) {
        
        cellheight = 0;
    }
    
//    CGFloat height = tmpHeight - 54 * 2;
//    if (height > 0) {
//        
//        CGRect frame = _scrollView.frame;
//        frame.size.height = 378 + height;
//        if (frame.size.height < (480-20-20-44-20+(iPhone5?88:0))) {
//            
//            _scrollView.frame = CGRectMake(10, 20, 300, frame.size.height-cellheight);
//        } else {
//            
//            _scrollView.frame = CGRectMake(10, 20, 300, (480-20-20-44-20+(iPhone5?88:0)));
//        }
//        _scrollView.contentSize = CGSizeMake(300, frame.size.height);
//    } else {
//        
//        _scrollView.frame = CGRectMake(10, 20, 300, 378);
//    }
    
    CGRect frame = _scrollView.frame;
    frame.size.height = self.tableView.contentSize.height;
    _scrollView.frame = frame;
}

- (void)getUserInfoDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}


#pragma mark - Table view TextFieldCellDelegate
- (void) textFieldCellTextDidChange:(ZSTPersonalVariableCell *)cell
{
    
}

- (void) textFieldCellDidBeginEditing:(ZSTPersonalBirthDayTableViewCell*)cell
{
    
    [cell.contentField resignFirstResponder];
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         self.datePicker.hidden = NO;
                         self.toolbarCancelDone.hidden = NO;
                         
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

#pragma mark - Table view PersonalSexCellDelegate
- (void) cell:(ZSTPersonalSexCell *)cell sexSelection:(int)sex
{
    if (!userInfo || sex == userInfo.sex) {
        
        return;
    }
    userInfo.sex = sex;
    
    if (!userInfo.iconUrl) {
        userInfo.iconUrl = @"";
    }
    
     [self.engine updateUserInfoWith:userInfo.Msisdn userId:[ZSTF3Preferences shared].UserId iconUrl:userInfo.iconUrl  name:userInfo.userName birthday:userInfo.birthday sex:sex address:userInfo.address signature:userInfo.signature phonePublic:[ZSTF3Preferences shared].snsaStates];
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    self.iconPickerController = [[UIImagePickerController alloc] init];
    [_iconPickerController.navigationBar setTintColor:RGBCOLOR(27, 140, 233)];
    
    if ( buttonIndex == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) { // 拍照
        
        _iconPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        _iconPickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        _iconPickerController.showsCameraControls = YES;
        
    } else if(buttonIndex != actionSheet.cancelButtonIndex) { // 本地图片
        
        _iconPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    }
    _iconPickerController.allowsEditing = YES;
    _iconPickerController.delegate = self;
    [self presentViewController:_iconPickerController animated:YES completion:nil];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (_iconPickerController.sourceType == UIImagePickerControllerSourceTypePhotoLibrary  && [navigationController.viewControllers count] <=2) {
        viewController.wantsFullScreenLayout = NO;
        navigationController.navigationBar.translucent = NO;
    }else{
        viewController.wantsFullScreenLayout = YES;
        navigationController.navigationBar.translucent = YES;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType =[NSString stringWithFormat:@"%@", [info objectForKey:UIImagePickerControllerMediaType]];
    if (![mediaType isEqualToString:@"public.image"]) {
        return;
    }
    
//    UIImage *tempImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *tempImage = [info objectForKey:UIImagePickerControllerEditedImage];
    [self compressImage:tempImage];
}

- (void)compressImage:(UIImage *)bigImage
{
    [self performSelector:@selector(doCompressImage:) withObject:bigImage afterDelay:0.6];
    UIImage *newImage = [UIImage imageWithData:[[bigImage compressedImageScaleFactor:640] compressedData]];
    
    NSData *data = UIImageJPEGRepresentation(newImage,0.5);
    [self.engine uploadFileWith:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId data:data];
}

- (void)doCompressImage:(UIImage *)bigImage
{
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    ZSTPersonalHeaderCell *cell = ( ZSTPersonalHeaderCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.avatarImgView.image = bigImage;
    
    [self.iconPickerController dismissViewControllerAnimated:YES completion:nil];
    self.iconPickerController = nil;
}

- (void)updateUserInfoDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    NSString *message = [response safeObjectForKey:@"notice"];
    [TKUIUtil alertInWindow:message withImage:nil];
}

- (void)updateUserInfoDidFailed:(NSString *)respone
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:respone withImage:nil];
}

- (void)updatePointDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
}

- (void)updatePointDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}

- (void)uploadFileDidSucceed:(NSDictionary *)response
{
    NSString *iconurl = [[response safeObjectForKey:@"data"] safeObjectForKey:@"Icon"];
    [self.engine updateUserInfoWith:userInfo.Msisdn userId:[ZSTF3Preferences shared].UserId iconUrl:iconurl  name:userInfo.userName birthday:userInfo.birthday sex:userInfo.sex address:userInfo.address signature:userInfo.signature phonePublic:[ZSTF3Preferences shared].snsaStates];
    
    _btnSaveBlock(iconurl,1);
}

- (void)uploadFileDidFailed:(NSString *)respone
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:respone withImage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
