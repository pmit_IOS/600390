//
//  ZSTInformViewController.m
//  F3
//
//  Created by LiZhenQu on 14-10-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTInformViewController.h"
#import "ZSTUtils.h"
#import "ZSTLogUtil.h"
#import <AudioToolbox/AudioToolbox.h>
#import "ZSTShell.h"
#import "ZSTF3Engine.h"

#define  kZSTSettingItemType_Sound          0
#define  kZSTSettingItemType_Shake          1
#define  kZSTSettingItemType_CleanMsg       2

@interface ZSTInformViewController ()
{
     NSMutableArray *descriptionArray;
}

@end

@implementation ZSTInformViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
     self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"通知设置", nil)];
    
    self.tableView.layer.borderWidth = 1;
    self.tableView.layer.cornerRadius = 4;
    self.tableView.layer.borderColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1].CGColor;
    self.tableView.layer.masksToBounds = YES;
    
    self.phoneView.layer.borderWidth = 1;
    self.phoneView.layer.cornerRadius = 4;
    self.phoneView.layer.borderColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1].CGColor;
    self.phoneView.layer.masksToBounds = YES;
    
//    _switchBtn = [[ZSTCustomSwitch alloc] initWithFrame:CGRectMake(235, 15, 49, 24)];
    _switchBtn = [[PMCustomSwitch alloc] initWithFrame:CGRectMake(235, 15, 49, 24)];
    _switchBtn.delegate = self;
//    _switchBtn.arrange = ZSTCustomSwitchArrangeOFFLeftONRight;
//    _switchBtn.onImage = ZSTModuleImage(@"module_personal_on.png");
//    _switchBtn.offImage = ZSTModuleImage(@"module_personal_off.png");
    
    if ([ZSTF3Preferences shared].snsaStates == 0) {
        _switchBtn.status = PMCustomSwitchStatusOn;
        _switchBtn.customSwitch.on = YES;
    }
    else {
        _switchBtn.status = PMCustomSwitchStatusOff;
        _switchBtn.customSwitch.on = NO;
    }
    
    [self.phoneView addSubview:_switchBtn];

    [self initSettingItems];
    
    self.engine = [[[ZSTF3Engine alloc] init] autorelease];
    self.engine.delegate = self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
          [TKUIUtil alertInWindow:@"检测到您未登录，请返回上级" withImage:nil];
         _switchBtn.status = 0;
     } else {
    
         _switchBtn.status = [ZSTF3Preferences shared].snsaStates;
     }
}

- (void) initSettingItems
{
    self.dataArray = [[NSMutableArray alloc] initWithObjects:@"声音",@"震动",@"消息清理提示", nil];
    descriptionArray = [[NSMutableArray alloc] initWithObjects:@"手机自身声音功能需开启",@"手机自身振动功能需要开启",@"消息大于100条，提示清理", nil];
    
    if (![ZSTShell isModuleAvailable:-1] && ![ZSTShell isModuleAvailable:22]) {
        [self.dataArray removeObject:@"消息清理提示"];
        [descriptionArray removeObject:@"消息大于100条，提示清理"];
    
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"ZSTMsgSettingTableViewCell";
    ZSTMsgSettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        
        NSArray* array = [[UINib nibWithNibName:identifier bundle:nil] instantiateWithOwner:self options:nil];
        cell = [array objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.delegate = self;
    cell.tag = indexPath.row;
    cell.titleLabel.text = [self.dataArray objectAtIndex:indexPath.row];
    cell.descriptionLabel.text = [descriptionArray objectAtIndex:indexPath.row];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    if (indexPath.row == kZSTSettingItemType_Sound) {
        if ([[ud objectForKey:@"soundsOpen"] integerValue] == 0)
        {
            cell.switchBtn.status = PMCustomSwitchStatusOn;
        }
        else
        {
            cell.switchBtn.status = PMCustomSwitchStatusOff;
        }
        
        cell.switchBtn.tag = kZSTSettingItemType_Sound;
    } else if (indexPath.row == kZSTSettingItemType_Shake) {
        NSLog(@"shakei-->%@",[ud objectForKey:@"shakeOpen"]);
        if ([[ud objectForKey:@"shakeOpen"] integerValue] == 0)
        {
            cell.switchBtn.status = PMCustomSwitchStatusOn;
        }
        else
        {
            cell.switchBtn.status = PMCustomSwitchStatusOff;
        }
        cell.switchBtn.tag = kZSTSettingItemType_Shake;
    } else if (indexPath.row == kZSTSettingItemType_CleanMsg) {
        if ([[ud objectForKey:@"cleanOpen"] integerValue] == 0)
        {
            cell.switchBtn.status = PMCustomSwitchStatusOn;
        }
        else
        {
            cell.switchBtn.status = PMCustomSwitchStatusOff;
        }
        cell.switchBtn.tag = kZSTSettingItemType_CleanMsg;
    }
    
    return cell;
}

- (void) cell:(ZSTMsgSettingTableViewCell *)cell set:(int)states
{
    switch (cell.switchBtn.tag) {
        case kZSTSettingItemType_Sound:
            
            if (cell.switchBtn.status == PMCustomSwitchStatusOn) {
                
                [ZSTLogUtil logSysInfo: NSLocalizedString(@"声音提示打开", nil)];
                //                [ZSTUtils playAlertSound:@"sms-received1" withExtension:@"caf"];
                
            } else {
                
                [ZSTLogUtil logSysInfo: NSLocalizedString(@"声音提示关闭", nil)];
            }
            //            [ZSTF3Preferences shared].sound = states;
            [[NSUserDefaults standardUserDefaults] setValue:@(states) forKey:@"soundsOpen"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            break;
        case kZSTSettingItemType_Shake:
            
            if (cell.switchBtn.status == PMCustomSwitchStatusOn) {
                
                //                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                [ZSTLogUtil logSysInfo: NSLocalizedString(@"震动提示打开", nil)];
                
            } else {
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"震动提示关闭", nil)];
            }
            
            //            [ZSTF3Preferences shared].shake = states;
            NSLog(@"%d",states);
            [[NSUserDefaults standardUserDefaults] setValue:@(states) forKey:@"shakeOpen"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            break;
        case kZSTSettingItemType_CleanMsg:
            
            if (cell.switchBtn.status == PMCustomSwitchStatusOn) {
                
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"信息自动清理功能打开", nil)];
                
                postNotificationOnMainThreadNoWait(NotificationName_CleanupMessages);
                
                [ZSTF3Preferences shared].cleanupDate = [NSDate date];
            } else {
                
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"信息自动清理功能关闭", nil)];
            }
            
            //            [ZSTF3Preferences shared].cleanUp = states;
            [[NSUserDefaults standardUserDefaults] setValue:@(states) forKey:@"cleanOpen"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            break;
        default:
            break;
    }
}

-(void)zstcustomSwitchSetStatus:(PMCustomSwitchStatus)status
{
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        return;
    }
    
    if (status != [ZSTF3Preferences shared].snsaStates) {
        [ZSTF3Preferences shared].snsaStates = status;
    
        [self.engine updateUserInfoWith:self.userInfo.Msisdn userId:[ZSTF3Preferences shared].UserId iconUrl:self.userInfo.iconUrl  name:self.userInfo.userName birthday:self.userInfo.birthday sex:self.userInfo.sex address:self.userInfo.address signature:self.userInfo.signature  phonePublic:[ZSTF3Preferences shared].snsaStates];
    }
}

- (void)updateUserInfoDidSucceed:(NSDictionary *)response
{
    NSString *message = [response safeObjectForKey:@"notice"];
    [TKUIUtil alertInWindow:message withImage:nil];
}

- (void)updateUserInfoDidFailed:(NSString *)respone
{
    [TKUIUtil alertInWindow:respone withImage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [_switchBtn release];
    self.phoneView = nil;
    [_dataArray release];
    [descriptionArray release];
    TKRELEASE(_tableView);
    [super dealloc];
}

@end
