//
//  LauncherViewController.m
//  F3
//
//  Created by luobin on 11/9/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//
#define RADIANS(degrees) ((degrees * M_PI) / 180.0)
#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define SelectNULL 99999999

#import "ZSTLauncherView.h"

@implementation ZSTLauncherView

@synthesize columnCount = _columnCount;
@synthesize rowCount = _rowCount;
@synthesize dataSource = _dataSource;
@synthesize delegate = _delegate;
@synthesize isHorizontal = _isHorizontal;
@synthesize largeIconVertical = _largeIconVertical;
@synthesize midIconVertical = _midIconVertical;

-(id)init
{
    self = [super init];
    if (self) {
        
        _rowCount = 3;
        _columnCount = 3;
        
        _editing = NO;
        
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.delegate = self;
        [self addSubview:_scrollView];
                
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    _scrollView.frame = frame;
}

- (void)dealloc {
    
    [_scrollView release];
    [_btns release];
    [_pageControl release];
    [super dealloc];
}

#pragma mark - ZSTLauncherButtonViewDelegate

- (void)launcherButtonDidLongPress:(ZSTLauncherButtonView *)launcherButton
{
    _isLongPress = YES;

}

- (void)launcherButtonDidClick:(ZSTLauncherButtonView *)launcherButton
{
    if ([launcherButton isEditing] || _isLongPress) {
        _isLongPress = NO;
        return;
    }
    
    if ([_delegate respondsToSelector:@selector(launcherView:didClickItemAtIndex:)]) {
        [_delegate launcherView:self didClickItemAtIndex:launcherButton.tag];
    }
}

- (void)launcherButtonDidClickDeleteButton:(ZSTLauncherButtonView *)launcherButton
{
    if ([_delegate respondsToSelector:@selector(launcherView:didClickDeleteButtonAtIndex:)]) {
        [_delegate launcherView:self didClickDeleteButtonAtIndex:launcherButton.tag];
    }
}

- (void)selected:(NSUInteger)itemIndex
{
    for (ZSTLauncherButtonView *launcherBtn in _btns) {
        launcherBtn.alpha = 1.00f;
    }
    if (itemIndex != SelectNULL ) {
        ZSTLauncherButtonView *btn = [_btns objectAtIndex:itemIndex];
        btn.alpha = 0.70f;
    }
}

- (void)setEnable:(BOOL)enable atIndex:(NSUInteger)index
{
    NSUInteger cout = [_btns count];
    if (index >= cout) {
        [NSException raise:@"RangeException" format:@"NSArray index Out of range:%@. The length of NSArray is %@, but except to index: %@", @(index), @(cout), @(index)];
    }
    ZSTLauncherButtonView *btn = [_btns objectAtIndex:index];
    [btn setEnable:enable];
}

- (void)setBadgeNumber:(NSUInteger)badgeNumber atIndex:(NSUInteger)index
{
    NSUInteger cout = [_btns count];
    if (index >= cout) {
        [NSException raise:@"RangeException" format:@"NSArray index Out of range:%@. The length of NSArray is %@, but except to index: %@", @(index), @(cout), @(index)];
    }
    ZSTLauncherButtonView *btn = [_btns objectAtIndex:index];
    [btn setBadgeNumber:badgeNumber];
}

- (void)_wobble {
    static BOOL wobblesLeft = NO;
    
    if (_editing) {
        CGAffineTransform leftWobble = CGAffineTransformMakeRotation(RADIANS(-3.0));
        CGAffineTransform rightWobble = CGAffineTransformMakeRotation(RADIANS(3.0));
        
        [UIView beginAnimations:nil context:nil];
        
        for (int i=0; i<[_btns count]; i++) {
            ZSTLauncherButtonView *btn = [_btns objectAtIndex:i];
            if (i %2 == 0) {
                btn.transform = wobblesLeft ? rightWobble : leftWobble;
            } else {
                btn.transform = wobblesLeft ? leftWobble : rightWobble;
            }
        }
        
        [UIView setAnimationDuration:0.07];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(_wobble)];
        wobblesLeft = !wobblesLeft;
        
        [UIView commitAnimations];
    }
}

-(void)beginEditing
{
    if (!_editing) {
        _editing = YES;
//        [self _wobble];
        
        for (int i=0; i<[_btns count]; i++) {
            ZSTLauncherButtonView *btn = [_btns objectAtIndex:i];
            [btn beginEditingAnimated:YES];
        }
    }
}

//停止编辑
-(void)endEditing
{
    if (_editing) {
        _editing = NO;
        for (int i=0; i<[_btns count]; i++) {
            [[_btns objectAtIndex:i] endEditingAnimated:YES];
        }
    }
}

- (void)reloadData
{
    for (int i=0; i<[_btns count]; i++) {
        ZSTLauncherButtonView *btn = [_btns objectAtIndex:i];
        [btn removeFromSuperview];
    }
    
    [_btns release];
    _btns = [[NSMutableArray alloc] init];
    
    _scrollView.pagingEnabled = _isHorizontal;//按页显示
    _scrollView.showsHorizontalScrollIndicator = NO;//水平指示针
    _scrollView.showsVerticalScrollIndicator = !_isHorizontal;//垂直指示针
    
    NSUInteger cout = 0;
    if ([_dataSource respondsToSelector:@selector(numberOfItemsForLauncherView:)]) {
        cout = [_dataSource numberOfItemsForLauncherView:self];
    }
    
    // 计算PageCount
    NSUInteger pageCount = cout / (_columnCount * _rowCount);
    
    // 如果不是边界，则增加1
    if (cout % (_columnCount * _rowCount)) 
    {
        pageCount++;
    }
    
    float horizontalSpacing;
    float verticalSpacing;
    if (_isHorizontal  && !isPad) {//iphone 横排
        horizontalSpacing = (_scrollView.frame.size.width - 80*_columnCount - 20) / (_columnCount + 1);
        verticalSpacing = (_scrollView.frame.size.height  - 105*_rowCount) / (_rowCount + 1);
        _scrollView.contentSize = CGSizeMake(pageCount *_scrollView.frame.size.width, _scrollView.frame.size.height);

    }else if (!_isHorizontal && !isPad){//iphone 竖排
        
        horizontalSpacing = (self.frame.size.width - 120*_columnCount) / (_columnCount + 1);
        verticalSpacing = 2;
        _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, (verticalSpacing)+(verticalSpacing + 120) * (cout/_columnCount + cout%_columnCount));
    }

    for (NSUInteger i = 0 ; i < cout; i ++)//现在很希望先排列行，在排列列
    {
        NSUInteger currentPage = i / (_columnCount * _rowCount);
        
        NSUInteger row = i / _columnCount;
        row = row % _rowCount;
        
        NSUInteger col = i % _columnCount;
        float x;
        float y;
        ZSTLauncherButtonView *buttonView = nil;

        if (_isHorizontal && !isPad) {//iphone 横排
            x = 320 * currentPage + (horizontalSpacing + 10)+(80 + horizontalSpacing)*col;
            y = (verticalSpacing) + (verticalSpacing + 105) * row;
            buttonView = [[[ZSTLauncherButtonView alloc] initWithFrame:CGRectMake(x, y, 80, 80)] autorelease];
            
        }else if (!_isHorizontal && !isPad){//iphone 竖排
            x = (horizontalSpacing)+(120 + horizontalSpacing)*col;
            row = i / _columnCount;
            y = (verticalSpacing)+ (verticalSpacing + 120) * row;
            
            buttonView = [[[ZSTLauncherButtonView alloc] initWithFrame:CGRectMake(x, y, 120, 120)] autorelease];

        }

        buttonView.isHorizontal = _isHorizontal;
        buttonView.largeIconVertical = _largeIconVertical;
        buttonView.midIconVertical = _midIconVertical;
        buttonView.delegate = self;
        buttonView.tag= i;
        buttonView.callbackOnSetImage = self;
        buttonView.backgroundColor = [UIColor clearColor];
        
        if (_isHorizontal && !isPad) {//iphone 横排
            buttonView.adorn = [[UIImage imageNamed:@"activity-photo-frame.png"] stretchableImageWithLeftCapWidth:4 topCapHeight:6];            
        }else if (!_isHorizontal && !isPad){//iphone 竖排

            buttonView.adorn = [[UIImage imageNamed:@"btn_busniess_default.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];     
        }

        buttonView.imageInset = UIEdgeInsetsMake(2.5, 2.5, 3, 2.5);
        ZSTLaucherItem *item = [_dataSource launcherView:self itemForIndex:i];
        
        
        [buttonView setIcon:item.icon];
        [buttonView setMessageIntroduce:item.title];
        [buttonView setBadgeNumber:item.badgeNumber];
        [buttonView setEnable:item.enable animated:NO];
        buttonView.canDelete = item.canDelete;
        if (_editing) {
            [buttonView beginEditingAnimated:NO];
        }
        
        [_scrollView addSubview:buttonView];
        
        [_btns addObject:buttonView];//把按钮加到按钮数组中
    }
    
    if (!_pageControl && _isHorizontal) {
        _pageControl = [[UIPageControl alloc] initWithFrame: CGRectMake(0, self.frame.size.height - 22, self.frame.size.width, 20)];
        _pageControl.hidden = YES;
        [_pageControl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
        [self addSubview: _pageControl];
    }
    _pageControl.numberOfPages = pageCount;
    if (pageCount != 1) {
        _pageControl.hidden = NO;
    }
    
}

#pragma mark - HJManagedImageVDelegate

-(void) managedImageSet:(HJManagedImageV*)mi
{
    mi.defaultImage = nil;
    
}

#pragma mark - View lifecycle



#pragma mark----------------------------ELButtonViewDelegate----------------------------------

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
#pragma mark -
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _pageControl.currentPage = scrollView.contentOffset.x / scrollView.bounds.size.width;//是页面控制器的按钮根据页数改变显示
}

//当页数变化时，改变scrollView的内容大小
- (void) pageTurn: (UIPageControl *) aPageControl
{
	NSInteger whichPage = aPageControl.currentPage;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.0f];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	_scrollView.contentOffset = CGPointMake(320.0f * whichPage, 0.0f);
	[UIView commitAnimations];
}

@end
