//
//  ZSTAccountManagementVC.m
//  F3
//
//  Created by P&M on 15/8/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTAccountManagementVC.h"
#import "ZSTUtils.h"
#import "ZSTMineViewController.h"
#import "ZSTSettingCell.h"
#import "ZSTF3RegisterViewController.h"
#import "ZSTThirdAccountCell.h"
#import "ZSTAMBindingMobileVC.h"
#import "ZSTAMSettingPassVC.h"
#import "ZSTF3ClientAppDelegate.h"

#define  kZSTSettingItemType_Password               @"Password"
#define  kZSTSettingItemType_MainAccount            @"Register"

@interface ZSTAccountManagementVC () <TencentSessionDelegate>

@property (strong,nonatomic) TencentOAuth *tencentOAuth;
@property (strong,nonatomic) NSArray *permissions;

@end

@implementation ZSTAccountManagementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector(popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"完善帐号资料", nil)];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[ZSTThirdAccountCell class] forCellReuseIdentifier:@"thirdCell"];
    [self.view addSubview:self.tableView];
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    [self getThirdLoginAuthorizeData];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    
    [self refreshArray];
    thirdAccount = [NSMutableArray arrayWithObjects:@"绑定QQ",@"绑定微信", @"绑定新浪", nil];
    
    [self.tableView reloadData];
}

- (void)refreshArray
{
    // 如果使用手机号码注册了帐号，就显示修改密码和更换手机号，否则显示完善帐号资料
    if ([[ZSTF3Preferences shared].loginMsisdn hasPrefix:@"1"]) {
        accountItems = [NSMutableArray arrayWithObjects:@"修改密码",@"更换手机号", nil];
    }
    else {
        accountItems = [NSMutableArray arrayWithObjects:@"完善帐号资料", nil];
    }
}

#pragma mark - tableView dataSource delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([ZSTF3Preferences shared].wxKey.length != 0 || [ZSTF3Preferences shared].qqKey.length != 0 || [ZSTF3Preferences shared].weiboKey.length != 0) {
        return 2;
    }
    else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return accountItems.count;
    }
    else if (section == 1) {
        return thirdAccount.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0)
    {
        if ([ZSTF3Preferences shared].qqKey.length == 0)
        {
            return 0;
        }
        else
        {
            return 50.0f;
        }
    }
    
    if (indexPath.section == 1 && indexPath.row == 1)
    {
        if ([ZSTF3Preferences shared].wxKey.length == 0)
        {
            return 0;
        }
        else
        {
            return 50.0f;
        }
    }
    
    if (indexPath.section == 1 && indexPath.row == 2)
    {
        if ([ZSTF3Preferences shared].weiboKey.length == 0)
        {
            return 0;
        }
        else
        {
            return 50.0f;
        }
    }
    
    
    return 50.f;
}

// 别忘了设置高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 35.0f;
    }
    else if (section == 1) {
        if ([ZSTF3Preferences shared].wxKey.length == 0 && [ZSTF3Preferences shared].qqKey.length == 0 && [ZSTF3Preferences shared].weiboKey.length == 0) {
            return 0;
        }
        else
        {
            return 17.0f;
        }
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        NSString *titleName = @"登录帐号";
        return titleName;
    }
    else if (section == 1) {
        NSString *titleName = @"绑定帐号";
        if ([ZSTF3Preferences shared].wxKey.length == 0 && [ZSTF3Preferences shared].qqKey.length == 0 && [ZSTF3Preferences shared].weiboKey.length == 0)
        {
            titleName = @"";
        }
        return titleName;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTThirdAccountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"thirdCell"];
    
    if (indexPath.section == 0) {
        [cell createThirdAccountUI];
        
        if (indexPath.row == 0) {
            [cell setThirdAccountTitle:accountItems[indexPath.row] state:nil];
        }
        else if (indexPath.row == 1) {
            [cell setThirdAccountTitle:accountItems[indexPath.row] state:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        return cell;
    }
    else if (indexPath.section == 1) {
        
        [cell createThirdAccountUI];
        
        
        if (indexPath.row == 0) {
            if (![ZSTF3Preferences shared].qqKey || [[ZSTF3Preferences shared].qqKey isEqualToString:@""]) {
                cell.titleLab.hidden = YES;
                cell.stateLab.hidden = YES;
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            else
            {
                cell.titleLab.hidden = NO;
                cell.stateLab.hidden = NO;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell setThirdAccountTitle:thirdAccount[indexPath.row] state:[ZSTF3Preferences shared].qqOpenId.length == 0 ? @"未绑定" : @"已绑定"];
            }
        }
        else if (indexPath.row == 1) {
            if (![ZSTF3Preferences shared].wxKey || [[ZSTF3Preferences shared].wxKey isEqualToString:@""]) {
                cell.titleLab.hidden = YES;
                cell.stateLab.hidden = YES;
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            else
            {
                cell.titleLab.hidden = NO;
                cell.stateLab.hidden = NO;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell setThirdAccountTitle:thirdAccount[indexPath.row] state:[ZSTF3Preferences shared].weixinOpenId.length == 0 ? @"未绑定" : @"已绑定"];
            }
        }
        else if (indexPath.row == 2) {
            if (![ZSTF3Preferences shared].weiboKey || [[ZSTF3Preferences shared].weiboKey isEqualToString:@""]) {
                cell.titleLab.hidden = YES;
                cell.stateLab.hidden = YES;
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            else
            {
                cell.titleLab.hidden = NO;
                cell.stateLab.hidden = NO;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell setThirdAccountTitle:thirdAccount[indexPath.row] state:[ZSTF3Preferences shared].sinaOpenId.length == 0 ? @"未绑定" : @"已绑定"];
            }
        }
        
        return cell;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            
            if ([[ZSTF3Preferences shared].loginMsisdn hasPrefix:@"1"]) {
                
                ZSTF3RegisterViewController *controller = [[ZSTF3RegisterViewController alloc] init];
                controller.type = 4;
                controller.phoneNum = [ZSTF3Preferences shared].loginMsisdn;
                [self.navigationController pushViewController:controller animated:YES];
            }
            else {
                // 跳转到绑定帐号
                ZSTAMBindingMobileVC *bindingMobileVC = [[ZSTAMBindingMobileVC alloc] init];
                [self.navigationController pushViewController:bindingMobileVC animated:YES];
            }
            
        } else if (indexPath.row == 1) {
            
            ZSTF3RegisterViewController *controller = [[ZSTF3RegisterViewController alloc] init];
            controller.type = 3;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
    else {
        ZSTF3ClientAppDelegate *app = (ZSTF3ClientAppDelegate *)[UIApplication sharedApplication].delegate;
        if (indexPath.row == 0)
        {
            _tencentOAuth = [[TencentOAuth alloc] initWithAppId:[ZSTF3Preferences shared].qqKey andDelegate:self];
            _tencentOAuth.redirectURI = @"www.qq.com";
            _permissions =  [NSArray arrayWithObjects:@"get_user_info", @"get_simple_userinfo", @"add_t", nil];
            
            // 向服务器发出认证请求
            [_tencentOAuth authorize:_permissions inSafari:NO];
        }
        else if (indexPath.row == 1)
        {
            if ([WXApi isWXAppInstalled] || [WXApi isWXAppSupportApi])
            {
                [app sendAuthRequest];
            }
            else
            {
                NSLog(@"还没有装微信");
            }
        }
        else
        {
            WBAuthorizeRequest *request = [WBAuthorizeRequest request];
            request.redirectURI = [ZSTF3Preferences shared].weiboRedirect;//@"http://www.sina.com"
            request.scope = @"all";
            request.userInfo = @{@"SSO_From": @"ZSTAccountManagementVC",
                                 @"Other_Info_1": [NSNumber numberWithInt:123],
                                 @"Other_Info_2": @[@"obj1", @"obj2"],
                                 @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
            [WeiboSDK sendRequest:request];
        }
    }
}

- (void)getThirdLoginAuthorizeData
{
    NSString *ecid = [ZSTF3Preferences shared].ECECCID;
    [self.engine getThirdLoginAuthorizeEcid:ecid];
}

- (void)loginAuthorizeDidSucceed:(NSDictionary *)response
{
    NSLog(@"第三方授权登录成功");
    
    // 如果status的值等于1有第三方授权登录，否侧没有
    if ([[response objectForKey:@"status"] integerValue] == 1) {
        NSDictionary *dict = [response objectForKey:@"data"];
        
        // QQ
        NSString *qqKey = [dict safeObjectForKey:@"qqappkey"];
        //        NSString *qqKey = @"";
        NSString *qqSecret = [dict safeObjectForKey:@"qqsecret"];
        
        // 微信
        NSString *wxKey = [dict safeObjectForKey:@"wechatappkey"];
        NSString *wxSecret = [dict safeObjectForKey:@"wechatsecret"];
        
        // 新浪微博
        NSString *wbKey = [dict safeObjectForKey:@"weiboappkey"];
        NSString *wbSecret = [dict safeObjectForKey:@"weibosecret"];
        NSString *wbRedirect = [dict safeObjectForKey:@"weiboredirect"];
        
        
        [ZSTF3Preferences shared].qqKey = qqKey;
        [ZSTF3Preferences shared].qqSecret = qqSecret;
        [ZSTF3Preferences shared].wxKey = wxKey;
        [ZSTF3Preferences shared].wxSecret = wxSecret;
        [ZSTF3Preferences shared].weiboKey = wbKey;
        [ZSTF3Preferences shared].weiboSecret = wbSecret;
        [ZSTF3Preferences shared].weiboRedirect = wbRedirect;
    }
}

- (void)loginAuthorizeDidFailed:(NSString *)response
{
    NSLog(@"123");
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
