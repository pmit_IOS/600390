//
//  ZSTAMBindingMobileVC.m
//  F3
//
//  Created by P&M on 15/8/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTAMBindingMobileVC.h"
#import "ZSTUtils.h"
#import "ZSTRegisterGetCapViewController.h"

@interface ZSTAMBindingMobileVC ()

@end

@implementation ZSTAMBindingMobileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector(popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"完善帐号资料", nil)];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    [self createFirstViewUI];
    [self createSecondViewUI];
}

- (void)createFirstViewUI
{
    UIView *firstView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 100)];
    firstView.backgroundColor = [UIColor clearColor];
    self.firstView = firstView;
    [self.view addSubview:firstView];
    
    // 分隔线
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = CGRectMake(40, 50, firstView.frame.size.width - 80, 0.5);
    layer.backgroundColor = RGBACOLOR(220, 220, 220, 1).CGColor;
    [firstView.layer addSublayer:layer];
    
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(40, 100 - 0.5, firstView.frame.size.width - 80, 0.5);
    layer1.backgroundColor = RGBACOLOR(220, 220, 220, 1).CGColor;
    [firstView.layer addSublayer:layer1];
    
    // 用户图片
    UIImageView *userIma = [[UIImageView alloc] initWithFrame:CGRectMake(50, 10, 20, 30)];
    userIma.image = ZSTModuleImage(@"user.png");
    userIma.contentMode = UIViewContentModeScaleAspectFit;
    [firstView addSubview:userIma];
    
    UIImageView *lockIma = [[UIImageView alloc] initWithFrame:CGRectMake(50, 60, 20, 30)];
    lockIma.image = ZSTModuleImage(@"lock.png");
    lockIma.contentMode = UIViewContentModeScaleAspectFit;
    [firstView addSubview:lockIma];
    
    // 手机号输入框
    UITextField *mobileTF = [[UITextField alloc] initWithFrame:CGRectMake(80, 15, firstView.frame.size.width - 140, 30)];
    mobileTF.delegate = self;
    mobileTF.borderStyle = UITextBorderStyleNone;
    mobileTF.tag = 1;
    mobileTF.textColor = RGBACOLOR(100, 100, 100, 1);
    mobileTF.textAlignment = NSTextAlignmentLeft;
    mobileTF.font = [UIFont systemFontOfSize:14.0f];
    mobileTF.placeholder = @"请输入手机号";
    mobileTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    mobileTF.returnKeyType = UIReturnKeyDone;
    mobileTF.keyboardType = UIKeyboardTypeNumberPad;
    self.mobileTF = mobileTF;
    [firstView addSubview:mobileTF];
    
    // 密码输入框
    UITextField *passTF = [[UITextField alloc] initWithFrame:CGRectMake(80, 65, firstView.frame.size.width - 140, 30)];
    passTF.delegate = self;
    passTF.borderStyle = UITextBorderStyleNone;
    passTF.secureTextEntry = YES;
    passTF.tag = 2;
    passTF.textColor = RGBACOLOR(100, 100, 100, 1);
    passTF.textAlignment = NSTextAlignmentLeft;
    passTF.font = [UIFont systemFontOfSize:14.0f];
    passTF.placeholder = @"请输入密码";
    passTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    passTF.returnKeyType = UIReturnKeyDone;
    passTF.keyboardType = UIKeyboardTypeDefault;
    self.passTF = passTF;
    [firstView addSubview:passTF];
}

- (void)createSecondViewUI
{
    UIView *secondView = [[UIView alloc] initWithFrame:CGRectMake(0, self.firstView.origin.y + self.firstView.frame.size.height + 40, self.view.frame.size.width, 130)];
    secondView.backgroundColor = [UIColor clearColor];
    self.secondView = secondView;
    [self.view addSubview:secondView];
    
    // 创建立即绑定手机号按钮
    UIButton *bindingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    bindingBtn.frame = CGRectMake(40, 0, self.view.frame.size.width - 80, 50);
    [bindingBtn setTitle:@"立即绑定手机号" forState:UIControlStateNormal];
    [bindingBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [bindingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [bindingBtn addTarget:self action:@selector(bindingMobileClick:) forControlEvents:UIControlEventTouchUpInside];
    [bindingBtn setBackgroundColor:[UIColor orangeColor]];
    [secondView addSubview:bindingBtn];
    
    // 没有帐号 view
    UIView *noView = [[UIView alloc] initWithFrame:CGRectMake(40, 80, self.view.frame.size.width - 80, 50)];
    noView.backgroundColor = [UIColor clearColor];
    [noView.layer setBorderWidth:1.0f];
    [noView.layer setBorderColor:RGBACOLOR(220, 220, 220, 1).CGColor];
    [secondView addSubview:noView];
    
    UILabel *noLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 90, 30)];
    noLabel.backgroundColor = [UIColor clearColor];
    noLabel.text = @"没有帐号？";
    noLabel.textColor = RGBACOLOR(200, 200, 200, 1);
    noLabel.textAlignment = NSTextAlignmentRight;
    [noView addSubview:noLabel];
    
    NSString *loginStr = @"手机号快速注册";
    CGSize loginSize = [loginStr sizeWithFont:[UIFont boldSystemFontOfSize:16.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
    
    UILabel *loginLab = [[UILabel alloc] initWithFrame:CGRectMake(noLabel.frame.origin.x + noLabel.frame.size.width, 10, loginSize.width, 30)];
    loginLab.backgroundColor = [UIColor clearColor];
    loginLab.text = loginStr;
    loginLab.textColor = [UIColor blackColor];
    loginLab.textAlignment = NSTextAlignmentLeft;
    loginLab.font = [UIFont boldSystemFontOfSize:16.0f];
    [noView addSubview:loginLab];
    
    // 分隔线
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(noLabel.frame.origin.x + noLabel.frame.size.width, loginLab.frame.origin.y + 26, loginSize.width, 0.5);
    layer3.backgroundColor = [UIColor blackColor].CGColor;
    [noView.layer addSublayer:layer3];
    
    // 手势响应
    UITapGestureRecognizer *loginTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginTapAction:)];
    [loginLab addGestureRecognizer:loginTap];
    loginLab.userInteractionEnabled = YES;
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.mobileTF.tag == 1 && self.mobileTF == textField) {
        NSInteger maxLength = 11;
        NSInteger strLength = textField.text.length - range.length + string.length;
        
        return (strLength <= maxLength);
    }
    if (self.passTF.tag == 2 && self.passTF == textField) {
        NSInteger maxLength = 16;
        NSInteger strLength = textField.text.length - range.length + string.length;
        
        return (strLength <= maxLength);
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (void)bindingMobileClick:(UIButton *)sender
{
    [self.mobileTF resignFirstResponder];
    [self.passTF resignFirstResponder];
    
    NSString *mobileNumberStr = [self.mobileTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *passWordStr = [self.passTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (mobileNumberStr.length == 0) {
        [TKUIUtil alertInWindow:@"亲，手机号码不能为空" withImage:nil];
        return;
    }
    else if(![self validateMobile:mobileNumberStr]) {
        
        UIAlertView* alertView = [[UIAlertView alloc]
                                  initWithTitle:nil
                                  message:@"您输入的手机号格式有误，请重试"
                                  delegate:self
                                  cancelButtonTitle:@"确定"
                                  otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    if(passWordStr.length == 0) {
        
        [TKUIUtil alertInWindow:@"亲，密码不能为空" withImage:nil];
        return;
    }
    
    
    [TKUIUtil showHUD:self.view];
    [self.engine bindingMobileWithMsisdn:mobileNumberStr password:passWordStr];
}

- (void)loginTapAction:(UITapGestureRecognizer *)tap
{
    ZSTRegisterGetCapViewController *registerGetCapVC = [[ZSTRegisterGetCapViewController alloc] init];
    [self.navigationController pushViewController:registerGetCapVC animated:YES];
}

#pragma mark ------------------ZSTF3EngineDelegate---------------------

- (void)bindingMobileDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    
    if ([[response safeObjectForKey:@"code"] integerValue] == 1) {
        
        // 页面跳转
        //[self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
        [ZSTF3Preferences shared].loginMsisdn = [[response safeObjectForKey:@"data"] safeObjectForKey:@"Msisdn"];
        [ZSTF3Preferences shared].UserId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"UserId"];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)bindingMobileDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)validateMobile:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,181,189,1700,1709,1705
     */
    NSString * MOBILE = @"^1(3[0-9]|4[0-9]|5[0-35-9]|7[0-9]|8[0125-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349|700|705|709)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)dealloc
{
    [self.engine cancelAllRequest];
}

@end
