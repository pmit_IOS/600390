//
//  ZSTGlobal+Module.h
//  F3Engine
//
//  Created by bin luo on 12-7-27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#ifndef F3Engine_ZSTGlobal_Module_h
#define F3Engine_ZSTGlobal_Module_h

#import "ZSTModuleAddition.h"
#import "ZSTDao.h"


#define SCREEN_BOUNDS [[UIScreen mainScreen] bounds]
#define WIDTH SCREEN_BOUNDS.size.width
#define HEIGHT SCREEN_BOUNDS.size.height

//UI设计是按照iphone6设计的，这个是一个比率
#define UIChangeRate WIDTH/750
#define WidthRate(a) WIDTH/750*a
#define HeightRate(a) HEIGHT/1334*a

#define ZSTPathForModuleBundleResource(relativePath) [[[NSBundle mainBundle] pathForResource:@"Module" ofType:@"bundle"] stringByAppendingPathComponent:relativePath]

#define ZSTModuleImage(relativePath) [UIImage imageNamed:[@"Module.bundle" stringByAppendingPathComponent:relativePath]]

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define IS_IOS_7 ([[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] intValue] >= 7)

#define CORVER_IMG  (iPhone5 ? @"module_covera_default_img.png": @"module_covera_default_ip4_img.png")

#endif

 