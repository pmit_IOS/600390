//
//  ZSTWebViewController.m
//  F3
//
//  Created by 9588 on 3/6/12.
//  Copyright 2012 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTWebViewController.h"
#import "ZSTUtils.h"
#import "ZSTModuleManager.h"
#import "ImageHelper.h"

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)


@implementation ZSTWebViewController

@synthesize delegate = _delegate;
@synthesize webCanGoBack;
@synthesize webCanGoForward;
@synthesize scrollToolBarView = _scrollToolBarView;
@synthesize uploadUrl,dWidth,callBackFunc,resultStr,dHeight,quality,uploadImageName;

/////////////////////////////////////////////////////////////////////////////////////
#pragma mark - helper

- (NSString *)normalizeURL:(NSString *)url
{
    if ([url length] != 0 && [url rangeOfString:@"://"].location == NSNotFound) {
        url = [NSString stringWithFormat:@"http://%@", url];
    }
    return url;
}

/////////////////////////////////////////////////////////////////////////////////////

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////



- (void)dealloc {
    _delegate = nil;
    _webView.delegate = nil;
    [_webView stopLoading];
    TKRELEASE(_webView);
    
    [_scrollToolBarView stopClock];
    [_scrollToolBarView release];
    
    [super dealloc];
}

#pragma mark ------------- button selector------------------------------
-(void)home
{
    //    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_url]]]; 
    [self setURL:_url];
    [_scrollToolBarView reclock];
}

-(void)refresh
{
    [_webView reload];
    [_scrollToolBarView reclock];
}
-(void)goBack
{
    [_webView goBack];
    [_scrollToolBarView reclock];
    
}
-(void)goForward
{
    [_webView goForward];
    [_scrollToolBarView reclock];
}


- (void)dismiss
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIView *)titleViewWithTitle:(NSString *)title
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(62, 0, 200, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = title;
    label.font = [UIFont boldSystemFontOfSize:20];
    label.lineBreakMode = NSLineBreakByTruncatingMiddle;
    label.textColor = [ZSTUtils getNavigationTextColor];
    return[label autorelease];
}

- (void)setURL:(NSString *)url
{
    url = [self normalizeURL:url];
    [url retain];
    [_url release];
    _url = url;
    
    if (_webView) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        [_webView loadRequest:request];
    }
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    _webView = [[JSBridgeWebView alloc] initWithFrame:self.view.bounds];
    _webView.scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _webView.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view addSubview:_webView];
    if (_url) {
        [self setURL:_url];
    }
    
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"浏览器", nil) target:self selector:@selector (openMore)];
    
//    UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    PMRepairButton *homeButton = [[PMRepairButton alloc] init];
    [homeButton setImage:[UIImage imageNamed:@"btn_webicon_home.png"] forState:UIControlStateNormal];
    [homeButton setImage:[UIImage imageNamed:@"btn_webicon_home_light.png"] forState:UIControlStateHighlighted];
    [homeButton addTarget:self action:@selector(home) forControlEvents:UIControlEventTouchUpInside];
    
//    UIButton *refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    PMRepairButton *refreshButton = [[PMRepairButton alloc] init];
    [refreshButton setImage:[UIImage imageNamed:@"btn_webicon_reload.png"]  forState:UIControlStateNormal];
    [refreshButton setImage:[UIImage imageNamed:@"btn_webicon_reload_light.png"] forState:UIControlStateHighlighted];
    [refreshButton addTarget:self action:@selector(refresh) forControlEvents:UIControlEventTouchUpInside];
    
//    _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _backButton = [[PMRepairButton alloc] init];
    [_backButton setImage:[UIImage imageNamed:@"btn_webicon_backward.png"]  forState:UIControlStateNormal];
    [_backButton setImage:[UIImage imageNamed:@"btn_webicon_backward_light.png"] forState:UIControlStateHighlighted];
    [_backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
//    _forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _forwardButton = [[PMRepairButton alloc] init];
    [_forwardButton setImage:[UIImage imageNamed:@"btn_webicon_forward.png"] forState:UIControlStateNormal];
    [_forwardButton setImage:[UIImage imageNamed:@"btn_webicon_forward_light.png"] forState:UIControlStateHighlighted];
    [_forwardButton addTarget:self action:@selector(goForward) forControlEvents:UIControlEventTouchUpInside];
    
    _scrollToolBarView = [[ZSTScrollToolBarView alloc] initWithFrame:CGRectMake(1+290, self.view.frame.size.height - 41, 325, 40) color:[UIColor blackColor] alpha:0.6];
    _scrollToolBarView.items = [NSArray arrayWithObjects:homeButton,refreshButton,_backButton,_forwardButton, nil];
    _scrollToolBarView.imageItems = [NSArray arrayWithObjects:@"btn_webicon_home.png",@"btn_webicon_reload.png",@"btn_webicon_backward.png",@"btn_webicon_forward.png",nil];
    _scrollToolBarView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _scrollToolBarView.delegate = self;
    [self.view addSubview:_scrollToolBarView];
    _scrollToolBarView.hidden = YES;
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"%@_%@",@"FIRST_LAUNCH_CUSTOMLINKS",@(self.type)]]) {
    
        [self showGuideView];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [TKUIUtil hiddenHUD];
}


- (void) showGuideView
{
    UIView *guideView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 480+(iPhone5?88:0))];
    guideView.backgroundColor = [UIColor blackColor];
    guideView.tag = 9999999;
    guideView.alpha = 0.7f;
    
    UILabel *signlal = [[UILabel alloc] init];
    signlal.backgroundColor = [UIColor clearColor];
    signlal.font = [UIFont systemFontOfSize:16];
    signlal.textColor = [UIColor whiteColor];
    signlal.text = @"如页面加载异常请在浏览器打开";
    signlal.numberOfLines = 0;
    CGSize size = [signlal.text sizeWithFont:signlal.font constrainedToSize:CGSizeMake(120, 1000) lineBreakMode:NSLineBreakByWordWrapping];
    signlal.frame = CGRectMake(120, 30, 120, size.height);
    [guideView addSubview:signlal];
    [signlal release];
    
    UIImageView *signImgView = [[UIImageView alloc] initWithFrame:CGRectMake(240, 2, 40, 54)];
    signImgView.image = ZSTModuleImage(@"module_web_tips.png");
    [guideView addSubview:signImgView];
    [signImgView release];
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:[NSString stringWithFormat:@"%@_%@",@"FIRST_LAUNCH_CUSTOMLINKS",@(self.type)]];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(removeGuideView)];
    [guideView addGestureRecognizer:singleTap];
    
    [self.view addSubview:guideView];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) removeGuideView
{
    [[[UIApplication sharedApplication].keyWindow viewWithTag:9999999] removeFromSuperview];
}

- (void) openMore
{
    if ([_url rangeOfString:[NSString stringWithFormat:@"&clientType=%@",[ZSTF3Preferences shared].platform]].location != NSNotFound) {
        
        _url = [_url stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"&clientType=%@",[ZSTF3Preferences shared].platform] withString:@""];
    }

    if ([_url rangeOfString:[NSString stringWithFormat:@"&msisdn=%@",[ZSTF3Preferences shared].loginMsisdn]].location != NSNotFound) {
        
        _url = [_url stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"msisdn=%@",[ZSTF3Preferences shared].loginMsisdn] withString:@"platformType=3"];
    }
    
    
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:_url]];
    [self removeGuideView];
}


- (void)viewDidUnload {
    [super viewDidUnload];
    
    [TKUIUtil hiddenHUD];
    
    _delegate = nil;
    _webView.delegate = nil;
    
    TKRELEASE(_webView);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              /////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIWebViewDelegate


///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request
 navigationType:(UIWebViewNavigationType)navigationType {
    if ([_delegate respondsToSelector:
         @selector(webController:webView:shouldStartLoadWithRequest:navigationType:)] &&
        ![_delegate webController:self webView:webView
       shouldStartLoadWithRequest:request navigationType:navigationType]) {
            return NO;
        }
    
    [_loadingURL release];
    _loadingURL = [request.URL retain];
    
    
    if (![[_loadingURL description] isEqualToString:_url]) {
        _scrollToolBarView.hidden = NO;
    }
    if (navigationType == UIWebViewNavigationTypeLinkClicked && [request.URL.scheme caseInsensitiveCompare:@"native"] == NSOrderedSame) {
        
        NSDictionary *params = [request.URL.absoluteString queryDictionaryUsingEncoding:NSUTF8StringEncoding];
        NSString *module_id = [params safeObjectForKey:@"native://?module_id"];
        NSString *module_type = [params safeObjectForKey:@"module_type"];
        NSString *title = [params safeObjectForKey:@"title"];
        
        NSMutableDictionary *moduleParams = [NSMutableDictionary dictionary];
        [moduleParams setSafeObject:module_type forKey:@"type"];
        
        ZSTModule *module = [[ZSTModuleManager shared] findModule:[module_id integerValue]];
        if (module) {
            UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:[module_id integerValue] withOptions:moduleParams];
            if (controller) {
                controller.hidesBottomBarWhenPushed = YES;
                controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:title];
                controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
                [self.navigationController pushViewController:controller animated:YES];      
            }
        }
        
    }
    
    return YES;
}
-(void)openGallery
{
    //初始化类
    pickerLibrary = [[UIImagePickerController alloc] init];
    //指定几总图片来源
    //UIImagePickerControllerSourceTypePhotoLibrary：表示显示所有的照片。
    //UIImagePickerControllerSourceTypeCamera：表示从摄像头选取照片。
    //UIImagePickerControllerSourceTypeSavedPhotosAlbum：表示仅仅从相册中选取照片。
    pickerLibrary.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //表示用户可编辑图片。
    pickerLibrary.allowsEditing = YES;
    //代理
    pickerLibrary.delegate = self;
    [self presentViewController:pickerLibrary animated:YES completion:nil];
}

- (void)webView:(UIWebView*) webview didReceiveJSNotificationWithDictionary:(NSDictionary*) dictionary
{
    if([[dictionary objectForKey:@"func"] isEqualToString:@"uploadLocalFile"])
    {
        self.uploadUrl = [[dictionary safeObjectForKey:@"content"] safeObjectForKey:@"upload_url"];
        self.dWidth = [[[dictionary safeObjectForKey:@"content"] safeObjectForKey:@"width"] floatValue];
        self.dHeight = [[[dictionary safeObjectForKey:@"content"] safeObjectForKey:@"height"] floatValue];
        self.quality = [[[dictionary safeObjectForKey:@"content"] safeObjectForKey:@"rate"] floatValue];
        if(self.dWidth == 0) {
            self.dWidth = 320.0f;
        }
        self.callBackFunc=[dictionary safeObjectForKey:@"callback"];
        NSLog(@"pick photoes!");
        [self openGallery];
    }
}

//上传图片方法
- (void) imageUpload:(UIImage *) image1
{
    [TKUIUtil showHUD:self.view withText:@"正在上传..."];
    //把图片转换成imageDate格式
    CGSize newSize;
    
    if (self.dWidth != 0 && self.dHeight != 0) {
        newSize= CGSizeMake(self.dWidth, self.dHeight);
    }else if (self.dWidth != 0) {
        CGFloat cWidth=self.dWidth;
        newSize= CGSizeMake(cWidth, image1.size.height*(cWidth/image1.size.width));
    }else if (self.dHeight != 0)
    {
        CGFloat cHeight=self.dHeight;
        newSize= CGSizeMake(image1.size.width*(cHeight/image1.size.height), cHeight);
    }

    UIImage *image=[ImageHelper image:image1 fitInSize:newSize];
    
    NSData *imageData = UIImageJPEGRepresentation(image, (self.quality == 0 ? 1.0 : self.quality));
    //传送路径
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@?token=&file_name=%@",self.uploadUrl,self.uploadImageName]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:60];
    
    //请求方式
    [request setHTTPMethod:@"POST"];
    //一连串上传头标签
    NSString *boundary = @"----WebKitFormBoundarycC4YiaUFwM44F6rT";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"attachment[file]\"; filename=\"%@\"\r\n",self.uploadImageName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    //上传文件开始
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    //获得返回值
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    self.resultStr = returnString;
    [TKUIUtil hiddenHUD];

    if (self.resultStr.length>0){
        NSString *str=[[self.callBackFunc stringByAppendingFormat:@"('%@')",self.resultStr] stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""] ;
        [_webView stringByEvaluatingJavaScriptFromString:str];
    }
}


//3.x  用户选中图片后的回调
- (void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info
{
    NSLog(@"3.x");
    //获得编辑过的图片
    UIImage* image = [info safeObjectForKey: @"UIImagePickerControllerEditedImage"];
    NSURL *imageFileURL = (NSURL *)[info safeObjectForKey: @"UIImagePickerControllerReferenceURL"];
    self.uploadImageName = [imageFileURL lastPathComponent];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self imageUpload:image];
    
}

//2.x  用户选中图片之后的回调
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    NSLog(@"2.x");
    NSMutableDictionary * dict= [NSMutableDictionary dictionaryWithDictionary:editingInfo];
    
    [dict setObject:image forKey:@"UIImagePickerControllerEditedImage"];
    
    //直接调用3.x的处理函数
    [self imagePickerController:picker didFinishPickingMediaWithInfo:dict];
}

// 用户选择取消
- (void) imagePickerControllerDidCancel: (UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)webViewDidStartLoad:(UIWebView*)webView {
    
    [TKUIUtil showHUD:_webView];
    
    _backButton.enabled = [webView canGoBack];
    _forwardButton.enabled = [webView canGoForward];
    
    webCanGoBack = [webView canGoBack];
    webCanGoForward = [webView canGoForward];
    
    if ([_delegate respondsToSelector:@selector(webController:webViewDidStartLoad:)]) {
        [_delegate webController:self webViewDidStartLoad:webView];
    }
    
    self.navigationItem.titleView = [self titleViewWithTitle:NSLocalizedString(@"加载中...", @"")];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)webViewDidFinishLoad:(UIWebView*)webView {
    _backButton.enabled = [webView canGoBack];
    _forwardButton.enabled = [webView canGoForward];
    
    webCanGoBack = [webView canGoBack];
    webCanGoForward = [webView canGoForward];
    
    if ([_delegate respondsToSelector:@selector(webController:webViewDidFinishLoad:)]) {
        [_delegate webController:self webViewDidFinishLoad:webView];
    }
    
    TKRELEASE(_loadingURL);
    NSString *title = @"";
    if ([self.titleString isEqualToString:@""] || !self.titleString)
    {
        title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    }
    else
    {
        title = self.titleString;
    }
    
    self.navigationItem.titleView = [self titleViewWithTitle:title];
    [TKUIUtil hiddenHUD];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
    
    _backButton.enabled = [_webView canGoBack];
    _forwardButton.enabled = [_webView canGoForward];
    
    webCanGoBack = [webView canGoBack];
    webCanGoForward = [webView canGoForward];
    
    self.navigationItem.titleView = [self titleViewWithTitle:NSLocalizedString(@"加载失败", nil)];
    
    if ([_delegate respondsToSelector:@selector(webController:webView:didFailLoadWithError:)]) {
        [_delegate webController:self webView:webView didFailLoadWithError:error];
    }
    
    TKRELEASE(_loadingURL);
        
    // 102 == WebKitErrorFrameLoadInterruptedByPolicyChange
    if ((([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) ||
          ([error.domain isEqualToString:@"WebKitErrorDomain"] && error.code == 102))) {
        [TKUIUtil hiddenHUD];
    }else{
        [TKUIUtil showHUDInView:_webView withText:NSLocalizedString(@"网络错误", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
        [TKUIUtil hiddenHUDAfterDelay:2];
    }
}


#pragma mark ----------ScrollToolBarViewDelegate-----------


-(void)clickedPushOut
{
    [UIView beginAnimations:@"ScrollToolBarViewPushOut" context:nil];
    [UIView setAnimationDuration:0.6];
    
    CGRect scrollToolBarViewFrame = _scrollToolBarView.frame;
    scrollToolBarViewFrame.origin.x -=  290;
    _scrollToolBarView.frame = scrollToolBarViewFrame;
    
    [UIView commitAnimations];
    
    _backButton.enabled = webCanGoBack;
    _forwardButton.enabled = webCanGoForward;
}

-(void)clickedPushIn
{
    [UIView beginAnimations:@"ScrollToolBarViewPushIn" context:nil];
    [UIView setAnimationDuration:0.6];
    
    CGRect scrollToolBarViewFrame = _scrollToolBarView.frame;
    scrollToolBarViewFrame.origin.x +=  290;
    _scrollToolBarView.frame = scrollToolBarViewFrame;
    
    [UIView commitAnimations];
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue* value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [value CGRectValue];
    self.scrollPoint = _webView.scrollView.contentOffset;
    _webView.scrollView.contentOffset = CGPointMake(0, _webView.scrollView.contentOffset.y + 44);
}

- (void)keyboardDidHide:(NSNotification *)notification
{
    _webView.scrollView.contentOffset = self.scrollPoint;
}

@end
