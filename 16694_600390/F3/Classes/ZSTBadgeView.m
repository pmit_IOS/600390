//
//  ELBudgetImageView.m
//  Test
//
//  Created by  on 11-7-1.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "ZSTBadgeView.h"

#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@implementation ZSTBadgeView  

-(id) initWithImage:(UIImage *)image
{
    UIImage *image1 = [image stretchableImageWithLeftCapWidth: 12 topCapHeight:12];
    self = [super initWithImage: image1];
    
    if (self) 
    {
        _badgeLabel = [[UILabel alloc] initWithFrame: CGRectZero];
        _badgeLabel.text = @"0";
        _badgeLabel.textAlignment = NSTextAlignmentCenter;
        _badgeLabel.textColor = [UIColor whiteColor];
        _badgeLabel.font = [UIFont boldSystemFontOfSize:15];
        _badgeLabel.backgroundColor = [UIColor clearColor];
        [self addSubview: _badgeLabel];
        
        [_badgeLabel sizeToFit];
        
        _badgeLabel.center = CGPointMake(self.frame.size.width / 2-0.3, self.frame.size.height / 2-1);
        
    }
    return self;
}

-(NSString *) text
{
    return _badgeLabel.text;
}

-(void) setText: (NSString *) text
{
    _badgeLabel.text = text;
    [_badgeLabel sizeToFit];
    
    CGFloat badgeLabelWidth = _badgeLabel.frame.size.width;
    CGPoint selfCenter = self.center;
    
    if ([_badgeLabel.text length] == 1) {
        selfCenter.x = 66;
    }else if ([_badgeLabel.text length] == 2){
        selfCenter.x = 61;
    }else if ([_badgeLabel.text length] == 3){
        selfCenter.x = 56;
    }else if ([_badgeLabel.text length] == 4){
        selfCenter.x = 52;
    }else if ([_badgeLabel.text length] >= 5)
    {
        _badgeLabel.text = @"••••";
        selfCenter.x = 56;
        badgeLabelWidth = _badgeLabel.frame.size.width*0.6;
        _badgeLabel.center = CGPointMake(self.frame.size.width / 2-0.3, self.frame.size.height / 2-1);
    }
    if (!isPad) {
        self.center = selfCenter;
    }
    self.bounds = CGRectMake(0, 0, self.image.size.width+(badgeLabelWidth-9), self.image.size.height);
}

- (void)dealloc
{
    [_badgeLabel release];
    [super dealloc];
}

@end
