//
//  ZSTPersonalClassTableViewCell.m
//  InfantCloud
//
//  Created by LiZhenQu on 14-8-1.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonalAddressTableViewCell.h"

@implementation ZSTPersonalAddressTableViewCell

- (void)awakeFromNib
{
    UIImageView *rightImgView = [[UIImageView alloc] initWithFrame:CGRectMake(285, (self.frame.size.height - 12)/2.0, 7, 12)];
    rightImgView.image = ZSTModuleImage(@"module_setting_icon_right.png");
    [self addSubview:rightImgView];
    
    _cellHeight = 54;
}

- (void)configCell:(NSString *)string
{
    if (!string || string.length == 0) {
        
        string = @"未填写";
    }
    
    
    CGSize withsize = [string sizeWithFont:[UIFont systemFontOfSize:14]];
    
    if (withsize.width <= 150) {
        
        self.contentLabel.textAlignment = NSTextAlignmentRight;
    }
    
    CGSize size = [string sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(180, 60) lineBreakMode:NSLineBreakByCharWrapping];
    self.contentLabel.frame = CGRectMake(90, 16, 180, size.height);
    self.contentLabel.text = string;
    _cellHeight = CGRectGetMaxY(self.contentLabel.frame) + 16;
    
    if (_cellHeight <= 54) {
        
        _cellHeight = 54;
    }
    
    self.divider.frame = CGRectMake(0, _cellHeight - 1, 320, 1);
}

- (CGFloat)cellHeight
{
    return _cellHeight;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
