//
//  ZSTPasswordRemindViewController.h
//  F3
//
//  Created by LiZhenQu on 14/11/29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTPasswordRemindViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIImageView *bgImeView;
@property (nonatomic, retain) IBOutlet UIImageView *avaterImgView;
@property (nonatomic, retain) IBOutlet UILabel *phoneNumLabel;

@property (nonatomic, retain) IBOutlet UIButton *setbtn;
@property (nonatomic, retain) IBOutlet UIButton *laterbtn;

@end
