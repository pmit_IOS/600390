//
//  ZSTCoverAView.h
//  F3
//
//  Created by xuhuijun on 13-1-14.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKAsynImageView.h"

@class ZSTCoverAView;

@protocol ZSTCoverAViewDelegate <NSObject>

- (void)coverAViewDidClicked:(ZSTCoverAView *)coverAView;
- (void)coverAViewDidSlided:(ZSTCoverAView *)coverAView;


@end

@interface ZSTCoverAView : UIView <TKAsynImageViewDelegate,HJManagedImageVDelegate>

{
    NSMutableArray *imagesArray;
    NSMutableArray *imageUrls;
    float timeTransition;
    BOOL isLoop;
    id <ZSTCoverAViewDelegate> delegate;
}

@property (nonatomic, assign) float timeTransition;
@property (nonatomic, retain) NSMutableArray *imagesArray;
@property (nonatomic, retain) NSMutableArray *imageUrls;


@property (nonatomic) BOOL isLoop;
@property (nonatomic,assign) id<ZSTCoverAViewDelegate> delegate;

- (void) animateWithURLs:(NSArray *)urls transitionDuration:(float)duration loop:(BOOL)shouldLoop;
- (void) coverAShow;
@end
