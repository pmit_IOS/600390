//
//  ZSTResponse.m
//  
//
//  Created by luobin on 4/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTResponse.h"
#import "ASIHTTPRequest.h"

#define kTKRequestUserInfo @"kTKRequestUserInfo"

@implementation ZSTResponse
@synthesize userInfo;
@synthesize errorMsg;
@synthesize stringResponse;
@synthesize jsonResponse;
@synthesize request;
@synthesize resultCode;

- (id)initWithRequest:(ASIHTTPRequest *)theRequest
{
    self = [super init];
    if (self) {
        [self loadInfoFromRequest:theRequest];
    }
    return self;
}

+ (id)responseWithRequest:(ASIHTTPRequest *)request
{
    return [[[ZSTResponse alloc] initWithRequest:request] autorelease];
}

-(void)dealloc
{
    TKRELEASE(request);
    self.userInfo = nil;
    self.errorMsg = nil;
    self.stringResponse = nil;
    self.jsonResponse = nil;
    [super dealloc];
}

@end
