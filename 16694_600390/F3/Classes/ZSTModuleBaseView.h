//
//  ZSTModuleBaseView.h
//  F3
//
//  Created by xuhuijun on 13-1-16.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZSTModule.h"
#import "ZSTModuleDelegate.h"
#import "ZSTDao.h"

@interface ZSTModuleBaseView : UIView <ZSTModuleDelegate>
@property (assign, readonly) UIView *rootView;
@property (assign, readonly) UIViewController *rootViewController;

@end
