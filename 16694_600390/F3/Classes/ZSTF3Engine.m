    //
//  ZSTF3Engine.m
//  F3Engine
//
//  Created by luobin on 4/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTF3Preferences.h"
#import "ZSTLegicyCommunicator.h"

#import "ElementParser.h"
#import "ZSTLogUtil.h"
#import "ZSTLegacyResponse.h"
#import "ZSTUtils.h"
#import "ZSTECMobileClientParams.h"
#import "ZSTUtils.h"
#import "SBJSON.h"
#import "ZipArchive.h"
#import "EncryptUtil.h"
#import <NSObject+SBJSON.h>


static NSThread * workThread;

@implementation ZSTF3Engine

@synthesize delegate;
@synthesize moduleType;
@synthesize dao;

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void)cancelAllRequest
{
    [[ZSTCommunicator shared] cancelByTarget:self];
    [[ZSTLegicyCommunicator shared] cancelByDelegate:self];
}

- (void)cancel:(NSURL *)theUrl
{
    [[ZSTCommunicator shared] cancelRequest:theUrl];
}

- (void)dealloc
{
    [self cancelAllRequest];
    self.dao = nil;
    [super dealloc];
}

#pragma mark Private method

- (BOOL) isValidDelegateForSelector:(SEL)selector
{
	return ((delegate != nil) && [delegate respondsToSelector:selector]);
}

+ (NSThread *)workThread
{
    if (workThread == nil) {
        workThread = [[NSThread alloc] initWithTarget:self selector:@selector(httpPollRunLoopThreadEntry) object:nil];
        assert(workThread != nil);
        
        [workThread setName:@"workThread"];
        [workThread start];
    }
    return workThread;
}

+ (void)httpPollRunLoopThreadEntry
{
    while (YES) {
        NSAutoreleasePool * pool;
        pool = [[NSAutoreleasePool alloc] init];
        assert(pool != nil);
        [[NSRunLoop currentRunLoop] run];
        [pool drain];
    }
    assert(NO);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (BOOL)updateECMobileClientParams
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
    NSDateFormatter *formater = [[[ NSDateFormatter alloc] init] autorelease];
    NSDate *curDate = [NSDate date];
    [formater setDateFormat:@"yyyy'-'MM'-'dd HH':'mm':'ss"];
    NSString * ClientTimeString = [formater stringFromDate:curDate];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:preferences.imsi forKey:@"IMEI"];
    [params setSafeObject:preferences.platform forKey:@"Platform"];
    [params setSafeObject:[UIDevice machine] forKey:@"UA"];
    [params setSafeObject:preferences.loginMsisdn forKey:@"Msisdn"];
    [params setSafeObject:@(preferences.version).stringValue forKey:@"Version"];
    [params setSafeObject:ClientTimeString forKey:@"ClientTime"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIPostToMethod:ECMOBILECLIENTPARAMS_URL
                                                              replacedParams:params
                                                              timeOutSeconds:8];
    if (response.error) {
        return NO;
    }
    
    DocumentRoot * element = response.xmlResponse;
    
    int MCRegistType = CONTENTSINT("Response MCRegistType");
    if (MCRegistType < MCRegistType_ForceRegister || MCRegistType > MCRegistType_VirtualRegister) {
        MCRegistType = MCRegistType_ForceRegister;
    }
    preferences.MCRegistType = MCRegistType;
    preferences.MCRegistDescription = CONTENTSTEXT("Response MCRegistDescription");
    preferences.MCRegistChangeDescription = CONTENTSTEXT("Response MCRegistChangeDescription");
    preferences.IPPushFlag = CONTENTSINT("Response IPPushFlag");
    preferences.HTTPPollTimeRule = CONTENTSTEXT("Response HTTPPollTimeRule");
    preferences.SMSHookFlag = CONTENTSINT("Response SMSHookFlag");
    preferences.centralServer = CONTENTSTEXT("Response CentralServer");
    preferences.centralServerPort = CONTENTSINT("Response CentralServerPort");
    preferences.httpServer = CONTENTSTEXT("Response HttpServer");
    preferences.httpServerPort = CONTENTSINT("Response HttpServerPort");
    preferences.udpServer = CONTENTSTEXT("Response UdpServer");
    preferences.udpServerPort = CONTENTSINT("Response UdpServerPort");
    preferences.RegToicpCMCC = CONTENTSTEXT("Response RegToicpCMCC");
    preferences.RegToicpUNICOM = CONTENTSTEXT("Response RegToicpUNICOM");
    preferences.RegToicpTELECOM = CONTENTSTEXT("Response RegToicpTELECOM");
    preferences.ParamValue = CONTENTSTEXT("Response StartUpCheckUpdate");

    NSString *ecid = [preferences.ECECCID length] == 8 ? preferences.ECECCID : [preferences.ECECCID stringByAppendingString:@"wg"];

    preferences.SinaWeiBo = [EncryptUtil decryptUseDES:CONTENTSTEXT("Response SinaWeiBoDeveloperAccount") key:ecid];
    preferences.TWeiBo = [EncryptUtil decryptUseDES:CONTENTSTEXT("Response TWeiBoDeveloperAccount") key:ecid];
    preferences.QQ = [EncryptUtil decryptUseDES:CONTENTSTEXT("Response QQDeveloperAccount") key:ecid];
    preferences.WeiXin = [EncryptUtil decryptUseDES:CONTENTSTEXT("Response WeiXinDeveloperAccount") key:ecid];
    
    [preferences synchronize];
    
    postNotificationOnMainThreadNoWait(NotificationName_UpdateECMobileClientParamsSuccessed);
    return YES;
}


- (void)doUpdateECMobileClientParams
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [ZSTF3Engine updateECMobileClientParams];
    [pool release];
}

- (void)enUpdateECMobileClientParams
{
    [self performSelector:@selector(doUpdateECMobileClientParams) onThread:[ZSTF3Engine workThread] withObject:nil waitUntilDone:NO];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getRegChecksumResponse:(ZSTLegacyResponse *)response
{
    if ([self isValidDelegateForSelector:@selector(getRegChecksumResponse)]) {
        [self.delegate getRegChecksumResponse];
    }
}
- (void)requestDidFail:(ZSTLegacyResponse *)response
{
    
    if ([self isValidDelegateForSelector:@selector(requestDidFail:)]) {
        [self.delegate requestDidFail:response];
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//手动绑定获取验证码
- (void)getRegChecksum:(NSString *)phoneNumber
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:phoneNumber forKey:@"LOGINMSISDN"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    [[ZSTLegicyCommunicator shared] openAPIPostToMethod:VERIFICATION_URL
                                   replacedParams:params
                                         delegate:(id<ZSTLegicyCommunicatorDelegate>)self
                                     failSelector:@selector(requestDidFail:)
                                  succeedSelector:@selector(getRegChecksumResponse:)
                                         userInfo:nil];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)registMobileClientDidResponse:(ZSTLegacyResponse *)response
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    DocumentRoot *element = response.xmlResponse;
    
    // 这里用的不是xPath，用的是Element Parser自己定义的语法,空格代表上下级关系
    NSString *loginMsisdn = CONTENTSTEXT("Response UserInfo LoginMsisdn");
    if (![loginMsisdn isEqualToString:preferences.loginMsisdn]) {
        preferences.loginMsisdn = loginMsisdn;
    }
    NSString *loginPassword = CONTENTSTEXT("Response UserInfo LoginPassword");
    if (![loginPassword isEqualToString:preferences.loginPassword]) {
        preferences.loginPassword = loginPassword;
    }
    
    if ([self isValidDelegateForSelector:@selector(registMobileClientDidResponse:loginPassword:)]) {
        [self.delegate registMobileClientDidResponse:loginMsisdn loginPassword:loginPassword];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)registMobileClient:(NSString *)phoneNumber checksum:(NSString *)checksum
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.imsi forKey:@"IMEI"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:preferences.platform forKey:@"Platform"];
    [params setSafeObject:@(preferences.version).stringValue forKey:@"Version"];
    [params setSafeObject:phoneNumber forKey:@"MSISDN"];
    [params setSafeObject:[UIDevice machine] forKey:@"UA"];
    [params setSafeObject:checksum forKey:@"VERIFICATIONCODE"];
    
    [[ZSTLegicyCommunicator shared] openAPIPostToMethod:REGISTER_URL
                                   replacedParams:params
                                         delegate:(id<ZSTLegicyCommunicatorDelegate>)self
                                     failSelector:@selector(requestDidFail:)
                                  succeedSelector:@selector(registMobileClientDidResponse:)
                                         userInfo:nil];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


+ (BOOL)registMobileClientFor:(NSString *)phoneNumber checksum:(NSString *)checksum
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.imsi forKey:@"IMEI"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:preferences.platform forKey:@"Platform"];
    [params setSafeObject:@(preferences.version).stringValue forKey:@"Version"];
    [params setSafeObject:phoneNumber forKey:@"MSISDN"];
    [params setSafeObject:[UIDevice machine] forKey:@"UA"];
    [params setSafeObject:checksum forKey:@"VERIFICATIONCODE"];
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIPostToMethod:REGISTER_URL
                                                              replacedParams:params
                                                              timeOutSeconds:15];
    if (response.error) {
        return NO;
    }
    DocumentRoot *element = response.xmlResponse;
    //    BOOL isReg = (CONTENTSINT("Response RegStatus") == REGISTER_STATUS_OK);
    
    // 这里用的不是xPath，用的是Element Parser自己定义的语法,空格代表上下级关系
    NSString *loginMsisdn = CONTENTSTEXT("Response UserInfo LoginMsisdn");
    if (![loginMsisdn isEqualToString:preferences.loginMsisdn]) {
        preferences.loginMsisdn = loginMsisdn;
        postNotificationOnMainThreadNoWait(NotificationName_LoginMsisdnChange);
    }
    preferences.loginPassword = CONTENTSTEXT("Response UserInfo LoginPassword");
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)checkVersionResponse:(ZSTLegacyResponse *)response
{
    DocumentRoot *element = response.xmlResponse;
    NSString *versionURL =  CONTENTSTEXT("Response Url");
    NSString *updateNote = nil;
    int updateVersion ;
    if (![versionURL isEmptyOrWhitespace]) {
        updateNote = CONTENTSTEXT("Response UpdateNote");
        updateVersion = CONTENTSINT("Response VerCode");
    }
    
    if ([self isValidDelegateForSelector:@selector(checkClientVersionResponse:updateNote:updateVersion:)]) {
        [self.delegate checkClientVersionResponse:versionURL updateNote:updateNote updateVersion:updateVersion];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)checkClientVersion
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    if ([ZSTF3Preferences shared].loginMsisdn) {
        
        [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    }
    
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:preferences.platform forKey:@"Platform"];
    [params setSafeObject:@"FALSE" forKey:@"TestFlag"];
    [params setSafeObject:@(preferences.version).stringValue forKey:@"Version"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    [[ZSTLegicyCommunicator shared] openAPIPostToMethod:CHECKVERSION_URL
                                   replacedParams:params
                                         delegate:(id<ZSTLegicyCommunicatorDelegate>)self
                                     failSelector:@selector(requestDidFail:)
                                  succeedSelector:@selector(checkVersionResponse:)
                                         userInfo:nil];
}

- (void)submitAdviceResponse:(ZSTLegacyResponse *)response
{
    DocumentRoot *element = response.xmlResponse;
    
    int resultCode = CONTENTSINT("Response Result");
    
    if (resultCode != REQUEST_CODE_OK)
    {
        [TKUIUtil alertInWindow:NSLocalizedString(@"提交失败", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
    }
    
    [TKUIUtil alertInWindow:NSLocalizedString(@"提交成功，谢谢支持", nil) withImage:[UIImage imageNamed:@"icon_smile_face.png"]];
    
    if ([self isValidDelegateForSelector:@selector(submitAdviceResponse)]) {
        [self.delegate submitAdviceResponse];
    }
}


- (void)submitAdvice:(NSString *)submitAdvice
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:submitAdvice forKey:@"Content"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    [[ZSTLegicyCommunicator shared] openAPIPostToMethod:SUBMITADVICE_URL
                                   replacedParams:params
                                         delegate:(id<ZSTLegicyCommunicatorDelegate>)self
                                     failSelector:@selector(requestDidFail:)
                                  succeedSelector:@selector(submitAdviceResponse:)
                                         userInfo:nil];
}


/////////////////////////////////////////////////////////////////////// private method /////////////////////////////////////////////////////////////////////////

+ (void)doSubmitUserLogBeforeDate:(NSDate *)date
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDate *latestSubmitClientLogDate = [userDefault objectForKey:LatestSubmitClientLogDate];
    if (latestSubmitClientLogDate && ![date isEqualToDateIgnoringTime:latestSubmitClientLogDate]) {
    
//    if (!latestSubmitClientLogDate || ![date isEqualToDateIgnoringTime:latestSubmitClientLogDate]) {
    
        [userDefault setObject:date forKey:LatestSubmitClientLogDate];
        [userDefault synchronize];
        NSString * content = [NSString stringWithFormat:@"<Log>%@</Log>",[[ZSTLogUtil getUserLogsBeforeDate:date] componentsJoinedByString:@"</Log>\n<Log>"]];
        BOOL bSuccess = [self submitUserLog:content];
        if (bSuccess) {
            //提交日志成功，删除旧日志
            [ZSTLogUtil deleteUserLogsBeforeDate:date];
        }
    }
    
    [pool release];
}

+ (void)submitUserLogBeforeDate:(NSDate *)date
{
    [self performSelector:@selector(doSubmitUserLogBeforeDate:) onThread:[self workThread] withObject:date waitUntilDone:NO];
}

//////////////////////////////////////////////////////////////////// private method ////////////////////////////////////////////////////////////////////////////

+ (BOOL)submitUserLog:(NSString *)content
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSString * LoginMsisdnString = preferences.loginMsisdn;
    
    if (!LoginMsisdnString)
    {
        return NO;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:content forKey:@"Content"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIPostToMethod:SUBMITUserLOG_URL_PATH replacedParams:params timeOutSeconds:180];
    return !response.error;
}

+ (BOOL)syncPushNotificationParams

{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSString * LoginMsisdnString = preferences.loginMsisdn;
    
    if (!LoginMsisdnString)
    {
        return NO;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.imsi forKey:@"Imsi"];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:preferences.platform forKey:@"Platform"];
    [params setSafeObject:preferences.pushToken forKey:@"PushNotificationCode"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIPostToMethod:SYNCPUSHNOTIFICATIONPARAM_URL
                                                                          replacedParams:params
                                                                          timeOutSeconds:8];
    if (response.error) {
        return NO;
    }
    
    return YES;
}


+ (NSString *) uploadFile:(NSData *)data extension:(NSString *)extension
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
	NSString * FileDataString = [data base64Encoding];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:FileDataString forKey:@"FileData"];
    [params setSafeObject:extension forKey:@"FileExtension"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIPostToMethod:SUBMITSYSTEMLOG_URL_PATH
                                                                          replacedParams:params];
    
	
    DocumentRoot * element = response.xmlResponse;
    
    int Result = CONTENTSINT("Response Result");
    if (Result == REQUEST_CODE_OK) {
        return CONTENTSTEXT("Response FileID");
    }
	return nil;
}

+ (void)uploadSysLog
{
    NSString *zipPath = [[ZSTUtils pathForECECC] stringByAppendingPathComponent: @"logs.zip"];
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    [zipArchive CreateZipFile2: zipPath];
    NSDirectoryEnumerator *enums = [[NSFileManager defaultManager] enumeratorAtPath: [ZSTUtils pathForLogs]];
    NSString *fileName = nil;
    while ((fileName = [enums nextObject]) != nil) {
        
        NSString *logPath = [[ZSTUtils pathForLogs] stringByAppendingPathComponent:fileName];
        [zipArchive addFileToZip: logPath newname: fileName];
    }
    [zipArchive CloseZipFile2];
    [zipArchive release];
    NSData *data = [NSData dataWithContentsOfFile:zipPath];
    [self uploadFile:data extension:@"log"];
    [[NSFileManager defaultManager] removeItemAtPath:zipPath error:nil];
}

- (void) updateECClientVisitInfoWithMsisdn:(NSString *)msisdn
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:UpdateECClientVisitInfo
                                         params:params
                                         target:self
                                       selector:@selector(updateECClientVisitResponse:userInfo:)
                                       userInfo:nil];
}

- (void)updateECClientVisitResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
   if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(updateECClientDidSucceed:)]) {
            [self.delegate updateECClientDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(updateECClientDidFailed:)]) {
            [self.delegate updateECClientDidFailed:message];
        }
    }

}

- (void) loginWithMsisdn:(NSString *)msisdn password:(NSString *)password
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:password forKey:@"Password"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:Login
                                         params:params
                                         target:self
                                       selector:@selector(loginResponse:userInfo:)
                                       userInfo:nil];
}

- (void)loginResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(loginDidSucceed:)]) {
            [self.delegate loginDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(loginDidFailed:)]) {
            [self.delegate loginDidFailed:message];
        }
    }
}

- (void) getVerificationCodeWithMsisdn:(NSString *)msisdn OperType:(int)opertype
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:[NSNumber numberWithInt:opertype] forKey:@"OperType"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:GetVerificationCode
                                         params:params
                                         target:self
                                       selector:@selector(getVerificationCodeResponse:userInfo:)
                                       userInfo:nil];

}

- (void)getVerificationCodeResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getVerificationCodeDidSucceed:)]) {
            [self.delegate getVerificationCodeDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(getVerificationCodeDidFailed:)]) {
            [self.delegate getVerificationCodeDidFailed:message];
        }
    }
}

- (void) checkVerificationCodeWithMsisdn:(NSString *)msisdn verificationCode:(NSString *)code OperType:(int)opertype
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:code forKey:@"VerificationCode"];
    [params setSafeObject:[NSNumber numberWithInt:opertype] forKey:@"OperType"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:CheckVerificationCode
                                         params:params
                                         target:self
                                       selector:@selector(checkVerificationCodeResponse:userInfo:)
                                       userInfo:nil];
}

- (void)checkVerificationCodeResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(checkVerificationCodeDidSucceed:)]) {
            [self.delegate checkVerificationCodeDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(checkVerificationCodeDidFailed:)]) {
            [self.delegate checkVerificationCodeDidFailed:message];
        }
    }
}

- (void) registerWithMsisdn:(NSString *)msisdn password:(NSString *)password verificationCode:(NSString *)code
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:password forKey:@"Password"];
    [params setSafeObject:code forKey:@"VerificationCode"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:Register
                                         params:params
                                         target:self
                                       selector:@selector(registerResponse:userInfo:)
                                       userInfo:nil];
}

- (void)registerResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(registerDidSucceed:)]) {
            [self.delegate registerDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(registerDidFailed:)]) {
            [self.delegate registerDidFailed:message];
        }
    }
}

- (void) updateMsisdnWithMsisdn:(NSString *)msisdn
                         userId:(NSString *)userid
               verificationCode:(NSString *)code
                       OperType:(int)opertype
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:code forKey:@"VerificationCode"];
    [params setSafeObject:userid forKey:@"UserId"];
    [params setSafeObject:[NSNumber numberWithInt:opertype] forKey:@"OperType"];
       
    [[ZSTCommunicator shared] openAPIPostToPath:UpdateMsisdn
                                         params:params
                                         target:self
                                       selector:@selector(updateMsisdnResponse:userInfo:)
                                       userInfo:nil];

}

- (void)updateMsisdnResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(updateMSisdnDidSucceed:)]) {
            [self.delegate updateMSisdnDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(updateMSisdnDidFailed:)]) {
            [self.delegate updateMSisdnDidFailed:message];
        }
    }
}

- (void) updatePasswordWithMsisdn:(NSString *)msisdn verificationCode:(NSString *)code password:(NSString *)password OperType:(int)opertype
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:code forKey:@"VerificationCode"];
    [params setSafeObject:password forKey:@"Password"];
    [params setSafeObject:[NSNumber numberWithInt:opertype] forKey:@"OperType"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:UpdatePassword
                                         params:params
                                         target:self
                                       selector:@selector(updatePasswordResponse:userInfo:)
                                       userInfo:nil];
}

- (void)updatePasswordResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(updatePasswordDidSucceed:)]) {
            [self.delegate updatePasswordDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(updatePasswordDidFailed:)]) {
            [self.delegate updatePasswordDidFailed:message];
        }
    }
}

- (void) getUserInfoWithMsisdn:(NSString *)msisdn userId:(NSString *)userid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:userid forKey:@"UserId"];

    
    [[ZSTCommunicator shared] openAPIPostToPath:GetUserInfo
                                         params:params
                                         target:self
                                       selector:@selector(getUserInfoResponse:userInfo:)
                                       userInfo:nil];
}


- (void)getUserInfoResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getUserInfoDidSucceed:)]) {
            [self.delegate getUserInfoDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(getUserInfoDidFailed:)]) {
            [self.delegate getUserInfoDidFailed:message];
        }
    }
}

- (void) updateUserInfoWith:(NSString *)msisdn
                     userId:(NSString *)userid
                    iconUrl:(NSString *)iconurl
                       name:(NSString *)name
                   birthday:(NSInteger)birthday
                        sex:(int)sex
                    address:(NSString *)address
                  signature:(NSString *)signature
                phonePublic:(NSInteger)phonePublic
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:userid forKey:@"UserId"];
    [params setSafeObject:iconurl forKey:@"Icon"];
    [params setSafeObject:name forKey:@"Name"];
    [params setSafeObject:@(birthday) forKey:@"Birthday"];
    [params setSafeObject:@(sex) forKey:@"Sex"];
    [params setSafeObject:address forKey:@"Address"];
    [params setSafeObject:signature forKey:@"Signature"];
    [params setSafeObject:@(phonePublic) forKey:@"PhonePublic"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:UpdateUserInfo
                                         params:params
                                         target:self
                                       selector:@selector(updateUserInfoResponse:userInfo:)
                                       userInfo:nil];
}

- (void)updateUserInfoResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(updateUserInfoDidSucceed:)]) {
            [self.delegate updateUserInfoDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(updateUserInfoDidFailed:)]) {
            [self.delegate updateUserInfoDidFailed:message];
        }
    }
}

- (void) getPointWithMsisdn:(NSString *)msisdn userId:(NSString *)userid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:userid forKey:@"UserId"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:GetPoint
                                         params:params
                                         target:self
                                       selector:@selector(getPointResponse:userInfo:)
                                       userInfo:nil];
}

- (void)getPointResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getPointDidSucceed:)]) {
            [self.delegate getPointDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(getPointDidFailed:)]) {
            [self.delegate getPointDidFailed:message];
        }
    }
}

- (void) automaticLoginWithMsisdn:(NSString *)msisdn userId:(NSString *)userid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:userid forKey:@"UserId"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:AutomaticLogin
                                         params:params
                                         target:self
                                       selector:@selector(automaticLoginResponse:userInfo:)
                                       userInfo:nil];

}

- (void)automaticLoginResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(automaticLoginDidSucceed:)]) {
            [self.delegate automaticLoginDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(automaticLoginDidFailed:)]) {
            [self.delegate automaticLoginDidFailed:message];
        }
    }
}


- (void) getECClientParams
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:GetECClientParams
                                         params:params
                                         target:self
                                       selector:@selector(getECClientParamsResponse:userInfo:)
                                       userInfo:nil];
}

- (void)getECClientParamsResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getECClientParamsDidSucceed:)]) {
            [self.delegate getECClientParamsDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(getECClientParamsDidFailed:)]) {
            [self.delegate getECClientParamsDidFailed:message];
        }
    }
}


- (void) updatePointWithMsisdn:(NSString *)msisdn
                        userId:(NSString *)userid
                      pointNum:(int)pointnum
                     pointType:(int)pointtype
                    costAmount:(NSString *)costamount
                          desc:(NSString *)desc
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:userid forKey:@"UserId"];
    [params setSafeObject:[NSNumber numberWithInt:pointnum] forKey:@"PointNum"];
    [params setSafeObject:@(pointtype) forKey:@"PointType"];
    [params setSafeObject:costamount forKey:@"CostAmount"];
    [params setSafeObject:desc forKey:@"Description"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:UpdatePoint
                                         params:params
                                         target:self
                                       selector:@selector(updatePointResponse:userInfo:)
                                       userInfo:nil];
}

- (void)updatePointResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(updatePointDidSucceed:)]) {
            [self.delegate updatePointDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(updatePointDidFailed:)]) {
            [self.delegate updatePointDidFailed:message];
        }
    }
}

- (void) uploadFileWith:(NSString *)msisdn userId:(NSString *)userid data:(NSData *)data
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:userid forKey:@"UserId"];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[NSNumber numberWithInt:5] forKey:@"Platform"];
    
    [[ZSTCommunicator shared] uploadFileTopath:UploadFile
                                        params:params
                                          data:data
                                        suffix:@"png"
                                        target:self
                                      selector:@selector(uploadFileResponse:userInfo:)
                                      userInfo:nil];
}

- (void)uploadFileResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(uploadFileDidSucceed:)]) {
            [self.delegate uploadFileDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(uploadFileDidFailed:)]) {
            [self.delegate uploadFileDidFailed:message];
        }
    }
}

- (void) checkPasswordWithMsisdn:(NSString *)msisdn userId:(NSString *)userid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:userid forKey:@"UserId"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:CheckPassword
                                         params:params
                                         target:self
                                       selector:@selector(checkPasswordResponse:userInfo:)
                                       userInfo:nil];

}

- (void)checkPasswordResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(checkPasswordDidSucceed:)]) {
            [self.delegate checkPasswordDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(checkPasswordDidFailed:)]) {
            [self.delegate checkPasswordDidFailed:message];
        }
    }
}

- (void) logoutWithMsisdn:(NSString *)msisdn userId:(NSString *)userid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:userid forKey:@"UserId"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:Logout
                                         params:params
                                         target:self
                                       selector:@selector(logoutResponse:userInfo:)
                                       userInfo:nil];
}

- (void)logoutResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(logoutDidSucceed:)]) {
            [self.delegate logoutDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(logoutDidFailed:)]) {
            [self.delegate logoutDidFailed:message];
        }
    }
}

// 获得第三方授权登录
- (void) getThirdLoginAuthorizeEcid:(NSString *)ecid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:ecid forKey:@"ecid"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:LoginAuthorize, ecid]
                                         params:params
                                         target:self
                                       selector:@selector(thirdLoginAuthorizeResponse:userInfo:)
                                       userInfo:nil];
}

- (void)thirdLoginAuthorizeResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"HHHHHHH === %@", response.jsonResponse);
    if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(loginAuthorizeDidSucceed:)]) {
            [self.delegate loginAuthorizeDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(loginAuthorizeDidFailed:)]) {
            [self.delegate loginAuthorizeDidFailed:message];
        }
    }
}


#pragma mark - 上传崩溃日志
-(void)postSystemExceptionLog:(NSString *)ecid logInfo:(NSString *)logInfo{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:ecid forKey:@"Ecid"];
    [params setSafeObject:logInfo forKey:@"LogInfo"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:SystemException
                                         params:params
                                         target:self
                                       selector:@selector(postSystemExceptionRespone:)
                                       userInfo:nil];
    
    
}

-(void)postSystemExceptionRespone:(ZSTResponse *)response{
    
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(postSystemExceptionLogSucceed:)]) {
            [self.delegate postSystemExceptionLogSucceed:response.jsonResponse];
        }
    }else{
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        if ([self isValidDelegateForSelector:@selector(postSystemExceptionLogFailed:)]) {
            [self.delegate postSystemExceptionLogFailed:message];
        }

    }
}


#pragma mark - 第三方登录数据
#pragma mark 微信开放平台，QQ开放平台
- (void)thirdLoginWithDictionary:(NSDictionary *)dict
{
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:ThirdLogin, [ZSTF3Preferences shared].ECECCID]
                                         params:dict
                                         target:self
                                       selector:@selector(thirdLoginResponse:userInfo:)
                                       userInfo:nil];
}

- (void)thirdLoginResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"HHHHHHH === %@", response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(thirdLoginDidSucceed:)]) {
            [self.delegate thirdLoginDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(thirdLoginDidFailed:)]) {
            [self.delegate thirdLoginDidFailed:message];
        }
    }
}

- (void)checkPhoneNumIsExist:(NSString *)phoneMsisdn
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [param setValue:phoneMsisdn forKey:@"Msisdn"];

//    
//    [[ZSTCommunicator shared] openAPIPostToPath:CheckPhoneNum params:param target:self selector:@selector(checkResponse:userInfo:)];
    [[ZSTCommunicator shared] openAPIPostToPaths:CheckPhoneNum params:param target:self selector:@selector(checkResponse:userInfo:)];
}

- (void)checkResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([self.delegate respondsToSelector:@selector(checkPhoneNumIsExistSuccess:)])
    {
        [self.delegate checkPhoneNumIsExistSuccess:response.jsonResponse];
    }
}

- (void)bindingMobileWithMsisdn:(NSString *)msisdn password:(NSString *)password
{
    NSString *userId = [ZSTF3Preferences shared].UserId;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:password forKey:@"Password"];
    [params setSafeObject:userId forKey:@"UserId"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:Login
                                         params:params
                                         target:self
                                       selector:@selector(bindingMobileResponse:userInfo:)
                                       userInfo:nil];
    
}

- (void)bindingMobileResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(bindingMobileDidSucceed:)]) {
            [self.delegate bindingMobileDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(bindingMobileDidFailed:)]) {
            [self.delegate bindingMobileDidFailed:message];
        }
    }
}

- (void)finishedLoginWithPassWord:(NSString *)password AndUserId:(NSString *)userId MobilePhone:(NSString *)msisdn Code:(NSString *)code
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:password forKey:@"Password"];
    [params setSafeObject:[ZSTF3Preferences shared].UserId forKey:@"UserId"];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:code forKey:@"VerificationCode"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:Register
                                         params:params
                                         target:self
                                       selector:@selector(finishedLoginResponse:userInfo:)
                                       userInfo:nil];
}


- (void)finishedLoginResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(finishedLoginDidSucceed:)]) {
            [self.delegate finishedLoginDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(finishedLoginDidFailed:)]) {
            [self.delegate finishedLoginDidFailed:message];
        }
    }
}


@end
