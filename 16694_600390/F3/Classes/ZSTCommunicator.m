//
//  ZSTCommunicator.h
//
//
//  Created by luobin on 7/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTCommunicator.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "ZSTF3Preferences.h"
#import "ZSTUtils.h"
#import "ZSTResponse.h"

#define kRequestUserInfo @"kZSTRequestUserInfo"

#define Platform 5

NSString * const ZSTHttpRequestErrorDomain = @"ZSTHttpRequestErrorDomain";

@interface ZSTCommunicator ()

@end

@implementation ZSTCommunicator

SINGLETON_IMPLEMENTATION(ZSTCommunicator);

- (id)init {
    self = [super init];
    if (self) {
        networkQueue = [[ASINetworkQueue alloc] init];
        [networkQueue setRequestDidFinishSelector:@selector(requestFetchComplete:)];
        [networkQueue setRequestDidFailSelector:@selector(requestFetchFailed:)];
        [networkQueue setDelegate:self];
        [networkQueue setShouldCancelAllRequestsOnFailure:NO];
        [networkQueue setMaxConcurrentOperationCount:2]; // ASI default set = 4.
        [networkQueue go];
    }
    return self;
}

- (void)dealloc {
    [networkQueue reset];
    TKRELEASE(networkQueue);
    [super dealloc];
}

#pragma mark - URL Request method
// ############################ URL request methods ############################
- (void)uploadFileToPath:(NSString *)path
                filePath:(NSString *)filePath
                  target:(id)target
                selector:(SEL)selector
        progressSelector:(SEL)progressSelector
                userInfo:(id)userInfo {
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    NSError *error;
    NSDictionary *imageInfo = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:&error];
    // TKDPRINT(@"The image info : %@", imageInfo);
    if (!exists || [imageInfo objectForKey:NSFileType]!=NSFileTypeRegular) {
        ZSTResponse *response = [[ZSTResponse alloc] init];
        response.request = nil;
        response.stringResponse = nil;
        response.userInfo = userInfo;
        response.resultCode = ZSTResultCode_Other;
        response.errorMsg = @"File not found.";
        [target performSelector:selector
                     withObject:response
                     withObject:userInfo];
        
        return;
    }
    NSString *suffix = [[[NSFileManager defaultManager] displayNameAtPath:filePath] pathExtension];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    [self uploadFileToPath:path
                      data:data
                    suffix:suffix
                    target:target
                  selector:selector
          progressSelector:progressSelector
                  userInfo:userInfo];
}

- (void)uploadFileToPath:(NSString *)path
                    data:(NSData *)data
                  suffix:(NSString *)suffix
                  target:(id)target
                selector:(SEL)selector
        progressSelector:(SEL)progressSelector
                userInfo:(id)userInfo {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    [params setSafeObject:[ZSTF3Preferences shared].imsi forKey:@"IMEI"];
    
    NSString *userID = [TKKeychainUtils getUserid];
    [params setObject:userID?userID:@"" forKey:@"UID"];
    
    [params setObject:suffix forKey:@"Ex"];
    NSString *url = [path stringByAddingQuery:params];
    TKDINFO(@"Will post url address : %@", url);
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url]];
    request.postBody = (NSMutableData *)data;
    
    [request setTimeOutSeconds:30];
    //    [request setShouldContinueWhenAppEntersBackground:NO];
    if (target && [target respondsToSelector:progressSelector]) {
        [request setUploadProgressDelegate:self];
        [request setShowAccurateProgress:YES];
    }
    [request setDidFinishSelector:@selector(requestFetchComplete:)];
    [request setDidFailSelector:@selector(requestFetchFailed:)];
    [request setDelegate:self];
    ZSTCommunicatorUserInfo *communicatorUserInfo = [[ZSTCommunicatorUserInfo alloc] init];
    communicatorUserInfo.target = target;
    communicatorUserInfo.selector = selector;
    request.userInfo = [NSMutableDictionary dictionaryWithObject:communicatorUserInfo forKey:kRequestUserInfo];
    [communicatorUserInfo release];
    
    [request startAsynchronous];
    [uploadRequestsSet addObject:request];
}

- (void)uploadFileTopath:(NSString *)path
                  params:(NSDictionary *)params
                    data:(NSData *)data
                  suffix:(NSString *)suffix
                  target:(id)target
                selector:(SEL)selector
                userInfo:(id)userInfo
{
    [(NSMutableDictionary *)params setSafeObject:@"png" forKey:@"ext"];
    NSString *url = [path stringByAddingQuery:params];
    TKDINFO(@"Will post url address : %@", url);
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url]];
    request.postBody = (NSMutableData *)data;
    
    [request setTimeOutSeconds:30];
    [request setDidFinishSelector:@selector(requestFetchComplete:)];
    [request setDidFailSelector:@selector(requestFetchFailed:)];
    [request setDelegate:self];
    ZSTCommunicatorUserInfo *communicatorUserInfo = [[ZSTCommunicatorUserInfo alloc] init];
    communicatorUserInfo.target = target;
    communicatorUserInfo.selector = selector;
    request.userInfo = [NSMutableDictionary dictionaryWithObject:communicatorUserInfo forKey:kRequestUserInfo];
    [communicatorUserInfo release];
    
    [request startAsynchronous];
    [uploadRequestsSet addObject:request];
}

- (void)openAPIPostToPaths:(NSString *)path
                   params:(NSDictionary *)params
                   target:(id)target
                 selector:(SEL)selector
                 userInfo:(id)userInfo
                matchCase:(BOOL)matchCase
{
    NSURL *url = [NSURL URLWithString:[path stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: [ZSTUtils md5Signature:params], @"Md5Verify", nil]]];
    [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].imsi forKey:@"IMEI"];
    [(NSMutableDictionary *)params setSafeObject:[UIDevice machine] forKey:@"UA"];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setTimeOutSeconds:30];
    
    NSData *body = [NSData data];
    body = [[(NSDictionary *)params JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
    [request appendPostData:body];
    
    ZSTCommunicatorUserInfo *communicatorUserInfo = [[ZSTCommunicatorUserInfo alloc] init];
    communicatorUserInfo.target = target;
    communicatorUserInfo.selector = selector;
    communicatorUserInfo.userInfo = userInfo;
    request.userInfo = [NSMutableDictionary dictionaryWithObject:communicatorUserInfo forKey:kRequestUserInfo];
    [communicatorUserInfo release];
    
#ifdef DEBUG
    NSString *requestString = [[[NSString alloc] initWithData:[request postBody] encoding:NSUTF8StringEncoding] autorelease];
    TKDPRINT(@"The requset postbody string : %@", requestString);
#endif
    [networkQueue addOperation:request];
}


- (void)openAPIPostByDCToPath:(NSString *)path params:(NSDictionary *)param target:(id)target selector:(SEL)selector userInfo:(id)userInfo matchCase:(BOOL)matchCase
{
    NSURL *url = [NSURL URLWithString:path];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setTimeOutSeconds:30];
    
    NSMutableDictionary *params = [param mutableCopy];
    [params setValue:@"ios" forKey:@"client"];
    
    NSData *body = [NSData data];
    body = [[(NSDictionary *)param JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
    [request appendPostData:body];
    
    ZSTCommunicatorUserInfo *communicatorUserInfo = [[ZSTCommunicatorUserInfo alloc] init];
    communicatorUserInfo.target = target;
    communicatorUserInfo.selector = selector;
    communicatorUserInfo.userInfo = userInfo;
    request.userInfo = [NSMutableDictionary dictionaryWithObject:communicatorUserInfo forKey:kRequestUserInfo];
    [communicatorUserInfo release];
    
#ifdef DEBUG
    NSString *requestString = [[[NSString alloc] initWithData:[request postBody] encoding:NSUTF8StringEncoding] autorelease];
    TKDPRINT(@"The requset postbody string : %@", requestString);
#endif
    [networkQueue addOperation:request];
}

- (void)openAPIPostToPath:(NSString *)path
                   params:(NSDictionary *)params
                   target:(id)target
                 selector:(SEL)selector
                 userInfo:(id)userInfo
                 matchCase:(BOOL)matchCase
{
    if (matchCase) {
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].imsi forKey:@"IMEI"];
        [(NSMutableDictionary *)params setSafeObject:[UIDevice machine] forKey:@"UA"];
    }else{
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"msisdn"];
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].imsi forKey:@"imei"];
        [(NSMutableDictionary *)params setSafeObject:[UIDevice machine] forKey:@"ua"];
    }

    NSURL *url = [NSURL URLWithString:[path stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: [ZSTUtils md5Signature:params], @"Md5Verify", nil]]];
    
    TKDINFO(@"Will post url address : %@", url);
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setTimeOutSeconds:30];//30

    
    TKDINFO(@"Will post param:%@", params);
    NSData *body = [NSData data];
    
    if (matchCase) {
        body = [[(NSDictionary *)params JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
    }else{
        body = [[[ZSTUtils lowerKeys:params] JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
    }
    [request appendPostData:body];
    
    ZSTCommunicatorUserInfo *communicatorUserInfo = [[ZSTCommunicatorUserInfo alloc] init];
    communicatorUserInfo.target = target;
    communicatorUserInfo.selector = selector;
    communicatorUserInfo.userInfo = userInfo;
    request.userInfo = [NSMutableDictionary dictionaryWithObject:communicatorUserInfo forKey:kRequestUserInfo];
    [communicatorUserInfo release];
    
#ifdef DEBUG
    NSString *requestString = [[[NSString alloc] initWithData:[request postBody] encoding:NSUTF8StringEncoding] autorelease];
    TKDPRINT(@"The requset postbody string : %@", requestString);
#endif
    [networkQueue addOperation:request];
}

#pragma mark  get


- (void)openAPIGetToPath:(NSString *)path
                  params:(NSDictionary *)params
                  target:(id)target
                selector:(SEL)selector
                userInfo:(id)userInfo
               matchCase:(BOOL)matchCase
{
    if (matchCase) {
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].imsi forKey:@"IMEI"];
        [(NSMutableDictionary *)params setSafeObject:[UIDevice machine] forKey:@"UA"];
    }else{
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"msisdn"];
        [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].imsi forKey:@"imei"];
        [(NSMutableDictionary *)params setSafeObject:[UIDevice machine] forKey:@"ua"];
    }
    
    
    path = [path stringByAddingPercentEscapesUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingUTF8)];
    NSURL *url = [NSURL URLWithString:[path stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: [ZSTUtils md5Signature:params], @"Md5Verify", nil]]];
    
    
    //    NSString* urlString = [NSString stringWithFormat:@"http://60.190.20.174/NBHYYYServices/ZQSBUpload.aspx?type=aqjc&filename=%@", fileName];
    //	urlString = [urlString stringByAddingPercentEscapesUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000)];
    
    
    TKDINFO(@"Will post url address : %@", url);
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setTimeOutSeconds:30];//30
    
    
    TKDINFO(@"Will post param:%@", params);
    NSData *body = [NSData data];
    
    if (matchCase) {
        body = [[(NSDictionary *)params JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
    }else{
        body = [[[ZSTUtils lowerKeys:params] JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
    }
    [request appendPostData:body];
    
    ZSTCommunicatorUserInfo *communicatorUserInfo = [[ZSTCommunicatorUserInfo alloc] init];
    communicatorUserInfo.target = target;
    communicatorUserInfo.selector = selector;
    communicatorUserInfo.userInfo = userInfo;
    request.userInfo = [NSMutableDictionary dictionaryWithObject:communicatorUserInfo forKey:kRequestUserInfo];
    [communicatorUserInfo release];
    
#ifdef DEBUG
    NSString *requestString = [[[NSString alloc] initWithData:[request postBody] encoding:NSUTF8StringEncoding] autorelease];
    TKDPRINT(@"The requset postbody string : %@", requestString);
#endif
    [networkQueue addOperation:request];
}

- (void)openAPIPostToPath:(NSString *)path
                   params:(NSDictionary *)params
                   target:(id)target
                 selector:(SEL)selector
                 userInfo:(id)userInfo
           longConnection:(BOOL)isLongCon
{
    
    NSMutableDictionary *mutableParams = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary *)params];
    if (mutableParams == nil) {
        mutableParams = [NSMutableDictionary dictionary];
    }
    [(NSMutableDictionary *)mutableParams setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    [(NSMutableDictionary *)mutableParams setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"msisdn"];
    [(NSMutableDictionary *)mutableParams setSafeObject:[ZSTF3Preferences shared].imsi forKey:@"imei"];
    
    NSURL *url = [NSURL URLWithString:[path stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: [ZSTUtils md5Signature:mutableParams], @"Md5Verify", nil]]];
    
    TKDINFO(@"Will post url address : %@", url);
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    if (isLongCon) {
        [request addRequestHeader:@"Connection" value:@"Keep-Alive"];
        [request setTimeOutSeconds:300];//30
    } else {
        [request setTimeOutSeconds:30];
    }
    
    TKDINFO(@"Will post param:%@", mutableParams);
    
    NSData *body = [[(NSDictionary *)mutableParams JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
    [request appendPostData:body];
    
    ZSTCommunicatorUserInfo *communicatorUserInfo = [[ZSTCommunicatorUserInfo alloc] init];
    communicatorUserInfo.target = target;
    communicatorUserInfo.selector = selector;
    communicatorUserInfo.userInfo = userInfo;
    request.userInfo = [NSMutableDictionary dictionaryWithObject:communicatorUserInfo forKey:kRequestUserInfo];
    [communicatorUserInfo release];
    
#ifdef DEBUG
    NSString *requestString = [[[NSString alloc] initWithData:[request postBody] encoding:NSUTF8StringEncoding] autorelease];
    TKDPRINT(@"The requset postbody string : %@", requestString);
#endif
    [networkQueue addOperation:request];
}

//个人中心
- (void)openAPIPostToPath:(NSString *)path
                   params:(NSDictionary *)params
                   target:(id)target
                 selector:(SEL)selector
                 userInfo:(id)userInfo
{
    [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].imsi forKey:@"IMEI"];
    [(NSMutableDictionary *)params setSafeObject:[UIDevice machine] forKey:@"UA"];
    [(NSMutableDictionary *)params setSafeObject:@([ZSTF3Preferences shared].version) forKey:@"Version"];
    [(NSMutableDictionary *)params setSafeObject:[ZSTF3Preferences shared].platform forKey:@"ClientType"];
     [(NSMutableDictionary *)params setSafeObject:[NSNumber numberWithInt:Platform] forKey:@"Platform"];
    
    NSURL *url = [NSURL URLWithString:[path stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: [ZSTUtils md5Signature:params], @"Md5Verify", nil]]];
    
    TKDINFO(@"Will post url address : %@", url);
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setTimeOutSeconds:30];//30
    
    
    TKDINFO(@"Will post param:%@", params);
    NSData *body = [NSData data];
    
    body = [[(NSDictionary *)params JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
    [request appendPostData:body];
    
    ZSTCommunicatorUserInfo *communicatorUserInfo = [[ZSTCommunicatorUserInfo alloc] init];
    communicatorUserInfo.target = target;
    communicatorUserInfo.selector = selector;
    communicatorUserInfo.userInfo = userInfo;
    request.userInfo = [NSMutableDictionary dictionaryWithObject:communicatorUserInfo forKey:kRequestUserInfo];
    [communicatorUserInfo release];
    
#ifdef DEBUG
    NSString *requestString = [[[NSString alloc] initWithData:[request postBody] encoding:NSUTF8StringEncoding] autorelease];
    TKDPRINT(@"The requset postbody string : %@", requestString);
#endif
    [networkQueue addOperation:request];
}



// Post
- (void)openAPIPostToPath:(NSString *)path
                   params:(NSDictionary *)params
                   target:(id)target
                 selector:(SEL)selector {
    
    [self openAPIPostToPath:path
                     params:params
                     target:target
                   selector:selector
                   userInfo:nil
                  matchCase:YES];

}

// Post
- (void)openAPIPostToPaths:(NSString *)path
                   params:(NSDictionary *)params
                   target:(id)target
                 selector:(SEL)selector {
    
    [self openAPIPostToPaths:path
                     params:params
                     target:target
                   selector:selector
                   userInfo:nil
                  matchCase:YES];
    
    
    
}

- (void)openAPIPostDCToPaths:(NSString *)path
                    params:(NSDictionary *)params
                    target:(id)target
                  selector:(SEL)selector {
    
    [self openAPIPostByDCToPath:path params:params target:target selector:selector userInfo:nil matchCase:YES];
    
}


- (void)openAPIDownLoadToPath:(NSString *)path
                     filepath:(NSString *)filepath
                       target:(id)target
                     selector:(SEL)selector
                     userInfo:(id)userInfo
{
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:path]];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setTimeOutSeconds:180];//30
    [request setDownloadDestinationPath:filepath];
    
    ZSTCommunicatorUserInfo *communicatorUserInfo = [[ZSTCommunicatorUserInfo alloc] init];
    communicatorUserInfo.target = target;
    communicatorUserInfo.selector = selector;
    communicatorUserInfo.userInfo = userInfo;
    request.userInfo = [NSMutableDictionary dictionaryWithObject:communicatorUserInfo forKey:kRequestUserInfo];
    [communicatorUserInfo release];
    
    [networkQueue addOperation:request];
}


- (void)cancelRequest:(NSURL *)url
{
    for (ASIHTTPRequest *request in [networkQueue operations]) {
        if ([request.url isEqual:url]) {
            [request cancel];
        }
    }
}

// ############################ URL request methods ############################


#pragma mark - ASI delegate
- (void)requestFetchFailed:(ASIHTTPRequest *)request {
    TKDINFO(@"request requestFetchFailed fetch has failed....... \n and error is : %@", [request error]);
    
    ZSTCommunicatorUserInfo *userInfo = (ZSTCommunicatorUserInfo *)[request.userInfo objectForKey:kRequestUserInfo];
    [uploadRequestsSet removeObject:request];
    
    if (userInfo.target && [userInfo.target respondsToSelector:userInfo.selector]) {
        ZSTResponse *response = [[ZSTResponse alloc] init];
        response.request = request;
        response.stringResponse = request.responseString;
        response.userInfo = userInfo.userInfo;
        NSError *error = request.error;
        if (error.code == ASIRequestTimedOutErrorType) {
            response.resultCode = ZSTResultCode_RequestTimedOut;
        } else if (error.code == ASIConnectionFailureErrorType) {
            response.resultCode = ZSTResultCode_ConnectionFailure; 
        } else {
            response.resultCode = ZSTResultCode_Other;
        }
        response.errorMsg = error.localizedDescription;
        [userInfo.target performSelector:userInfo.selector
                              withObject:response
                              withObject:userInfo.userInfo];
    }
}

- (void)requestFetchComplete:(ASIHTTPRequest *)request {
    ZSTCommunicatorUserInfo *userInfo = (ZSTCommunicatorUserInfo *)[request.userInfo objectForKey:kRequestUserInfo];
    [uploadRequestsSet removeObject:request];
    TKDINFO(@"The requestFetchComplete response : [%@]", [request responseString]);
    
    if (userInfo.target && [userInfo.target respondsToSelector:userInfo.selector]) {
        ZSTResponse *response = [[ZSTResponse alloc] init];
        response.request = request;
        response.stringResponse = request.responseString;
        response.userInfo = userInfo.userInfo;
        NSDictionary *jsonDic = [[request responseString] JSONValue];
        if (jsonDic == nil) {
            response.resultCode = ZSTResultCode_ParseJsonFailure;
            response.errorMsg = [NSString stringWithFormat:@"Parser json string:'%@' has occur an error.", [request responseString]];
        } else {
            
            id resultTmp = [jsonDic insensitiveObjectForKey:@"Result"];
            id statusTmp = [jsonDic insensitiveObjectForKey:@"status"];
            
            BOOL result = NO;
            BOOL status = NO;
            
            if (![resultTmp isKindOfClass:[NSArray class]]) {
                result = [resultTmp boolValue];
            }
            if (![statusTmp isKindOfClass:[NSArray class]]) {
                status = [statusTmp boolValue];
            }
            
            response.jsonResponse = jsonDic;
            
            if (!result && !status) {
                NSString *message = [jsonDic insensitiveObjectForKey:@"Notice"];
                response.resultCode = ZSTResultCode_Failure;
                response.errorMsg = message;
            } else {
                response.resultCode = ZSTResultCode_OK;
                response.errorMsg = nil;
            }
        }
        [userInfo.target performSelector:userInfo.selector
                              withObject:response
                              withObject:userInfo.userInfo];
    }
}

- (void)cancelByTarget:(id)target {
    @synchronized(self) {
        for (ASIHTTPRequest *request in [networkQueue operations]) {
            ZSTCommunicatorUserInfo *userInfo = (ZSTCommunicatorUserInfo *)[request.userInfo objectForKey:kRequestUserInfo];
            if (userInfo.target == target) {
                [request clearDelegatesAndCancel];
                userInfo.target = nil;
            }
        }
    }
}

- (void)cancelUploadByTarget:(id)target {
    @synchronized(self) {
        
        for (ASIHTTPRequest *request in uploadRequestsSet) {
            ZSTCommunicatorUserInfo *userInfo = (ZSTCommunicatorUserInfo *)[request.userInfo objectForKey:kRequestUserInfo];
            if (userInfo.target == target) {
                TKDINFO(@"The cancel upload target is : %@", target);
                
                [request clearDelegatesAndCancel];
                userInfo.target = nil;
            }
        }
    }
}

@end

#pragma mark -
@implementation ZSTCommunicatorUserInfo

@synthesize target;
@synthesize selector;
@synthesize userInfo;

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc {
    self.userInfo = nil;
//    target = nil;
//    selector = nil;
    [super dealloc];
}
@end

