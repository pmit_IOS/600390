//
//  ZSTSettingCell.m
//  F3
//
//  Created by LiZhenQu on 13-12-11.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTSettingCell.h"

@implementation ZSTSettingCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void) initWithData
{
    UIImageView *rightImgView = [[UIImageView alloc] initWithFrame:CGRectMake(285, (self.frame.size.height - 12)/2.0+5, 7, 12)];
    rightImgView.image = ZSTModuleImage(@"module_setting_icon_right.png");
    [self addSubview:rightImgView];
    [rightImgView release];
}

- (void)dealloc
{
    [super dealloc];
}

@end
