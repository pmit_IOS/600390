//
//  ZSTAMSettingPassVC.h
//  F3
//
//  Created by P&M on 15/8/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTAMSettingPassVC : UIViewController <UITextFieldDelegate, ZSTF3EngineDelegate>

@property (strong, nonatomic) UIView *firstView;
@property (strong, nonatomic) UITextField *passTF;
@property (copy,nonatomic) NSString *mobilePhone;
@property (strong, nonatomic) ZSTF3Engine *engine;
@property (copy,nonatomic) NSString *verifyCode;

@end
