//
//  ZSTModuleManager.m
//  F3
//
//  Created by luobin on 7/12/12.
//  Copyright (c) 2012 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTModuleManager.h"
#import "ZSTModule.h"
#import "ElementParser.h"
#import "ZSTShell.h"
@interface ZSTModuleManager()

@property (retain) NSMutableDictionary *modules;

- (void)loadModules;

@end

@implementation ZSTModuleManager

@synthesize modules;

SINGLETON_IMPLEMENTATION(ZSTModuleManager)

- (id)init
{
    self = [super init];
    if (self) {
        self.modules = [NSMutableDictionary dictionary];
        [self loadModules];
    }
    return self;
}

- (void)dealloc
{
    self.modules = nil;
    [super dealloc];
}


- (void)compareModuleId:(NSInteger)moduleId
{
    NSString *moduleContent = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];

    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *moduleRoot = [parser parseXML:moduleContent];

    NSArray *items = [moduleRoot selectElements:@"Modules Item"];

    for (int i = 0 ; i< [items count];i ++) {
        Element *item = [items objectAtIndex:i];
        NSInteger moduleModuleId = [[[item selectElement:@"ModuleID"] contentsNumber] integerValue];
        if (moduleId == moduleModuleId) {
            NSString *entryPoint = [[item selectElement:@"ModuleEnter"] contentsText];
            if (entryPoint) {
                Class entryPointClass = NSClassFromString(entryPoint);
                if (entryPointClass) {
                    ZSTModule *module = [ZSTModule pluginWithElement:item];
                    NSInteger moduleID = module.ID;                    
                    [modules setObject:module forKey:@(moduleID)];
                }
            }
        }
    }
    
    [parser release];
}

- (void)loadModules
{
    NSString *content = [NSString stringWithContentsOfFile:
                         [[NSBundle mainBundle] pathForResource:
                          [NSString stringWithFormat:@"shell_config"] ofType:@"xml"]
                                                  encoding:NSUTF8StringEncoding error:nil];

    
    ElementParser *parser1 = [[ElementParser alloc] init];
    DocumentRoot *shellRoot = [parser1 parseXML:content];

    NSArray *shellItems = [shellRoot selectElements:@"ModuleList ModuleID"];

    for(Element *item in shellItems) {
        NSInteger shellModuleId = [[[item selectElement:@"ModuleID"] contentsNumber] integerValue];
        [self compareModuleId:shellModuleId];
        
        if (shellModuleId == shellcID) {
            shellModuleId = shellcHomeID;
            [self compareModuleId:shellModuleId];

        }else if (shellModuleId == shellbID)
        {
            shellModuleId = shellbHomeID;
            [self compareModuleId:shellModuleId];

        }else if (shellModuleId == shelleID)
        {
            shellModuleId = shelleHomeID;
            [self compareModuleId:shellModuleId];
            
        }else if (shellModuleId == shellfID)
        {
            shellModuleId = shellfHomeID;
            [self compareModuleId:shellModuleId];
            
        }
        else if (shellModuleId == shellgID)
        {
            shellModuleId = shellgHomeID;
            [self compareModuleId:shellModuleId];
        }
        else if (shellModuleId == shellpID)
        {
            shellModuleId = shellpHomeID;
            [self compareModuleId:shellModuleId];
        }
        else if (shellModuleId == shellsID)
        {
            shellModuleId = shellsHomeID;
            [self compareModuleId:shellModuleId];
        }
    }

    [parser1 release];

}

- (void)destoryModules
{
    [self.modules removeAllObjects];
}

- (ZSTModule *)findModule:(NSInteger)moduleID
{
    return [self.modules safeObjectForKey:[NSNumber numberWithInteger:moduleID]];
}

- (UIViewController *)launchModuleApplication:(NSInteger)moduleID withOptions:(NSDictionary *)launchOptions
{
    //7 12 -3 -8 38
    ZSTModule *module = [self findModule:moduleID];
    ZSTModuleApplication *application = [[ZSTModuleApplication alloc] initWithModule:module];
    UIViewController *root = [application launchWithOptions:launchOptions];
    [application release];
    return root;
}

- (UIView *)launchModuleView:(NSInteger)moduleID withOptions:(NSDictionary *)launchOptions
{
    ZSTModule *module = [self findModule:moduleID];
    ZSTModuleApplication *application = [[ZSTModuleApplication alloc] initWithModule:module];
    UIView *cover = [application launchViewWithOptions:launchOptions];
    [application release];
    return cover;
}


- (UIViewController *)launchModuleSetting:(NSInteger)moduleID withOptions:(NSDictionary *)launchOptions
{
    ZSTModule *module = [self findModule:moduleID];
    ZSTModuleApplication *application = [[ZSTModuleApplication alloc] initWithModule:module];
    UIViewController *settingViewController = [application moduleSettingViewController];
    [application release];
    return settingViewController;
}

@end
