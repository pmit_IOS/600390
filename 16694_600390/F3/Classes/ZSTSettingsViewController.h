//
//  Setview.h
//  Net_Information
//
//  Created by  on 11-6-5.
//  Copyright 2011 . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <CoreTelephony/CTCarrier.h>
#import "ZSTModuleBaseViewController.h"

@interface ZSTSettingItem : NSObject {
}

+(ZSTSettingItem *)itemWithTitle:(NSString *)title type:(NSString *)type;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, retain) NSString *type;
@end


@interface ZSTSettingsViewController : ZSTModuleBaseViewController <UITableViewDelegate,UITableViewDataSource, MFMessageComposeViewControllerDelegate,ZSTF3EngineDelegate>
{
	UITableView *_tableView;

	NSMutableArray *_tableViewDataArray;
    
    NSMutableArray *_settingItems;
    
    NSMutableArray *_urlSettingItems;
    
    NSMutableDictionary *_urlSettingItemDict;
	
	UIButton *button;
    
    NSString * _clientVersionUrl;
    UITextField *_phoneNumberTextField;
}

-(void) remove;

@end
