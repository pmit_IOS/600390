//
//  ZSTInformViewController.h
//  F3
//
//  Created by LiZhenQu on 14-10-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTMsgSettingTableViewCell.h"
#import "ZSTCustomSwitch.h"
#import "PMCustomSwitch.h"
#import "ZSTPersonalInfo.h"

@interface ZSTInformViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,MsgSettingCellDelegate,PMCustomSwitchDelegate,ZSTF3EngineDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, retain) IBOutlet UIView *phoneView;
//@property (nonatomic, strong) ZSTCustomSwitch *switchBtn;
@property (nonatomic,strong) PMCustomSwitch *switchBtn;

@property (nonatomic, retain) UserInfo *userInfo;

@property(nonatomic, retain) ZSTF3Engine *engine;

@end
