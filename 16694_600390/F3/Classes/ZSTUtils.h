//
//  ZSTUtils.h
//  F3
//
//  Created by luo bin on 11/9/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZSTUtils : NSObject {
    
}

//提示
+ (void)showAlertTitle: (NSString *) title message: (NSString *) message;

//播放声音
+ (void)playAlertSound:(NSString *)name withExtension:extension;

//判断是否为图片路径
+ (BOOL)isImagePath:(NSString *)path;

//判断是否为html路径
+ (BOOL)isHTMLPath:(NSString *)path;

//判断是否为图片url
+ (BOOL)isImageURL:(NSURL *)url;

//将document文件夹的文件路径 "/程序路径/Document/自定义目录/文件名" 转换成 "documents://自定义目录/文件名"
+ (NSString *)makeDocumentRelativePath:(NSString *)absolutePath;

//获取当前日期字符串形式
+ (NSString *) getCurrentDate;

//转换日期格式成yyyy-MM-dd HH:mm:ss的标准格式
//etc. 2011-2-2 12:02:03  --->  2011-02-02 12:02:03
+ (NSDate *)convertToDate:(NSString *)string;

//格式化日期
+ (NSString *)formatDate:(NSDate *)date format:(NSString *)format;

//把对象转换成数组可存储形式
+ (id)objectForArray:(id)val;

//判断date1和date2是否在同一天
+ (BOOL)isInOneDate:(NSDate *)date1 date2:(NSDate *)date2;

//将16进制颜色转换成UIColor
+ (UIColor *)colorFromHexColor:(NSString *)hexColor;

+ (CGSize)supportHighestResolution;

//通过电话号码获取名称
+ (NSString *)getNameWithPhoneNumber:(NSString *)phoneNumber;

//播放F3铃声
+ (void)playNMSAlert;

//封面图片文件夹
+ (NSString *)pathForCover;

//日志文件夹
+ (NSString *)pathForLogs;

//收件文件夹
+ (NSString *)pathForInbox;

//发件文件夹
+ (NSString *)pathForOutbox;

//临时发件文件夹
+ (NSString *)pathForTempFinderOfOutbox;

//下载缓存地址
+ (NSString *)pathForDownloadCache;

//发件箱发送文件夹
+ (NSString *)pathForSendFileOfOutbox:(NSInteger)ID;

+ (NSString *)pathForECECC;

+ (NSString *)httpSever:(NSString *)url;

+ (UIColor *)getBGColor;

+ (UIColor *)getNavigationTintColor;

+ (UIColor *)getNavigationTextColor;

+ (NSString*)convertToTime:(NSString*)message;

+ (NSString *) getECECCID;

+ (UIButton *)customButtonTitle:(NSString *)title nImage:(UIImage *)nImage pImage:(UIImage *)pImage titleFontSize:(float)size conerRsdius:(float)rsdius;

+ (UIView *)logoView;

+ (UIImageView *)shellClogoView;

+ (UIView *)titleViewWithTitle:(NSString *)title;

+ (NSString *)imageDateString;

+ (NSString *)audioDateString;

+ (NSString *)videoDateString;

+ (NSInteger)fileSize:(NSString *)path;

+ (NSString *)setNeedReloadMessageType;//信息分类开启应用同步时间

+ (BOOL)messageTypeIsUpdate;//信息分类开启同步后不再重复同步


//转换时间格式成HH:mm:ss的标准格式
//etc. 12:2:3  --->  12:02:03
+ (NSString *) convertToStandardTime:(NSString *)timeString;

/*先将除MD5Verify以外的其他参数和参数值按字典升序排列组成字符串(对于Attachment只是记录名字，不处理内容)，再将LoginPassword跟在后面，对获得的字符串
 ”LoginMsisdn=13899999999&ToUserID=18910692132&Subject=你好&Content=测试&OriMSGID=&Attachment=AlbumSample_06.jpg& Attachment=08.jpg&LoginPassword=abc123iu”，进行32 位算法的MD5 加密后，转化为大写。
 */
+ (NSString *)md5Signature:(NSDictionary *)params;

+(NSDateComponents *) dateComponents:(NSDate *)date;

//将参数转化为小写
+ (NSDictionary *)lowerKeys:(NSDictionary *)params;

+ (BOOL)plistAddURLName:(NSString *)urlName URLSchemes:(NSString *)urlScheme;

@end
