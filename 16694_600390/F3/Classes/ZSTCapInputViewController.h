//
//  ZSTCapInputViewController.h
//  F3
//
//  Created by pmit on 15/8/13.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTUtils.h"

@interface ZSTCapInputViewController : UIViewController <UITextFieldDelegate>

@property (strong,nonatomic) ZSTF3Engine *engine;
@property (strong,nonatomic) NSString *mobilePhone;

@end
