//
//  HelpViewController.m
//  F3
//
//  Created by xuhuijun on 11-12-22.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTHelpViewController.h"
#import "UIImage+cut.h"

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

NSString *const HelpViewControllerDidDismissNotification = @"HelpViewControllerDidDismissNotification";

@implementation ZSTHelpViewController

@synthesize delegate = _delegate;

SINGLETON_IMPLEMENTATION(ZSTHelpViewController)

- (void)dealloc
{
    [super dealloc];
}

-(id)init{
    self = [super init];
    if (self) {
        _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        _window.windowLevel = UIWindowLevelNormal;
        _window.hidden = YES;
    }
    return self;
}

- (void)showWithImages:(NSArray *)images
{
    UIScrollView *helpScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 480+(iPhone5?88:0))];
    helpScrollView.contentSize = CGSizeMake(320*([images count] + 1), 480+(iPhone5?88:0));
    helpScrollView.backgroundColor = [UIColor clearColor];
    helpScrollView.delegate = self;
    helpScrollView.alwaysBounceHorizontal = YES;
    helpScrollView.showsHorizontalScrollIndicator = NO;
    
    if (NSClassFromString(@"UITapGestureRecognizer")) {
        
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapFrom:)];
        if ([recognizer respondsToSelector:@selector(numberOfTapsRequired)]) {
            recognizer.numberOfTapsRequired = 1;
        }
        
        [helpScrollView addGestureRecognizer:recognizer];
        [recognizer release];
        
        if ([recognizer respondsToSelector:@selector(requireGestureRecognizerToFail:)]) {
            UITapGestureRecognizer *doubleRecognizer = [[UITapGestureRecognizer alloc] init];
            
            if ([doubleRecognizer respondsToSelector:@selector(numberOfTapsRequired)]) {
                doubleRecognizer.numberOfTapsRequired = 2; // 双击
            }
            [helpScrollView addGestureRecognizer:doubleRecognizer];
            [doubleRecognizer release];
            
            //没有触发双击手势时才触发单击手势
            [recognizer requireGestureRecognizerToFail:doubleRecognizer];
        }
    }
    
    //设置图片
    for (int i =0; i<[images count]; i++) {
        
        UIImage *sourceImage = [UIImage imageWithContentsOfFile:[images objectAtIndex:i]];
        UIImage *image = [sourceImage cutInFrame:CGRectMake(0, 0, 320, 480+(iPhone5?88:0)) cutMode:ImageCutFromCenter];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(320*i, 0, 320, 480+(iPhone5?88:0))];
        imageView.image = image; 
        [helpScrollView addSubview:imageView];
        [imageView release];
    }
    
    helpScrollView.pagingEnabled = YES;
    helpScrollView.alpha = 0;
    [_window addSubview:helpScrollView];
    [helpScrollView release];
    _window.windowLevel = UIWindowLevelStatusBar;
    _window.hidden = NO;
    
    [UIView beginAnimations:@"helpScrollViewAnimation" context:nil];
    [UIView setAnimationDuration:0.5];
    helpScrollView.alpha = 1;
    [UIView commitAnimations];
    
}

- (void)handleSingleTapFrom:(UITapGestureRecognizer *)recognizer
{
    UIScrollView *view = (UIScrollView *)recognizer.view;
    if (view.contentOffset.x < (view.contentSize.width - 2 * 320)) {
        CGPoint scrollViewContentOffSet = view.contentOffset;
        scrollViewContentOffSet.x = 320* ((view.contentOffset.x / view.bounds.size.width)+1);
        [view setContentOffset:scrollViewContentOffSet animated:YES];
    }else
    {
        [self dismissAnimated:YES];
    }
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x > (scrollView.contentSize.width - 2 * 320) + 65) {
        [self dismissAnimated:YES];
    }
}

- (void)dismissAnimated:(BOOL)animated
{
    NSArray *subviews = [_window subviews];
    if ([subviews count] > 0) {
        
        for (NSInteger i = [subviews count] -1;  i >= 0; i--) {
            UIView *subview = [subviews objectAtIndex:i];
            if ([subview isKindOfClass:[UIScrollView class]]) {
                UIScrollView *view = (UIScrollView *)subview;
                if (animated) {
                    CGPoint scrollViewContentOffSet = view.contentOffset;
                    scrollViewContentOffSet.x = 320* ((view.contentOffset.x / view.bounds.size.width)+1);
                    [view setContentOffset:scrollViewContentOffSet animated:YES];
                    view.scrollEnabled = NO;
                    
                } else {
                    [view removeFromSuperview];
                    _window.windowLevel = UIWindowLevelNormal;
                    _window.hidden = YES;
                    [[NSNotificationCenter defaultCenter] postNotificationName:HelpViewControllerDidDismissNotification object:nil];
                    if ([_delegate respondsToSelector:@selector(helpViewControllerDidDismiss:)]) {
                        [_delegate helpViewControllerDidDismiss:self];
                    }
                }
                
                break;
            }
        }
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView; // called when setContentOffset/scrollRectVisible:animated: finishes. not called if not animating
{
    if (scrollView.contentOffset.x > (scrollView.contentSize.width - 2 * 320)) {
        [scrollView removeFromSuperview];
        _window.windowLevel = UIWindowLevelNormal;
        _window.hidden = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:HelpViewControllerDidDismissNotification object:nil];
        if ([_delegate respondsToSelector:@selector(helpViewControllerDidDismiss:)]) {
            [_delegate helpViewControllerDidDismiss:self];
        }
    }
}


@end
