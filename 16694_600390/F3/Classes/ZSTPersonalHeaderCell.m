//
//  ZSTPersonalHeaherCell.m
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-18.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonalHeaderCell.h"

@implementation ZSTPersonalHeaderCell

- (void)awakeFromNib
{
//    self.avatarImgView.layer.borderWidth = 1;
//    self.avatarImgView.layer.cornerRadius = self.avatarImgView.frame.size.width / 2;
//    self.avatarImgView.layer.borderColor = [UIColor clearColor].CGColor;
//    self.avatarImgView.layer.masksToBounds = YES;
//
//    [self.avatarImgView setContentScaleFactor:[[UIScreen mainScreen] scale]];
//    self.avatarImgView.contentMode =  UIViewContentModeScaleAspectFill;
//    self.avatarImgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    self.avatarImgView.clipsToBounds  = YES;
    
//    self.avatarImgView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(244, 12, 32, 32)];
    self.avatarImgView = [[UIImageView alloc] initWithFrame:CGRectMake(244, 12, 32, 32)];
    [self.contentView addSubview:self.avatarImgView];
    [self.contentView bringSubviewToFront:self.avatarbtn];
    
    UIImageView *rightImgView = [[UIImageView alloc] initWithFrame:CGRectMake(285, (self.frame.size.height - 12)/2.0, 7, 12)];
    rightImgView.image = ZSTModuleImage(@"module_setting_icon_right.png");
    [self addSubview:rightImgView];
    [rightImgView release];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
