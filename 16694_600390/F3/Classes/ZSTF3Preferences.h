//
//  ZSTF3Preferences.h
//  
//
//  Created by Xie Wei on 11-6-20.
//  Copyright 2011年 e-linkway.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTLogUtil.h"
#import "ASIProgressDelegate.h"
#import "KeychainItemWrapper.h"

#import "SDWebFileManagerDelegate.h"
#import "SDWebFileDownloaderDelegate.h"

#define CONTENTSTEXT(xpath) [[[element selectElement:@xpath] contentsText] \
stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]

#define CONTENTSINT(xpath) [[[element selectElement:@xpath] \
contentsNumber]intValue]

#define CONTENTSUNSIGNEDLONGLONG(xpath) [[[element selectElement:@xpath] \
contentsNumber]unsignedLongLongValue]

#define CONTENTSBOOL(xpath) ([[[element selectElement:@xpath] contentsText] intValue]!=0)

#define PERSONAL_MSGTYPE @"0"
#define ALLMESSAGE_MSGTYPE @"-1"
#define PERSONAL_IMAGE @"-999999"

#define USERINFO_KEY_ID @"userInfo_key_id"
#define USERINFO_KEY_PROGRESSAMOUNT @"userInfo_key_progressAmount"

////运营商类型
//typedef enum _CarrierType
//{
//    CarrierType_ChinaMobile = 0,
//    CarrierType_ChinaUnicom = 1,
//    CarrierType_ChinaTelecom = 2,
//    CarrierType_Unkown = 9
//} CarrierType;

typedef enum {
    sinaWeibo_ShareType = 0, //新浪微博
    TWeibo_ShareType = 1,    //腾讯微博
    QQ_ShareType = 2,         //QQ
    WX_ShareType = 3         //微信
} ShareType;

//客户端状态
typedef enum _ClientStatus
{
    ClientStatus_Init = 0,                  //安装初始化
    ClientStatus_Sync = 1,                  //已同步信息，获得了虚拟手机号，尚未发送绑定短信(虚拟号绑定成功)
    ClientStatus_SMSRegister = 2,           //短信绑定状态，等待同步信息
    ClientStatus_ManualRegister = 3,        //同步失败，处于手动绑定绑定状态
    ClientStatus_Failure = 4,               //绑定失败，使用虚拟手机号
    ClientStatus_OK = 9,                    //绑定成功(获得真实手机号)
} ClientStatus;

//网络请求返回类型
typedef enum _Status
{
    Status_NO = 0,
    Status_OK = 1,
    Status_ConnectServerFailure = 3
} Status;

//消息分类设置类型
typedef enum _MsgTypeSetAction
{
    MsgTypeSetAction_NotShield = 0,
    MsgTypeSetAction_Shield = 1,
    MsgTypeSetAction_Remove = 2,
} MsgTypeSetAction;

//push 打开方式
typedef NS_ENUM(NSInteger, PushOpentype)
{
    PushOpentype_TabBar = 0,
    PushOpentype_ImageButton = 1,    
};


//绑定类型
typedef NS_ENUM(NSInteger, MCRegistType)
{
    MCRegistType_ForceRegister = 1,         //强制绑定
    MCRegistType_PromptRegister = 2,        //提示绑定
    MCRegistType_VirtualRegister = 3          //虚拟号绑定
};

//获取包类型
typedef NS_ENUM(NSInteger, NMSPackageType)
{
    NMSPackageType_normal = 2,         //普通图包
    NMSPackageType_noPicture = 5,      //无图包
    
};

extern NSString *const NotificationNMSKey;

@class MessageInfo;
@class MsgTypeInfo;
@class ASINetworkQueue;

/**
 *	@brief  偏好设置数据存储
 *
 */

@interface ZSTF3Preferences : NSObject <SDWebFileManagerDelegate,SDWebFileDownloaderDelegate>

SINGLETON_INTERFACE(ZSTF3Preferences)

+(NSString *) stringFromBundlePath: (NSString *) bundlePath;

/*存储用户的手机号*/
@property(assign) NSString *loginMsisdn;//这个号码当真实绑定失败时存放虚拟号
@property(copy) NSString *phoneNumber;//真实手机号
@property(assign) NSTimeInterval remainingTime;//手动绑定获得验证码的剩余时间

@property(retain)NSString *verficationCode;//验证码
@property(retain)NSString *UserId;
@property(retain)NSDictionary *avaterName;

@property(assign) BOOL smsShare;                //短信分享
@property(assign) BOOL WX_Friend;               //微信好友
@property(assign) BOOL WX_FriendsCircle;        //微信朋友圈
@property(assign) BOOL SinaWeiBoShare;          //新浪微博

@property(assign) NSInteger WXshareType;              //微信分享类型

/*存储用户的密码*/
@property(assign) NSString *loginPassword;

@property(assign) NSString * UID;

/*得到ECECCID号*/
@property(readonly) NSString *ECECCID;  

@property(readonly) NSString *imsi;

@property(readonly) NSString *platform;

@property(assign) NSInteger conId;

@property(assign) NSInteger regManualTime;

@property(readonly) NSInteger version;

@property(assign) BOOL isDownloading;

@property(assign) NSInteger BSVersion;

@property(assign) BOOL isFirstLogin;

@property(assign) BOOL hasUpdateClientParams;

@property(assign) BOOL isShowHelp;

@property(assign) NSString *pushToken;

@property(assign) PushOpentype pushOpentype;

@property(assign) BOOL isInPush;

@property(assign) NSInteger pushSelectIndex;

@property(retain,nonatomic) UIViewController *pushViewController;

@property(assign) NSInteger UpdateVersion;

@property(assign) NSInteger newVersion;

@property(assign) NSInteger shellId;

@property(assign) NSInteger snsaStates;

@property(assign) NSInteger CarrierType;

@property(retain,nonatomic) NSString *ParamValue;

/*得到第三方登录绑定状态*/
@property(retain) NSString *weixinOpenId;
@property(retain) NSString *qqOpenId;
@property(retain) NSString *sinaOpenId;


//############################################# 设置项信息 #########################################

@property(assign) BOOL sound;

@property(assign) BOOL shake;

@property(assign) BOOL cleanUp;

@property(assign) NSDate *cleanupDate;

@property(assign) BOOL reportState;

@property(assign) BOOL fullScreen;

@property(assign) NMSPackageType packageType;


//############################################# 服务器端配置信息 #########################################

/**
 *	@brief	客户端绑定方式：1强制绑定2提示绑定3虚拟号绑定
 */
@property(assign) MCRegistType MCRegistType;

/**
 *	@brief	客户端绑定前的提示信息
 */
@property(retain) NSString *MCRegistDescription;

/**
 *	@brief	客户端绑定方式改动时候的提示信息
 */
@property(retain) NSString *MCRegistChangeDescription;

/**
 *	@brief	IPPush链接开关，1为开启，0关闭
 */
@property(assign) BOOL IPPushFlag;

/**
 *	@brief	HTTP轮询规则，为空时关闭
 */
@property(retain) NSString *HTTPPollTimeRule;

/**
 *	@brief	拦截短信模块是否开启，1为开启，0关闭
 */
@property(assign) BOOL SMSHookFlag; 

/**
 *	@brief	F3中央服务器域名或IP地址，检查客户端更新使用
 */
@property(retain) NSString *centralServer;

/**
 *	@brief	F3中央服务器端口号
 */
@property(assign) NSInteger centralServerPort;

/**
 *	@brief	HTTP接口地址域名或服务器IP
 */
@property(retain) NSString *httpServer;

/**
 *	@brief	HTTP接口地址端口号
 */
@property(assign) NSInteger httpServerPort;

/**
 *	@brief	UDP接口地址域名或服务器IP
 */
@property(retain) NSString *udpServer;

/**
 *	@brief	UDP接口地址域名或服务器端口
 */
@property(assign) NSInteger udpServerPort;

/**
 *	@brief	移动短信绑定长号码
 */
@property(retain) NSString *RegToicpCMCC;

/**
 *	@brief	联通短信绑定长号码
 */
@property(retain) NSString *RegToicpUNICOM;

/**
 *	@brief	电信短信绑定长号码
 */
@property(retain) NSString *RegToicpTELECOM;

/**
 *	@brief	加密数据包的解密密钥
 */
@property(retain) NSString *msgKey;

/**
 *	@brief	新浪微博帐号
 */
@property(retain) NSString *SinaWeiBo;

/**
 *	@brief	腾讯微博帐号
 */
@property(retain) NSString *TWeiBo;

/**
 *	@brief	qq帐号
 */
@property(retain) NSString *QQ;

/**
 *	@brief	微信帐号
 */
@property(retain) NSString *WeiXin;

/**
 *  @brief 是否切换账号
 */

@property (assign) BOOL isChange;
@property (assign,nonatomic) BOOL isSNSABlack;

@property (copy,nonatomic) NSString *qqKey;
@property (copy,nonatomic) NSString *qqSecret;
@property (copy,nonatomic) NSString *wxKey;
@property (copy,nonatomic) NSString *wxSecret;
@property (copy,nonatomic) NSString *weiboKey;
@property (copy,nonatomic) NSString *weiboSecret;
@property (copy,nonatomic) NSString *weiboRedirect;



-(void)synchronize;

-(void) deleteSysLogsBeforeDate:(NSDate *)compareDate;

//从文件系统删除临时附件
-(void) deleteAttachmentFromFileManagerByPath:(NSString *)path;

@end
