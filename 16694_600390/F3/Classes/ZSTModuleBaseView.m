//
//  ZSTModuleBaseView.m
//  F3
//
//  Created by xuhuijun on 13-1-16.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTModuleBaseView.h"
#import "ZSTModuleAddition.h"

@implementation ZSTModuleBaseView

@synthesize application;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UIView *)rootView
{
    return self;
}

- (void)dealloc
{
    self.application = nil;
    [super dealloc];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
