//
//  ZSTAccountViewController.m
//  F3
//
//  Created by LiZhenQu on 14-10-27.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTAccountViewController.h"
#import "ZSTUtils.h"
#import "ZSTMineViewController.h"
#import "ZSTSettingCell.h"
#import "ZSTF3RegisterViewController.h"

#define  kZSTSettingItemType_Password               @"Password"
#define  kZSTSettingItemType_MainAccount            @"Register"

@interface ZSTAccountViewController ()

@end

@implementation ZSTAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"账号管理", nil)];
    
    [self refreshArray];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [_tableView reloadData];
}

- (void)refreshArray
{
    accountItems = [[NSMutableArray alloc] init];
    
    [accountItems addObject:[ZSTSettingItems itemWithTitle:NSLocalizedString(@"修改密码", nil) type:kZSTSettingItemType_Password]];
    
    NSString *GP_Setting_Items = @"Password,Register";
    
    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
    settingItems = (NSMutableArray *)[[GP_Setting_Items componentsSeparatedByString:@","] retain];
    
    if ([settingItems containsObject:kZSTSettingItemType_MainAccount]) {
        NSString *title = NSLocalizedString(@"更换手机号", nil);
        
        [accountItems addObject:[ZSTSettingItems itemWithTitle:title type:kZSTSettingItemType_MainAccount]];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return accountItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTSettingCell *cell = [[[ZSTSettingCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                          reuseIdentifier:nil] autorelease];
    cell.customBackgroundColor = [UIColor whiteColor];
    cell.customSeparatorStyle = UITableViewCellSeparatorStyleSingleLine;
    cell.cornerRadius = 4.0;
    [cell prepareForTableView:tableView indexPath:indexPath];
    ZSTSettingItems *item = [accountItems objectAtIndex:indexPath.row];
    cell.textLabel.text =  [NSString stringWithFormat:@"  %@",item.title];;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    if (indexPath.row == 0) {
        
         [cell initWithData];
        
        if (self.passwordStatus == 1) {
            
            UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(190+(IS_IOS_7?10:0), 15, 80, 21)];
            rightLabel.backgroundColor = RGBCOLOR(206, 18, 24);
            rightLabel.textColor = [UIColor whiteColor];
            rightLabel.font = [UIFont systemFontOfSize:11];
            rightLabel.text = @"未设置密码";
            rightLabel.textAlignment = NSTextAlignmentCenter;
            rightLabel.clipsToBounds = YES;
            rightLabel.layer.cornerRadius = 10;//设置那个圆角的有多圆
            rightLabel.layer.masksToBounds = YES;//设为NO去试试
            [cell.contentView addSubview:rightLabel];
            [rightLabel release];

        }
    }
   
    if ([item.type isEqualToString:kZSTSettingItemType_MainAccount])
    {
        UILabel *loginMsisdnLable = [[UILabel alloc] initWithFrame:CGRectMake(200, 15, 90, 20)];
        loginMsisdnLable.text = [NSString stringWithFormat:@"%@",[ZSTF3Preferences shared].loginMsisdn];
        loginMsisdnLable.textColor = RGBCOLOR(155, 155, 155);
        loginMsisdnLable.font = [UIFont systemFontOfSize:14];
        loginMsisdnLable.backgroundColor = [UIColor clearColor];
        loginMsisdnLable.textAlignment = NSTextAlignmentRight;
        [cell.contentView addSubview:loginMsisdnLable];
        [loginMsisdnLable release];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.f;
}

#pragma mark －－－－－－－－－－－－－－－－－－－－－UITableViewDataSource－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失
    
    if (indexPath.row == 0) {
        
        ZSTF3RegisterViewController *controller = [[ZSTF3RegisterViewController alloc] init];
        controller.type = 4;
        controller.phoneNum = [ZSTF3Preferences shared].loginMsisdn;
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
        
    } else if (indexPath.row == 1) {
        
        ZSTF3RegisterViewController *controller = [[ZSTF3RegisterViewController alloc] init];
        controller.type = 3;
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    self.tableView = nil;
    [super dealloc];
}

@end
