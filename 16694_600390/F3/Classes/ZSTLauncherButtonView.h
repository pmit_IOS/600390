//
//  ELButtonView.h
//  CustomButton
//
//  Created by Xie Wei、luobin on 11-6-29.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMRepairButton.h"

@class ZSTBadgeView;
@class ZSTLongPressButton;

@class ZSTLauncherButtonView;

@protocol ZSTLauncherButtonViewDelegate<NSObject>

@optional

- (void)launcherButtonDidLongPress:(ZSTLauncherButtonView *)launcherButton;

- (void)launcherButtonDidClick:(ZSTLauncherButtonView *)launcherButton;

- (void)launcherButtonDidClickDeleteButton:(ZSTLauncherButtonView *)launcherButton;

- (void)launcherButtonDidBeginEditing:(ZSTLauncherButtonView *)launcherButton;

- (void)launcherButtonDidEndEditing:(ZSTLauncherButtonView *)launcherButton;

@end

@interface ZSTLauncherButtonView : TKAsynImageView <HJManagedImageVDelegate>
{
    ZSTLongPressButton *_longPressBtn;
    UILabel *_nameLabel;
    ZSTBadgeView *_countView;
//    TKAsynImageView *_iconView;
    UIImageView *_iconView;
    
    BOOL _editing;
    BOOL _shouldDisapear;
    BOOL _inShaking;
    BOOL _canDelete;
    BOOL _isHorizontal;
    BOOL _largeIconVertical;
    BOOL _midIconVertical;
    
    id<ZSTLauncherButtonViewDelegate> _delegate;
    PMRepairButton *_deleteButton;
    PMRepairButton *_enableButton;
}

@property (nonatomic, assign, getter = isEnable) BOOL enable;
@property (nonatomic, assign, getter = isCanDelete) BOOL canDelete;
@property (nonatomic, readonly, getter = isEditing) BOOL editing;
@property (nonatomic, assign) BOOL isHorizontal;
@property (nonatomic) BOOL largeIconVertical;
@property (nonatomic) BOOL midIconVertical;
@property (nonatomic,retain) UIImageView *badgeNumIV;
@property (nonatomic,retain) UILabel *badgeLB;

@property (nonatomic, assign) id<ZSTLauncherButtonViewDelegate> delegate;

-(void)setEnable:(BOOL)enable animated:(BOOL)animated;

//设置图标和名字
-(void)setIcon:(NSString *)imageUrl;

//设置信息简介
- (void)setMessageIntroduce:(NSString *)introduce;

//设置未读信息数
-(void)setBadgeNumber:(NSInteger)count;

//开始编辑
-(void)beginEditingAnimated:(BOOL)animated;

//停止编辑
-(void)endEditingAnimated:(BOOL)animated;

@end

