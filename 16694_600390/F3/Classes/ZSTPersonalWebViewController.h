//
//  ZSTPersonalWebViewController.h
//  F3
//
//  Created by LiZhenQu on 14/11/12.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTPersonalWebViewController : UIViewController<UIWebViewDelegate>

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain)  NSString *url;

- (void)setURL:(NSString *)url;

@end
