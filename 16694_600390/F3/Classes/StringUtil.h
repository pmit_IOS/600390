//
//  StringUtil.h
//  TuanGo
//
//  Created by simon on 11-1-21.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString(extend) 

+(NSString*)trim:(NSString*)string;
-(BOOL)containString:(NSString*)string;

@end
