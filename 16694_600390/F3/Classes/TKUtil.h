
@interface TKUtil : NSObject {
    
}
// 获取POST UID
+ (NSString *)postUID;
//+ (NSString *)postUIDWithGMT;

+(UIImage *)imageNamed:(NSString *)name;

// 去除前后空格
+ (NSString *)trim:(NSString *)str;

+ (NSString *)buildURL:(NSString *)baseURL params:(NSDictionary *)params;

+ (id)wrapNilObject:(id)obj;

// md5 转换
+ (NSString *)md5:(NSString *)str;

+ (NSString *)md5Data:(NSData *)data;

+ (NSInteger)strLength:(NSString*)strtemp;
+ (UIView *)bubbleView:(NSString *)text from:(BOOL)fromSelf;

// 两点间的距离
+ (double)distanceBetweenPoint:(CGPoint)fromPoint withPoint:(CGPoint)toPoint;

+ (double)distanceBetweenFirstX:(double)x1
                         firstY:(double)y1
                        secondX:(double)x2
                        secondY:(double)y2;

// string must be Interval string
+ (NSString *)timeFrom1970ToString:(NSString *)string
                            formot:(NSString *)format;

+ (NSString *)timeIntervalSince1970:(double)interval
                             formot:(NSString *)format;
// 格式化为.4f
+ (double)nowIntervalSince1970;

+ (NSString*)timeIntervalSince1970:(double)timeInterval;

#define degreesToRadians(x) (M_PI * x / 180.0)

// return NIL when has error
+ (UIColor *)string2Color:(NSString *)colorStr; // ffffff05 : 白色半透明
+ (UIColor *)string2Color:(NSString *)colorStr
             defaultColor:(UIColor *)color; // default color when colorStr style error..

@end

@interface TKUtil (SSToolkit)

///---------------------------------
/// @name Drawing Rounded Rectangles
///---------------------------------
extern void SSDrawRoundedRect(CGContextRef context, CGRect rect, CGFloat cornerRadius);
@end

#ifndef TKColor
#define TKColor(r, g, b, al) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:al]
#endif
#ifndef TKexColor
#define TKexColor(rgbal) [TKUtil string2Color:rgbal]
#endif


