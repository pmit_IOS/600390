//
//  HHCommentController.m
//  HHelloCat
//
//  Created by luo bin on 12-3-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTHHSinaShareController.h"
#import "TKUIUtil.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/TencentOAuthObject.h>
//#import "SinaWeibo.h"
#import <WeiboSDK.h>
#import "ZSTModuleAddition.h"
#import "ZSTF3ClientAppDelegate.h"

#define kMaxCommentLength 140

@implementation ZSTHHSinaShareController

@synthesize textView;
@synthesize countLabel;
@synthesize backgroundView;
@synthesize delegate;
@synthesize shareString;
@synthesize isLogin;
@synthesize shareImage;
@synthesize shareImageView;
@synthesize isHaveImage;
@synthesize shareType;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [DSBezelActivityView removeViewAnimated:NO];
    self.backgroundView = nil;
    self.countLabel = nil;
    self.textView = nil;
    self.shareImage = nil;
    self.shareImageView = nil;
    [super dealloc];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.isLogin = NO;
    self.isHaveImage = self.shareImage ? YES : NO;
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];

    self.view.backgroundColor = [UIColor whiteColor];

    self.navigationItem.leftBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (dismiss)];
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"分享", @"") target:self selector:@selector (sendButtonAction:)];

    self.backgroundView = [[[UIImageView alloc] initWithFrame:CGRectMake(0.0, 148.0, 320.0, 40.0)] autorelease];//24
    self.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.backgroundView.backgroundColor = [UIColor clearColor];
    self.backgroundView.image = ZSTModuleImage(@"bottom_tool_bar.png");
    self.backgroundView.userInteractionEnabled = YES;
    CALayer *layer = self.backgroundView.layer;
    layer.shadowOffset = CGSizeMake(0, 3); 
    layer.shadowRadius = 5.0; 
    layer.shadowColor = [UIColor blackColor].CGColor; 
    layer.shadowOpacity = 0.8; 
    
    self.textView = [[[TKPlaceHolderTextView alloc] initWithFrame:CGRectMake(5, 15, 310.0, 170.0)] autorelease];//25
    self.textView.font = [UIFont systemFontOfSize:16];
    self.textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.textView.clearsContextBeforeDrawing = YES;
    self.textView.clipsToBounds = YES;
    self.textView.contentMode = UIViewContentModeScaleToFill;
    self.textView.text = @"";
    self.textView.delegate = self;
    self.textView.textAlignment = NSTextAlignmentLeft;

    if ([self.shareString isEqualToString:@""] || [self.shareString length] == 0 || self.shareString == nil) {
        [self.textView setPlaceholder:NSLocalizedString(@"说点什么", @"")];
    }else{
        self.textView.text = self.shareString;
    }
    [self.textView becomeFirstResponder];
    self.textView.selectedRange = NSMakeRange(0, 0);
    
    countLabel = [[UILabel alloc] initWithFrame:CGRectMake(260.0, 10.0, 48.0, 21.0)];//26
    countLabel.adjustsFontSizeToFitWidth = YES;
    countLabel.autoresizesSubviews = YES;
    countLabel.backgroundColor = [UIColor clearColor];
    countLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    countLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    countLabel.contentMode = UIViewContentModeLeft;
    countLabel.font = [UIFont systemFontOfSize:14];
    countLabel.minimumScaleFactor = 10.000;
    countLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    countLabel.text = @"0/140";
    countLabel.textAlignment = NSTextAlignmentRight;
    
    [self.view addSubview:self.textView];
    [self.backgroundView addSubview:countLabel];
    [self.view addSubview:self.backgroundView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendWeiboSuccess:) name:@"sendWeiboSuccess" object:nil];
}

- (void) keyboardWasShown:(NSNotification *) notif{ 
    NSDictionary *info = [notif userInfo]; 
    NSValue *value = [info objectForKey:UIKeyboardFrameEndUserInfoKey]; 
    CGSize keyboardSize = [value CGRectValue].size; 
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.backgroundView.frame = CGRectMake(0.0, 148.0 - (keyboardSize.height-216), 320.0, 40.0);//24
    self.textView.frame = CGRectMake(5, 15, 310.0, 170 - (keyboardSize.height-216));//25
    [UIView commitAnimations];
}


- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidUnload
{
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//////////////////////////////////////////////////////////////////////////////////////

#pragma mark - ButtonAction

- (void)deleteShareImageAction:(UIButton *)sender
{
    self.isHaveImage = NO;
    [shareImageView removeFromSuperview];
    [sender removeFromSuperview];
}

- (void)sendButtonAction:(id)sender
{
    [textView resignFirstResponder];

    if (self.shareType == sinaWeibo_ShareType) {
        
//        if ([self.sinaWeiboEngine isAuthValid]) {
//            self.isLogin = NO;
//            [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"发送中" , nil)];
//            if (self.isHaveImage) {
//                [self.sinaWeiboEngine sendWeiBoWithText:textView.text image:self.shareImage];
//            }else {
//                [self.sinaWeiboEngine sendWeiBoWithText:textView.text image:nil];
//            }
//        }else {
//            [self.sinaWeiboEngine logIn];
//        }
        ZSTF3ClientAppDelegate *myDelegate = (ZSTF3ClientAppDelegate*)[[UIApplication sharedApplication] delegate];
        WBAuthorizeRequest *authRequest = [WBAuthorizeRequest request];
        authRequest.redirectURI = [ZSTF3Preferences shared].weiboRedirect;
        authRequest.scope = @"all";
        WBMessageObject *message = [WBMessageObject message];
        message.text = textView.text;
        WBSendMessageToWeiboRequest *request = [WBSendMessageToWeiboRequest requestWithMessage:message authInfo:authRequest access_token:myDelegate.wbtoken];
        request.userInfo = @{@"ShareMessageFrom": @"SendMessageToWeiboViewController",
                             @"Other_Info_1": [NSNumber numberWithInt:123],
                             @"Other_Info_2": @[@"obj1", @"obj2"],
                             @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
        [WeiboSDK sendRequest:request];
        
    }else if (self.shareType == TWeibo_ShareType){
        
        if ([self.tencentWeiboEngine isAuthorizeExpired]) {
            [self.tencentWeiboEngine logInWithDelegate:self
                                             onSuccess:@selector(tencentLoginSuccessCallBack)
                                             onFailure:@selector(tencentLoginFailureCallBack:)];
            [self.tencentWeiboEngine setRootViewController:self];
        }else{
            [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"发送中" , nil)];
            NSData *dataImage = nil;
            if (self.shareImage) {
                dataImage = UIImageJPEGRepresentation(self.shareImage, 1.0);
            }
            
            [self.tencentWeiboEngine postPictureTweetWithFormat:@"json"
                                                        content:textView.text
                                                       clientIP:nil
                                                            pic:dataImage
                                                 compatibleFlag:@"0"
                                                      longitude:nil
                                                    andLatitude:nil
                                                    parReserved:nil
                                                       delegate:self
                                                      onSuccess:@selector(postPictureTweetSuccessCallBack:)
                                                      onFailure:@selector(postPictureTweetFailureCallBack:)];
        }
    }else if (self.shareType == QQ_ShareType){
//        
//        if ([self.QQEngine isSessionValid]) {
//            [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"发送中" , nil)];
//            TCAddShareDic *params = [TCAddShareDic dictionary];
//            params.paramTitle = @"腾讯内部addShare接口测试";
//            params.paramComment = @"风云乔帮主";
//            params.paramSummary =  @"乔布斯被认为是计算机与娱乐业界的标志性人物，同时人们也把他视作麦金塔计算机、iPod、iTunes、iPad、iPhone等知名数字产品的缔造者，这些风靡全球亿万人的电子产品，深刻地改变了现代通讯、娱乐乃至生活的方式。";
//            params.paramImages = @"http://img1.gtimg.com/tech/pics/hv1/95/153/847/55115285.jpg";
//            params.paramUrl = @"http://www.qq.com";
//            
//            if (![self.QQEngine addShareWithParams:params]) {
//                [TKUIUtil hiddenHUD];
//                [self dismissViewControllerAnimated:YES completion:nil];
//                
//                if ([self.delegate respondsToSelector:@selector(shareDidFail: options:)]) {
//                    [self.delegate shareDidFail:self options:NSLocalizedString(@"QZone分享失败", nil)];
//                }
//            }else{
//                [TKUIUtil hiddenHUD];
//                [self dismissViewControllerAnimated:YES completion:nil];
//                if ([self.delegate respondsToSelector:@selector(shareDidFinish: options:)]) {
//                    [self.delegate shareDidFinish:self options:NSLocalizedString(@"QZone分享成功", nil)];
//                }
//            }
//            
//        }else{
//          NSArray * permissions = [NSArray arrayWithObjects:
//                             kOPEN_PERMISSION_GET_USER_INFO,
//                             kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
//                             kOPEN_PERMISSION_ADD_ALBUM,
//                             kOPEN_PERMISSION_ADD_IDOL,
//                             kOPEN_PERMISSION_ADD_ONE_BLOG,
//                             kOPEN_PERMISSION_ADD_PIC_T,
//                             kOPEN_PERMISSION_ADD_SHARE,
//                             kOPEN_PERMISSION_ADD_TOPIC,
//                             kOPEN_PERMISSION_CHECK_PAGE_FANS,
//                             kOPEN_PERMISSION_DEL_IDOL,
//                             kOPEN_PERMISSION_DEL_T,
//                             kOPEN_PERMISSION_GET_FANSLIST,
//                             kOPEN_PERMISSION_GET_IDOLLIST,
//                             kOPEN_PERMISSION_GET_INFO,
//                             kOPEN_PERMISSION_GET_OTHER_INFO,
//                             kOPEN_PERMISSION_GET_REPOST_LIST,
//                             kOPEN_PERMISSION_LIST_ALBUM,
//                             kOPEN_PERMISSION_UPLOAD_PIC,
//                             kOPEN_PERMISSION_GET_VIP_INFO,
//                             kOPEN_PERMISSION_GET_VIP_RICH_INFO,
//                             kOPEN_PERMISSION_GET_INTIMATE_FRIENDS_WEIBO,
//                             kOPEN_PERMISSION_MATCH_NICK_TIPS_WEIBO,
//                             nil] ;
//            [self.QQEngine authorize:permissions inSafari:NO];
//            [permissions release];
//
//        }
//        
    }
}

//////////////////////////////////////////////////////////////////////////////////////

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)theTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *comment = [theTextView.text stringByReplacingCharactersInRange:range withString:text];
    NSUInteger length = [comment length];
    
    UIButton *sendButton = (UIButton *)self.navigationItem.rightBarButtonItem.customView;
    sendButton.enabled = (length != 0);
    
    if (length <= kMaxCommentLength) {
        self.countLabel.text = [NSString stringWithFormat:@"%@/%d", @(length), kMaxCommentLength];
        return YES;
    } else {
        return NO;
    }
}

#pragma -sinaWeiboEngineDelegate---

- (void)storeAuthData:(SinaWeibo *)sinaWeibo
{
    NSDictionary *authData = [NSDictionary dictionaryWithObjectsAndKeys:
                              sinaWeibo.accessToken, @"AccessTokenKey",
                              sinaWeibo.expirationDate, @"ExpirationDateKey",
                              sinaWeibo.userID, @"UserIDKey",
                              sinaWeibo.refreshToken, @"refresh_token", nil];
    [[NSUserDefaults standardUserDefaults] setObject:authData forKey:@"SinaWeiboAuthData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaWeibo
{
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"发送中" , nil)];
    [self storeAuthData:sinaWeibo];
//    [self.sinaWeiboEngine getUserInfo:sinaWeibo.userID];
        if (self.isHaveImage) {
        [self.sinaWeiboEngine sendWeiBoWithText:textView.text image:self.shareImage];
    }else {
        [self.sinaWeiboEngine sendWeiBoWithText:textView.text image:nil];
    }
}
- (void)sinaweibo:(SinaWeibo *)sinaweibo logInDidFailWithError:(NSError *)error
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"新浪微博授权失败" , nil) withImage:nil];
}

- (void)sendWeiBoInfoSucceed:(NSDictionary *)result
{
    [TKUIUtil hiddenHUD];
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([self.delegate respondsToSelector:@selector(shareDidFinish: options:)]) {
        [self.delegate shareDidFinish:self options:NSLocalizedString(@"新浪微博分享成功", nil)];
    }
}
- (void)sendWeiBoInfoFailured
{
    [TKUIUtil hiddenHUD];
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([self.delegate respondsToSelector:@selector(shareDidFinish: options:)]) {
        [self.delegate shareDidFinish:self options:NSLocalizedString(@"新浪微博不允许我们下手太快哦", nil)];
    }
}

- (void)getUserInfoSucceed:(NSDictionary *)result
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"新浪用户信息获取成功" , nil) withImage:nil];
}

- (void)getUserInfoFailured
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"新浪用户信息获取失败" , nil) withImage:nil];
}


#pragma mark - TencentWeibo Delegate

- (void)tencentLoginSuccessCallBack
{
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"发送中" , nil)];
    NSData *dataImage = nil;
    if (self.shareImage) {
        dataImage = UIImageJPEGRepresentation(self.shareImage, 1.0);
    }
    
    [self.tencentWeiboEngine postPictureTweetWithFormat:@"json"
                                                content:textView.text
                                               clientIP:nil
                                                    pic:dataImage
                                         compatibleFlag:@"0"
                                              longitude:nil
                                            andLatitude:nil
                                            parReserved:nil
                                               delegate:self
                                              onSuccess:@selector(postPictureTweetSuccessCallBack:)
                                              onFailure:@selector(postPictureTweetFailureCallBack:)];
    
}

- (void)tencentLoginFailureCallBack:(id)result
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"腾讯微博授权失败", nil) withImage:nil];
}

- (void)postPictureTweetSuccessCallBack:(id)result
{
    [TKUIUtil hiddenHUD];
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([self.delegate respondsToSelector:@selector(shareDidFinish: options:)]) {
        [self.delegate shareDidFinish:self options:NSLocalizedString(@"腾讯微博分享成功", nil)];
    }
}

- (void)postPictureTweetFailureCallBack:(id)result
{
    [TKUIUtil hiddenHUD];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([self.delegate respondsToSelector:@selector(shareDidFail: options:)]) {
        [self.delegate shareDidFail:self options:NSLocalizedString(@"腾讯微博分享失败", nil)];
    }
}


#pragma mark - QQ Delegate


- (void)tencentDidLogin {
    
//    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"发送中" , nil)];
//    TCAddShareDic *params = [TCAddShareDic dictionary];
//    params.paramTitle = @"腾讯内部addShare接口测试";
//    params.paramComment = @"风云乔帮主";
//    params.paramSummary =  @"乔布斯被认为是计算机与娱乐业界的标志性人物，同时人们也把他视作麦金塔计算机、iPod、iTunes、iPad、iPhone等知名数字产品的缔造者，这些风靡全球亿万人的电子产品，深刻地改变了现代通讯、娱乐乃至生活的方式。";
//    params.paramImages = @"http://img1.gtimg.com/tech/pics/hv1/95/153/847/55115285.jpg";
//    params.paramUrl = @"http://www.qq.com";
//    
//    if (![self.QQEngine addShareWithParams:params]) {
//        [TKUIUtil hiddenHUD];
//        [self dismissViewControllerAnimated:YES completion:nil];
//        
//        if ([self.delegate respondsToSelector:@selector(shareDidFail: options:)]) {
//            [self.delegate shareDidFail:self options:NSLocalizedString(@"QZone分享失败", nil)];
//        }
//    }else{
//        [TKUIUtil hiddenHUD];
//        [self dismissViewControllerAnimated:YES completion:nil];
//        if ([self.delegate respondsToSelector:@selector(shareDidFinish: options:)]) {
//            [self.delegate shareDidFinish:self options:NSLocalizedString(@"QZone分享成功", nil)];
//        }
//    }

}

- (void)tencentDidNotLogin:(BOOL)cancelled
{
    if (!cancelled) {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"QZone授权失败" , nil) withImage:nil];
    }
}

- (void)sendWeiboSuccess:(NSNotification *)notify
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([self.delegate respondsToSelector:@selector(shareDidFinish: options:)]) {
        [self.delegate shareDidFinish:self options:NSLocalizedString(@"新浪微博分享成功", nil)];
    }
}

@end
