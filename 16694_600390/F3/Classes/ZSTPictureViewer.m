//
//  ZSTPictureShow.m
//  F3
//
//  Created by xuhuijun on 12-3-31.
//  Copyright 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPictureViewer.h"
#import <QuartzCore/QuartzCore.h>
#import "TKUIUtil.h"

#define pic_save_tag 9
#define pic_forward_tag 8

@interface ZSTCustomView : UIView 
@end
@implementation ZSTCustomView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) 
    {
        self.alpha = 0.0;
    }
    return self;
}
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
    
    CGFloat colors[] = 
    {10.0 / 255.0, 10.0 / 255.0, 10.0 / 255.0, 0.8,
        10.0/ 255.0, 10.0 / 255.0, 10.0 / 255.0, 0.8};
    CGGradientRef gradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
    CGContextDrawLinearGradient(context, gradient, CGPointMake(rect.origin.x,rect.origin.y), CGPointMake(rect.origin.x,rect.size.height), (CGGradientDrawingOptions)NULL);
    CGGradientRelease(gradient);
    CGColorSpaceRelease(rgb);
}
@end


@implementation ZSTPictureViewer

@synthesize image = _image;

+ (ZSTPictureViewer *)showPicture:(UIImage *)image addedTo:(UIView *)view animated:(BOOL)animated
{
    ZSTPictureViewer *displayPicture = [[ZSTPictureViewer alloc] initWithView:view];
    displayPicture.image = image;
    [view addSubview:displayPicture];

    return [displayPicture autorelease];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_backgroudView release];_backgroudView = nil;
    [_scrollView release]; _scrollView = nil;
    [_imageView release]; _imageView = nil;

    [super dealloc];
}


#pragma mark -
#pragma mark Manual oritentation change

#define RADIANS(degrees) ((degrees * (float)M_PI) / 180.0f)

- (void)setTransformForCurrentOrientation:(BOOL)animated {
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    NSInteger degrees = 0;
    
    // Stay in sync with the superview
    if (self.superview) {
        self.bounds = self.superview.bounds;
        [self setNeedsDisplay];
    }
    
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        if (orientation == UIInterfaceOrientationLandscapeLeft) { degrees = -90; } 
        else { degrees = 90; }
        // Window coordinates differ!
        self.bounds = CGRectMake(0, 0, self.bounds.size.height, self.bounds.size.width);
    } else {
        if (orientation == UIInterfaceOrientationPortraitUpsideDown) { degrees = 180; } 
        else { degrees = 0; }
    }
    
    rotationTransform = CGAffineTransformMakeRotation(RADIANS(degrees));
    
    if (animated) {
        [UIView beginAnimations:nil context:nil];
    }
    [self setTransform:rotationTransform];
    if (animated) {
        [UIView commitAnimations];
    }
}

- (void)deviceOrientationDidChange:(NSNotification *)notification { 
    
    if (!self.superview) {
        return;
    }
    if ([self.superview isKindOfClass:[UIWindow class]]) {
        [self setTransformForCurrentOrientation:YES];
    } else {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        
        if (UIDeviceOrientationIsPortrait(orientation)) {
            self.frame = CGRectMake(0, 0, 320, 480);
 
        }else if (UIDeviceOrientationIsLandscape(orientation)){
            self.frame = CGRectMake(0, 0, 480, 320);
        }

        [self setNeedsDisplay];
    }
}

- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView {
    
    CGSize boundsSize = scroll.bounds.size;
    CGRect frameToCenter = rView.frame;
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else {
        frameToCenter.origin.x = 0;
    }
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else {
        frameToCenter.origin.y = 0;
    }
    return frameToCenter;
}

- (CGSize)redifineImageSize:(UIImage *)image
{
    
    CGSize imageSize = image.size;
    
    CGFloat width = imageSize.width;
    
    CGFloat height = imageSize.height;
    
    if (width <= self.frame.size.width) {
        
        // no need to compress.
        
        return image.size;
    }
    
    if (width == 0 || height == 0) {
        
        // void zero exception
        
        return image.size;
    }
    
    
    CGFloat scaleFactor = self.frame.size.width / width;
    
    CGFloat scaledWidth  = width * scaleFactor;
    
    CGFloat scaledHeight = height * scaleFactor;
    
    CGSize targetSize = CGSizeMake(scaledWidth, scaledHeight);
    return targetSize;
    
}

#pragma mark -
#pragma mark - layout


- (void)drawRect:(CGRect)rect
{  
    _backgroudView.frame = rect; 
    _scrollView.frame = rect;
    _scrollView.zoomScale = _scrollView.minimumZoomScale;
    self.image = _image;

    [UIView animateWithDuration:0.4 animations:^{
        _backgroudView.alpha = 0.8;
        [_backgroudView setNeedsDisplay];
    }]; 
    
    UIButton *forwardImageButton = (UIButton *)[self viewWithTag:pic_forward_tag];
    forwardImageButton.center = CGPointMake(CGRectGetMidX(rect)-50, CGRectGetMaxY(rect)-50);
    
    UIButton *saveImageButton = (UIButton *)[self viewWithTag:pic_save_tag];
    saveImageButton.center = CGPointMake(CGRectGetMidX(rect)+50, CGRectGetMaxY(rect)-50);
}

#pragma mark -
#pragma mark -


- (UIImage *)image
{
    return _image;
}

- (void)setImage:(UIImage *)image
{
    _image = [image retain];
    _imageView.image = image;
        
    CGSize imageSize = [self redifineImageSize:image];
    
    _imageView.frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
    _imageView.frame = [self centeredFrameForScrollView:_scrollView andUIView:_imageView];

}

#pragma mark -
#pragma mark -init －－－－

- (id)initWithWindow:(UIWindow *)window {
    return [self initWithView:window];
}


- (id)initWithView:(UIView *)view {
    if (!view) {
        [NSException raise:@"ZSTDisplayPictureViewIsNillException" 
                    format:@"The view used in the ZSTDisplayPicture initializer is nil."];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deviceOrientationDidChange:)
                                                 name:NotificationName_OrientationDidChange object:nil];
    
    id me = [self initWithFrame:view.bounds];
    
    if ([view isKindOfClass:[UIWindow class]]) {
        [self setTransformForCurrentOrientation:NO];
    }
    
    return me;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        
        _backgroudView = [[ZSTCustomView alloc] initWithFrame:frame];
        [self addSubview:_backgroudView];
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake( 0,  0 ,  self.frame.size.width, self.frame.size.height)];
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.delegate = self;
        _scrollView.scrollEnabled = YES;
        _scrollView.bouncesZoom = YES; 
        _scrollView.bounces = YES;
        _scrollView.contentMode = UIViewContentModeScaleAspectFit;
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [_scrollView addSubview:_imageView];
        [self addSubview:_scrollView];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidePicture:)];
        [_scrollView addGestureRecognizer:tapGesture]; 
        [tapGesture release];
    
        float minimumScale = 0.37;
        [_scrollView setMinimumZoomScale:minimumScale];
        [_scrollView setZoomScale:minimumScale];
        
        UIButton *saveImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        saveImageButton.frame = CGRectMake(0, 0, 32, 32);
        saveImageButton.tag = pic_save_tag;
        saveImageButton.showsTouchWhenHighlighted = YES;
        [saveImageButton setImage:[UIImage imageNamed:@"pic_save.png"] forState:UIControlStateNormal];
        [saveImageButton addTarget:self action:@selector(saveImage:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:saveImageButton];
        
        UIButton *forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        forwardButton.frame = CGRectMake(0, 0, 32, 32);
        forwardButton.tag = pic_forward_tag;
        forwardButton.showsTouchWhenHighlighted = YES;
        [forwardButton setImage:[UIImage imageNamed:@"pic_forward.png"] forState:UIControlStateNormal];
        [forwardButton addTarget:self action:@selector(forwardImage:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:forwardButton];
    }
    return self;
    
}

#pragma mark -
#pragma mark -selector－－－－－


- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{  
    [self removeFromSuperview];
}

- (void)hidePicture:(UITapGestureRecognizer* )sender
{    
    [UIView beginAnimations:@"hidePicture" context:nil];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    self.alpha = 0.0;
    [UIView commitAnimations];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error == nil)
    {
        [TKUIUtil alertInView:self withTitle:NSLocalizedString(@"图片保存成功!", nil) withImage:nil];
    }else{
        [TKUIUtil alertInView:self withTitle:NSLocalizedString(@"图片保存失败，请重试!", nil) withImage:nil];
    }
}

- (void)saveImage:(UIButton *)sender
{
    UIImageWriteToSavedPhotosAlbum(_image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);

}

- (void)forwardImage:(UIButton *)sender
{
    
}
#pragma mark UIScrollViewDelegate methods


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    
    return _imageView;
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    scrollView.bounces = NO;
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    _imageView.frame = [self centeredFrameForScrollView:scrollView andUIView:_imageView];

}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    scrollView.bounces = YES;
}



@end
