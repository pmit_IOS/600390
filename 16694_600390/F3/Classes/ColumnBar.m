//
//  ColumnBar.m
//  ColumnBarDemo
//
//  Created by chenfei on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
// 特例，默认加入头条 


#import "ColumnBar.h"
#import <QuartzCore/QuartzCore.h>
#import "ZSTUtils.h"

@implementation ColumnBar

@synthesize scrollView, selectedIndex, dataSource, delegate , moverImage ,topbarType,leftCapImage,rightCapImage,selectedColor,unSelectedColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        scrollView.contentInset = UIEdgeInsetsMake(0.0f, 15.0f, 0.0f, 15.0f);
        scrollView.backgroundColor = [UIColor clearColor];
        scrollView.delegate = self;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.scrollEnabled = YES;
        scrollView.layer.cornerRadius = frame.size.height/2;
        scrollView.backgroundColor = [UIColor clearColor];
        
        [self addSubview:scrollView];
        
        mover = [[UIImageView alloc] init];
        mover.clipsToBounds = YES;
        [scrollView addSubview:mover];
        
        leftCap = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, scrollView.contentInset.left, self.frame.size.height)];
        leftCap.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        [self addSubview:leftCap];
        
        rightCap = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-scrollView.contentInset.right, 0, scrollView.contentInset.right, self.frame.size.height)];
        rightCap.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [self addSubview:rightCap]; 
    }
    return self;
}

- (void)dealloc
{
    [scrollView release];
    [mover release];
    [leftCap release];
    [rightCap release];
    [super dealloc];
}

- (void)setTopbarType:(TopBarType)topbartype
{
    topbarType = topbartype;
    if (topbarType == TopBarType_SelectedBlock) {
        scrollView.layer.cornerRadius = self.frame.size.height/2;
        scrollView.contentInset = UIEdgeInsetsMake(0.0f, 15.0f, 0.0f, 15.0f);
    }else if(topbarType == TopBarType_Slider) {
        scrollView.contentInset = UIEdgeInsetsMake(0.0f, 0, 0.0f, 0);
        scrollView.layer.cornerRadius = 0;
        self.selectedColor = [ZSTUtils colorFromHexColor:@"#818181"];
        self.unSelectedColor = [ZSTUtils colorFromHexColor:@"#a5a5a5"];
    }
}

- (void)setMoverImage:(UIImage *)image
{
    mover.image = [image stretchableImageWithLeftCapWidth:25 topCapHeight:0];
}

- (void)setLeftCapImage:(UIImage *)image
{
    leftCap.image = image;
    if (self.topbarType == TopBarType_Slider) {
        leftCap.frame = CGRectMake(0, (self.frame.size.height-image.size.height)/2, 8, image.size.height);
    }
}

- (void)setRightCapImage:(UIImage *)image
{
    rightCap.image = image;
    if (self.topbarType == TopBarType_Slider) {
        rightCap.frame = CGRectMake(self.frame.size.width-8, (self.frame.size.height-image.size.height)/2, 8, image.size.height);
    }
}

- (void)setupCaps
{
    if (self.topbarType == TopBarType_Slider) {
        
        if(scrollView.contentSize.width <= scrollView.frame.size.width-scrollView.contentInset.left-scrollView.contentInset.right) {
            leftCap.hidden = YES;
            rightCap.hidden = YES;
        } else {
            if(scrollView.contentOffset.x > (-scrollView.contentInset.left)+10.0f) {
                leftCap.hidden = NO;
            } else {
                leftCap.hidden = YES;
            }
            
            if((scrollView.frame.size.width+scrollView.contentOffset.x)+10.0f >= scrollView.contentSize.width) {
                rightCap.hidden = YES;
            } else {
                rightCap.hidden = NO;
            }
        }
    }
}

- (void)setupVisible:(UIButton *)sender
{
	CGRect rect = sender.frame;
	[scrollView scrollRectToVisible:rect animated:YES];
}

- (void)reloadData
{    
    if(scrollView.subviews && scrollView.subviews.count > 0)
		[scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
	
	if(!self.dataSource)
		return;
	
	NSInteger items = [self.dataSource numberOfTabsInColumnBar:self];
	float origin_x = 0;
    float siliderWidth = 0;
    if (items <=4) {
        siliderWidth = self.width/items;
    }

	for(int x = 0; x < items; x++) {
        
        NSString *name = nil;

        name = [self.dataSource columnBar:self titleForTabAtIndex:x];
          
		UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        button.adjustsImageWhenHighlighted = NO;
		[button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        CGSize size = [name sizeWithFont:button.titleLabel.font];
        
        NSString * maxStr = @"新闻咨询";
        CGSize maxSize = [maxStr sizeWithFont:button.titleLabel.font];
        if (size.width > maxSize.width) {
            size = maxSize;
        }
        if (self.topbarType == TopBarType_Slider) {
            
            button.frame = CGRectMake(origin_x, 0.0f, siliderWidth ? siliderWidth : size.width + 18, scrollView.frame.size.height);
        }else{
            button.frame = CGRectMake(origin_x, 0.0f, size.width + kSpace, scrollView.frame.size.height);
        }
        
        if ([self.dataSource respondsToSelector:@selector(columnWidthOfTabsInColumnBar:)]) {
            int columnWidth = [self.dataSource columnWidthOfTabsInColumnBar:self];
            if (columnWidth != 0) {
                origin_x += columnWidth;  
            }
        }
        if (self.topbarType == TopBarType_Slider) {
            origin_x += siliderWidth ? siliderWidth : size.width + 18*2;

        }else{
            origin_x += size.width + kSpace*2;

        }

        button.titleLabel.font = [UIFont systemFontOfSize:15];
        
		[button setTitle:name forState:UIControlStateNormal];
     
        [button setTitleColor:self.unSelectedColor ? self.unSelectedColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button setTitleShadowColor:[UIColor colorWithWhite:0.4 alpha:1] forState:UIControlStateNormal];
        [button setTitleColor:self.selectedColor ? self.selectedColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [button setTitleShadowColor:[UIColor colorWithWhite:0.95 alpha:1] forState:UIControlStateSelected];
        [button setTitleColor:self.selectedColor ? self.selectedColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [button setTitleShadowColor:[UIColor colorWithWhite:0.95 alpha:1] forState:UIControlStateSelected];
        
        if ([self.dataSource respondsToSelector:@selector(columnBar:selectImageForTabAtIndex:)]) {
            UIImage *selectImage = [self.dataSource columnBar:self selectImageForTabAtIndex:x];
            if (selectImage != nil) {
                [button setBackgroundImage:selectImage forState:UIControlStateSelected];
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [button setTitleShadowColor:[UIColor colorWithWhite:0.4 alpha:1] forState:UIControlStateNormal];
                [button setTitleColor:RGBCOLOR(47, 137, 198) forState:UIControlStateSelected];
                [button setTitleShadowColor:[UIColor colorWithWhite:0.95 alpha:1] forState:UIControlStateSelected];
            }
        }
        [scrollView addSubview:button];
        if (x == 0) {
            button.selected = YES;
            button.userInteractionEnabled = NO;
        }
	}
	
	scrollView.contentSize = CGSizeMake(origin_x, scrollView.frame.size.height);
	
	[self setupCaps];
}

- (void)moveToFrame:(CGRect)frame animated:(BOOL)animated
{
    NSTimeInterval duration;
    
    if (animated)
        duration = 0.3;
    else
        duration = 0;
        
    [UIView animateWithDuration:duration animations:^(void) {
        [mover removeFromSuperview];
        if (self.topbarType == TopBarType_SelectedBlock) {
            mover.frame = CGRectMake(frame.origin.x-1, 6.5, frame.size.width, 24);
        }else if (self.topbarType == TopBarType_Slider) {
            mover.frame = CGRectMake(frame.origin.x, frame.size.height-3, frame.size.width, 3);
        }
        [scrollView addSubview:mover];
    }];

    [scrollView sendSubviewToBack:mover];
}

- (void)buttonClicked:(UIButton *)button
{
    [self moveToFrame:button.frame animated:YES];
    
    [self setupVisible:button];
    
    for (UIButton *btn in scrollView.subviews) {
        if ([btn isKindOfClass:[UIButton class]])
        {
            btn.selected = NO;
            btn.userInteractionEnabled = YES;
        }
    }
    
    button.selected = YES;
    button.userInteractionEnabled = NO;

    selectedIndex = [scrollView.subviews indexOfObject:button]-1;
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(columnBar:didSelectedTabAtIndex:)])
		[self.delegate columnBar:self didSelectedTabAtIndex:selectedIndex];
}

- (void)selectTabAtIndex:(int)index
{
    if(!scrollView.subviews || scrollView.subviews.count < index+1)
        return;
	
    UIButton *button = (UIButton *)[scrollView.subviews objectAtIndex:index];
    [self setupVisible:button];

	[self setupCaps];
    
    [self moveToFrame:button.frame animated:NO];
    
    [self buttonClicked:button];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)inScrollView
{
	[self setupCaps];
}

@end
