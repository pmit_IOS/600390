//
//  ZSTMainBodyCell.h
//  F3
//
//  Created by  on 12-6-14.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTWeiBoInfo.h"

@interface ZSTMainBodyUserCell : UITableViewCell     ///////11111111

@property (nonatomic, retain) TKAsynImageView *avtarImageView;//用户头像
@property (nonatomic, retain) UILabel *name;//用户名称

- (void)configCell:(ZSTWeiBoInfo *)weiBoInfo;

@end

@class ZSTMainBodyUserTextCell;                     
@protocol ZSTMainBodyUserTextCellDelegate <NSObject>

@optional
- (void)ZSTMainBodyUserTextCell:(ZSTMainBodyUserTextCell *)cell shouldAssignHeight:(CGFloat)newHeight;

- (void)ZSTMainBodyUserTextCell:(ZSTMainBodyUserTextCell *)cell didSelectLink:(NSString *)url;


- (void)ZSTMainBodyUserTextCell:(ZSTMainBodyUserTextCell *)cell didSelectImageLink:(NSString *)url;


@end

@interface ZSTMainBodyUserTextCell : UITableViewCell <UIWebViewDelegate>   ///////22222222
{
    NSMutableDictionary *imagesLinks;
	int _newLineCounter;
    id<ZSTMainBodyUserTextCellDelegate>delegate;
}

@property (nonatomic, retain) UIWebView *contentLabel;//用户发表内容
@property (nonatomic, retain) ZSTWeiBoInfo *WBInfo;
@property (nonatomic, assign) float webHeight;
@property (nonatomic, retain) id<ZSTMainBodyUserTextCellDelegate>delegate;


- (void)configCell:(ZSTWeiBoInfo *)weiBoInfo;


@end

@interface ZSTMainBodyForwardAndCommentCell : UITableViewCell                ///////3333333333

@property (nonatomic, retain) TKAsynImageView *avtarImageView;//用户头像

@property (nonatomic, retain) UILabel *name;//用户名称

@property (nonatomic, retain) UILabel *contentLabel;//用户发表内容

@property (nonatomic, retain) TKAsynImageView *pictureImageView;//用户发表图片

@property (nonatomic, retain) UILabel *dateLabel;//用户发表日期

- (void)configCell:(ZSTWeiBoInfo *)weiBoInfo;

@end