//
//  ZSTThirdAccountCell.m
//  F3
//
//  Created by P&M on 15/8/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTThirdAccountCell.h"

@implementation ZSTThirdAccountCell

- (void)createThirdAccountUI
{
    if (!self.titleLab) {
        
        // 标题
        self.titleLab = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, 160, 30)];
        self.titleLab.backgroundColor = [UIColor clearColor];
        self.titleLab.textColor = [UIColor blackColor];
        self.titleLab.textAlignment = NSTextAlignmentLeft;
        self.titleLab.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.titleLab];
        
        // 状态
        self.stateLab = [[UILabel alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width - 150, 10, 120, 30)];
        self.stateLab.backgroundColor = [UIColor clearColor];
        self.stateLab.textColor = RGBACOLOR(200, 200, 200, 1);
        self.stateLab.textAlignment = NSTextAlignmentRight;
        self.stateLab.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.stateLab];
        
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
}

- (void)setThirdAccountTitle:(NSString *)title state:(NSString *)state
{
    self.titleLab.text = title;
    if ([state isEqualToString:@"已绑定"])
    {
        self.stateLab.textColor = [UIColor greenColor];
    }
    else
    {
        self.stateLab.textColor = RGBACOLOR(200, 200, 200, 1);
    }
    
    self.stateLab.text = state;
}

@end
