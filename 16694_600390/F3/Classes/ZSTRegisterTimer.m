//
//  RegisterManager.m
//  F3
//
//  Created by 9588 on 10/24/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTRegisterTimer.h"
#import "TKUIUtil.h"

#define DEFAULT_REMAINING_TIME 60

@implementation ZSTRegisterTimer

+(ZSTRegisterTimer *) sharedTimer   
{
    static ZSTRegisterTimer *manager = nil;
    if (!manager)
    {
        manager = [[ZSTRegisterTimer alloc] init];
        [ZSTF3Preferences shared].remainingTime = DEFAULT_REMAINING_TIME;
	}
    return manager;
}

- (void)start
{
    [_timeInterval invalidate];
    [_timeInterval release];
    _timeInterval = [[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateIntervalAction) userInfo:nil repeats:YES] retain];
}

- (void)reset
{
    [ZSTF3Preferences shared].remainingTime = DEFAULT_REMAINING_TIME;
    [self start];
}

- (int)remainingTime
{
    return [ZSTF3Preferences shared].remainingTime;
}

-(void)updateIntervalAction
{
    ZSTF3Preferences *dataManager = [ZSTF3Preferences shared];
    
    --dataManager.remainingTime;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_RemaingTimeChange object:[NSNumber numberWithInt:dataManager.remainingTime]];
    
    if (dataManager.remainingTime <= 0) {
        [_timeInterval invalidate];        
        [_timeInterval release];
        _timeInterval = nil;
        [ZSTF3Preferences shared].remainingTime = DEFAULT_REMAINING_TIME;
    }
}

- (BOOL)isTiming
{
    return _timeInterval != nil;
}

-(void)dealloc
{
    [_timeInterval release];
    [super dealloc];
}

@end
