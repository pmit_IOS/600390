//
//  ZSTF3Preferences.m
//  F3_UI
//
//  Created by Xie Wei on 11-6-20.
//  Copyright 2011年 e-linkway.com. All rights reserved.
//

#import "ZSTF3Preferences.h"
#import "ZSTUtils.h"

NSString *const NotificationNMSKey = @"NotificationNMSKey";
NSString *const DeviceTokenKey = @"DeviceTokenKey";

#define REGISTER_NO 0           //未绑定
#define REGISTER_TEMP 1         //使用虚拟手机号
#define REGISTER_OK 9           //使用真实手机号，绑定成功

#define stringForKey(name) [[NSUserDefaults standardUserDefaults] objectForKey: @name] ;

#define intForKey(name) [[NSUserDefaults standardUserDefaults] integerForKey: @name] ;

#define boolForKey(name) [[NSUserDefaults standardUserDefaults] boolForKey: @name] ;

@implementation ZSTF3Preferences

@synthesize remainingTime;
@synthesize isDownloading;
@synthesize pushViewController;
@synthesize isChange;

SINGLETON_IMPLEMENTATION(ZSTF3Preferences)

+(NSString *) stringFromBundlePath: (NSString *) bundlePath
{
    NSString *filePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: bundlePath];
    NSString *contentString = [NSString stringWithContentsOfFile: filePath encoding: NSUTF8StringEncoding error: nil];
    return contentString;
}

-(void) setLoginMsisdn:(NSString *)loginMsisdn
{
    [[NSUserDefaults standardUserDefaults] setObject: loginMsisdn forKey: @"LoginMsisdn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*存储用户的手机号*/
-(NSString *) loginMsisdn
{
    NSString *loginMsisdn = stringForKey("LoginMsisdn");
    
    if (loginMsisdn == nil || [loginMsisdn isEqualToString:@""]) {
        return self.UserId;
    }
    
    
   return loginMsisdn;
}

-(void) setLoginPassword: (NSString *)loginPassword
{
    [[NSUserDefaults standardUserDefaults] setObject: loginPassword forKey: @"LoginPassword"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


/*存储用户的密码*/
-(NSString *) loginPassword
{
    return [[NSUserDefaults standardUserDefaults] objectForKey: @"LoginPassword"];
}

- (void)setPushToken:(NSString *)pushToken
{
    [[NSUserDefaults standardUserDefaults] setObject: pushToken forKey: @"PushToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (NSString *)pushToken
{
    NSString *pushToken = stringForKey("PushToken");
    
    if (pushToken == nil) {
        return @"";
    }
    return pushToken;
}

- (PushOpentype)pushOpentype
{
     return intForKey("pushOpentype");
}

- (void)setPushOpentype:(PushOpentype)pushOpentype
{
    [[NSUserDefaults standardUserDefaults] setInteger: pushOpentype forKey: @"pushOpentype"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (NSString *)phoneNumber
{
    return [[NSUserDefaults standardUserDefaults] objectForKey: @"phoneNumber"];
}

-(void) setPhoneNumber: (NSString *)phoneNumber
{
    [[NSUserDefaults standardUserDefaults] setObject: phoneNumber forKey: @"phoneNumber"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) setShellId: (NSInteger)shellid
{
    [[NSUserDefaults standardUserDefaults] setInteger:shellid forKey: @"shellid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSInteger)shellId
{
    return [[NSUserDefaults standardUserDefaults] integerForKey: @"shellid"];
}

-(void) setSnsaStates:(NSInteger)states
{
    [[NSUserDefaults standardUserDefaults] setInteger:states forKey: @"SnsaStates"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSInteger) snsaStates
{
    return [[NSUserDefaults standardUserDefaults] integerForKey: @"SnsaStates"];
}


- (NSInteger)pushSelectIndex
{
    return intForKey("pushSelectIndex");
}

- (void)setPushSelectIndex:(NSInteger)pushSelectIndex
{
    [[NSUserDefaults standardUserDefaults] setInteger:pushSelectIndex forKey:@"pushSelectIndex"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (NSInteger)conId
{
    return [[NSUserDefaults standardUserDefaults] integerForKey: @"conid"];
}

-(void) setConId:(NSInteger)conid
{
    [[NSUserDefaults standardUserDefaults] setInteger:conid forKey:@"conid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)verficationCode
{
    return [[NSUserDefaults standardUserDefaults] objectForKey: @"verficationCode"];
}

-(void) setVerficationCode: (NSString *)verficationCode 
{
    [[NSUserDefaults standardUserDefaults] setObject: verficationCode forKey:@"verficationCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setUserId:(NSString *)userId
{
    [[NSUserDefaults standardUserDefaults] setObject: userId forKey:@"UserId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *)UserId
{
    NSString *userid = [[NSUserDefaults standardUserDefaults] objectForKey: @"UserId"];
    if (!userid || userid.length == 0) {
        userid = @"";
    }
    
    return userid;
}

- (void) setAvaterName:(NSDictionary *)name
{
    [[NSUserDefaults standardUserDefaults] setObject: name forKey:@"AvaterName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDictionary *)avaterName
{
     return [[NSUserDefaults standardUserDefaults] objectForKey: @"AvaterName"];
}

/*得到ECECCID*/
-(NSString *) ECECCID
{
    return [ZSTUtils getECECCID];
}

//使用uuid作为唯一标识符
-(NSString *) imsi
{
    NSString *uuidString = nil;
    
    if (IS_IOS_7) {
        
        uuidString = [TKKeychainUtils getUserid:@"com.9588.f3"];
        
        if ([uuidString isEqualToString:@""] || uuidString == nil || [uuidString length] == 0)
        {
            uuidString = [NSString stringWithNewUUID];
            [TKKeychainUtils createKeychainValueForUserid:uuidString withToken:@"" withName:@"" withAccountType:0 withAvtarURL:@"" forIdentifier:@"com.9588.f3"];
        }
        
    }else{
        
        NSUserDefaults *UUIDDefault = [NSUserDefaults standardUserDefaults];
        uuidString = [UUIDDefault objectForKey:@"UUID"];
        
        if ([uuidString isEqual:@""] || uuidString == nil || [uuidString length] == 0) {
            uuidString = [UIDevice uniqueGlobalDeviceIdentifier];
            [UUIDDefault setObject:uuidString forKey:@"UUID"];
            [UUIDDefault synchronize];
        }
    }
    
    return uuidString ;
}


-(NSInteger) regManualTime
{
    return [[NSUserDefaults standardUserDefaults] integerForKey: @"regManualTime"];
}

-(void) setRegManualTime:(NSInteger)regManualTime
{
    [[NSUserDefaults standardUserDefaults] setInteger:regManualTime 
                                               forKey:@"regManualTime"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSInteger) version
{
    
//    return [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"] intValue];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSArray *array = [version componentsSeparatedByString:@"."];
    
    return [[array objectAtIndex:0] intValue];
}

-(NSString *) platform
{
    NSString *platform = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"platform"];
    NSString *bundleName = [[NSBundle mainBundle] bundleIdentifier];
    if (TKIsPad()) {
        if ([platform isEqualToString:@"ipc"]) {
            platform = @"ipc";
        }else if ([platform isEqualToString:@"ipec"]) {
            platform = @"ipec";
        }else {
             platform = @"ip";
        }
    } else {
//        if ([platform isEqualToString:@"ic"]) {
//            platform = @"ic";
//        }else if ([platform isEqualToString:@"iec"]) {
//            platform = @"iec";
//        }else {
//            platform = @"i";
//        }
        
        NSRange range = [bundleName rangeOfString:@"ec"];
        //if ([bundleName containsString:@"ec"]) // 这个判断在iOS7上存在问题，iOS8不会
        if (range.length > 0)
        {
            platform = @"iec";
        }
        else
        {
            platform = @"i";
        }
        
    }
    return platform;
}

- (void)setCarrierType:(NSInteger)carrierType
{
    [[NSUserDefaults standardUserDefaults] setInteger:carrierType
                                               forKey:@"CarrierType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSInteger) CarrierType
{
    return [[NSUserDefaults standardUserDefaults] integerForKey: @"CarrierType"];
}

-(MCRegistType) MCRegistType
{
    if (TKIsPad()) {
        return MCRegistType_VirtualRegister;
    }
    
    NSInteger MCRegistType = intForKey("MCRegistType");
    if (MCRegistType < MCRegistType_ForceRegister || MCRegistType > MCRegistType_VirtualRegister) {
        
        MCRegistType = MCRegistType_ForceRegister;
    }
    return MCRegistType;
}

-(void) setMCRegistType:(MCRegistType)MCRegistType
{
    [[NSUserDefaults standardUserDefaults] setInteger:MCRegistType 
                                              forKey:@"MCRegistType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *) MCRegistDescription
{
    return stringForKey("MCRegistDescription");
}

-(void) setMCRegistDescription:(NSString *)MCRegistDescription
{
    [[NSUserDefaults standardUserDefaults] setObject:MCRegistDescription 
                                              forKey:@"MCRegistDescription"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *) MCRegistChangeDescription
{
    return stringForKey("MCRegistChangeDescription");
}

-(void) setMCRegistChangeDescription:(NSString *)MCRegistChangeDescription
{
    [[NSUserDefaults standardUserDefaults] setObject:MCRegistChangeDescription 
                                              forKey:@"MCRegistChangeDescription"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL) smsShare
{
    return boolForKey("smsShare");
}

- (void) setSmsShare:(BOOL)smsShare
{
    [[NSUserDefaults standardUserDefaults] setBool:smsShare
                                            forKey:@"smsShare"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL) WX_Friend
{
     return boolForKey("WX_Friend");
}

- (void) setWX_Friend:(BOOL)wx_friend
{
    [[NSUserDefaults standardUserDefaults] setBool:wx_friend
                                            forKey:@"WX_Friend"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL) WX_FriendsCircle
{
    return boolForKey("WX_FriendsCircle");
}

- (void) setWX_FriendsCircle:(BOOL)wx_friendsCircle
{
    [[NSUserDefaults standardUserDefaults] setBool:wx_friendsCircle
                                            forKey:@"WX_FriendsCircle"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)SinaWeiBoShare
{
    return boolForKey("SinaWeiBoShare");
}

- (void) setSinaWeiBoShare:(BOOL)sinaWeiBoShare
{
    [[NSUserDefaults standardUserDefaults] setBool:sinaWeiBoShare
                                            forKey:@"SinaWeiBoShare"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSInteger)WXshareType
{
    return [[NSUserDefaults standardUserDefaults] integerForKey: @"WXshareType"];
}

-(void) setWXshareType:(NSInteger)wxshareType
{
    [[NSUserDefaults standardUserDefaults] setInteger:wxshareType
                                               forKey:@"WXshareType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(BOOL) IPPushFlag
{
    return boolForKey("IPPushFlag");
}

-(void) setIPPushFlag:(BOOL)IPPushFlag
{
    [[NSUserDefaults standardUserDefaults] setBool:IPPushFlag
                                            forKey:@"IPPushFlag"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isShowHelp
{
    return boolForKey("isShowHelp")
}

- (void)setIsShowHelp:(BOOL)isShowHelp
{
    [[NSUserDefaults standardUserDefaults] setBool:isShowHelp forKey:@"isShowHelp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSInteger)UpdateVersion
{
     return intForKey("UpdateVersion")
}

- (void)setUpdateVersion:(NSInteger)UpdateVersion
{
    [[NSUserDefaults standardUserDefaults] setInteger:UpdateVersion forKey:@"UpdateVersion"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSInteger)newVersion
{
    return intForKey("newVersion")
}

- (void)setNewVersion:(NSInteger)newVersion
{
    [[NSUserDefaults standardUserDefaults] setInteger:newVersion forKey:@"newVersion"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(NSString *) HTTPPollTimeRule
{
    return stringForKey("HTTPPollTimeRule");
}

-(void) setHTTPPollTimeRule:(NSString *)HTTPPollTimeRule
{
    [[NSUserDefaults standardUserDefaults] setObject:HTTPPollTimeRule 
                                              forKey:@"HTTPPollTimeRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL) SMSHookFlag
{
    return boolForKey("SMSHookFlag");
}

-(void) setSMSHookFlag:(BOOL)SMSHookFlag
{
    [[NSUserDefaults standardUserDefaults] setBool:SMSHookFlag
                                            forKey:@"SMSHookFlag"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(NSString *) centralServer
{
    return stringForKey("centralServer");
}

-(void) setCentralServer:(NSString *)centralServer
{
    [[NSUserDefaults standardUserDefaults] setObject:centralServer 
                                           forKey:@"centralServer"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSInteger) centralServerPort
{
    return intForKey("centralServerPort");
}

-(void) setCentralServerPort:(NSInteger)centralServerPort
{
    [[NSUserDefaults standardUserDefaults] setInteger:centralServerPort 
                                           forKey:@"centralServerPort"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *) httpServer
{
    return stringForKey("httpServer");
}

-(void) setHttpServer:(NSString *)httpServer
{
    [[NSUserDefaults standardUserDefaults] setObject:httpServer 
                                           forKey:@"httpServer"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSInteger) httpServerPort
{
    return intForKey("httpServerPort");
}

-(void) setHttpServerPort:(NSInteger)httpServerPort
{
    [[NSUserDefaults standardUserDefaults] setInteger:httpServerPort 
                                               forKey:@"httpServerPort"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *) udpServer
{
    return stringForKey("udpServer");
}

-(void) setUdpServer:(NSString *)udpServer
{
    [[NSUserDefaults standardUserDefaults] setObject:udpServer 
                                              forKey:@"udpServer"];
}

-(NSInteger) udpServerPort
{
    return intForKey("udpServerPort");
}

-(void) setUdpServerPort:(NSInteger)udpServerPort
{
    [[NSUserDefaults standardUserDefaults] setInteger:udpServerPort 
                                               forKey:@"udpServerPort"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *) RegToicpCMCC
{
    return stringForKey("RegToicpCMCC");
}

-(void) setRegToicpCMCC:(NSString *)RegToicpCMCC
{
    [[NSUserDefaults standardUserDefaults] setObject:RegToicpCMCC 
                                              forKey:@"RegToicpCMCC"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *) RegToicpUNICOM
{
    return stringForKey("RegToicpUNICOM");
}

-(void) setRegToicpUNICOM:(NSString *)RegToicpUNICOM
{
    
    [[NSUserDefaults standardUserDefaults] setObject:RegToicpUNICOM 
                                              forKey:@"RegToicpUNICOM"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *) RegToicpTELECOM
{
    return stringForKey("RegToicpTELECOM");
}

-(void) setRegToicpTELECOM:(NSString *)RegToicpTELECOM
{
    [[NSUserDefaults standardUserDefaults] setObject:RegToicpTELECOM 
                                              forKey:@"RegToicpTELECOM"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *) msgKey
{
    NSString *string = stringForKey("msgKey");
    if (string.length== 0) {
        string = nil;
    }
    return string;
}

-(void) setMsgKey:(NSString *)msgKey
{
    [[NSUserDefaults standardUserDefaults] setObject:msgKey 
                                              forKey:@"msgKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(NSString *)SinaWeiBo
{
    NSString *string = stringForKey("SinaWeiBo");
    if (string.length== 0) {
        string = nil;
    }
    return string;
}

- (void)setSinaWeiBo:(NSString *)SinaWeiBo
{
    [[NSUserDefaults standardUserDefaults] setObject:SinaWeiBo
                                              forKey:@"SinaWeiBo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)TWeiBo
{
    NSString *string = stringForKey("TWeiBo");
    if (string.length== 0) {
        string = nil;
    }
    return string;
}

- (void)setTWeiBo:(NSString *)TWeiBo
{
    [[NSUserDefaults standardUserDefaults] setObject:TWeiBo
                                              forKey:@"TWeiBo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)QQ
{
    NSString *string = stringForKey("QQ");
    if (string.length== 0) {
        string = nil;
    }
    return string;
}

- (void)setQQ:(NSString *)QQ
{
    [[NSUserDefaults standardUserDefaults] setObject:QQ
                                              forKey:@"QQ"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)WeiXin
{
    NSString *string = stringForKey("WeiXin");
    if (string.length== 0) {
        string = nil;
    }
    return string;
}

- (void)setWeiXin:(NSString *)WeiXin
{
    [[NSUserDefaults standardUserDefaults] setObject:WeiXin
                                              forKey:@"WeiXin"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL) sound
{
    return !boolForKey("noSound");
}

-(void) setSound:(BOOL)sound
{
    [[NSUserDefaults standardUserDefaults] setBool:!sound
                                            forKey:@"noSound"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL) shake
{
    return !boolForKey("noShake");
}

-(void) setShake:(BOOL)shake
{
    [[NSUserDefaults standardUserDefaults] setBool:!shake
                                            forKey:@"noShake"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL) cleanUp
{
    return [[NSUserDefaults standardUserDefaults] boolForKey: @"cleanUp"];
    
}

-(void) setCleanUp:(BOOL)cleanUp
{
    [[NSUserDefaults standardUserDefaults] setBool:cleanUp
                                            forKey:@"cleanUp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDate *)cleanupDate
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"cleanupDate"];
}

-(void) setCleanupDate:(NSDate *)cleanupDate
{
    [[NSUserDefaults standardUserDefaults] setObject:cleanupDate forKey:@"cleanupDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL) reportState
{
    return boolForKey("reportState");
}

-(void) setReportState:(BOOL)reportState
{
    [[NSUserDefaults standardUserDefaults] setBool:reportState
                                              forKey:@"reportState"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL) fullScreen
{
    return [[NSUserDefaults standardUserDefaults] boolForKey: @"fullScreen"];
}

-(void) setFullScreen:(BOOL)fullScreen
{
    [[NSUserDefaults standardUserDefaults] setBool:fullScreen
                                            forKey:@"fullScreen"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(NMSPackageType) packageType
{
    NSInteger packageType = intForKey("NoPicture");
    if (packageType != NMSPackageType_noPicture) {
        return NMSPackageType_normal;
    }
    
   return  packageType;
}

-(void) setPackageType:(NMSPackageType)packageType
{
    [[NSUserDefaults standardUserDefaults] setInteger:packageType 
                                               forKey:@"NoPicture"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSInteger) BSVersion
{
    return intForKey("BSVersion");
}

-(void) setBSVersion:(NSInteger)BSVersion
{
    [[NSUserDefaults standardUserDefaults] setInteger:BSVersion 
                                               forKey:@"BSVersion"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL) isFirstLogin
{
    return !boolForKey("notFirstLaunch");
}

-(void) setIsFirstLogin:(BOOL)isFirstLaunch
{
    [[NSUserDefaults standardUserDefaults] setBool:!isFirstLaunch 
                                               forKey:@"notFirstLaunch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isInPush
{
    return boolForKey("isInPush");
}

- (void)setIsInPush:(BOOL)isInPush
{
    [[NSUserDefaults standardUserDefaults] setBool:isInPush
                                            forKey:@"isInPush"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL) hasUpdateClientParams
{
    return boolForKey("hasUpdateClientParams");
}

-(void) setHasUpdateClientParams:(BOOL)hasUpdateClientParams
{
    [[NSUserDefaults standardUserDefaults] setBool:hasUpdateClientParams 
                                            forKey:@"hasUpdateClientParams"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)synchronize
{
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)deleteSysLogsBeforeDate:(NSDate *)compareDate
{
    NSDirectoryEnumerator *enums = [[NSFileManager defaultManager] enumeratorAtPath: [ZSTUtils pathForLogs]];
    NSString *fileName = nil;
    while ((fileName = [enums nextObject]) != nil) {
        NSDate *createDate = [[enums fileAttributes] fileCreationDate];
        
        if ([createDate compare:compareDate] == NSOrderedAscending) {//时间有序递增
            NSString *logPath = [[ZSTUtils pathForLogs] stringByAppendingPathComponent:fileName];
            NSError *error = nil;
            // 从文件系统中删除
            if (![[NSFileManager defaultManager] removeItemAtPath:logPath error:&error]) {
                
            }
        }
    }
}


-(void)setUID:(NSString *)UID
{
    [[NSUserDefaults standardUserDefaults] setObject: UID forKey: @"snsauserid"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
-(NSString *)UID
{
    NSString *uid = stringForKey("snsauserid");
    
    if (uid == nil) {
        return @"";
    }
    return uid;

}
-(void) deleteAttachmentFromFileManagerByPath:(NSString *)path
{
    [[NSFileManager defaultManager] removeItemAtPath: path error: nil];
}

@end
