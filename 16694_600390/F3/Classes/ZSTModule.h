//
//  FTPlugin.h
//  F3
//
//  Created by luobin on 7/10/12.
//  Copyright (c) 2012 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Element;

@interface ZSTModule : NSObject

@property (nonatomic, readonly) NSInteger ID;                   //插件ID
@property (nonatomic, readonly) NSInteger defaultModuleType;
@property (nonatomic, retain, readonly) NSString *name;                 //插件名称
@property (nonatomic, retain, readonly) NSString *package;              //插件包名
@property (nonatomic, readonly) Class entryPoint;               //插件入口类
@property (nonatomic, readonly) Class settingEntryPoint;        //设置插件入口类
@property (nonatomic, readonly) BOOL hasSetting;                //插件是否有设置项
@property (nonatomic, readonly) UIImage *moduleIconN;
@property (nonatomic, readonly) UIImage *moduleIconP;

+ (id)pluginWithElement:(Element *)element;

@end
