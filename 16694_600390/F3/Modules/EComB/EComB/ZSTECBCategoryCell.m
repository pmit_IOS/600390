//
//  ZSTECBCategoryCell.m
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBCategoryCell.h"

@implementation ZSTECBCategoryCell

@synthesize titleLabel;
@synthesize position;
@synthesize checked = _isSelected;

- (void)dealloc
{
    self.titleLabel = nil;
    self.iconImg = nil;
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        
        self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(15, 5, 268, 30)] autorelease];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.shadowColor = [UIColor whiteColor];
		self.titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        self.titleLabel.textColor = RGBCOLOR(26, 26, 26);
        self.titleLabel.text =  @"";
        self.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        self.titleLabel.highlightedTextColor = [UIColor whiteColor];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.titleLabel];
        
        self.iconImg = [[[UIImageView alloc] initWithFrame:CGRectMake(285, (40-14)/2, 14, 14)] autorelease];
        self.iconImg.image = ZSTModuleImage(@"module_ecomb_category_arrow_down.png");
        [self.contentView addSubview:self.iconImg];
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 39, 320, 1)];
        line.backgroundColor = RGBCOLOR(242, 242, 242);
        [self addSubview:line];
        [line release];
    }
    return self;
}

- (void)setChecked:(BOOL)checked
{
    //选中
	if (checked)
	{
        //勾选的图标
		self.iconImg.image = ZSTModuleImage(@"module_ecomb_category_arrow_up.png");
    
	}//反选
    else
	{
        //反选的图标
		self.iconImg.image = ZSTModuleImage(@"module_ecomb_category_arrow_down.png");
	}
    
    _isSelected = checked;
}


- (void)configCell:(NSDictionary *)category forRowAtIndex:(NSIndexPath *)indexPath
{
    NSArray *children = [category safeObjectForKey:@"items"];
    
    self.iconImg.hidden = NO;
    
    if (children.count == 0) {
        
        self.iconImg.hidden = YES;;
    }
    
    self.titleLabel.text = [category safeObjectForKey:@"categoryname"];
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    [[UIColor whiteColor] set];
    CGContextFillRect(c, self.bounds);
    
    CGFloat height = 1.f;
    if (TKIsRetina()) {
        height = 0.5f;
    }
    
    if (position == CustomCellPositionTop) {
        
        [ZSTModuleImage(@"module_ecomb_category.png") drawAsPatternInRect:CGRectMake(0, 0, self.width, self.height - height)];
        
        [[UIColor colorWithWhite:0.8 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, self.height - height, self.width, height));
        
        return;
    } else if (position == CustomCellPositionBottom) {
        
        [ZSTModuleImage(@"module_ecomb_category.png") drawAsPatternInRect:CGRectMake(0, height, self.width, self.height - height*2)];
        
        [[UIColor colorWithWhite:0.98 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, 0, self.width, height));
        
        [[UIColor colorWithWhite:0.8 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, self.height - height, self.width, height));
        
        return;
    }
    else if (position == CustomCellPositionSingle || position == CustomCellPositionMiddle) {
        
        [ZSTModuleImage(@"module_ecomb_category.png") drawAsPatternInRect:CGRectMake(0, height, self.width, self.height - 2*height)];
        
        [[UIColor colorWithWhite:0.98 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, 0, self.width, height));
        
        [[UIColor colorWithWhite:0.8 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, self.height - height, self.width, height));
        
        return;
    } else {
        [ZSTModuleImage(@"module_ecomb_category.png") drawAsPatternInRect:CGRectMake(0, 0, self.width, self.height - height)];
        
        [[UIColor colorWithWhite:0.8 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, self.height - height, self.width, height));
    }
}

@end
