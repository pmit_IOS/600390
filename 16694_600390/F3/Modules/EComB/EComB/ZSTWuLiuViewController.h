//
//  ZSTWuLiuViewController.h
//  EComB
//
//  Created by LiZhenQu on 14-8-21.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTWuLiuViewController : UIViewController<UIWebViewDelegate>
{
    
    NSString *orderId;
    UIWebView *webview;
}

@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) NSString *orderId;

@end
