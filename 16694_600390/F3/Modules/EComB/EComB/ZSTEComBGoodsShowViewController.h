//
//  ZSTEComBGoodsShowViewController.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECBHomeCarouselView.h"
#import "ZSTECBPropertyViewController.h"
#import "ZSTEComBPutInCarButton.h"
#import "EGOPhotoViewController.h"
//#import "EGOPhotoGlobal.h"
#import <ZSTLoginController.h>

@interface ZSTEComBGoodsShowViewController : UIViewController<UIScrollViewDelegate,TKAsynImageViewDelegate,ZSTECBHomeCarouselViewDataSource,ZSTECBHomeCarouselViewDelegate,ZSTF3EngineECBDelegate,UIActionSheetDelegate,ZSTLoginControllerDelegate>

@property (retain,nonatomic)TKAsynImageView * titleImgView;
@property (retain,nonatomic)UILabel * titleLabel;
@property (retain,nonatomic)UILabel * priceLabel;
@property (retain,nonatomic)UILabel * originalPriceLabel;
@property (retain, nonatomic) UILabel *pointLabel;
@property (retain,nonatomic)UILabel * freightPriceLabel;
@property (retain,nonatomic)EComBHomePageInfo * pageInfo;
@property (retain, nonatomic) UIView *menuView;
@property (retain,nonatomic)UILabel * slip;
@property (retain,nonatomic)ZSTECBHomeCarouselView * carouseView;
@property (retain,nonatomic)NSArray * carouseData;
@end
