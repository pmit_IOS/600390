//
//  ZSTEComBPutInCarButton.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-18.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBPutInCarButton.h"

@implementation ZSTEComBPutInCarButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}//273.0/2, 63.0/2.0
-(CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(40, 7, 136 - 40, 16);
}
-(CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(15, 7, 15, 15);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
