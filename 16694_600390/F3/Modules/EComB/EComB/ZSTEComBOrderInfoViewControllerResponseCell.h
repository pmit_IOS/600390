//
//  ZSTEComBOrderInfoViewControllerResponseCell.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PMRepairButton.h>

@protocol ZSTEComBOrderInfoViewControllerResponseCellDelegate <NSObject>

-(void)submitBtnDidClicked;

@end

@interface ZSTEComBOrderInfoViewControllerResponseCell : UITableViewCell

@property (assign,nonatomic) id <ZSTEComBOrderInfoViewControllerResponseCellDelegate> delegate;
@property (retain,nonatomic) PMRepairButton * payBtn;
@property (retain,nonatomic) UILabel * priceLabel;
@property (retain,nonatomic) UILabel * otherLabel;

-(void)hideSubmit;
@end
