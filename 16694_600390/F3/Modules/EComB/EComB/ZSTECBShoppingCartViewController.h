//
//  ZSTECBShoppingCartViewController.h
//  EComB
//
//  Created by LiZhenQu on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModuleBaseViewController.h"

@interface ZSTECBShoppingCartViewController : UIViewController<ZSTF3EngineECBDelegate>
{
    float _totalPrice;
    int _totalFreight;
    BOOL _hasGools;
    NSInteger _maxRow;
    NSInteger _totalCount;
    EcomBAddress * _address;
}
@property (nonatomic,retain) NSMutableArray * dataArray;
//@property (nonatomic, assign) float totalPrice;

@end
