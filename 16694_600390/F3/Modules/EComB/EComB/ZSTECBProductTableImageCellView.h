//
//  ZSTECBProductTableImageCellView.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECBProductTableImageCellView : UIView
//@property (retain,nonatomic) TKAsynImageView * imageView;
@property (retain,nonatomic) UIImageView *imageView;
@property (retain,nonatomic) UILabel * textLabel;
@property (retain,nonatomic) UILabel * priceLabel;
@property (retain, nonatomic) UILabel * originalPriceLabel;
@property (retain,nonatomic)UILabel * slip;
@property (retain, nonatomic) UIView *pointview;

-(void)reloadData:(EComBHomePageInfo *) info;
@end
