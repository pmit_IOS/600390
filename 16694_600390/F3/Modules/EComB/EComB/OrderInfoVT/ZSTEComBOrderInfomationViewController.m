//
//  ZSTEComBOrderInfomationViewController.m
//  EComB
//
//  Created by qiuguian on 15/7/29.
//  Copyright (c) 2015年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBOrderInfomationViewController.h"
#import "ZSTEComBOrderInfoAddressCell.h"
#import "ZSTEComBOrderInfoPayCell.h"
#import "ZSTEComBOrderInfoInformationCell.h"
#import "ZSTEComBOrderInfoJifenCell.h"
#import "ZSTEComBInputAddressViewController.h"
#import "ZSTEComBPaymentViewController.h"

@interface ZSTEComBOrderInfomationViewController (){
    BOOL isyinlian;
    int isjifen;
    
    int pointNum;
    float deductionAmount;
    
    BOOL isjifenpay;
    
    float tmpprice;
    NSInteger payTypeNum;
    NSInteger finalPayType;
}

@end

@implementation ZSTEComBOrderInfomationViewController{

    PAYTYPEN _paytype;
    STATEN _state;
    BOOL _haveAddress;
    BOOL _haveJifen;
    NSInteger currentIndex;
    NSArray *payWayType;
    UILabel *priceLabel;
    UILabel *l1;
    UILabel *l3;
    UIButton *b1;

}

@synthesize payNameArray = _payNameArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"订单详情", nil)];
    self.view.backgroundColor = [UIColor whiteColor];
    
    //创建底部的视图的值
    l1 =[[UILabel alloc] init];
    l1.frame = CGRectMake(15, 425, 120, 25);
    l1.font = [UIFont boldSystemFontOfSize:14];
    l1.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
    l1.text = @"实付款";
    [self.view addSubview:l1];
    [l1 release];
    
    priceLabel =[[UILabel alloc] init];
    priceLabel.frame = CGRectMake(20, 450, 160, 30);
    priceLabel.font = [UIFont systemFontOfSize:22];
    priceLabel.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
    priceLabel.text = [NSString stringWithFormat:@"￥0.00"];
    [self.view addSubview:priceLabel];
    [priceLabel release];
    
    l3=[[UILabel alloc] init];
    l3.frame = CGRectMake(20, 475, 120, 30);
    l3.font = [UIFont systemFontOfSize:12];
    l3.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
    l3.text = [NSString stringWithFormat:@"(含运费0.00元)"];
    [self.view addSubview:l3];
    [l3 release];
    
    b1 =[[UIButton alloc] init];
    b1 = [[PMRepairButton alloc] init];
    b1.frame = CGRectMake(320 - 15 - 130, 450, 140, 38);
    [b1 setImage:ZSTModuleImage(@"module_ecomb_orderinfo_Responsebtn.png") forState:UIControlStateNormal];
    [b1 addTarget:self action:@selector(goPayAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:b1];
    [b1 release];
    
    //付款方式 初始化
    _payNameArray = [[NSArray alloc] initWithObjects:@"支付宝",@"货到付款", nil];
    payWayType = [NSArray arrayWithObjects:@"zhifubao",@"",nil];
    
    _haveAddress = NO;
    _paytype = 1;//默认只有支付宝
    finalPayType = 0;//默认为支付宝
    [self createTabelView];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.engine getEcomBAddress];
    [self.engine getEcomBOrderInfoViewWithOrderID:_orderID];
    [self.engine getEcomBOrderAmountWithOrderID:_orderID];
    
}

/**
 *  创建一个TableView
 */
-(void)createTabelView{
    CGRect rect = self.view.bounds;
    _tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
    _tableView.frame =CGRectMake(0, 0, 320, 480);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.allowsSelection = YES;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.separatorColor = [UIColor clearColor];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_tableView];
}


/**
 *  创建底部的Lable 与 buttom
 */
-(void)bottomPayControl{
    
    priceLabel.text = [NSString stringWithFormat:@"￥%.2f",_totalAmount];
    
    l3.text = [NSString stringWithFormat:@"(含运费%d元)",_totalFreight];
    
    if (_state == statezhifuchenggongn || _state == stateyiwanchengn || _state == stateyifahuon) {
        b1.hidden =YES;
        b1.enabled = NO;
    }else{
        b1.hidden = NO;
        b1.enabled = YES;
    }
    
}

#pragma mark - 数据源代理
/**
 *  <#Description#>
 *
 *  @param tableView <#tableView description#>
 *
 *  @return <#return value description#>
 */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

#pragma mark -  订单总金额
-(void)getEcomBOrderAmountdidSucceed:(float)amount totalfreight:(float)freight
{
    tmpprice = amount;
    _totalAmount = amount;
    _totalFreight = freight;
    
    [_tableView reloadData];
    
    [self reloadViewWith:1];
    
    [self.engine getjifenPayInfoWithOrderId:_orderID costamount:amount];
}
-(void)getEcomBOrderAmountdidFailed:(int)resultCode
{
    NSLog(@"获取订单总金额失败");
}

#pragma mark -  积分
- (void)getjifenPayDidSucceed:(int)pointnum deductionamount:(float)deductionamount
{
    [TKUIUtil hiddenHUD];
    pointNum = pointnum;
    deductionAmount = deductionamount;
    
    [self.tableView reloadData];
}

- (void)getjifenPayDidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
}

#pragma mark - 订单详情
-(void)getEcomBOrderInfoViewdidSucceed:(NSArray *)array payType:(int)payType status:(int)status yinlian:(BOOL)yinlian jifenpay:(int)jifenpay
{
    //    _payWay = (payType == typezhifubao||payType == typetotal)?zhifubao:huodao;
    [TKUIUtil hiddenHUD];
    switch (payType) {
        case typezhifubaon:
            _payWay = zhifubaon;
            break;
        case typehuodaon:
            _payWay = huodaon;
            break;
        case 3:
            _payWay = zhifubaon;
            break;
        default:
            _payWay = huodaon;
            break;
    }
    
    
    isyinlian = yinlian;
    isjifen = jifenpay;
    
    _paytype =payType;
    _state = status;
    _orderGools = [EcombOrderGools ecombOrderGoolsWithArray:array];
    
    //底部视图
    [self bottomPayControl];
    
    [_tableView reloadData];
    NSLog(@"订单详情成功");
}
-(void)getEcomBOrderInfoViewdidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
    NSLog(@"订单详情失败");
}
-(void)getEComBAddressdidSucceed:(NSDictionary *)addressDic
{
    [TKUIUtil hiddenHUD];
    _address = [(EcomBAddress *)[EcomBAddress addressWithDictonary:addressDic] retain];
    if (_address.ecomAddress.length > 0) {
        _haveAddress = YES;
    }
    
    [self.engine getFreightTemplate:self.orderID];
    
    [self.tableView reloadData];
}

-(void)getEComBAddressdidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
    NSLog(@"EcomB 地址请求失败");
}
-(void)addressDidPressedAction
{
    ZSTEComBInputAddressViewController * addInputViewController  = [[ZSTEComBInputAddressViewController alloc]init];
    addInputViewController.address = _address;
    addInputViewController.hidesBottomBarWhenPushed = YES;
    addInputViewController.isNullFrom = NO;
    [self.navigationController pushViewController:addInputViewController animated:YES];
    [addInputViewController release];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section ==0) {
        return 1;
    }
    
    if (section ==1) {
        
        if (_paytype==1) {
            
            if (isjifen) {
                return _payNameArray.count;
            }
            
            return _payNameArray.count-1;
        }
        
        if (isjifen) {
            return _payNameArray.count+1;
        }
        return 2;
    }
    
    if (section ==2) {
        
        return _orderGools.count;
    }
    return @"";
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (_address) {
            return 100.0f;
        }
        return 60.0f;
    }
    else if(indexPath.section == 1)
    {
        if (isjifen) {
            if (indexPath.row==_payNameArray.count) {
                return 70.0f;
            }
        }
        
        return 41.0f;
        
    }
    else if(indexPath.section == 2)
    {
        return 60.0f;
    }
    
    return 41.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        
        static NSString * ZSTEComBOrderInfoAddressCellName = @"ZSTEComBOrderInfoAddressCellName";
        ZSTEComBOrderInfoAddressCell * cell = [tableView dequeueReusableCellWithIdentifier:ZSTEComBOrderInfoAddressCellName];
        if (!cell) {
            cell = [[ZSTEComBOrderInfoAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ZSTEComBOrderInfoAddressCellName];
            cell.delegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if (_address) {
            [cell initData:_address];
        }
        return  cell;
        
    }else if(indexPath.section==1){
        
        if (indexPath.row !=_payNameArray.count) {
            
            static NSString * ZSTEComBOrderInfoPayCellName = @"ZSTEComBOrderInfoPayCellName";
            ZSTEComBOrderInfoPayCell * cell = [tableView dequeueReusableCellWithIdentifier:ZSTEComBOrderInfoPayCellName];
            if (!cell) {
                cell = [[ZSTEComBOrderInfoPayCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ZSTEComBOrderInfoPayCellName];
                cell.delegate = self;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            if (indexPath.row == 0) {
                cell.payWayImageView.image = ZSTModuleImage(@"module_ecomb_orderinfo_zhifubao.png");
                cell.payWayImageView.hidden = NO;
                cell.payWayLabel.hidden = YES;
                cell.selectedImageView.image = ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png");
                cell.statusLabel.text = @"1";
                
                if (finalPayType == 0) {
                    cell.selectedImageView.image = ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png");
                    cell.statusLabel.text = @"1";
                }else{
                    cell.selectedImageView.image = ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png");
                    cell.statusLabel.text = @"0";
                }
                
            }else{
                cell.payWayImageView.hidden = YES;
                cell.payWayLabel.hidden = NO;
                cell.payWayLabel.text = _payNameArray[indexPath.row];
                cell.selectedImageView.image = ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png");
                cell.statusLabel.text = @"0";
                
                if (finalPayType == 1) {
                    cell.selectedImageView.image = ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png");
                    cell.statusLabel.text = @"1";
                }else{
                    cell.selectedImageView.image = ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png");
                    cell.statusLabel.text = @"0";
                }
            }
            
            
            return  cell;
            
        }else{
            static NSString * ZSTEComBOrderInfoJifenCellName = @"ZSTEComBOrderInfoJifenCellName";
            ZSTEComBOrderInfoJifenCell * cell = [tableView dequeueReusableCellWithIdentifier:ZSTEComBOrderInfoJifenCellName];
            if (!cell) {
                cell = [[ZSTEComBOrderInfoJifenCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ZSTEComBOrderInfoJifenCellName];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell setEditing:NO];
            }
            
            [cell setInfoStatesBlock:^(ZSTEComBOrderInfoJifenCell *cell,int states) {
                
                [self reloadViewWith:states];
            }];
            
            [cell jifenpay:isjifen pointnum:pointNum deductionamount:deductionAmount];
            
            if (pointNum == 0) {
                cell.switchOne.userInteractionEnabled = NO;
            }
            return  cell;
            
        }
        
    }else if(indexPath.section==2){
        
        static NSString * ZSTEComBOrderInfoInformationCellName = @"ZSTEComBOrderInfoInformationCellName";
        ZSTEComBOrderInfoInformationCell * cell = [tableView dequeueReusableCellWithIdentifier:ZSTEComBOrderInfoInformationCellName];
        if (!cell) {
            cell = [[ZSTEComBOrderInfoInformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ZSTEComBOrderInfoInformationCellName];
            //cell.delegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        [cell reloadData:[_orderGools objectAtIndex:indexPath.row]];
        return  cell;
        
    }

    return  nil;
}

//对应事件
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==1) {
        
        if (indexPath.row==0) {
            _payWay = zhifubaon;
        }else if (indexPath.row==1){
            _payWay = huodaon;
        }
        
        finalPayType = _payWay;
        
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        if (indexPath.row ==currentIndex) {
            return;
        }
        NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:currentIndex
                                                       inSection:1];
        ZSTEComBOrderInfoPayCell *newCell = (ZSTEComBOrderInfoPayCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        ZSTEComBOrderInfoPayCell *oldCell = (ZSTEComBOrderInfoPayCell *)[self.tableView cellForRowAtIndexPath:oldIndexPath];
        
        if ([newCell.statusLabel.text isEqualToString:@"0"]) {
            
            newCell.selectedImageView.image = ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png");
            newCell.statusLabel.text = @"1";
            
        }
        if ([oldCell.statusLabel.text isEqualToString:@"1"]) {
            oldCell.selectedImageView.image = ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png");
            oldCell.statusLabel.text = @"0";
        }
        
        currentIndex=indexPath.row;
        
    }
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return  nil;
    }
    
    UILabel * label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(10, 15, 320, 20);
    //    label.backgroundColor = [UIColor clearColor];
    //    label.backgroundColor = [UIColor redColor];
    label.font=[UIFont fontWithName:@"Arial" size:14];
    label.text = sectionTitle;
    
    UIView * sectionView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 35)] autorelease];
    [sectionView setBackgroundColor:[UIColor whiteColor]];
    [sectionView addSubview:label];
    return sectionView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;//0.1f;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section ==0) {
        return @"收货信息";
    }
    
    if (section ==1) {
        return @"支付方式";
    }
    
    if (section ==2) {
        return @"订单明细";
    }
    return @"";
}


-(void)goPayAction:(UIButton *)btn
{
    [self submitBtnDidClicked];
}


-(void)selectedPayWay:(payWayn)payWay
{
    _payWay = payWay;
    
}

- (void) reloadViewWith:(int)states
{
    
    if (states == 0) {
        
        isjifenpay = YES;
        
        _totalAmount = tmpprice - deductionAmount;
        
    } else {
        
        isjifenpay = NO;
        
        _totalAmount = tmpprice;
    }
    
    //    ZSTEComBOrderInfoViewControllerResponseCell *tableCell = (ZSTEComBOrderInfoViewControllerResponseCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3]];
    //    tableCell.priceLabel.text = [NSString stringWithFormat:@"￥%.2f",_totalAmount];
    priceLabel.text =@"";
    priceLabel.text = [NSString stringWithFormat:@"￥%.2f",_totalAmount];
}

#pragma mark - 确定付款
-(void)submitBtnDidClicked
{
    if (!_haveAddress) {
        [TKUIUtil alertInWindow:@"请填写地址" withImage:nil];
        return;
    }
    
    if (isjifenpay) {
        
        [self.engine jifenUsePay:_orderID costamount:deductionAmount pointnum:pointNum];
        
        if (deductionAmount >= _totalAmount) {
            
            [self.engine jifenPay:_orderID];
            
            return;
        }
    }
    
    NSLog(@"==你选择的付款方式是==%d",_payWay);
    
    if (_payWay == zhifubaon) {
        ZSTEComBPaymentViewController *webView = [[ZSTEComBPaymentViewController alloc] init];
        [webView setURL:[NSString stringWithFormat:ALIPAYTRADE,_orderID]];
        webView.hidesBottomBarWhenPushed = YES;
        webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
        [self.navigationController pushViewController:webView animated:YES];
        [webView release];
    }
    else if(_payWay == huodaon){
        NSLog(@"货到付款");
        [self.engine huodaoPay:_orderID];
    } else if(_payWay == yinlianpay){
        ZSTEComBPaymentViewController *webView = [[ZSTEComBPaymentViewController alloc] init];
        [webView setURL:[NSString stringWithFormat:@"%@orderid=%@&moduletype=%ld&ecid=%@",YINLIANPAY,_orderID,self.moduleType,[ZSTF3Preferences shared].ECECCID]];
        webView.hidesBottomBarWhenPushed = YES;
        webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
        [self.navigationController pushViewController:webView animated:YES];
        [webView release];
        
    }
    
}


#pragma mark - 货到付款返回的状态
#pragma mark 货到付款成功
-(void)huodaoPayDidSucceed
{
    NSLog(@"货到付款成功");
    ZSTEComBPaymentViewController *webView = [[ZSTEComBPaymentViewController alloc] init];
    [webView setURL:[NSString stringWithFormat:PAYRESULTS,3,[ZSTF3Preferences shared].ECECCID,self.moduleType,_orderID]];
    webView.hidesBottomBarWhenPushed = YES;
    webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    [self.navigationController pushViewController:webView animated:YES];
    [webView release];
}

#pragma mark 货到付款失败
-(void)huodaoPayDidFailed:(int)resultCode
{
    NSLog(@"货到付款失败");
    ZSTEComBPaymentViewController *webView = [[ZSTEComBPaymentViewController alloc] init];
    [webView setURL:[NSString stringWithFormat:PAYRESULTS,2,[ZSTF3Preferences shared].ECECCID,self.moduleType,_orderID]];
    webView.hidesBottomBarWhenPushed = YES;
    webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    [self.navigationController pushViewController:webView animated:YES];
    [webView release];
}




-(void)hideSubmit:(BOOL)hide
{
    _hideSubmit = hide;
    [_tableView reloadData];
}

#pragma mark - UIAlertView delegate

- (void)popOver:(ZSTECBPopOver *)popOver clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)dealloc{
    [_payNameArray release];
    
    [super dealloc];
}

- (void)getEcomBFreightInfoViewDidSucceed:(double)amount AndFreight:(double)totalFreight
{
    NSLog(@"guessSecond");
    tmpprice = amount;
    //    _totalAmount = amount;
    priceLabel.text = [NSString stringWithFormat:@"￥%.2f",amount];
    if (totalFreight > 0)
    {
        l3.text = [NSString stringWithFormat:@"(含运费%@元)",@(totalFreight)];
    }
    else
    {
        l3.text = @"(包邮)";
    }
    _totalFreight = totalFreight;
    [_tableView reloadData];
    
    
    [self.engine getjifenPayInfoWithOrderId:_orderID costamount:amount];
}

- (void)getEcomBFreightInfoViewDidFailed:(int)resultCode
{
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [TKUIUtil hiddenHUD];
}

@end
