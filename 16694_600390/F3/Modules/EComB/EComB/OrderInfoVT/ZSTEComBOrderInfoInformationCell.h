//
//  ZSTEComBOrderInfoInformationCell.h
//  EComB
//
//  Created by qiuguian on 15/7/27.
//  Copyright (c) 2015年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTEComBOrderInfoInformationCell : UITableViewCell

@property(retain,nonatomic) UILabel * goalInfoLabel;
@property(retain,nonatomic) UILabel * goalPriceLabel;
@property(retain,nonatomic) UILabel * goalCountLabel;

@property (retain, nonatomic) UILabel *pointLabel;

-(void)reloadData:(EcombOrderGools *)gools;

@end
