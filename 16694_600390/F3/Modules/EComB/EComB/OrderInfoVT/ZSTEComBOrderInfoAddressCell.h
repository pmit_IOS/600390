//
//  ZSTEComBOrderInfoAddressCell.h
//  EComB
//
//  Created by anan on 7/26/15.
//  Copyright (c) 2015 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTEComBOrderInfoAddressCellDelegateNew <NSObject>

-(void)addressDidPressedAction;

@end

@interface ZSTEComBOrderInfoAddressCell : UITableViewCell
@property (assign,nonatomic) id<ZSTEComBOrderInfoAddressCellDelegateNew> delegate;
@property (retain,nonatomic) UILabel * addressLabel;
@property (retain,nonatomic) UILabel * tellLabel;

-(void) initData:(EcomBAddress *)address;
@end
