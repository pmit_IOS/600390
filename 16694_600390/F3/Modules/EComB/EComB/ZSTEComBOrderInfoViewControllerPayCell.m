//
//  ZSTEComBOrderInfoViewControllerPayCell.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBOrderInfoViewControllerPayCell.h"

@implementation ZSTEComBOrderInfoViewControllerPayCell
{
    ZSTRoundRectView * _round;
    ZSTRoundRectView * _line;
    UIView * _zhifubaoView;
    UIView * _huodaoView;
    UIView * _yinlianView;
    UIView * _jifenView;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
//        UIImageView * bg = [[UIImageView alloc]init];
//        bg.frame = CGRectMake(5, 30, 310, 81);
//        bg.image = ZSTModuleImage(@"module_ecomb_orderinfo_paycell_bg.png");
//        [self.contentView addSubview:bg];
//        [bg release];
        
        _round = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(5, 30, 310, 131) radius:4.0 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
        _round.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_round];
        [_round release];
        
        _line = [[ZSTRoundRectView alloc]initWithPoint:CGPointMake(0, 40) toPoint:CGPointMake(310, 40) borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
        [_round addSubview:_line];
        [_line release];
        
        UILabel * l1 = [[UILabel alloc]init];
        l1.font = [UIFont systemFontOfSize:17];
        l1.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
        l1.text = @"付款方式";
        [self.contentView addSubview:l1];
        CGSize size1 = [l1.text sizeWithFont:l1.font constrainedToSize:CGSizeMake(235, 44) lineBreakMode:NSLineBreakByWordWrapping];
        l1.backgroundColor = [UIColor clearColor];
        l1.frame = CGRectMake(5,  1, size1.width, 30);
        [l1 release];
        
        _zhifubaoView = [[UIView alloc]init];
        _zhifubaoView.frame = CGRectMake(0, 0, 310, 40);
        [_round addSubview:_zhifubaoView];
        [_zhifubaoView release];
        
        UIImageView * zhifubao  = [[UIImageView alloc]init];
        zhifubao.frame = CGRectMake(12, 10, 53, 18);
        zhifubao.image = ZSTModuleImage(@"module_ecomb_orderinfo_zhifubao.png");
        [_zhifubaoView addSubview:zhifubao];
        [zhifubao release];
        
        _zhifubaoImageView = [[UIImageView alloc]init];
        _zhifubaoImageView.frame = CGRectMake(320 - 15 - 15, CGRectGetMinY(zhifubao.frame), 15, 15);
        [_zhifubaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png")];
        _zhifubaoImageView.userInteractionEnabled = YES;
        [_zhifubaoView addSubview:_zhifubaoImageView];
        UIControl * zhifubaoControl = [[UIControl alloc]initWithFrame:_zhifubaoView.bounds];
        [zhifubaoControl addTarget:self action:@selector(payAction:) forControlEvents:UIControlEventTouchUpInside];
        zhifubaoControl.tag = 0;
//        zhifubaoControl.backgroundColor = [UIColor yellowColor];
        [_zhifubaoView addSubview:zhifubaoControl];
        
        _huodaoView = [[UIView alloc]init];
        _huodaoView.frame = CGRectMake(0, 40, 310, 40);
        [_round addSubview:_huodaoView];
        [_huodaoView release];
        
        UILabel * l2 = [[UILabel alloc]init];
        l2.font = [UIFont systemFontOfSize:14];
        l2.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
        l2.text = @"货到付款";
        [_huodaoView addSubview:l2];
        CGSize size2 = [l2.text sizeWithFont:l2.font constrainedToSize:CGSizeMake(235, 44) lineBreakMode:NSLineBreakByWordWrapping];
        l2.frame = CGRectMake(12, 10, size2.width, 22);
        [l2 release];
        
        _huodaoImageView = [[UIImageView alloc]init];
        _huodaoImageView.frame = CGRectMake(320 - 15 - 15, CGRectGetMinY(l2.frame) + 3, 15, 15);
        [_huodaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        _huodaoImageView.userInteractionEnabled = YES;
        [_huodaoView addSubview:_huodaoImageView];
        UIControl * huodaoControl = [[UIControl alloc]initWithFrame:_huodaoView.bounds];
        [huodaoControl addTarget:self action:@selector(payAction:) forControlEvents:UIControlEventTouchUpInside];
        huodaoControl.tag = 1;
//        huodaoControl.backgroundColor = [UIColor yellowColor];
        [_huodaoView addSubview:huodaoControl];
        
//        _line = [[ZSTRoundRectView alloc]initWithPoint:CGPointMake(0, 80) toPoint:CGPointMake(310, 80) borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
//        [_round addSubview:_line];
//        [_line release];
        
        _yinlianView = [[UIView alloc] init];
        _yinlianView.frame = CGRectMake(0, 80, 310, 40);
        [_round addSubview:_yinlianView];
        
        UIImageView * yinlian  = [[UIImageView alloc]init];
        yinlian.frame = CGRectMake(12, 10, 53, 18);
        yinlian.image = ZSTModuleImage(@"module_ecomb_orderinfo_yinlian.png");
        [_yinlianView addSubview:yinlian];
        [yinlian release];
        
        _yinlianImageView = [[UIImageView alloc] init];
        _yinlianImageView.frame = CGRectMake(320 - 15 - 15, CGRectGetMinY(yinlian.frame) + 3, 15, 15);
        [_yinlianImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        _yinlianImageView.userInteractionEnabled = YES;
        [_yinlianView addSubview:_yinlianImageView];
        UIControl * yinlianControl = [[UIControl alloc]initWithFrame:_yinlianView.bounds];
        [yinlianControl addTarget:self action:@selector(payAction:) forControlEvents:UIControlEventTouchUpInside];
        yinlianControl.backgroundColor = [UIColor clearColor];
        yinlianControl.tag = 2;
        [_yinlianView addSubview:yinlianControl];
        
        _jifenView = [[UIView alloc] initWithFrame:CGRectMake(0, 120, 310, 60)];
        _jifenView.backgroundColor = [UIColor clearColor];
        [_round addSubview:_jifenView];
        
        UILabel * l3 = [[UILabel alloc]init];
        l3.font = [UIFont systemFontOfSize:14];
        l3.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
        l3.text = @"积分支付";
        [_jifenView addSubview:l3];
        CGSize size3 = [l3.text sizeWithFont:l3.font constrainedToSize:CGSizeMake(235, 44) lineBreakMode:NSLineBreakByWordWrapping];
        l3.frame = CGRectMake(12, 8, size3.width, 22);
        [l3 release];
        
        _jifenLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 30, 280, 18)];
        _jifenLabel.backgroundColor = [UIColor clearColor];
        _jifenLabel.textColor = RGBCOLOR(166, 166, 166);
        _jifenLabel.font = [UIFont systemFontOfSize:12];
        [_jifenView addSubview:_jifenLabel];
        
        _switchOne = [[PMCustomSwitch alloc] initWithFrame:CGRectMake(250, (self.frame.size.height - 21)/2+2, 42, 21)];
//        _switchOne.arrange = ZSTCustomSwitchArrangeONLeftOFFRight;
//        _switchOne.onImage = ZSTModuleImage(@"module_ecomb_switch_on.png");
//        _switchOne.offImage = ZSTModuleImage(@"module_ecomb_switch_off.png");
        _switchOne.status = PMCustomSwitchStatusOff;
        _switchOne.delegate = self;
        _switchOne.userInteractionEnabled = YES;
        [_jifenView addSubview:_switchOne];

        UILabel * l4 = [[UILabel alloc]init];
        l4.font = [UIFont systemFontOfSize:17];
        l4.frame = CGRectMake(5,  CGRectGetMaxY(_round.frame) + 45 , 320, 30);
        l4.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
        l4.backgroundColor = [UIColor clearColor];
        l4.text = @"商品明细";
        [self.contentView addSubview:l4];
        [l4 release];
        
        _cellHeight = 41;
    }
    return self;
}
-(void)payAction:(UIControl *)control
{
    if (control.tag == 0) {
        NSLog(@"支付宝");
        [_huodaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        [_zhifubaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png")];
        [_yinlianImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        [_delegate selectedPayWay:zhifubao];
    }
    else if (control.tag == 1)
    {
        NSLog(@"货到付款");
        [_zhifubaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        [_huodaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png")];
        [_yinlianImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        [_delegate selectedPayWay:huodao];
    } else if (control.tag == 2) {
        
        [_yinlianImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png")];
        [_zhifubaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        [_huodaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        [_delegate selectedPayWay:yinlianpay];
    }
}
//1 网银 2 货到 3 都显示
-(void)hidden:(int)state
{
    if (state == 1) {
        _line.hidden = YES;
        _round.frame = CGRectMake(5, 30, 310, 41);
        _huodaoView.hidden = YES;
        _huodaoView.userInteractionEnabled = NO;
        [_zhifubaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png")];
    }
    else if (state == 2) {
        _line.hidden = YES;
        _round.frame = CGRectMake(5, 30, 310, 41);
        _zhifubaoView.hidden = YES;
        _zhifubaoView.userInteractionEnabled = NO;
        _huodaoView.frame = _zhifubaoView.frame;
        [_huodaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png")];
    }
    
    _cellHeight = _round.frame.size.height;
}

- (void)payState:(BOOL)state
{
    if (!state) {
        _line.hidden = YES;
        _yinlianView.hidden = YES;
        
        if (!_zhifubaoView.hidden && !_huodaoView.hidden) {
            
            _round.frame = CGRectMake(5, 30, 310, 81);
        } else {
            
             _round.frame = CGRectMake(5, 30, 310, 41);
        }
    } else {
        
        _line.hidden = NO;
        _yinlianView.hidden = NO;
        _round.frame = CGRectMake(5, 30, 310, 121);
        
        [_zhifubaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png")];
        [_huodaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        [_yinlianImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        
        if (_zhifubaoView.hidden && !_huodaoView.hidden) {
            
            _huodaoView.frame = CGRectMake(0, 0, 310, 41);
            _yinlianView.frame = CGRectMake(0, 40, 310, 41);
            _round.frame = CGRectMake(5, 30, 310, 81);
            [_huodaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png")];
            
        } else if(!_zhifubaoView.hidden && _huodaoView.hidden) {
            
            _yinlianView.frame = CGRectMake(0, 40, 310, 41);
            _round.frame = CGRectMake(5, 30, 310, 81);
            [_zhifubaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png")];
        }
    }
    
    _cellHeight = _round.frame.size.height;
}

- (void)jifenpay:(int)state pointnum:(int)pointnum deductionamount:(float)deductionamount
{
    if (!state) {
        
        _jifenView.hidden = YES;
        _line.hidden = YES;
        
        if (!_yinlianView.hidden && !_huodaoView.hidden) {
            
            _round.frame = CGRectMake(5, 30, 310, 131);
        } else if (_yinlianView.hidden && !_huodaoView.hidden){
            
            _round.frame = CGRectMake(5, 30, 310, 81);
        } else {
            
            _round.frame = CGRectMake(5, 30, 310, 81);
        }
    } else {
        
        _line.hidden = NO;
        _jifenView.hidden = NO;
        _zhifubaoView.hidden = NO;
        _round.frame = CGRectMake(5, 30, 310, 181);
        [_zhifubaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_on.png")];
        [_huodaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        [_yinlianImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        
        if (_yinlianView.hidden && !_huodaoView.hidden) {
            
            _huodaoView.frame = CGRectMake(0, 41, 310, 41);
            _jifenView.frame = CGRectMake(0, 81, 310, 50);
            _round.frame = CGRectMake(5, 30, 310, 141);
            [_huodaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
            
        } else if(!_yinlianView.hidden && _huodaoView.hidden) {
            
            _yinlianView.frame = CGRectMake(0, 40, 310, 41);
            _jifenView.frame = CGRectMake(0, 81, 310, 50);
            _round.frame = CGRectMake(5, 30, 310, 141);
            [_zhifubaoImageView setImage:ZSTModuleImage(@"module_ecomb_orderinfo_pay_off.png")];
        } else {
            
            _jifenView.frame = CGRectMake(0, 41, 310, 50);
            _round.frame = CGRectMake(5, 30, 310, 101);
        }
        
        _jifenLabel.text = [NSString stringWithFormat:@"可用%d积分，可抵¥%.2f",pointnum,deductionamount];
        
//        _switchOne.enabled = YES;
        _switchOne.userInteractionEnabled = YES;
    }
    
    if (state == 2) {
        
//        _switchOne.enabled = NO;
        _switchOne.userInteractionEnabled = NO;
        
        _jifenLabel.text = @"部分商品不支持积分换购，暂不可用";
    }
    
    _cellHeight = _round.frame.size.height;
}

- (CGFloat)cellHeight
{
    return _cellHeight;
}

- (void)setInfoStatesBlock:(EcbinfoStatesBlock)block
{
    statesBlock = [block copy];
}

#pragma mark - customSwitch delegate
-(void)zstcustomSwitchSetStatus:(PMCustomSwitchStatus)status
{
    if (!statesBlock) {
        
        return;
    }
    
    statesBlock(self,status);
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
