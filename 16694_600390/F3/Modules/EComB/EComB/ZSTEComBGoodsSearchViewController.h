//
//  ZSTEComBGoodsSearchViewController.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECBProductTableImageCell.h"

@interface ZSTEComBGoodsSearchViewController :UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ZSTECBProductTableImageCellDelegate,ZSTF3EngineECBDelegate,TKLoadMoreViewDelegate>
@property (retain,nonatomic)UIView * headView;
@property (retain,nonatomic)UITableView * tableView;
@property (retain,nonatomic)NSMutableArray * resultData;
@end
