//
//  ZSTEComBAskOnLineTableViewCell.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-22.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBAskOnLineTableViewCell.h"

@implementation ZSTEComBAskOnLineTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        askLabel = [[UILabel alloc]init];
        askLabel.numberOfLines = 0;
        askLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        askLabel.font = [UIFont systemFontOfSize:15];
        askLabel.textColor = RGBCOLOR(177, 177, 177);
        askLabel.backgroundColor = RGBCOLOR(243, 243, 243);
        [self.contentView addSubview:askLabel];
        
        answerLabel = [[UILabel alloc]init];
        answerLabel.numberOfLines = 0;
        answerLabel.backgroundColor = [UIColor clearColor];
        answerLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        answerLabel.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:answerLabel];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)reloadData:(EcomBAskInfo *)askInfo
{
    askLabel.text = [NSString stringWithFormat:@"    问：%@",askInfo.EcomBAskInfoQuestion];
    CGSize  askSize = [askInfo.EcomBAskInfoQuestion sizeWithFont:askLabel.font constrainedToSize:CGSizeMake(320, 500) lineBreakMode:NSLineBreakByTruncatingMiddle];
    askLabel.frame = CGRectMake(0, 0,320, askSize.height + 25);
    
    answerLabel.text = [NSString stringWithFormat:@"    答：%@",askInfo.EcomBAskInfoAnswer];
    CGSize  answerSize = [askInfo.EcomBAskInfoAnswer sizeWithFont:answerLabel.font constrainedToSize:CGSizeMake(320, 500) lineBreakMode:NSLineBreakByTruncatingMiddle];
    answerLabel.frame = CGRectMake(0, askLabel.frame.size.height,320, answerSize.height + 25);
}

@end
