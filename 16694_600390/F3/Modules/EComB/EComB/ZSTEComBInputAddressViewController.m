//
//  ZSTEComBInputAddressViewController.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBInputAddressViewController.h"
#import <PMRepairButton.h>
#import "ZSTEComBOrderInfomationViewController.h"
#import <addressUtil/HZAreaPickerView.h>

@interface ZSTEComBInputAddressViewController () <HZAreaPickerDelegate>

@property (retain,nonatomic) HZAreaPickerView *locatePicker;
@property (retain,nonatomic) NSString *areaID;

@end

@implementation ZSTEComBInputAddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
     self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"修改地址", nil)];
    self.view.backgroundColor = [UIColor whiteColor];
    
    _scrollView = [[UIScrollView alloc]init];
    _scrollView.frame = CGRectMake(0, 0, 320, 480 - 44 + (iPhone5?88:0));
    _scrollView.contentSize = CGSizeMake(320, 480 - 44 + (iPhone5?88:0) + 30);
    _scrollView.delegate = self;
    [self.view addSubview:_scrollView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignResponse:)];
    [self.scrollView addGestureRecognizer:tap];
    
    UILabel * l1 = [[UILabel alloc]init];
    l1.frame = CGRectMake(15, 10, 320, 22);
    l1.font = [UIFont boldSystemFontOfSize:14];
    l1.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
    l1.text = @"收货人";
    [_scrollView addSubview:l1];
    [l1 release];

    ZSTRoundRectView * nameRoundView = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(l1.frame) + 5, 300, 40) radius:4 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
    [_scrollView addSubview:nameRoundView];
    _nameField = [[UITextField alloc]init];
    _nameField.frame = CGRectMake(10, 0 + (IS_IOS_7?0:10), 300, 40);
    _nameField.borderStyle = UITextBorderStyleNone;
    _nameField.font = [UIFont systemFontOfSize:14];
    _nameField.placeholder = @"请在此输入姓名";
    _nameField.delegate = self;
    [nameRoundView addSubview:_nameField];
    
    
    UILabel * l2 = [[UILabel alloc]init];
    l2.frame = CGRectMake(15,CGRectGetMaxY(nameRoundView.frame) + 10, 320, 22);
    l2.font = [UIFont boldSystemFontOfSize:14];
    l2.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
    l2.text = @"收货人手机";
    [_scrollView addSubview:l2];
    [l2 release];
    
    
    ZSTRoundRectView * phoneRoundView = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(l2.frame) + 5, 300, 40) radius:4 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
    [_scrollView addSubview:phoneRoundView];
    _phoneField = [[UITextField alloc]init];
    _phoneField.frame = CGRectMake(10, 0 + (IS_IOS_7?0:10), 300, 40);
    _phoneField.borderStyle = UITextBorderStyleNone;
    _phoneField.font = [UIFont systemFontOfSize:14];
    _phoneField.placeholder = @"请在此输入手机号";
    _phoneField.delegate = self;
    [phoneRoundView addSubview:_phoneField];
    
    
    
    UILabel * l3 = [[UILabel alloc]init];
    l3.frame = CGRectMake(15,CGRectGetMaxY(phoneRoundView.frame) + 10, 320, 22);
    l3.font = [UIFont boldSystemFontOfSize:14];
    l3.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
    l3.text = @"所在地区";
    [_scrollView addSubview:l3];
    [l3 release];
    
    
    ZSTRoundRectView * addressRoundView = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(l3.frame) + 5, 300, 40) radius:4 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
    [_scrollView addSubview:addressRoundView];
    _addressField = [[UITextField alloc]init];
    _addressField.frame = CGRectMake(10, 0 + (IS_IOS_7?0:10), 300, 40);
    _addressField.font = [UIFont systemFontOfSize:14];
    _addressField.borderStyle = UITextBorderStyleNone;
    _addressField.placeholder = @"请点此选择地区";
    _addressField.delegate = self;
    [addressRoundView addSubview:_addressField];
    
    UIButton *seletctedAddress = [UIButton buttonWithType:UIButtonTypeCustom];
    seletctedAddress.frame = addressRoundView.frame;
    [_scrollView addSubview:seletctedAddress];
    [seletctedAddress addTarget:self action:@selector(showAddressPicker:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *l4 = [[UILabel alloc] init];
    l4.frame = CGRectMake(15,CGRectGetMaxY(addressRoundView.frame) + 10, 320, 22);
    l4.font = [UIFont boldSystemFontOfSize:14];
    l4.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
    l4.text = @"详细地址";
    [_scrollView addSubview:l4];
    [l4 release];
    
    ZSTRoundRectView *detailAddressRoundView = [[ZSTRoundRectView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(l4.frame) + 5, 300, 40) radius:4 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
    [_scrollView addSubview:detailAddressRoundView];
    _detailAddressField = [[UITextField alloc] init];
    _detailAddressField.frame = CGRectMake(10, 0 + (IS_IOS_7?0:10), 300, 40);
    _detailAddressField.font = [UIFont systemFontOfSize:14];
    _detailAddressField.borderStyle = UITextBorderStyleNone;
    _detailAddressField.placeholder = @"请填写详细地址";
    _detailAddressField.delegate = self;
    [detailAddressRoundView addSubview:_detailAddressField];
    
    UILabel * l5 = [[UILabel alloc]init];
    l5.frame = CGRectMake(15,CGRectGetMaxY(detailAddressRoundView.frame) + 10, 320, 22);
    l5.font = [UIFont boldSystemFontOfSize:14];
    l5.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
    l5.text = @"邮编";
    [_scrollView addSubview:l5];
    [l5 release];
    
    ZSTRoundRectView * addressNumberRoundView = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(l5.frame) + 5, 300, 40) radius:4 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
    [_scrollView addSubview:addressNumberRoundView];
    _addressNumberField = [[UITextField alloc]init];
    _addressNumberField.frame = CGRectMake(10, 0 + (IS_IOS_7?0:10), 300, 40);
    _addressNumberField.borderStyle = UITextBorderStyleNone;
    _addressNumberField.font = [UIFont systemFontOfSize:14];
    _addressNumberField.placeholder = @"请在此输入邮编（选填）";
    _addressNumberField.delegate = self;
    [addressNumberRoundView addSubview:_addressNumberField];
    
    
//    UIButton * delBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    PMRepairButton *delBtn = [[PMRepairButton alloc] init];
    delBtn.frame = CGRectMake(10, CGRectGetMaxY(addressNumberRoundView.frame) + 10, 140, 35);
    [delBtn setImage:ZSTModuleImage(@"module_ecomb_orderinfo_del.png") forState:UIControlStateNormal];
    delBtn.tag = 1;
    [delBtn addTarget:self action:@selector(saveAddrAction:) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:delBtn];

    
//    UIButton * submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    PMRepairButton *submitBtn = [[PMRepairButton alloc] init];
    submitBtn.frame = CGRectMake(320 - 10 - 140, CGRectGetMaxY(addressNumberRoundView.frame) + 10, 140, 35);
    [submitBtn setImage:ZSTModuleImage(@"module_ecomb_orderinfo_save.png") forState:UIControlStateNormal];
    submitBtn.tag = 0;
    [submitBtn addTarget:self action:@selector(saveAddrAction:) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:submitBtn];

    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:YES];
    
    [self initAddress:_address];
    
}
- (BOOL) isValidPost:(NSString*)post
{
    NSString *emailRegex = @"[0-9]{6,6}";//@"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if (post == nil || post.length == 0)
    {
//        [TKUIUtil alertInWindow:@"邮编不为空" withImage:nil];
        return YES;
    } else {
        if (post.length < 6) {
            [TKUIUtil alertInWindow:@"邮编不能小于6位" withImage:nil];
            return NO;
        }
    
        if (![emailTest evaluateWithObject:post]) {
            [TKUIUtil alertInWindow:@"邮编格式不正确" withImage:nil];
            return NO;
        }
    }
    return [emailTest evaluateWithObject:post];
}
- (BOOL) isValidMobileNumber:(NSString*)string
{
    if (string.length == 0) {
        [TKUIUtil alertInWindow:@"号码不能为空" withImage:nil];
        return NO;
    }
    
//    if (string.length < 6) {
//        [TKUIUtil alertInWindow:@"号码不能少于6位" withImage:nil];
//        return NO;
//    }
//    NSError *error;
//    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber
//                                                               error:&error];
//    NSUInteger numberOfMatches = [detector numberOfMatchesInString:string
//                                                           options:0
//                                                             range:NSMakeRange(0, [string length])];
//    if (numberOfMatches>0) {
//        return YES;
//    }
//    [TKUIUtil alertInWindow:@"号码格式不正确" withImage:nil];
    
    if(![self validateMobile:string]) {
        
        [TKUIUtil alertInWindow:@"号码格式不正确" withImage:nil];
        return NO;
    }
    
    return YES;
}

- (BOOL)validateMobile:(NSString *)mobileNum
{
        /**
         * 手机号码
         * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
         * 联通：130,131,132,152,155,156,185,186
         * 电信：133,1349,153,180,189
         */
        NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
        /**
         10         * 中国移动：China Mobile
         11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
         12         */
        NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
        /**
         15         * 中国联通：China Unicom
         16         * 130,131,132,152,155,156,185,186
         17         */
        NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
        /**
         20         * 中国电信：China Telecom
         21         * 133,1349,153,180,189
         22         */
        NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
        /**
         25         * 大陆地区固话及小灵通
         26         * 区号：010,020,021,022,023,024,025,027,028,029
         27         * 号码：七位或八位
         28         */
        // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
        
        NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
        NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
        NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
        NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
        
        if (([regextestmobile evaluateWithObject:mobileNum] == YES)
            || ([regextestcm evaluateWithObject:mobileNum] == YES)
            || ([regextestct evaluateWithObject:mobileNum] == YES)
            || ([regextestcu evaluateWithObject:mobileNum] == YES))
        {  
            return YES;  
        }  
        else  
        {  
            return NO;  
        }  
}


-(BOOL)isValidAdd:(NSString *)address
{
    if (address.length == 0) {
        [TKUIUtil alertInWindow:@"所在区域不能为空" withImage:nil];
        return NO;
    }
    return YES;
}

-(BOOL)isValidName:(NSString *)name
{
    if (name.length == 0) {
        [TKUIUtil alertInWindow:@"姓名不能为空" withImage:nil];
        return NO;
    }
    return YES;
}

-(BOOL)isValidDetailAddress:(NSString *)detailAddress
{
    if (detailAddress.length == 0) {
        [TKUIUtil alertInWindow:@"详细地址不能为空" withImage:nil];
        return NO;
    }
    return YES;
}

-(BOOL)isValidAddress:(EcomBAddress *)address
{
    
    if (![self isValidName:address.ecomUserName]) {
        return NO;
    }
    if (![self isValidMobileNumber:address.ecomMobile]) {
        
        return NO;
    }
    
    if (![self isValidAdd:address.ecomAddress]) {
        return NO;
    }
    if (![self isValidPost:address.ecomPostNum]) {
        return NO;
    }
    
    if (![self isValidDetailAddress:address.ecomDetailAddress])
    {
        return NO;
    }
    
    return YES;
}

-(void)saveAddrAction:(UIButton *)btn
{
    if (btn.tag == 0) {
        
        _address.ecomPostNum = _addressNumberField.text;
        _address.ecomAddress = _addressField.text;
        _address.ecomMobile = _phoneField.text;
        _address.ecomUserName = _nameField.text;
        _address.ecomDetailAddress = _detailAddressField.text;
        if ([self isValidAddress:_address]) {
            [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在保存,请稍后..." , nil)];
            [self.engine saveEcomBAddress:_address];
        }
        
    }
    else if(btn.tag == 1)
    {
        
        _addressNumberField.text = nil;
        _addressField.text = nil;
        _phoneField.text = nil;
        _nameField.text = nil;
        
        _address.ecomPostNum = @" ";
        _address.ecomAddress = @" ";
        _address.ecomMobile = @" ";
        _address.ecomUserName = @" ";
        //        [self.engine saveEcomBAddress:_address];
    }
    else
    {
        NSLog(@"地址提交失败");
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [_addressNumberField resignFirstResponder];
    [_addressField resignFirstResponder];
    [_phoneField resignFirstResponder];
    [_nameField resignFirstResponder];
    [self cancelEditLocatePicker];
}
-(void)saveEComBAddressdidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
    NSLog(@"保存失败  --- %d",resultCode);
}


-(void)saveEComBAddressdidSucceed
{
    [TKUIUtil hiddenHUD];
    if (self.isNullFrom)
    {
        ZSTEComBOrderInfomationViewController *infoViewController = [[ZSTEComBOrderInfomationViewController alloc] init];
        [infoViewController hideSubmit:YES];
        infoViewController.orderID = _orderID;
        infoViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:infoViewController animated:YES];
        [infoViewController release];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}


-(void)initAddress:(EcomBAddress *)addr
{
    if (addr == nil) {
        _address = [[EcomBAddress alloc]init];
        return;
    }
    else _address = addr;
    
    _nameField.text = addr.ecomUserName;
    _phoneField.text = addr.ecomMobile;
    _addressField.text = addr.ecomAddress;
    _addressNumberField.text = addr.ecomPostNum;
    _detailAddressField.text = addr.ecomDetailAddress;
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    _scrollView.frame = CGRectMake(0, 0, 320,self.view.frame.size.height - keyboardRect.size.height);
    
}

- (void)showAddressPicker:(UIButton *)sender
{
    if(!self.locatePicker)
        self.locatePicker = [[HZAreaPickerView alloc] initWithStyle:HZAreaPickerWithStateAndCityAndDistrict delegate:self];
    if ([_addressField.text isEqualToString:@""])
    {
        self.areaValue = @"北京北京市";
        self.areaID = @"36";
    }
    else
    {
        [self.locatePicker selectDefaultCity:_addressField.text];
    }
    
    
    [_nameField resignFirstResponder];
    [_phoneField resignFirstResponder];
    [_detailAddressField resignFirstResponder];
    [_addressNumberField resignFirstResponder];
    [self.locatePicker showInView:self.view];
}


- (void)keyboardWillHide:(NSNotification *)notification {
    
    _scrollView.frame = self.view.bounds;
    
}

#pragma mark - HZAreaPicker delegate
- (void)pickerDidChaneStatus:(HZAreaPickerView *)picker
{
    self.areaValue = [NSString stringWithFormat:@"%@%@", picker.locate.state, picker.locate.city];
    self.areaID = picker.locate.cityId;
}

- (void)setAreaID:(NSString *)areaID
{
    if (![_areaID isEqualToString:areaID]) {
        
        _address.ecomCityId = areaID;
    }
}

- (void)cancelEditLocatePicker
{
    [self.locatePicker cancelPicker];
}

- (void)setAreaValue:(NSString *)areaValue
{
    if (![_areaValue isEqualToString:areaValue]) {
        
        _addressField.text = areaValue;
    }
}

- (void)resignResponse:(UITapGestureRecognizer *)tap
{
    [self cancelEditLocatePicker];
    [_nameField resignFirstResponder];
    [_phoneField resignFirstResponder];
    [_detailAddressField resignFirstResponder];
    [_addressNumberField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-(void)dealloc
//{
//    
//    [_addressField release];
//    [_nameField release];
//    [_phoneField release];
//    [_addressNumberField release];
//    [_scrollView release];
//    [super dealloc];
//}
@end
