//
//  ZSTF3Engine+EComB.m
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTF3Engine+EComB.h"
#import "ZSTSqlManager.h"
#import "ZSTDao+EComB.h"

@implementation ZSTF3Engine (EComB)

////////////////////////////////////////////////////////////////// category ///////////////////////////////////////////////////////////////////////

- (void)getProductBCategory
{
    NSString *loginMsisdn = @"";
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([ZSTF3Preferences shared].loginMsisdn && ![[ZSTF3Preferences shared].loginMsisdn isEqualToString:@""]) {
        loginMsisdn = [ZSTF3Preferences shared].loginMsisdn;
    }
    else
    {
        loginMsisdn = [ZSTF3Preferences shared].UserId;
    }
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:loginMsisdn forKey:@"Msisdn"];
    // category
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:GETSORTLIST,(long)self.moduleType,[ZSTF3Preferences shared].ECECCID]
                                         params:params
                                         target:self
                                       selector:@selector(getProductBCategoryResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:YES];
}

- (void)getProductBCategoryResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
//        BOOL showImg = ([[response.jsonResponse objectForKey:@"ShowImg"] caseInsensitiveCompare:@"true"] == NSOrderedSame);
//        [[NSUserDefaults standardUserDefaults] setBool:showImg forKey:@"showImgKey"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
        
        id categories = [response.jsonResponse safeObjectForKey:@"info"];
        if (![categories isKindOfClass:[NSArray class]]) {
            categories = [NSArray array];
        }
        
                [ZSTSqlManager beginTransaction];
                [self.dao deleteAllProductBCategories];
                for (NSDictionary *category in categories) {
                    id children = [category objectForKey:@"items"];
                    if (![children isKindOfClass:[NSArray class]]) {
                        children = [NSArray array];
                    }
                    
                    for (NSDictionary *child in children) {
                        [self.dao addecbCategory:[[child safeObjectForKey:@"categoryid"] integerValue]
                                    categoryName:[child safeObjectForKey:@"categoryname"]
                                        parentID:[[child safeObjectForKey:@"parentid"] integerValue]
                                        orderNum:[[child safeObjectForKey:@"ordernum"] integerValue]
                                     description:[child safeObjectForKey:@"description"]];
                    }
                }
                [ZSTSqlManager commit];
        
        if ([self isValidDelegateForSelector:@selector(getProductCategoryDidSucceed:)]) {
            [self.delegate getProductCategoryDidSucceed:categories];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getProductCategoryDidFailed:)]) {
            [self.delegate getProductCategoryDidFailed:response.resultCode];
        }
    }
    
}


////////////////////////////////////////////////////////////////// 获取收货地址 ///////////////////////////////////////////////////////////////////////
//收货地址
- (void)getEcomBAddress
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:GETADDRESS,[ZSTF3Preferences shared].loginMsisdn,(long)self.moduleType,[ZSTF3Preferences shared].ECECCID]
                                         params:params
                                         target:self
                                       selector:@selector(getEComBAddressResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:YES];
}


- (void)getEComBAddressResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id EComBAddress = [response.jsonResponse objectForKey:@"info"];
        if (![EComBAddress isKindOfClass:[NSArray class]]) {
            EComBAddress = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(getEComBAddressdidSucceed:)]) {
            [self.delegate getEComBAddressdidSucceed:[(NSArray *)EComBAddress objectAtIndex:0]];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getEComBAddressdidFailed:)]) {
            [self.delegate getEComBAddressdidFailed:response.resultCode];
        }
    }
    
}
////////////////////////////////////////////////////////////////// 修改收货地址 ///////////////////////////////////////////////////////////////////////


- (void)saveEcomBAddress:(EcomBAddress *)addr
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    //[params setSafeObject:addr.ecomUserName forKey:@"TEST"];
    NSString * url = [NSString stringWithFormat:ADDADDRESS,[ZSTF3Preferences shared].loginMsisdn,addr.ecomUserName,addr.ecomMobile,addr.ecomDetailAddress,addr.ecomPostNum,(long)self.moduleType,[ZSTF3Preferences shared].ECECCID,addr.ecomAddress,addr.ecomCityId];
    
    [[ZSTCommunicator shared] openAPIGetToPath:url
                                        params:params
                                        target:self
                                      selector:@selector(saveEcomBAddressResponse:userInfo:)
                                      userInfo:nil
                                     matchCase:YES];
}

- (void)saveEcomBAddressResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(saveEComBAddressdidSucceed)]) {
            [self.delegate saveEComBAddressdidSucceed];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(saveEComBAddressdidFailed:)]) {
            [self.delegate saveEComBAddressdidFailed:response.resultCode];
        }
    }
}

////////////////////////////////////////////////////////////////// 获取订单详情 ///////////////////////////////////////////////////////////////////////


- (void)getEcomBOrderInfoViewWithOrderID:(NSString *)orderID
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    //[params setSafeObject:addr.ecomUserName forKey:@"TEST"];
    [[ZSTCommunicator shared] openAPIGetToPath:[NSString stringWithFormat:GETORDERPRODUCTLIST,orderID,(long)self.moduleType,[ZSTF3Preferences shared].ECECCID]
                                        params:params
                                        target:self
                                      selector:@selector(getEcomBOrderInfoViewResponse:userInfo:)
                                      userInfo:nil
                                     matchCase:YES];
}
- (void)getEcomBOrderInfoViewResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        NSDictionary * resultDic = [response.jsonResponse safeObjectForKey:@"info"];
        id carouseles = [resultDic safeObjectForKey:@"productlist"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        int payType = [[resultDic safeObjectForKey:@"paytype"] intValue];
        int status = [[resultDic safeObjectForKey:@"status"] intValue];
        BOOL yinlian = [[resultDic safeObjectForKey:@"yinlianpay"] boolValue];
        int jifenpay = [[resultDic safeObjectForKey:@"jifenpay"] intValue];
        if ([self isValidDelegateForSelector:@selector(getEcomBOrderInfoViewdidSucceed:payType:status:yinlian:jifenpay:)]) {
            [self.delegate getEcomBOrderInfoViewdidSucceed:carouseles payType:payType status:status yinlian:yinlian jifenpay:jifenpay];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getEcomBOrderInfoViewdidFailed:)]) {
            [self.delegate getEcomBOrderInfoViewdidFailed:response.resultCode];
        }
    }
}
////////////////////////////////////////////////////////////////// 获取订单详情金额 ///////////////////////////////////////////////////////////////////////


- (void)getEcomBOrderAmountWithOrderID:(NSString *)orderID
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    //[params setSafeObject:addr.ecomUserName forKey:@"TEST"];
    [[ZSTCommunicator shared] openAPIGetToPath:[NSString stringWithFormat:GETORDERAMOUNT,orderID,(long)self.moduleType,[ZSTF3Preferences shared].ECECCID]
                                        params:params
                                        target:self
                                      selector:@selector(getEcomBOrderAmountResponse:userInfo:)
                                      userInfo:nil
                                     matchCase:YES];
}
- (void)getEcomBOrderAmountResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSDictionary class]]) {
            carouseles = [NSDictionary dictionary];
        }
        
        if ([self isValidDelegateForSelector:@selector(getEcomBOrderAmountdidSucceed:totalfreight:)]) {
            [self.delegate getEcomBOrderAmountdidSucceed:[[carouseles safeObjectForKey:@"amount"] floatValue] totalfreight:[[carouseles safeObjectForKey:@"totalfreight"] intValue]];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getEcomBOrderAmountdidFailed:)]) {
            [self.delegate getEcomBOrderAmountdidFailed:response.resultCode];
        }
    }
}

- (void)getFreightTemplate:(NSString *)orderID
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    
    [[ZSTCommunicator shared] openAPIGetToPath:[NSString stringWithFormat:GetFREIGHTTEMPLATE,orderID,(long)self.moduleType,[ZSTF3Preferences shared].ECECCID]
                                        params:params
                                        target:self
                                      selector:@selector(getFreightTemplateInfoViewResponse:userInfo:)
                                      userInfo:nil
                                     matchCase:YES];
    
}


- (void)getFreightTemplateInfoViewResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        
        id resultDic = [response.jsonResponse safeObjectForKey:@"info"];
        if ([resultDic isKindOfClass:[NSDictionary class]])
        {
            double amount = [[resultDic safeObjectForKey:@"amount"] doubleValue];
            double totalFreight = [[resultDic safeObjectForKey:@"totalfreight"] doubleValue];
            
            if ([self isValidDelegateForSelector:@selector(getEcomBFreightInfoViewDidSucceed:AndFreight:)]) {
                [self.delegate getEcomBFreightInfoViewDidSucceed:amount AndFreight:totalFreight];
            }
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(getEcomBFreightInfoViewDidFailed:)])
        {
            [self.delegate getEcomBFreightInfoViewDidFailed:response.resultCode];
        }
    }
}


- (void) getjifenPayInfoWithOrderId:(NSString *)orderid costamount:(float)amount
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    //[params setSafeObject:addr.ecomUserName forKey:@"TEST"];
    [[ZSTCommunicator shared] openAPIGetToPath:[NSString stringWithFormat:JIFENPAY,orderid,(long)self.moduleType,[ZSTF3Preferences shared].ECECCID,amount]
                                        params:params
                                        target:self
                                      selector:@selector(getJifenPay:userInfo:)
                                      userInfo:nil
                                     matchCase:YES];
}

- (void)getJifenPay:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSDictionary class]]) {
            carouseles = [NSDictionary dictionary];
        }
        
        if ([self isValidDelegateForSelector:@selector(getjifenPayDidSucceed:deductionamount:)]) {
            [self.delegate getjifenPayDidSucceed:[[carouseles safeObjectForKey:@"pointnum"] intValue] deductionamount:[[carouseles safeObjectForKey:@"deductionamount"] floatValue]];
             }
    } else {
        if ([self isValidDelegateForSelector:@selector(getjifenPayDidFailed:)]) {
            [self.delegate getjifenPayDidFailed:response.resultCode];
        }
    }
}


////////////////////////////////////////////////////////////////// 提交订单 ///////////////////////////////////////////////////////////////////////


- (void)submitOrder:(NSDictionary *)dic
{
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    
//    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
//    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    //[params setSafeObject:addr.ecomUserName forKey:@"TEST"];
      [[ZSTCommunicator shared] openAPIGetToPath:[NSString stringWithFormat:SUBMITORDER,[ZSTF3Preferences shared].loginMsisdn,self.moduleType,[ZSTF3Preferences shared].ECECCID,[ZSTF3Preferences shared].UserId]
                                        params:dic
                                        target:self
                                      selector:@selector(submitOrderResponse:userInfo:)
                                      userInfo:nil
                                     matchCase:YES];
}
- (void)submitOrderResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"orderid"];
        if (![carouseles isKindOfClass:[NSString class]]) {
            carouseles = [NSString string];
        }
        
        if ([self isValidDelegateForSelector:@selector(submitOrderdidSucceed:)]) {
            [self.delegate submitOrderdidSucceed:carouseles];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(submitOrderDidFailed:)]) {
            [self.delegate submitOrderDidFailed:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
}

////////////////////////////////////////////////////////////////// 获取主页数据 ///////////////////////////////////////////////////////////////////////

- (void)getPage:(NSInteger)pageSize pageIndex:(NSInteger)pageIndex
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];

    [[ZSTCommunicator shared] openAPIGetToPath:[NSString stringWithFormat:GETPRODUCTLIST,self.moduleType,[ZSTF3Preferences shared].ECECCID,pageSize,pageIndex]
                                        params:nil
                                        target:self
                                      selector:@selector(getPageResponse:userInfo:)
                                      userInfo:nil
                                     matchCase:YES];
    
}
- (void)getPageResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(getPageDidSucceed:hasMore:)]) {
            [self.delegate getPageDidSucceed:carouseles hasMore:[response.jsonResponse safeObjectForKey:@"hasmore"]];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getPageDidFailed:)]) {
            [self.delegate getPageDidFailed:response.resultCode];
        }
    }
}


///////////////////////////////////////////////////////////////////获取主页轮播图////////////////////////////////////////////////////////////////////
- (void)getEcomBCarousel
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    
   
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:GETBANNERLIST,self.moduleType,[ZSTF3Preferences shared].ECECCID]
                                         params:params
                                         target:self
                                       selector:@selector(getEcomBCarouselResponse:userInfo:)];
}
- (void)getEcomBCarouselResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(getEcomBCarouselDidSucceed:)]) {
            [self.delegate getEcomBCarouselDidSucceed:carouseles];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getEcomBCarouselDidFailed:)]) {
            [self.delegate getEcomBCarouselDidFailed:response.resultCode];
        }
    }
}

///////////////////////////////////////////////////////////////////商品搜索////////////////////////////////////////////////////////////////////
- (void)ecomBSearch:(NSString *) str pageIndex:(NSInteger)index
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    
    [[ZSTCommunicator shared] openAPIGetToPath:[NSString stringWithFormat:SEARCH,(long)self.moduleType,[ZSTF3Preferences shared].ECECCID,str,index]
                                        params:nil
                                        target:self
                                      selector:@selector(getEcomBSearchResponse:userInfo:)
                                      userInfo:nil
                                     matchCase:YES];
}
- (void)getEcomBSearchResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(getEcomBSearchDidSucceed:hasMore:)]) {
            BOOL hasmore = [[response.jsonResponse safeObjectForKey:@"hasmore"] boolValue];
            [self.delegate getEcomBSearchDidSucceed:carouseles hasMore:hasmore];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getEcomBSearchDidFailed:hasMore:)]) {
            [self.delegate getEcomBSearchDidFailed:response.resultCode hasMore:NO];
        }
    }
}

///////////////////////////////////////////////////////////////////在线咨询内容////////////////////////////////////////////////////////////////////
- (void)getAskOnLineData:(NSString *)productid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//productid=%d&moduletype=%d&ecid=%@&msisdn=%@
    [[ZSTCommunicator shared] openAPIGetToPath:[NSString stringWithFormat:CONSULTATION,productid,self.moduleType,[ZSTF3Preferences shared].ECECCID,[ZSTF3Preferences shared].loginMsisdn]
                                        params:nil
                                        target:self
                                      selector:@selector(getEcomBAskResponse:userInfo:)
                                      userInfo:nil
                                     matchCase:YES];
}
- (void)getEcomBAskResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(getEcomBAskDidSucceed:)]) {
            [self.delegate getEcomBAskDidSucceed:carouseles];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getEocmBAskDidFailed:)]) {
            [self.delegate getEcomBAskDidFailed:response.resultCode];
        }
    }
}


///////////////////////////////////////////////////////////////////发送在线咨询内容////////////////////////////////////////////////////////////////////
- (void)sendAskOnLineData:(NSString *)str productid:(NSString *) productid;
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//productid=%@&moduletype=%d&ecid=%@&msisdn=%@&content=%@
    [[ZSTCommunicator shared] openAPIGetToPath:[NSString stringWithFormat:CONSULTATIONADD,productid,self.moduleType,[ZSTF3Preferences shared].ECECCID,[ZSTF3Preferences shared].loginMsisdn,str]
                                        params:nil
                                        target:self
                                      selector:@selector(sendEcomBAskResponse:userInfo:)
                                      userInfo:nil
                                     matchCase:YES];
}
- (void)sendEcomBAskResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(sendEcomBAskDidSucceed:)]) {
            [self.delegate sendEcomBAskDidSucceed:carouseles];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(sendEcomBAskDidFailed:)]) {
            [self.delegate sendEcomBAskDidFailed:response.resultCode];
        }
    }
}

///////////////////////////////////////////////////////////////////获取商品详情页内容////////////////////////////////////////////////////////////////////

- (void)getProductsDetail:(NSInteger)newsID
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//moduletype=%d&ecid=%@&productid=%@
    NSString *url = [NSString stringWithFormat:GETPRODUCTINFOBYID,self.moduleType,[ZSTF3Preferences shared].ECECCID,newsID];
    
    [[ZSTCommunicator shared] openAPIPostToPath:url
                                         params:params
                                         target:self
                                       selector:@selector(getProductsResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:YES];
}

- (void)getProductsResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id product = [response.jsonResponse safeObjectForKey:@"info"];
        NSString *details = @"";
        if (![[product safeObjectForKey:@"Detailurl"] isKindOfClass:[NSNull class]] && [[product safeObjectForKey:@"Detailurl"] isKindOfClass:[NSString class]] && ![[product safeObjectForKey:@"Detailurl"] isEqualToString:@""])
        {
            details = [product safeObjectForKey:@"Detailurl"];
        }
        else
        {
            details = [product safeObjectForKey:@"details"];
        }
        if ([self isValidDelegateForSelector:@selector(getProductsContentDidSucceed:)]) {
            [self.delegate getProductsContentDidSucceed:details];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getProductsContentDidFailed:)]) {
            [self.delegate getProductsContentDidFailed:response.resultCode];
        }
    }
}
///////////////////////////////////////////////////////////////////获取商品展示内容////////////////////////////////////////////////////////////////////

- (void) getProductsShowInfo:(NSString *)productId
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//moduletype=%d&ecid=%@&productid=%@
    NSString *url = [NSString stringWithFormat:GETPRODUCTBYID,self.moduleType,[ZSTF3Preferences shared].ECECCID,productId];
    
    [[ZSTCommunicator shared] openAPIPostToPath:url
                                         params:params
                                         target:self
                                       selector:@selector(getProductsShowInfo:userInfo:)
                                       userInfo:nil
                                      matchCase:YES];

}

- (void)getProductsShowInfo:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id products = [response.jsonResponse safeObjectForKey:@"info"];
        ZSTECBProduct *product = [ZSTECBProduct voWithDic:products];
        if ([self isValidDelegateForSelector:@selector(getProductsShowDidSucceed:)]) {
            [self.delegate getProductsShowDidSucceed:product];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getProductsShowFaild:)]) {
            [self.delegate getProductsShowFaild:response.resultCode];
        }
    }
}
///////////////////////////////////////////////////////////////////获取商品展示页轮播图////////////////////////////////////////////////////////////////////
- (void)getEcomBGoodsInfoCarousel:(NSString *) protecteid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//moduletype=%d&ecid=%@&productid=%@
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:GETBANNERLISTSHOW,self.moduleType,[ZSTF3Preferences shared].ECECCID,protecteid]
                                         params:params
                                         target:self
                                       selector:@selector(getEcomBGoodsInfoCarouselResponse:userInfo:)];
}
- (void)getEcomBGoodsInfoCarouselResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(getEcomBGoodsInfoCarouselDidSucceed:)]) {
            [self.delegate getEcomBGoodsInfoCarouselDidSucceed:carouseles];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getEcomBGoodsInfoCarouselDidFailed:)]) {
            [self.delegate getEcomBGoodsInfoCarouselDidFailed:response.resultCode];
        }
    }
}
///////////////////////////////////////////////////////////////////属性选择页////////////////////////////////////////////////////////////////////
-(void)getPropertyViewControllerData:(NSString *) productid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    NSLog(@"---%ld---",self.moduleType);
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//moduletype=%d&ecid=%@&msisdn=%@&productid=%@
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:GETATTRIBUTE,self.moduleType,[ZSTF3Preferences shared].ECECCID,[ZSTF3Preferences shared].loginMsisdn,productid]
                                         params:nil
                                         target:self
                                       selector:@selector(getPropertyViewControllerDataResponse:userInfo:)];
}
- (void)getPropertyViewControllerDataResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        NSDictionary * info = [response.jsonResponse safeObjectForKey:@"info"];
        if (![info isKindOfClass:[NSDictionary class]]) {
            info = [NSDictionary dictionary];
        }
        
        if ([self isValidDelegateForSelector:@selector(getPropertyViewControllerDataDidSucceed:)]) {
            [self.delegate getPropertyViewControllerDataDidSucceed:info];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getPropertyViewControllerDataDidFailed:)]) {
            [self.delegate getPropertyViewControllerDataDidFailed:response.resultCode];
        }
    }
}



///////////////////////////////////////////////////////////////////分类显示////////////////////////////////////////////////////////////////////
- (void)getCategoriesShowData:(NSString *)categoryid pageIndex:(NSInteger) index
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//moduletype=%d&ecid=%@&categoryid=%@
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:SEARCHCATEGORY,(long)self.moduleType,[ZSTF3Preferences shared].ECECCID,categoryid,index]
                                         params:nil
                                         target:self
                                       selector:@selector(getCategoriesShowResponse:userInfo:)];
}
- (void)getCategoriesShowResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(getCategoriesShowDidSucceed:hasMore:)]) {
            [self.delegate getCategoriesShowDidSucceed:carouseles hasMore:[response.jsonResponse safeObjectForKey:@"hasmore"]];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getCategoriesShowDidFailed:)]) {
            [self.delegate getCategoriesShowDidFailed:response.resultCode];
        }
    }
}

///////////////////////////////////////////////////////////////////获取购物车中内容////////////////////////////////////////////////////////////////////
- (void)getShoppingCartData
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//moduletype=%d&ecid=%@&msisdn=%@
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:GETSHOPPINGCAR,self.moduleType,[ZSTF3Preferences shared].ECECCID,[ZSTF3Preferences shared].loginMsisdn]
                                         params:nil
                                         target:self
                                       selector:@selector(getShoppingCartResponse:userInfo:)];
}
- (void)getShoppingCartResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(getShoppingCartDidSucceed:)]) {
            [self.delegate getShoppingCartDidSucceed:carouseles];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getShoppingCartDidFailed:)]) {
            [self.delegate getShoppingCartDidFailed:response.resultCode];
        }
    }
}

///////////////////////////////////////////////////////////////////商品加入购物车////////////////////////////////////////////////////////////////////
- (void)putGoodsIntoCar:(NSString *) productid propertyId:(NSString *) propertyid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//moduletpye=%d&ecid=%@&msisdn=%@&productid=%@&propertyid=%@
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:ADDPROTOCAR,self.moduleType,[ZSTF3Preferences shared].ECECCID,[ZSTF3Preferences shared].loginMsisdn,productid,propertyid]
                                         params:nil
                                         target:self
                                       selector:@selector(putGoodsIntoCarResponse:userInfo:)];
}

- (void)putNewsGoodsIntoCar:(NSString *) productid propertyId:(NSString *) propertyid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//moduletpye=%d&ecid=%@&msisdn=%@&productid=%@&propertyid=%@
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:ADDNEWSPROTOCAR,self.moduleType,[ZSTF3Preferences shared].ECECCID,[ZSTF3Preferences shared].loginMsisdn,productid,propertyid,propertyid]
                                         params:nil
                                         target:self
                                       selector:@selector(putGoodsIntoCarResponse:userInfo:)];
}

- (void)putGoodsIntoCarResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(putGoodsIntoCarDidSucceed:)]) {
            [self.delegate putGoodsIntoCarDidSucceed:carouseles];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(putGoodsIntoCarDidFailed:)]) {
            [self.delegate putGoodsIntoCarDidFailed:response.resultCode];
        }
    }
}

///////////////////////////////////////////////////////////////////我的订单页数据////////////////////////////////////////////////////////////////////
-(void)getMyOrderData
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//moduletype=%d&ecid=%@&msisdn=%@
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:GETMYORDER,self.moduleType,[ZSTF3Preferences shared].ECECCID,[ZSTF3Preferences shared].loginMsisdn]
                                         params:nil
                                         target:self
                                       selector:@selector(getMyOrderResponse:userInfo:)];
}
- (void)getMyOrderResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(getMyOrderDidSucceed:isLoadMore:hasMore:isFinish:)]) {
            [self.delegate getMyOrderDidSucceed:carouseles isLoadMore:NO hasMore:NO isFinish:YES];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getMyOrderDidFailed:)]) {
            [self.delegate getMyOrderDidFailed:response.resultCode];
        }
    }
}

///////////////////////////////////////////////////////////////////货到付款提交数据////////////////////////////////////////////////////////////////////

-(void)huodaoPay:(NSString *) _orderID;
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];//orderid=%@moduletype=%d&ecid=%@&msisdn=%@
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:HUODAOPAY,_orderID,self.moduleType,[ZSTF3Preferences shared].ECECCID,[ZSTF3Preferences shared].loginMsisdn]
                                         params:nil
                                         target:self
                                       selector:@selector(huodaoPayResponse:userInfo:)];
}
- (void)huodaoPayResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(huodaoPayDidSucceed)]) {
            [self.delegate huodaoPayDidSucceed];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(huodaoPayDidFailed:)]) {
            [self.delegate huodaoPayDidFailed:response.resultCode];
        }
    }
}

///////////////////////////////////////////////////////////////////////

- (void)jifenPay:(NSString *)orderid
{
    //NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:JIFENALLPAY,orderid]
                                         params:nil
                                         target:self
                                       selector:@selector(jifenPayResponse:userInfo:)];
}

- (void)jifenPayResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(jifenpayDidSucceed)]) {
            [self.delegate jifenpayDidSucceed];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(jifenpayDidFailed:)]) {
            [self.delegate jifenpayDidFailed:response.resultCode];
        }
    }
}

- (void)jifenUsePay:(NSString *)orderid costamount:(float)amount pointnum:(NSInteger)pointnum
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:JIFENUSEPAY,orderid,(long)self.moduleType,[ZSTF3Preferences shared].ECECCID,amount,pointnum]
                                         params:nil
                                         target:self
                                       selector:@selector(jifenUsePayResponse:userInfo:)];
}

- (void)jifenUsePayResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {

        if ([self isValidDelegateForSelector:@selector(jifenUsePayDidSucceed)]) {
            [self.delegate jifenUsePayDidSucceed];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(jifenUsePayDidFailed:)]) {
            [self.delegate jifenUsePayDidFailed:response.resultCode];
        }
    }
}

@end
