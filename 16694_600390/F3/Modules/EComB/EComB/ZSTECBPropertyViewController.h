//
//  ZSTECBPropertyViewController.h
//  EComB
//
//  Created by LiZhenQu on 14-3-5.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModuleBaseViewController.h"
#import "ZSTECBShoppingCartViewController.h"
typedef enum
{
    PUSHTOFRONPAGE = 0,
    PUSHTOBUYNOW = 1
}PushTo;

//@protocol ZSTECBPropertyViewControllerDelgeate;

@interface ZSTECBPropertyViewController : UIViewController<TKAsynImageViewDelegate,ZSTF3EngineECBDelegate>
{
    EcomBPropertyInfo * _propertyInfo;
}

@property (nonatomic,assign) PushTo push;
//@property (nonatomic,assign) id <ZSTECBPropertyViewControllerDelgeate> delegate;
@property (nonatomic, retain) UIScrollView *scrollView;
//@property (nonatomic, retain) TKAsynImageView *productImg;
@property (nonatomic,retain) UIImageView *productImg;
@property (nonatomic, retain) UILabel *productNameLabel;
@property (nonatomic, retain) UILabel *priceLabel;
@property (nonatomic, retain) UIButton *reductionbtn;
@property (nonatomic, retain) UIButton *addbtn;
@property (nonatomic, retain) UILabel *countLabel;

@property (nonatomic, retain) UIView *colorView;
@property (nonatomic, retain) UILabel *colortagLabel;

@property (nonatomic, retain) UIView *sizeView;
@property (nonatomic, retain) UILabel *sizeLabel;

@property (nonatomic, retain) UIButton *okbtn;
@property (nonatomic, retain) NSString *productid;

@end

