//
//  ZSTEComBOrderInfoViewControllerResponseCell.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBOrderInfoViewControllerResponseCell.h"

@implementation ZSTEComBOrderInfoViewControllerResponseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 16, 80, 18)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:15];
        label.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
        label.text = @"实付款";
        [self.contentView addSubview:label];
        [label release];
        
        // Initialization code
        _payBtn = [[PMRepairButton alloc] init];
        _payBtn.frame = CGRectMake(320 - 15 - 140, 25+15, 140, 38);
        [_payBtn setImage:ZSTModuleImage(@"module_ecomb_orderinfo_Responsebtn.png") forState:UIControlStateNormal];
        [_payBtn addTarget:self action:@selector(goPayAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_payBtn];

        _priceLabel = [[UILabel alloc]init];
        _priceLabel.font = [UIFont systemFontOfSize:22];
        _priceLabel.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
//        _priceLabel.text = @"￥664.00";
        [self.contentView addSubview:_priceLabel];
        _priceLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _priceLabel.frame = CGRectMake(15, 35, 150, 30);
        
        _otherLabel = [[UILabel alloc]init];
        _otherLabel.font = [UIFont systemFontOfSize:12];
        _otherLabel.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
//        _otherLabel.text = @"（含运费10元）";
        [self.contentView addSubview:_otherLabel];
        _otherLabel.numberOfLines = 0;
        _otherLabel.backgroundColor = [UIColor clearColor];
        _otherLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _otherLabel.frame = CGRectMake(15, CGRectGetMaxY(_priceLabel.frame), 320, 16);
    }
    return self;
}
-(void)goPayAction:(UIButton *)btn
{
    [_delegate submitBtnDidClicked];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)hideSubmit
{
    _payBtn.hidden = YES;
    _payBtn.enabled = NO;
}
//-(void)dealloc
//{
//    
//    [_priceLabel release];
//    [_otherLabel release];
//    [super dealloc];
//}
@end
