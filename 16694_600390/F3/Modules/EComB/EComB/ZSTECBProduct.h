//
//  ZSTECBProduct.h
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTECBProduct : NSObject

@property (nonatomic, retain) NSString *productName;//商品名称

@property (nonatomic, retain) NSNumber *salesPrice;//现价
@property (nonatomic, retain) NSNumber *primePrice;//原价
@property (nonatomic, retain) NSNumber *freightPrice;//运费

@property (nonatomic, retain) NSString *primeLabel;//原价标签
@property (nonatomic, retain) NSString *salesLabel;//现价标签

@property (nonatomic, retain) NSString *price;//成交价（唯一价格）

@property (nonatomic, retain) NSString *summary;//副标题

@property (nonatomic, retain) NSString *iconUrl;//图标图片地址
@property (nonatomic, retain) NSString *carouselUrl;//轮播图地址
@property (nonatomic, retain) NSString *addTime;//上架日期
@property (nonatomic, retain) NSNumber *categoryID;//分类id
@property (nonatomic, retain) NSNumber *productID;//商品id
@property (nonatomic, retain) NSNumber *orderNum;//排列顺序
@property (nonatomic, retain) NSString *orderID;//订单id
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString *discount; //折扣

@property (strong, nonatomic) NSString *mainTitle;// 主标题

@property (nonatomic, assign) BOOL jifenflag;

+ (id)voWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;

@end


//地址类

@interface EcomBAddress : NSObject

@property (nonatomic,retain) NSString * ecomUserName;
@property (nonatomic,retain) NSString * ecomMobile;
@property (nonatomic,retain) NSString * ecomAddress;
@property (nonatomic,retain) NSString * ecomPostNum;
@property (nonatomic,retain) NSString * ecomDetailAddress;
@property (nonatomic,retain) NSString * ecomCityId;

+ (id)addressWithDictonary:(NSDictionary *) dic;
- (id)initWithDictionary:(NSDictionary *) dic;
@end


//主页
@interface EComBHomePageInfo : NSObject

@property (nonatomic,retain) NSString * ecombHomeImage;
@property (nonatomic,retain) NSString * ecombHomeText;
@property (nonatomic,retain) NSString * ecombHomePrice;
@property (nonatomic,retain) NSString * ecombHomeOrangePrice;
@property (nonatomic,retain) NSString * ecombHomeProductid;
@property (nonatomic, assign) BOOL jifenflag;

+ (id)homePageWithArray:(NSArray *) arr;
- (id)initWithDictionary:(NSDictionary *) dic;

@end



//轮播图

@interface EcomBCarouseInfo : NSObject

@property (nonatomic,retain) NSString * ecomBCarouseInfoCarouselurl;
@property (nonatomic,retain) NSString * ecomBCarouseInfoTitle;
@property (nonatomic,retain) NSString * ecomBCarouseInfoLinkurl;
@property (nonatomic,assign) int ecomBCarouseInfoProductid;

+ (id)ecomBCarouseWithArray:(NSArray *) arr;
- (id)initWithDictionary:(NSDictionary *) dic;

@end


//在线咨询请求

@interface EcomBAskInfo : NSObject

@property (nonatomic,retain) NSString * EcomBAskInfoMsgid;
@property (nonatomic,retain) NSString * EcomBAskInfoQuestion;
@property (nonatomic,retain) NSString * EcomBAskInfoAnswer;

+ (id)EcomBAskWithArray:(NSArray *) arr;
- (id)initWithDictionary:(NSDictionary *) dic;

@end

//购物车
@interface EcomBShopingCarInfo : NSObject
@property (nonatomic,retain) NSString * shopingCarInfoImageStr;
@property (nonatomic,retain) NSString * shopingCarInfoProductName;
@property (nonatomic,assign) float  shopingCarInfoPrice;
@property (nonatomic,assign)  int shopingCarInfoCount;
@property (nonatomic,assign) float  shopingCarInfoFreight;
@property (nonatomic,assign)  int  shopingCarInfoPropertyid;
@property (nonatomic,assign)  int  shopingCarInfoProductid;
@property (nonatomic,assign)  int  shopingCarInfoStock;
@property (nonatomic, assign) BOOL jifenflag;

+(id)EcomBShopingCarInfo:(NSArray *) arr;
-(id)initWithDictionary:(NSDictionary *) dic;
@end


//商品属性
@interface EcomBProperty : NSObject
@property (nonatomic,retain) NSString * ecomBPropertyProperty;
@property (nonatomic,retain) NSString * ecomBPropertyPropertyid;
@property (nonatomic,assign) int ecomBPropertyStockcount;
@property (nonatomic,retain) NSString *spec;
@property (nonatomic,assign) double salePrice;
+(id)EcomBPropertyWithArray:(NSArray *) arr;
-(id)initWithDictionary:(NSDictionary *) dic;
@end


//商品属性页
@interface EcomBPropertyInfo : NSObject
@property (nonatomic,retain) NSArray * ecomBPropertyInfoProperties;
@property (nonatomic,retain) NSString * ecomBPropertyInfoImageStr;
@property (nonatomic,retain) NSString * ecomBPropertyInfoProductname;
@property (nonatomic,assign) float  ecomBPropertyInfoSalesprice;
@property (nonatomic,assign) int ecomBCarouseInfoStockcount;
@property (nonatomic,assign) BOOL isOld;
@property (nonatomic,retain) NSString *ecomBPropertyInfoSpecNames;
+(id)ecomBPropertyInfoWithDictionary:(NSDictionary *)dic;
-(id)initWithDictionary:(NSDictionary *)dic;
@end

//我的订单页
@interface EcomBOrderInfo: NSObject

@property (nonatomic,retain) NSString * orderOrderid;//订单编号
@property (nonatomic,assign) float orderAmount;//订单总额
@property (nonatomic,retain) NSString * orderTime;//订单时间
@property (nonatomic,assign) int orderProductcount;//数量
@property (nonatomic,assign) int orderStatus;//状态
@property (nonatomic,retain) NSString * orderPhone;//电话
@property (nonatomic,retain) NSArray * orderImageArr;//图片列表

+(id)EcomBMyOrderWithArray:(NSArray *)arr;
-(id)initWithDictionary:(NSDictionary *)dic;
@end;

//订单 商品
@interface EcombOrderGools : NSObject
@property (nonatomic,retain) NSString * goolsDscription;//商品描述
@property (nonatomic,assign) float goolsPrice;//商品价格
@property (nonatomic,assign) int goolsCount;//商品数量
@property (nonatomic, assign) BOOL jifenflag;

+(id)ecombOrderGoolsWithArray:(NSArray *)arr;
-(id)initWithDictionary:(NSDictionary *)dic;
@end
