//
//  ZSTEComBAskOnLineTableViewCell.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-22.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTEComBAskOnLineTableViewCell : UITableViewCell
{
    UILabel * askLabel;
    UILabel * answerLabel;
}

-(void)reloadData:(EcomBAskInfo *)askInfo;
@end
