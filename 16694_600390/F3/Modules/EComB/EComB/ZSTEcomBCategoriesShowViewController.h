//
//  ZSTEcomBCategoriesShowViewController.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-11.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//ZSTEcomBCategoriesShowViewController

#import "ZSTECBProductTableImageCell.h"

@interface ZSTEcomBCategoriesShowViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ZSTECBProductTableImageCellDelegate,ZSTF3EngineECBDelegate,TKLoadMoreViewDelegate>
@property (retain,nonatomic)UIView * headView;
@property (retain,nonatomic)UITableView * tableView;
@property (retain,nonatomic)NSMutableArray * resultData;
@property (retain,nonatomic)NSString * categoryid;
@end
