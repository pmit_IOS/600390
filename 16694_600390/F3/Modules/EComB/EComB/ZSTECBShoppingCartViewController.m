//
//  ZSTECBShoppingCartViewController.m
//  EComB
//
//  Created by LiZhenQu on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBShoppingCartViewController.h"
#import "ZSTUtils.h"
#import "ZSTECBShoppingCarCell.h"
#import "ZSTECBPropertyViewController.h"
#import "ZSTEComBOrderInfoViewController.h"
#import "ZSTECBShoppingCarCell.h"
#import "ZSTEComBOrderInfomationViewController.h"
#import "ZSTEComBInputAddressViewController.h"


#define SYSTEM_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define SCREEN_BOUNDS [[UIScreen mainScreen] bounds]
#define WIDTH SCREEN_BOUNDS.size.width
#define HEIGHT SCREEN_BOUNDS.size.height

@interface ZSTECBShoppingCartViewController ()<UITableViewDelegate,UITableViewDataSource,ZSTECBShoppingCarDelegate>
{
    UIView * nodataView;
    UIView * havedataView;
}
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) UILabel *totalFreightLabel;
@property (nonatomic, retain) UILabel *postageLabel;

@property (nonatomic, retain) UIView *bottomView;
@property (nonatomic, retain) UILabel *totalPriceLabel;
@property (nonatomic, retain) UIButton *donebtn;
@property (nonatomic, retain) UILabel * resultLabel;

@end

@implementation ZSTECBShoppingCartViewController
{
    BOOL _haveAddress;
}

static NSString *const carCell = @"carCell";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(UIColor *)stringToColor:(NSString *)colorStr
{
    int a[6] = {},i = 0;
    char * cchar = (char *)[colorStr cStringUsingEncoding:NSASCIIStringEncoding];
    
    char * p = cchar;
    
    while (*p != '\0') {
        if (*p == '#') {
            
        }
        else if (*p >= 'a' && *p<='z'){
            a[i++] = 10 + *p - 'a';
        }
        else if(*p >= 'A' && *p<='Z')
        {
            a[i++] = 10 + *p - 'A';
        }
        else if (*p >= '0' && *p<='9') {
            a[i++] = *p - '0';
        }
        else{
            a[i++] = 0;
        }
        p++;
    }
    
    UIColor * color = RGBCOLOR(a[0] * 16 + a[1], a[2] * 16 + a[3], a[4]*16 + a[5]);
    return color;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    nodataView = [[UIView alloc]initWithFrame:self.view.bounds];
    nodataView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:nodataView];
    
    _resultLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 200, 320, 30)];
    _resultLabel.textAlignment = NSTextAlignmentCenter;
    _resultLabel.textColor = [self stringToColor:@"#f3f3f3"];
    _resultLabel.text = @"购物车还未添加任何商品哦~";
    _resultLabel.hidden = YES;
    [nodataView addSubview:_resultLabel];
    
    havedataView = [[UIView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:havedataView];
    havedataView.hidden = YES;
    
    _maxRow = -1;
    _dataArray = [[NSMutableArray alloc]init];
    _totalPrice = 0.0f;
    _totalFreight = 0;
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"购物车", nil)];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UILabel *markLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, 100, 20)];
    markLabel.backgroundColor = [UIColor whiteColor];
    markLabel.textColor = RGBCOLOR(171, 171, 171);
    markLabel.font = [UIFont boldSystemFontOfSize:16];
    markLabel.text = @"已订购商品:";
    [havedataView addSubview:markLabel];
    [markLabel release];
    
    self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, 35, WIDTH, HEIGHT - 64 - 35 - 50) style:UITableViewStylePlain] autorelease];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.tableView.separatorStyle =  UITableViewCellSeparatorStyleNone;
    //为了多选
    self.tableView.allowsSelection = YES;
    self.tableView.allowsSelectionDuringEditing = YES;
    [self.tableView registerClass:[ZSTECBShoppingCarCell class] forCellReuseIdentifier:carCell];
    //直接让tableView处于编辑状态
    [self.tableView setEditing:YES animated:YES];
    [havedataView addSubview:self.tableView];
    
    //    _bottomView = [[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.tableView.frame)-50-(IS_IOS_7?0:20), 320, 90)] autorelease];
    _bottomView = [[[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 50 - 64, WIDTH, 50)] autorelease];
    _bottomView.backgroundColor = [UIColor clearColor];
    _bottomView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [havedataView addSubview:_bottomView];
    
    UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 12, 20, 20)];
    tagLabel.backgroundColor = [UIColor clearColor];
    tagLabel.textColor = RGBCOLOR(202, 0, 22);
    tagLabel.font = [UIFont boldSystemFontOfSize:25];
    tagLabel.text = @"￥";
    [_bottomView addSubview:tagLabel];
    [tagLabel release];
    
    _totalPriceLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 10, 100, 20)] autorelease];
    _totalPriceLabel.backgroundColor = [UIColor clearColor];
    _totalPriceLabel.textColor = RGBCOLOR(202, 0, 22);
    _totalPriceLabel.font = [UIFont boldSystemFontOfSize:25];
    _totalPriceLabel.text = [NSString stringWithFormat:@"%.2f",0.0f];
    [_bottomView addSubview:_totalPriceLabel];
    _totalPriceLabel.adjustsFontSizeToFitWidth = YES;
    
    _totalFreightLabel = [[[UILabel alloc] initWithFrame:CGRectMake(25 + 5, 30, 100, 15)] autorelease];
    _totalFreightLabel.backgroundColor = [UIColor clearColor];
    _totalFreightLabel.textColor = RGBCOLOR(171, 171, 171);
    _totalFreightLabel.font = [UIFont systemFontOfSize:13];
    _totalFreightLabel.text = [NSString stringWithFormat:@"(含邮费%d元)",_totalFreight];
    [_bottomView addSubview:_totalFreightLabel];
    
    _donebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _donebtn.frame = CGRectMake(165 + 15, 10, 135 - 15, 30);
    _donebtn.backgroundColor = RGBCOLOR(211, 3, 6);
    [_donebtn setTitle:@"¥  确认结算" forState:UIControlStateNormal];
    [_donebtn setBackgroundImage:ZSTModuleImage(@"module_ecomb_submitbutton_red.png") forState:UIControlStateNormal];
    [_donebtn setTintColor:[UIColor whiteColor]];
    [_donebtn.titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
    _donebtn.layer.cornerRadius = 3.0f;
    _donebtn.clipsToBounds = YES;
    [_donebtn addTarget:self action:@selector(confirmAction:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:_donebtn];
    _donebtn.hidden = YES;
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
    [self.engine getShoppingCartData];
}
-(void)showResult:(NSString *)info
{
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(60, 100, 200, 100)];
    [self.view addSubview:view];
    view.alpha = 0.8;
    view.backgroundColor = [UIColor blackColor];
    [view release];
    
    UILabel * label = [[UILabel alloc]init];
    label.frame = CGRectMake(0, 30, 200, 40);
    label.backgroundColor = [UIColor clearColor];
    [label setTextColor:[UIColor whiteColor]];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = info;
    [view addSubview:label];
    [label release];
    
    view.layer.cornerRadius = 4;
    view.clipsToBounds = YES;
    
    [UIView animateWithDuration:5 animations:^{
        view.alpha = 0;
    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
}

- (void)getShoppingCartDidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
    NSLog(@"购物车获取数据错误");
    
    havedataView.hidden = YES;
    _resultLabel.hidden = NO;
    nodataView.hidden = NO;
    nodataView.userInteractionEnabled = YES;
    _bottomView.hidden = YES;
    
    //    [self showResult:@"购物车还未添加任何商品哦~"];
}
-(void)getShoppingCartDidSucceed:(NSArray *)carouseles
{
    [TKUIUtil hiddenHUD];
    if (carouseles.count == 0) {
        //        [self showResult:@"购物车还未添加任何商品哦~"];
        //        [TKUIUtil alertInWindow:@"购物车还未添加任何商品哦~" withImage:nil];
        _resultLabel.hidden = NO;
        return;
    }
    
    [self.engine getEcomBAddress];//获取邮递地址
    
    _resultLabel.hidden = YES;
    NSLog(@"购物车获取数据成功");
    havedataView.hidden = NO;
    havedataView.userInteractionEnabled = YES;
    
    _donebtn.hidden = NO;
    _donebtn.userInteractionEnabled = YES;
    
    nodataView.hidden = YES;
    nodataView.userInteractionEnabled = NO;
    _dataArray = [EcomBShopingCarInfo EcomBShopingCarInfo:carouseles];
    
    [_tableView reloadData];
    
//    [self calculateFreight];
    
    [self calculatePrice];
}
-(void)calculatePrice
{
    _totalPrice = 0.0f;
    for (EcomBShopingCarInfo * carInfo in _dataArray) {
        if (carInfo.shopingCarInfoCount>0) {
            _totalPrice += carInfo.shopingCarInfoCount * carInfo.shopingCarInfoPrice;
            _hasGools = YES;
        }
    }
    _totalPriceLabel.text = [NSString stringWithFormat:@"%.2f",_totalPrice + _totalFreight];
}
-(void)calculateFreight
{
    _totalFreight = 0;
    for (EcomBShopingCarInfo * carInfo in _dataArray) {
        if (carInfo.shopingCarInfoCount>0&&carInfo.shopingCarInfoFreight>_totalFreight) {
            _totalFreight = carInfo.shopingCarInfoFreight;
            _hasGools = YES;
        }
    }
    _totalFreightLabel.text = [NSString stringWithFormat:@"(含邮费%d元)",_totalFreight];
}
-(void)calculateCount
{
    _totalCount = 0;
    for (EcomBShopingCarInfo * carInfo in _dataArray) {
        if (carInfo.shopingCarInfoCount>0) {
            _totalCount ++;        }
    }
}
- (void) confirmAction:(id)sender
{
    [self calculateCount];
    if (_totalCount > 0) {
        NSMutableDictionary * dateDic = [[NSMutableDictionary alloc]init];
        
        NSTimeInterval  time = [[NSDate date]timeIntervalSince1970];
        long long int timeLong = (long long int)time;
        [dateDic setObject:[NSNumber numberWithLongLong:timeLong] forKey:@"date"];
        
        NSArray * orderArr = [self getOrderArray];
        [dateDic setObject:orderArr forKey:@"info"];
        
        [dateDic setObject:[NSString stringWithFormat:@"%d",_totalFreight] forKey:@"TotalFreight"];
        
        [dateDic setObject:[NSString stringWithFormat:@"%f",_totalPrice] forKey:@"TotalPrice"];
        
        NSString * moduleType = [NSString stringWithFormat:@"%ld",self.engine.moduleType];
        [dateDic setObject:moduleType forKey:@"moduletype"];
        
        NSString * ecid = [ZSTF3Preferences shared].ECECCID;
        [dateDic setObject:ecid forKey:@"ecid"];
        
        [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"提交订单中，请稍后..." , nil)];
        [self.engine submitOrder:dateDic];
        NSLog(@"提交订单...");
    }
    else
    {
        [TKUIUtil alertInWindow:@"请选择商品" withImage:nil];
    }
}
-(void)submitOrderDidFailed:(NSString *)result
{
    [TKUIUtil hiddenHUD];
#ifdef DEBUG
    NSLog(@"获取订单号失败");
#endif
    
    [TKUIUtil alertInWindow:@"库存不足" withImage:nil];
}

-(void)submitOrderdidSucceed:(NSString *)orderID
{
    [TKUIUtil hiddenHUD];
    // 如果没有地址
    if (!_haveAddress) {
        
        ZSTEComBInputAddressViewController * addInputViewController  = [[ZSTEComBInputAddressViewController alloc]init];
        addInputViewController.address = _address;
        addInputViewController.orderID = orderID;
        addInputViewController.isNullFrom = YES;
        addInputViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:addInputViewController animated:YES];
        [addInputViewController release];
        return;
    }
    
    ZSTEComBOrderInfomationViewController *infoViewController = [[ZSTEComBOrderInfomationViewController alloc] init];
    infoViewController.orderID = orderID;
    infoViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:infoViewController animated:YES];
    [infoViewController release];
}

-(NSArray *)getOrderArray
{
    NSMutableArray * arr = [[NSMutableArray alloc]init];
    for (int i = 0; i<_dataArray.count; i++) {
        EcomBShopingCarInfo * carInfo = [_dataArray objectAtIndex:i];
        if (carInfo.shopingCarInfoCount > 0) {
            [arr addObject:[self getDictionaryWith:carInfo]];
        }
    }
    return arr;
}
-(NSDictionary *)getDictionaryWith:(EcomBShopingCarInfo *)shoppingCar
{
    NSArray * keysArr = [[NSArray alloc]initWithObjects:@"productid",@"count",@"propertyid", nil];
    NSString * propertyidStr = [NSString stringWithFormat:@"%d",shoppingCar.shopingCarInfoPropertyid];
    NSString * productidStr = [NSString stringWithFormat:@"%d",shoppingCar.shopingCarInfoProductid];
    NSString * countStr = [NSString stringWithFormat:@"%d",shoppingCar.shopingCarInfoCount];
    
    NSArray * objectsArr = [[NSArray alloc]initWithObjects:productidStr,countStr,propertyidStr,nil];
    NSDictionary * dic = [[NSDictionary alloc]initWithObjects:objectsArr forKeys:keysArr];
    return dic;
}
#pragma mark tableDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return 4;
    return self.dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75.f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    static NSString * ECBProductTableViewCell = @"ZSTECBShoppingCartCell";
    //    ZSTECBShoppingCartCell * cell = [tableView dequeueReusableCellWithIdentifier:ECBProductTableViewCell];
    ZSTECBShoppingCarCell *cell = [tableView dequeueReusableCellWithIdentifier:carCell];
    //    if (!cell) {
    //        cell = [[ZSTECBShoppingCartCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ECBProductTableViewCell];
    //    }
    [cell createUI];
    cell.delegate = self;
    cell.selectBtn.tag = indexPath.row;
    [cell.selectBtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell loadData:[_dataArray objectAtIndex:indexPath.row] isAdd:(indexPath.row > _maxRow)];
    if (indexPath.row > _maxRow) {
        _maxRow = indexPath.row;
    }
    
    return  cell;
}


- (void) clickAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    ZSTECBShoppingCarCell* cell = (ZSTECBShoppingCarCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:button.tag inSection:0]];
    cell.checked = !cell.checked;
    [cell setChecked:cell.checked];
    
    EcomBShopingCarInfo * carInfo = [_dataArray objectAtIndex:button.tag];
    float price = [cell.countLabel.text intValue] * [cell.priceLabel.text floatValue];
    if (!cell.checked) {
        _totalPrice += price;
        carInfo.shopingCarInfoCount = [cell.countLabel.text intValue];
        [self calculateFreight];
        
    } else {
        _totalPrice -= price;
        carInfo.shopingCarInfoCount = 0;
    }
    
    
    [self calculateFreight];
    [self calculatePrice];
    [self calculateCount];
    if (_totalCount > 0) {
        _donebtn.hidden = NO;
        _donebtn.enabled = YES;
    }
    else
    {
        _donebtn.hidden = YES;
        _donebtn.enabled = NO;
    }
    //    _totalPriceLabel.text = [NSString stringWithFormat:@"%.2f",_totalPrice + _totalFreight];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //不要显示任何编辑的图标
    return UITableViewCellEditingStyleNone;
}

- (void) shoppingCartCell:(ZSTECBShoppingCarCell *)tableViewCell price:(float)value
{
    //    _totalPrice -= value;
    [self calculateFreight];
    
    //    float  total = _totalPrice + _totalFreight;
    //    _totalPriceLabel.text = [NSString stringWithFormat:@"%.2f",total];
    [self calculatePrice];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [super dealloc];
}

-(void)getEComBAddressdidSucceed:(NSDictionary *)addressDic
{
    [TKUIUtil hiddenHUD];
    _address = [(EcomBAddress *)[EcomBAddress addressWithDictonary:addressDic] retain];
    if (_address.ecomAddress.length > 0) {
        _haveAddress = YES;
    }
    [self.tableView reloadData];
}

-(void)getEComBAddressdidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
    NSLog(@"EcomB 地址请求失败");
}

@end
