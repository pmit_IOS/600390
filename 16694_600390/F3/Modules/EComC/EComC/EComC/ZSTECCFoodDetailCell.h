//
//  ZSTECCFoodDetailCell.h
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PMRepairButton.h>

@interface ZSTECCFoodDetailCell : UITableViewCell

@property (strong,nonatomic) UIImageView *foodIV;
@property (strong,nonatomic) UILabel *foodTitleLB;
@property (strong,nonatomic) UILabel *priceLB;
@property (strong,nonatomic) PMRepairButton *minusBtn;
@property (strong,nonatomic) PMRepairButton *plusBtn;
@property (strong,nonatomic) UILabel *numLB;
@property (assign,nonatomic) BOOL isHasBook;

- (void)createUI;
- (void)getCellData:(NSDictionary *)foodDetailDic;

@end
