//
//  ZSTECCMessageCell.m
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCMessageCell.h"

@implementation ZSTECCMessageCell 

- (void)createUI
{
    if (!self.iconIV)
    {
        self.iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 20, 20, 20)];
        self.iconIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.iconIV];
        
        self.messageTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(50, 15, WIDTH * 0.4, 30)];
        self.messageTitleLB.textAlignment = NSTextAlignmentLeft;
        self.messageTitleLB.font = [UIFont systemFontOfSize:14.0f];
        self.messageTitleLB.textColor = RGBA(85, 85, 85, 1);
        [self.contentView addSubview:self.messageTitleLB];
        
        self.nameTF = [[UITextField alloc] initWithFrame:CGRectMake(50, 15, WIDTH * 0.4, 30)];
        self.nameTF.placeholder = @"输入联系人";
        self.nameTF.font = [UIFont systemFontOfSize:14.0f];
        self.nameTF.hidden = YES;
        [self.contentView addSubview:self.nameTF];
        
        self.backUpTV = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 120)];
        self.backUpTV.delegate = self;
        self.backUpTV.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.backUpTV];
        
        self.placeholderLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, WIDTH - 30, 20)];
        self.placeholderLB.text = @"备注:";
        self.placeholderLB.textColor = RGBA(205, 205, 205, 1);
        self.placeholderLB.textAlignment = NSTextAlignmentLeft;
        [self.backUpTV addSubview:self.placeholderLB];
        
        self.backUpTV.hidden = YES;
        
        self.rightLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH * 0.4 + 50, 15, WIDTH * 0.6 - 60, 30)];
        self.rightLB.textColor = RGBA(206, 206, 206, 1);
        self.rightLB.textAlignment = NSTextAlignmentRight;
        self.rightLB.font = [UIFont systemFontOfSize:14.0f];
        [self.contentView addSubview:self.rightLB];
        self.rightLB.hidden = YES;
        
        self.phoneTF = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH * 0.4 + 50, 15, WIDTH * 0.6 - 60, 30)];
        self.phoneTF.font = [UIFont systemFontOfSize:14.0f];
        self.phoneTF.textColor = RGBA(206, 206, 206, 1);
        self.phoneTF.placeholder = @"请输入手机号码";
        self.phoneTF.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.phoneTF];
        self.phoneTF.hidden = YES;
        
        self.selectedSeatTag = -1;
        self.selectedSexTag = -1;
    }
}

- (void)setCellDataWithIconIV:(NSString *)iconName messageTitle:(NSString *)titleString isSeat:(BOOL)isSeat isCanChoose:(BOOL)isCanChoose isBackUp:(BOOL)isBackUp SeatTypeArr:(NSArray *)seatTypeArr SexTypeArr:(NSArray *)sexTypeArr
{
    if (isBackUp)
    {
        self.backUpTV.hidden = NO;
    }
    else
    {
        self.iconIV.image = [UIImage imageNamed:iconName];
        self.messageTitleLB.text = titleString;
        if (isCanChoose)
        {
            if (!isSeat)
            {
                self.messageTitleLB.textColor = RGBA(153, 153, 153, 1);
                self.nameTF.hidden = NO;
                self.messageTitleLB.hidden = YES;
                NSDictionary *sexDic = sexTypeArr[0];
                for (NSInteger i = 0;i < [[sexDic allKeys] count];i++)
                {
                    NSString *sexString = [[sexDic allValues] objectAtIndex:i];
                    UIButton *sexBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                    [sexBtn setTitle:sexString forState:UIControlStateNormal];
                    [sexBtn setTitleColor:RGBA(212, 116, 117, 1) forState:UIControlStateSelected];
                    sexBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
                    sexBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
                    sexBtn.layer.borderWidth = 0.8;
                    sexBtn.layer.cornerRadius = 4.0f;
                    [sexBtn setTitleColor:RGBA(153, 153, 153, 1) forState:UIControlStateNormal];
                    sexBtn.frame = CGRectMake(self.messageTitleLB.frame.origin.x + self.messageTitleLB.frame.size.width + 10 + (WidthRate(120) + 5) * i, 20, WidthRate(120) , 20);
                    sexBtn.tag = 100 + i;
                    [sexBtn addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventTouchUpInside];
                    [self.contentView addSubview:sexBtn];
                }
            }
            else
            {
                self.messageTitleLB.textColor = RGBA(85, 85, 85, 1);
                NSDictionary *seatDic = seatTypeArr[0];
                for (NSInteger i = 0;i < [[seatDic allKeys] count];i++)
                {
                    NSString *seatString = [seatDic allValues][i];
                    UIButton *seatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                    [seatBtn setTitle:seatString forState:UIControlStateNormal];
                    seatBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
                    seatBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
                    seatBtn.layer.borderWidth = 0.8;
                    seatBtn.layer.cornerRadius = 4.0f;
                    seatBtn.tag = 10000 + i;
                    [seatBtn setTitleColor:RGBA(153, 153, 153, 1) forState:UIControlStateNormal];
                    [seatBtn setTitleColor:RGBA(212, 116, 117, 1) forState:UIControlStateSelected];
                    seatBtn.frame = CGRectMake(self.messageTitleLB.frame.origin.x + self.messageTitleLB.frame.size.width + 10 + (WidthRate(120) + 5) * i, 20, WidthRate(120) , 20);
                    [seatBtn addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventTouchUpInside];
                    [self.contentView addSubview:seatBtn];
                }
            }
        }
        else
        {
            self.messageTitleLB.textColor = RGBA(85, 85, 85, 1);
        }
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    if ([textView.text length] == 0)
    {
        self.placeholderLB.hidden = NO;
    }
    else
    {
        self.placeholderLB.hidden = YES;
    }
}

- (void)changeValue:(UIButton *)sender
{
    
    UIButton *selectedSexBtn = (UIButton *)[self.contentView viewWithTag:self.selectedSexTag];
    UIButton *selectedSeatBtn = (UIButton *)[self.contentView viewWithTag:self.selectedSeatTag];
    
    if (sender.tag >= 10000)
    {
        selectedSeatBtn.selected = NO;
        selectedSeatBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
        sender.selected = YES;
        self.selectedSeatTag = sender.tag;
        sender.layer.borderColor = RGBA(212, 116, 117, 1).CGColor;
    }
    else
    {
        selectedSexBtn.selected = NO;
        selectedSexBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
        sender.selected = YES;
        self.selectedSexTag = sender.tag;
        sender.layer.borderColor = RGBA(212, 116, 117, 1).CGColor;
    }
}

@end
