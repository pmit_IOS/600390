//
//  ZSTECCBookTimeCell.m
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCBookTimeCell.h"

@implementation ZSTECCBookTimeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.titleLB)
    {
        NSString *titleString = @"预约时间";
        CGSize stringSize = [titleString sizeWithFont:[UIFont systemFontOfSize:18.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, stringSize.width, 20)];
        self.titleLB.font = [UIFont systemFontOfSize:14.0f];
        self.titleLB.textAlignment = NSTextAlignmentLeft;
        self.titleLB.textColor = RGBA(85, 85, 85, 1);
        self.titleLB.text = titleString;
        [self.contentView addSubview:self.titleLB];
        
        NSString *exampleTimeString = @"2015-01-01 00:00";
        CGSize exampleSize = [exampleTimeString sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
        self.bookTimeLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, exampleSize.width, 20)];
        self.bookTimeLB.textColor = RGBA(153, 153, 153, 1);
        self.bookTimeLB.textAlignment = NSTextAlignmentRight;
        self.bookTimeLB.font = [UIFont systemFontOfSize:11.0f];
        self.bookTimeLB.text = exampleTimeString;
        self.accessoryView = self.bookTimeLB;
    }
}

- (void)setCellDataWithTime:(NSString *)bookTimeString
{
    self.bookTimeLB.text = @"2015-08-11 11:40";
}

@end
