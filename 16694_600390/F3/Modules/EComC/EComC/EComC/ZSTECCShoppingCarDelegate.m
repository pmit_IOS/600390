//
//  ZSTECCShoppingCarDelegate.m
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShoppingCarDelegate.h"
#import "ZSTECCShoppingCarCell.h"

@implementation ZSTECCShoppingCarDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTECCShoppingCarCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shoppingCarCell"];
    [cell createUI];
    
    cell.plusBtn.tag = 10000 + indexPath.row;
    cell.minusBtn.tag = 1000 + indexPath.row;
    
    [cell.plusBtn addTarget:self action:@selector(changeCountValue:) forControlEvents:UIControlEventTouchUpInside];
    [cell.minusBtn addTarget:self action:@selector(changeCountValue:) forControlEvents:UIControlEventTouchUpInside];
    [cell setCellDataFoodName:@"" FoodPrice:@"" FoodCount:@""];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)changeCountValue:(PMRepairButton *)sender
{
    NSInteger senderTag = sender.tag;
    
    if (senderTag >= 10000)
    {
        NSInteger cellIndex = senderTag - 10000;
//        ZSTECCShoppingCarCell *cell = 
        
    }
    else
    {
        NSInteger cellIndex = senderTag - 1000;
    }
}

@end
