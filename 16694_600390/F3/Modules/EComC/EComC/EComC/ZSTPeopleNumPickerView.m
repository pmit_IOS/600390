//
//  ZSTPeopleNumPickerView.m
//  EComC
//
//  Created by pmit on 15/8/19.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTPeopleNumPickerView.h"

@implementation ZSTPeopleNumPickerView

- (void)createPickerView:(CGRect)rect
{
    self.backgroundColor = RGBA(236, 236, 236, 1);
    self.frame = rect;
    
    UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, 40)];
    whiteView.backgroundColor = [UIColor whiteColor];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(10, 0, 60, 40);
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelBtn.tag = 100;
    [cancelBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(clickBtnCancel:) forControlEvents:UIControlEventTouchUpInside];
    [whiteView addSubview:cancelBtn];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(rect.size.width - 70, 0, 60, 40);
    [sureBtn setTitle:@"完成" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cancelBtn.tag = 101;
    [sureBtn addTarget:self action:@selector(clickBtnSure:) forControlEvents:UIControlEventTouchUpInside];
    [whiteView addSubview:sureBtn];
    
    [self addSubview:whiteView];
    
    UIPickerView *pick = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, rect.size.width, rect.size.height - 40)];
    pick.delegate = self;
    pick.dataSource = self;
    [self addSubview:pick];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.maxCount;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@",@(row + 1)];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.seletedCount = row + 1;
}

- (void)clickBtnSure:(UIButton *)sender
{
    if ([self.peopleDelegate respondsToSelector:@selector(sureSelected:)])
    {
        if (self.seletedCount == 0)
        {
            self.seletedCount = 1;
        }
        [self.peopleDelegate sureSelected:self.seletedCount];
    }
}

- (void)clickBtnCancel:(UIButton *)sender
{
    if ([self.peopleDelegate respondsToSelector:@selector(cancelSeleted)])
    {
        [self.peopleDelegate cancelSeleted];
    }
}

@end
