//
//  ZSTF3Engine+EComC.m
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTF3Engine+EComC.h"
#import "ZSTGlobal+EComC.h"

@implementation ZSTF3Engine (EComC)

#pragma mark - 点餐系统
#pragma mark - 获取主页轮播图
- (void)getEcomCCarousel
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    
    
    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:getBannerURL,self.moduleType,[ZSTF3Preferences shared].ECECCID]
                                         params:params
                                         target:self
                                       selector:@selector(getEcomCCarouselResponse:userInfo:)];
}
- (void)getEcomCCarouselResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(getEcomCCarouselDidSucceed:)]) {
            [self.delegate getEcomCCarouselDidSucceed:carouseles];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getEcomCCarouselDidFailed:)]) {
            [self.delegate getEcomCCarouselDidFailed:response.resultCode];
            
            
        }
    }
}


#pragma mark - 获取个人订单列表信息
/**
 *  <#Description#>
 *
 *  @param userId      用户标识
 *  @param orderStatus 订单状态(1:待消费,2:已消费)
 */
-(void)getEcomCOrderListByUserId:(NSString *)userId andStatus:(NSString *)orderStatus{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:orderStatus forKey:@"orderStatus"];
    [params setSafeObject:userId forKey:@"userId"];
    
//    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:listOrderURL]
//                                         params:params
//                                         target:self
//                                       selector:@selector(getEcomCOrderListResponse:userInfo:)];
    [[ZSTCommunicator shared] openAPIPostDCToPaths:listOrderURL params:params target:self selector:@selector(getEcomCOrderListResponse:userInfo:)];
}

#pragma mark 结果返回
-(void)getEcomCOrderListResponse:(ZSTResponse *)response userInfo:(id)userInfo{
    
    if (response.resultCode == ZSTResultCode_OK) {
        
        NSDictionary *orderList = [response.jsonResponse safeObjectForKey:@"data"];
        
        if ([self isValidDelegateForSelector:@selector(getEcomCOrderListDidSucceed:)]) {
            [self.delegate getEcomCOrderListDidSucceed:orderList];
        }
        
    }else{
       
        if ([self isValidDelegateForSelector:@selector(getEcomCOrderListDidFailed:)]) {
            [self.delegate getEcomCOrderListDidFailed:response.resultCode];
        }
    
    }

}


#pragma mark - 根据经纬度获取当前位置
/**
 *  <#Description#>
 *
 *  @param userId 用户标识
 *  @param lat    纬度
 *  @param lng    经度
 */
-(void)getShopAddressInfoByUserId:(NSString *)userId andLat:(NSString *)lat andLng:(NSString *)lng{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:userId forKey:@"userId"];
    [params setSafeObject:lat forKey:@"lat"];
    [params setSafeObject:lng forKey:@"lng"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:getAddressInfoURL
                                         params:params
                                         target:self
                                       selector:@selector(getShopAddressInfoResponse:userInfo:)];
    

}

#pragma mark 结果返回
-(void)getShopAddressInfoResponse:(ZSTResponse *)response userInfo:(id)userInfo{

    NSLog(@"userInfo --> %@",response.jsonResponse);
    if (response.resultCode == ZSTResultCode_OK) {
        
        NSDictionary *addressInfo = [response.jsonResponse safeObjectForKey:@"data"];
        
        if ([self isValidDelegateForSelector:@selector(getShopAddressInfoDidSucceed:)]) {
            [self.delegate getShopAddressInfoDidSucceed:addressInfo];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(getShopAddressInfoDidFailed:)]) {
            [self.delegate getShopAddressInfoDidFailed:response.resultCode];
        }
        
    }
}



#pragma mark - 获取店铺列表
/**
 *  <#Description#>
 *
 *  @param userId 用户标识
 *  @param lat    纬度
 *  @param lng    经度
 */
-(void)getShopListByUserId:(NSString *)userId andLat:(NSString *)lat andLng:(NSString *)lng andCurrentPage:(NSInteger)currentPage PageSize:(NSInteger)pageSize
{

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:userId forKey:@"userId"];
    [params setSafeObject:lat forKey:@"lat"];
    [params setSafeObject:lng forKey:@"lng"];
    [params setSafeObject:@(currentPage) forKey:@"curPage"];
    [params setSafeObject:@(pageSize) forKey:@"pageSize"];
    
//    [[ZSTCommunicator shared] openAPIPostToPath:getShopListURL
//                                         params:params
//                                         target:self
//                                       selector:@selector(getShopAddressInfoResponse:userInfo:)];
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getShopDCListUrl params:params target:self selector:@selector(getShopAddressInfoResponse:userInfo:)];
}


#pragma mark 结果返回
-(void)getShopListResponse:(ZSTResponse *)response userInfo:(id)userInfo{
    
    if (response.resultCode == ZSTResultCode_OK) {
        
        NSDictionary *shopList = [response.jsonResponse safeObjectForKey:@"data"];
        
        if ([self isValidDelegateForSelector:@selector(getShopListDidSucceed:)]) {
            [self.delegate getShopListDidSucceed:shopList];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(getShopListDidFailed:)]) {
            [self.delegate getShopListDidFailed:response.resultCode];
        }
        
    }
}

- (void)getInitShopInfoByShopId:(NSString *)shopId
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:shopId forKey:@"shopId"];
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getInitOrderInfoUrl params:param target:self selector:@selector(getInitshopInfoResponse:userInfo:)];
}

- (void)getInitshopInfoResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK)
    {
        NSDictionary *orderInitDic = [response.jsonResponse safeObjectForKey:@"data"];
        if ([self isValidDelegateForSelector:@selector(getInitOrderInfoSuccessd:)])
        {
            [self.delegate getInitOrderInfoSuccessd:orderInitDic];
        }
        else
        {
            [self.delegate getInitOrderInfoFailed:response.resultCode];
        }
    }
}

- (void)getShopDishesByShopId:(NSString *)shopId AndUserId:(NSString *)userId
{
    NSDictionary *param = @{@"shopId":shopId,@"userId":userId};
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getSelectedDishesUrl params:param target:self selector:@selector(getShopDishesResponse:userInfo:)];
}

- (void)getShopDishesResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"response --> %@",response.jsonResponse);
    if (response.resultCode == ZSTResultCode_OK)
    {
        NSDictionary *dishesDic = [response.jsonResponse safeObjectForKey:@"data"];
        if ([self isValidDelegateForSelector:@selector(getDishesInfoSuccessed:)])
        {
            [self.delegate getDishesInfoSuccessed:dishesDic];
        }
        else
        {
            [self.delegate getDishesInfoFailed:response.resultCode];
        }
    }
}


#pragma mark - 获取店铺信息
-(void)getShopDetailByShopId:(NSString *)shopId{
    
    NSDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:shopId forKey:@"shopId"];
    
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getShopDetailUrl params:param target:self selector:@selector(getShopDetailResponse:userInfo:)];

}

#pragma mark 结果返回
-(void)getShopDetailResponse:(ZSTResponse *)response userInfo:(id)userInfo{

    
    if (response.resultCode == ZSTResultCode_OK)
    {
        NSDictionary *shopInfoDic = [response.jsonResponse safeObjectForKey:@"data"];
        if ([self isValidDelegateForSelector:@selector(getShopDetailDidSucceed:)])
        {
            [self.delegate getShopDetailDidSucceed:shopInfoDic];
        }
        else
        {
            [self.delegate getShopDetailDidFailed:response.resultCode];
        }
    }
}

#pragma mark - 获取店铺信息 用户评价
-(void)getShopReviewListByShopId:(NSString *)shopId{
 
    NSDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:shopId forKey:@"shopId"];
    
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getShopReviewListUrl params:param target:self selector:@selector(getShopDetailResponse:userInfo:)];

}


@end
