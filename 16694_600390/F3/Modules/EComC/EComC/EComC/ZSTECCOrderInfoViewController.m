//
//  ZSTECCOrderInfoViewController.m
//  EComC
//
//  Created by qiuguian on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCOrderInfoViewController.h"
#import "ZSTECCOrderInfoCell.h"
#import "ZSTECCOrderFoodInfoCell.h"
#import "ZSTECCSureOrderViewController.h"

@interface ZSTECCOrderInfoViewController ()

@end

@implementation ZSTECCOrderInfoViewController{

    UIView *bottomView;
    NSMutableDictionary *dic;
    NSMutableDictionary *foodDic;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView= [ZSTUtils titleViewWithTitle:NSLocalizedString(@"订单详情", nil)];
    
    self.view.backgroundColor = RGBCOLOR(244, 244, 244);
    
    [self initData];
    [self createTable];
    [self createBottomView];
}

-(void)initData{
   
    NSArray *array = @[@{@"orderCode":@"2015949721987487214",@"payStatus":@"未支付",@"orderTime":@"2015-08-05 12:00",@"bookTime":@"2015-08-05 12:00",@"bookType":@"订座点餐",@"bookPersonNum":@"6人",@"deskType":@"大厅",@"orderMoney":@"1300元",@"foodList":@[@{@"foodName":@"肉同内",@"number":@"X2",@"money":@"￥100"}]}];
    
    NSArray *array1 = @[@{@"foodName":@"肉同内",@"number":@"X2",@"money":@"￥100"}];
    
    dic = [[NSMutableDictionary alloc] init];
    dic = (NSMutableDictionary *)array[0];
    foodDic = [[NSMutableDictionary alloc] init];
    foodDic = (NSMutableDictionary *)array1[0];

}


#pragma mark - 创建table视图
-(void)createTable{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, HEIGHT-64-60)];
    _tableView.dataSource = self;
    _tableView.delegate= self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = RGBCOLOR(244, 244, 244);
    [self.view addSubview:self.tableView];
}

#pragma mark - 创建底部视图
-(void)createBottomView{

    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 50, WIDTH, 50)];
    [self.view addSubview:bottomView];
    
    UIButton *payBtn = [[UIButton alloc] initWithFrame:bottomView.bounds];
    [payBtn setBackgroundColor:[UIColor redColor]];
    [payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [payBtn setTitle:@"去付款" forState:UIControlStateNormal];
    [payBtn addTarget:self action:@selector(payBtnAction) forControlEvents:UIControlEventTouchUpInside];
    payBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [bottomView addSubview:payBtn];
    
}


#pragma  mark - 创建数据源
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 3;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (section == 1) {
        return 3;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  
    if (indexPath.section == 1) {
        return 40;
    }else if (indexPath.section == 2){
        return 40;
    }
    
    return 350;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (indexPath.section == 0) {
        static NSString *orderInfoCell = @"ZSTECCOrderInfoCell";
        
        ZSTECCOrderInfoCell *cell = (ZSTECCOrderInfoCell *)[tableView dequeueReusableCellWithIdentifier:orderInfoCell];
        if (!cell) {
            cell = [[ZSTECCOrderInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:orderInfoCell];
        }
        
        [cell createUI];
        [cell setCellData:dic];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    if (indexPath.section == 1) {
        static NSString *foodInfoCell = @"ZSTECCOrderFoodInfoCell";
        
        ZSTECCOrderFoodInfoCell *cell = (ZSTECCOrderFoodInfoCell *)[tableView dequeueReusableCellWithIdentifier:foodInfoCell];
        if (!cell) {
            cell = [[ZSTECCOrderFoodInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:foodInfoCell];
        }
        
        [cell createUI];
        [cell setCellData:foodDic];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    
    if (indexPath.section == 2) {
        
        UIImageView *sawImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 50, 320, 30)];
        [sawImage setImage:ZSTModuleImage(@"model_ecomc_sawbgdown.png")];
        
        static NSString * bottomSawCell = @"bottomSawCell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:bottomSawCell];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:bottomSawCell];
           
        }
        [cell addSubview:sawImage];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = RGBCOLOR(244, 244, 244);
        return  cell;
    }
    
    return nil;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (section ==1) {
        
        UILabel * label = [[UILabel alloc] init];
        label.frame = CGRectMake(10, 15, 300, 20);
        label.font=[UIFont fontWithName:@"Arial" size:15.0f];
        label.textColor = [UIColor darkGrayColor];
        label.text = @"已点菜品";
    
        
        UIView * sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 50)];
        [sectionView setBackgroundColor:RGBA(244, 244, 244, 1)];
        [sectionView addSubview:label];
        return sectionView;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 50;//0.1f;
    }
    return 0;
}


#pragma mark - 跳转到支付界页
-(void)payBtnAction{
    
    ZSTECCSureOrderViewController *sureOrderView = [[ZSTECCSureOrderViewController alloc] init];
    [self.navigationController pushViewController:sureOrderView animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
