//
//  ZSTECCReservationViewComtroller.m
//  EComC
//
//  Created by qiuguian  on 15/8/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCReservationViewComtroller.h"
#import "ZSTECCFoodInfoViewController.h"
#import "ZSTECCCarouselView.h"
#import "ZSTECCShop.h"
#import "ZSTECCShopCell.h"
#import "ZSTECCShopReviewsCell.h"
#import "ZSTECCBookSeatViewController.h"
#import "ZSTECCShopMessageViewController.h"

@interface ZSTECCReservationViewComtroller ()

@property (strong,nonatomic) UIViewController *currentVC;


@end


static NSString *const shopCell = @"shopCell";

@implementation ZSTECCReservationViewComtroller{
    UIView *titleBtnView;
    UIView *sitInfoView;
    UIView *shopInfoView;
    ZSTECCRoundRectView * _line;
    ZSTECCRoundRectView * _bookLine;
    ZSTECCRoundRectView * _shopLine;
    
    ZSTECCRoundRectView * bgView;
    ZSTECCRoundRectView * bottomView;
    
    UIButton *bookInfoBtn;
    UIButton *shopInfoBtn;
    
    UIImageView * rightImage;
}

#pragma mark - viewDidLoad 方法
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.view.backgroundColor = RGBCOLOR(246, 246, 246);
    
    //切换视图按钮
    titleBtnView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH,50)];
    titleBtnView.backgroundColor =[UIColor whiteColor];
    [self initTiltBtnView];
    [self.view addSubview:titleBtnView];

    
    //订座信息 视图
    sitInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-64-50)];
    sitInfoView.backgroundColor =RGBCOLOR(246, 246, 246);
    [self initSitInfoView];
    [self.view addSubview:sitInfoView];
    
    //店铺信息 视图
    shopInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-64-50)];
    shopInfoView.backgroundColor =[UIColor whiteColor];
    shopInfoView.hidden = YES;
    [self.view addSubview:shopInfoView];
    
    [self buildChildVC];
}

#pragma mark - 初始化切换视图按钮
-(void)initTiltBtnView{
   
    //订座信息 按钮
    bookInfoBtn= [[UIButton alloc] initWithFrame:CGRectMake(0, 5, WIDTH / 2, 50-4-5)];
    bookInfoBtn.titleLabel.textAlignment = UITextAlignmentCenter;
    [bookInfoBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    bookInfoBtn.tag = 1;
    bookInfoBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [bookInfoBtn setTitle:@"订座信息" forState:UIControlStateNormal];
    [bookInfoBtn addTarget:self action:@selector(bookInfoBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [titleBtnView addSubview:bookInfoBtn];
    
    //订座信息 底线
    _bookLine = [[ZSTECCRoundRectView alloc]initWithPoint:CGPointMake(0, 50-4) toPoint:CGPointMake(WIDTH/2, 40) borderWidth:4.0f borderColor:[UIColor redColor]];
    [titleBtnView addSubview:_bookLine];
    
    //店铺信息 按钮
    shopInfoBtn = [[UIButton alloc] initWithFrame:CGRectMake(160, 5, WIDTH / 2, 50-4-5)];
    shopInfoBtn.titleLabel.textAlignment = UITextAlignmentCenter;
    shopInfoBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [shopInfoBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    shopInfoBtn.tag =2 ;
    [shopInfoBtn setTitle:@"店铺信息" forState:UIControlStateNormal];
    [shopInfoBtn addTarget:self action:@selector(shopInfoAction) forControlEvents:UIControlEventTouchUpInside];
    [titleBtnView addSubview: shopInfoBtn];

    //订座信息 底线
    _shopLine = [[ZSTECCRoundRectView alloc] initWithPoint:CGPointMake(160, 50-4) toPoint:CGPointMake(320, 40) borderWidth:4.0f borderColor:[UIColor redColor]];
    _shopLine.hidden = YES;
    [titleBtnView addSubview:_shopLine];
    
}


#pragma  mark - 初始化订座信息视图
-(void)initSitInfoView{
    
    //底部视图
    bottomView = [[ZSTECCRoundRectView alloc]initWithFrame:CGRectMake(0, HEIGHT-64-50-60, 320,60) radius:0.0 borderWidth:0.0f borderColor:[UIColor grayColor]];
    bottomView.backgroundColor = RGBA(252, 252, 252, 1);
    [sitInfoView addSubview:bottomView];
    
    //订座 按钮
    UIButton *bookBtn1 = [[UIButton alloc] initWithFrame:CGRectMake(10,10, 130, 40)];
    bookBtn1.titleLabel.textAlignment = UITextAlignmentCenter;
    bookBtn1.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    bookBtn1.titleLabel.textColor = [UIColor whiteColor];
    bookBtn1.backgroundColor = [UIColor redColor];
    bookBtn1.layer.cornerRadius =5;
    [bookBtn1 setTitle:@"订座" forState:UIControlStateNormal];
    bookBtn1.tag =3;
    [bookBtn1 addTarget:self action:@selector(bookSitAction) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:bookBtn1];
    
    //订位点餐 按钮
    UIButton *bookBtn2 = [[UIButton alloc] initWithFrame:CGRectMake(180, 10, 130, 40)];
    bookBtn2.titleLabel.textAlignment = UITextAlignmentCenter;
    bookBtn2.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [bookBtn2 setBackgroundColor:[UIColor blackColor]];
    bookBtn2.backgroundColor = [UIColor lightGrayColor];
    bookBtn2.layer.cornerRadius =5;
    bookBtn2.tag = 4;
    [bookBtn2 addTarget:self action:@selector(foodInfoAction) forControlEvents:UIControlEventTouchUpInside];
    [bookBtn2 setTitle:@"订位点餐" forState:UIControlStateNormal];
    [bottomView addSubview:bookBtn2];

}

#pragma mark - 切换订座信息视图
-(void)bookInfoBtnAction{
    //订座信息
    sitInfoView.hidden = NO;
    _bookLine.hidden = NO;
    [bookInfoBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    //店铺信息
    shopInfoView.hidden = YES;
    _shopLine.hidden = YES;
    [shopInfoBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    if (![self.currentVC isKindOfClass:[ZSTECCBookSeatViewController class]])
    {
        [self transitionFromViewController:self.currentVC toViewController:self.childViewControllers[0] duration:0.3 options:UIViewAnimationOptionTransitionNone animations:^{
            self.currentVC = self.childViewControllers[0];
        } completion:^(BOOL finished) {
            
        }];
    }
    
}

#pragma mark - 切换店铺信息视图
-(void)shopInfoAction{
    //订座信息
    sitInfoView.hidden = YES;
    _bookLine.hidden = YES;
    [bookInfoBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    //店铺信息
    shopInfoView.hidden = NO;
    _shopLine.hidden = NO;
    [shopInfoBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    if (![self.currentVC isKindOfClass:[ZSTECCShopMessageViewController class]])
    {
        [self transitionFromViewController:self.currentVC toViewController:self.childViewControllers[1] duration:0.3 options:UIViewAnimationOptionTransitionNone animations:^{
            self.currentVC = self.childViewControllers[1];
        } completion:^(BOOL finished) {
            
        }];
    }
    
}


- (void)buildChildVC
{
    ZSTECCBookSeatViewController *bookSeatMessage = [[ZSTECCBookSeatViewController alloc] init];
    bookSeatMessage.shopId = self.shopId;
    [self addChildViewController:bookSeatMessage];
    self.currentVC = bookSeatMessage;
    [self.view addSubview:bookSeatMessage.view];
    
    ZSTECCShopMessageViewController *shopMessage = [[ZSTECCShopMessageViewController alloc] init];
    [self addChildViewController:shopMessage];
}


#pragma mark - 确定订位
-(void)bookSitAction{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请写完整资料，谢谢！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - 确定订位点餐
-(void)foodInfoAction{
    ZSTECCFoodInfoViewController *foodView = [[ZSTECCFoodInfoViewController alloc] init];
    foodView.shopId = self.shopId;
    [self.navigationController pushViewController:foodView animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


@end
