//
//  ZSTECCEvaluateViewController.m
//  EComC
//
//  Created by qiuguian on 8/11/15.
//  Copyright (c) 2015 pmit. All rights reserved.
//

#import "ZSTECCEvaluateViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PMRepairButton.h"

@interface ZSTECCEvaluateViewController ()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    /**
     *  图片是否已放大
     */
    BOOL isFullScreen;
    /**
     *  没加载图片是不能点击
     */
    BOOL allowClick;
}

@property (strong,nonatomic) PMRepairButton *addPhotoBtn;
@property (strong,nonatomic) NSMutableArray *myImageArr;
@property (strong,nonatomic) UIImageView *noNameIV;
@property (assign,nonatomic) BOOL isNoName;

@end

@implementation ZSTECCEvaluateViewController{

    UIView *starView;
    UIView *contentView;
    UIView *imageView;
    UIView  *bgView;
    UIView *nuomingView;
    UIView  *backgroundView;
    UIView  *bottomView;
    
    PMRepairButton *starBtn1;
    UIButton *starBtn2;
    UIButton *starBtn3;
    UIButton *starBtn4;
    UIButton *starBtn5;
    
    UITextView *textView;
    UILabel *wordNum;
    
    UIButton *sendBtn;
}

#define button(i) [[NSString stringWithFormat:@"100%d",i] intValue]
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self registerForKeyboardNotifications];
    self.myImageArr = [NSMutableArray array];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回",@"") target:self selector:@selector(popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:@"评价"];
    
    [self initView];

    bottomView =[[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT-64-60, 320, 60)];
    bottomView.backgroundColor = RGBA(252, 252, 252, 1);
    [self.view addSubview:bottomView];
    
    sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBtn.frame = CGRectMake(10, 10, WIDTH - 20, 40);
    [sendBtn setTitle:@"发    送" forState:UIControlStateNormal];
//    [sendBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [sendBtn setBackgroundColor:[UIColor redColor]];
    sendBtn.layer.cornerRadius = 4.0f;
    [bottomView addSubview:sendBtn];
}


-(void)initView{
    
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH ,HEIGHT - 64 - 60)];
    backgroundView.backgroundColor =RGBCOLOR(244, 244, 244);
    [self.view addSubview:backgroundView];
    
    bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 320, 210)];
    bgView.backgroundColor = [UIColor whiteColor];
    [backgroundView addSubview:bgView];
   
    starView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    starView.backgroundColor = [UIColor whiteColor];
    [bgView addSubview:starView];
    starBtn1 = [[PMRepairButton alloc] init];
    starBtn1.frame = CGRectMake(10, 10, 20, 20);
    [starBtn1 setImage:[UIImage imageNamed:@"dStart.png"] forState:UIControlStateNormal];
    [starBtn1 setImage:[UIImage imageNamed:@"pStart.png"] forState:UIControlStateSelected];
    starBtn1.tag =1004;
    [starBtn1 addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    starBtn2 = [[PMRepairButton alloc] initWithFrame:CGRectMake(10+30, 10, 20, 20)];
    [starBtn2 setImage:[UIImage imageNamed:@"dStart.png"] forState:UIControlStateNormal];
    [starBtn2 setImage:[UIImage imageNamed:@"pStart.png"] forState:UIControlStateSelected];
    starBtn2.tag =1003;
    [starBtn2 addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    starBtn3 = [[PMRepairButton alloc] initWithFrame:CGRectMake(10+30*2, 10, 20, 20)];
    [starBtn3 setImage:[UIImage imageNamed:@"dStart.png"] forState:UIControlStateNormal];
    [starBtn3 setImage:[UIImage imageNamed:@"pStart.png"] forState:UIControlStateSelected];
    starBtn3.tag =1002;
    [starBtn3 addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    starBtn4 = [[PMRepairButton alloc] initWithFrame:CGRectMake(10+30*3, 10, 20, 20)];
    [starBtn4 setImage:[UIImage imageNamed:@"dStart.png"] forState:UIControlStateNormal];
    [starBtn4 setImage:[UIImage imageNamed:@"pStart.png"] forState:UIControlStateSelected];
    starBtn4.tag = 1001;
    [starBtn4 addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    starBtn5 = [[PMRepairButton alloc] initWithFrame:CGRectMake(10+30*4, 10, 20, 20)];
    [starBtn5 setImage:[UIImage imageNamed:@"dStart.png"] forState:UIControlStateNormal];
    [starBtn5 setImage:[UIImage imageNamed:@"pStart.png"] forState:UIControlStateSelected];
    starBtn5.tag =1000;
    [starBtn5 addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [starView addSubview:starBtn1];
    [starView addSubview:starBtn2];
    [starView addSubview:starBtn3];
    [starView addSubview:starBtn4];
    [starView addSubview:starBtn5];

    contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 320, 80)];
    [bgView addSubview:contentView];
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    line.alpha = 0.5;
    [contentView addSubview:line];
    
    textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 1, 300, 60)];
    textView.text = @"";
    textView.delegate = self;
    textView.tag = 1;
    textView.textColor = [UIColor grayColor];
    textView.font = [UIFont systemFontOfSize:15];
    [contentView addSubview:textView];
    
    
    wordNum = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH-60-10, 60, 60, 30)];
    wordNum.text = @"0/50";
    wordNum.textColor = [UIColor lightGrayColor];
    wordNum.font = [UIFont systemFontOfSize:14];
    wordNum.textAlignment = UITextAlignmentRight;
    [contentView addSubview:wordNum];
    
    line = [[UILabel alloc] initWithFrame:CGRectMake(10, 60+30, 300, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    line.alpha = 0.5;
    [contentView addSubview:line];
    
    
//    imageView = [[UIView alloc] initWithFrame:CGRectMake(0, 50+89, 320, 80)];
//    [bgView addSubview:imageView];
//    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 60, 60)];
//    [image setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://zjdaily.zjol.com.cn/csjr/images/2006-09/30/csjr20060930c0008v01b006.jpg"]]];
//    image.userInteractionEnabled = YES;
//    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectImage)];
//    [image addGestureRecognizer:singleTap1];
//    
//    [imageView addSubview:image];
//    
//    UIButton *deleate = [[UIButton alloc] initWithFrame:CGRectMake(60-20, 0, 20, 20)];
//    deleate.backgroundColor =[UIColor blackColor];
//    [deleate setTitle:@"X" forState:UIControlStateNormal];
//    [deleate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [deleate addTarget:self action:@selector(deleteImage:) forControlEvents:UIControlEventTouchUpInside];
//    [image addSubview:deleate];
//    
//    [self addImage];
    imageView = [[UIView alloc] initWithFrame:CGRectMake(0, 40 + 89, WIDTH, 80)];
    [bgView addSubview:imageView];
    
    self.addPhotoBtn = [[PMRepairButton alloc] init];
    self.addPhotoBtn.frame = CGRectMake(10, 10, 60, 60);
    [self.addPhotoBtn setImage:[UIImage imageNamed:@"ecomcDefaultImg.png"] forState:UIControlStateNormal];
    [self.addPhotoBtn addTarget:self action:@selector(addMyImage:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:self.addPhotoBtn];

    
    nuomingView = [[UIView alloc] initWithFrame:CGRectMake(0, 220+20, WIDTH, 40)];
    nuomingView.backgroundColor = [UIColor whiteColor];
    [backgroundView addSubview:nuomingView];
    
//    UIButton *b1 = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 100, 20)];
//    [b1 setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//    [b1 setTitle:@"匿名评价" forState:UIControlStateNormal];
//    [nuomingView addSubview:b1];
    
    UILabel *noNameLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 20)];
    noNameLB.textColor = RGBA(153, 153, 153, 1);
    noNameLB.textAlignment = NSTextAlignmentLeft;
    noNameLB.text = @"匿名评价";
    noNameLB.font = [UIFont systemFontOfSize:15.0f];
    [nuomingView addSubview:noNameLB];
    
    UIImageView *okIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH-10-20, 10, 20, 20)];
    okIV.contentMode = UIViewContentModeScaleAspectFit;
    okIV.image = [UIImage imageNamed:@"ok.png"];
    self.noNameIV = okIV;
    okIV.hidden = YES;
    [nuomingView addSubview:okIV];
    
    UIButton *nonameBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    nonameBtn.frame = (CGRect){CGPointZero,nuomingView.bounds.size};
    [nonameBtn addTarget:self action:@selector(chooseIsNoNameStaus:) forControlEvents:UIControlEventTouchUpInside];
    [nuomingView addSubview:nonameBtn];

}

- (void)addMyImage:(PMRepairButton *)sender
{
    UIActionSheet *sheet;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] ) {
        sheet=[[UIActionSheet alloc]initWithTitle:@"选择" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"拍照" otherButtonTitles:@"从手机相册中选择", nil];
    }else{
        sheet=[[UIActionSheet alloc]initWithTitle:@"选择" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"从手机相册中选择" otherButtonTitles: nil];
    }
    [sheet showInView:self.view];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{

    if (range.location>=50)
    {
        
        return  NO;
    }
    else
    {
        wordNum.text = [NSString stringWithFormat:@"%lu/50",(unsigned long)range.location+1];
        return YES;
    }

}

-(void)starBtnAction:(UIButton *)sender
{
    NSInteger tapTag = sender.tag;
    UIButton *levelOneBtn = (UIButton *)[starView viewWithTag:1004];
    UIButton *levelTwoBtn = (UIButton *)[starView viewWithTag:1003];
    UIButton *levelThreeBtn = (UIButton *)[starView viewWithTag:1002];
    UIButton *levelFourBtn = (UIButton *)[starView viewWithTag:1001];
    UIButton *levelFiveBtn = (UIButton *)[starView viewWithTag:1000];
    
    if (tapTag == 1000)
    {
        if (sender.isSelected)
        {
            sender.selected = NO;
        }
        else
        {
            levelOneBtn.selected = YES;
            levelTwoBtn.selected = YES;
            levelThreeBtn.selected = YES;
            levelFourBtn.selected = YES;
            levelFiveBtn.selected = YES;
        }
    }
    else if (tapTag == 1001)
    {
        if (sender.isSelected)
        {
            if (levelFiveBtn.isSelected)
            {
                levelFiveBtn.selected = NO;
            }
            else
            {
                sender.selected = NO;
            }
        }
        else
        {
            levelOneBtn.selected = YES;
            levelTwoBtn.selected = YES;
            levelThreeBtn.selected = YES;
            levelFourBtn.selected = YES;
        }
    }
    else if (tapTag == 1002)
    {
        if (sender.isSelected)
        {
            if (levelFourBtn.isSelected || levelFiveBtn.isSelected)
            {
                levelFiveBtn.selected = NO;
                levelFourBtn.selected = NO;
            }
            else
            {
                sender.selected = NO;
            }
        }
        else
        {
            levelOneBtn.selected = YES;
            levelTwoBtn.selected = YES;
            levelThreeBtn.selected = YES;
        }
    }
    else if (tapTag == 1003)
    {
        if (sender.isSelected)
        {
            if (levelFiveBtn.isSelected || levelFourBtn.isSelected || levelThreeBtn.isSelected)
            {
                levelThreeBtn.selected = NO;
                levelFourBtn.selected = NO;
                levelFiveBtn.selected = NO;
            }
            else
            {
                sender.selected = NO;
            }
        }
        else
        {
            levelOneBtn.selected = YES;
            levelTwoBtn.selected = YES;
        }
    }
    else
    {
        if (sender.isSelected)
        {
            if (levelFiveBtn.isSelected || levelFourBtn.isSelected || levelThreeBtn.isSelected || levelTwoBtn.isSelected)
            {
                levelTwoBtn.selected = NO;
                levelThreeBtn.selected = NO;
                levelFourBtn.selected = NO;
                levelFiveBtn.selected = NO;
            }
        }
    }
    
}


-(void)initStar:(UIButton *)btn tag:(int)tag forEeach:(int)i{
    
    UIButton *myButton = (UIButton *)[self.view viewWithTag:(button(i))];

    [myButton setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"] forState:UIControlStateNormal];


    
}


-(void)addImage{

    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(10+70, 10, 60, 60)];
    [image setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://zjdaily.zjol.com.cn/csjr/images/2006-09/30/csjr20060930c0008v01b006.jpg"]]];
    [imageView addSubview:image];
    
    UIButton *deleate = [[UIButton alloc] initWithFrame:CGRectMake(60-20, 0, 20, 20)];
    deleate.backgroundColor =[UIColor blackColor];
    [deleate setTitle:@"X" forState:UIControlStateNormal];
    [deleate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [image addSubview:deleate];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


- (void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(keyboardWasHidden:) name:UIKeyboardDidHideNotification object:nil];
}

- (void) keyboardWasShown:(NSNotification *) notif
{
    NSDictionary *info = [notif userInfo];
    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    
    NSLog(@"keyBoard:%f", keyboardSize.height);  //216
    
    bottomView.frame = CGRectMake(0, HEIGHT-64-60-keyboardSize.height-40, 320, 60);
    
    ///keyboardWasShown = YES;
}
- (void) keyboardWasHidden:(NSNotification *) notif
{
    NSDictionary *info = [notif userInfo];
    
    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    NSLog(@"keyboardWasHidden keyBoard:%f", keyboardSize.height);
    
    bottomView.frame = CGRectMake(0, HEIGHT-64-60, 320, 60);
    // keyboardWasShown = NO;
    
}


//类型
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSInteger sourceType=0;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        switch (buttonIndex) {
            case 2:
                return;
                break;
                
            case 0:
                sourceType=UIImagePickerControllerSourceTypeCamera;
                break;
            case 1:
                sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
                break;
        }
    }else{
        sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    UIImagePickerController *imagePicker=[[UIImagePickerController alloc]init];
    imagePicker.delegate=self;
    imagePicker.allowsEditing=YES;
    imagePicker.sourceType=sourceType;
    [self presentViewController:imagePicker animated:YES completion:^{}];
}


//UIImagePickerController 代理方法
-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    [picker dismissViewControllerAnimated:YES completion:^{}];
    UIImage *img=[info objectForKey:UIImagePickerControllerOriginalImage];
    
    [self performSelector:@selector(selectPic:) withObject:img afterDelay:0.1];
    
}


//UIImagePickerController 代理方法
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}



- (void)selectPic:(UIImage*)image
{
    UIImageView *myNewsIV = [[UIImageView alloc] initWithFrame:CGRectMake(10 + (self.myImageArr.count) * 70, 10, 60, 60)];
    myNewsIV.image = image;
    myNewsIV.contentMode = UIViewContentModeScaleAspectFit;
    myNewsIV.userInteractionEnabled = YES;
    PMRepairButton *deleteBtn = [[PMRepairButton alloc] init];
    deleteBtn.frame = CGRectMake(40, 0, 20, 20);
    [deleteBtn setImage:[UIImage imageNamed:@"deletePhoto.png"] forState:UIControlStateNormal];
    deleteBtn.tag = 100 + self.myImageArr.count;
    [deleteBtn addTarget:self action:@selector(deletePhoto:) forControlEvents:UIControlEventTouchUpInside];
    [myNewsIV addSubview:deleteBtn];
    [imageView addSubview:myNewsIV];
    
    [self.myImageArr addObject:myNewsIV];
    
    if (self.myImageArr.count == 4)
    {
        self.addPhotoBtn.hidden = YES;
    }
    else
    {
        self.addPhotoBtn.frame = CGRectMake(10 + self.myImageArr.count * 70, 10, 60, 60);
    }
    
    
}


- (void)deletePhoto:(PMRepairButton *)sender
{
    UIImageView *delelteIV = (UIImageView *)(sender.superview);
    CGRect deleteFrame = delelteIV.frame;
    [UIView animateWithDuration:0.3f animations:^{
        
        sender.hidden = YES;
        delelteIV.frame = CGRectMake(0, 0, 0, 0);
        
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3f animations:^{
            
            NSInteger index = [self.myImageArr indexOfObject:delelteIV];
            if (index == 3)
            {
                self.addPhotoBtn.hidden = NO;
                self.addPhotoBtn.frame = deleteFrame;
            }
            else if (index == 2)
            {
                if (self.addPhotoBtn.hidden)
                {
                    UIImageView *lastIV = [self.myImageArr lastObject];
                    self.addPhotoBtn.hidden = NO;
                    self.addPhotoBtn.frame = lastIV.frame;
                    lastIV.frame = deleteFrame;
                }
                else
                {
                    self.addPhotoBtn.frame = deleteFrame;
                }
            }
            else if (index == 1)
            {
                if (self.addPhotoBtn.hidden)
                {
                    UIImageView *lastSecondIV = [self.myImageArr objectAtIndex:2];
                    UIImageView *lastIV = [self.myImageArr lastObject];
                    self.addPhotoBtn.hidden = NO;
                    self.addPhotoBtn.frame = lastIV.frame;
                    lastIV.frame = lastSecondIV.frame;
                    lastSecondIV.frame = deleteFrame;
                    
                }
                else
                {
                    if (index == self.myImageArr.count - 1)
                    {
                        self.addPhotoBtn.frame = deleteFrame;
                    }
                    else
                    {
                        UIImageView *lastIV = [self.myImageArr lastObject];
                        self.addPhotoBtn.frame = lastIV.frame;
                        lastIV.frame = deleteFrame;
                    }
                }
            }
            else
            {
                if (self.addPhotoBtn.hidden)
                {
                    UIImageView *lastThirdIV = [self.myImageArr objectAtIndex:1];
                    UIImageView *lastSecondIV = [self.myImageArr objectAtIndex:2];
                    UIImageView *lastIV = [self.myImageArr lastObject];
                    self.addPhotoBtn.hidden = NO;
                    self.addPhotoBtn.frame = lastIV.frame;
                    lastIV.frame = lastThirdIV.frame;
                    lastThirdIV.frame = lastSecondIV.frame;
                    lastSecondIV.frame = deleteFrame;
                }
                else
                {
                    if (index == self.myImageArr.count - 1)
                    {
                        self.addPhotoBtn.frame = deleteFrame;
                    }
                    else
                    {
                        if (self.myImageArr.count == 2)
                        {
                            UIImageView *lastIV = [self.myImageArr lastObject];
                            self.addPhotoBtn.frame = lastIV.frame;
                            lastIV.frame = deleteFrame;
                        }
                        else
                        {
                            UIImageView *lastSecondIV = [self.myImageArr objectAtIndex:1];
                            UIImageView *lastIV = [self.myImageArr lastObject];
                            self.addPhotoBtn.hidden = NO;
                            self.addPhotoBtn.frame = lastIV.frame;
                            lastIV.frame = lastSecondIV.frame;
                            lastSecondIV.frame = deleteFrame;
                        }
                    }
                }
            }
            
        } completion:^(BOOL finished) {
            [self.myImageArr removeObject:delelteIV];
            [delelteIV removeFromSuperview];
            
        }];
        
        
        
        
    }];
}

- (void)chooseIsNoNameStaus:(UIButton *)sender
{
    if (self.noNameIV.hidden)
    {
        self.noNameIV.hidden = NO;
        self.isNoName = YES;
    }
    else
    {
        self.noNameIV.hidden = YES;
        self.isNoName = NO;
    }
}

@end
