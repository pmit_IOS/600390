//
//  ZSTECCBookSeatViewController.m
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCBookSeatViewController.h"
#import "ZSTECCMessageCell.h"

@interface ZSTECCBookSeatViewController() <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSArray *titleArr;
@property (strong,nonatomic) NSArray *iconArr;
@property (strong,nonatomic) UIView *shadowView;
@property (assign,nonatomic) NSInteger maxCount;
@property (strong,nonatomic) NSArray *seatTypeArr;
@property (strong,nonatomic) NSArray *sexArr;
@property (strong,nonatomic) NSIndexPath *nowIndexPath;

@end

@implementation ZSTECCBookSeatViewController

static NSString *const messageCell = @"messageCell";

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.titleArr = @[@[@"预约时间",@"预约人数",@"餐桌类型"],@[@"输入联系人",@"联系方式"]];
    self.iconArr = @[@[@"bookSeat.png",@"peopleCount.png",@"seat.png"],@[@"linkMen.png",@"phone.png"]];
    self.view.frame = CGRectMake(0, 47, WIDTH, HEIGHT - 47 - 64 - 60);
    [self buildTableView];
    [self createYMSelected];
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    [self.engine getInitShopInfoByShopId:self.shopId];
    
    
}

- (void)buildTableView
{
    self.seatMessageTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.view.bounds.size.height) style:UITableViewStylePlain];
    self.seatMessageTableView.delegate = self;
    self.seatMessageTableView.dataSource = self;
    [self.seatMessageTableView registerClass:[ZSTECCMessageCell class] forCellReuseIdentifier:messageCell];
    self.seatMessageTableView.backgroundColor = RGBA(244, 244, 244, 1);
    [self.view addSubview:self.seatMessageTableView];
    
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    shadowView.backgroundColor = [UIColor lightGrayColor];
    shadowView.alpha = 0.5;
    shadowView.hidden = YES;
    self.shadowView = shadowView;
    [self.view addSubview:shadowView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titleArr.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section < self.titleArr.count)
    {
        return [[self.titleArr objectAtIndex:section] count];
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTECCMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:messageCell];
    [cell createUI];
    if (indexPath.section < self.titleArr.count)
    {
        BOOL isCanChoose = NO;
        BOOL isSeat = NO;
        if (indexPath.section == 0 && indexPath.row == 2)
        {
            isCanChoose = YES;
            isSeat = YES;
        }
        else if (indexPath.section == 1 && indexPath.row == 0)
        {
            isCanChoose = YES;
            isSeat = NO;
        }
        
        [cell setCellDataWithIconIV:self.iconArr[indexPath.section][indexPath.row] messageTitle:self.titleArr[indexPath.section][indexPath.row] isSeat:isSeat isCanChoose:isCanChoose isBackUp:NO SeatTypeArr:self.seatTypeArr SexTypeArr:self.sexArr];
    }
    else
    {
        [cell setCellDataWithIconIV:@"" messageTitle:@"备注" isSeat:NO isCanChoose:NO isBackUp:YES SeatTypeArr:self.seatTypeArr SexTypeArr:self.sexArr];
    }
    
    if (indexPath.section && indexPath.row == 1)
    {
        cell.phoneTF.hidden = NO;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section < self.titleArr.count)
    {
        return 60;
    }
    else
    {
        return 120;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] init];
    sectionHeaderView.backgroundColor = RGBA(244, 244, 244, 1);
    return sectionHeaderView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0)
    {
        self.nowIndexPath = indexPath;
        [self showTimePicker];
    }
    else if (indexPath.section == 0 && indexPath.row == 1)
    {
        self.nowIndexPath = indexPath;
        [self showCountView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.seatMessageTableView)
    {
        CGFloat sectionHeaderHeight = 10;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

- (void)buildHideView
{
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 300)];
    shadowView.backgroundColor = [UIColor lightGrayColor];
    shadowView.alpha = 0.5;
    shadowView.hidden = YES;
    self.shadowView = shadowView;
    [self.view addSubview:shadowView];
}

#pragma mark - 年月选择器
- (void)createYMSelected
{
    NSDate *nowDate = [NSDate new];
    NSCalendar *calendar =  [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit |
    NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:nowDate];
    NSInteger year = [dateComponent year];
    NSInteger month = [dateComponent month];
    NSInteger day = [dateComponent day];
    NSInteger hour = [dateComponent hour];
    NSInteger minute = [dateComponent minute];
    
    NSLog(@"year is: %ld", year);
    NSLog(@"month is: %ld", month);
    NSLog(@"day is: %ld", day);
    NSLog(@"hour is: %ld", hour);
    NSLog(@"minute is: %ld", minute);
    
    ZSTECCTimePickerView *pickView = [[ZSTECCTimePickerView alloc] init];
    self.pickView = pickView;
    self.pickView.timeDelegate = self;
    pickView.nowYear = [NSString stringWithFormat:@"%@",@(year)];
    pickView.nowMonth = [NSString stringWithFormat:@"%@",@(2)];
    pickView.nowDay = [NSString stringWithFormat:@"%@",@(27)];
    [pickView createUIWithFrame:CGRectMake(0, HEIGHT, WIDTH, 300)];
    [self.view addSubview:pickView];
}

- (void)showTimePicker
{
    self.shadowView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT - 300 - 64, WIDTH, 300);
    }];
}

- (void)cancelPickerView
{
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 300);
    }];
}

- (void)getInitOrderInfoSuccessd:(NSDictionary *)response
{
    self.maxCount = [[response safeObjectForKey:@"yyrsCount"] integerValue];
    self.seatTypeArr = [response safeObjectForKey:@"czlxList"];
    self.sexArr = [response safeObjectForKey:@"yyrlxList"];
    [self.seatMessageTableView reloadData];
    [self buildCountView];
}

- (void)getInitOrderInfoFailed:(int)resultCode
{
    
}

- (void)buildCountView
{
    ZSTPeopleNumPickerView *pickView = [[ZSTPeopleNumPickerView alloc] init];
    self.peoplePickerView = pickView;
    pickView.maxCount = self.maxCount;
    pickView.peopleDelegate = self;
    [pickView createPickerView:CGRectMake(0, HEIGHT, WIDTH, 300)];
    [self.view addSubview:pickView];
}

- (void)showCountView
{
    self.shadowView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.peoplePickerView.frame = CGRectMake(0, HEIGHT - 300 - 64, WIDTH, 300);
    }];
}

- (void)sureSelected:(NSInteger)count
{
    self.shadowView.hidden = YES;
    
    ZSTECCMessageCell *cell = (ZSTECCMessageCell *)[self.seatMessageTableView cellForRowAtIndexPath:self.nowIndexPath];
    cell.rightLB.hidden = NO;
    cell.rightLB.text = [NSString stringWithFormat:@"%@人",@(count)];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.peoplePickerView.frame = CGRectMake(0, HEIGHT, WIDTH, 300);
    }];
}

- (void)cancelSeleted
{
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.peoplePickerView.frame = CGRectMake(0, HEIGHT, WIDTH, 300);
    }];
}

- (void)sureTimeSelected:(NSString *)stringDate
{
    ZSTECCMessageCell *cell = (ZSTECCMessageCell *)[self.seatMessageTableView cellForRowAtIndexPath:self.nowIndexPath];
    cell.rightLB.hidden = NO;
    cell.rightLB.text = stringDate;
    
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 300);
    }];
}

- (void)cancelTimeSelected
{
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 300);
    }];
}

@end
