//
//  ZSTECCShopInfoAddressTableViewCell.m
//  EComC
//
//  Created by anqiu on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShopInfoAddressTableViewCell.h"
#import "ZSTECCAutoLabel.h"


@implementation ZSTECCShopInfoAddressTableViewCell{
  
    UIView *v1;
    UIView *v2;
    UIView *v3;

}

-(void)createUI{

    if (!v1)
    {
        v1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
        [self.contentView addSubview:v1];
        UILabel *l1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 70, 20)];
        l1.font = [UIFont systemFontOfSize:14];
        l1.text = @"联系电话:";
        l1.textColor = [UIColor grayColor];
        [v1 addSubview:l1];
        self.phone = [[UILabel alloc] initWithFrame:CGRectMake(10+70, 5, 210, 20)];
        //self.phone.text = @"020-8098290384";
        self.phone.font = [UIFont systemFontOfSize:14];
        self.phone.textColor = [UIColor redColor];
        [v1 addSubview:self.phone];
        
        v2 = [[UIView alloc] initWithFrame:CGRectMake(0, 25, 320, 20)];
        [self.contentView addSubview:v2];
        UILabel *l21 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 70, 20)];
        l21.text = @"预约时间:";
        l21.font = [UIFont systemFontOfSize:14];
        l21.textColor = [UIColor grayColor];
        [v2 addSubview:l21];
        self.time = [[UILabel alloc] initWithFrame:CGRectMake(10+70, 0, 210, 20)];
        self.time.font = [UIFont systemFontOfSize:14];
        self.time.textColor = [UIColor grayColor];
        [v2 addSubview:self.time];
        
        v3 = [[UIView alloc] initWithFrame:CGRectMake(0, 25+20, 320, 20)];
        [self.contentView addSubview:v3];
        UILabel *l31 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 70, 20)];
        l31.text = @"店铺地址:";
        l31.font = [UIFont systemFontOfSize:14];
        l31.textColor = [UIColor grayColor];
        [v3 addSubview:l31];
        self.addressLabel = [[ZSTECCAutoLabel alloc] initWithFrame:CGRectMake(10+70, 0, 230, 40)];
        self.addressLabel.textColor = [UIColor grayColor];
        self.addressLabel.characterSpacing = 0;
        self.addressLabel.lineBreakMode = UILineBreakModeWordWrap;
        self.addressLabel.numberOfLines = 0;
        self.addressLabel.font = [UIFont systemFontOfSize:14];
        [self.addressLabel setTextColor:[UIColor grayColor]];
        
        [v3 addSubview:self.addressLabel];
    }
}



-(void)setCellData:(NSDictionary *)dic{
   
    self.phone.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"phone"]];
    self.time.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"time"]];
    self.addressLabel.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"address"]];
    
    _height = [self.addressLabel getAttributedStringHeightWidthValue:230];
    
    v3.frame = CGRectMake(0, 20+25, 320,_height+10);
    self.addressLabel.frame = CGRectMake(10+70, 5, 230, _height+10);

}




@end
