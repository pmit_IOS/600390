//
//  ZSTECCSureOrderViewController.m
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCSureOrderViewController.h"
#import "ZSTECCAddressCell.h"
#import "ZSTECCBookTimeCell.h"
#import "ZSTECCPayWayCell.h"
#import "ZSTECCOrderProductCell.h"

@interface ZSTECCSureOrderViewController() <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *orderSureTableView;
@property (strong,nonatomic) NSString *payWay;
@property (strong,nonatomic) NSIndexPath *currentIndexPath;
@property (strong,nonatomic) UILabel *priceLB;

@end

@implementation ZSTECCSureOrderViewController

static NSString *const addressCell = @"addressCell";
static NSString *const bookTimeCell = @"bookTimeCell";
static NSString *const payWayCell = @"payWayCell";
static NSString *const orderProductCell = @"orderProductCell";

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"订单确认", nil)];
    self.currentIndexPath = [NSIndexPath indexPathForRow:-1 inSection:2];
    [self buildTableView];
    [self buildBottomView];
    
}

- (void)buildTableView
{
    self.orderSureTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 60) style:UITableViewStylePlain];
    self.orderSureTableView.delegate = self;
    self.orderSureTableView.dataSource = self;
    self.orderSureTableView.backgroundColor = RGBA(244, 244, 244, 1);
    [self.orderSureTableView registerClass:[ZSTECCAddressCell class] forCellReuseIdentifier:addressCell];
    [self.orderSureTableView registerClass:[ZSTECCBookTimeCell class] forCellReuseIdentifier:bookTimeCell];
    [self.orderSureTableView registerClass:[ZSTECCPayWayCell class] forCellReuseIdentifier:payWayCell];
    [self.orderSureTableView registerClass:[ZSTECCOrderProductCell class] forCellReuseIdentifier:orderProductCell];
    [self.view addSubview:self.orderSureTableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = RGBA(244, 244, 244, 1);
    self.orderSureTableView.tableFooterView = footerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 2 || section == 3)
    {
        return 3;
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        ZSTECCAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:addressCell];
        [cell createUI];
        [cell setCellDataWithShopName:@"" ShopAddress:@""];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 1)
    {
        ZSTECCBookTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:bookTimeCell];
        [cell createUI];
        [cell setCellDataWithTime:@""];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 2)
    {
        BOOL isTitle = NO;
        BOOL isWeChat = NO;
        if (indexPath.row == 0)
        {
            isTitle = YES;
        }
        else
        {
            isTitle = NO;
        }
        
        ZSTECCPayWayCell *cell = [tableView dequeueReusableCellWithIdentifier:payWayCell];
        [cell createUIWithIsTitle:isTitle];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 1)
        {
            isWeChat = YES;
        }
        else if (indexPath.row == 2)
        {
            isWeChat = NO;
        }
        
        [cell setCellDataWithPayWay:isWeChat];
        return cell;
    }
    else
    {
        ZSTECCOrderProductCell *cell = [tableView dequeueReusableCellWithIdentifier:orderProductCell];
        [cell createUI];
        [cell setCellDataWithFoodName:@"" FoodCount:@"" FoodPrice:@""];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2 && indexPath.row == 1)
    {
        if (self.currentIndexPath.row != 1)
        {
            if (self.currentIndexPath.row >= 0)
            {
                ((ZSTECCPayWayCell *)[tableView cellForRowAtIndexPath:self.currentIndexPath]).checkIV.hidden = YES;
            }
            ((ZSTECCPayWayCell *)[tableView cellForRowAtIndexPath:indexPath]).checkIV.hidden = NO;
            self.currentIndexPath = indexPath;
            self.payWay = @"wechat";
        }
        
    }
    else if (indexPath.section == 2 && indexPath.row == 2)
    {
        if (self.currentIndexPath.row != 2)
        {
            if (self.currentIndexPath.row >= 0)
            {
                ((ZSTECCPayWayCell *)[tableView cellForRowAtIndexPath:self.currentIndexPath]).checkIV.hidden = YES;
            }
            ((ZSTECCPayWayCell *)[tableView cellForRowAtIndexPath:indexPath]).checkIV.hidden = NO;
            self.currentIndexPath = indexPath;
            self.payWay = @"alipay";
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return 80;
    }
    else if (indexPath.section == 1)
    {
        return 40;
    }
    else if (indexPath.section == 2)
    {
        return 40;
    }
    else
    {
        return 50;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] init];
    sectionHeaderView.backgroundColor = RGBA(244, 244, 244, 1);
    return sectionHeaderView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.orderSureTableView)
    {
        CGFloat sectionHeaderHeight = 10;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

- (void)buildBottomView
{
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 60, WIDTH, 60)];
    bottomView.backgroundColor = [UIColor whiteColor];
    CALayer *line = [CALayer layer];
    line.frame = CGRectMake(0, 0, WIDTH, 1);
    line.backgroundColor = RGBA(244, 244, 244, 1).CGColor;
    [bottomView.layer addSublayer:line];
    [self.view addSubview:bottomView];
    
    self.priceLB = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, WIDTH / 2, 40)];
    self.priceLB.textColor = RGBA(250, 52, 46, 1);
    self.priceLB.textAlignment = NSTextAlignmentLeft;
    self.priceLB.font = [UIFont systemFontOfSize:16.0f];
    NSString *priceString = @"￥110";
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:priceString];
    [nodeString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0f]} range:NSMakeRange(0, 1)];
    self.priceLB.attributedText = nodeString;
    [bottomView addSubview:self.priceLB];
    
    UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.frame = CGRectMake(WIDTH / 2 + 20, 10, WIDTH / 2 - 40, 40);
    submitBtn.backgroundColor = RGBA(250, 52, 46, 1);
    submitBtn.layer.cornerRadius = 4.0f;
    submitBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [submitBtn setTitle:@"提交订单" forState:UIControlStateNormal];
    [bottomView addSubview:submitBtn];
}

@end
