//
//  ZSTECCFoodInfoViewController.m
//  EComC
//
//  Created by qiuguian on 15/8/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCFoodInfoViewController.h"
#import "ZSTECCLeftMenuCell.h"
#import "ZSTEComCDetailDelegate.h"
#import "ZSTECCFoodDetailCell.h"
#import "ZSTECCSureOrderViewController.h"
#import "ZSTECCShoppingCarDelegate.h"
#import "ZSTECCShoppingCarCell.h"
#import <PMCallButton.h>

@interface ZSTECCFoodInfoViewController ()

@property (strong,nonatomic) ZSTEComCDetailDelegate *rightFoodDelegate;
@property (strong,nonatomic) PMRepairButton *shopCarBtn;
@property (strong,nonatomic) UILabel *priceLB;
@property (strong,nonatomic) UIView *clearView;
@property (assign,nonatomic) BOOL isCarHidden;
@property (strong,nonatomic) UIView *shoppingCarView;
@property (strong,nonatomic) ZSTECCShoppingCarDelegate *shoppingCarDelegate;
@property (strong,nonatomic) UITableView *shoppingCarTableView;
@property (strong,nonatomic) UILabel *shoppingCarViewPriceLB;
@property (strong,nonatomic) NSArray *dishesTypeArr;
@property (strong,nonatomic) NSArray *dishesDetailArr;

@end

@implementation ZSTECCFoodInfoViewController

static NSString *const foodDetailCell = @"foodDetailCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.rightFoodDelegate = [[ZSTEComCDetailDelegate alloc] init];
    self.shoppingCarDelegate = [[ZSTECCShoppingCarDelegate alloc] init];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"点餐信息", nil)];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    [self createTableView];
    [self buildBottomToolView];
    [self buildDarkView];
    [self buildShoppingCarView];
    
    [self getShopDishes];

}

#pragma mark - 初始化左侧菜单与右侧食品 视图
-(void)createTableView{
   
    self.leftMenuTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH * 0.3, HEIGHT-64-60)];
    self.leftMenuTableView.backgroundColor = RGBCOLOR(246, 246, 246);
    self.leftMenuTableView.delegate = self;
    self.leftMenuTableView.dataSource = self;
    self.leftMenuTableView.separatorStyle = UITableViewCellAccessoryNone;
    [self.view addSubview:self.leftMenuTableView];
    
    self.rightFoodTableView = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH * 0.3, 0, WIDTH * 0.7, HEIGHT-64-60)];
//    self.rightFoodTableView.delegate = self;
//    self.rightFoodTableView.dataSource = self;
    self.rightFoodTableView.delegate = self.rightFoodDelegate;
    self.rightFoodTableView.dataSource = self.rightFoodDelegate;
    [self.rightFoodTableView registerClass:[ZSTECCFoodDetailCell class] forCellReuseIdentifier:foodDetailCell];
    [self.view addSubview:self.rightFoodTableView];

    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = [UIColor whiteColor];
    self.rightFoodTableView.tableFooterView = footerView;
}


#pragma mark - 数据源
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dishesTypeArr.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *leftMenuCell = @"leftMenuCell";
    ZSTECCLeftMenuCell *cell = (ZSTECCLeftMenuCell *)[tableView dequeueReusableCellWithIdentifier:leftMenuCell];
    if (!cell) {
        cell = [[ZSTECCLeftMenuCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:leftMenuCell];
    }
    [cell createUI];
    NSDictionary *oneDishes = self.dishesTypeArr[indexPath.row];
    NSString *typeName = [[oneDishes allKeys] firstObject];
    [cell setCellDataWithTypeName:typeName];
    
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.rightFoodDelegate.detailTableView = self.rightFoodTableView;
    NSDictionary *thisTypeDic = self.dishesTypeArr[indexPath.row];
    NSString *key = [[thisTypeDic allKeys] firstObject];
    NSArray *detailArr = [thisTypeDic objectForKey:key];
    self.rightFoodDelegate.detailFoodArr = detailArr;
    [self.rightFoodTableView reloadData];
    
//    self.rightFoodDelegate.detailFoodArr = 
}

- (void)buildBottomToolView
{
    UIView *toolView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 60 - 64, WIDTH, 60)];
    toolView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:toolView];
    
    CALayer *topLine = [CALayer layer];
    topLine.backgroundColor = [UIColor lightGrayColor].CGColor;
    topLine.frame = CGRectMake(0, 0, WIDTH, 1);
    [toolView.layer addSublayer:topLine];
    
    self.shopCarBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(15, -10, 56, 56)];
    self.shopCarBtn.backgroundColor = RGBA(255, 199, 0, 1);
    self.shopCarBtn.layer.cornerRadius = 28.0f;
//    [self.shopCarBtn setImage:[UIImage imageNamed:@"ecomCShopCar"] forState:UIControlStateNormal];
    [self.shopCarBtn addTarget:self action:@selector(showShopCar:) forControlEvents:UIControlEventTouchUpInside];
    [toolView addSubview:self.shopCarBtn];
    
    UIImageView *carIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ecomCShopCar.png"]];
    carIV.frame = CGRectMake(10, 15, 36, 36);
    carIV.contentMode = UIViewContentModeScaleAspectFit;
    [self.shopCarBtn addSubview:carIV];
    
    self.priceLB = [[UILabel alloc] initWithFrame:CGRectMake(80, 10, WIDTH - 80 - 15 - 100, 30)];
    self.priceLB.textAlignment = NSTextAlignmentLeft;
    self.priceLB.font = [UIFont systemFontOfSize:16.0f];
    self.priceLB.textColor = RGBA(254, 78, 60, 1);
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:@"￥30.00"];
    [nodeString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0f]} range:NSMakeRange(0, 1)];
    self.priceLB.attributedText = nodeString;
    [toolView addSubview:self.priceLB];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(WIDTH - 15 - 100, 10, 100, 30);
    sureBtn.backgroundColor = RGBA(254, 78, 60, 1);
    sureBtn.layer.cornerRadius = 4.0f;
    [sureBtn addTarget:self action:@selector(sureOrder:) forControlEvents:UIControlEventTouchUpInside];
    [sureBtn setTitle:@"选好了" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [toolView addSubview:sureBtn];
    
}

- (void)buildShoppingCarView
{
    self.shoppingCarView = [[UIView alloc] initWithFrame:CGRectMake(0, (HEIGHT - 64) * 0.4 + HEIGHT, WIDTH, (HEIGHT - 64) * 0.6)];
    self.shoppingCarView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.shoppingCarView];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(244, 244, 244, 1).CGColor;
    line.frame = CGRectMake(0, 38, WIDTH, 1);
    [self.shoppingCarView.layer addSublayer:line];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(15, -18, 56, 56);
    button.layer.cornerRadius = 28.0f;
    button.backgroundColor = RGBA(255, 199, 0, 1);
    [button addTarget:self action:@selector(dismissShoppingCarView:) forControlEvents:UIControlEventTouchUpInside];
    [self.shoppingCarView addSubview:button];
    
    UIImageView *carIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ecomCShopCar.png"]];
    carIV.frame = CGRectMake(10, 15, 36, 36);
    carIV.contentMode = UIViewContentModeScaleAspectFit;
    [button addSubview:carIV];
    
    PMCallButton *deleteBtn = [[PMCallButton alloc] initWithFrame:CGRectMake(WIDTH - 15 - 120, 0, 120, 38)];
    [deleteBtn setImage:[UIImage imageNamed:@"ZSTEECTrash.png"] forState:UIControlStateNormal];
    [deleteBtn setTitle:@"清空购物车" forState:UIControlStateNormal];
    [deleteBtn setTitleColor:RGBA(153, 153, 153, 1) forState:UIControlStateNormal];
    deleteBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.shoppingCarView addSubview:deleteBtn];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissShoppingCarViewByGesture:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self.shoppingCarView addGestureRecognizer:swipe];
    
    self.shoppingCarTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 39, WIDTH, (HEIGHT - 64) * 0.6 - 39 - 60) style:UITableViewStylePlain];
    self.shoppingCarTableView.delegate = self.shoppingCarDelegate;
    self.shoppingCarTableView.dataSource = self.shoppingCarDelegate;
    self.shoppingCarTableView.backgroundColor = [UIColor whiteColor];
    [self.shoppingCarTableView registerClass:[ZSTECCShoppingCarCell class] forCellReuseIdentifier:@"shoppingCarCell"];
    [self.shoppingCarView addSubview:self.shoppingCarTableView];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, self.shoppingCarView.bounds.size.height - 60, WIDTH, 60)];
    view.backgroundColor = [UIColor whiteColor];
    CALayer *bottomLine = [CALayer layer];
    bottomLine.backgroundColor = RGBA(244, 244, 244, 1).CGColor;
    bottomLine.frame = CGRectMake(0, 0, WIDTH, 1);
    [view.layer addSublayer:bottomLine];
    [self.shoppingCarView addSubview:view];
    
    self.shoppingCarViewPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, WIDTH / 2, 40)];
    self.shoppingCarViewPriceLB.font = [UIFont systemFontOfSize:16.0f];
    self.shoppingCarViewPriceLB.textColor = RGBA(254, 78, 60, 1);
    self.shoppingCarViewPriceLB.text = @"￥150";
    [view addSubview:self.shoppingCarViewPriceLB];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(WIDTH - 15 - 100, 15, 100, 30);
    sureBtn.backgroundColor = RGBA(254, 78, 60, 1);
    sureBtn.layer.cornerRadius = 4.0f;
    [sureBtn addTarget:self action:@selector(okCar:) forControlEvents:UIControlEventTouchUpInside];
    [sureBtn setTitle:@"选好了" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [view addSubview:sureBtn];
    
}

- (void)buildDarkView
{
    self.clearView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.clearView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.clearView];
    
    UIView *darkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.clearView.bounds.size.width, self.clearView.bounds.size.height)];
    darkView.backgroundColor = [UIColor blackColor];
    darkView.alpha = 0.8;
    [self.clearView addSubview:darkView];
    
    UITapGestureRecognizer *tap  = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissCarView:)];
    [darkView addGestureRecognizer:tap];
    
    self.clearView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)sureOrder:(UIButton *)sender
{
    ZSTECCSureOrderViewController *orderVC = [[ZSTECCSureOrderViewController alloc] init];
    [self.navigationController pushViewController:orderVC animated:YES];
}

- (void)showShopCar:(UIButton *)sender
{
    self.clearView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shoppingCarView.frame = CGRectMake(0, (HEIGHT - 64) * 0.4, WIDTH, (HEIGHT - 64) * 0.6);
    }];
}

- (void)dismissShoppingCarView:(UIButton *)sender
{
    self.clearView.hidden = YES;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shoppingCarView.frame = CGRectMake(0, (HEIGHT - 64) * 0.4 + HEIGHT, WIDTH, (HEIGHT - 64) * 0.6);
    }];
}

- (void)dismissShoppingCarViewByGesture:(UISwipeGestureRecognizer *)swipe
{
    self.clearView.hidden = YES;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shoppingCarView.frame = CGRectMake(0, (HEIGHT - 64) * 0.4 + HEIGHT, WIDTH, (HEIGHT - 64) * 0.6);
    }];
}

- (void)okCar:(UIButton *)sender
{
    
}

- (void)dismissCarView:(UITapGestureRecognizer *)tap
{
    self.clearView.hidden = YES;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shoppingCarView.frame = CGRectMake(0, (HEIGHT - 64) * 0.4 + HEIGHT, WIDTH, (HEIGHT - 64) * 0.6);
    }];
}

- (void)getShopDishes
{
    [self.engine getShopDishesByShopId:self.shopId AndUserId:[ZSTF3Preferences shared].UserId];
}

- (void)getDishesInfoSuccessed:(NSDictionary *)response
{
    self.dishesTypeArr = [response safeObjectForKey:@"dataList"];
    [self.leftMenuTableView reloadData];
    if (self.dishesTypeArr.count == 0)
    {
        
    }
    else
    {
        NSIndexPath *ip=[NSIndexPath indexPathForRow:0 inSection:0];
        [self.leftMenuTableView selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionBottom];
        [self tableView:self.leftMenuTableView didSelectRowAtIndexPath:ip];
    }
    
}

- (void)dealloc
{
    [self.engine cancelAllRequest];
    self.engine.delegate = nil;
}

@end
