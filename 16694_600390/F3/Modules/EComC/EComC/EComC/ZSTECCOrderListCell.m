//
//  ZSTECCOrderListCell.m
//  EComC
//
//  Created by qiuguian on 8/10/15.
//  Copyright (c) 2015 pmit. All rights reserved.
//

#import "ZSTECCOrderListCell.h"

@implementation ZSTECCOrderListCell{

    UIView *codeView;
    UIView *shopView;
    UIView *payView;
    
    UILabel *payStatusLab;
}

#pragma mark - 创建cell视图
-(void)createUI{
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    if (!codeView)
    {
        codeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 35)];
        codeView.backgroundColor = RGBCOLOR(206, 206, 206);
        [self.contentView addSubview:codeView];
        
        UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 35)];
        bg.backgroundColor = [UIColor whiteColor];
        [codeView addSubview:bg];
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 1)];
        line.backgroundColor = [UIColor lightGrayColor];
        //[bg addSubview:line];
        
        UILabel *orderLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 40, 30)];
        orderLab.text = @"订单号:";
        orderLab.font = [UIFont systemFontOfSize:12];
        orderLab.textColor = [UIColor grayColor];
        orderLab.textAlignment = UITextAlignmentLeft;
        [bg addSubview:orderLab];
        
        self.orderCode = [[UILabel alloc] initWithFrame:CGRectMake(55, 5,120, 30)];
        self.orderCode.font = [UIFont systemFontOfSize:12];
        self.orderCode.textColor = [UIColor grayColor];
        self.orderCode.textAlignment = UITextAlignmentLeft;
        [bg addSubview:self.orderCode];
        
        self.time = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH-120-10, 5, 120, 30)];
        self.time.font = [UIFont systemFontOfSize:12];
        self.time.textColor = [UIColor grayColor];
        self.time.textAlignment = UITextAlignmentRight;
        [bg addSubview:self.time];
        
        line = [[UILabel alloc] initWithFrame:CGRectMake(10, 34, 300, 1)];
        line.backgroundColor = [UIColor lightGrayColor];
        line.alpha = 0.5;
        [bg addSubview:line];
        
        
        //
        shopView = [[UIView alloc] initWithFrame:CGRectMake(0, 35, 320, 60)];
        shopView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:shopView];
        
        self.shopName = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 150, 50)];
        self.shopName.font = [UIFont systemFontOfSize:16];
        self.shopName.textColor = [UIColor grayColor];
        self.shopName.textAlignment = UITextAlignmentLeft;
        [shopView addSubview:self.shopName];
        
        self.bookType = [[UILabel alloc] initWithFrame:CGRectMake(10+150, 5, 80, 50)];
        self.bookType.font = [UIFont systemFontOfSize:16];
        self.bookType.textColor = [UIColor grayColor];
        self.bookType.textAlignment = UITextAlignmentLeft;
        [shopView addSubview:self.bookType];
        
        self.totalMoney = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH-70-10, 5, 70, 50)];
        self.totalMoney.font = [UIFont systemFontOfSize:16];
        self.totalMoney.textColor = [UIColor grayColor];
        self.totalMoney.textAlignment = UITextAlignmentRight;
        [shopView addSubview:self.totalMoney];
        
        line = [[UILabel alloc] initWithFrame:CGRectMake(10, 59, 300, 1)];
        line.backgroundColor = [UIColor lightGrayColor];
        line.alpha = 0.5;
        [shopView addSubview:line];
        
        UIControl * control = [[UIControl alloc]initWithFrame:shopView.bounds];
        [control addTarget:self action:@selector(orderInfoAction) forControlEvents:UIControlEventTouchUpInside];
        [shopView addSubview:control];
        
        //
        payView  = [[UIView alloc] initWithFrame:CGRectMake(0, 35+60, 320, 50)];
        payView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:payView];
        
        payStatusLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 50, 30)];
        payStatusLab.font = [UIFont systemFontOfSize:15];
        payStatusLab.textColor = [UIColor grayColor];
        payStatusLab.textAlignment = UITextAlignmentLeft;
        [payView addSubview:payStatusLab];
        
        self.cancellBtn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 160, 10, 70, 25)];
        [self.cancellBtn setTitle:@"取消" forState:UIControlStateNormal];
        [self.cancellBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.cancellBtn.layer.cornerRadius =4;
        self.cancellBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.cancellBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        self.cancellBtn.layer.borderWidth =1;
        [payView addSubview: self.cancellBtn];
        
        self.payBtn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 10 - 70, 10, 70, 25)];
        [self.payBtn setTitle:@"去付款" forState:UIControlStateNormal];
        [self.payBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        self.payBtn.layer.cornerRadius = 4;
        self.payBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        self.payBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.payBtn.layer.borderWidth =1;
        [payView addSubview:self.payBtn];
        
        self.reviewBtn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 10 - 70, 10, 70, 25)];
        [self.reviewBtn setTitle:@"去评价" forState:UIControlStateNormal];
        [self.reviewBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.reviewBtn  setHidden:YES];
        [self.reviewBtn addTarget:self action:@selector(goToEvaluate) forControlEvents:UIControlEventTouchUpInside];
        self.reviewBtn.layer.cornerRadius = 4;
        self.reviewBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        self.reviewBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.reviewBtn.layer.borderWidth =1;
        [payView addSubview:self.reviewBtn];
        
        line = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, 320, 1)];
        line.backgroundColor = [UIColor lightGrayColor];
        //[payView addSubview:line];
        line.alpha = 0.5;
    }

}


#pragma mark - 设置数据
-(void)setCellData:(NSMutableDictionary *)dic{
    
    self.orderCode.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"orderCode"]];
    self.time.text =  [NSString stringWithFormat:@"%@",[dic objectForKey:@"createdOn"]];
    self.shopName.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"shopName"]];
    self.bookType.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"bespeakTypeName"]];
    self.totalMoney.text = [NSString stringWithFormat:@"￥%@",[dic objectForKey:@"orderPrice"]];
    self.isPay = [[dic objectForKey:@"isPay"] boolValue];
    payStatusLab.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"payStatusName"]];
    
    //待消费
    if ([[dic objectForKey:@"orderStatus"] isEqualToString:@"1"]) {
        
        self.reviewBtn.hidden = YES;
        
        if ([[dic objectForKey:@"payStatus"] isEqualToString:@"1"]) {
            self.payBtn.hidden = YES;
            self.cancellBtn.frame = CGRectMake(10+130+80, 10, 90, 30);
            self.cancellBtn.hidden = NO;
        }else{
            self.payBtn.hidden = NO;
            self.cancellBtn.frame = CGRectMake(10+100, 10, 90, 30);
            self.cancellBtn.hidden = NO;
        }
    }
    
    //已消费
    if ([[dic objectForKey:@"orderStatus"] isEqualToString:@"2"]) {
        self.payBtn.hidden = YES;
        self.cancellBtn.hidden = YES;
        self.reviewBtn.hidden = NO;
        
        [self.reviewBtn setTitle:[dic objectForKey:@"isevaluateName"] forState:UIControlStateNormal];
        if ([[dic objectForKey:@"isevaluate"] isEqualToString:@"1"]) {
            self.reviewBtn.enabled = NO;
        }else{
            self.reviewBtn.enabled =YES;
        }
    }

}


-(void)orderInfoAction{
    
    NSLog(@"－－－－－－ 点击了 －－－－－－ ");
    [_delagate orderInfoAction:@"id"];
}



-(void)goToEvaluate{
    
    
    
    [_delagate goToEvaluate:@"id"];
}


@end
