//
//  ZSTECCOrderInfoViewController.h
//  EComC
//
//  Created by qiuguian on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCOrderInfoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) UITableView *tableView;

@end
