//
//  ZSTECCNoDataCell.m
//  EComC
//
//  Created by anqiu on 15/8/19.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCNoDataCell.h"

@implementation ZSTECCNoDataCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        UILabel *tip = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 320, 30)];
        tip.textAlignment = NSTextAlignmentCenter;
        tip.textColor = [UIColor lightGrayColor];
        tip.text = @"暂无记录";
        [self.contentView addSubview:tip];
    }
    return self;
}


@end
