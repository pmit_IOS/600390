//
//  ZSTECCShopCell.m
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShopCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTECCShopCell

- (void)createUI
{
    if (!self.shopLogoIV)
    {
        self.shopLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 70, 60)];
        self.shopLogoIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.shopLogoIV];
        
        self.shopNameLB = [[UILabel alloc] initWithFrame:CGRectMake(90, 10, WIDTH - 80 - 15, 20)];
        self.shopNameLB.textAlignment = NSTextAlignmentLeft;
        self.shopNameLB.font = [UIFont systemFontOfSize:14.0f];
        self.shopNameLB.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.shopNameLB];
        
        UIImageView *locationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(90, 38, 10, 15)];
        [locationIcon setImage:[UIImage imageNamed:@"model_ecomc_location_icon.png"]];
        locationIcon.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:locationIcon];
        
        self.startTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(90+15, 35, WIDTH - 90 - 100, 35)];
        self.startTitleLB.textAlignment = NSTextAlignmentLeft;
        self.startTitleLB.textColor = [UIColor lightGrayColor];
        self.startTitleLB.numberOfLines = 2;
        self.startTitleLB.font = [UIFont systemFontOfSize:11.5f];
        [self.contentView addSubview:self.startTitleLB];
        
//        self.lineLB = [[UILabel alloc] initWithFrame:CGRectMake(90, 45, 1, 20)];
//        self.lineLB.backgroundColor = [UIColor lightGrayColor];
//        [self.contentView addSubview:self.lineLB];
        
        self.distanceLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - 70, 35, 60, 20)];
        self.distanceLB.textAlignment = NSTextAlignmentRight;
        self.distanceLB.textColor = [UIColor lightGrayColor];
        self.distanceLB.font = [UIFont systemFontOfSize:14.0f];
        [self.contentView addSubview:self.distanceLB];
    }
}

- (void)setCellData:(NSDictionary *)dic
{
    [self.shopLogoIV setImageWithURL:[NSURL URLWithString:[dic safeObjectForKey:@"shopLogo"]]];
    self.shopNameLB.text = [dic safeObjectForKey:@"shopName"];
    
    NSString *addressString = [dic safeObjectForKey:@"shopAddress"];
    self.startTitleLB.text = addressString;
    
    
    NSString *distanceString = [NSString stringWithFormat:@"%.lfkm",[[dic safeObjectForKey:@"distance"] doubleValue] / 1000];
    self.distanceLB.text = distanceString;
}

@end
