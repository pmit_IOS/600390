//
//  ZSTECCOrderListViewController.m
//  EComC
//
//  Created by qiuguian on 8/10/15.
//  Copyright (c) 2015 pmit. All rights reserved.
//

#import "ZSTECCOrderListViewController.h"
#import "ZSTECCOrderListCell.h"
#import "ZSTECCOrderInfoViewController.h"
#import "ZSTECCEvaluateViewController.h"
#import "ZSTECCNoDataCell.h"


@interface ZSTECCOrderListViewController ()

@end

static NSString *const shopCell = @"orderListCell";

@implementation ZSTECCOrderListViewController{

    UIView *tabView;
    
    UIButton *isPayBtn;
    UIButton *notPayBtn;
    
    ZSTECCRoundRectView * isPayLine;
    ZSTECCRoundRectView * notPayLine;
    
    NSArray *initDataArry;
    
    NSMutableDictionary *orderDic;
   
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ZSTF3Engine *engine = [[ZSTF3Engine alloc] init];
    engine.delegate = self;
    self.engine = engine;
    
    //请求订单
    [self.engine getEcomCOrderListByUserId:[ZSTF3Preferences shared].UserId andStatus:@"1"];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView= [ZSTUtils titleViewWithTitle:NSLocalizedString(@"订单列表", nil)];
    self.view.backgroundColor =RGBCOLOR(206, 206, 206);
    //切换视图按钮
    tabView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH,50)];
    tabView.backgroundColor = [UIColor whiteColor];
    [self initTabBtnView];
    [self.view addSubview:tabView];
    
    [self initDatas];
    [self createTableView];
    
}


-(void)initDatas{

//    initDataArry = @[@{@"orderCode":@"201516189300234",@"createdOn":@"2015-10-20",@"shopName":@"天上人间(东圃店)",@"bespeakTypeName":@"订座点餐",@"orderPrice":@"1200",@"isPay":@"0"}];
//    orderDic = [[NSMutableDictionary alloc] init];
//    orderDic = (NSMutableDictionary *)initDataArry[0];
}



#pragma mark - 初始化切换视图按钮
-(void)initTabBtnView{
    
    //待消费 按钮
    isPayBtn= [[UIButton alloc] initWithFrame:CGRectMake(0, 5, 160, 40-4-5)];
    isPayBtn.titleLabel.textAlignment = UITextAlignmentCenter;
    isPayBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [isPayBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    isPayBtn.tag = 1;
    [isPayBtn setTitle:@"待消费" forState:UIControlStateNormal];
    [isPayBtn addTarget:self action:@selector(notPayBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [tabView addSubview:isPayBtn];
    
    //底线
    isPayLine = [[ZSTECCRoundRectView alloc]initWithPoint:CGPointMake(0, 40-1) toPoint:CGPointMake(WIDTH/2, 30) borderWidth:1.0f borderColor:[UIColor redColor]];
    [tabView addSubview:isPayLine];
    
    //已消费 按钮
    notPayBtn = [[UIButton alloc] initWithFrame:CGRectMake(160, 5, 160, 40-4-5)];
    notPayBtn.titleLabel.textAlignment = UITextAlignmentCenter;
    notPayBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [notPayBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    notPayBtn.tag =2 ;
    [notPayBtn setTitle:@"已消费" forState:UIControlStateNormal];
    [notPayBtn addTarget:self action:@selector(isPayBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [tabView addSubview: notPayBtn];
    
    //底线
    notPayLine = [[ZSTECCRoundRectView alloc]initWithPoint:CGPointMake(160, 40-1) toPoint:CGPointMake(320, 30) borderWidth:1.0f borderColor:[UIColor redColor]];
    notPayLine.hidden = YES;
    [tabView addSubview:notPayLine];
    
}

#pragma mark - 待消费
-(void)notPayBtnAction{
    
    //待消费
    isPayLine.hidden = NO;
    [isPayBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    //已消费
    notPayLine.hidden = YES;
    [notPayBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.engine getEcomCOrderListByUserId:@"" andStatus:@"1"];
    [self.orderTableView reloadData];
}

#pragma mark - 已消费
-(void)isPayBtnAction{
    
    [self.engine getEcomCOrderListByUserId:@"" andStatus:@"2"];

    //待消费
    isPayLine.hidden = YES;
    [isPayBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    //已消费
    notPayLine.hidden = NO;
    [notPayBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];

   [self.orderTableView reloadData];
}




#pragma mark - 创建tableView
-(void)createTableView{

    self.orderTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, WIDTH, HEIGHT - 64 - 40)];
    self.orderTableView.delegate = self;
    self.orderTableView.dataSource = self;
    [self.orderTableView registerClass:[ZSTECCOrderListCell class] forCellReuseIdentifier:shopCell];
    self.orderTableView.separatorStyle = UITableViewCellAccessoryNone;
    self.orderTableView.backgroundColor = RGBA(244, 244, 244, 1);
    [self.view addSubview:self.orderTableView];
}


#pragma mark 数据源 tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (initDataArry.count == 0) {
        return 1;
    }
    
    return initDataArry.count;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
   return 1;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *shView = [[UIView alloc] init];
    shView.backgroundColor = RGBA(244, 244, 244, 244);
    return shView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (initDataArry.count == 0) {
        return 60.0f;
    }
    return 145.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //无数据
    if (initDataArry.count == 0) {
        static NSString *noDataCell = @"noDataCell";
        ZSTECCNoDataCell *cell = (ZSTECCNoDataCell *)[tableView dequeueReusableCellWithIdentifier:noDataCell];
        if (!cell) {
            cell = [[ZSTECCNoDataCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noDataCell];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    
    static NSString *orderListCell = @"orderListCell";
    ZSTECCOrderListCell *cell = (ZSTECCOrderListCell *)[tableView dequeueReusableCellWithIdentifier:orderListCell];
    if (!cell) {
        cell = [[ZSTECCOrderListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:orderListCell];
    }
    cell.delagate = self;
    [cell createUI];
    [cell setCellData:[initDataArry objectAtIndex:indexPath.section]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return  cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


-(void)orderInfoAction:(NSString*)orderId{
    
    ZSTECCOrderInfoViewController *orderInfo = [[ZSTECCOrderInfoViewController alloc] init];
    [self.navigationController pushViewController:orderInfo animated:YES];
    
}

-(void)goToEvaluate:(NSString *)orderId{
    
    ZSTECCEvaluateViewController *evaluateView = [[ZSTECCEvaluateViewController alloc] init];
    [self.navigationController pushViewController:evaluateView animated:YES];
    
}


#pragma mark - 返回个人订单列表信息
-(void)getEcomCOrderListDidSucceed:(NSDictionary *)orderList{
    
    initDataArry = [orderList objectForKey:@"dataList"];
     NSLog(@"======返回个人订单列表信息===%@",initDataArry);

    [_orderTableView reloadData];
}

-(void)getEcomCOrderListDidFailed:(int)resultCode{

   NSLog(@"======返回个人订单列表信息 失败===");

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.orderTableView)
    {
        CGFloat sectionHeaderHeight = 10;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

@end
