//
//  ZSTECCShoppingCarDelegate.h
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ZSTECCShoppingCarDelegate : NSObject <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) UITableView *shoppingCarTableView;

@end
