//
//  ZSTEComCDetailDelegate.m
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTEComCDetailDelegate.h"
#import "ZSTECCFoodDetailCell.h"

@implementation ZSTEComCDetailDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.detailFoodArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTECCFoodDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"foodDetailCell"];
    [cell createUI];
    [cell getCellData:self.detailFoodArr[indexPath.row]];
    cell.minusBtn.tag = indexPath.row;
    cell.plusBtn.tag = indexPath.row + 1000;
    cell.isHasBook = NO;
    [cell.minusBtn addTarget:self action:@selector(changeBookValue:) forControlEvents:UIControlEventTouchUpInside];
    [cell.plusBtn addTarget:self action:@selector(changeBookValue:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}

- (void)changeBookValue:(PMRepairButton *)sender
{
    //大于1000是增加数量
    if (sender.tag >= 1000)
    {
        NSInteger cellIndex = sender.tag - 1000;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellIndex inSection:0];
        ZSTECCFoodDetailCell *cell = (ZSTECCFoodDetailCell *)[self.detailTableView cellForRowAtIndexPath:indexPath];
        if (!cell.isHasBook)
        {
            cell.isHasBook = YES;
            cell.minusBtn.hidden = NO;
            cell.numLB.hidden = NO;
            cell.numLB.text = @"1";
        }
        else
        {
            NSInteger numLBCount = [cell.numLB.text integerValue];
            numLBCount++;
            cell.numLB.text = [NSString stringWithFormat:@"%@",@(numLBCount)];
        }
    }
    else
    {
        NSInteger cellIndex = sender.tag;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellIndex inSection:0];
        ZSTECCFoodDetailCell *cell = (ZSTECCFoodDetailCell *)[self.detailTableView cellForRowAtIndexPath:indexPath];
        if ([cell.numLB.text isEqualToString:@"1"])
        {
            cell.isHasBook = NO;
            cell.minusBtn.hidden = YES;
            cell.numLB.hidden = YES;
        }
        else
        {
            NSInteger numLBCount = [cell.numLB.text integerValue];
            numLBCount--;
            cell.numLB.text = [NSString stringWithFormat:@"%@",@(numLBCount)];
        }
    }
}

@end
