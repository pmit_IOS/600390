//
//  ZSTECCImageViewer.m
//  EComC
//
//  Created by anqiu on 15/8/19.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCImageViewer.h"

@interface ZSTECCImageViewer ()

@end

@implementation ZSTECCImageViewer

@synthesize sv;

@synthesize iv;

@synthesize loadingView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    self.sv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    
    [self.view addSubview:self.sv];
    [self.view addSubview:self.iv];
    [self.view addSubview:loadingView];
    
    
    self.sv.delegate = self;
    
    
    [self loadImage:@"http://b218.photo.store.qq.com/psb?/V14b2MUB2EAmtl/kbZCrL7A9u.GV4SWP.1EHsReqMEspo1uv5aKcDn*.8c!/b/Yas484HnBAAAYjY684GiBAAA&bo=fAIgA3s!"];
    

}

// 加载图片
- (void)loadImage:(NSString *)imageURL
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageURL]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if([data length] > 0 && error==nil){
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSRange range = [result rangeOfString:@"404 Not Found"];
            if(range.location != 0){
                [self showLoadFailedAlert];
                [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationCurveLinear animations:^  {
                    
                    self.loadingView.alpha = 0;
                } completion:^(BOOL finished){
                    [self.loadingView setHidden:YES];
                }];
                return;
            }
            
            UIImage *image = [UIImage imageWithData:data];
            // 重置UIImageView的Frame，让图片居中显示
            
            CGFloat origin_x = abs(sv.frame.size.width - image.size.width)/2.0;
            CGFloat origin_y = abs(sv.frame.size.height - image.size.height)/2.0;
            self.iv.frame = CGRectMake(origin_x, origin_y, sv.frame.size.width, sv.frame.size.width*image.size.height/image.size.width);
            [self.iv setImage:image];
            
            
            CGSize maxSize = sv.frame.size;
            CGFloat widthRatio = maxSize.width/image.size.width;
            CGFloat heightRatio = maxSize.height/image.size.height;
            CGFloat initialZoom = (widthRatio > heightRatio) ? heightRatio : widthRatio;
            /*
             
             ** 设置UIScrollView的最大和最小放大级别（注意如果MinimumZoomScale == MaximumZoomScale，
             
             ** 那么UIScrllView就缩放不了了
             
             */
            [sv setMinimumZoomScale:initialZoom];
            [sv setMaximumZoomScale:5];
            // 设置UIScrollView初始化缩放级别
            
            [sv setZoomScale:initialZoom];
            
            // 动态隐藏加载界面，从而显示图片
            [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationCurveLinear animations:^{
                
                self.loadingView.alpha = 0;
            } completion:^(BOOL finished){
                [self.loadingView setHidden:YES];
            }];
        }else{
            [self showLoadFailedAlert];
            
        }
    }];
}
// 设置UIScrollView中要缩放的视图

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.iv;
    
}
// 让UIImageView在UIScrollView缩放后居中显示
- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    iv.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                            scrollView.contentSize.height * 0.5 + offsetY);
}
// 显示加载失败的提示对话框
- (void)showLoadFailedAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                          
                                                    message:@"加载图片失败"
                          
                                                   delegate:nil
                          
                                          cancelButtonTitle:@"确定"
                          
                                          otherButtonTitles:nil];
    [alert show];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end