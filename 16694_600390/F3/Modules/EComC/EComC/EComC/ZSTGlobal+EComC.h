//
//  ZSTGlobal+EComC.h
//  EComC
//
//  Created by qiuguian on 15/8/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

#define EShopBaseURL @"http://mod.pmit.cn/EcomB/WuLiu.html"


#define FoodShopUrl @"http://192.168.1.3:8081"

#pragma mark - 获取主页轮播图
#define getBannerURL EShopBaseURL @"/GetBannerList.ashx?moduletype=%ld&ecid=%@"


#pragma mark - 获取个人订单列表信息
#define listOrderURL FoodShopUrl @"/mealorderclient/meal_orderclient!listOrder"

#pragma mark - 根据经纬度获取当前位置
#define getAddressInfoURL FoodShopUrl @"/mealshop/meal_shop!getAddressInfo"

#pragma mark - 获取店铺列表
#define getShopDCListUrl FoodShopUrl @"/mealshopclient/meal_shopclient!listShop"

#pragma mark - 初始化订餐数据
#define getInitOrderInfoUrl FoodShopUrl @"/mealorderclient/meal_orderclient!getInitOrderInfo"

#pragma mark - 菜单分类
#define getSelectedDishesUrl FoodShopUrl @"/mealdishesclient/meal_dishesclient!selectCateAndDish"

#pragma mark - 获取店铺信息
#define getShopDetailUrl FoodShopUrl @"/mealshopclient/meal_shopclient!detail"

#pragma mark - 获取店铺信息 用户评价
#define getShopReviewListUrl FoodShopUrl @""

