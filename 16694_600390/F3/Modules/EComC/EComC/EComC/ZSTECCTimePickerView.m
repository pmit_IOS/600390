//
//  ZSTECCTimePickerView.m
//  EComC
//
//  Created by pmit on 15/8/19.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCTimePickerView.h"

@interface ZSTECCTimePickerView ()


@end

@implementation ZSTECCTimePickerView

- (void)createUIWithFrame:(CGRect)rect
{
    self.backgroundColor = RGBA(236, 236, 236, 1);
    self.i = 0;
    self.frame = rect;
    
    UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, 40)];
    whiteView.backgroundColor = [UIColor whiteColor];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(10, 0, 60, 40);
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelBtn.tag = 100;
    [cancelBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(clickBtnCancel:) forControlEvents:UIControlEventTouchUpInside];
    [whiteView addSubview:cancelBtn];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(rect.size.width - 70, 0, 60, 40);
    [sureBtn setTitle:@"完成" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cancelBtn.tag = 101;
    [sureBtn addTarget:self action:@selector(clickBtnSure:) forControlEvents:UIControlEventTouchUpInside];
    [whiteView addSubview:sureBtn];
    
    [self addSubview:whiteView];
    
    UIPickerView *pick = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, rect.size.width, rect.size.height - 40)];
    pick.delegate = self;
    pick.dataSource = self;
    [pick selectRow:0 inComponent:0 animated:YES];
    [pick selectRow:0 inComponent:1 animated:YES];
    [pick selectRow:0 inComponent:2 animated:YES];
    [self addSubview:pick];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return 7;
    }
    else if (component == 1)
    {
        return 24;
    }
    else
    {
        return 60;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    BOOL isBigMonth = NO;
    BOOL isTwo = NO;
    BOOL isRun = NO;
    
    if ([self.nowYear integerValue] % 4 == 0 || ([self.nowYear integerValue] % 100 == 0 && [self.nowYear integerValue] % 400 == 0))
    {
        isRun = YES;
    }
    else
    {
        isRun = NO;
    }
    
    if ([self.nowMonth isEqualToString:@"1"] | [self.nowMonth isEqualToString:@"3"] | [self.nowMonth isEqualToString:@"5"] | [self.nowMonth isEqualToString:@"7"] | [self.nowMonth isEqualToString:@"8"] | [self.nowMonth isEqualToString:@"10"] | [self.nowMonth isEqualToString:@"12"])
        {
            isBigMonth = YES;
            isTwo = NO;
        }
    else if ([self.nowMonth isEqualToString:@"2"])
    {
        isBigMonth = NO;
        isTwo = YES;
    }
    else
    {
        isBigMonth = NO;
        isTwo = NO;
    }
    
    if (component == 0)
    {
        NSInteger showDay = [self.nowDay integerValue] + row;
        NSInteger showMonth = [self.nowMonth integerValue];
        if (isBigMonth)
        {
            if (showDay > 31)
            {
                showMonth += 1;
                if (showMonth > 12)
                {
                    showMonth = 1;
                }
                showDay = showDay - 31;
            }
        }
        else if (!isBigMonth && !isTwo)
        {
            if (showDay > 30)
            {
                showMonth += 1;
                showDay = showDay - 30;
            }
        }
        else if (isRun)
        {
            if (showDay > 29)
            {
                showMonth += 1;
                showDay = showDay - 29;
            }
        }
        else
        {
            if (showDay > 28)
            {
                showMonth += 1;
                showDay = showDay - 28;
            }
        }
        return [NSString stringWithFormat:@"%@月%@日",@(showMonth),@(showDay)];
    }
    else if (component == 1)
    {
        return [NSString stringWithFormat:@"%@",@(row)];
    }
    else 
    {
        return [NSString stringWithFormat:@"%@",@(row)];
    }
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0)
    {
        BOOL isBigMonth = NO;
        BOOL isTwo = NO;
        BOOL isRun = NO;
        
        if ([self.nowYear integerValue] % 4 == 0 || ([self.nowYear integerValue] % 100 == 0 && [self.nowYear integerValue] % 400 == 0))
        {
            isRun = YES;
        }
        else
        {
            isRun = NO;
        }
        
        if ([self.nowMonth isEqualToString:@"1"] | [self.nowMonth isEqualToString:@"3"] | [self.nowMonth isEqualToString:@"5"] | [self.nowMonth isEqualToString:@"7"] | [self.nowMonth isEqualToString:@"8"] | [self.nowMonth isEqualToString:@"10"] | [self.nowMonth isEqualToString:@"12"])
        {
            isBigMonth = YES;
            isTwo = NO;
        }
        else if ([self.nowMonth isEqualToString:@"2"])
        {
            isBigMonth = NO;
            isTwo = YES;
        }
        else
        {
            isBigMonth = NO;
            isTwo = NO;
        }
        

        NSInteger showDay = [self.nowDay integerValue] + row;
        NSInteger showMonth = [self.nowMonth integerValue];
        if (isBigMonth)
        {
            if (showDay > 31)
            {
                showMonth += 1;
                if (showMonth > 12)
                {
                    showMonth = 1;
                }
                showDay = showDay - 31;
            }
        }
        else if (!isBigMonth && !isTwo)
        {
            if (showDay > 30)
            {
                showMonth += 1;
                showDay = showDay - 30;
            }
        }
        else if (isRun)
        {
            if (showDay > 29)
            {
                showMonth += 1;
                showDay = showDay - 29;
            }
        }
        else
        {
            if (showDay > 28)
            {
                showMonth += 1;
                showDay = showDay - 28;
            }
        }
        
        if (showMonth < [self.nowMonth integerValue])
        {
            self.selectedYear = [self.nowYear integerValue] + 1;
            
        }
        else
        {
            self.selectedYear = [self.nowYear integerValue];
        }
        
        self.selectedMonth = showMonth;
        self.selectedDay = showDay;
        
    }
    else if (component == 1)
    {
        self.selectedHour = row;
    }
    else
    {
        self.selectedMinus = row;
    }
}

- (void)clickBtnSure:(UIButton *)sender
{
    if ([self.timeDelegate respondsToSelector:@selector(sureTimeSelected:)])
    {
        if (self.selectedYear == 0 || self.selectedMonth == 0 || self.selectedDay == 0)
        {
            self.selectedYear = [self.nowYear integerValue];
            self.selectedMonth = [self.nowMonth integerValue];
            self.selectedDay = [self.nowDay integerValue];
            self.selectedHour = [self.nowHour integerValue];
            self.selectedMinus = [self.nowMin integerValue];
        }
        
        [self.timeDelegate sureTimeSelected:[NSString stringWithFormat:@"%@-%@-%@ %@:%@",@(self.selectedYear),@(self.selectedMonth),@(self.selectedDay),self.selectedHour >= 10 ? @(self.selectedHour) : [NSString stringWithFormat:@"0%@",@(self.selectedHour)],self.selectedMinus >= 10 ? @(self.selectedMinus) : [NSString stringWithFormat:@"0%@",@(self.selectedMinus)]]];
    }
}

- (void)clickBtnCancel:(UIButton *)sender
{
    if ([self.timeDelegate respondsToSelector:@selector(cancelTimeSelected)])
    {
        [self.timeDelegate cancelTimeSelected];
    }
}

@end
