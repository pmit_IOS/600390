//
//  ZSTECCSearchViewController.m
//  EComC
//
//  Created by pmit on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCSearchViewController.h"
#import "ZSTECCSearchResultDelegate.h"

@interface ZSTECCSearchViewController () <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (strong,nonatomic) UITableView *searchHistoryTableView;
@property (strong,nonatomic) UITextField *searchTF;
@property (strong,nonatomic) NSMutableArray *historyArr;
@property (strong,nonatomic) UITableView *searchResultTableView;
@property (strong,nonatomic) ZSTECCSearchResultDelegate *searchDelegate;

@end

@implementation ZSTECCSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self buildSearchTextView];
    self.view.backgroundColor = RGBA(244, 244, 244, 1);
    self.searchDelegate = [[ZSTECCSearchResultDelegate alloc] init];
    [self buildSearchHistoryTableView];
    [self buildSearchResultTableView];
    self.searchResultTableView.hidden = YES;
    
}

- (void)viewWillAppear:(BOOL)animated
{
//    self.historyArr = [[[NSUserDefaults standardUserDefaults] objectForKey:@"history"] mutableCopy];
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"history"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"history"] isKindOfClass:[NSNull class]])
    {
        self.historyArr = [NSMutableArray array];
        self.searchHistoryTableView.hidden = YES;
    }
    else
    {
        self.historyArr = [[[NSUserDefaults standardUserDefaults] objectForKey:@"history"] mutableCopy];
        
        if (self.historyArr.count == 0)
        {
            self.searchHistoryTableView.hidden = YES;
        }
        else
        {
            self.searchHistoryTableView.hidden = NO;
            self.searchHistoryTableView.frame = CGRectMake(20, 20, WIDTH - 40, self.historyArr.count * 44);
        }
        
        [self.searchHistoryTableView reloadData];
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildSearchTextView
{
    self.searchTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(105), 7, WIDTH - WidthRate(250), 30)];
    [self.searchTF setBorderStyle:UITextBorderStyleRoundedRect];
    //搜索框背景色
    [self.searchTF setBackgroundColor:[UIColor whiteColor]];
    [self.searchTF.layer setCornerRadius:5.0];
    [self.searchTF.layer setBorderWidth:0];
    self.searchTF.placeholder = @"输入关键字搜索商品";
    [self.searchTF setValue:[UIFont boldSystemFontOfSize:11] forKeyPath:@"_placeholderLabel.font"];
    [self.searchTF setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, self.searchTF.bounds.size.width, self.searchTF.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    [self.searchTF setValue:RGBA(160,160,160,1) forKeyPath:@"_placeholderLabel.textColor"];
    
    self.searchTF.returnKeyType = UIReturnKeySearch;
    self.searchTF.textColor = [UIColor darkGrayColor];
    self.searchTF.font = [UIFont systemFontOfSize:12];
    self.searchTF.delegate = self;
    
    UIImage * image = [UIImage imageNamed:@"search.png"];
    
    UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    searchIcon.image = image;
    searchIcon.contentMode = UIViewContentModeScaleAspectFit;
    
    self.searchTF.leftView = searchIcon;
    self.searchTF.leftViewMode = UITextFieldViewModeAlways;
    self.navigationItem.titleView = self.searchTF;
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
}

- (void)buildSearchHistoryTableView
{
    self.searchHistoryTableView = [[UITableView alloc] initWithFrame:CGRectMake(20, 20, WIDTH - 40, self.historyArr.count * 44) style:UITableViewStylePlain];
    self.searchHistoryTableView.delegate = self;
    self.searchHistoryTableView.dataSource = self;
    self.searchHistoryTableView.backgroundColor = RGBA(244, 244, 244, 1);
    [self.view addSubview:self.searchHistoryTableView];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - 40, 44)];
    footerView.backgroundColor = [UIColor whiteColor];
    self.searchHistoryTableView.tableFooterView = footerView;
}

- (void)buildSearchResultTableView
{
    self.searchResultTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.searchResultTableView.delegate = self.searchDelegate;
    self.searchResultTableView.dataSource = self.searchDelegate;
    [self.view addSubview:self.searchResultTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.historyArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"historyCell"];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"historyCell"];
    }
        
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = self.historyArr[indexPath.row];
    return cell;
 
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.searchTF.text = self.historyArr[indexPath.row];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self isContainsEmoji:string])
    {
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField.text isEqualToString:@""])
    {
        [textField resignFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        
        NSString *searchString = textField.text;
        BOOL isHasSame = NO;
        for (NSString *historyString in self.historyArr)
        {
            if ([historyString isEqualToString:searchString])
            {
                isHasSame = YES;
                break;
            }
        }
        
        if (!isHasSame)
        {
            [self.historyArr addObject:searchString];
            [[NSUserDefaults standardUserDefaults] setValue:self.historyArr forKey:@"history"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    return YES;
}

- (BOOL)isContainsEmoji:(NSString *)string {
    __block BOOL isEomji = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     isEomji = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 isEomji = YES;
             }
         } else {
             if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
                 isEomji = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 isEomji = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 isEomji = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 isEomji = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
                 isEomji = YES;
             }
         }
     }];
    return isEomji;
}

@end
