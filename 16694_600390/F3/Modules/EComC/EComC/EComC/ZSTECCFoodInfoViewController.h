//
//  ZSTECCFoodInfoViewController.h
//  EComC
//
//  Created by qiuguian on 15/8/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine+EComC.h"

@interface ZSTECCFoodInfoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ZSTF3EngineECCDelegate>


@property (nonatomic, strong) UITableView *leftMenuTableView;
@property (nonatomic, strong) UITableView *rightFoodTableView;
@property (copy,nonatomic) NSString *shopId;
@property (strong,nonatomic) ZSTF3Engine *engine;

@end
