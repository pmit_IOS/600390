//
//  ZSTF3Engine+EComC.h
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTF3Engine.h"

@protocol ZSTF3EngineECCDelegate <ZSTF3EngineDelegate>

@optional

#pragma mark - ---------------protocol----------------
#pragma mark 获取主页轮播列表
-(void)getEcomCCarouselDidSucceed:(NSArray *)carouseles;
-(void)getEcomCCarouselDidFailed:(int)resultCode;

#pragma mark 返回个人订单列表信息
-(void)getEcomCOrderListDidSucceed:(NSDictionary *)orderList;
-(void)getEcomCOrderListDidFailed:(int)resultCode;

#pragma mark 根据经纬度获取当前位置
-(void)getShopAddressInfoDidSucceed:(NSDictionary *)addressInfo;
-(void)getShopAddressInfoDidFailed:(int)resultCode;

#pragma mark 获取店铺列表
-(void)getShopListDidSucceed:(NSDictionary *)addressInfo;
-(void)getShopListDidFailed:(int)resultCode;

- (void)getInitOrderInfoSuccessd:(NSDictionary *)response;
- (void)getInitOrderInfoFailed:(int)resultCode;

- (void)getDishesInfoSuccessed:(NSDictionary *)response;
- (void)getDishesInfoFailed:(int)resultCode;

#pragma mark - 获取店铺信息
-(void)getShopDetailDidSucceed:(NSDictionary *)shopDetail;
-(void)getShopDetailDidFailed:(int)resultCode;
@end




@interface ZSTF3Engine (EComC)

#pragma mark - ---------------method----------------
- (void)getEcomCCarousel;

#pragma mark 获取个人订单列表信息
-(void)getEcomCOrderListByUserId:(NSString *)userId andStatus:(NSString *)orderStatus;

#pragma mark 根据经纬度获取当前位置
-(void)getShopAddressInfoByUserId:(NSString *)userId andLat:(NSString *)lat andLng:(NSString *)lng;

#pragma mark 获取店铺列表
-(void)getShopListByUserId:(NSString *)userId andLat:(NSString *)lat andLng:(NSString *)lng andCurrentPage:(NSInteger)currentPage PageSize:(NSInteger)pageSize;

#pragma mark - 初始化订单
- (void)getInitShopInfoByShopId:(NSString *)shopId;

#pragma mark - 初始化菜单
- (void)getShopDishesByShopId:(NSString *)shopId AndUserId:(NSString *)userId;

#pragma mark - 获取店铺信息
-(void)getShopDetailByShopId:(NSString *)shopId;

#pragma mark - 获取店铺信息 用户评价
-(void)getShopReviewListByShopId:(NSString *)shopId;

@end