//
//  ZSTECCTimePickerView.h
//  EComC
//
//  Created by pmit on 15/8/19.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTECCTimePickerViewDelegate <NSObject>

- (void)sureTimeSelected:(NSString *)stringDate;
- (void)cancelTimeSelected;

@end


@interface ZSTECCTimePickerView : UIView <UIPickerViewDataSource,UIPickerViewDelegate>

@property (copy,nonatomic) NSString *nowYear;
@property (copy,nonatomic) NSString *nowMonth;
@property (copy,nonatomic) NSString *nowDay;
@property (copy,nonatomic) NSString *nowHour;
@property (copy,nonatomic) NSString *nowMin;
@property (copy,nonatomic) NSString *morningStartTime;
@property (copy,nonatomic) NSString *morningEndTime;
@property (copy,nonatomic) NSString *afterStartTime;
@property (copy,nonatomic) NSString *afterEndTime;
@property (assign,nonatomic) NSInteger i;
@property (assign,nonatomic) NSInteger showDay;
@property (assign,nonatomic) NSInteger showMonth;
@property (assign,nonatomic) NSInteger selectedYear;
@property (assign,nonatomic) NSInteger selectedMonth;
@property (assign,nonatomic) NSInteger selectedDay;
@property (assign,nonatomic) NSInteger selectedHour;
@property (assign,nonatomic) NSInteger selectedMinus;

@property (weak,nonatomic) id<ZSTECCTimePickerViewDelegate> timeDelegate;

- (void)createUIWithFrame:(CGRect)rect;

@end
