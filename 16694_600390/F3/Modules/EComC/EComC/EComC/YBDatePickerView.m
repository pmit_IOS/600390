//
//  YBDatePickerView.m
//  changeViewController
//
//  Created by EapinZhang on 15/4/3.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import "YBDatePickerView.h"

@interface YBDatePickerView()

@property (copy,nonatomic) NSString *selectedYear;
@property (copy,nonatomic) NSString *selectedMonth;
@property (copy,nonatomic) NSString *selectedDay;
@property (copy,nonatomic) NSString *selectedHour;
@property (copy,nonatomic) NSString *selectedMin;

@end

@implementation YBDatePickerView

- (void)createUIWithFrame:(CGRect)rect
{
    self.backgroundColor = RGBA(236, 236, 236, 1);
    
    self.frame = rect;
    
    UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, 40)];
    whiteView.backgroundColor = [UIColor whiteColor];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(10, 0, 60, 40);
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelBtn.tag = 100;
    [cancelBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(clickBtnCancel:) forControlEvents:UIControlEventTouchUpInside];
    [whiteView addSubview:cancelBtn];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(rect.size.width - 70, 0, 60, 40);
    [sureBtn setTitle:@"完成" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cancelBtn.tag = 101;
    [sureBtn addTarget:self action:@selector(clickBtnSure:) forControlEvents:UIControlEventTouchUpInside];
    [whiteView addSubview:sureBtn];
    
    [self addSubview:whiteView];
    
    UIPickerView *pick = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, rect.size.width, rect.size.height - 40)];
    pick.delegate = self;
    pick.dataSource = self;
    NSInteger year = [self.nowYear integerValue];
    NSInteger month = [self.nowMonth integerValue];
    NSInteger yearDiffence = year - 1970;
    NSInteger monthDiffence = month - 1;
    [pick selectRow:yearDiffence inComponent:0 animated:YES];
    [pick selectRow:monthDiffence inComponent:1 animated:YES];
    
    self.selectedYear = self.nowYear;
    self.selectedMonth = self.nowMonth;
    
    [self addSubview:pick];
}

- (void)clickBtnCancel:(UIButton *)btn
{
    [self.myDelegate cancelPickerView];
}

- (void)clickBtnSure:(UIButton *)btn
{
    [self.myDelegate surePickerView:self.selectedYear And:self.selectedMonth];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 5;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return 10000;
    }
    else if (component == 1)
    {
        return 12;
    }
    else if (component == 2)
    {
        return 31;
    }
    else if (component == 3)
    {
        return 24;
    }
    else
    {
        return 60;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0)
    {
        if ([[NSString stringWithFormat:@"%ld",(long)(1970 + row)] isEqualToString:self.nowYear])
        {
            [pickerView selectRow:row inComponent:component animated:YES];
        }
        return [NSString stringWithFormat:@"%ld",(long)(1970 + row)];
    }
    else if (component == 1)
    {
        if ([[NSString stringWithFormat:@"%ld",(long)(1970 + row)] isEqualToString:self.nowMonth])
        {
            [pickerView selectRow:row inComponent:component animated:YES];
        }
        return [NSString stringWithFormat:@"%ld",(long)(row + 1)];
    }
    else if (component == 2)
    {
        if ([[NSString stringWithFormat:@"%ld",(long)(1970 + row)] isEqualToString:self.nowDay])
        {
            [pickerView selectRow:row inComponent:component animated:YES];
        }
        return [NSString stringWithFormat:@"%ld",(long)(row + 1)];
    }
    else if (component == 3)
    {
        if ([[NSString stringWithFormat:@"%@",@(1970 + row)] isEqualToString:self.nowHour])
        {
            [pickerView selectRow:row inComponent:component animated:YES];
        }
        return [NSString stringWithFormat:@"%@",@(row)];
    }
    else
    {
        if ([[NSString stringWithFormat:@"%@",@(1970 + row)] isEqualToString:self.nowMin])
        {
            [pickerView selectRow:row inComponent:component animated:YES];
        }
        return [NSString stringWithFormat:@"%@",@(row)];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0)
    {
        self.selectedYear = [NSString stringWithFormat:@"%@",@(1970 + row)];
    }
    else if (component == 1)
    {
        if (1 + row < 10)
        {
            self.selectedMonth = [NSString stringWithFormat:@"0%@",@(1 + row)];
        }
        else
        {
            self.selectedMonth = [NSString stringWithFormat:@"%@",@(1 + row)];
        }
    }
    else if (component == 2)
    {
        if (1 + row < 10)
        {
            self.selectedDay = [NSString stringWithFormat:@"0%@",@(1 + row)];
        }
        else
        {
            self.selectedDay = [NSString stringWithFormat:@"%@",@(1 + row)];
        }
    }
    else if (component == 3)
    {
        if (1 + row < 10)
        {
            self.selectedHour = [NSString stringWithFormat:@"0%@",@(1 + row)];
        }
        else
        {
            self.selectedHour = [NSString stringWithFormat:@"%@",@(1 + row)];
        }
    }
    else
    {
        if (1 + row < 10)
        {
            self.selectedMin = [NSString stringWithFormat:@"0%@",@(1 + row)];
        }
        else
        {
            self.selectedMin = [NSString stringWithFormat:@"%@",@(1 + row)];
        }
    }
   
}

@end
