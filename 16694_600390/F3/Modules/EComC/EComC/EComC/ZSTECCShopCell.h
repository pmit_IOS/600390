//
//  ZSTECCShopCell.h
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCShopCell : UITableViewCell

@property (strong,nonatomic) UIImageView *shopLogoIV;
@property (strong,nonatomic) UILabel *shopNameLB;
@property (strong,nonatomic) UILabel *startTitleLB;
@property (strong,nonatomic) UILabel *distanceLB;
@property (strong,nonatomic) UILabel *lineLB;

- (void)createUI;
- (void)setCellData:(NSDictionary *)dic;

@end
