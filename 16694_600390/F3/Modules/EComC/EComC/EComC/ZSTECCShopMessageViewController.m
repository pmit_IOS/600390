//
//  ZSTECCShopMessageViewController.m
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShopMessageViewController.h"
#import "ZSTECCShopReviewsCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ZSTF3Engine.h"
#import "ZSTECCShopInfoAddressTableViewCell.h"

@interface ZSTECCShopMessageViewController()
{
    TKLoadMoreView *_loadMoreView;
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    NSInteger _pagenum;
}


@end

#define imageStar(i) [[NSString stringWithFormat:@"200%d",i] intValue]

@implementation ZSTECCShopMessageViewController{

    ZSTECCAutoLabel *heightLable;
    ZSTECCAutoLabel *heightLableAdd;
    ZSTECCAutoLabel *heightShopIntro;
    ZSTECCAutoLabel *heightShopIntro1;
    ZSTECCAutoLabel *heightShopIntro2;
    
    
    NSMutableDictionary *dic;
    
    NSInteger labelHeight;
    NSInteger adHeight;
    NSInteger introHeight;
    
    UIImageView *starImage1;
    UIImageView *starImage2;
    UIImageView *starImage3;
    UIImageView *starImage4;
    UIImageView *starImage5;
    
    UIView * sectionView;
    
    NSDictionary *adic;
    
    BOOL btnDown;
    
    NSString *introduceStr;
    NSString *str;
    
    ZSTECCAutoLabel *introduceLabel;
    UIButton *tempBtn;
    UIButton *tempBtnCover;
    UILabel *tableSectionLabel;
    UIView *tableSectionView;

}

static NSString *const shopMessageCell = @"shopMessageCell";


- (void)viewDidLoad
{
    [super viewDidLoad];
    ZSTF3Engine *engine = [[ZSTF3Engine alloc] init];
    engine.delegate = self;
    self.engine = engine;
    [self initData];
    
    btnDown = NO;
    
    [self.engine getShopDetailByShopId:self.shopId];
    
    self.reviewArray = @[@{@"headImage":@"http://mod.pmit.cn:800/NewsB/Home/GetFile?FileID=68857",@"name":@"安安",@"time":@"2015-08-09 10:20",@"levelImages":@"4",@"reviewText":@"向消费者专门提供各种酒水、食品，消费场所和设施的食品生产经营行业;向消费者专门提供各种酒水、食品，消费场所和设施的食品生产经营行业;",@"foodImages":@"http://zjdaily.zjol.com.cn/csjr/images/2006-09/30/csjr20060930c0008v01b006.jpg"}];
    
    NSArray *sarray = @[@{@"phone":@"020-9879872763",@"time":@"20162093801928388",@"address":@"广东省广州市天河区东圃一横路汇宝商业中心B座1A020广东省广州市天河区东圃一横路汇宝商业中心B座1A020"}];
    
    adic= [[NSMutableDictionary alloc] init];
    adic = (NSDictionary *)sarray[0];
    
    heightLableAdd = [[ZSTECCAutoLabel  alloc] init];
    heightLableAdd.font = [UIFont systemFontOfSize:14];
    heightLableAdd.text = [adic objectForKey:@"address"];
    adHeight = [heightLableAdd getAttributedStringHeightWidthValue:210];
    
    heightLable= [[ZSTECCAutoLabel alloc] init];
    dic = [[NSMutableDictionary alloc] init];
    dic = (NSMutableDictionary *)self.reviewArray[0];
    
    heightLable.text =[dic objectForKey:@"reviewText"];
    labelHeight = [heightLable getAttributedStringHeightWidthValue:320];//自动获取label的高度
    
    self.view.frame = CGRectMake(0, 47, WIDTH, HEIGHT - 64 - 47);
    [self buildTableView];
    
    introduceStr =@"2013年12月2日 - IOS系统提取百度云下载的文件独家发布,IOS系统提取百度云下载的文件独家发布在苹果的设备用,2013年12月2日 - IOS系统提取百度云下载的文件独家发布,IOS系统提取百度云下载的文件独家发布在苹果的设备用";
    //初始化商铺介绍高度
    str = introduceStr;
    introHeight = 37;
    
    [self initStars:4];
    [self buildTableViewHeader];
}

#pragma mark - 满意度 图标
-(void)initStars:(NSInteger)level{

    sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    sectionView.backgroundColor = [UIColor whiteColor];
    starImage1 = [[UIImageView alloc] initWithFrame:CGRectMake(110+5, 15, 20, 20)];
    starImage1.tag = 2000;
    [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_starGray.png"]];
    [sectionView addSubview:starImage1];
    
    starImage2 = [[UIImageView alloc] initWithFrame:CGRectMake(110+30*1, 15, 20, 20)];
    starImage2.tag = 2001;
    [starImage2 setImage:[UIImage imageNamed:@"model_ecomc_big_starGray.png"]];
    [sectionView addSubview:starImage2];
    
    starImage3 = [[UIImageView alloc] initWithFrame:CGRectMake(105+30*2, 15, 20, 20)];
    [starImage3 setImage:[UIImage imageNamed:@"model_ecomc_big_starGray.png"]];
    starImage3.tag = 2002;
    [sectionView addSubview:starImage3];
    
    starImage4 = [[UIImageView alloc] initWithFrame:CGRectMake(100+30*3, 15, 20, 20)];
    [starImage4 setImage:[UIImage imageNamed:@"model_ecomc_big_starGray.png"]];;
    starImage4.tag = 2003;
    [sectionView addSubview:starImage4];
    
    starImage5 = [[UIImageView alloc] initWithFrame:CGRectMake(95+30*4, 15, 20, 20)];
    [starImage5 setImage:[UIImage imageNamed:@"model_ecomc_big_starGray.png"]];
    starImage5.tag = 2004;
    [sectionView addSubview:starImage5];
    
    if (level == 1) {
        [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
    }else if (level == 2){
        [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage2 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
    }else if (level == 3){
        [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage2 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage3 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
    }else if (level == 4){
        [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage2 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage3 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage4 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
    }else if (level == 5){
        [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage2 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage3 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage4 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage5 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
    }
    
    UILabel *levelLabel = [[UILabel alloc] initWithFrame:CGRectMake(100+30*4+25, 15, 40, 25)];
    levelLabel.text =[NSString stringWithFormat:@"%ld.0",level];
    levelLabel.font = [UIFont systemFontOfSize:19];
    levelLabel.textColor = [UIColor lightGrayColor];
    [sectionView addSubview:levelLabel];
}

- (void)buildTableView
{
    self.shopMessageTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.view.frame.size.height) style:UITableViewStylePlain];
    self.shopMessageTableView.delegate = self;
    self.shopMessageTableView.dataSource = self;
    self.shopMessageTableView.backgroundColor = RGBA(244, 244, 244, 1);
    [self.shopMessageTableView registerClass:[ZSTECCShopReviewsCell class] forCellReuseIdentifier:shopMessageCell];
    self.shopMessageTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
    self.shopMessageTableView.separatorStyle = UITableViewCellAccessoryNone;
    [self.view addSubview:self.shopMessageTableView];
}

- (void)buildTableViewHeader
{
    [self.shopMessageTableView setTableHeaderView:_carouselView];
    self.shopMessageTableView.tableHeaderView.backgroundColor = RGBACOLOR(224, 224, 224, 1);
}

-(void)initData
{
    
    _pagenum = 1;
    _hasMore = YES;
    
    _carouselView = [[ZSTECCCarouselView alloc]initWithFrame:CGRectMake(0, 0, 320, 160)];
    _carouselView.carouselDataSource = self;
    _carouselView.carouselDelegate = self;
    _carouselView.haveTitle = YES;
    
    
    _carouseData = nil;
    
    //[self.view addSubview:_carouselView];
    //轮播图获取
    //[self.engine getEcomCCarousel];
    
}


#pragma mark - 数据源
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return 1;
    }
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0){
        return  40+adHeight+15;
    }else if (indexPath.section == 1){
        return  30+introHeight+10;
    }
    
    return 20+70+labelHeight+50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        static NSString *defualts2 = @"ZSTECCShopInfoAddressTableViewCell";
        ZSTECCShopInfoAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:defualts2];
        
        if (!cell) {
             cell = [[ZSTECCShopInfoAddressTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:defualts2];
        }
        
        [cell createUI];
        [cell setCellData:adic];
        cell.height = adHeight;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return  cell;
    }
    
    
    if(indexPath.section ==1){
        
        static NSString *defualts1 = @"defualts1";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:defualts1];
    
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:defualts1];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.contentView.backgroundColor = RGBACOLOR(224, 224, 224,0.5);
            tableSectionView = [[UIView alloc] init];
            tableSectionView.backgroundColor = [UIColor whiteColor];
            [cell.contentView addSubview:tableSectionView];
            
            UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 1)];
            line.backgroundColor = [UIColor lightGrayColor];
            line.alpha =0.5;
            [tableSectionView addSubview:line];
            
            tableSectionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 70, 30)];
            tableSectionLabel.text = @"商家介绍:";
            tableSectionLabel.font = [UIFont systemFontOfSize:14];
            tableSectionLabel.textColor = [UIColor grayColor];
            [tableSectionView addSubview:tableSectionLabel];
            
            introduceLabel = [[ZSTECCAutoLabel alloc] init];
            introduceLabel.textColor = [UIColor grayColor];
            introduceLabel.font = [UIFont systemFontOfSize:14];
            [tableSectionView addSubview:introduceLabel];
            
            tempBtn = [[UIButton alloc] init];
            [cell.contentView addSubview:tempBtn];
            
            tempBtnCover = [[UIButton alloc] init];
            [tempBtnCover addTarget:self action:@selector(tapAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:tempBtnCover];
        }
        
        tableSectionView.frame = CGRectMake(0, 0, 320, introHeight+10+20);
        introduceLabel.frame = CGRectMake(10+70, 6, 230, introHeight);
        introduceLabel.text =str;
        tempBtn.frame = CGRectMake(150,introHeight+10, 15, 15);
        tempBtnCover.frame = CGRectMake(130,introHeight+10, 60, 20);
        
        if (btnDown) {
            [tempBtn setBackgroundImage:[UIImage imageNamed:@"model_ecomc_up.png"] forState:UIControlStateNormal];
        }else{
            [tempBtn setBackgroundImage:[UIImage imageNamed:@"model_ecomc_down.png"] forState:UIControlStateNormal];
        }
        
        return  cell;
    }
    
    
    
    if(indexPath.section == 2){
        ZSTECCShopReviewsCell *cell = [tableView dequeueReusableCellWithIdentifier:shopMessageCell];
        
        if (!cell) {
            cell = [[ZSTECCShopReviewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:shopMessageCell];
        }
        
        [cell createUI];
        [cell setCellData:dic];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return  cell;
    }
    
    
    return nil;

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (section == 2) {
       // NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
        
        UILabel * label = [[UILabel alloc] init];
        label.text = @"商家满意度";
        label.textColor = [UIColor grayColor];
        label.frame = CGRectMake(10, 10, 120, 30);
        label.font=[UIFont fontWithName:@"Arial" size:19];
        
        [sectionView addSubview:label];
        
        return sectionView;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    if (section == 2) {
        return 50;
    }
    return 0;
}


//商家满意度
-(void)starImage:(int)num{
    
    for (int i=0; i<=num; i++) {
        [self initStar:num forEeach:i];
    }
    
}


-(void)initStar:(int)tag forEeach:(int)i{
    
    UIImageView *starImage = (UIImageView *)[self.view viewWithTag:imageStar(i)];
    
    [starImage setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];

}


- (void)getEcomCCarouselDidSucceed:(NSArray *)carouseles
{
    _carouseData = nil;
    _carouseData = [EcomCCarouseInfo ecomCCarouseWithArray:carouseles];
    if (_carouseData.count>0) {
        [_carouselView reloadData];
    }
}
- (void)getEcomCCarouselDidFailed:(int)resultCode
{
    [TKUIUtil alertInView:self.view withTitle:@"广告暂无数据！" withImage:nil];
}

#pragma mark - ZSTEComBCarouselViewDataSource
- (NSInteger)numberOfViewsInCarouselView:(ZSTECCCarouselView *)newsCarouselView;
{
    return [_carouseData count];
}

- (EcomCCarouseInfo *)carouselView:(ZSTECCCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    
    if ([_carouseData  count] != 0) {
        return [_carouseData  objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTEComBCarouselViewDelegate
- (void)carouselView:(ZSTECCCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    //NSString *urlPath = [[_carouseData objectAtIndex:index] ecomBCarouseInfoLinkurl];
    //    if (urlPath == nil || [urlPath length] == 0 || [urlPath isEmptyOrWhitespace]) {
    //        return;
    //    }
    //    EComCHomePageInfo * pageInfo = [[EComCHomePageInfo alloc]init];
    //    pageInfo.ecombHomeProductid = [NSString stringWithFormat:@"%d",[[_carouseData objectAtIndex:index] ecomBCarouseInfoProductid]];
    //
    //    //    ZSTEComBGoodsShowViewController * showViewcontroller = [[ZSTEComBGoodsShowViewController alloc]init];
    //    ZSTGoodsShowViewController *showViewcontroller = [[ZSTGoodsShowViewController alloc] init];
    //    showViewcontroller.pageInfo = pageInfo;
    //    showViewcontroller.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:showViewcontroller animated:YES];
    //    [showViewcontroller release];
    
    //
    //    ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
    //    [webViewController setURL:urlPath];
    //    webViewController.hidesBottomBarWhenPushed = YES;
    //    webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    //
    //    [self.navigationController pushViewController:webViewController animated:NO];
    //    [webViewController release];
}

#pragma mark - 商铺介绍扩展
-(void)tapAction:(UIButton *)sender{

    if (!btnDown) {
        heightShopIntro1 = [[ZSTECCAutoLabel alloc] init];
        heightShopIntro1.font = [UIFont systemFontOfSize:14];
        heightShopIntro1.text = introduceStr;
        str = introduceStr;
        introHeight = [heightShopIntro1 getAttributedStringHeightWidthValue:230];
        btnDown = YES;
    }else{
        introHeight = 37;
        btnDown = NO;
    }
    [self.shopMessageTableView reloadData];
}

#pragma mark - 
-(void)getShopDetailDidSucceed:(NSDictionary *)shopDetail{
    
    NSLog(@"====shopDetail====>%@",shopDetail);
}

-(void)getShopDetailDidFailed:(int)resultCode{
    NSLog(@"====shopDetail=  失败===>");

}

@end
