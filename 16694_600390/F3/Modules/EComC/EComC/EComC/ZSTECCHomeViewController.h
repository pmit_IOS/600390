//
//  ZSTECCHomeViewController.h
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"
#import "ZSTF3Engine+EComC.h"
//#import "ZSTLoginViewController.h"
#import "ZSTECCCarouselView.h"
#import <ZSTLoginController.h>


@class ZSTF3Engine;
@interface ZSTECCHomeViewController : ZSTModuleBaseViewController<UITableViewDataSource,UITableViewDelegate,ZSTECCCarouselViewDataSource,TKLoadMoreViewDelegate,ZSTECCCarouselViewDelegate,ZSTF3EngineECCDelegate,ZSTLoginControllerDelegate>
{
    TKLoadMoreView *_loadMoreView;
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    NSInteger _pagenum;
}
@property (retain,nonatomic) UIView * headView;
@property (retain,nonatomic) UITableView * tableView;
@property (retain,nonatomic) NSMutableArray * btnTextArr;
@property (retain,nonatomic) ZSTECCCarouselView * carouselView;
@property (retain,nonatomic) NSArray * carouseData;
@property (retain,nonatomic) NSMutableArray * resultData;
@property (nonatomic, retain) ZSTF3Engine *engine;
@property (nonatomic, strong) UIView *titleView;

@end
