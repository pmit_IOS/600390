//
//  ZSTECCFoodDetailCell.m
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCFoodDetailCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTECCFoodDetailCell

- (void)createUI
{
    if (!self.foodIV)
    {
        self.foodIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
        self.foodIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.foodIV];
        
        self.foodTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, WIDTH * 0.7 - 70, 20)];
        self.foodTitleLB.textColor = RGBA(85, 85, 85, 1);
        self.foodTitleLB.font = [UIFont systemFontOfSize:14.0f];
        self.foodTitleLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.foodTitleLB];
        
        self.minusBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(WidthRate(250), 70, 25, 25)];
        [self.minusBtn setImage:[UIImage imageNamed:@"minus.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.minusBtn];
        
        self.numLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(250) + 25, 70, WidthRate(170) - 25, 25)];
        self.numLB.textAlignment = NSTextAlignmentCenter;
        self.numLB.font = [UIFont systemFontOfSize:14.0f];
        self.numLB.textColor = RGBA(85, 85, 85, 1);
        self.numLB.text = @"2";
        [self.contentView addSubview:self.numLB];
        
        self.plusBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(WidthRate(420), 70, 25, 25)];
        [self.plusBtn setImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.plusBtn];
        
        self.priceLB = [[UILabel alloc] initWithFrame:CGRectMake(70, 40, WIDTH * 0.6, 20)];
        self.priceLB.font = [UIFont systemFontOfSize:12.0f];
        self.priceLB.textColor = RGBA(187, 187, 187, 1);
        [self.contentView addSubview:self.priceLB];
        
    }
    
    if (!self.isHasBook)
    {
        self.numLB.hidden = YES;
        self.minusBtn.hidden = YES;
    }
    else
    {
        self.numLB.hidden = NO;
        self.minusBtn.hidden = NO;
    }
}

- (void)getCellData:(NSDictionary *)foodDetailDic
{
    [self.foodIV setImageWithURL:[NSURL URLWithString:[foodDetailDic safeObjectForKey:@"attachUrl"]] placeholderImage:[UIImage imageNamed:@"module_personal_avater_deafaut.png"]];
    self.foodTitleLB.text = [foodDetailDic safeObjectForKey:@"dishesName"];
    NSString *priceString = [NSString stringWithFormat:@"%.2lf元/份",[[foodDetailDic safeObjectForKey:@"price"] doubleValue]];
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:priceString];
    NSInteger location = [priceString rangeOfString:@"元"].location;
    [nodeString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],UITextAttributeTextColor:RGBA(254, 77, 61, 1)} range:NSMakeRange(0, location)];
    self.priceLB.attributedText = nodeString;
}

@end
