//
//  ZSTECCBookSeatViewController.h
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine+EComC.h"
#import "ZSTECCTimePickerView.h"
#import "ZSTPeopleNumPickerView.h"

@interface ZSTECCBookSeatViewController : UIViewController <ZSTF3EngineDelegate,ZSTF3EngineECCDelegate,ZSTPeopleNumPickerViewDelegate,ZSTECCTimePickerViewDelegate>

@property (strong,nonatomic) UITableView *seatMessageTableView;
@property (strong,nonatomic) ZSTECCTimePickerView *pickView;
@property (strong,nonatomic) NSString *selectedSeatTypeString;
@property (strong,nonatomic) NSString *selectedSexTypeString;
@property (strong,nonatomic) ZSTF3Engine *engine;
@property (strong,nonatomic) NSString *shopId;
@property (strong,nonatomic) ZSTPeopleNumPickerView *peoplePickerView;

@end
