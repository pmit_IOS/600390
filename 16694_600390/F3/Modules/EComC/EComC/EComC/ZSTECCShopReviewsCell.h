//
//  ZSTECCShopReviewsCell.h
//  EComC
//
//  Created by qiugian on 15/8/8.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECCAutoLabel.h"

@protocol ZSTECCShopReviewsCellDelegate <NSObject>

-(void)clickImage:(UIImageView *)button imageArray:(NSArray *)imageArray;

@end


@interface ZSTECCShopReviewsCell : UITableViewCell

@property (nonatomic, strong) UIImageView *headImage;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *time;
@property (nonatomic, strong) UIImageView *levelImages;
@property (nonatomic, strong) ZSTECCAutoLabel *reviewContent;
@property (nonatomic, strong) UIImageView *imageBtn;
@property (nonatomic, strong) NSArray *imageArray;

@property (nonatomic, strong) id<ZSTECCShopReviewsCellDelegate> delegate;

@property (nonatomic, assign) NSInteger labelHeight;

- (void)createUI;

- (void)setCellData:(NSDictionary *)dic;

@end
