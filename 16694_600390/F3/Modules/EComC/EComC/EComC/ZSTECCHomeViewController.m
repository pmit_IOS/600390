//
//  ZSTECCHomeViewController.m
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCHomeViewController.h"
#import "ZSTECCShopCell.h"
#import "ZSTECCShop.h"
#import "ZSTECCReservationViewComtroller.h"
#import "CCLocationManager.h"
#import "ZSTECCOrderListViewController.h"
#import "ZSTECCSearchViewController.h"


#define IS_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)

@interface ZSTECCHomeViewController () <UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate>{

    CLLocationManager *locationmanager;

}

@property (strong,nonatomic) NSMutableArray *shopNameArr;
@property (strong,nonatomic) UITableView *shopTableView;
@property (assign,nonatomic) NSInteger currentPage;
@property (assign,nonatomic) NSInteger totalPage;
@property (strong,nonatomic) NSMutableArray *hasRefreshArr;
@property (assign,nonatomic) BOOL isLoadMore;

@end


@implementation ZSTECCHomeViewController{
    UIButton *leftBtn;
    UIButton *rightBtn;
    
    NSArray *bannerArray;
    
    float latitude;//纬度
    float longitude;//经度
    
    UILabel *locationLable;
}

static NSString *const shopCell = @"shopCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createTitleView];
    
    ZSTF3Engine *engine = [[ZSTF3Engine alloc] init];
    engine.delegate  = self;
    self.engine = engine;
    
    //[self.navigationController setNavigationBarHidden:YES];
    [self initData];
    
    self.navigationItem.rightBarButtonItem = [self aInitRightBtn];
    //self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"东圃一路66号   >", nil)];
    self.hasRefreshArr = [NSMutableArray array];
    self.shopNameArr = [NSMutableArray array];
    self.currentPage = 1;
    self.isLoadMore = NO;
    
    [self getAllInfo];
    
//    [self buildFakeData];
    [self buildTableView];
    
   
}

-(void)viewWillAppear:(BOOL)animated{

   self.navigationItem.leftBarButtonItem = [self aInitLeftBtn];
}

#pragma mark - 创建标题视图
-(void)createTitleView{
  
    self.titleView = [[UIView alloc] initWithFrame:CGRectMake(52, 0, 220, 44)];
    //self.titleView.backgroundColor = [UIColor grayColor];
    
    UIImageView *locationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 15,20)];
    [locationIcon setImage:[UIImage imageNamed:@"model_ecomc_location_icon_big.png"]];
    [self.titleView addSubview:locationIcon];
    
    locationLable = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, 220-30, 20)];
    locationLable.text = @"地址查找中...";
    locationLable.textColor = [UIColor whiteColor];
    locationLable.font = [UIFont systemFontOfSize:17];
    [self.titleView addSubview:locationLable];
    
    self.navigationItem.titleView = self.titleView;

}


-(void)initData
{
    
    _pagenum = 1;
    _hasMore = YES;
    
    _carouselView = [[ZSTECCCarouselView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, WIDTH / 2)];
    _carouselView.carouselDataSource = self;
    _carouselView.carouselDelegate = self;
    _carouselView.haveTitle = YES;
    
    _carouseData = nil;
    //_carouseData = [EcomCCarouseInfo ecomCCarouseWithArray:bannerArray];
    //[self.view addSubview:_carouselView];
    //轮播图获取
    [self.engine getEcomCCarousel];
    
}


- (UIBarButtonItem *) aInitLeftBtn
{
    leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(10, 25, 35, 35);
    [leftBtn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(orderBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    leftBtn.layer.cornerRadius = leftBtn.frame.size.width / 2.0;
    leftBtn.layer.borderColor = [UIColor clearColor].CGColor;
    leftBtn.layer.masksToBounds = YES;
    
    NSString *path = nil;
    UIImage *image = nil;
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    } else {
        
        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic safeObjectForKey:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        if (path && path.length > 0) {
            
            NSString *homePath = NSHomeDirectory();
            NSString *imgTempPath = [path substringFromIndex:[path rangeOfString:@"tmp"].location];
            NSString *finalPath = [NSString stringWithFormat:@"%@/%@",homePath,imgTempPath];
            NSData *reader = [NSData dataWithContentsOfFile:finalPath];
            image = [UIImage imageWithData:reader];
            
            if (!image) {
                
                image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
            }
            
        } else {
            
            image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
        }
    }
    
    [leftBtn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:leftBtn.imageView.frame];
    iv.image = image;
    [leftBtn insertSubview:iv aboveSubview:leftBtn.imageView];
    
    
    return [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
}


- (UIBarButtonItem *) aInitRightBtn
{
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(10, 25, 20, 20);
    [rightBtn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(goToSearch:) forControlEvents:UIControlEventTouchUpInside];
//    UIImage *image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    UIImage *image = [UIImage imageNamed:@"ecomCSearch.png"];
    
    [rightBtn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:rightBtn.imageView.frame];
    iv.image = image;
    [rightBtn insertSubview:iv aboveSubview:rightBtn.imageView];
    
    
    return [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
}


-(void)orderBtnAction{
    
    ZSTECCOrderListViewController * orderListView = [[ZSTECCOrderListViewController alloc] init];
    orderListView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:orderListView animated:YES];
    
}


- (void)getEcomCCarouselDidSucceed:(NSArray *)carouseles
{
    _carouseData = nil;
    _carouseData = [EcomCCarouseInfo ecomCCarouseWithArray:carouseles];
    if (_carouseData.count>0) {
        [_carouselView reloadData];
    }
}
- (void)getEcomCCarouselDidFailed:(int)resultCode
{
    NSLog(@"广告暂无数据！广告暂无数据！广告暂无数据！广告暂无数据！广告暂无数据！");
    
    [TKUIUtil alertInView:self.view withTitle:@"广告暂无数据！" withImage:nil];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildFakeData
{
    self.shopNameArr = @[@{@"shopName":@"膳人膳食",@"shopLogo":@"http://mod.pmit.cn:800/NewsB/Home/GetFile?FileID=68857",@"startPrice":@"21",@"distance":@"217"},@{@"shopName":@"意达披萨",@"shopLogo":@"http://mod.pmit.cn:800/NewsB/Home/GetFile?FileID=17085",@"startPrice":@"30",@"distance":@"50"},@{@"shopName":@"开心花甲",@"shopLogo":@"http://mod.pmit.cn:800/NewsB/Home/GetFile?FileID=77485",@"startPrice":@"20",@"distance":@"400"},@{@"shopName":@"林记隆江猪脚饭",@"shopLogo":@"http://mod.pmit.cn:800/NewsB/Home/GetFile?FileID=6761",@"startPrice":@"12",@"distance":@"100"},@{@"shopName":@"丰椰炖品",@"shopLogo":@"http://mod.pmit.cn:800/NewsB/Home/GetFile?FileID=77488",@"startPrice":@"21",@"distance":@"150"},@{@"shopName":@"粗茶淡饭",@"shopLogo":@"http://mod.pmit.cn:800/NewsB/Home/GetFile?FileID=68857",@"startPrice":@"30",@"distance":@"20"},@{@"shopName":@"湘缘",@"shopLogo":@"http://mod.pmit.cn:800/NewsB/Home/GetFile?FileID=30587",@"startPrice":@"10",@"distance":@"100"}];
    
    
    bannerArray = @[@{@"ad_imgurl":@"http://mod.pmit.cn/shelle/GetImageFile.ashx?ecid=600390&moduleid=37&id=1&v=1439534075",@"ad_linkurl":@"http://mod.pmit.cn/newsbwap/index.html?ecid=600390&moduletype=79735&module_type=79735",@"ad_title":@""},@{@"ad_imgurl":@"http://mod.pmit.cn/shelle/GetImageFile.ashx?ecid=600390&moduleid=37&id=2&v=1439534075",@"ad_linkurl":@"http://www.pmit.cn/app/jiajianzhichuang.php",@"ad_title":@""}];
}

- (void)buildTableView
{
    self.shopTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 50) style:UITableViewStylePlain];
    self.shopTableView.delegate = self;
    self.shopTableView.dataSource = self;
    [self.shopTableView registerClass:[ZSTECCShopCell class] forCellReuseIdentifier:shopCell];
    [self.view addSubview:self.shopTableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = [UIColor whiteColor];
    self.shopTableView.tableFooterView = footerView;
}

- (void)buildTableViewHeader
{
    self.tableView.tableHeaderView = _carouselView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.shopNameArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTECCShopCell *cell = [tableView dequeueReusableCellWithIdentifier:shopCell];
    [cell createUI];
    [cell setCellData:self.shopNameArr[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row == self.shopNameArr.count - 4)
    {
        if (![self.hasRefreshArr containsObject:indexPath])
        {
            if (self.currentPage < self.totalPage)
            {
                self.currentPage ++;
                self.isLoadMore = YES;
                [self loadMoreData];
            }
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSString *shopId = [self.shopNameArr[indexPath.row] safeObjectForKey:@"id"];
    ZSTECCReservationViewComtroller *reservationView = [[ZSTECCReservationViewComtroller alloc] init];
    reservationView.shopId = shopId;
    reservationView.hidesBottomBarWhenPushed = YES;
    reservationView.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString([self.shopNameArr[indexPath.row] objectForKey:@"shopName"], nil)];
    [self.navigationController pushViewController:reservationView animated:YES];

}


- (void)buildTableHeaderView
{
    
}

#pragma mark - 获取纬度与经度
-(void)getLat
{
    if (IS_IOS8) {
        
        [[CCLocationManager shareLocation] getLocationCoordinate:^(CLLocationCoordinate2D locationCorrrdinate) {
            
            latitude = locationCorrrdinate.latitude;
            longitude =locationCorrrdinate.longitude;
            
            NSLog(@"%f %f",locationCorrrdinate.latitude,locationCorrrdinate.longitude);
        }];
    }
    
}

#pragma mark - 获取当地城市
-(void)getCity
{
    
    if (IS_IOS8) {
        
        [[CCLocationManager shareLocation]getCity:^(NSString *cityString) {
            NSLog(@"%@",cityString);
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            //locationLable.text = [NSString stringWithFormat:@"%@  >",cityString];
            
            [self.engine getShopListByUserId:[ZSTF3Preferences shared].UserId andLat:[NSString stringWithFormat:@"%f",[[ud objectForKey:CCLastLatitude] doubleValue]] andLng:[NSString stringWithFormat:@"%f",[[ud objectForKey:CCLastLongitude] doubleValue]] andCurrentPage:self.currentPage PageSize:10];

        }];
        
    }
    
}

#pragma mark - 获取所有当地信息
-(void)getAllInfo
{
    __block NSString *string;
    
    if (IS_IOS8) {
        
        [[CCLocationManager shareLocation] getLocationCoordinate:^(CLLocationCoordinate2D locationCorrrdinate) {
            string = [NSString stringWithFormat:@"%f %f",locationCorrrdinate.latitude,locationCorrrdinate.longitude];
        } withAddress:^(NSString *addressString) {
            locationLable.text = [NSString stringWithFormat:@"%@  >",addressString];
            string = [NSString stringWithFormat:@"%@\n%@",string,addressString];
        }];
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [self.engine getShopListByUserId:[ZSTF3Preferences shared].UserId andLat:[NSString stringWithFormat:@"%f",[[ud objectForKey:CCLastLatitude] doubleValue]] andLng:[NSString stringWithFormat:@"%f",[[ud objectForKey:CCLastLongitude] doubleValue]] andCurrentPage:self.currentPage PageSize:10];
    }
    
    
    
}


#pragma mark - ZSTEComBCarouselViewDataSource
- (NSInteger)numberOfViewsInCarouselView:(ZSTECCCarouselView *)newsCarouselView;
{
    return [_carouseData count];
}

- (EcomCCarouseInfo *)carouselView:(ZSTECCCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    
    if ([_carouseData  count] != 0) {
        return [_carouseData  objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTEComBCarouselViewDelegate
- (void)carouselView:(ZSTECCCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    //NSString *urlPath = [[_carouseData objectAtIndex:index] ecomBCarouseInfoLinkurl];
    //    if (urlPath == nil || [urlPath length] == 0 || [urlPath isEmptyOrWhitespace]) {
    //        return;
    //    }
//    EComCHomePageInfo * pageInfo = [[EComCHomePageInfo alloc]init];
//    pageInfo.ecombHomeProductid = [NSString stringWithFormat:@"%d",[[_carouseData objectAtIndex:index] ecomBCarouseInfoProductid]];
//    
//    //    ZSTEComBGoodsShowViewController * showViewcontroller = [[ZSTEComBGoodsShowViewController alloc]init];
//    ZSTGoodsShowViewController *showViewcontroller = [[ZSTGoodsShowViewController alloc] init];
//    showViewcontroller.pageInfo = pageInfo;
//    showViewcontroller.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:showViewcontroller animated:YES];
//    [showViewcontroller release];
    
    //
    //    ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
    //    [webViewController setURL:urlPath];
    //    webViewController.hidesBottomBarWhenPushed = YES;
    //    webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    //
    //    [self.navigationController pushViewController:webViewController animated:NO];
    //    [webViewController release];
}


- (void)getShopAddressInfoDidSucceed:(NSDictionary *)addressInfo
{
    if (self.isLoadMore)
    {
        [self.shopNameArr addObject:[addressInfo safeObjectForKey:@"dataList"]];
    }
    else
    {
        self.shopNameArr = [addressInfo safeObjectForKey:@"dataList"];
    }
    [self.shopTableView reloadData];
}

- (void)loadMoreData
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [self.engine getShopListByUserId:[ZSTF3Preferences shared].UserId andLat:[NSString stringWithFormat:@"%f",[[ud objectForKey:CCLastLatitude] doubleValue]] andLng:[NSString stringWithFormat:@"%f",[[ud objectForKey:CCLastLongitude] doubleValue]] andCurrentPage:self.currentPage PageSize:10];
}

- (void)goToSearch:(UIButton *)sender
{
    ZSTECCSearchViewController *searchVC = [[ZSTECCSearchViewController alloc] init];
    [self.navigationController pushViewController:searchVC animated:YES];
}

@end
