//
//  ZSTECCSearchResultDelegate.m
//  EComC
//
//  Created by pmit on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCSearchResultDelegate.h"
#import "ZSTECCShopCell.h"

@implementation ZSTECCSearchResultDelegate

static NSString *const shopCell = @"shopCell";

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTECCShopCell *cell = [tableView dequeueReusableCellWithIdentifier:shopCell];
    [cell createUI];
    [cell setCellData:self.resultArr[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

@end
