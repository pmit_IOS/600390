//
//  ZSTECCShopInfoAddressTableViewCell.h
//  EComC
//
//  Created by anqiu on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECCAutoLabel.h"

@interface ZSTECCShopInfoAddressTableViewCell : UITableViewCell

@property (nonatomic,strong) ZSTECCAutoLabel *addressLabel;
@property (nonatomic,strong) UILabel *time;
@property (nonatomic,strong) UILabel *phone;
@property (nonatomic) NSInteger height;

-(void)createUI;

-(void)setCellData:(NSDictionary *)dic;

@end
