//
//  ZSTECCPayWayCell.h
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCPayWayCell : UITableViewCell

@property (strong,nonatomic) UILabel *payTitleLB;
@property (strong,nonatomic) UIImageView *payWayIV;
@property (strong,nonatomic) UIImageView *checkIV;

- (void)createUIWithIsTitle:(BOOL)isTitle;
- (void)setCellDataWithPayWay:(BOOL)isWeChat;

@end
