//
//  ZSTECCAutoLabel.h
//  EComC
//
//  Created by qiuguian on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCAutoLabel : UILabel
{
@private
    CGFloat  _characterSpacing;  //字间距
    long     _linesSpacing;      //行间距
    CGFloat  _paragraphSpacing;   //段间距
    
}

@property (nonatomic,assign) CGFloat characterSpacing;
@property (nonatomic,assign) CGFloat paragraphSpacing;
@property (nonatomic,assign) long    linesSpacing;

/**
 *  绘制前获取label高度
 *
 *  @param width <#width description#>
 */
-(int)getAttributedStringHeightWidthValue:(int)width;

@end
