//
//  ZSTECCCarouselView.h
//  EComC
//
//  Created by qiuguian on 15/8/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKAsynImageView.h"

typedef enum
{
    pageControlStateLEFT,
    pageControlStateCENTER,
    pageControlStateRIGHT
}pageControlState;

@class ZSTECCCarouselView;

@protocol ZSTECCCarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(ZSTECCCarouselView *)newsCarouselView;
- (EcomCCarouseInfo *)carouselView:(ZSTECCCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTECCCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTECCCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface ZSTECCCarouselView : UIView<HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    TKPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;

    UILabel * _titleLabel;
    NSString * _carousBgImg;
}

@property(nonatomic, assign) id<ZSTECCCarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<ZSTECCCarouselViewDelegate> carouselDelegate;
@property(nonatomic, assign) BOOL haveTitle;
- (void)reloadData;
- (void)stopAnimation;
- (void)startAnimation;
- (void)hideStateBar;
- (void)pageControlState:(pageControlState) state;
- (void)setCarouselViewBackgroundImg:(NSString *)img;
@end
