//
//  ZSTECCShopReviewsCell.m
//  EComC
//
//  Created by 阮飞 on 15/8/8.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShopReviewsCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+WebCache.h"

@implementation ZSTECCShopReviewsCell{
    
    UIView *headView;
    UIView *contentView;
    
    ZSTECCRoundRectView * _line;
    
    UIImageView *star1;
    UIImageView *star2;
    UIImageView *star3;
    UIImageView *star4;
    UIImageView *star5;

}

- (void)createUI{
    
    self.contentView.backgroundColor =RGBCOLOR(246, 246, 246);
    
    headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    headView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:headView];
    
    //_line = [[ZSTECCRoundRectView alloc]initWithPoint:CGPointMake(0, 0) toPoint:CGPointMake(320, 40) borderWidth:1.0f borderColor:[UIColor lightGrayColor]];
    _line.alpha = 0.5;
    [headView addSubview:_line];
    
    self.headImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 50, 50)];
    [headView addSubview:self.headImage];
    
    self.name = [[UILabel alloc] initWithFrame:CGRectMake(50+15, 5, 60, 20)];
    self.name.textColor = [UIColor grayColor];
    [headView addSubview:self.name];
    
    self.time = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH-130, 5, 120, 30)];
    self.time.textColor = [UIColor grayColor];
    self.time.font = [UIFont systemFontOfSize:12];
    self.time.textAlignment = UITextAlignmentRight;
    [headView addSubview:self.time];
    
    
    UIView *starView = [[UIView alloc] initWithFrame:CGRectMake(60, 30, 250, 30)];
    [headView addSubview:starView];
    
    star1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    [star1 setImage:[UIImage imageNamed:@"model_ecomc_starGray.png"]];
    [starView addSubview:star1];
    
    star2 = [[UIImageView alloc] initWithFrame:CGRectMake(15, 0, 15, 15)];
    [star2 setImage:[UIImage imageNamed:@"model_ecomc_starGray.png"]];
    [starView addSubview:star2];
    
    star3 = [[UIImageView alloc] initWithFrame:CGRectMake(15*2, 0, 15, 15)];
    [star3 setImage:[UIImage imageNamed:@"model_ecomc_starGray.png"]];
    [starView addSubview:star3];
    
    star4 = [[UIImageView alloc] initWithFrame:CGRectMake(15*3, 0, 15, 15)];
    [star4 setImage:[UIImage imageNamed:@"model_ecomc_starGray.png"]];
    [starView addSubview:star4];
    
    star5 = [[UIImageView alloc] initWithFrame:CGRectMake(15*4, 0, 15, 15)];
    [star5 setImage:[UIImage imageNamed:@"model_ecomc_starGray.png"]];
    [starView addSubview:star5];
   
    
    _line = [[ZSTECCRoundRectView alloc]initWithPoint:CGPointMake(10, 60) toPoint:CGPointMake(300, 40) borderWidth:1.0f borderColor:[UIColor lightGrayColor]];
    _line.alpha = 0.5;
    [headView addSubview:_line];
    
    
    contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 65, 320, 135)];
    contentView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:contentView];
    
    self.reviewContent = [[ZSTECCAutoLabel alloc] init];
    self.reviewContent.frame = CGRectMake(10, 0, 300, self.labelHeight);
    self.reviewContent.textColor = [UIColor grayColor];
    self.reviewContent.lineBreakMode = UILineBreakModeWordWrap;
    self.reviewContent.numberOfLines = 0;
    self.reviewContent.font = [UIFont systemFontOfSize:15];
    [contentView addSubview:self.reviewContent];
    
    self.imageBtn = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10+70, 50, 50)];
//    [contentView addSubview:self.foodImages];
    
    
    contentView.frame = CGRectMake(0, 65, 320, 5+self.labelHeight+60);
    
}


-(void)setCellData:(NSDictionary *)dic{
    
    [self.headImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"headImage"]]]];
    self.name.text = [dic objectForKey:@"name"];
    self.time.text = [dic objectForKey:@"time"];
    [self.levelImages setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"levelImages"]]]];
    self.reviewContent.text = [dic objectForKey:@"reviewText"];
    
    self.labelHeight = [self.reviewContent getAttributedStringHeightWidthValue:320];//自动获取label的高度
    self.reviewContent.frame = CGRectMake(10, 0, 300, self.labelHeight);
//    self.foodImages.frame = CGRectMake(10, 10+self.labelHeight, 50, 50);
    contentView.frame = CGRectMake(0, 65, 320, 15+self.labelHeight+60);
    
    _imageArray = [dic objectForKey:@"imageArray"];
    
    for (int i=0; i<_imageArray.count; i++) {
        [self initFoodImages:i height:self.labelHeight imageUrl:_imageArray[i]];
    }
    
    int level =[[dic objectForKey:@"levelImages"] intValue];
    
    if (level == 1) {
        [star1 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
    }else if(level == 2){
        [star1 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star2 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
    }else if(level == 3){
        [star1 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star2 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star3 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
    }else if(level == 4){
        [star1 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star2 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star3 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star4 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
    }else if(level == 5){
        [star1 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star2 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star3 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star4 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star5 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
    }
    
    
    
}


-(void)initFoodImages:(int)num height:(float)height imageUrl:(NSString *)imageUrl{

    self.imageBtn = [[UIImageView alloc] init];
    self.imageBtn.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImage:)];
    [self.imageBtn addGestureRecognizer:singleTap1];
    self.imageBtn.frame = CGRectMake(10+60*num, 10+self.labelHeight, 50, 50);
   [self.imageBtn setImageWithURL:[NSURL URLWithString:imageUrl]];
    
    [contentView addSubview:self.imageBtn];
}

-(void)clickImage:(UIImageView *)btn{
    
    [_delegate clickImage:btn imageArray:_imageArray];
}



@end
