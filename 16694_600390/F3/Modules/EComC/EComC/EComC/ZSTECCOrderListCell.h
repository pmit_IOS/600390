//
//  ZSTECCOrderListCell.h
//  EComC
//
//  Created by qiuguian on 8/10/15.
//  Copyright (c) 2015 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTECCOrderListCellDelegate <NSObject>

-(void)orderInfoAction:(NSString *)orderId;

-(void)goToEvaluate:(NSString *)orderId;

@end

@interface ZSTECCOrderListCell : UITableViewCell

@property (nonatomic, strong) UILabel *orderCode;        //订单号
@property (nonatomic, strong) UILabel *time;             //时间
@property (nonatomic, strong) UILabel *shopName;         //店铺名
@property (nonatomic, strong) UILabel *bookType;         //订座类型
@property (nonatomic, strong) UILabel *totalMoney;       //总价格
@property (nonatomic) BOOL isPay;                        //true 为已支付， false 为未支付
@property (nonatomic, strong) UIButton *cancellBtn;      //取消按钮
@property (nonatomic, strong) UIButton *payBtn;          //付款按钮
@property (nonatomic, strong) UIButton *reviewBtn;       //评价按钮


@property (nonatomic,assign) id<ZSTECCOrderListCellDelegate> delagate;


-(void)createUI;                                          //创建cell视图
-(void)setCellData:(NSMutableDictionary *)dic;            //设置数据

@end
