//
//  ZSTECCOrderProductCell.m
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCOrderProductCell.h"

@implementation ZSTECCOrderProductCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.orderFoodNameLB)
    {
        self.orderFoodNameLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, (WIDTH - 30) / 3, 30)];
        self.orderFoodNameLB.font = [UIFont systemFontOfSize:14.0f];
        self.orderFoodNameLB.textColor = RGBA(85, 85, 85, 1);
        self.orderFoodNameLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.orderFoodNameLB];
        
        self.orderFoodCountLB = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH - 30) / 3 + 15, 10, (WIDTH - 30) / 3 , 30)];
        self.orderFoodCountLB.textAlignment = NSTextAlignmentCenter;
        self.orderFoodCountLB.textColor = RGBA(85, 85, 85, 1);
        self.orderFoodCountLB.font = [UIFont systemFontOfSize:14.0f];
        [self.contentView addSubview:self.orderFoodCountLB];
        
        self.orderFoodPriceLB = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH - 30) / 3 * 2 + 15, 10, (WIDTH - 30) / 3, 30)];
        self.orderFoodPriceLB.textAlignment = NSTextAlignmentRight;
        self.orderFoodPriceLB.font = [UIFont systemFontOfSize:14.0f];
        self.orderFoodPriceLB.textColor = RGBA(85, 85, 85, 1);
        [self.contentView addSubview:self.orderFoodPriceLB];
    }
}

- (void)setCellDataWithFoodName:(NSString *)foodName FoodCount:(NSString *)foodCount FoodPrice:(NSString *)foodPrice
{
    self.orderFoodNameLB.text = @"红烧鸡翅";
    self.orderFoodCountLB.text = @"X2";
    self.orderFoodPriceLB.text = @"￥20";
}

@end
