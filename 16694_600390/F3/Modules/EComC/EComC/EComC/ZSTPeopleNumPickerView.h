//
//  ZSTPeopleNumPickerView.h
//  EComC
//
//  Created by pmit on 15/8/19.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTPeopleNumPickerViewDelegate <NSObject>

- (void)sureSelected:(NSInteger)count;
- (void)cancelSeleted;

@end

@interface ZSTPeopleNumPickerView : UIView <UIPickerViewDataSource,UIPickerViewDelegate>

@property (assign,nonatomic) NSInteger maxCount;
@property (weak,nonatomic) id<ZSTPeopleNumPickerViewDelegate> peopleDelegate;
@property (assign,nonatomic) NSInteger seletedCount;

- (void)createPickerView:(CGRect)frame;

@end
