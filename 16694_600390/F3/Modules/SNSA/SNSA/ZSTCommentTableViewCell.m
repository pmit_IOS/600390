//
//  ZSTCommentTableViewCell.m
//  YouYun
//
//  Created by luobin on 6/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <CoreText/CoreText.h>

#import "ZSTCommentTableViewCell.h"
#import "ZSTContactDetailController.h"
#import "CommonFunc.h"
#import "NSDate+NSDateEx.h"
#import "VoiceConverter.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTCommentTableViewCell

@synthesize avtarImageView;
@synthesize comment;
@synthesize circle;
@synthesize audioPlayer;
@synthesize _label;
@synthesize lineView;
@synthesize soundView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(10, 10, 36, 36)] autorelease];
        self.avtarImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 36, 36)] autorelease];
        self.avtarImageView.userInteractionEnabled = YES;
//        avtarImageView.defaultImage = ZSTModuleImage(@"module_snsa_default_avatar.png");
        self.avtarImageView.backgroundColor = [UIColor whiteColor];
        self.avtarImageView.image = ZSTModuleImage(@"module_snsa_default_avatar.png");
//        self.avtarImageView.needCropper = YES;
//        [self.avtarImageView addTarget:self action:@selector(openUserInfo:) forControlEvents:UIControlEventTouchUpInside];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUserMessage:)];
        self.avtarImageView.userInteractionEnabled = YES;
        [self.avtarImageView addGestureRecognizer:tap];
        self.avtarImageView.tag = 10;
        CALayer * avtarImageViewLayer = [self.avtarImageView layer];
        [avtarImageViewLayer setMasksToBounds:YES];
        [avtarImageViewLayer setCornerRadius:5.0];
        [self addSubview:self.avtarImageView];
        
        self._label = [[NIAttributedLabel alloc] init];
        _label.backgroundColor = [UIColor clearColor];
//        _label.autoDetectLinks = YES;
        _label.tag = 11;
        [self addSubview:_label];
        
        self.lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        lineView.backgroundColor = [UIColor colorWithRed:0.83 green:0.83 blue:0.83 alpha:1];
        lineView.tag = 12;
        [self addSubview:lineView];
        
        self.soundView = [[UIView alloc] initWithFrame:CGRectMake(210, 5, 47, 34)];
        self.soundView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.soundView];
    }
    return self;
}

- (void)openUserInfo:(TKAsynImageView *)avtarImageView
{
//    ZSTContactDetailController *groupChatController = [[ZSTContactDetailController alloc] init];
//    groupChatController.userID = [comment objectForKey:@"UID"];
//    groupChatController.circle = self.circle;
//    [self.viewController.navigationController pushViewController:groupChatController animated:YES];
//    [groupChatController release];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc
{
    self.avtarImageView = nil;
    self.comment = nil;
    self.circle = nil;
    self.lineView = nil;
    TKRELEASE(_label);
    [super dealloc];
}

- (void)setComment:(NSDictionary *)theComment
{
//    if (comment != theComment) { //影响数据的正确显示
        [comment release];
        comment = [theComment retain];
    
        NSString *fileUrl = [comment safeObjectForKey:@"VideoUrl"];
        if (fileUrl==nil || [@"" isEqualToString:fileUrl]) {
            NSString *fileKey = [comment safeObjectForKey:@"New_UAvatar"];
//            [self.avtarImageView clear];
            if (fileKey != nil && ![fileKey isEqualToString:@""]) {
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",fileKey]];
//                self.avtarImageView.url = url;
//                [self.avtarImageView loadImage];
                [self.avtarImageView setImageWithURL:url placeholderImage:ZSTModuleImage(@"module_snsa_default_avatar.png")];
            }
            NSString *names = [[CommonFunc sharedCommon] trimStr:[comment safeObjectForKey:@"New_UName"]];
            NSString *uName = @"";
            if ([self isPureInt:names] && names.length >= 11)
            {
                uName = [names stringByReplacingCharactersInRange:NSMakeRange(5, 4) withString:@"****"];
            }
            else
            {
                uName = names;
            }
            NSString *content = [comment safeObjectForKey:@"MContent"];
            
            NSString *pContent = [[CommonFunc sharedCommon] getDisplayStr:content];

            _label.attributedString = [ZSTCommentTableViewCell attributedStringFromComment:comment content:pContent];
            
            NSArray *originArr = [[CommonFunc sharedCommon] getFaceOrigin:[NSString stringWithFormat:@"%@: %@",uName,content] maxWidth:250 hasPic:NO];

            for (int i=0; i<[originArr count]; i++) {
                NSDictionary *dic = [originArr objectAtIndex:i];
                if (dic) {
                    NSInteger x = [[dic safeObjectForKey:@"l"] integerValue];
                    NSInteger y = [[dic safeObjectForKey:@"h"] integerValue];
                    UIImageView *face = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, 18, 18)];
                    face.image = [UIImage imageNamed:[dic safeObjectForKey:@"n"]];
                    [_label addSubview:face];
                    [face release];
                }
            }
        } else { //语音
            NSAttributedString *attributedString = [self getVoiceLabelString:comment isSmall:NO];
            _label.attributedString = attributedString;

            if([fileUrl rangeOfString:@".wav"].length>0 || [fileUrl rangeOfString:@".amr"].length>0){ //如果有内容，数据正确
                
                NSString *fileKey = [comment safeObjectForKey:@"New_UAvatar"];
                //            [self.avtarImageView clear];
                if (fileKey != nil && ![fileKey isEqualToString:@""]) {
                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",fileKey]];
                    //                self.avtarImageView.url = url;
                    //                [self.avtarImageView loadImage];
                    [self.avtarImageView setImageWithURL:url placeholderImage:ZSTModuleImage(@"module_snsa_default_avatar.png")];
                }
                
                UIImage *image = ZSTModuleImage(@"module_snsa_btn_sound_bg.png");
                CGFloat capWidth = image.size.width / 2;
                CGFloat capHeight = image.size.height / 2;
                
                UIImageView *soundbgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 47, 34)];
                soundbgImg.image = [image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
                soundbgImg.tag = 9588;
                [self.soundView addSubview:soundbgImg];
                
                UIImageView *voiceView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 18, 13)];
                voiceView.image = ZSTModuleImage(@"module_snsa_chat_sound_play_3.png");
                voiceView.tag = 95888;
                [self.soundView addSubview:voiceView];
                
                ZSTTagUITapGestureRecognizer *voiceTap = [[ZSTTagUITapGestureRecognizer alloc] initWithTarget:self action:@selector(playVoice:)];
                voiceTap.fileName = [comment safeObjectForKey:@"VideoUrl"];
                voiceTap.numberOfTapsRequired = 1;
                voiceTap.imageView = voiceView;
                voiceView.userInteractionEnabled = YES;
                [soundbgImg addGestureRecognizer:voiceTap];
                [self.soundView addGestureRecognizer:voiceTap];
                
                [soundbgImg release];
                [voiceView release];
                [voiceTap release];
                
                UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(52, 5, 25, 15)];
                time.textAlignment = NSTextAlignmentCenter;
                time.text = [NSString stringWithFormat:@"%@\"",[comment safeObjectForKey:@"VideoTimeLength"]];
                time.backgroundColor = [UIColor clearColor];
                time.textColor = [UIColor colorWithWhite:0.7 alpha:1];
                time.font = [UIFont systemFontOfSize:11];
                time.tag = 958888;
                [self.soundView addSubview:time];
                [time release];
            }
        }
//    }
}

-(void)playVoice:(id)sender { //用button不好搞动画
    ZSTTagUITapGestureRecognizer *tap = (ZSTTagUITapGestureRecognizer*)sender;
    
    BOOL same = NO; //两次点的是同一个文件
    if ([self.audioPlayer isPlaying]) {
        [self.audioPlayer stop];
        [self.audioPlayer.imageView stopAnimating];
        if ([tap.fileName isEqualToString:audioPlayer.playingFile]) {
            same = YES;
        } else {
            self.audioPlayer = nil;
        }
    }
    
    if (!tap.imageView.animationImages) {
        tap.imageView.animationImages = [NSArray arrayWithObjects:
                                         ZSTModuleImage(@"module_snsa_chat_sound_play_0.png"),
                                         ZSTModuleImage(@"module_snsa_chat_sound_play_1.png"),
                                         ZSTModuleImage(@"module_snsa_chat_sound_play_2.png"),
                                         ZSTModuleImage(@"module_snsa_chat_sound_play_3.png"),nil];
        tap.imageView.animationDuration = 1.0;
        tap.imageView.animationRepeatCount = 0; //暂时智能控制吧
    }
    
    //检查wav文件是否存在
    NSString *fileName = [tap.fileName substringToIndex:(tap.fileName.length-4)];
     NSArray *strArray = [fileName componentsSeparatedByString:@"/"];
//    NSString *urlstr = @"http://mod.pmit.cn/SNSA/resource/video/";
//    NSString *tmpStr = [fileName substringFromIndex:urlstr.length];
//    NSRange range = NSMakeRange(urlstr.length-1, tmpStr.length);
//    NSString *filename = [tap.fileName substringWithRange:range];
    NSString *filename = [strArray lastObject];
    NSString *wavFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.wav",filename]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL wavExist = [fileManager fileExistsAtPath:wavFile];

    NSData *wavData;
    NSError *error;
    if (!wavExist) {
        //    文件下载
        BOOL netExist = [[CommonFunc sharedCommon] isExistNetWork];
        if (!netExist) {
            return;
        }
        NSURL *fileUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",tap.fileName]];
        NSData *spxData = [NSData dataWithContentsOfURL:fileUrl];
        //    写入本地
        
        NSString *spxFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.amr",filename]];
        BOOL istest = [spxData writeToFile:spxFile atomically:YES];
        NSLog(@"%d",istest);
        //检测压缩语音文件是否存在
        if (![fileManager fileExistsAtPath:spxFile]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示信息"
                                                            message:@"语音文件不存在!"
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"确定", nil];
            [alert show];
            [alert release];
            return;
        }
        //        if ([tap.fileName rangeOfString:@"spx"].length>0) {
        //            SpeexDecoder *decoder = [[SpeexDecoder alloc] init];
        //            [decoder decodeInFilePath:spxFile outFilePath:wavFile];
        //            [decoder release];
        //        } else if([tap.fileName rangeOfString:@"amr"].length>0){
        //            [VoiceConverter amrToWav:spxFile];//这个解码有点慢，第一次播放有问题
        //
        //        }
        
        if([tap.fileName rangeOfString:@"amr"].length>0){
            
            [VoiceConverter amrToWav:spxFile wavSavePath:wavFile];//这个解码有点慢，第一次播放有问题
            
        }
        //        wavFile = spxFile;
    }
    
    wavData = [NSData dataWithContentsOfFile:wavFile];
    
    
    if (!self.audioPlayer) {
        self.audioPlayer = [[ImageAudioPlayer alloc] initWithData:wavData error:&error];
        if (!audioPlayer) {
            NSLog(@"Error: %@", [error localizedDescription]);
        }
        audioPlayer.delegate = self;  // 设置代理
        audioPlayer.numberOfLoops = 0;// 不循环播放
        audioPlayer.imageView = tap.imageView;
    }
    
    if (![self.audioPlayer isPlaying] && !same) {//播放
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayback error:nil];
        audioPlayer.playingFile = tap.fileName;
        [audioPlayer prepareToPlay];
        [audioPlayer play];
        [audioPlayer.imageView startAnimating];
    }
}
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [self.audioPlayer.imageView stopAnimating];
    self.audioPlayer = nil;
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error{
    [self.audioPlayer.imageView stopAnimating];
    self.audioPlayer = nil;
}


-(NSAttributedString *)getVoiceLabelString:(NSDictionary *)dic isSmall:(BOOL)isSmall{
    NSString *userName = [[CommonFunc sharedCommon] trimStr:[dic safeObjectForKey:@"New_UName"]];
    if ([self isPureInt:userName] && userName.length >= 11)
    {
        userName = [userName stringByReplacingCharactersInRange:NSMakeRange(5, 4) withString:@"****"];
    }
    NSString *content = @"发表语音";
    
    double time = [[dic safeObjectForKey:@"AddTime"] doubleValue];
//    NSString *timeStr = [TKUtil timeIntervalSince1970:time];
    NSDate* createdAt = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [NSDate dateStringWithDate:createdAt];
    
    NSMutableAttributedString *attributedString = [[[NSMutableAttributedString alloc] init] autorelease];
    NSAttributedString *nameAttributedString;
    NSAttributedString *timeAttributedString;
    if (isSmall) {
        nameAttributedString = [[NSAttributedString alloc] initWithString:userName?[NSString stringWithFormat:@"%@: ", userName]:@" : " attributes:smallNameAttributes()];
        timeAttributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",content, timeStr] attributes:smallTimeAttributes()];
    } else {
        nameAttributedString = [[NSAttributedString alloc] initWithString:userName?[NSString stringWithFormat:@"%@: ", userName]:@" : " attributes:nameAttributes()];
        timeAttributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",content, timeStr] attributes:timeAttributes()];
    }
    [attributedString appendAttributedString:nameAttributedString];
    [attributedString appendAttributedString:timeAttributedString];
    
    [nameAttributedString release];
    [timeAttributedString release];
    
    return attributedString;
}
- (void)layoutSubviews
{
    CGSize size = [_label sizeThatFits:CGSizeMake(250, CGFLOAT_MAX)];

    if(CGRectEqualToRect(_label.frame, CGRectZero)) {
        _label.frame = CGRectMake(56, 10, 250, size.height);
    }
//    self.lineView.frame = CGRectMake(0, size.height, 320, 1);
//    self.backgroundView.frame = self.bounds;
    
}

+ (NSAttributedString *)attributedStringForName:(NSString*)userName
{
    NSAttributedString *nameAttributedString = [[NSAttributedString alloc] initWithString:userName attributes:nameAttributes()];
    return [nameAttributedString autorelease];
}

+ (NSAttributedString *)attributedStringFromComment:(NSDictionary *)comment content:(NSString*)content
{
    NSString *userName = [[CommonFunc sharedCommon] trimStr:[comment safeObjectForKey:@"New_UName"]];
//    NSString *content = [comment objectForKey:@"MContent"];
//    content = [content stringByTrimmingTrailingWhitespaceAndNewlineCharacters];
    if (content == nil || [content isEqualToString:@""]) {
        content = @" ";
    }
    double time = [[comment safeObjectForKey:@"AddTime"] doubleValue];
//    NSString *timeStr = [TKUtil timeIntervalSince1970:time];
    NSDate* createdAt = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [NSDate dateStringWithDate:createdAt];
    
    NSMutableAttributedString *attributedString = [[[NSMutableAttributedString alloc] init] autorelease];
    
    NSAttributedString *nameAttributedString = [[NSAttributedString alloc] initWithString:userName?[NSString stringWithFormat:@"%@: ", userName]:@" : " attributes:nameAttributes()];
    [attributedString appendAttributedString:nameAttributedString];
    [nameAttributedString release];
    
    NSAttributedString *contentAttributedString = [[NSAttributedString alloc] initWithString:content attributes:contentAttributes()];
    [attributedString appendAttributedString:contentAttributedString];
    [contentAttributedString release];
    
    NSAttributedString *timeAttributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", timeStr] attributes:timeAttributes()];
    [attributedString appendAttributedString:timeAttributedString];
    [timeAttributedString release];
    
    return attributedString;
}

+ (CGSize)suggestSizeForComment:(NSDictionary *)comment;
{
    CFAttributedStringRef attributedStringRef = (CFAttributedStringRef)[self attributedStringFromComment:comment content:[comment safeObjectForKey:@"MContent"]];
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attributedStringRef);
	
	CGSize newSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0),
                                                                  NULL, CGSizeMake(250, CGFLOAT_MAX), nil);
	if (nil != framesetter) {
        CFRelease(framesetter);
        framesetter = nil;
    }
    return CGSizeMake(newSize.width, MAX(newSize.height, 36) + 20);
}

- (void)openUserMessage:(UITapGestureRecognizer *)tap
{
    
}

- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    NSInteger val;
    return [scan scanInteger:&val] && [scan isAtEnd];
}

@end
