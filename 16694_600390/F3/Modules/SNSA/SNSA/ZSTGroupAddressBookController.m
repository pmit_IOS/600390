//
//  ZSTGroupAddressBookController.m
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTGroupAddressBookController.h"
#import "ZSTGroupAddressBookTableviewCell.h"
#import "ZSTContactDetailController.h"
#import "ZSTUtils.h"

@implementation ZSTGroupAddressBookController

@synthesize engine;
@synthesize users;
@synthesize circle;
@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    self.engine = nil;
    self.users = nil;
    self.engine = nil;
    self.circle = nil;
    self.tableView = nil;
    self.searchBar = nil;
    self.resultArray = nil;
    self.dataArray = nil;
    [super dealloc];
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    UIView *topLine = [self.navigationController.navigationBar viewWithTag:1001];
//    [topLine removeFromSuperview];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.resultArray = [[NSMutableArray alloc] init];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, 320, 480 - 44 - 44 - 20 + (iPhone5?88:0)) style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.searchBar.backgroundColor = [UIColor whiteColor];
    self.searchBar.delegate = self;
	self.searchBar.placeholder = NSLocalizedString(@"输入查找内容" , nil);
	self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
	self.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.searchBar.keyboardType = UIKeyboardTypeDefault;
    [self.view addSubview:_searchBar];
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:[circle safeObjectForKey:@"CName"]];
  
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"退出", nil) target:self selector:@selector (quitCircle)];
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    self.engine.moduleType = self.moduleType;
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        [self.engine getUsersByCID:[circle safeObjectForKey:@"CID"]];
    }];
    
    self.disableViewOverlay = [[UIView alloc] initWithFrame:CGRectMake(0.0f,42.0f,320.0f,416.0f)];
    self.disableViewOverlay.backgroundColor=[UIColor blackColor];
    self.disableViewOverlay.alpha = 0;
    
   UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardWillHide)];
    [self.disableViewOverlay addGestureRecognizer:singleTap];
    [singleTap release];
}

- (void)keyboardWillHide
{
    [self.searchBar resignFirstResponder];
    [self.disableViewOverlay removeFromSuperview];
    [self.searchBar setShowsCancelButton:NO animated:YES];
}


- (void) quitCircle
{
    [self.engine quitCircle:[circle safeObjectForKey:@"CID"]];
}

-(void)quitCircleResponse
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil showHUD:self.view withText:@"退出成功！"];
    [TKUIUtil hiddenHUDAfterDelay:2];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)quitCircleFailed
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil showHUD:self.view withText:@"退出失败！"];
    [TKUIUtil hiddenHUDAfterDelay:2];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - ZSTYouYunEngineDelegate
- (void)getUsersByCIDResponse:(NSArray *)theUsers
{
    [self.view removeNetworkIndicatorView];
    
    self.users = theUsers;
    self.dataArray = self.users;
    [self.tableView reloadData];
}

- (void)requestDidFail:(NSError *)error method:(NSString *)method
{
    [self.view refreshFailed];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    ZSTContactDetailController *groupChatController = [[ZSTContactDetailController alloc] init];
    groupChatController.userInfo = [self.dataArray objectAtIndex:indexPath.row];
    groupChatController.hidesBottomBarWhenPushed = YES;
    groupChatController.leftBtn.enabled = YES;
    groupChatController.rightBtn.enabled = YES;
    [self.navigationController pushViewController:groupChatController animated:YES];
    [groupChatController release];
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"ZSTGroupAddressBookTableviewCell";
    ZSTGroupAddressBookTableviewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[ZSTGroupAddressBookTableviewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
    }
    cell.userData = [self.dataArray objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark -
#pragma mark UISearchBarDelegate Methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText && ![searchText isEqualToString:@""]) {
    
        [self retrievedResult:searchText];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self searchBar:self.searchBar activate:YES];
    
    for(id cc in [self.searchBar subviews])
        
    {
        if([cc isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)cc;
            [btn setTitle:NSLocalizedString(@"取消",@"取消") forState:UIControlStateNormal];
            [btn setTintColor:[UIColor lightGrayColor]];
        }
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searchBar.text = @"";
    [self.resultArray removeAllObjects];
    [self searchBar:self.searchBar activate:NO];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self searchBar:self.searchBar activate:NO];
}

- (void)searchBar:(UISearchBar *)searchBar activate:(BOOL) active
{
    self.tableView.allowsSelection = !active;
    self.tableView.scrollEnabled = !active;
    if (!active) {
        [self.disableViewOverlay removeFromSuperview];
        [self.searchBar resignFirstResponder];
        [self retrievedResult:searchBar.text];
    } else {
        self.disableViewOverlay.alpha = 0;
        [self.view addSubview:self.disableViewOverlay];
        
        [UIView beginAnimations:@"FadeIn" context:nil];
        [UIView setAnimationDuration:0.5];
        self.disableViewOverlay.alpha = 0.6;
        [UIView commitAnimations];
        
        NSIndexPath *selected = [self.tableView indexPathForSelectedRow];
        if (selected) {
            [self.tableView deselectRowAtIndexPath:selected animated:NO];
        }
    }
    
    [self.searchBar setShowsCancelButton:active animated:YES];
}

- (void) retrievedResult:(NSString *)key
{
    [self.resultArray removeAllObjects];
    NSString *UserNameStr;
    NSString *Msisdn;
    for (NSDictionary *dic in users) {
        
        UserNameStr = [dic safeObjectForKey:@"UserName"];
        Msisdn = [dic safeObjectForKey:@"Msisdn"];
        if ([UserNameStr rangeOfString:key].location != NSNotFound || [Msisdn rangeOfString:key].location != NSNotFound) {
            
            [self.resultArray addObject:dic];
        }
    }
    
    if (self.resultArray.count > 0) {
        
        self.dataArray = self.resultArray;
    } else {
        
        self.dataArray = self.users;
    }
    
    [self.tableView reloadData];
}


@end
