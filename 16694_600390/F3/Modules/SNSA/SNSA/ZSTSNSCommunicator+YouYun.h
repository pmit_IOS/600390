//
//  Category.h
//  YouYun
//
//  Created by luobin on 6/5/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

extern NSString *const ZSTRequestErrorDomain;

//#ifndef URL_SERVER_BASE_PATH
extern NSString *const URL_SERVER_BASE_PATH; //default
//#endif

extern NSString *const URL_SERVER_PATH_UPLOAD_FILE;

extern NSString *const URL_SERVER_PATH_UPLOAD_VIDEO;

extern NSString *const URL_SERVER_PATH_GET_FILE;

extern NSString *const  URL_SERVER_PATH_APP_API;

/////////////////////////////////////////////////////// http method  /////////////////////////////////////////////////////////

extern NSString *const  ZSTHttpMethod_AddCircle;            //添加圈子
extern NSString *const  ZSTHttpMethod_GetCircles;           //获取圈子列表
extern NSString *const  ZSTHttpMethod_GetUsersByCID;        //获取圈子成员名单
extern NSString *const  ZSTHttpMethod_QuitCircle;           //退出圈子
extern NSString *const  ZSTHttpMethod_SendMessage;          //发表消息
extern NSString *const  ZSTHttpMethod_GetMessages;          //获取圈子消息列表
extern NSString *const  ZSTHttpMethod_GetUserByUID;         //获取个人信息
extern NSString *const  ZSTHttpMethod_UpdateUserByUID;      //修改个人名片信息
extern NSString *const  ZSTHttpMethod_UserRegClient;        //注册
extern NSString *const  ZSTHttpMethod_SendVerifyCode;       //发送验证码接口
extern NSString *const  ZSTHttpMethod_GetCommentsByMID;     //获取评论列表

#define ZSTHttpMethod_JionCircles  @"JoinCircles"

#define ZSTSNSA_REPORT @"http://bo.pmit.cn/Mod/BackOffice/web/SNSA/Report.html"