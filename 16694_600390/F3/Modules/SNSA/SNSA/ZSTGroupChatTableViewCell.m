//
//  ZSTGroupChatTableViewCell.m
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTGroupChatTableViewCell.h"
#import "ZSTChatBubble.h"
#import "ZSTContactDetailController.h"
#import "ZSTGroupChatroomController.h"

#define kZSTMessageMaxHeight 600

@implementation ZSTGroupChatTableViewCell

@synthesize avtarImageView;
@synthesize pictureImageView;
@synthesize chatBubble;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.autoresizesSubviews = NO;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(3.5, 10, 45, 45)] autorelease];
        avtarImageView.defaultImage = ZSTModuleImage(@"module_snsa_default_avatar.png");
        [avtarImageView addTarget:self action:@selector(openUserInfo) forControlEvents:UIControlEventTouchUpInside];
        avtarImageView.layer.borderWidth = 2;
        avtarImageView.layer.borderColor =[ [UIColor whiteColor] CGColor];
        [self addSubview:avtarImageView];
        
        self.pictureImageView = [[[TKAsynImageView alloc] init] autorelease];
        self.pictureImageView.backgroundColor = [UIColor clearColor];
        self.pictureImageView.adorn = [ZSTModuleImage(@"module_snsa_chat_img_frame.png") stretchableImageWithLeftCapWidth:4 topCapHeight:6];
        self.pictureImageView.imageView.zoomDelegate = self;
        self.pictureImageView.imageInset = UIEdgeInsetsMake(3, 4, 5, 4);
        self.pictureImageView.adjustsImageWhenHighlighted = NO;
        
        UITapGestureRecognizer *singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
        singleRecognizer.numberOfTapsRequired = 1;
        
        UITapGestureRecognizer *doubleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:nil action:nil];
        doubleRecognizer.numberOfTapsRequired = 2;
        [singleRecognizer requireGestureRecognizerToFail:doubleRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        longPressGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
        longPressGestureRecognizer.cancelsTouchesInView = NO;
        [singleRecognizer requireGestureRecognizerToFail:longPressGestureRecognizer];
        
        [self.pictureImageView addGestureRecognizer:longPressGestureRecognizer];
        [self.pictureImageView addGestureRecognizer:singleRecognizer];
        [self.pictureImageView addGestureRecognizer:doubleRecognizer];
        self.pictureImageView.userInteractionEnabled = YES;
        
        [longPressGestureRecognizer release];
        [singleRecognizer release];
        [doubleRecognizer release];
        
        [self addSubview:self.pictureImageView];
        
        self.chatBubble = [[[ZSTChatBubble alloc] init] autorelease];
        self.chatBubble.backgroundColor = [UIColor clearColor];
        self.chatBubble.font = [UIFont systemFontOfSize:15];
        [self addSubview:self.chatBubble];
    }
    return self;
}

- (void)openUserInfo
{
    ZSTGroupChatroomController *vc = (ZSTGroupChatroomController *)self.viewController;
    if (vc.circle != nil) {
        NSString *defaultCircle = [vc.circle safeObjectForKey:@"DefaultCircle"];
        if (defaultCircle!=nil && 1==[defaultCircle integerValue]) {
            return;
        }
    }
    ZSTContactDetailController *groupChatController = [[ZSTContactDetailController alloc] init];
    groupChatController.userID = [self.object safeObjectForKey:@"UID"];
    [self.viewController.navigationController pushViewController:groupChatController animated:YES];
    [groupChatController release];
}

- (CGSize)displayRectForImage:(CGSize)imageSize
{
     CGRect screenRect = TKScreenBounds();
    if (imageSize.width/ imageSize.height >= screenRect.size.width/ screenRect.size.height) {
        CGFloat height = screenRect.size.width * imageSize.height / imageSize.width;
        return CGSizeMake(screenRect.size.width, height);
    } else {
        CGFloat width = screenRect.size.height * imageSize.width / imageSize.height;
        return CGSizeMake(width, screenRect.size.height);
    }
}

+ (CGSize)reSetImageSize:(CGSize)imageSize
{
    CGRect screenRect = CGRectMake(0, 0, 245, imageSize.height);
    
    if (imageSize.width > screenRect.size.width) {
        CGFloat height = imageSize.height * screenRect.size.width / imageSize.width;
        CGFloat width = screenRect.size.width;
        if (height > screenRect.size.height) {
            width = width * screenRect.size.height/height;
            height = screenRect.size.height;
        }
        return CGSizeMake(width, height);
    } else {
        return CGSizeMake(imageSize.width, imageSize.height); 
    }
}

- (void)singleTap
{
     NSString *videoURL = [self.object safeObjectForKey:@"VideoUrl"];
     NSString *imageURL = [self.object safeObjectForKey:@"ImgUrl"];
    
    if (videoURL != nil && ![videoURL isEmptyOrWhitespace]) {
        
        [self playVideo];
    }
    
    if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
        
        self.pictureImageView.imageView.zoomedSize = [self displayRectForImage:self.pictureImageView.frame.size];
        
        self.pictureImageView.imageView.wrapInScrollviewWhenZoomed = YES;
        [self.pictureImageView.imageView zoomIn];
    }
}

-(void)playVideo
{
    NSString *attachmentPath = [self.object safeObjectForKey:@"VideoUrl"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@", @"PlayVideo"] object:attachmentPath userInfo:nil];
}


- (void)longPress:(UILongPressGestureRecognizer *)gesture 
{
    
    NSString *videoURL = [self.object safeObjectForKey:@"VideoUrl"];
    NSString *imageURL = [self.object safeObjectForKey:@"ImgUrl"];
    
    if (videoURL != nil && ![videoURL isEmptyOrWhitespace]) {
        
        return;
    }
    
     if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
        if (gesture.state==UIGestureRecognizerStateBegan || gesture.state == UIGestureRecognizerStateChanged) {
        
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil 
                                                                     delegate:self 
                                                            cancelButtonTitle:NSLocalizedString(@"取消",@"取消")
                                                       destructiveButtonTitle:nil 
                                                            otherButtonTitles:NSLocalizedString(@"保存到本地", nil), nil];
            actionSheet.actionSheetStyle = UIBarStyleBlackOpaque;
            [actionSheet showInView:self];
            [actionSheet release];
        }
     }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    
    if ([touch.view isKindOfClass:[UIButton class]]) {      //change it to your condition
        return NO;
    }
    return YES;
}

#pragma mark - MTZoomWindowDelegate
- (void)zoomWindowLongPress:(MTZoomWindow *)zoomWindow {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil 
                                                             delegate:self 
                                                    cancelButtonTitle:NSLocalizedString(@"取消",@"取消")
                                               destructiveButtonTitle:nil 
                                                    otherButtonTitles:NSLocalizedString(@"保存到本地", nil), nil];
    actionSheet.actionSheetStyle = UIBarStyleBlackOpaque;
    [actionSheet showInView:self];
    [actionSheet release];
    
}


#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        UIView *currentView = nil;
       
        UIImageWriteToSavedPhotosAlbum(self.pictureImageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);  
        if (self.pictureImageView.imageView.isZoomedIn) {
            currentView = self.pictureImageView.imageView.window;
        } else {
            currentView = self;
        }
        
        [TKUIUtil showHUDInView:currentView
                       withText:NSLocalizedString(@"正在保存", @"正在保存") 
                      withImage:nil 
                     withCenter:CGPointMake(160, 175)];

    } 
}

- (void) image:(UIImage *) image didFinishSavingWithError:(NSError *) error contextInfo:(void *)contextInfo 
{
    UIView *currentView = nil;
    
    if (error != NULL) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"保存失败，请重新操作！", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];

    } else {
        
        if (self.pictureImageView.imageView.isZoomedIn) {
            currentView = self.pictureImageView.imageView.window;
        } else {
            currentView = self;
        }
        
        [TKUIUtil showHUDInView:currentView withText:NSLocalizedString(@"保存成功!", nil) withImage:[UIImage imageNamed:@"icon_smile_face.png"] withCenter:CGPointMake(160, 175)];
         [TKUIUtil hiddenHUDAfterDelay:2];
    }
}

 - (void)didMoveToSuperview
{
    [super didMoveToSuperview];
    
    ZSTGroupChatroomController *vc = (ZSTGroupChatroomController *)self.viewController;
    if (vc.circle != nil) {
        NSString *defaultCircle = [vc.circle safeObjectForKey:@"DefaultCircle"];
        if (defaultCircle!=nil && 1==[defaultCircle integerValue]) {
            avtarImageView.adjustsImageWhenHighlighted = NO;
        }
    } 
}

+ (CGSize)getImageSize:(NSString *)imageUrl
{
    NSRange rangePre = [imageUrl rangeOfString:@"filename="];
    NSRange rangeSuf = [imageUrl rangeOfString:@".png"];
    NSString *subStr = nil;
    NSString *width = nil;
    NSString *height = nil;
    
    if( rangePre.location != NSNotFound  && rangeSuf.location != NSNotFound) {
        
        NSInteger location = rangePre.location+rangePre.length;
        NSInteger length = rangeSuf.location-location;
        
        NSRange range = NSMakeRange(location, length);
        
        subStr = [imageUrl substringWithRange:range];
        
        NSArray *array = [subStr componentsSeparatedByString:@"_"];
        subStr = [array lastObject];
        
        NSRange heightPre = [subStr rangeOfString:@"x"];

        if( heightPre.location != NSNotFound) {
            
            NSInteger widthLoc = 0;
            NSInteger widthLen = heightPre.location-widthLoc;
            NSRange widthRange = NSMakeRange(widthLoc, widthLen);
            
            NSInteger heightLoc = heightPre.location+1;
            NSInteger heighten = [subStr length]-heightLoc;
            NSRange heightRange = NSMakeRange(heightLoc, heighten);
            
            width = [subStr substringWithRange:widthRange];
            height = [subStr substringWithRange:heightRange];
            
        }
    }
    
    if ([width length] == 0 || width == nil) {
        width = @"245";
    }
    if ([height length] == 0 || width == nil) {
        height = @"245";
    }
    
    CGSize imageSize = [ZSTGroupChatTableViewCell reSetImageSize:CGSizeMake([width intValue], [height intValue])];
    return imageSize;
}

- (void)objectUpdate
{
    [self.avtarImageView clear];
    NSString *fileKey = [self.object safeObjectForKey:@"UAvatar"];
    if (fileKey == nil) {
        fileKey = @"";
    }
    
    NSURL *url = [NSURL URLWithString:fileKey];
    [self.avtarImageView setUrl:url];
    [self.avtarImageView loadImage];

    
    NSString *imageURL = [self.object safeObjectForKey:@"ImgUrl"];
    NSString *videoURL = [self.object safeObjectForKey:@"VideoUrl"];
    [self.pictureImageView clear];
    if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
        CGSize imageSize = [ZSTGroupChatTableViewCell getImageSize:imageURL];
        self.pictureImageView.url = [NSURL URLWithString:imageURL];
        self.pictureImageView.defaultImage = ZSTModuleImage(@"topic_tmp_image.png");
        [self.pictureImageView performSelector:@selector(loadImage) withObject:nil afterDelay:0];
        self.pictureImageView.frame = CGRectMake(58, 10, imageSize.width, imageSize.height);
        self.pictureImageView.hidden = NO;
    }  else if (videoURL != nil && ![videoURL isEmptyOrWhitespace]) {
//        CGSize imageSize = [ZSTGroupChatTableViewCell getImageSize:videoURL];
//        self.pictureImageView.url = [NSURL URLWithString:videoURL];
        self.pictureImageView.defaultImage = [UIImage imageNamed:@"voice_attachmet_icon.png"];
//        [self.pictureImageView performSelector:@selector(loadImage) withObject:nil afterDelay:0];
        self.pictureImageView.frame = CGRectMake(58, 10, 32, 36);
        self.pictureImageView.hidden = NO;
    }  else {
        self.pictureImageView.frame = CGRectZero;
        self.pictureImageView.hidden = YES;
    }
    
    self.chatBubble.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 5, 
                                     CGRectGetMaxY(self.pictureImageView.frame) + 10, 250, 
                                     [ZSTChatBubble suggestSizeForTopic:self.object font:[UIFont systemFontOfSize:15]].height);
    [self.chatBubble setTopic:self.object];
}


+ (CGFloat)tableView:(UITableView*)tableView rowHeightForObject:(id)object
{    
    CGFloat height = 10.0f;
    
    NSString *imageURL = [object safeObjectForKey:@"ImgUrl"];
    NSString *videoURL = [object safeObjectForKey:@"VideoUrl"];
    
    if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {

        CGSize imageSize = [ZSTGroupChatTableViewCell getImageSize:imageURL];
        height += (imageSize.height + 10);
    }
    
    if (videoURL != nil && ![videoURL isEmptyOrWhitespace]) {
        
//        CGSize imageSize = [ZSTGroupChatTableViewCell getImageSize:videoURL];
        CGSize imageSize = CGSizeMake(32, 36);
        height += (imageSize.height + 10);
    }
    
    height += [ZSTChatBubble suggestSizeForTopic:object font:[UIFont systemFontOfSize:15]].height;
    height += 10;
    
    return height;
}

- (void)drawRect:(CGRect)rect
{
    [[ZSTModuleImage(@"module_snsa_addressBook_list_cell_bg.png") stretchableImageWithLeftCapWidth:0 topCapHeight:29] drawInRect:rect];
}

- (void)dealloc
{
    self.avtarImageView = nil;
    self.pictureImageView = nil;
    [super dealloc];
}

- (CGSize)getImageSize:(NSString *)imageUrl
{
    NSRange rangePre = [imageUrl rangeOfString:@"filename="];
    NSRange rangeSuf = [imageUrl rangeOfString:@".png"];
    NSString *subStr = nil;
    NSString *width = nil;
    NSString *height = nil;
    
    if( rangePre.location != NSNotFound  && rangeSuf.location != NSNotFound) {
        
        NSInteger location = rangePre.location+rangePre.length;
        NSInteger length = rangeSuf.location-location;
        
        NSRange range = NSMakeRange(location, length);
        
        subStr = [imageUrl substringWithRange:range];
        
        NSArray *array = [subStr componentsSeparatedByString:@"_"];
        subStr = [array lastObject];
        
        NSRange heightPre = [subStr rangeOfString:@"x"];
        
        if( heightPre.location != NSNotFound) {
            
            NSInteger widthLoc = 0;
            NSInteger widthLen = heightPre.location-widthLoc;
            NSRange widthRange = NSMakeRange(widthLoc, widthLen);
            
            NSInteger heightLoc = heightPre.location+1;
            NSInteger heighten = [subStr length]-heightLoc;
            NSRange heightRange = NSMakeRange(heightLoc, heighten);
            
            width = [subStr substringWithRange:widthRange];
            height = [subStr substringWithRange:heightRange];
            
        }
    }
    
    if ([width length] == 0 || width == nil) {
        width = @"245";
    }
    if ([height length] == 0 || width == nil) {
        height = @"245";
    }
    
    CGSize imageSize = [ZSTGroupChatTableViewCell reSetImageSize:CGSizeMake([width intValue], [height intValue])];
    return imageSize;
}

@end
