//
//  ZSTYouYunModel.h
//  YouYun
//
//  Created by luobin on 7/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary(Topic)

@property (nonatomic, assign, readonly) int z_ID;
@property (nonatomic, retain, readonly) NSString *z_mID;
@property (nonatomic, retain, readonly) NSString *z_circleID;
@property (nonatomic, retain, readonly) NSString *z_userID;
@property (nonatomic, retain, readonly) NSString *z_content;
@property (nonatomic, retain, readonly) NSString *z_imgUrl;
@property (nonatomic, retain, readonly) NSString *z_vedioUrl;
@property (nonatomic, retain, readonly) NSDate *z_time;
@property (nonatomic, retain, readonly) NSString *z_name;

@end
