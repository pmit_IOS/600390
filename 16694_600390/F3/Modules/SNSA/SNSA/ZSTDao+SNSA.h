
#import <Foundation/Foundation.h>

@interface ZSTDao(SNSA)

- (void)createTableIfNotExistForSnsbModule;

- (BOOL)addMessage:(NSString *)mID
          circleID:(NSString *)circleID
            userID:(NSString *)userID
           content:(NSString *)content
            imgUrl:(NSString *)imgUrl
           addTime:(NSDate *)time;

- (BOOL)addTopic:(NSInteger)topicId
         addTime:(NSDate *)time
          imgUrl:(NSString *)imgUrl
        videoUrl:(NSString *)videoUrl
         content:(NSString *)topicContent
        circleId:(NSInteger)circleId
          userId:(NSInteger)userId
      userAvatar: (NSString *)userAvatar
        userName: (NSString *)userName;

- (BOOL)addComment:(NSInteger)commentId
           topicId:(NSInteger)topicId
           addTime:(NSDate *)time
            imgUrl:(NSString *)imgUrl
    commentContent:(NSString *)commentContent
          circleId:(NSInteger)circleId
            userId:(NSInteger)userId
        userAvatar:(NSString *)userAvatar
          userName:(NSString *)userName;

- (NSArray *)topicAndCommentOfCircle:(NSString *)circleId 
                             startId:(NSInteger)startTopic
                          topicCount:(NSInteger)topicCount
                        commentCount:(NSInteger)commentCount;


- (BOOL)deleteMessage:(NSString *)mID;

- (BOOL)messageExist:(NSString *)mID;

- (NSDictionary *)messagesOfCircle:(NSString *)circleID atIndex:(int)index;

- (NSArray *)messagesOfCircle:(NSString *)circleID  atPage:(int)pageNum pageCount:(int)pageCount;

- (NSUInteger)messagesCountOfCircle:(NSString *)circleID;

- (NSString *) getMaxTopicID:(NSString *)circleID;

- (BOOL)deleteTopicOfCircle:(NSString *)circleId;

- (BOOL)deleteCommentOfCircle: (NSString *)circleId;

- (BOOL)saveComments:(NSArray *)comments circle:(NSString *)circleId;

@end
