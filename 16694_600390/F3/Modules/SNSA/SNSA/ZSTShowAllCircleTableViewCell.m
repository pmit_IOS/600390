//
//  ZSTShowAllCircleTableViewCell.m
//  SNSA
//
//  Created by zhangwanqiang on 14-5-4.
//
//

#import "ZSTShowAllCircleTableViewCell.h"
#import "TKAsynImageView.h"
#import "ZSTUnreadMessageCountLabel.h"

#define AVTARBGSIZE  ((109/2.0))
#define AVTARSIZE    ((101/2.0))



@implementation ZSTShowAllCircleTableViewCell
{

    UIView *rightJionedView;
    UIView *rightWillJionView;
    UIView *rightCheckJionView;
    UIView *failedView;
}
@synthesize avtarImageView;
@synthesize groupNameLabel;
@synthesize unreadMessageCountLabel;
@synthesize desLabel;
@synthesize circle;
@synthesize newsImage;
@synthesize addressBookSeparatorImageView;
@synthesize addressBookButton;
@synthesize horizontalImg;
//@synthesize joinInClickedBlock;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        UIImageView *avtarbg = [[[UIImageView alloc] initWithFrame:CGRectMake(((139/2.0)-AVTARBGSIZE)/2.0, ((139/2.0)-AVTARBGSIZE)/2.0, AVTARBGSIZE, AVTARBGSIZE)] autorelease];
        //avtarbg.image = [ZSTModuleImage(@"module_snsa_icon_frame.png") stretchableImageWithLeftCapWidth:2 topCapHeight:2];
        avtarbg.backgroundColor = [UIColor clearColor];
        [self addSubview:avtarbg];
        
        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake((AVTARBGSIZE-AVTARSIZE)/2.0,(AVTARBGSIZE-AVTARSIZE)/2.0 , AVTARSIZE, AVTARSIZE)] autorelease];
        self.avtarImageView.userInteractionEnabled = NO;
        avtarImageView.defaultImage = ZSTModuleImage(@"module_snsa_default_circle.png");
        [avtarbg addSubview:avtarImageView];
        
        avtarImageView.layer.cornerRadius = 4.f;
        avtarImageView.clipsToBounds = YES;
        avtarbg.layer.cornerRadius = 4.0f;
        avtarbg.clipsToBounds = YES;
        
        
        self.groupNameLabel = [[[UILabel alloc] initWithFrame:CGRectMake(75, 16, 170, 14)] autorelease];
        self.groupNameLabel.backgroundColor = [UIColor clearColor];
        self.groupNameLabel.font = [UIFont systemFontOfSize:14];
        self.groupNameLabel.textColor = [UIColor colorWithRed:0.17 green:0.25 blue:0.32 alpha:1];
        self.groupNameLabel.highlightedTextColor = [UIColor whiteColor];
        self.groupNameLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.groupNameLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self addSubview:self.groupNameLabel];
        
        self.unreadMessageCountLabel = [[[ZSTUnreadMessageCountLabel alloc] initWithFrame:CGRectMake(50, 48, 37/2.0 , 18)] autorelease];
        self.unreadMessageCountLabel.backgroundColor = [UIColor clearColor];
        self.unreadMessageCountLabel.font = [UIFont systemFontOfSize:10];
        self.unreadMessageCountLabel.textColor = [UIColor whiteColor];
        self.unreadMessageCountLabel.highlightedTextColor = [UIColor whiteColor];
        self.unreadMessageCountLabel.textAlignment = NSTextAlignmentCenter;
        self.unreadMessageCountLabel.hidden = YES;
        [self addSubview:self.unreadMessageCountLabel];
        
        self.desLabel = [[[UILabel alloc] initWithFrame:CGRectMake(75, 34, 170, 30)] autorelease];
        self.desLabel.numberOfLines = 2;
        self.desLabel.backgroundColor = [UIColor clearColor];
        self.desLabel.font = [UIFont systemFontOfSize:12];
        self.desLabel.textColor = [UIColor colorWithWhite:0.667 alpha:1];
        self.desLabel.highlightedTextColor = [UIColor whiteColor];
        self.desLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.desLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self addSubview:self.desLabel];
        
        rightJionedView = [[[UIView alloc] initWithFrame:CGRectMake(248, 0, 72, (139/2))] autorelease];
        UIImageView * rightBackGroundImgView = [[UIImageView alloc]init];
        rightBackGroundImgView.frame = CGRectMake(0, 0, 72, 70);
        rightBackGroundImgView.image = ZSTModuleImage(@"module_snsa_grass.png");
        [rightJionedView addSubview:rightBackGroundImgView];
        [self addSubview:rightJionedView];
        rightJionedView.hidden = YES;
        rightJionedView.userInteractionEnabled = NO;
        
        UIView * rightJionedlineView = [[UIView alloc]init];
        rightJionedlineView.backgroundColor = RGBCOLOR(204, 204, 204);
        rightJionedlineView.frame = CGRectMake(0, rightJionedView.frame.size.height * 0.2, 1, rightJionedView.frame.size.height* 0.6);
        [rightJionedView addSubview:rightJionedlineView];
        
        UIImage *image = ZSTModuleImage(@"module_snsa_btn_jion.png");
        CGFloat capWidth = image.size.width / 2;
        CGFloat capHeight = image.size.height / 2;
        
        rightWillJionView= [[[UIView alloc] initWithFrame:CGRectMake(248, 0, 72, (139/2))] autorelease];
        UIButton * JionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        JionButton.frame = CGRectMake(5, rightJionedView.frame.size.height/4, 62, rightJionedView.frame.size.height/2);
        [JionButton setTitle:@"加 入" forState:UIControlStateNormal];
        [JionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [JionButton setBackgroundImage:[image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight] forState:UIControlStateNormal];
        [JionButton addTarget:self action:@selector(jionAction:) forControlEvents:UIControlEventTouchUpInside];
        JionButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [rightWillJionView addSubview:JionButton];
        [self addSubview:rightWillJionView];
        rightWillJionView.hidden = YES;
        rightWillJionView.userInteractionEnabled = NO;
        
        UIView * rightWillJionlineView = [[UIView alloc]init];
        rightWillJionlineView.backgroundColor = RGBCOLOR(204, 204, 204);
        rightWillJionlineView.frame = CGRectMake(0, rightWillJionView.frame.size.height * 0.2, 1, rightWillJionView.frame.size.height* 0.6);
        [rightWillJionView addSubview:rightWillJionlineView];

        rightCheckJionView = [[[UIView alloc] initWithFrame:CGRectMake(248, 0, 72, (139/2))] autorelease];
        self.addressBookSeparatorImageView = [[[UIImageView alloc] init] autorelease];
        self.addressBookSeparatorImageView.frame = CGRectMake(0, 0, 1, (139/2));
        self.addressBookSeparatorImageView.backgroundColor = [UIColor clearColor];
         self.addressBookSeparatorImageView.image = [ZSTModuleImage(@"module_snsa_circle_list_split.png") stretchableImageWithLeftCapWidth:0 topCapHeight:0];
//        rightCheckJionView.backgroundColor = [UIColor brownColor];
//        UILabel * checkJionLabel = [[UILabel alloc]init];
//        checkJionLabel.frame = CGRectMake(0, 0, 72, 70);
//        checkJionLabel.text = @"审核中";
//        checkJionLabel.textAlignment = NSTextAlignmentCenter;
//        checkJionLabel.font = [UIFont systemFontOfSize:14];
//        checkJionLabel.textColor = [UIColor orangeColor];
//        [rightCheckJionView addSubview:checkJionLabel];
//        [checkJionLabel release];
        [rightCheckJionView addSubview:self.addressBookSeparatorImageView];
        self.addressBookButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.addressBookButton.titleLabel.font = [UIFont systemFontOfSize:12];
        self.addressBookButton.backgroundColor = [UIColor clearColor];
        CGFloat right = self.addressBookSeparatorImageView.right;
        self.addressBookButton.frame = CGRectMake(right, 0, 72 - right, (139/2));
//        self.addressBookButton.imageEdgeInsets = UIEdgeInsetsMake(30, 16, 30, 16);
        [self.addressBookButton setImage:ZSTModuleImage(@"module_snsa_public_circle_list_contact_n.png") forState:UIControlStateNormal];
        [self.addressBookButton setTitleColor:RGBCOLOR(195, 195, 195) forState:UIControlStateNormal];
        [self.addressBookButton setTitle:@"审核中" forState:UIControlStateNormal];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(right, 12, 72 - right, (80/2))];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.image = ZSTModuleImage(@"module_snsa_public_circle_list_contact_n.png");
        [rightCheckJionView addSubview:imageView];
        
        [rightCheckJionView addSubview:self.addressBookButton];
        [self addSubview:rightCheckJionView];
        rightCheckJionView.hidden = YES;
        rightCheckJionView.userInteractionEnabled = NO;
        
        failedView = [[[UIView alloc] initWithFrame:CGRectMake(248, 0, 120, (139/2))] autorelease];
//        UIButton * failedbtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        failedbtn.frame = CGRectMake(15, rightJionedView.frame.size.height/4, 120 - 30, rightJionedView.frame.size.height/2);
//        [failedbtn setTitle:@"审核失败" forState:UIControlStateNormal];
//        [failedbtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [failedbtn setBackgroundImage:[image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight] forState:UIControlStateNormal];
//        [failedbtn addTarget:self action:@selector(failedAction:) forControlEvents:UIControlEventTouchUpInside];
//        JionButton.titleLabel.font = [UIFont systemFontOfSize:14];
//        [failedView addSubview:failedbtn];
        self.faildbtnSeparatorImageView = [[[UIImageView alloc] init] autorelease];
        self.faildbtnSeparatorImageView.frame = CGRectMake(0, 0, 1, (139/2));
        self.faildbtnSeparatorImageView.backgroundColor = [UIColor clearColor];
        self.faildbtnSeparatorImageView.image = [ZSTModuleImage(@"module_snsa_circle_list_split.png") stretchableImageWithLeftCapWidth:0 topCapHeight:0];
        [self addSubview:failedView];
        [failedView addSubview:self.faildbtnSeparatorImageView];
        self.faildbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.faildbtn.titleLabel.font = [UIFont systemFontOfSize:12];
        self.faildbtn.backgroundColor = [UIColor clearColor];
        right = self.faildbtnSeparatorImageView.right;
        self.faildbtn.frame = CGRectMake(right, 0, 72 - right, (139/2));
//        self.faildbtn.imageEdgeInsets = UIEdgeInsetsMake(30, 16, 30, 16);
        [self.faildbtn setImage:ZSTModuleImage(@"module_snsa_circle_list_contact_n.png") forState:UIControlStateNormal];
        [self.faildbtn setTitleColor:RGBCOLOR(195, 195, 195) forState:UIControlStateNormal];
        [self.faildbtn setTitle:@"审核失败" forState:UIControlStateNormal];
        [self.faildbtn addTarget:self action:@selector(failedAction:) forControlEvents:UIControlEventTouchUpInside];
        [failedView addSubview:self.faildbtn];

        failedView.hidden = YES;
        failedView.userInteractionEnabled = NO;
        
        UIView * rightCheckJionlineView = [[UIView alloc]init];
        rightCheckJionlineView.backgroundColor = RGBCOLOR(204, 204, 204);
        rightCheckJionlineView.frame = CGRectMake(0, rightJionedView.frame.size.height * 0.2, 1, rightJionedView.frame.size.height* 0.6);
        [rightCheckJionView addSubview:rightCheckJionlineView];
        
        self.horizontalImg = [[[UIImageView alloc] initWithFrame:CGRectMake(0, (139/2), CGRectGetWidth(self.frame), 1)] autorelease];
        self.horizontalImg.image = [ZSTModuleImage(@"module_snsa_horizontal_divider.png") stretchableImageWithLeftCapWidth:0 topCapHeight:0];
        [self addSubview:self.horizontalImg];
        
    }
    return self;
}
-(void)setBlock:(blocked) btnClickedBlock
{
    _btnClickedBlock = [btnClickedBlock copy];
}
-(void)jionAction:(id) obj
{
     NSInteger choise = [[circle safeObjectForKey:@"AuditStatus"]integerValue];
    _btnClickedBlock(choise);
}

- (void) failedAction:(id)sender
{
     NSInteger choise = [[circle safeObjectForKey:@"AuditStatus"]integerValue];
    _btnClickedBlock(choise);
}

- (void)centerImageAndTitle:(float)spacing
{
    // get the size of the elements here for readability
    CGSize imageSize = self.addressBookButton.imageView.frame.size;
    CGSize titleSize = self.addressBookButton.titleLabel.frame.size;
    
    // get the height they will take up as a unit
    CGFloat totalHeight = (imageSize.height + titleSize.height + spacing);
    
    //适配IOS7
    //float systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    // raise the image and push it right to center it
    if (!rightCheckJionView.hidden) {
        self.addressBookButton.imageEdgeInsets = UIEdgeInsetsMake(
                                                                  - floor(totalHeight - imageSize.height) - (IS_IOS_7?10:0), 0.0 + (IS_IOS_7?10:0), -10, - floor(titleSize.width));
        
        // lower the text and push it left to center it
        self.addressBookButton.titleEdgeInsets = UIEdgeInsetsMake(
                                                                  0.0, - floor(imageSize.width)+8, - floor(totalHeight - titleSize.height) - 10, 0.0);
    }
    
    if (!failedView.hidden) {
        self.faildbtn.imageEdgeInsets = UIEdgeInsetsMake(
                                                                  - floor(totalHeight - imageSize.height) - (IS_IOS_7?10:0), 0.0 + (IS_IOS_7?10:0), -10, - floor(titleSize.width));
        
        // lower the text and push it left to center it
        self.faildbtn.titleEdgeInsets = UIEdgeInsetsMake(
                                                                  0.0, - floor(imageSize.width)+5, - floor(totalHeight - titleSize.height) - 10, 0.0);
    }

    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize size = [self.unreadMessageCountLabel.text sizeWithFont:self.unreadMessageCountLabel.font constrainedToSize:CGSizeMake(30, 16) lineBreakMode:NSLineBreakByCharWrapping];
    
    size = [self.groupNameLabel.text sizeWithFont:self.groupNameLabel.font constrainedToSize:CGSizeMake(156 - size.width, 16) lineBreakMode:NSLineBreakByCharWrapping];
    self.groupNameLabel.width = size.width;
    
    [self centerImageAndTitle:4.f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
}

//- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
//{
//    [super setHighlighted:highlighted animated:animated];
//    
//    self.addressBookButton.highlighted = NO;
//    self.addressBookSeparatorImageView.hidden = highlighted;
//}
-(void)hiddenAllRight
{
    rightWillJionView.hidden = YES;
    rightWillJionView.userInteractionEnabled = NO;
    
    rightJionedView.hidden = YES;
    rightJionedView.userInteractionEnabled = NO;
    
    rightCheckJionView.hidden = YES;
    rightCheckJionView.userInteractionEnabled = NO;
    
    failedView.hidden = YES;
    failedView.userInteractionEnabled = NO;
}
- (void)setCircle:(NSDictionary *)theCircle
{
    [self hiddenAllRight];
    if (theCircle) {
        [circle release];
        circle = [theCircle retain];
        
        NSInteger choise = [[theCircle safeObjectForKey:@"AuditStatus"]integerValue];
        switch (choise) {
            case 0:
            {
                rightWillJionView.hidden = NO;
                rightWillJionView.userInteractionEnabled = YES;
            }
                break;
            case 1:
            {
                rightJionedView.hidden = NO;
                rightJionedView.userInteractionEnabled = YES;
            }
                break;
            case 2:
            {
                rightCheckJionView.hidden = NO;
                rightCheckJionView.userInteractionEnabled = YES;
            }
                break;
            case 3:
            {
                failedView.hidden = NO;
                failedView.userInteractionEnabled = YES;
            }
                break;
            default:
                break;
        }
//        [self.avtarImageView clear];
        NSString *imageURL = [circle safeObjectForKey:@"CImgUrl"];
        if (imageURL == nil) {
            imageURL = @"";
        }
        
        self.avtarImageView.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", imageURL]];
        [self.avtarImageView loadImage];
        
        self.groupNameLabel.text = [circle safeObjectForKey:@"CName"];
        self.desLabel.text = [circle safeObjectForKey:@"Description"];
        
        
        [self setNeedsLayout];
    }
    
}


- (void)drawRect:(CGRect)rect
{
    [[ZSTModuleImage(@"module_snsa_circle_item_bg_n.png") stretchableImageWithLeftCapWidth:0 topCapHeight:0] drawInRect:rect];
}

- (void)dealloc
{
    self.addressBookSeparatorImageView = nil;
    self.addressBookButton = nil;
    self.faildbtn = nil;
    self.faildbtnSeparatorImageView = nil;
    self.avtarImageView = nil;
    self.groupNameLabel = nil;
    self.newsImage = nil;
    self.unreadMessageCountLabel = nil;
    self.desLabel = nil;
    self.circle = nil;
    self.horizontalImg = nil;
    
    [super dealloc];
}

@end
