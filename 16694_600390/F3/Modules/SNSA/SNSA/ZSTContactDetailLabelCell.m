//
//  ZSTContactDetailLabelCell.m
//  YouYun
//
//  Created by luobin on 7/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTContactDetailLabelCell.h"

@implementation ZSTContactDetailLabelCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.contentMode = UIViewContentModeRedraw;
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)drawRect:(CGRect)rect
{
//    UIImageView *separatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"contactDetail_list_separator.png"]];
//    separatorImageView.frame = CGRectMake(0, 63, 320, 1);
//    [cell addSubview:separatorImageView];
//    [separatorImageView release];
    [ZSTModuleImage(@"module_snsa_contactDetail_list_separator.png") drawInRect:CGRectMake(0, 63, self.width, 1)];
}

@end
