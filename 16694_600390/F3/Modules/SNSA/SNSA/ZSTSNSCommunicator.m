
#import "ZSTSNSCommunicator.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "TKKeychainUtils.h"
#import "ZSTF3Preferences.h"

#define kMaxCountPerLoad

#define kRequestUserInfo @"kRequestUserInfo"
#define kRequestUID @"id"


@interface ZSTSNSCommunicator ()

- (NSString *)md5Signature:(NSDictionary *)params;

- (NSData *)postDataFromMethod:(NSString *)method
                        params:(NSDictionary *)params
              needUserAuth:(BOOL)need;

- (void)requestFormPostURL:(NSString *)url
                    params:(NSDictionary *)params
                  fileData:(NSData *)data
                  delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
              failSelector:(SEL)failSelector
           succeedSelector:(SEL)succeedSelector
          progressDelegate:(id)progress 
                  userInfo:(id)userInfo;

- (void)requestFetchFailed:(ZSTSNSCommunicatorUserInfo *)info error:(NSError *)error;
@end

@implementation ZSTSNSCommunicator
@synthesize showErrorAlert, prefixParams;

SINGLETON_IMPLEMENTATION(ZSTSNSCommunicator);

- (id)init {
    self = [super init];
    if (self) {
        networkQueue = [[ASINetworkQueue alloc] init];
        [networkQueue setRequestDidFinishSelector:@selector(requestFetchComplete:)];
        [networkQueue setRequestDidFailSelector:@selector(requestFetchFailed:)];
        [networkQueue setDelegate:self];
        [networkQueue setMaxConcurrentOperationCount:1]; // ASI default set = 4.
        [networkQueue go];
        uploadRequestsSet = [[NSMutableSet alloc] init];
        showErrorAlert = NO;
    }
    return self;
}

- (void)dealloc {
    showErrorAlert = NO;
    [networkQueue reset];
    TKRELEASE(networkQueue);
    TKRELEASE(uploadRequestsSet);
    TKRELEASE(prefixParams);
    [super dealloc];
}

#pragma mark - helper
- (NSString *)md5Signature:(NSDictionary *)params{
    NSString *password = [TKKeychainUtils getToken:userKeyChainIdentifier];
    if (password==nil) {
        password = @"";
    }
    
    NSMutableString *result = [NSMutableString string];
    
    NSArray *keys = [params allKeys];
    keys = [keys sortedArrayUsingSelector:@selector(compare:)];
    
    for (NSString *key in keys) {
        [result appendString:[NSString stringWithFormat:@"%@=%@&", key, [params objectForKey:key]]];
    }
    
    [result appendString:[NSString stringWithFormat:@"Password=%@", password]];
    return [result md5];
}

#pragma mark - URL Request method
// ############################ URL request methods ############################
- (void)uploadFilePath:(NSString *)path
              delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
          failSelector:(SEL)failSelector
       succeedSelector:(SEL)succeedSelector
      progressDelegate:(id)progress
              userInfo:(id)theUserInfo
{
    
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    NSError *error;
    NSDictionary *imageInfo = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&error];
    // HHDPRINT(@"The image info : %@", imageInfo);
    if (!exists || [imageInfo objectForKey:NSFileType]!=NSFileTypeRegular) {
        NSError *error = [NSError errorWithDomain:ZSTRequestErrorDomain code:NSFileNoSuchFileError userInfo:[NSDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"File not found.", @"File not found."), NSLocalizedDescriptionKey, nil]];
        ZSTSNSCommunicatorUserInfo *userInfo = [[[ZSTSNSCommunicatorUserInfo alloc] init] autorelease];
        userInfo.delegate = delegate;
        userInfo.succeedSelector = succeedSelector;
        userInfo.failSelector = failSelector;
        userInfo.userInfo = theUserInfo;
        [self requestFetchFailed:userInfo error:error];
        return;
    }
    NSString *suffix = [[[NSFileManager defaultManager] displayNameAtPath:path] pathExtension];
    NSData *data = [NSData dataWithContentsOfFile:path];
    [self uploadFileData:data suffix:suffix delegate:delegate failSelector:failSelector succeedSelector:succeedSelector progressDelegate:progress userInfo:theUserInfo];
}

// upload file data
- (void)uploadFileData:(NSData *)data
                suffix:(NSString *)suffix
              delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
          failSelector:(SEL)failSelector
       succeedSelector:(SEL)succeedSelector
      progressDelegate:(id)progress 
              userInfo:(id)userInfo {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    
    NSString *ecID = [ZSTF3Preferences shared].ECECCID;
    [params setObject:ecID?ecID:@"" forKey:@"ECID"];

    NSString *userID = [TKKeychainUtils getUserid];
    [params setObject:userID?userID:@"" forKey:@"UID"];
    
    [params setObject:suffix forKey:@"Ex"];
//    [params setObject:[self md5Signature:params] forKey:@"Auth"];
    NSString *url = [NSString stringWithFormat:@"%@%@", URL_SERVER_BASE_PATH, URL_SERVER_PATH_UPLOAD_FILE];
    [self requestFormPostURL:url params:params fileData:data delegate:delegate failSelector:failSelector succeedSelector:succeedSelector progressDelegate:progress userInfo:userInfo];
}

- (void)uploadVideoData:(NSData *)data
                suffix:(NSString *)suffix
              delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
          failSelector:(SEL)failSelector
       succeedSelector:(SEL)succeedSelector
      progressDelegate:(id)progress
              userInfo:(id)userInfo {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    
    NSString *ecID = [ZSTF3Preferences shared].ECECCID;
    [params setObject:ecID?ecID:@"" forKey:@"ECID"];
    
    NSString *userID = [TKKeychainUtils getUserid];
    [params setObject:userID?userID:@"" forKey:@"UID"];
    
    [params setObject:suffix forKey:@"Ex"];
    //    [params setObject:[self md5Signature:params] forKey:@"Auth"];
    NSString *url = [NSString stringWithFormat:@"%@%@", URL_SERVER_BASE_PATH, URL_SERVER_PATH_UPLOAD_VIDEO];
    [self requestFormPostURL:url params:params fileData:data delegate:delegate failSelector:failSelector succeedSelector:succeedSelector progressDelegate:progress userInfo:userInfo];
}


//- (void)uploadUserCoverData:(NSData *)data
//                  imageName:(NSString *)name
//                      seqId:(NSString *)sid
//                 postParams:(NSDictionary *)value
//                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
//           progressDelegate:(id)progress {
//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:value];
//    [params setObject:name forKey:@"imagename"];
//    if ([params objectForKey:@"lat"]==nil || [params objectForKey:@"lng"]==nil) {
//        [params setObject:[[HHLocationManager shared] getLatitude] forKey:@"lat"];
//        [params setObject:[[HHLocationManager shared] getLongitude] forKey:@"lng"];
//    }
//    NSString *url = [NSString stringWithFormat:@"%@%@", URL_SERVER_BASE_PATH, URL_SERVER_PATH_UPLOAD_FILE];
//    [self requestFormPostURL:url params:params fileData:data seqId:sid delegate:delegate progressDelegate:progress];
//}
//
//- (void)uploadFaceIconPath:(NSString *)path
//                     seqId:(NSString *)sid
//                postParams:(NSDictionary *)params
//                  delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
//          progressDelegate:(id)progress {
//    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:path];
//    NSError *error;
//    NSDictionary *imageInfo = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&error];
//    // HHDPRINT(@"The image info : %@", imageInfo);
//    if (!exists || [imageInfo objectForKey:NSFileType]!=NSFileTypeRegular) {
//        NSError *error = [NSError error];
//        error.code = 0;
//        error.message = NSLocalizedString(@"HH_File_Not_Exists", @"File not found.");
//        ZSTSNSCommunicatorUserInfo *userInfo = [[[ZSTSNSCommunicatorUserInfo alloc] init] autorelease];
//        userInfo.delegate = delegate;
//        userInfo.uid = sid;
//        [self requestFetchFailed:userInfo error:error];
//        return;
//    }
//    NSString *name = [[NSFileManager defaultManager] displayNameAtPath:path];
//    NSData *data = [NSData dataWithContentsOfFile:path];
//    NSMutableDictionary *paramsDic = [NSMutableDictionary dictionaryWithDictionary:params];
//    [paramsDic setObject:name forKey:@"imagename"];
//    
////    CLLocation *location = [HHLocationManager shared].thisLocation;
////    CLLocationCoordinate2D coordinate = [location coordinate];    
////    [params setObject:[NSString stringWithFormat:@"%f", coordinate.latitude] forKey:@"lat"];
////    [params setObject:[NSString stringWithFormat:@"%f", coordinate.longitude] forKey:@"lng"];
//    NSString *url = [NSString stringWithFormat:@"%@%@", URL_SERVER_BASE_PATH, URL_SERVER_PATH_UPLOAD_FILE];
//    [self requestFormPostURL:url params:paramsDic fileData:data seqId:sid delegate:delegate progressDelegate:progress];
//}

- (void)openAPIPostToMethod:(NSString *)method
                 postParams:(NSDictionary *)params
               needUserAuth:(BOOL)need
                   delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector
                   userInfo:(id)theUserInfo
{
  // NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", URL_SERVER_BASE_PATH, URL_SERVER_PATH_APP_API]];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", URL_SERVER_BASE_PATH,method]];

    TKDINFO(@"Will post url address : %@", url);
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setTimeOutSeconds:30];
    NSData * body = [self postDataFromMethod:method params:params needUserAuth:need];
    [request appendPostData:body];
    
    ZSTSNSCommunicatorUserInfo *userInfo = [[ZSTSNSCommunicatorUserInfo alloc] init];
    userInfo.delegate = delegate;
    userInfo.succeedSelector = succeedSelector;
    userInfo.failSelector = failSelector;
    userInfo.userInfo = theUserInfo;
    
    [request setUserInfo:[NSMutableDictionary dictionaryWithObject:userInfo forKey:kRequestUserInfo]];
    [userInfo release];
    
#ifdef DEBUG
    NSString *requestString = [[[NSString alloc] initWithData:[request postBody] encoding:NSUTF8StringEncoding] autorelease];
    TKDPRINT(@"The requset postbody string : %@", requestString);
#endif   
    [networkQueue addOperation:request];
}

// Post
- (void)openAPIPostToMethod:(NSString *)method
                 postParams:(NSDictionary *)params
           needUserAuth:(BOOL)need
                   delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector{
    
    [self openAPIPostToMethod:method 
                   postParams:params 
                 needUserAuth:need 
                     delegate:delegate 
                 failSelector:failSelector 
              succeedSelector:succeedSelector 
                     userInfo:nil
     ];
}

- (void)openAPIPostToMethod:(NSString *)method
                 postParams:(NSDictionary *)params
                   delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector {
    [self openAPIPostToMethod:method postParams:params needUserAuth:YES delegate:delegate failSelector:failSelector succeedSelector:succeedSelector];
}

- (void)openAPIPostToMethod:(NSString *)method
                 postParams:(NSDictionary *)params
               needUserAuth:(BOOL)need
                   delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
                   userInfo:(id)userInfo
{
    [self openAPIPostToMethod:method postParams:params needUserAuth:need delegate:delegate failSelector:nil succeedSelector:nil userInfo:userInfo];
}


// ############################ URL request methods ############################

#pragma mark - Helper
- (NSData *)postDataFromMethod:(NSString *)method
                        params:(NSDictionary *)params
              needUserAuth:(BOOL)need {
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:params];
   // [dic setObject:method forKey:@"Method"];
    if (need) {
        NSString *userID = [TKKeychainUtils getUserid:userKeyChainIdentifier];
        [dic setObject:userID?userID:@"" forKey:@"UID"];
    } else {
        NSString * phoneNumber = [ZSTF3Preferences shared].phoneNumber;
        if (phoneNumber && ![phoneNumber isEqualToString:@""])
        {
            [dic setObject:phoneNumber forKey:@"Msisdn"];
        }
        else{
            NSString *loginMsisdn = [ZSTF3Preferences shared].loginMsisdn;
            if (!loginMsisdn || [loginMsisdn isEqualToString:@""])
            {
                loginMsisdn = [ZSTF3Preferences shared].UserId;
            }
            [dic setObject:loginMsisdn forKey:@"Msisdn"];
        }
    }
    
    NSString *ecID = [ZSTF3Preferences shared].ECECCID;
    [dic setObject:ecID?ecID:@"" forKey:@"ECID"];
        
//    [dic setObject:[self md5Signature:dic] forKey:@"Auth"];
    
    TKDINFO(@"Will post param:%@", dic);
    
    NSData *data = [[dic JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
    return data;
}

// Form upload file
- (void)requestFormPostURL:(NSString *)url
                    params:(NSDictionary *)params
                  fileData:(NSData *)data
                  delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
              failSelector:(SEL)failSelector
           succeedSelector:(SEL)succeedSelector
          progressDelegate:(id)progress 
                  userInfo:(id)theUserInfo {
    
    url = [url stringByAddingQuery:params];
    TKDINFO(@"Will post url address : %@", url);
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:url]];
    request.postBody = (NSMutableData *)data;
    [request setTimeOutSeconds:180];
//    [request setShouldContinueWhenAppEntersBackground:NO];
    if (progress) {
        [request setUploadProgressDelegate:progress];
        [request setShowAccurateProgress:YES];
    }
    [request setDidFinishSelector:@selector(requestFetchComplete:)];
    [request setDidFailSelector:@selector(requestFetchFailed:)];
    [request setDelegate:self];
    ZSTSNSCommunicatorUserInfo *userInfo = [[ZSTSNSCommunicatorUserInfo alloc] init];
    userInfo.delegate = delegate;
    userInfo.succeedSelector = succeedSelector;
    userInfo.failSelector = failSelector;
    userInfo.userInfo = theUserInfo;
    
    TKDINFO(@"the params : %@", params);
    
    [request setUserInfo:[NSDictionary dictionaryWithObject:userInfo forKey:kRequestUserInfo]];
    
    [request startAsynchronous];
    [uploadRequestsSet addObject:request];
    [userInfo release];
}

#pragma mark - ASI delegate
- (void)requestFetchFailed:(ZSTSNSCommunicatorUserInfo *)userInfo error:(NSError *)error{
    TKDERROR(@"requestFetchFailed, error: %@", error);
    
    if (userInfo.delegate) {
        if (!userInfo.failSelector || ![userInfo.delegate respondsToSelector:userInfo.failSelector]) {
            userInfo.failSelector = @selector(requestDidFail:userInfo:);
        }
        if ([userInfo.delegate respondsToSelector:userInfo.failSelector]) {
            [userInfo.delegate performSelector:userInfo.failSelector withObject:error withObject:userInfo.userInfo];
        }
    }
}

- (void)requestFetchFailed:(ASIHTTPRequest *)request {
//   HHDPRINT(@"request requestFetchFailed fetch has failed....... \n and error is : %@", [request error]);
    NSError *requsetError = [request error];
    if (showErrorAlert) {
        // TODO: 提示用户网络错误
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                        message:@"哎呀, 你的网络好像有点问题..."
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//        [alert release];
    }
    NSError *error = [NSError errorWithDomain:ZSTRequestErrorDomain
                                         code:requsetError.code
                                     userInfo:[NSDictionary dictionaryWithObjectsAndKeys: @"Request has occur an error.", NSLocalizedDescriptionKey, requsetError, NSUnderlyingErrorKey, request.url,NSURLErrorKey,nil]];
    
    ZSTSNSCommunicatorUserInfo *userInfo = [[request userInfo] objectForKey:kRequestUserInfo];
    [uploadRequestsSet removeObject:request];
    [self requestFetchFailed:userInfo error:error];
}

- (void)requestFetchComplete:(ASIHTTPRequest *)request {
    ZSTSNSCommunicatorUserInfo *userInfo = [[request userInfo] objectForKey:kRequestUserInfo];
    [uploadRequestsSet removeObject:request];
    TKDINFO(@"The requestFetchComplete response : [%@]", [request responseString]);
    
    NSDictionary *jsonDic = [[request responseString] JSONValue];
    if (jsonDic == nil) {
        NSError *error = [NSError errorWithDomain:ZSTRequestErrorDomain code:ZSTResultCode_ParseJsonFailure userInfo:[NSDictionary dictionaryWithObjectsAndKeys: @"Parse jason has occur an error.",NSLocalizedDescriptionKey, request.url, NSURLErrorKey, nil]];
        [self requestFetchFailed:userInfo error:error];
    }
    
    BOOL result = [[jsonDic objectForKey:@"Result"] boolValue];
        
    if (result) {
        
        if (userInfo.delegate) {
            if (!userInfo.succeedSelector || ![userInfo.delegate respondsToSelector:userInfo.succeedSelector]) {
                userInfo.succeedSelector = @selector(requestDidSucceed:userInfo:);
            }
            if ([userInfo.delegate respondsToSelector:userInfo.succeedSelector]) {
                [userInfo.delegate performSelector:userInfo.succeedSelector
                                        withObject:jsonDic
                                        withObject:userInfo.userInfo];
            }
        }
                
    } else {
        NSString *notice = [jsonDic objectForKey:@"Notice"];
        NSError *error = [NSError errorWithDomain:ZSTRequestErrorDomain code:ZSTResultCode_Failure userInfo:[NSDictionary dictionaryWithObjectsAndKeys:notice, NSLocalizedDescriptionKey, request.url, NSURLErrorKey, nil]];
        [self requestFetchFailed:userInfo error:error];
    }
}

- (void)cancelByDelegate:(id)delegate {
    @synchronized(self) {
        ZSTSNSCommunicatorUserInfo *userInfo;
        for (ASIHTTPRequest *request in [networkQueue operations]) {
            userInfo = [[request userInfo] objectForKey:kRequestUserInfo];
            if (userInfo.delegate == delegate) {
                [request clearDelegatesAndCancel];
                userInfo.delegate = nil;
            }
        }
    }
}

- (void)cancelUploadByDelegate:(id)delegate {
    @synchronized(self) {
        ZSTSNSCommunicatorUserInfo *userInfo;
        for (ASIHTTPRequest *request in uploadRequestsSet) {
            userInfo = [[request userInfo] objectForKey:kRequestUserInfo];
            if (userInfo.delegate == delegate) {
                TKDINFO(@"The cancel upload delegate is : %@", delegate);
                [request clearDelegatesAndCancel];
                userInfo.delegate = nil;
            }
        }
    }
}

@end

#pragma mark - 
@implementation ZSTSNSCommunicatorUserInfo

@synthesize delegate;
@synthesize failSelector;
@synthesize uid;
@synthesize succeedSelector;
@synthesize userInfo;

- (id)init {
    self = [super init];
    if (self) {
        uid = [[NSString alloc] init];
    }
    return self;
}

- (void)dealloc {
    self.userInfo = nil;
    delegate = nil;
    failSelector = nil;
    succeedSelector = nil;
    TKRELEASE(uid);
    [super dealloc];
}
@end
