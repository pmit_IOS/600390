//
//  ZSTShowAllCircleTableViewCell.h
//  SNSA
//
//  Created by zhangwanqiang on 14-5-4.
//
//

#import <UIKit/UIKit.h>
@class TKAsynImageView;
@class ZSTUnreadMessageCountLabel;
typedef  void (^blocked)(NSInteger tag);
@interface ZSTShowAllCircleTableViewCell : UITableViewCell
{
    blocked _btnClickedBlock;
}
@property (nonatomic, retain) TKAsynImageView *avtarImageView;

@property (nonatomic, retain) UILabel *groupNameLabel;

@property (nonatomic, retain) UIImageView *newsImage;

@property (nonatomic, retain) ZSTUnreadMessageCountLabel *unreadMessageCountLabel;

@property (nonatomic, retain) UILabel *desLabel;

@property (nonatomic, retain) UIButton *addressBookButton;

@property (nonatomic, retain) UIImageView *addressBookSeparatorImageView;

@property (nonatomic, retain) UIButton *faildbtn;

@property (nonatomic, retain) UIImageView *faildbtnSeparatorImageView;

@property (nonatomic, retain) UIImageView *horizontalImg;

@property (nonatomic, retain) NSDictionary *circle;

-(void)setBlock:(blocked) btnClickedBlock;

@end
