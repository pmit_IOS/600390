//
//  ZSTMyProfileController.m
//  YouYun
//
//  Created by luobin on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

//#define kZSTPropertyPhoneRow      0                 //手机
//#define kZSTPropertyCityRow       1                 //城市
//#define kZSTPropertyAddressRow    2                 //地址
//#define kZSTPropertyCompanyRow    5                 //公司
//#define kZSTPropertyEmailRow      4                 //邮箱
//#define kZSTPropertyQQRow         3                 //QQ
#define kZSTPropertyPhoneRow      0                 //手机
#define kZSTPropertyAddressRow    1                 //地址
#define kZSTPropertyEmailRow      2                 //邮箱
#define kZSTPropertyCompanyRow    3                 //公司

#import "ZSTMyProfileController.h"
#import "ZSTSexSwitch.h"
#import "ZSTProfileFieldCell.h"
#import "OverlayViewController.h"
#import "ZSTUtils.h"
#import "UIImage+Compress.h"
#import "CommonFunc.h"

@protocol ZSTHeaderCellDelegate <NSObject>

@optional

- (void)headerCellNameDidChange:(NSString *)newName;

- (void)headerCellSexDidChange:(ZST_Sex)newSex;

- (void)headerCellAvatarDidChange:(NSString *)fileKey;

@end

@interface ZSTHeaderCell : UITableViewCell<UITextFieldDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, ZSTYouYunEngineDelegate, UINavigationControllerDelegate>

@property (nonatomic, retain) TKAsynImageView *avtarImageView;

@property (nonatomic, retain) UITextField *nameTf;

@property (nonatomic, retain) ZSTSexSwitch *sexSwitch;

@property (nonatomic, assign) id<ZSTHeaderCellDelegate> delegate;

@property (nonatomic, retain) UIImagePickerController *iconPickerController;

@property (nonatomic, retain) ZSTYouYunEngine *engine;

@property (nonatomic, retain) NSString *promptStr;

//- (void)editUserName:(id)sender;

@end

@implementation ZSTHeaderCell

@synthesize avtarImageView;
@synthesize nameTf;
@synthesize sexSwitch;
@synthesize delegate;
@synthesize iconPickerController;
@synthesize engine;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(20, 20, 65, 65)] autorelease];
        self.avtarImageView.adorn = [ZSTModuleImage(@"module_snsa_chat_img_frame.png") stretchableImageWithLeftCapWidth:4 topCapHeight:6];
        self.avtarImageView.imageInset = UIEdgeInsetsMake(4, 4, 4, 4);
        avtarImageView.defaultImage = ZSTModuleImage(@"module_snsa_default_avatar.png");
        [avtarImageView addTarget:self action:@selector(changeAvatar) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:avtarImageView];
        
        self.nameTf = [[[UITextField alloc] initWithFrame:CGRectMake(108, 23, 180, 20)] autorelease];
        self.nameTf.backgroundColor = [UIColor clearColor];
        self.nameTf.font = [UIFont systemFontOfSize:16];
        nameTf.clearButtonMode = UITextFieldViewModeWhileEditing;
        nameTf.placeholder = NSLocalizedString(@"请输入你的姓名", nil);
        nameTf.textColor = RGBCOLOR(87, 87, 87);
        [self addSubview:self.nameTf];
        
        self.sexSwitch = [[[ZSTSexSwitch alloc] initWithFrame:CGRectMake(105, 55, 176, 26)] autorelease];

        [self addSubview:self.sexSwitch];
        
        UILabel *profileLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20, 110, 100, 20)] autorelease];
        profileLabel.backgroundColor = [UIColor clearColor];
        profileLabel.font = [UIFont systemFontOfSize:16];
        profileLabel.textColor = RGBCOLOR(87, 87, 87);
        profileLabel.text = NSLocalizedString(@"个人资料", nil);
        [self addSubview:profileLabel];
        
        UILabel *promptLabel = [[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(profileLabel.frame), 110, 200, 20)] autorelease];
        promptLabel.backgroundColor = [UIColor clearColor];
        promptLabel.font = [UIFont systemFontOfSize:14];
        promptLabel.textColor = [UIColor redColor];
        [self addSubview:promptLabel];
        profileLabel.text = self.promptStr;
        
        UIView *profileDivideBg = [[[UIView alloc] initWithFrame:CGRectMake(0, 139, 320, 3)] autorelease];
        profileDivideBg.backgroundColor = RGBCOLOR(250, 250, 250);
        [self addSubview:profileDivideBg];
        
        UIImageView *profileDivide = [[[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_snsa_profileDivide.png")] autorelease];
        profileDivide.frame = CGRectMake(20, 139, 320, 1);
        [self addSubview:profileDivide];
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(textFieldTextDidChange:) 
													 name:UITextFieldTextDidChangeNotification 
												   object:self.nameTf];	
        
        [self.sexSwitch addObserver:self forKeyPath:@"sex" options:NSKeyValueObservingOptionNew context:nil];
        
        self.engine = [ZSTYouYunEngine engineWithDelegate:self];
        self.engine.moduleType = self.viewController.moduleType;
    }
    return self;
}

- (void)textFieldTextDidChange:(NSNotification*)notification
{
    if (delegate && [delegate respondsToSelector:@selector(headerCellNameDidChange:)]) {
        [delegate headerCellNameDidChange:self.nameTf.text];
    }
    return;
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == self.sexSwitch && [keyPath isEqualToString:@"sex"]) {

        if (delegate && [delegate respondsToSelector:@selector(headerCellSexDidChange:)]) {
            [delegate headerCellSexDidChange:self.sexSwitch.sex];
        }
        return;
    }
    
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];	
    self.iconPickerController = nil;
    self.engine.delegate = self;
    self.engine = nil;
    self.avtarImageView = nil;
    self.nameTf = nil;
    self.sexSwitch = nil;
    [super dealloc];
}

- (void)changeAvatar
{
    UIActionSheet *actionSheet = nil;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"获取图片",nil)
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"取消",@"取消")
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:NSLocalizedString(@"拍照",nil), NSLocalizedString(@"本地图片",nil), nil];
    } else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"获取图片",nil)
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"取消",@"取消")
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:NSLocalizedString(@"本地图片",nil), nil];
    }
    
    [actionSheet showInView:self];
    [actionSheet release];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    self.iconPickerController = [[[UIImagePickerController alloc] init] autorelease];
    
    if ( buttonIndex == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) { // 拍照
        
        iconPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        iconPickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        iconPickerController.showsCameraControls = YES;
        
    } else if(buttonIndex != actionSheet.cancelButtonIndex) { // 本地图片
        
        iconPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    }
    iconPickerController.allowsEditing = YES;
    iconPickerController.delegate = self;
    [self.viewController presentModalViewController:iconPickerController animated:YES];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType =[NSString stringWithFormat:@"%@", [info objectForKey:UIImagePickerControllerMediaType]];
    if (![mediaType isEqualToString:@"public.image"]) {  
        return;
    }
    
    UIImage *tempImage = [info objectForKey:UIImagePickerControllerEditedImage];   
    [self compressImage:tempImage];
    [self.iconPickerController dismissViewControllerAnimated:YES completion:nil];

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)compressImage:(UIImage *)bigImage
{
    [self performSelector:@selector(doCompressImage:) withObject:bigImage afterDelay:0.6];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)doCompressImage:(UIImage *)bigImage
{
    UIImage *newImage = [bigImage compressedImageScaleFactor:100];
    
    [self uploadFile:newImage];
    
    [self.iconPickerController dismissViewControllerAnimated:YES completion:nil];
    self.iconPickerController = nil;
        
}

- (void)uploadFile:(UIImage *)image
{
    CGFloat height = 100;
    if (![(ZSTMyProfileController *)self.viewController keyboardHasShow]) {
        height = 176;
    }
    
    [TKUIUtil showHUDInView:self.viewController.view 
                   withText:NSLocalizedString(@"正在上传", @"正在上传") 
                  withImage:nil 
                 withCenter:CGPointMake(160, height)];
    
    [self.engine upLoadFile:UIImagePNGRepresentation(image) progressDelegate:nil suffix:@"png"];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - ZSTLegicyCommunicatorDelegate

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)upLoadFileResponse:(NSString *)fileKey
{
    CGFloat height = 100;
    if (![(ZSTMyProfileController *)self.viewController keyboardHasShow]) {
        height = 176;
    }
    
    [TKUIUtil showHUDInView:self.viewController.view withText:NSLocalizedString(@"上传成功!", nil)
                  withImage:[UIImage  imageNamed:@"icon_smile_face.png"]
                 withCenter:CGPointMake(160, height)];
    
    [TKUIUtil hiddenHUDAfterDelay:2];

    self.avtarImageView.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",fileKey]];
    [self.avtarImageView loadImage];
    if (delegate && [delegate respondsToSelector:@selector(headerCellAvatarDidChange:)]) {
        [delegate headerCellAvatarDidChange:fileKey];
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)requestDidFail:(NSError *)error method:(NSString *)method
{
    CGFloat height = 100;
    if (![(ZSTMyProfileController *)self.viewController keyboardHasShow]) {
        height = 176;
    }
    
    [TKUIUtil showHUDInView:self.viewController.view withText:NSLocalizedString(@"网络连接失败!", nil)  withImage:[UIImage imageNamed:@"icon_warning.png"] withCenter:CGPointMake(160, height)];
    [TKUIUtil hiddenHUDAfterDelay:2];
}

@end


@interface ZSTMyProfileController ()<TextFieldCellDelegate, ZSTHeaderCellDelegate, ZSTYouYunEngineDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) UITableView *tableView;

@property (nonatomic, retain) UIButton *saveBtn;

@property (nonatomic, retain) NSMutableDictionary *userInfo;

@property (nonatomic, retain) ZSTYouYunEngine *engine;



@end

@implementation ZSTMyProfileController

@synthesize saveBtn;
@synthesize tableView;
@synthesize userInfo;
@synthesize engine;
@synthesize keyboardHasShow;
@synthesize promptStr;

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        keyboardHasShow = NO;
        
        isAlert = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(keyboardWillShow:) 
													 name:UIKeyboardWillShowNotification 
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(keyboardWillHide:) 
													 name:UIKeyboardWillHideNotification 
												   object:nil];	
        
        
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];	
    self.engine = nil;
    self.saveBtn = nil;
    self.tableView = nil;
    self.engine = nil;
    self.userInfo = nil;
    self.promptStr = nil;
    [super dealloc];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    lastContentOffsetY = 0.f;
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 374 + 42+(iPhone5?88:0)) style:UITableViewStylePlain] autorelease];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = RGBCOLOR(250, 250, 250);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"保存",@"") target:self selector:@selector (saveButtonAction)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"取消",@"取消") target:self selector:@selector (dismissModalViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"个人名片", nil)];
    
    self.navigationController.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];
    
    self.promptStr = [[NSString alloc] initWithString:@""];
    if (self.isComplete) {
        
        self.promptStr = @"请完善个人资料";
    }
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    self.engine.moduleType = self.moduleType;
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        [self.engine getUserByUID:[TKKeychainUtils getUserid:userKeyChainIdentifier]];
    }];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)setBlock:(saveBlocked) btnClickedBlock
{
    _btnSaveBlock = [btnClickedBlock copy];
}


- (void)saveButtonAction
{
    UIView *firstREsponder = [self.tableView findFirstResponder];
    [firstREsponder resignFirstResponder];
    
     [self.userInfo setSafeObject:[NSString stringWithFormat:@"%d",![ZSTF3Preferences shared].snsaStates] forKey:@"ispublic"];
    
    CGFloat height = 100;
    if (![self keyboardHasShow]) {
        height = 176;
    }
    
    if (![self.userInfo isKindOfClass:[NSNull class]]) {
        
        NSString *useNameStr = [self.userInfo safeObjectForKey:@"UserName"];
        NSString *addressStr = [self.userInfo safeObjectForKey:@"Address"];
        NSString *emailStr = [self.userInfo safeObjectForKey:@"Email"];
        NSString *companyStr = [self.userInfo safeObjectForKey:@"Company"];
        
        if (useNameStr.length > 5 && isAlert) {
            
            [TKUIUtil alertInWindow:@"亲，昵称不能超过5个字！" withImage:nil];
            return;
        }
        
        if (addressStr.length == 0) {
            
            [TKUIUtil alertInWindow:@"地址不为空" withImage:nil];
            return;
        }
        
        if (emailStr.length == 0) {
            
            [TKUIUtil alertInWindow:@"邮箱不为空" withImage:nil];
            return;
        }
        
        if (companyStr.length == 0) {
            
            [TKUIUtil alertInWindow:@"备注不为空" withImage:nil];
            return;
        }
    } else {
        
        [TKUIUtil alertInWindow:@"保存失败，请重新填写资料" withImage:nil];
        return;
    }
    
    [TKUIUtil showHUDInView:self.view 
                   withText:NSLocalizedString(@"正在设置名片", @"正在设置名片") 
                  withImage:nil 
                 withCenter:CGPointMake(160, height)];

    self.saveBtn.enabled = NO;
    
    [self.engine updateUserByUID:[TKKeychainUtils getUserid:userKeyChainIdentifier]
                          msisdn:[self.userInfo safeObjectForKey:@"Msisdn"]
                        userName:[self.userInfo safeObjectForKey:@"UserName"]
                          gender:[[self.userInfo safeObjectForKey:@"Gender"] intValue]
                            city:[self.userInfo safeObjectForKey:@"City"]
                         company:[self.userInfo safeObjectForKey:@"Company"]
                         address:[self.userInfo safeObjectForKey:@"Address"]
                              qq:[self.userInfo safeObjectForKey:@"QQ"]
                           email:[self.userInfo safeObjectForKey:@"Email"]
                          imgUrl:[self.userInfo safeObjectForKey:@"ImgUrl"]
                        ispublic:[[self.userInfo safeObjectForKey:@"ispublic"] intValue]];
    
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.tracking && lastContentOffsetY > scrollView.contentOffset.y) {
        UIView *firstREsponder = [self.tableView findFirstResponder];
        [firstREsponder resignFirstResponder];
    }
    lastContentOffsetY = scrollView.contentOffset.y;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return self.userInfo? 2 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return section ? 4 : 1;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
        static NSString *CellIdentifier = @"ZSTHeaderCell";
        ZSTHeaderCell *cell = [theTableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[ZSTHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.delegate = self;
            cell.backgroundColor = RGBCOLOR(250, 250, 250);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        // Configure the cell...
        [cell.avtarImageView clear];
        NSString *fileKey = [self.userInfo safeObjectForKey:@"ImgUrl"];
        if (fileKey == nil) {
            fileKey = @"";
        }
//        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/resource/thumbnail/%@", URL_SERVER_BASE_PATH, fileKey]];
         NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",fileKey]];
        [cell.avtarImageView setUrl:url];
        [cell.avtarImageView loadImage];
        cell.nameTf.text = [self.userInfo safeObjectForKey:@"UserName"];
        cell.sexSwitch.sex = [[self.userInfo safeObjectForKey:@"Gender"] intValue];
        cell.promptStr = self.promptStr;
        
        return cell;
        
    } else {
        
        static NSString *CellIdentifier = @"TextFieldCell";
        ZSTProfileFieldCell *cell = [theTableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[ZSTProfileFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.delegate = self;
            cell.labelWidth = 90.f;
            cell.textLabel.textColor = [UIColor colorWithWhite:106.0/255 alpha:1];
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.textLabel.font = [UIFont systemFontOfSize:14];
            cell.alwaysEditable = YES;
            cell.backgroundColor = RGBCOLOR(250, 250, 250);
            cell.textField.placeholder = NSLocalizedString(@"还没填写哦", nil);
            cell.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            cell.textField.font = [UIFont systemFontOfSize:14];
            cell.textField.textColor = [UIColor colorWithWhite:142.0/255 alpha:1];
        }
        cell.tag = indexPath.row;
        
        if(([theTableView numberOfRowsInSection:indexPath.section]-1) == 0){
            cell.position = CustomCellBackgroundViewPositionSingle;
        }
        else if(indexPath.row == 0){
            cell.position = CustomCellBackgroundViewPositionTop;
        }
        else if (indexPath.row == ([theTableView numberOfRowsInSection:indexPath.section]-1)){
            cell.position  = CustomCellBackgroundViewPositionBottom;
        }
        else{
            cell.position = CustomCellBackgroundViewPositionMiddle;
        }
        
//        if (indexPath.row == kZSTPropertyPhoneRow) {
//            cell.alwaysEditable = NO;
//            cell.textLabel.text = NSLocalizedString(@"   手机", nil);
//            cell.textField.text = [self.userInfo safeObjectForKey:@"Msisdn"];
//        } else if (indexPath.row == kZSTPropertyCityRow) {
//            cell.alwaysEditable = YES;
//            cell.textLabel.text = NSLocalizedString(@"   城市", nil);
//            cell.textField.keyboardType = UIKeyboardTypeDefault;
//            cell.textField.text = [self.userInfo safeObjectForKey:@"City"];
//        } else if(indexPath.row == kZSTPropertyAddressRow) {
//            cell.alwaysEditable = YES;
//            cell.textLabel.text = NSLocalizedString(@"   地址", nil);
//            cell.textField.keyboardType = UIKeyboardTypeDefault;
//            cell.textField.text = [self.userInfo safeObjectForKey:@"Address"];
//        } else if(indexPath.row == kZSTPropertyCompanyRow) {
//            cell.alwaysEditable = YES;
//            cell.textLabel.text = NSLocalizedString(@"   备注", nil);
//            cell.textField.keyboardType = UIKeyboardTypeDefault;
//            cell.textField.text = [self.userInfo safeObjectForKey:@"Company"];
//        } else if(indexPath.row == kZSTPropertyEmailRow) {
//            cell.alwaysEditable = YES;
//            cell.textLabel.text = NSLocalizedString(@"   邮箱", nil);
//            cell.textField.keyboardType = UIKeyboardTypeEmailAddress;
//            cell.textField.text = [self.userInfo safeObjectForKey:@"Email"];
//        } else if(indexPath.row == kZSTPropertyQQRow) {
//            cell.alwaysEditable = YES;
//            cell.textLabel.text = NSLocalizedString(@"    QQ", nil);
//            cell.textField.keyboardType = UIKeyboardTypeNumberPad;
//            cell.textField.text = [self.userInfo safeObjectForKey:@"QQ"];
//        }
        
        if (indexPath.row == kZSTPropertyPhoneRow) {
            cell.alwaysEditable = NO;
            cell.textLabel.text = NSLocalizedString(@"   手机", nil);
            cell.textField.text = [self.userInfo safeObjectForKey:@"Msisdn"];
            
            NSString *string = [self.userInfo safeObjectForKey:@"Msisdn"];
            if ([[self.userInfo safeObjectForKey:@"ispublic"] intValue] == 1) {
                
                
            } else {
                
                string = [string stringByReplacingCharactersInRange:NSMakeRange(3,4) withString:@"****"];
            }
            
            cell.textField.text = string;
            
            cell.switchOne.hidden = NO;
            cell.switchOne.status = ![[self.userInfo safeObjectForKey:@"ispublic"] intValue];
            
            [cell setInfoStatesBlock:^(ZSTProfileFieldCell *cell,int states) {
                
                [ZSTF3Preferences shared].snsaStates = states;
                
                NSString *string = [self.userInfo safeObjectForKey:@"Msisdn"];
                
                if (states == 0) {
                    
                    
                } else if (states == 1) {
                    
                    string = [string stringByReplacingCharactersInRange:NSMakeRange(3,4) withString:@"****"];
                }
                
                cell.textField.text = string;
                
            }];
            
        } else if(indexPath.row == kZSTPropertyAddressRow) {
            cell.alwaysEditable = YES;
            cell.textLabel.text = NSLocalizedString(@"   地址", nil);
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.textField.text = [self.userInfo safeObjectForKey:@"Address"];
        } else if(indexPath.row == kZSTPropertyEmailRow) {
            cell.alwaysEditable = YES;
            cell.textLabel.text = NSLocalizedString(@"   邮箱", nil);
            cell.textField.keyboardType = UIKeyboardTypeEmailAddress;
            cell.textField.text = [self.userInfo safeObjectForKey:@"Email"];
        } else if(indexPath.row == kZSTPropertyCompanyRow) {
            cell.alwaysEditable = YES;
            cell.textLabel.text = NSLocalizedString(@"   备注", nil);
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.textField.text = [self.userInfo safeObjectForKey:@"Company"];
        }
        
        cell.textLabel.textColor = RGBCOLOR(140, 140, 140);
        cell.textField.textColor = RGBCOLOR(87, 87, 87);
        cell.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        cell.labelWidth = 84;
        
        return cell;
    }
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.section? 50.f : 140.f;
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==1 && indexPath.row>0) {
        ZSTProfileFieldCell *cell = (ZSTProfileFieldCell *)[tableView cellForRowAtIndexPath:indexPath];
        [cell.textField becomeFirstResponder];
    }
}

#pragma mark - Table view TextFieldCellDelegate
- (void) textFieldCellTextDidChange:(TextFieldCell*)cell
{
   if(cell.tag == kZSTPropertyAddressRow) {
        
        [self.userInfo setObject:cell.textField.text forKey:@"Address"];
   } else if (cell.tag == kZSTPropertyEmailRow) {
       
        [self.userInfo setObject:cell.textField.text forKey:@"Email"];
    } else if(cell.tag == kZSTPropertyCompanyRow) {
        
        [self.userInfo setObject:cell.textField.text forKey:@"Company"];
    }
}

#pragma mark - Table view ZSTHeaderCellDelegate
- (void)headerCellNameDidChange:(NSString *)newName
{
    isAlert = YES;
    [self.userInfo setObject:newName forKey:@"UserName"];
}

- (void)headerCellSexDidChange:(ZST_Sex)newSex
{
    [self.userInfo setObject:[NSNumber numberWithInt:newSex] forKey:@"Gender"];
}

- (void)headerCellAvatarDidChange:(NSString *)fileKey
{
    [self.userInfo setObject:fileKey forKey:@"ImgUrl"];
}

//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
	// get a rect for the textView frame
	CGRect btnFrame = self.saveBtn.frame;
    btnFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + btnFrame.size.height);
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    [UIView setAnimationDelegate:self];
	
	// set views with new info
	saveBtn.frame = btnFrame;
    self.tableView.frame = CGRectMake(0, 0, 320, btnFrame.origin.y);
	
	// commit animations
	[UIView commitAnimations];
    
    keyboardHasShow = YES;
}

- (void)animationDidStop:(NSString *)animationID finished:(BOOL)finished
{
    [self.tableView scrollFirstResponderIntoView];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
	// get a rect for the textView frame
	CGRect btnFrame = self.saveBtn.frame;
    btnFrame.origin.y = self.view.bounds.size.height - btnFrame.size.height;
	
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
	self.saveBtn.frame = btnFrame;
    self.tableView.frame = CGRectMake(0, 0, 320, btnFrame.origin.y);
	
	// commit animations
	[UIView commitAnimations];
    
    keyboardHasShow = NO;
}

#pragma mark - ZSTYouYunEngineDelegate
- (void)getUserByUIDResponse:(NSDictionary *)theUserInfo
{
    [self.view removeNetworkIndicatorView];
    self.saveBtn.enabled = YES;
    self.userInfo = [[theUserInfo mutableCopy] autorelease];
     [[CommonFunc sharedCommon] updateUVO:theUserInfo];
    
    [self.tableView insertSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 2)] withRowAnimation:UITableViewRowAnimationTop];
}

- (void)updateUserByUIDResponse
{
    CGFloat height = 100;
    if (![self keyboardHasShow]) {
        height = 176;
    }
    
    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"保存成功!", nil) withImage:[UIImage  imageNamed:@"icon_smile_face.png"] withCenter:CGPointMake(160, height)];
    [TKUIUtil hiddenHUDAfterDelay:2];
    
    [self performSelector:@selector(dismissModalViewController) withObject:nil afterDelay:2.1];//0.6
    
    if (self.isComplete) {
        _btnSaveBlock(1);
    }
}

- (void)requestDidFail:(NSError *)error method:(NSString *)method
{
    if ([method isEqualToString:ZSTHttpMethod_UpdateUserByUID]) {
        
        TKAlertAppNameTitle(NSLocalizedString(@"网络连接失败!", nil));
        
        self.saveBtn.enabled = YES;
    } else {
        [self.view refreshFailed];
    }
}


@end
