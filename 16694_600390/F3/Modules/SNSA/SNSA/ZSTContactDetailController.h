//
//  ZSTContactDetailController.h
//  YouYun
//
//  Created by luobin on 5/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
#import <AddressBook/AddressBook.h>
#import <PMRepairButton.h>

@interface ZSTContactDetailController : UIViewController<ABNewPersonViewControllerDelegate>

/**
 *	@brief	<#Description#>
 */
@property (nonatomic, retain) NSDictionary *userInfo;

@property (nonatomic, retain) PMRepairButton *saveAddressBtn;

@property (nonatomic) BOOL isAddressSave;

@property (nonatomic, retain) PMRepairButton *leftBtn;

@property (nonatomic, retain) PMRepairButton *rightBtn;

/**
 *	@brief	指定用户ID，当userInfo不为空时有效
 */
@property (nonatomic, copy) NSString *userID;


@end
