//
//  ZSTMemberCountLabel.m
//  YouYun
//
//  Created by luobin on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTUnreadMessageCountLabel.h"

@implementation ZSTUnreadMessageCountLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines
{
    return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, 2, 0, 2));
}

- (void)drawTextInRect:(CGRect)rect
{
    [super drawTextInRect: [self textRectForBounds:rect limitedToNumberOfLines:self.numberOfLines]];
}

//// Only override drawRect: if you perform custom drawing.
//// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [ZSTModuleImage(@"module_snsa_number_bg.png") drawInRect:CGRectMake(0, 0, 37/2, 36/2)];
    [super drawRect:rect];
}

@end
