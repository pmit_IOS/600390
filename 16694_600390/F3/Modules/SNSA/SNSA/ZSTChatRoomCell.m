//
//  ZSTChatRoomCell.m
//  SNSA
//
//  Created by LiZhenQu on 14-5-6.
//
//

#import "ZSTChatRoomCell.h"
#import "CommonFunc.h"
#import "ZSTTopicCommentController.h"
#import "ZSTChatRoomViewController.h"

@interface ZSTChatRoomCell() <ZSTCommentSendFinishDelegate,UIActionSheetDelegate>

@property (assign,nonatomic) BOOL isBig;

@end

@implementation ZSTChatRoomCell

@synthesize avtarImageView;
@synthesize pictureImageView;
@synthesize chatBg;
@synthesize cellData;
@synthesize cid;
@synthesize engine;
@synthesize contentLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.engine = [ZSTYouYunEngine engineWithDelegate:self];
        self.autoresizesSubviews = NO;//?
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
//        self.avtarImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(10, 10, 45, 45)];
        self.avtarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 45, 45)];
//        avtarImageView.defaultImage = ZSTModuleImage(@"module_snsa_default_avatar.png");
//        self.avtarImageView.needCropper = YES;
//        self.avtarImageView.asynImageDelegate = self;
        [self.avtarImageView setImage:ZSTModuleImage(@"module_snsa_default_avatar.png")];
        CALayer * avtarImageViewLayer = [avtarImageView layer];
        [avtarImageViewLayer setMasksToBounds:YES];
        [avtarImageViewLayer setCornerRadius:5.0];
        [self addSubview:avtarImageView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTheHead:)];
        self.avtarImageView.userInteractionEnabled = YES;
        [self.avtarImageView addGestureRecognizer:tap];
        
        self.contentLabel = [[[NIAttributedLabel alloc] init] autorelease];
        contentLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:contentLabel];
        
        //消息图片
//        self.pictureImageView = [[TKAsynImageView alloc] init];
//        self.pictureImageView.backgroundColor = [UIColor clearColor];
//        self.pictureImageView.adorn = [[UIImage imageNamed:@"activity-photo-frame.png"] stretchableImageWithLeftCapWidth:4 topCapHeight:6];
//        self.pictureImageView.imageInset = UIEdgeInsetsMake(3, 4, 5, 4);
//        self.pictureImageView.adjustsImageWhenHighlighted = NO;
        self.pictureImageView = [[UIImageView alloc] init];
        self.pictureImageView.backgroundColor = [UIColor clearColor];
        
        UITapGestureRecognizer *singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
        singleRecognizer.numberOfTapsRequired = 1;
        
        UITapGestureRecognizer *doubleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:nil action:nil];
        doubleRecognizer.numberOfTapsRequired = 2;
        [singleRecognizer requireGestureRecognizerToFail:doubleRecognizer];
        
        [self.pictureImageView addGestureRecognizer:singleRecognizer];
        [self.pictureImageView addGestureRecognizer:doubleRecognizer];
        self.pictureImageView.userInteractionEnabled = YES;
        
        [singleRecognizer release];
        [doubleRecognizer release];
        [self addSubview:self.pictureImageView];
        
        self.chatBg = [[UIImageView alloc] initWithFrame:CGRectMake(1, 1, 1, 1)];
        self.chatBg.userInteractionEnabled = YES;
        [self addSubview:self.chatBg];
//        TagUILongPressGestureRecognizer *chatLongRecognizer = [[TagUILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(chatLongTap:)];
//        [self.chatBg addGestureRecognizer:chatLongRecognizer];
//        [chatLongRecognizer release];
        
        UITapGestureRecognizer *cmtRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chatBubbleTouchUpInsideAction)];
        cmtRecognizer.numberOfTapsRequired = 1;
        [self.chatBg addGestureRecognizer:cmtRecognizer];
        [cmtRecognizer release];
    }
    return self;
}

- (void)chatBubbleTouchUpInsideAction //弹出评论
{
    NSNumber *fileType = [self.cellData safeObjectForKey:@"Type"];
    NSString *content = [self.cellData safeObjectForKey:@"MContent"];
    if (fileType.intValue==2) {
        if (!content || [@"" isEqualToString:content]) {
            TKAlertAppNameTitle(@"语音文件残缺!");
            return;
        }
    }
    ZSTTopicCommentController *topicCommentController = [[ZSTTopicCommentController alloc] init];
    topicCommentController.delegate = self;
    topicCommentController.topic = self.cellData;
    topicCommentController.CID = self.cid;
    topicCommentController.hidesBottomBarWhenPushed = YES;
    [self.viewController.navigationController pushViewController:topicCommentController animated:YES];
    [topicCommentController release];
}

- (CGSize)displayRectForImage
{
    CGRect screenRect = TKScreenBounds();
    
    CGSize imageSize = self.pictureImageView.frame.size;
    if (imageSize.width/ imageSize.height >= screenRect.size.width/ screenRect.size.height) {
        CGFloat height = screenRect.size.width * imageSize.height / imageSize.width;
        return CGSizeMake(screenRect.size.width, height);
    } else {
        CGFloat width = screenRect.size.height * imageSize.width / imageSize.height;
        return CGSizeMake(width, screenRect.size.height);
    }
}

- (void)singleTap
{
    NSString *imageURL = [self.cellData safeObjectForKey:@"ImgUrl"];
    
    if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
        
//        self.pictureImageView.imageView.zoomedSize = [self displayRectForImage:self.pictureImageView.frame.size];
//        
//        self.pictureImageView.imageView.wrapInScrollviewWhenZoomed = YES;
//        [self.pictureImageView.imageView zoomIn];
        self.pictureImageView.zoomedSize = [self displayRectForImage:self.pictureImageView.frame.size];
        self.pictureImageView.wrapInScrollviewWhenZoomed = YES;
        if (!self.isBig)
        {
            self.isBig = YES;
            [self.pictureImageView zoomIn];
        }
        else
        {
            self.isBig = NO;
            [self.pictureImageView zoomOut];
        }
        
    }
}

- (CGSize)displayRectForImage:(CGSize)imageSize
{
    CGRect screenRect = TKScreenBounds();
    if (imageSize.width/ imageSize.height >= screenRect.size.width/ screenRect.size.height) {
        CGFloat height = screenRect.size.width * imageSize.height / imageSize.width;
        return CGSizeMake(screenRect.size.width, height);
    } else {
        CGFloat width = screenRect.size.height * imageSize.width / imageSize.height;
        return CGSizeMake(width, screenRect.size.height);
    }
}


-(void)chatLongTap:(UILongPressGestureRecognizer *)longPress {
    if (longPress.state == UIGestureRecognizerStateBegan) {
        UIMenuController *menuController = [UIMenuController sharedMenuController];
        UIMenuItem *transmitItem = [[[UIMenuItem alloc] initWithTitle:@"转发" action:@selector(transmit:)] autorelease];
        [menuController setMenuItems:[NSArray arrayWithObject:transmitItem]];
        [self becomeFirstResponder];
        CGPoint location = [longPress locationInView:self.viewController.view]; //点哪，显哪
        [menuController setTargetRect:CGRectMake(location.x, location.y, 0, 0) inView:self.viewController.view];
        [menuController setMenuVisible:YES animated:YES];
    }
}

- (BOOL)canBecomeFirstResponder{
    return YES;
}
//
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    if(action == @selector(copy:)){
        return YES;
    } else if(action == @selector(delete:)){
        NSNumber *muid = [cellData safeObjectForKey:@"UID"];
        NSNumber *uid = [[[CommonFunc sharedCommon] getCurrentUVO] safeObjectForKey:@"UID"];
        if (muid.intValue == uid.intValue) {
            return YES;
        }
        return NO;
    } else if (action == @selector(transmit:)) {
        return YES;
    }
    return [super canPerformAction:action withSender:sender];
}

-(void)copy:(id)sender {
    UIPasteboard *gpBoard = [UIPasteboard generalPasteboard];
    NSString *content = [cellData safeObjectForKey:@"MContent"];
    //如果有表情把表情替换一下
    content = [[CommonFunc sharedCommon] copyConvertStr:content];
    [gpBoard setString:content];
}

-(void)transmit:(id)sender {
    //    UIActionSheet *transmitSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"网易微博",@"新浪微博",@"腾讯微博",@"搜狐微博",@"人人社区",@"邮件分享",@"短信分享", nil];
    UIActionSheet *transmitSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"邮件分享",@"短信分享", nil];
    [transmitSheet showInView:self.viewController.view];
    [transmitSheet release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
//    NSString *content = [cellData objectForKey:@"MContent"];
//    NSString *body = [NSString stringWithFormat:@"%@ %@",content,@"--分享来自友云(Youyun)，访问http://pengyouyun.cn/ 了解更多。"];
//    if (buttonIndex == 0) {
//        Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
//        if (mailClass !=nil) {
//            if ([mailClass canSendMail]) {
//                MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
//                picker.mailComposeDelegate =self;
//                [picker setSubject:content];
//                //如果有图片，发送图片附件
//                if (CGRectGetMaxY(self.pictureImageView.frame)>0) {
//                    NSString *imageURL = [cellData objectForKey:@"ImgUrl"];
//                    NSString *path = [NSString stringWithFormat:@"%@/resource/thumbnail/%@", URL_SERVER_BASE_PATH, imageURL];
//                    NSData *myData = [NSData dataWithContentsOfURL:[NSURL URLWithString:path]];
//                    
//                    if ([[self.pictureImageView.url description] hasSuffix:@"png"]) {
//                        [picker addAttachmentData:myData mimeType:@"image/png" fileName:imageURL];
//                    } else {
//                        [picker addAttachmentData:myData mimeType:@"image/jpeg" fileName:imageURL];
//                    }
//                }
//                [picker setMessageBody:body isHTML:YES];
//                [self.viewController presentModalViewController:picker animated:YES];
//                [picker release];
//            }else{
//                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示信息" message:@"该设备不支持邮件功能" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
//                [alert show];
//                [alert release];
//            }
//        }else{
//            NSString *path = [NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",@"", content, body];
//            NSURL *url = [NSURL URLWithString:[path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//            [[UIApplication sharedApplication] openURL:url];
//        }
//    } else if (buttonIndex == 1) {
//        Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
//        if (messageClass != nil) {
//            if ([MFMessageComposeViewController canSendText]) {
//                MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
//                [picker.navigationBar setTintColor:RGBCOLOR(27, 140, 233)];
//                picker.messageComposeDelegate = self;
//                picker.body = body;
//                
//                [self.viewController presentModalViewController: picker animated:YES];
//                [picker release];
//            } else {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示信息"
//                                                                message:@"该设备不支持短信功能"
//                                                               delegate:self
//                                                      cancelButtonTitle:nil
//                                                      otherButtonTitles:@"确定", nil];
//                [alert show];
//                [alert release];
//            }
//        } else {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"sms://%@", @""]]];
//        }
//    }
}


-(void)delete:(id)sender {
    //调删除接口
//    NSString *uid = [[CommonFunc sharedCommon] getCurrentUid];
//    [self.engine deleteMessage:[cellData objectForKey:@"MID"] uid:uid];
}

-(void)deleteMessageResponse {
    //成功后更新
    NSInteger atIndex = self.tag - 500;
    NSMutableArray *messages = [TKDataCache getCacheDataByURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetMessages, self.cid]];
    [messages removeObjectAtIndex:atIndex];
    [TKDataCache setCacheData:messages andURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetMessages,self.cid]];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRefreshChat object:nil];
    
    ZSTChatRoomViewController *chat = (ZSTChatRoomViewController*)self.viewController;
    [chat.mTable reloadData];
}

- (void)drawRect:(CGRect)rect
{
    [[ZSTModuleImage(@"module_snsa_addressBook_list_cell_bg.png") stretchableImageWithLeftCapWidth:0 topCapHeight:29] drawInRect:rect];
}

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    if (_delegate && [_delegate respondsToSelector:@selector(ClickViewDidClicked:)]) {
         [_delegate ClickViewDidClicked:self];
    }
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)commentSendFinished:(NSDictionary *)topic row:(NSInteger)row
{
    
}

- (void)tapTheHead:(UITapGestureRecognizer *)tap
{
    if (_delegate && [_delegate respondsToSelector:@selector(ClickViewDidClicked:)]) {
        [_delegate ClickViewDidClicked:self];
    }
}

@end
