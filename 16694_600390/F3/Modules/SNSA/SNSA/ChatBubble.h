//
//  ChatBubble.h
//  YouYun
//
//  Created by admin on 12-9-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kCommentMaxHeight INT_MAX

#define kChatBubbleWidth 225 //文字消息的水平宽度
#define kCommentBubbleWidth 180 //评论文字消息的水平宽度

#define kCommentHeadWidth 30 //评论头像高宽度

@interface ChatBubble : UIControl {

}

@property (nonatomic, retain) NIAttributedLabel *messageLabel;
@property (nonatomic, retain) TKAsynImageView *avtarImageView; //评论头像

@end
