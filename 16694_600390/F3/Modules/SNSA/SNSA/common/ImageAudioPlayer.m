//
//  ImageAudioPlayer.m
//  YouYun
//
//  Created by admin on 12-11-8.
//
//

#import "ImageAudioPlayer.h"

@implementation ImageAudioPlayer

@synthesize imageView;
@synthesize playingFile;

- (void)dealloc
{
    self.imageView = nil;
    self.playingFile = nil;
    [super dealloc];
}

@end
