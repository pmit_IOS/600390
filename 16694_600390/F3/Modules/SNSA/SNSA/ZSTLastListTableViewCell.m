//
//  ZSTLastListTableViewCell.m
//  SNSA
//
//  Created by LiZhenQu on 14-5-16.
//
//

#import "ZSTLastListTableViewCell.h"

@implementation ZSTLastListTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(10, 15, 300, 34)];
        bgView.backgroundColor = [UIColor whiteColor];
        bgView.layer.borderWidth = 1;
        bgView.layer.borderColor = [RGBCOLOR(241, 241, 241) CGColor];
        [self addSubview:bgView];
        
        UIImageView *addImg = [[UIImageView alloc] initWithFrame:CGRectMake(95, 8, 17, 17)];
        addImg.image = ZSTModuleImage(@"module_snsa_ico_add.png");
        [bgView addSubview:addImg];
        
        UILabel *moreLalel = [[UILabel alloc] initWithFrame:CGRectMake(20, 8, 300, 18)];
        moreLalel.backgroundColor = [UIColor clearColor];
        moreLalel.font = [UIFont systemFontOfSize:16];
        moreLalel.textAlignment = NSTextAlignmentCenter;
        moreLalel.textColor = RGBCOLOR(194, 194, 194);
        moreLalel.text = @"加入更多圈子";
        [bgView addSubview:moreLalel];
        
        [addImg release];
        [moreLalel release];
        [bgView release];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
