//
//  ZSTChatRoomViewController.m
//  SNSA
//
//  Created by LiZhenQu on 14-5-6.
//
//

#import "ZSTChatRoomViewController.h"
#import "ZSTChatRoomCell.h"
#import "ZSTUtils.h"
#import "CommonFunc.h"
#import "ChatBubble.h"
#import "ZSTTagUITapGestureRecognizer.h"
#import "NSDate+NSDateEx.h"
#import "VoiceConverter.h"
#import "ZSTReportViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define noDisableVerticalScrollTag 836913
#define noDisableHorizontalScrollTag 836914

#define ROOTFILEPATH [ZSTUtils pathForTempFinderOfOutbox]
#define AUDIOPATH [ROOTFILEPATH stringByAppendingPathComponent:[ZSTUtils audioDateString]]

#define KCommentLimit  3

@implementation UIImageView (ForScrollView)

- (void) setAlpha:(float)alpha {
    
    if (self.superview.tag == noDisableVerticalScrollTag) {
        if (alpha == 0 && self.autoresizingMask == UIViewAutoresizingFlexibleLeftMargin) {
            if (self.frame.size.width < 10 && self.frame.size.height > self.frame.size.width) {
                UIScrollView *sc = (UIScrollView*)self.superview;
                if (sc.frame.size.height < sc.contentSize.height) {
                    return;
                }
            }
        }
    }
    
    if (self.superview.tag == noDisableHorizontalScrollTag) {
        if (alpha == 0 && self.autoresizingMask == UIViewAutoresizingFlexibleTopMargin) {
            if (self.frame.size.height < 10 && self.frame.size.height < self.frame.size.width) {
                UIScrollView *sc = (UIScrollView*)self.superview;
                if (sc.frame.size.width < sc.contentSize.width) {
                    return;
                }
            }
        }
    }
    
    [super setAlpha:alpha];
}
@end


@interface ZSTChatRoomViewController ()
{
    NSString *toUserId;
}

@end

@implementation ZSTChatRoomViewController

@synthesize mTable;
@synthesize faceView;
@synthesize circle;
@synthesize engine;
@synthesize delegate;
@synthesize messages;
@synthesize voiceBg;
@synthesize btnicon;
@synthesize recordBtn;
@synthesize messageView;
@synthesize picBg;
@synthesize iconPickerController;
@synthesize uploadImageBg;
@synthesize uploadImageUrl;
@synthesize selectFaceArr;
@synthesize audioPlayer;
@synthesize recordImage;
@synthesize recorder;
@synthesize recordFile;
@synthesize keyboardCover;
@synthesize tipView;
@synthesize circleId;
@synthesize uploadVideoUrl;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.circle = nil;
    self.circleId = nil;
    self.engine = nil;
    self.delegate = nil;
    self.messages = nil;
    self.voiceBg = nil;
    self.btnicon = nil;
    self.recordBtn = nil;
    self.messageView = nil;
    self.picBg = nil;
    self.iconPickerController = nil;
    self.uploadImageBg = nil;
    self.mTable = nil;
    self.faceView = nil;
    self.audioPlayer = nil;
    self.recordImage = nil;
    self.recorder = nil;
    self.recordFile = nil;
    self.keyboardCover = nil;
    self.tipView = nil;
    [super dealloc];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
   
    sendNum = 0;
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    
    [self.engine getUserByUID:[ZSTF3Preferences shared].UID];
    
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"举报", nil) target:self selector:@selector (report)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString([self.circle safeObjectForKey:@"CName"], nil)];
    
    mTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, HEIGHT - 64 - 45) style:UITableViewStylePlain];
    mTable.delegate = self;
    mTable.dataSource = self;
    mTable.backgroundColor = [UIColor whiteColor];
    mTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:mTable];
    
    _isLoadingMore = NO;
    _isRefreshing = NO;
    if (_refreshHeaderView == nil) {
        
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-250.0, self.view.frame.size.width, 250.0)];
        _refreshHeaderView.delegate = self;
        [mTable addSubview:_refreshHeaderView];
        
        [_refreshHeaderView refreshLastUpdatedDate];
        [self performSelector:@selector(autoRefresh) withObject:nil afterDelay:1];
    }
    
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, 320, 55)];
        _loadMoreView.delegate = self;
        if (_hasMore) {
            mTable.tableFooterView = _loadMoreView;
        }
    }
    
    
    //返回刷新通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishComment) name:kNotificationRefreshChat object:nil];
    //默认为语音
    
    UIImage *rawBackground = ZSTModuleImage(@"module_snsa_MessageEntryBackground.png");
    
    self.voiceBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 45, 320, 45)];
    voiceBg.image = [rawBackground stretchableImageWithLeftCapWidth:1 topCapHeight:22];;
    [self.view addSubview:voiceBg];
    [voiceBg setUserInteractionEnabled:YES];
    
    UIButton *textBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    textBtn.frame = CGRectMake(10, 5, 40, 35);
    UIImage *image1 = ZSTModuleImage(@"module_snsa_btn_voice_n.png");
    UIImage *image2 = ZSTModuleImage(@"module_snsa_btn_voice_p.png");
    [textBtn setBackgroundImage:[image1 stretchableImageWithLeftCapWidth:5 topCapHeight:18] forState:UIControlStateNormal];
    [textBtn setBackgroundImage:[image2 stretchableImageWithLeftCapWidth:5 topCapHeight:18] forState:UIControlStateHighlighted];
    textBtn.tag = 0;
    [textBtn addTarget:self action:@selector(switchBtn:) forControlEvents:UIControlEventTouchUpInside];
    [voiceBg addSubview:textBtn];
    
    self.btnicon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 7, 21, 21)];
    btnicon.image = ZSTModuleImage(@"module_snsa_texticon.png");
    [textBtn addSubview:btnicon];
    
    UIImage *image3 = ZSTModuleImage(@"module_snsa_btn_voice_n.png");
    UIImage *image4 = ZSTModuleImage(@"module_snsa_btn_voice_p.png");
    self.recordBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    recordBtn.frame = CGRectMake(60, 5, 250, 35);
    [recordBtn setBackgroundImage:[image3 stretchableImageWithLeftCapWidth:5 topCapHeight:18] forState:UIControlStateNormal];
    [recordBtn setBackgroundImage:[image4 stretchableImageWithLeftCapWidth:5 topCapHeight:18] forState:UIControlStateHighlighted];
    [recordBtn addTarget:self action:@selector(beginRecord) forControlEvents:UIControlEventTouchDown];
    [recordBtn addTarget:self action:@selector(finishRecord) forControlEvents:UIControlEventTouchUpInside];
    [recordBtn addTarget:self action:@selector(cancelRecord) forControlEvents:UIControlEventTouchCancel];
    [recordBtn addTarget:self action:@selector(cancelRecord) forControlEvents:UIControlEventTouchUpOutside];
    
    [voiceBg addSubview:recordBtn];
    recordBtn.titleLabel.textColor = [UIColor blackColor];
    [recordBtn setTitle:@"按住说话" forState:UIControlStateNormal];
    [recordBtn setTitle:@"松手停止录音" forState:UIControlStateHighlighted];
    [recordBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [recordBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    self.messageView = [[TKPlaceHolderTextView alloc] initWithFrame:CGRectMake(60, 5, 200, 35)];
    self.messageView.font = [UIFont systemFontOfSize:14];
    [self.messageView setPlaceholder:@"说点什么吧"];
    self.messageView.returnKeyType = UIReturnKeyDone;
    self.messageView.enablesReturnKeyAutomatically = YES;
    self.messageView.delegate = self;
    UIImage *image5 = ZSTModuleImage(@"module_snsa_text_view_bg.png");
    self.messageView.backgroundImage = [image5 stretchableImageWithLeftCapWidth:floorf(image5.size.width/4) topCapHeight:floorf(image5.size.height/4)];
    self.messageView.hidden = YES;
    self.messageView.clearsContextBeforeDrawing = YES;
    [voiceBg addSubview:self.messageView];
    
    self.picBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 362+(iPhone5?88:0), 320, 55)];
    [picBg setBackgroundColor:[UIColor colorWithRed:243/255.0f green:243/255.0f blue:243/255.0f alpha:1]];
    [self.view addSubview:picBg];
    picBg.hidden = YES;
    [picBg setUserInteractionEnabled:YES];
    
    UIImage *image6 = ZSTModuleImage(@"module_snsa_btn_sound_bg.png");
    UIImage *image7 = [UIImage imageNamed:@"chat_picture_p.png"];
    
    UIButton *picBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    picBtn.frame = CGRectMake(15, 5, 145, 40);
    [picBtn setBackgroundImage:[image6 stretchableImageWithLeftCapWidth:floorf(image6.size.width/4) topCapHeight:floorf(image6.size.height/4)] forState:UIControlStateNormal];
    [picBtn setBackgroundImage:[image7 stretchableImageWithLeftCapWidth:floorf(image7.size.width/4) topCapHeight:floorf(image7.size.height/4)] forState:UIControlStateHighlighted];
    [picBtn addTarget:self action:@selector(addPic) forControlEvents:UIControlEventTouchUpInside];
    [picBg addSubview:picBtn];
    picBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    picBtn.titleLabel.textColor = [UIColor blackColor];
    [picBtn setTitle:@"插入图片" forState:UIControlStateNormal];
    [picBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIButton *faceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    faceBtn.frame = CGRectMake(165, 5, 145, 40);
    [faceBtn setBackgroundImage:[image6 stretchableImageWithLeftCapWidth:floorf(image6.size.width/4) topCapHeight:floorf(image6.size.height/4)] forState:UIControlStateNormal];
    [faceBtn setBackgroundImage:[image7 stretchableImageWithLeftCapWidth:floorf(image7.size.width/4) topCapHeight:floorf(image7.size.height/4)] forState:UIControlStateHighlighted];
    [faceBtn addTarget:self action:@selector(addFace) forControlEvents:UIControlEventTouchUpInside];
    [picBg addSubview:faceBtn];
    faceBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    faceBtn.titleLabel.textColor = [UIColor blackColor];
    [faceBtn setTitle:@"插入表情" forState:UIControlStateNormal];
    [faceBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    UIImageView *iconB2 = [[UIImageView alloc] initWithFrame:CGRectMake(12, 10, 21, 21)];
    iconB2.image = ZSTModuleImage(@"module_snsa_ico_picture.png");
    [picBtn addSubview:iconB2];
    [iconB2 release];
    
    UIImageView *iconB3 = [[UIImageView alloc] initWithFrame:CGRectMake(12, 10, 21, 21)];
    iconB3.image = ZSTModuleImage(@"module_snsa_ico_face.png");
    [faceBtn addSubview:iconB3];
    [iconB3 release];
    
    //上传图片的显示图
    self.uploadImageBg = [[UIImageView alloc] initWithFrame:CGRectMake(265, 288+(iPhone5?88:0)-20, 45, 44)];
    uploadImageBg.backgroundColor = [UIColor clearColor];
    uploadImageBg.hidden = YES;
    uploadImageBg.image = ZSTModuleImage(@"module_snsa_bg_chat_pictuerbox.png");
    [self.view addSubview:uploadImageBg];
    
    UIImageView *uploadImage = [[UIImageView alloc] initWithFrame:CGRectMake(6, 5, 33, 34)];
    uploadImage.backgroundColor = [UIColor clearColor];
    uploadImage.tag = 12;
    [uploadImageBg addSubview:uploadImage];
    //release
    [uploadImage release];

//    self.messages = (NSMutableArray *)[self.dao topicAndCommentOfCircle:[circle objectForKey:@"CID"] startId:0 topicCount:kOneLimit commentCount:KCommentLimit];
//    [mTable reloadData];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeSound) name:SNSA_SOUND_CLOSE object:nil];
    
    if (false)
    {
        mTable.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
        self.voiceBg.hidden = YES;
    }
}

- (id)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillShow:)
													 name:UIKeyboardWillShowNotification
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(keyboardWillHide:)
													 name:UIKeyboardWillHideNotification
												   object:nil];
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self performSelector:@selector(autoRefresh) withObject:nil afterDelay:0.4];
    
    if ([self.audioPlayer isPlaying]) {
        [self.audioPlayer stop];
        [self.audioPlayer.imageView stopAnimating];
        self.audioPlayer = nil;
    }
    
    NSInteger stateIndex = [[[NSUserDefaults standardUserDefaults] objectForKey:@"stateIndex"] integerValue];
    if (stateIndex == 2) {
        self.voiceBg.hidden = YES;
        mTable.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
    }
    else {
        self.voiceBg.hidden = NO;
    }
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if ([self.audioPlayer isPlaying]) {
        [self.audioPlayer stop];
        [self.audioPlayer.imageView stopAnimating];
         self.audioPlayer = nil;
    }
}

- (void) closeSound
{
    if ([self.audioPlayer isPlaying]) {
        [self.audioPlayer stop];
        [self.audioPlayer.imageView stopAnimating];
         self.audioPlayer = nil;
    }
}

- (void) report
{
    NSDictionary *userDic = [[CommonFunc sharedCommon] getCurrentUVO];
    NSString *userId;
    if (!userDic || [userDic isKindOfClass:[NSNull class]]) {
        userId = @"";
    } else {
        userId = [userDic valueForKey:@"UID"];
    }
    ZSTReportViewController *controller = [[ZSTReportViewController alloc] init];
    controller.circleId = [self.circle safeObjectForKey:@"CID"];
    controller.userId = userId;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)switchBtn:(id)sender {//切换
    if ([self.messageView isFirstResponder]) {
        [self.messageView resignFirstResponder];
    }
    
    if (!self.uploadImageBg.hidden) {
        
        UIImageView *imageView = (UIImageView*)[self.uploadImageBg viewWithTag:12];
        imageView.image = nil;
        self.uploadImageBg.hidden = YES;
    }
    
    UIButton *btn = (UIButton*)sender;
    if ([btn tag]==0) {
        btn.tag = 1;
        self.btnicon.image = ZSTModuleImage(@"module_snsa_voiceicon.png");
        self.voiceBg.frame = CGRectMake(0, 337+(iPhone5?88:0)-20, 320, 45);
        self.recordBtn.frame = CGRectMake(270, 5, 40, 35);
        recordBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [recordBtn setTitle:@"发送" forState:UIControlStateNormal];
        [recordBtn setTitle:@"发送" forState:UIControlStateHighlighted];
        [recordBtn removeTarget:self action:@selector(beginRecord) forControlEvents:UIControlEventTouchDown]; //按下
        [recordBtn removeTarget:self action:@selector(finishRecord) forControlEvents:UIControlEventTouchUpInside]; //按起
        
        [recordBtn addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
        
        self.mTable.frame = CGRectMake(0, 0, self.view.width, 416-35+(iPhone5?88:0)-55);
        
        self.messageView.hidden = NO;
        self.picBg.hidden = NO;
        isRecord = NO;
        if (faceView) {
            //            self.voiceBg.frame = CGRectMake(0, 337-faceView.size.height, 320, 45);
            //            self.picBg.frame = CGRectMake(0, 382-faceView.size.height, 320, 35);
            self.voiceBg.frame = CGRectMake(0, 337+(iPhone5?88:0)-20, 320, 45);
            self.picBg.frame = CGRectMake(0, 362+(iPhone5?88:0), 320, 55);
            self.faceView.frame = CGRectMake(0, 417+(iPhone5?88:0), 320, 0);
            faceView.hidden = NO;
        }
    } else {
        btn.tag = 0;
        self.btnicon.image = ZSTModuleImage(@"module_snsa_texticon.png");
        self.voiceBg.frame = CGRectMake(0, 372+(iPhone5?88:0), 320, 45);
        self.recordBtn.frame = CGRectMake(60, 5, 250, 35);
        recordBtn.titleLabel.font = [UIFont systemFontOfSize:18];
        [recordBtn setTitle:@"按住说话" forState:UIControlStateNormal];
        [recordBtn setTitle:@"松手停止录音" forState:UIControlStateHighlighted];
        [recordBtn removeTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
        
        [recordBtn addTarget:self action:@selector(beginRecord) forControlEvents:UIControlEventTouchDown];
        [recordBtn addTarget:self action:@selector(finishRecord) forControlEvents:UIControlEventTouchUpInside];
        
        self.mTable.frame = CGRectMake(0, 0, self.view.width, 416-35+(iPhone5?88:0));
        
        self.messageView.hidden = YES;
        self.picBg.hidden = YES;
        isRecord = YES;
        if (faceView) {
            faceView.hidden = YES;
        }
    }
}

-(void)addPic {
    
    UIActionSheet *actionSheet = nil;
    //是否支持拍照
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"获取图片"
                                                  delegate:self
                                         cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"拍照", @"本地图片", nil];
    } else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"获取图片"
                                                  delegate:self
                                         cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"本地图片", nil];
    }
    
    [actionSheet showInView:self.view];
    [actionSheet release];
}

-(void)addFace {
    
    [self.messageView resignFirstResponder];
    //表情
    if (self.messageView.isFirstResponder) {
        [UIView animateWithDuration:0.25 animations:^{
            if (faceView) {
                self.voiceBg.frame = CGRectMake(0, 337-self.faceView.size.height+(iPhone5?88:0)-20, 320, 45);
            } else {
                self.voiceBg.frame = CGRectMake(0, 337+(iPhone5?88:0), 320, 45);
            }
            
            self.uploadImageBg.frame = CGRectMake(265, self.voiceBg.frame.origin.y - 49, 45, 44);
        }];
    }
	// commit animations
    if (!faceView) {
        self.selectFaceArr = [NSMutableArray array];
        
        self.faceView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 417+(iPhone5?88:0), 320, 181)];
        faceView.contentSize = CGSizeMake(320, 576);
        [faceView setMaximumZoomScale:1.0f];
        [faceView setMinimumZoomScale:1.0f];
        faceView.backgroundColor = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:1];
        faceView.showsHorizontalScrollIndicator = YES;
        [faceView flashScrollIndicators];
        faceView.tag = noDisableVerticalScrollTag;
        [self.view addSubview:faceView];
        for(int i=0; i<72; i++) {
            NSString *faceName = @"";
            if (i<9) {
                faceName = [NSString stringWithFormat:@"face00%d.png",i+1];
            } else if(i<99) {
                faceName = [NSString stringWithFormat:@"face0%d.png",i+1];
            }
//            UIButton *faceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//            faceBtn.frame = CGRectMake(10+53*(i%6), 2+48*(i/6), 36, 36);
//            [faceBtn setImage:[UIImage imageNamed:faceName] forState:UIControlStateNormal];
//            [faceBtn setBackgroundImage:ZSTModuleImage(@"module_snsa_bg_chat_face.png") forState:UIControlStateHighlighted];
//            [faceBtn addTarget:self action:@selector(selectFace:) forControlEvents:UIControlEventTouchUpInside];
//            faceBtn.tag = i+1;
//            [self.faceView addSubview:faceBtn];
            
            UIImageView *faceIV = [[UIImageView alloc] initWithFrame:CGRectMake(12+53*(i%6), 4+48*(i/6), 32, 32)];
            faceIV.image = [UIImage imageNamed:faceName];
            [self.faceView addSubview:faceIV];
            
            UIButton *faceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            faceBtn.frame = CGRectMake(10+53*(i%6), 2+48*(i/6), 36, 36);
            [faceBtn addTarget:self action:@selector(selectFace:) forControlEvents:UIControlEventTouchUpInside];
            faceBtn.tag = i+1;
            [self.faceView addSubview:faceBtn];
        }
        //来一个动画
        [UIView animateWithDuration:0.25 animations:^{
            self.faceView.frame = CGRectMake(0, 236+(iPhone5?88:0), 320, 181);
        }];
    } else {
        [UIView animateWithDuration:0.25 animations:^{
            if (self.faceView.size.height == 181 ) {
                self.faceView.frame = CGRectMake(0, 417+(iPhone5?88:0), 320, 0);
            } else {
                self.faceView.frame = CGRectMake(0, 236+(iPhone5?88:0), 320, 181);
            }
        }];
    }
    [UIView animateWithDuration:0.25 animations:^{
        self.voiceBg.frame = CGRectMake(0, 337-self.faceView.size.height+(iPhone5?88:0)-20, 320, 45);
        self.picBg.frame = CGRectMake(0, 362-self.faceView.size.height+(iPhone5?88:0), 320, 55);
        self.uploadImageBg.frame = CGRectMake(265, self.voiceBg.frame.origin.y - 49, 45, 44);
	}];
}

-(void)selectFace:(id)sender {
    NSInteger index = [sender tag];
    
    //    NSString *selectText = [self.faceTilteArr objectAtIndex:index-1];
    NSString *selectText = [[CommonFunc sharedCommon].faceTilteArr objectAtIndex:index-1];
    self.messageView.text = [NSString stringWithFormat:@"%@[%@]",self.messageView.text,selectText];
    
    [self.selectFaceArr addObject:[NSString stringWithFormat:@"%@",selectText]];
}


-(void)send {
    if ([self.messageView isFirstResponder]) {
        [self.messageView resignFirstResponder];
    }
    if (self.messageView.text != nil && [self.messageView.text isEmptyOrWhitespace]) {
        [TKUIUtil showHUDInView:self.view withText:@"内容不能为空！" withImage:[UIImage imageNamed:@"icon_warning.png"] withCenter:CGPointMake(160, 100)];
        [TKUIUtil hiddenHUDAfterDelay:2];
        
        return;
    }
    
    [TKUIUtil showHUDInView:self.view
                   withText:NSLocalizedString(@"正在发送", @"正在发送")
                  withImage:nil
                 withCenter:CGPointMake(160, 100)];
    
    UIImageView *imageV = (UIImageView*)[self.uploadImageBg viewWithTag:12];
    UIImage *attachmentImage = imageV.image;
    if (attachmentImage) {
         [self.engine upLoadFile:UIImagePNGRepresentation(attachmentImage) progressDelegate:nil suffix:@"png"];
    } else {
        NSString *str = [self convertMessage:[self.messageView.text stringByTrimmingWhitespaceCharacters]];
        [self.engine sendMessage:[self.circle safeObjectForKey:@"CID"] parentID:nil content:str fileKey:nil];
    }
}


-(void)beginRecord {
    if (!self.recordImage) {
        isRecord = YES;
        self.recordImage = [[UIImageView alloc] initWithFrame:CGRectMake(90, 90, 135, 195)];
        recordImage.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 135, 40)];
        title.text = @"开始说话";
        title.backgroundColor = [UIColor clearColor];
        title.textColor = [UIColor whiteColor];
        title.textAlignment = NSTextAlignmentCenter;
        title.font = [UIFont systemFontOfSize:16];
        [recordImage addSubview:title];
        [title release];
        
        UIImageView *goldphone = [[UIImageView alloc] initWithFrame:CGRectMake(30, 43, 75, 115)];
        goldphone.animationImages = [NSArray arrayWithObjects:
                                     ZSTModuleImage(@"module_snsa_img_golden_microphone.png"),
                                     ZSTModuleImage(@"module_snsa_img_gray_microphone.png"),nil];
        goldphone.animationDuration = 1.0;
        goldphone.animationRepeatCount = 0;
        goldphone.tag = 100;
        [self.recordImage addSubview:goldphone];
        [goldphone release];
        
        UILabel *prompt = [[UILabel alloc] initWithFrame:CGRectMake(0, 162, 135, 40)];
        prompt.text = @"向上滑动手指取消发送";
        prompt.backgroundColor = [UIColor clearColor];
        prompt.textColor = [UIColor whiteColor];
        prompt.textAlignment = NSTextAlignmentCenter;
        prompt.font = [UIFont systemFontOfSize:12];
        [recordImage addSubview:prompt];
        [prompt release];
    }
    [self.view addSubview:recordImage];
    UIImageView *v = (UIImageView*)[recordImage viewWithTag:100];
    [v startAnimating];
    
    if (!self.recorder) {
        AVAudioSession *session = [AVAudioSession sharedInstance]; //开启录音session，并设置好相关的session类型
        NSError *sessionError;
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
        if(session == nil) {
            NSLog(@"Error creating session: %@", [sessionError description]);
        } else {
            [session setActive:YES error:nil];
        }
        
        //路径指到临时目录
//        NSString *uid = [[CommonFunc sharedCommon] getCurrentUid];
        
//        NSString *uid = [[NSUserDefaults standardUserDefaults] objectForKey:userKeyChainIdentifier];
//        //目录中有空格时会报错哎
//        self.recordFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat: @"%@_%.0f.%@",uid,[NSDate timeIntervalSinceReferenceDate] * 1000000.0, @"wav"]];
//        //        NSLog(@"-----------------%@",recordFile);
//        NSError *error;
//        self.recorder = [[AVAudioRecorder alloc] initWithURL:[NSURL URLWithString:self.recordFile] settings:[self audioRecordingSettings] error:&error];
//
        
        NSError *error;
        
        // Recording settings//PCM脉冲编码调制
        NSMutableDictionary *settings = [[NSMutableDictionary alloc] init];
        [settings setValue: [NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];//音频格式（此处为非常大的文件）
        [settings setValue: [NSNumber numberWithFloat:8000.0] forKey:AVSampleRateKey];//每秒采样8000次（采样率）
        [settings setValue: [NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey]; // mono单声道
        [settings setValue: [NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];//线性pcm位深度（采样位数）
        [settings setValue: [NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
        [settings setValue: [NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];//采样信号是整数还是浮点数
        
        [self.recordFile release];
        self.recordFile = [AUDIOPATH retain];
        
        NSURL *url = [NSURL fileURLWithPath:self.recordFile];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:self.recordFile]) {
            [[NSFileManager defaultManager] removeItemAtPath:self.recordFile error:nil];
        }
        
        self.recorder = [[[AVAudioRecorder alloc] initWithURL:url settings:settings error:&error] autorelease];
        [settings release];
//            [self.recorder recordForDuration:59];//设置录音上限为59秒
        if (!self.recorder)
        {
            NSLog(@"Error: %@", [error localizedDescription]);
            return;
        }
        
        [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayAndRecord error:nil];
        
        [[AVAudioSession sharedInstance] setActive:YES error:nil];
        
        UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
        
        AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(audioRouteOverride), &audioRouteOverride);
        
        [recorder prepareToRecord];
        [recorder record];
        beginTime = [NSDate timeIntervalSinceReferenceDate];
    }
}

-(NSDictionary *) audioRecordingSettings//android那边的也是这样
{
    NSDictionary *result = nil;
    NSMutableDictionary *settings =[NSMutableDictionary dictionary];
    //设置录音格式
    [settings setValue:[NSNumber numberWithInteger:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    //设置采样率，标准6800-8000,值越小噪声越小，把周围人的音也录进去了。如果大则噪音也大
    [settings setValue:[NSNumber numberWithFloat:8000.f] forKey:AVSampleRateKey];
    //设置频道数
    [settings setValue:[NSNumber numberWithInteger:1] forKey:AVNumberOfChannelsKey];
    //4 采样位数  默认 16  8, 16, 24, 32
    [settings setObject:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    //设置质量
    [settings setValue:[NSNumber numberWithInteger:AVAudioQualityMin] forKey:AVEncoderAudioQualityKey];
    result = [NSDictionary dictionaryWithDictionary:settings];
    return  result;
}

-(void)finishRecord {
    //完成录音，对文件进行编码。并进行上传
    UIImageView *v = (UIImageView*)[recordImage viewWithTag:100];
    [v stopAnimating];
    [self.recordImage removeFromSuperview];
    [self.recorder stop];
    self.recorder = nil;
    
    NSString *amrFile = [self.recordFile stringByReplacingOccurrencesOfString:@"wav" withString:@"amr"];
    //将wav格式的音频文件转成amr格式上传，并保存一份在本地临时文件中
    [VoiceConverter wavToAmr:self.recordFile amrSavePath:amrFile];
    //上传处理，完了再发送一条消息
    NSData *amrData = [NSData dataWithContentsOfFile:amrFile];
    
    double overTime = [NSDate timeIntervalSinceReferenceDate];
    duration = floor(overTime - beginTime + 0.5); //四舍五入
    beginTime = 0;
//    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
////    NSString *uid = [[CommonFunc sharedCommon] getCurrentUid];
//     NSString *uid = [[NSUserDefaults standardUserDefaults] objectForKey:userKeyChainIdentifier];
//    [dic setObject:uid forKey:@"UID"];
//    [dic setObject:[self.circle objectForKey:@"CID"] forKey:@"CID"];
//    [dic setObject:[NSNumber numberWithInteger:duration] forKey:@"duration"];
//    [self.engine upLoadFile:amrData progressDelegate:nil suffix:@"amr" withDic:dic];
    
        #ifdef DEBUG
    
        NSLog(@"----- %@ --------",@(duration));
    
        #endif
    
    if (duration < 1) {
        
        [TKUIUtil alertInWindow:@"说话时间太短" withImage:nil];
        return;
    }
    
      [self.engine upLoadVideoFile:amrData progressDelegate:nil suffix:@"amr"];
}

-(void)cancelRecord {//手指滑到按钮之外时触发取消录音
    if (isRecord) {
        UIImageView *v = (UIImageView*)[recordImage viewWithTag:100];
        [v stopAnimating];
        [self.recordImage removeFromSuperview];
        [self.recorder stop];
        self.recorder = nil;
        
        [TKUIUtil alertInWindow:@"取消发送成功" withImage:nil];
    }
}

- (void)upLoadVideoFileResponse:(NSString *)fileKey
{
    TKDINFO(@"fileKey == %@", fileKey);
    
    [self.engine sendvideoMessage:[self.circle safeObjectForKey:@"CID"] parentID:nil content:@"发表了语音" fileKey:fileKey videoTimeLength:duration];
    self.uploadVideoUrl = fileKey;
}

- (void)upLoadFileResponse:(NSString *)fileKey
{
    //    TKDINFO(@"fileKey == %@", fileKey);
    //如果有图片，发完图片成功后，再发送文字消息。如果没图片，单发送文字消息
    if ([fileKey rangeOfString:@"amr"].length>0) { //音频
        //把本地文件名更新一下
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        NSString *temp = [fileKey stringByReplacingOccurrencesOfString:@"amr" withString:@"wav"];
        NSString *filePath2 = [NSTemporaryDirectory() stringByAppendingPathComponent:temp];
        NSError *error;
        if ([fileMgr moveItemAtPath:self.recordFile toPath:filePath2 error:&error] != YES)
            NSLog(@"Unable to move file: %@", [error localizedDescription]);
        //delete amr
        temp = [self.recordFile stringByReplacingOccurrencesOfString:@"wav" withString:@"amr"];
        [fileMgr removeItemAtPath:temp error:&error];
        //        NSLog(@"Documentsdirectory: %@",[fileMgr contentsOfDirectoryAtPath:documentsDirectoryerror:&error]);
        self.recordFile = nil;
        //更新一下聊天列表
        NSDictionary *userDic = [[CommonFunc sharedCommon] getCurrentUVO];
        
        NSMutableDictionary *issueDic = [NSMutableDictionary dictionary];
        NSString *str = @"我刚刚使用新版客户端发表了一条语音消息，请升级客户端后收听吧。";
        [issueDic setObject:str forKey:@"MContent"];
        [issueDic setObject:[NSNumber numberWithInteger:floorf([[NSDate date] timeIntervalSince1970])] forKey:@"AddTime"];
        [issueDic setObject:[NSNumber numberWithInt:0] forKey:@"Parentid"];
        [issueDic setObject:[NSNumber numberWithInt:2] forKey:@"Type"];
        if (userDic) {
            [issueDic setObject:[userDic valueForKey:@"ImgUrl"] forKey:@"New_UAvatar"];
            NSString *names = [[userDic objectForKey:@"UserName"] objectForKey:@"New_UName"];
            if ([self isPureInt:names] && names.length >= 11)
            {
                NSString *names = [userDic valueForKey:@"UserName"];
                names = [names stringByReplacingCharactersInRange:NSMakeRange(5, 4) withString:@"****"];
                [issueDic setObject:names forKey:@"UName"];
            }
            else
            {
                [issueDic setObject:[userDic valueForKey:@"UserName"] forKey:@"New_UName"];
            }
            [issueDic setObject:[userDic valueForKey:@"UID"] forKey:@"UID"];
        }
        [issueDic setObject:@"768" forKey:@"MID"];
        [issueDic setObject:fileKey forKey:@"FileUrl"];
        [issueDic setObject:[NSNumber numberWithInteger:duration] forKey:@"VideoTimeLength"];
        [issueDic setObject:@"" forKey:@"ImgUrl"];
        
        
        [self.messages insertObject:issueDic atIndex:0];
        [mTable reloadData];
        [TKDataCache setCacheData:self.messages andURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetMessages, [circle safeObjectForKey:@"CID"]]];
        //更新消息id号
        NSMutableDictionary *readDic = [TKDataCache getCacheDataByURLKey:@"alreadyReadDic"];
        if (!readDic) {
            readDic = [NSMutableDictionary dictionary];
        }
        [readDic setValue:[NSString stringWithFormat:@"%@",@"768"] forKey:[NSString stringWithFormat:@"%@",[circle safeObjectForKey:@"CID"]]];
        [TKDataCache setCacheData:readDic andURLKey:@"alreadyReadDic"];
        
    } else {
        NSString *str = [self convertMessage:[self.messageView.text stringByTrimmingWhitespaceCharacters]];
        [self.engine sendMessage:[self.circle safeObjectForKey:@"CID"] parentID:nil content:str fileKey:fileKey];
        if (self.uploadImageBg.hidden == NO) {
            self.uploadImageBg.hidden = YES;
        }
        self.uploadImageUrl = fileKey;
    }
}

//- (void)upLoadFileResponse:(NSString *)fileKey
//{
//    TKDINFO(@"fileKey == %@", fileKey);
//    self.uploadImageUrl = fileKey;
////     NSString *str = [self convertMessage:[self.messageView.text stringByTrimmingWhitespaceCharacters]];
//    [self.engine sendMessage:[self.circle objectForKey:@"CID"] parentID:nil content:self.messageView.text fileKey:fileKey];
//    if (!self.uploadImageBg.hidden) {
//        self.uploadImageBg.hidden = YES;
//    }
//}

- (void)sendMessageResponse
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil showHUDInView:self.view withText:@"发表成功！" withImage:[UIImage imageNamed:@"icon_smile_face.png"] withCenter:CGPointMake(160, 100)];
    [TKUIUtil hiddenHUDAfterDelay:2];
    
    //聊天更新列表
    NSDictionary *userDic = [[CommonFunc sharedCommon] getCurrentUVO];
    
    NSMutableDictionary *issueDic = [NSMutableDictionary dictionary];
    NSString *str = [self convertMessage:[self.messageView.text stringByTrimmingWhitespaceCharacters]];
    [issueDic setObject:str forKey:@"MContent"];
    [issueDic setObject:[NSNumber numberWithInteger:floorf([[NSDate date] timeIntervalSince1970])] forKey:@"AddTime"];
    [issueDic setObject:[NSNumber numberWithInt:0] forKey:@"Parentid"];
    [issueDic setObject:[NSNumber numberWithInt:0] forKey:@"Type"];
    if (userDic) {
        NSString *names = [[userDic objectForKey:@"UserName"] objectForKey:@"New_UName"];
        if ([self isPureInt:names] && names.length >= 11)
        {
            
            NSString *names = [userDic valueForKey:@"UserName"];
            names = [names stringByReplacingCharactersInRange:NSMakeRange(5, 4) withString:@"****"];
            [issueDic setObject:names forKey:@"UName"];
        }
        else
        {
            [issueDic setObject:[userDic valueForKey:@"UserName"] forKey:@"New_UName"];
        }
        [issueDic setObject:[userDic valueForKey:@"UID"] forKey:@"UID"];
        [issueDic setObject:[userDic valueForKey:@"ImgUrl"] forKey:@"New_UAvatar"];
    } else {
       
        NSString *msisdn = [ZSTF3Preferences shared].loginMsisdn;
        msisdn = [msisdn stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
        [issueDic setObject:msisdn forKey:@"New_UName"];
    }
    if (self.uploadImageUrl) {
        [issueDic setObject:self.uploadImageUrl forKey:@"ImgUrl"];
    }
    if (self.uploadVideoUrl) {
        [issueDic setObject:self.uploadVideoUrl forKey:@"VideoUrl"];
        [issueDic setObject:[NSNumber numberWithInteger:duration] forKey:@"VideoTimeLength"];
    }
    
    [self.messages insertObject:issueDic atIndex:0];
    [mTable reloadData];
//    [self performSelector:@selector(autoRefresh) withObject:nil afterDelay:0.4];
    [TKDataCache setCacheData:self.messages andURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetMessages, [circle safeObjectForKey:@"CID"]]];
    //清空输入框
    self.messageView.text = @"";
    //置空图片和语音
    UIImageView *imageView = (UIImageView*)[self.uploadImageBg viewWithTag:12];
    imageView.image = nil;
    self.uploadImageUrl = nil;
    self.uploadVideoUrl = nil;
    //更新消息id号
    NSMutableDictionary *readDic = [TKDataCache getCacheDataByURLKey:@"alreadyReadDic"];
    if (!readDic) {
        readDic = [NSMutableDictionary dictionary];
    }
    [readDic setValue:[NSString stringWithFormat:@"%@",@"768"] forKey:[NSString stringWithFormat:@"%@",[circle safeObjectForKey:@"CID"]]];
    [TKDataCache setCacheData:readDic andURLKey:@"alreadyReadDic"];
}

-(NSString *)convertMessage:(NSString*)message { //把文字中的表情替换为图片名
    for (int i=0; i<[selectFaceArr count]; i++) {
        NSString *str = [NSString stringWithFormat:@"[%@]",[selectFaceArr objectAtIndex:i]];
        //        NSInteger index = [self.faceTilteArr indexOfObject:[selectFaceArr objectAtIndex:i]];
        NSInteger index = [[CommonFunc sharedCommon].faceTilteArr indexOfObject:[selectFaceArr objectAtIndex:i]];
        NSString *faceName = @"";
        if (index<9) {
            faceName = [NSString stringWithFormat:@"[face00%@]",@(index+1)];
        } else if(index<99) {
            faceName = [NSString stringWithFormat:@"[face0%@]",@(index+1)];
        }
        if (![faceName isEqualToString:@""]) {
            message = [message stringByReplacingOccurrencesOfString:str withString:faceName];
        }
    }
    //    message = [message stringByReplacingOccurrencesOfString:@" " withString:@""];
    return message;
}

-(void)finishComment {
//    self.messages = [TKDataCache getCacheDataByURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetMessages, [circle objectForKey:@"CID"]]];
    [mTable reloadData];
}

#pragma mark -------UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.messages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"ChatRoomCell";
    ZSTChatRoomCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[ZSTChatRoomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
    }
    
    NSDictionary *cellData = [self.messages objectAtIndex:indexPath.row];
    cell.cellData = cellData;
    cell.cid = [self.circle safeObjectForKey:@"CID"];
    cell.tag = indexPath.row +500;
    cell.delegate = self;

    //头像图片
    NSString *fileKey = [cellData safeObjectForKey:@"New_UAvatar"];
//    [cell.avtarImageView clear];
    
    NSInteger firstHeight = 0;
    if (indexPath.row == 0) {
        firstHeight = 14;
    } else {
        firstHeight = 0;
    }
    cell.avtarImageView.frame = CGRectMake(10, 10+firstHeight, 45, 45);
//    [cell.avtarImageView addTarget:self action:@selector(openUserInfo:) forControlEvents:UIControlEventTouchUpInside];
    cell.avtarImageView.tag = [[cellData safeObjectForKey:@"UID"] intValue];
    if (fileKey != nil && ![fileKey isEmptyOrWhitespace]) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", fileKey]];
        if (url) {
//            [cell.avtarImageView setUrl:url];
//            [cell.avtarImageView loadImage];
            [cell.avtarImageView setImageWithURL:url placeholderImage:ZSTModuleImage(@"module_snsa_default_avatar.png")];
        }
    }
    //消息图片
    NSString *imageURL = [cellData safeObjectForKey:@"ImgUrl"];
//    [cell.pictureImageView clear];
    CGFloat newHeight = 0.0;
    if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
//        cell.pictureImageView.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",imageURL]];
        newHeight = [[CommonFunc sharedCommon] getHeightByURL:[[NSURL URLWithString:imageURL] description] assignWidth:245];
        //            NSLog(@"%@  %f",[cell.pictureImageView.url description],newHeight);
//        [cell.pictureImageView loadImage];
        [cell.pictureImageView setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:@"activity-photo-frame.png"]];
        cell.pictureImageView.frame = CGRectMake(65, 10+firstHeight, 245, newHeight);
        cell.pictureImageView.hidden = NO;
        cell.contentLabel.hidden = NO;
    }  else {
        cell.pictureImageView.frame = CGRectZero;
        cell.pictureImageView.hidden = YES;
        cell.contentLabel.hidden = YES;
    }
    
    //先设置大背景图片，确定位置
    [cell.chatBg removeAllSubviews];

    if (cell.pictureImageView.hidden) {
        
        cell.chatBg.frame = CGRectMake(60, newHeight+10+firstHeight, 250, 10);//60 =
        
        cell.chatBg.image = [ZSTModuleImage(@"module_snsa_feed-thought-bubble.png") stretchableImageWithLeftCapWidth:20 topCapHeight:25];
        
        [self setCellData:cellData hasPic:NO parentView:cell.chatBg index:indexPath.row];
    } else {//有图片的
        cell.chatBg.frame = CGRectMake(65, newHeight+14+firstHeight, 245, 10);
        cell.chatBg.image = [ZSTModuleImage(@"module_snsa_feed-comment-bubble.png") stretchableImageWithLeftCapWidth:34 topCapHeight:15];
        
//         NSString *content = [cellData objectForKey:@"MContent"];
//         NSString *pContent = [[CommonFunc sharedCommon] getDisplayStr:content];
//         NSAttributedString *attributedString = [[CommonFunc sharedCommon] getMessageLabelString:cellData isSmall:NO content:pContent addName:YES];
//         cell.contentLabel.attributedString = attributedString;
//         CGSize size = [ZSTChatBubble suggestSizeForCmtAttributedString:attributedString];
//        cell.contentLabel.frame = CGRectMake(70, 6, kChatBubbleWidth+2, size.height);
//        cell.pictureImageView.frame = CGRectMake(65, CGRectGetMaxY(cell.contentLabel.frame)+10, 245, newHeight);
        [self setCellData:cellData hasPic:YES parentView:cell.chatBg index:indexPath.row];
    }
    return cell;
}

//chatBg 为白色背景，得调整对应尺寸
-(void)setCellData:(NSDictionary *)cellData hasPic:(BOOL)hasPic parentView:(UIImageView*)chatBg index:(NSInteger)index {
    CGFloat newHeight = 0.0;
    CGFloat height = 0.0; //在这里计算
    //我的消息
    ChatBubble *myChat = [[ChatBubble alloc] init];
//    myChat.backgroundColor = [UIColor redColor];
    //    myChat.messageLabel.backgroundColor = [UIColor redColor];
    
    NSInteger uid = [[cellData safeObjectForKey:@"UID"] intValue];
    NSAttributedString *attributedString;
    //获取消息内容做一下处理，然后提取图片。
    NSString *fileUrl = [cellData safeObjectForKey:@"VideoUrl"];
    if (fileUrl==nil || [@"" isEqualToString:fileUrl]) {//文字加图片
        height += 15;
        NSString *names = [[CommonFunc sharedCommon] trimStr:[cellData safeObjectForKey:@"New_UName"]];
        NSString *uName = @"";
        if ([self isPureInt:names] && names.length >= 11)
        {
            uName = [names stringByReplacingCharactersInRange:NSMakeRange(5, 4) withString:@"****"];
        }
        else
        {
            uName = names;
        }
        NSString *content = [cellData safeObjectForKey:@"MContent"];
        NSString *pContent = [[CommonFunc sharedCommon] getDisplayStr:content];
        NSArray *originArr;
        if (hasPic) {
            attributedString = [[CommonFunc sharedCommon] getMessageLabelString:cellData isSmall:YES content:pContent addName:YES];
            originArr = [[CommonFunc sharedCommon] getFaceOrigin:[NSString stringWithFormat:@"%@: %@",uName,content] maxWidth:kChatBubbleWidth hasPic:YES];
        } else {
            attributedString = [[CommonFunc sharedCommon] getMessageLabelString:cellData isSmall:NO content:pContent addName:YES];
            originArr = [[CommonFunc sharedCommon] getFaceOrigin:[NSString stringWithFormat:@"%@: %@",uName,content] maxWidth:kChatBubbleWidth hasPic:NO];
        }
        myChat.messageLabel.attributedString = attributedString;
        
        for (int i=0; i<[originArr count]; i++) {
            NSDictionary *dic = [originArr objectAtIndex:i];
            if (dic) {
                NSInteger x = [[dic safeObjectForKey:@"l"] integerValue];
                NSInteger y = [[dic safeObjectForKey:@"h"] integerValue];
                //                NSLog(@"%d and %d ====",x,y);
                UIImageView *face = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, kFaceSize, kFaceSize)];
                face.image = [UIImage imageNamed:[dic safeObjectForKey:@"n"]];
                [myChat.messageLabel addSubview:face];
                [face release];
            }
        }
        
        CGSize size = [ZSTChatBubble suggestSizeForAttributedString:attributedString];
        [chatBg addSubview:myChat];
        
        myChat.frame = CGRectMake(0, newHeight, kCommentBubbleWidth, size.height); //设置chatbubble的大小
        
        if (hasPic) {
            
            size = [ZSTChatBubble suggestSizeForCmtAttributedString:attributedString];
            
            myChat.messageLabel.frame = CGRectMake(55, 10, kCommentBubbleWidth, size.height);
            NSString *fileKey = [cellData safeObjectForKey:@"New_UAvatar"];
            [myChat.avtarImageView clear];
            ZSTTagUITapGestureRecognizer *singleRecognizer = [[ZSTTagUITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUserInfo:)];
            singleRecognizer.tag = uid;
            singleRecognizer.numberOfTapsRequired = 1;
            [myChat.avtarImageView addGestureRecognizer:singleRecognizer];
            [singleRecognizer release];
            if (fileKey != nil && ![fileKey isEqualToString:@""]) {
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",fileKey]];
                [myChat.avtarImageView setUrl:url];
                [myChat.avtarImageView loadImage];
            }
        } else {
            myChat.messageLabel.frame = CGRectMake(15, 6, kChatBubbleWidth+2, size.height);
            myChat.avtarImageView.frame = CGRectZero;
        }
        myChat.messageLabel.frame = CGRectMake(15, 6, kChatBubbleWidth+2, size.height);
        myChat.avtarImageView.frame = CGRectZero;
        [myChat release];
        newHeight += (size.height);
        height += size.height;
    } else { //语音
        attributedString = [self getVoiceLabelString:cellData isSmall:NO];
        myChat.messageLabel.attributedString = attributedString;
        [chatBg addSubview:myChat];
        
        myChat.messageLabel.frame = CGRectMake(15, newHeight+10, kChatBubbleWidth, 75);
        myChat.frame = CGRectMake(0, newHeight, kCommentBubbleWidth, 75); //设置chatbubble的大小
        myChat.avtarImageView.frame = CGRectZero;
        
        if([fileUrl rangeOfString:@".wav"].length>0 || [fileUrl rangeOfString:@".amr"].length>0){
            
            UIImage *image = ZSTModuleImage(@"module_snsa_btn_sound_bg.png");
            CGFloat capWidth = image.size.width / 2;
            CGFloat capHeight = image.size.height / 2;

            UIView *soundView = [[UIView alloc] initWithFrame:CGRectMake(180, 0, 60, 55)];
            soundView.backgroundColor = [UIColor clearColor];
            
            UIImageView *soundbgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, 47, 34)];
            soundbgImg.image = [image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
            [soundView addSubview:soundbgImg];
            
            UIImageView *voiceView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 18, 13)];
            voiceView.image = ZSTModuleImage(@"module_snsa_chat_sound_play_3.png");
            [soundView addSubview:voiceView];
            
            ZSTTagUITapGestureRecognizer *voiceTap = [[ZSTTagUITapGestureRecognizer alloc] initWithTarget:self action:@selector(playVoice:)];
            voiceTap.fileName = [cellData safeObjectForKey:@"VideoUrl"];
            voiceTap.numberOfTapsRequired = 1;
            voiceTap.imageView = voiceView;
            voiceView.userInteractionEnabled = YES;
            [soundbgImg addGestureRecognizer:voiceTap];
            [voiceView addGestureRecognizer:voiceTap];
            [soundView addGestureRecognizer:voiceTap];
        
            [chatBg addSubview:soundView];
            [chatBg bringSubviewToFront:soundView];
            
            [soundbgImg release];
            [voiceView release];
            [voiceTap release];
            
            UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(44, 10, 25, 15)];
            time.textAlignment = NSTextAlignmentCenter;
            time.text = [NSString stringWithFormat:@"%@\"",[cellData safeObjectForKey:@"VideoTimeLength"]];
            time.backgroundColor = [UIColor clearColor];
            time.textColor = [UIColor colorWithWhite:0.7 alpha:1];
            time.font = [UIFont systemFontOfSize:11];
            [soundView addSubview:time];
            [time release];
            [soundView release];
        }
        chatBg.userInteractionEnabled = YES;
        //语音重置高度，到最后设置
        //        chatBg.frame = CGRectMake(chatBg.origin.x, chatBg.origin.y, chatBg.size.width, 58);
        newHeight += 40;
        height += 58;
    }
    //评论
    NSArray *comments = [cellData safeObjectForKey:@"Cmts"];
    
    for ( int i = 0; i< [comments count]; i++ ) {
        newHeight += 10; //每行评论间隔
        height += 10;
        NSDictionary *comment = [comments objectAtIndex:i];
        uid = [[comment safeObjectForKey:@"UID"] intValue];
        
        UIImageView *divider = [[UIImageView alloc] initWithFrame:CGRectMake(7, newHeight +4, chatBg.width-9, 2)];
        divider.image = [ZSTModuleImage(@"module_snsa_feed-comment-divider.png") stretchableImageWithLeftCapWidth:0 topCapHeight:1];
        [chatBg addSubview:divider];
        [divider release];
        
        ChatBubble *myCmt = [[ChatBubble alloc] init];
        myCmt.clipsToBounds = YES;
        NSString *comCon = [comment safeObjectForKey:@"MContent"];
        attributedString = [[CommonFunc sharedCommon] getMessageLabelString:comment isSmall:YES content:comCon addName:YES];
        myCmt.messageLabel.attributedString = attributedString;
        CGSize size = [ZSTChatBubble suggestSizeForAttributedString:attributedString];
        myCmt.messageLabel.frame = CGRectMake(55, 10, kCommentBubbleWidth, size.height);
        [chatBg addSubview:myCmt];
        
        myCmt.frame = CGRectMake(0, newHeight, 250, size.height+10); //设置chatbubble的大小
        
        myCmt.avtarImageView.frame = CGRectMake(15, 12, kCommentHeadWidth, kCommentHeadWidth);
        NSString *fileKey = [comment safeObjectForKey:@"New_UAvatar"];
        if (fileKey && fileKey.length > 0) {
            [myCmt.avtarImageView clear];
        }
        ZSTTagUITapGestureRecognizer *singleRecognizer = [[ZSTTagUITapGestureRecognizer alloc] initWithTarget:self action:@selector(openUserInfo:)];
        singleRecognizer.tag = uid;
        singleRecognizer.numberOfTapsRequired = 1;
        [myCmt.avtarImageView addGestureRecognizer:singleRecognizer];
        [singleRecognizer release];
        if (fileKey != nil && ![fileKey isEqualToString:@""]) {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",fileKey]];
            if (fileKey && fileKey.length > 0) {

                [myCmt.avtarImageView setUrl:url];
                [myCmt.avtarImageView loadImage];
            }
        }
        newHeight += (size.height); //加上评论高度
        height += size.height;
        [myCmt release];
    }
    
    if (comments.count > 2) {
        
        UIImageView *divider = [[UIImageView alloc] initWithFrame:CGRectMake(7, newHeight + 10, chatBg.width-9, 2)];
        divider.image = [ZSTModuleImage(@"module_snsa_feed-comment-divider.png") stretchableImageWithLeftCapWidth:0 topCapHeight:1];
        [chatBg addSubview:divider];
        [divider release];
        
        UIButton *morebtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [morebtn setTitle:@"查看更多信息" forState:UIControlStateNormal];
        [morebtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        morebtn.titleLabel.font = [UIFont systemFontOfSize:14];
        morebtn.frame = CGRectMake(0, newHeight+14, 250, 20);
        morebtn.tag = index;
        [morebtn addTarget:self action:@selector(commentAction:) forControlEvents:UIControlEventTouchUpInside];
        [chatBg addSubview:morebtn];
        
        newHeight += 20;
        height += 20;
    }
    
    chatBg.frame = CGRectMake(chatBg.origin.x, chatBg.origin.y, chatBg.size.width, height);
}

- (void)commentAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    ZSTChatRoomCell *cell = (ZSTChatRoomCell *)[self.mTable cellForRowAtIndexPath:indexPath];
    [cell chatBubbleTouchUpInsideAction];
}

- (void)openUserInfo:(id)sender
{
    
}

-(void)playVoice:(id)sender { //用button不好搞动画
    ZSTTagUITapGestureRecognizer *tap = (ZSTTagUITapGestureRecognizer*)sender;
    
    BOOL same = NO; //两次点的是同一个文件
    if ([self.audioPlayer isPlaying]) {
        [self.audioPlayer stop];
        [self.audioPlayer.imageView stopAnimating];
        if ([tap.fileName isEqualToString:audioPlayer.playingFile]) {
            same = YES;
            self.audioPlayer = nil;
            return;
        } else {
            self.audioPlayer = nil;
        }
    }
    
    if (!tap.imageView.animationImages) {
        tap.imageView.animationImages = [NSArray arrayWithObjects:
                                         ZSTModuleImage(@"module_snsa_chat_sound_play_0.png"),
                                         ZSTModuleImage(@"module_snsa_chat_sound_play_1.png"),
                                         ZSTModuleImage(@"module_snsa_chat_sound_play_2.png"),
                                         ZSTModuleImage(@"module_snsa_chat_sound_play_3.png"),nil];
        tap.imageView.animationDuration = 1.0;
        tap.imageView.animationRepeatCount = 0; //暂时智能控制吧
    }
    
    //检查wav文件是否存在
    NSString *fileName = [tap.fileName substringToIndex:(tap.fileName.length-4)];
//    NSString *urlstr = @"http://mod.pmit.cn/SNSA/resource/video/";
//    NSString *tmpStr = [fileName substringFromIndex:urlstr.length];
    NSArray *strArray = [fileName componentsSeparatedByString:@"/"];
//    NSRange range = NSMakeRange(urlstr.length-1, tmpStr.length);
//    NSString *filename = [tap.fileName substringFromIndex:urlstr.length];
//    NSString *filename = [tap.fileName substringWithRange:range];
    NSString *filename = [strArray lastObject];
    NSString *wavFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.wav",filename]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL wavExist = [fileManager fileExistsAtPath:wavFile];
    
    NSData *wavData;
    NSError *error;
    if (!wavExist) {
        //    文件下载
        BOOL netExist = [[CommonFunc sharedCommon] isExistNetWork];
        if (!netExist) {
            return;
        }
        NSURL *fileUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",tap.fileName]];
        NSData *spxData = [NSData dataWithContentsOfURL:fileUrl];
        //    写入本地

        NSString *spxFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.amr",filename]];
        [spxData writeToFile:spxFile atomically:YES];
        //检测压缩语音文件是否存在
        if (![fileManager fileExistsAtPath:spxFile]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示信息"
                                                            message:@"语音文件不存在!"
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"确定", nil];
            [alert show];
            [alert release];
            return;
        }
//        if ([tap.fileName rangeOfString:@"spx"].length>0) {
//            SpeexDecoder *decoder = [[SpeexDecoder alloc] init];
//            [decoder decodeInFilePath:spxFile outFilePath:wavFile];
//            [decoder release];
//        } else if([tap.fileName rangeOfString:@"amr"].length>0){
//            [VoiceConverter amrToWav:spxFile];//这个解码有点慢，第一次播放有问题
//            
//        }
        
        if([tap.fileName rangeOfString:@"amr"].length>0){
            
            //将网络的amr文件转换成wav保存到本地临时文件夹中
            [VoiceConverter amrToWav:spxFile wavSavePath:wavFile];//这个解码有点慢，第一次播放有问题
        
        }
    }
    
    wavData = [NSData dataWithContentsOfFile:wavFile];
    
    
    if (!self.audioPlayer) {
        self.audioPlayer = [[ImageAudioPlayer alloc] initWithData:wavData error:&error];
        if (!audioPlayer) {
            NSLog(@"Error: %@", [error localizedDescription]);
        }
        audioPlayer.delegate = self;  // 设置代理
        audioPlayer.numberOfLoops = 0;// 不循环播放
        audioPlayer.imageView = tap.imageView;
    }
    
    if (![self.audioPlayer isPlaying] && !same) {//播放
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayback error:nil];
        audioPlayer.playingFile = tap.fileName;
        [audioPlayer prepareToPlay];
        [audioPlayer play];
        [audioPlayer.imageView startAnimating];
    }
}


- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [self.audioPlayer.imageView stopAnimating];
    self.audioPlayer = nil;
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error{
    [self.audioPlayer.imageView stopAnimating];
    self.audioPlayer = nil;
}

-(NSAttributedString *)getVoiceLabelString:(NSDictionary *)dic isSmall:(BOOL)isSmall{
    NSString *userName = @"";
    NSString *names = [[CommonFunc sharedCommon] trimStr:[dic safeObjectForKey:@"New_UName"]];
    if ([self isPureInt:names] && names.length >= 11)
    {
        userName = [[CommonFunc sharedCommon] trimStr:[dic safeObjectForKey:@"UName"]];
    }
    else
    {
        userName = names;
    }
    NSString *content = @"发表语音";
    
    double time = [[dic safeObjectForKey:@"AddTime"] doubleValue];
//    NSString *timeStr = [TKUtil timeIntervalSince1970:time];
    NSDate* createdAt = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [NSDate dateStringWithDate:createdAt];
    
    NSMutableAttributedString *attributedString = [[[NSMutableAttributedString alloc] init] autorelease];
    NSAttributedString *nameAttributedString;
    NSAttributedString *timeAttributedString;
    if (isSmall) {
        nameAttributedString = [[NSAttributedString alloc] initWithString:userName?[NSString stringWithFormat:@"%@: ", userName]:@" : " attributes:smallNameAttributes()];
        timeAttributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",content, timeStr] attributes:smallTimeAttributes()];
    } else {
        nameAttributedString = [[NSAttributedString alloc] initWithString:userName?[NSString stringWithFormat:@"%@: ", userName]:@" : " attributes:nameAttributes()];
        timeAttributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@",content, timeStr] attributes:timeAttributes()];
    }
    [attributedString appendAttributedString:nameAttributedString];
    [attributedString appendAttributedString:timeAttributedString];
    
    [nameAttributedString release];
    [timeAttributedString release];
    
    return attributedString;
}

- (void)ClickViewDidClicked:(ZSTChatRoomCell *)cell
{
    toUserId = [cell.cellData objectForKey:@"UID"];
    
    UIActionSheet *transmitSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"举报", nil];
    transmitSheet.tag = 2014;
    [transmitSheet showInView:self.view.window];
    [transmitSheet release];
}

#pragma mark -------UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //计算cell行高，这个行高应该算一遍之后把它存起来
    NSDictionary *message = [self.messages objectAtIndex:indexPath.row];
    
    CGFloat height = [[CommonFunc sharedCommon] calculateHeightForCell:message maxWidth:kChatBubbleWidth faceSize:kFaceSize];
    if (indexPath.row ==0) {
        height += 14;
    }
    return height;
}

- (void)refreshNewestData {
//    cacheData = YES;
    [self.engine getMessages:[circle safeObjectForKey:@"CID"] messageID:@"0" sortType:SortType_Desc fetchCount:kOneLimit];
}

- (void)loadMoreData {
    [self.engine getMessages:[circle safeObjectForKey:@"CID"] messageID:[[self.messages lastObject] safeObjectForKey:@"MID"] sortType:SortType_Desc fetchCount:kOneLimit];
}


- (void)autoRefresh
{
    [_refreshHeaderView autoRefreshOnScroll:mTable animated:YES];
}

- (void)appendData:(NSArray*)dataArray
{
    [self.messages addObjectsFromArray:dataArray];
}

- (void)getMessagesResponse:(NSArray *)msgs dataFromLocal:(BOOL)dataFromLocal hasMore:(BOOL)hasMore
{
    [[NSUserDefaults standardUserDefaults] setBool:hasMore forKey:[NSString stringWithFormat:@"%@_%@_hasMore", ZSTHttpMethod_GetMessages, [circle safeObjectForKey:@"CID"]]];
    [self fetchDataSuccess:msgs hasMore:hasMore];
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@_%@", @"ZSTSNSMessageCount", @(self.moduleType)] object:nil userInfo:nil];
}


#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidScroll:scrollView];
    }
}

#pragma mark EGORefreshTableHeaderDelegate Methods
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view {
    _isRefreshing = YES;
    [self refreshNewestData];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view {
    return _isRefreshing;
}

- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view {
    return [NSDate date];
}

#pragma mark HHLoadMoreViewDelegate Methods
- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    if (_hasMore) {
        _isLoadingMore = YES;
        [self loadMoreData];
    }
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}

- (void)finishLoading
{
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:mTable];
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:mTable];
    
    _isRefreshing = NO;
    _isLoadingMore = NO;
}

- (void)fetchDataSuccess:(NSArray *)theData hasMore:(BOOL)hasMore
{
    _hasMore = hasMore;
    
    if (hasMore) {
        mTable.tableFooterView = _loadMoreView;
    } else {
        mTable.tableFooterView = nil;
    }
    
    if (_isRefreshing) {
        
        self.messages = [NSMutableArray arrayWithArray:theData];
        
    } else if(_isLoadingMore)
    {
        [self appendData:theData];
    }
    
    [mTable reloadData];
    
    [self finishLoading];
}

- (void)fetchDataFailed
{
    [self finishLoading];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag == 2014) {
        
        if (buttonIndex == 0) {
            
            NSDictionary *userDic = [[CommonFunc sharedCommon] getCurrentUVO];
            NSString *userId;
            if (!userDic || [userDic isKindOfClass:[NSNull class]]) {
                userId = @"";
            } else {
                userId = [userDic valueForKey:@"UID"];
            }

            ZSTReportViewController *controller = [[ZSTReportViewController alloc] init];
             controller.circleId = [self.circle safeObjectForKey:@"CID"];
            controller.userId = userId;
            controller.toUserId = toUserId;
            [self.navigationController pushViewController:controller animated:YES];
            [controller release];
        }
        
        return;
    }
    
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    self.iconPickerController = [[[UIImagePickerController alloc] init] autorelease];
    [iconPickerController.navigationBar setTintColor:RGBCOLOR(27, 140, 233)];
    
    if ( buttonIndex == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) { // 拍照
        
        iconPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        iconPickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        iconPickerController.showsCameraControls = YES;
        
    } else if(buttonIndex != actionSheet.cancelButtonIndex) { // 本地图片
        
        iconPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    }
    iconPickerController.allowsEditing = NO;
    iconPickerController.delegate = self;
    [self presentModalViewController:iconPickerController animated:YES];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType =[NSString stringWithFormat:@"%@", [info objectForKey:UIImagePickerControllerMediaType]];
    if (![mediaType isEqualToString:@"public.image"]) {
        return;
    }
    
    UIImage *tempImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self compressImage:tempImage];
}

- (void)compressImage:(UIImage *)bigImage
{
    [TKUIUtil showHUDInView:self.iconPickerController.view
                   withText:NSLocalizedString(@"正在压缩", @"正在压缩")
                  withImage:nil
                 withCenter:self.iconPickerController.view.center ];
    [self performSelector:@selector(doCompressImage:) withObject:bigImage afterDelay:0.6];
}

- (void)doCompressImage:(UIImage *)bigImage
{
    UIImage *newImage = [UIImage imageZoom:bigImage andLength:640];
    self.uploadImageBg.hidden = NO;
    UIImageView *imageView = (UIImageView*)[self.uploadImageBg viewWithTag:12];
    imageView.image = newImage;
    
    [TKUIUtil hiddenHUD];
    [self.iconPickerController dismissViewControllerAnimated:YES completion:nil];
    self.iconPickerController = nil;
}


#pragma mark - UITextView Delegate Methods
-(BOOL)textView:(UITextView *)textVie shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textVie resignFirstResponder];
        return NO;
    }
    if ([text isEqualToString:@""]) {
//        NSRange r1 = textVie.selectedRange;
        //        NSLog(@"%@,%d",NSStringFromRange(r1),r1.location);
        
        
        //        NSString *s = [textVie.text substringFromIndex:textVie.text.length-1];
        ////        NSRange r1 = [textVie.text rangeOfString:@"["];
        //        if ([@"]" isEqualToString:s]) {
        //            NSString *last = [selectFaceArr objectAtIndex:[selectFaceArr count]-1];
        //            NSLog(@"------%@ %d",last,[last length]);
        //
        ////            NSString *ss = @"你好吗？";
        ////            ss = [ss substringToIndex:3]; // 8 - 2 + 1 = 7
        //
        ////            NSLog(@"==%@",ss);
        //            textVie.text = [textVie.text substringToIndex:(textVie.text.length-(last.length+2))];
        //        }
    }
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView {
    
}

#pragma mark touches...继承UIResponder，UIViewController也继承UIResponder，override就可以
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([self.messageView isFirstResponder]) {
        [self.messageView resignFirstResponder];
    }
}

-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *myDuration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
	// get a rect for the textView frame
    //    CGFloat height = self.view.bounds.size.height - (keyboardBounds.size.height);
    
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[myDuration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    [UIView setAnimationDelegate:self];
	
	// set views with new info
    //    self.tblView.frame = CGRectMake(0, 0, 320, btnFrame.origin.y);
    //    NSLog(@"%f",keyboardBounds.size.height); 216，252
    
    if (self.messageView.isFirstResponder) {
        self.voiceBg.frame = CGRectMake(0, 372-keyboardBounds.size.height+(iPhone5?88:0)-45, 320, 45);
        self.picBg.frame = CGRectMake(0, CGRectGetMaxY(self.voiceBg.frame), 320, 55);
        self.uploadImageBg.frame = CGRectMake(265, self.voiceBg.frame.origin.y - 49, 45, 44);
        self.mTable.frame = CGRectMake(0, 0, self.view.width, 416-35+(iPhone5?88:0)-55);
    }
	// commit animations
	[UIView commitAnimations];
    
    //添加
    if (!self.keyboardCover) {
        self.keyboardCover = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.size.width, 372-keyboardBounds.size.height)];
        keyboardCover.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *hideTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
        hideTap.numberOfTapsRequired = 1;
        [keyboardCover addGestureRecognizer:hideTap];
        [self.view addSubview:keyboardCover];
        [hideTap release];
    } else {
        self.keyboardCover.frame = CGRectMake(0, 0, self.view.size.width, 372-keyboardBounds.size.height);
    }
}

-(void)hideKeyboard{
    if(self.messageView.isFirstResponder) {
        [self.messageView resignFirstResponder];
    }
}

-(void) keyboardWillHide:(NSNotification *)note{
    if (self.keyboardCover) {
        [self.keyboardCover removeFromSuperview];
        self.keyboardCover = nil;
    }
    
    NSNumber *myDuration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
	// get a rect for the textView frame
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[myDuration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
    if (self.messageView.isFirstResponder) {
        //        if (faceView) {
        //            self.voiceBg.frame = CGRectMake(0, 337-self.faceView.size.height, 320, 45);
        //        } else {
        //            self.voiceBg.frame = CGRectMake(0, 337, 320, 45);
        //        }
        self.voiceBg.frame = CGRectMake(0, 337+(iPhone5?88:0)-20, 320, 45);
        self.picBg.frame = CGRectMake(0, 362+(iPhone5?88:0), 320, 55);
        self.faceView.frame = CGRectMake(0, 417+(iPhone5?88:0), 320, 0);
        self.uploadImageBg.frame = CGRectMake(265, 288+(iPhone5?88:0)-20, 45, 44);
    }
    
	// commit animations
	[UIView commitAnimations];
}

- (void)getCommentsByMIDResponse:(NSArray *)msgs
{
    if (msgs && [msgs count]) {
        NSUInteger startPos = 0;
        if ([msgs count] > 3) {
            startPos = [msgs count] - 3;
        } else {
            startPos = 0;
        }
        
        if (row < [self.messages count]) {
            NSDictionary *topic = [self.messages objectAtIndex:row];
            NSString *mid = [topic safeObjectForKey:@"MID"];
            NSString *newMid = [[msgs objectAtIndex: 0] safeObjectForKey:@"Parentid"];
            if ([mid intValue] != [newMid intValue]) {
                return;
            }
            NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
            for (NSUInteger i=startPos; i<[msgs count]; i++) {
                id object = [[[msgs objectAtIndex:i] copy] autorelease];
                [mutableArray addObject:object];
            }
            
            NSMutableDictionary *newTopic = [NSMutableDictionary dictionaryWithDictionary:topic];
            [newTopic removeObjectForKey:@"bubbleHeight"];
            [newTopic setObject:mutableArray forKey:@"Cmts"];
            [self.messages replaceObjectAtIndex:row withObject:newTopic];
            
            [self.dao saveComments:mutableArray circle:[circle safeObjectForKey:@"CID"]];
            [mutableArray release];
            
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
//            NSArray *array = [NSArray arrayWithObjects:indexPath, nil];
//            [self.mTable reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
            [self.mTable reloadData];
        }
    }
}

#pragma mark - ZSTCommentSendFinishDelegate
- (void)commentSendFinished:(NSDictionary *)topic row:(NSInteger)pos
{
    row = pos;
    [engine getCommentsByMID:[[topic safeObjectForKey:@"MID"] description]];
    
//     self.messages = [TKDataCache getCacheDataByURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetMessages, [circle objectForKey:@"CID"]]];
    
//    [self performSelector:@selector(autoRefresh) withObject:nil afterDelay:0.3];
}

- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    NSInteger val;
    return [scan scanInteger:&val] && [scan isAtEnd];
}


- (void)getUserByUIDResponse:(NSDictionary *)userInfo
{
    if (![[userInfo safeObjectForKey:@"state"] isKindOfClass:[NSNull class]] || [userInfo safeObjectForKey:@"state"])
    {
    }
}


@end
