//
//  ZSTChatTopArrowCell.m
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTChatBubble.h"
#import "ZSTTopicCommentController.h"
#import "ZSTGroupChatroomController.h"
#import "ZSTContactDetailController.h"
#import "TKUtil.h"
#import "NSDate+NSDateEx.h"
#import "ZSTChatRoomViewController.h"

#define kCommentMaxHeight INT_MAX

#define kChatBubbleWidth 180
#define kTopicBubbleWidth 225
#define kCommentBubbleWidth 180 //评论文字消息的水平宽度

@interface ZSTChatBubble() <ZSTCommentSendFinishDelegate>

@end

@implementation ZSTChatBubble

@synthesize topic;
@synthesize font;
@synthesize arrowDirection;

- (id)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _attributedLabels = [[NSMutableArray alloc] init];
        _avatarImageViews = [[NSMutableArray alloc] init];
        self.font = [UIFont systemFontOfSize:14];
        [self addTarget:self action:@selector(chatBubbleTouchUpInsideAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (id)initWithTopic:(NSDictionary *)theTopic
{
    self = [super init];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        _attributedLabels = [[NSMutableArray alloc] init];
        _avatarImageViews = [[NSMutableArray alloc] init];
        self.font = [UIFont systemFontOfSize:14];
        self.topic = theTopic;
        [self addTarget:self action:@selector(chatBubbleTouchUpInsideAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)chatBubbleTouchUpInsideAction
{
    ZSTTopicCommentController *topicCommentController = [[ZSTTopicCommentController alloc] init];
    topicCommentController.topic = self.topic;
    if (IS_IOS_7) {
        topicCommentController.row = ((TKPageTableViewCell *)[[self superview] superview]).row;
    } else {
        topicCommentController.row = ((TKPageTableViewCell *)[self superview]).row;
    }
    
    topicCommentController.delegate = self;
    topicCommentController.CID = [((ZSTChatRoomViewController *)self.viewController).circle safeObjectForKey:@"CID"];
    NSString *defaultCircle = [((ZSTChatRoomViewController *)self.viewController).circle safeObjectForKey:@"DefaultCircle"];
    if (defaultCircle != nil && 1==[defaultCircle integerValue]) {
        topicCommentController.isDefaultCircle = YES;
    }
    [self.viewController.navigationController pushViewController:topicCommentController animated:YES];
    [topicCommentController release];
}

+ (NSAttributedString *)attributedStringFromComment:(NSDictionary *)comment
{
    NSString *userName = [comment safeObjectForKey:@"UName"];
    NSString *content = [comment safeObjectForKey:@"MContent"];
    content = [content stringByTrimmingTrailingWhitespaceAndNewlineCharacters];
    if (content == nil || [content isEqualToString:@""]) {
        content = @" ";
    }
    double time = [[comment safeObjectForKey:@"AddTime"] doubleValue];
//    NSString *timeStr = [TKUtil timeIntervalSince1970:time];
    NSDate* createdAt = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [NSDate dateStringWithDate:createdAt];
    
    NSMutableAttributedString *attributedString = [[[NSMutableAttributedString alloc] init] autorelease];
    
    NSAttributedString *nameAttributedString = [[NSAttributedString alloc] initWithString:userName?[NSString stringWithFormat:@"%@: ", userName]:@" : " attributes:nameAttributes()];
    [attributedString appendAttributedString:nameAttributedString];
    [nameAttributedString release];
    
    NSAttributedString *contentAttributedString = [[NSAttributedString alloc] initWithString:content attributes:contentAttributes()];
    [attributedString appendAttributedString:contentAttributedString];
    [contentAttributedString release];
    
    NSAttributedString *timeAttributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", timeStr] attributes:timeAttributes()];
    [attributedString appendAttributedString:timeAttributedString];
    [timeAttributedString release];
    
    return attributedString;
}

+ (NSAttributedString *)smallAttributedStringFromComment:(NSDictionary *)comment
{
    NSString *userName;
    NSString *content;
    if (comment) {
        
        userName = [comment safeObjectForKey:@"UName"];
        content = [comment safeObjectForKey:@"MContent"];
    } else {
        
        userName = @"";
        content = @"查看更多评论";
    }
   
    content = [content stringByTrimmingTrailingWhitespaceAndNewlineCharacters];
    if (content == nil || [content isEqualToString:@""]) {
        content = @" ";
    }
    double time = [[comment safeObjectForKey:@"AddTime"] doubleValue];
//    NSString *timeStr = [TKUtil timeIntervalSince1970:time];
    NSDate* createdAt = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [NSDate dateStringWithDate:createdAt];
    
    NSMutableAttributedString *attributedString = [[[NSMutableAttributedString alloc] init] autorelease];
    
    NSAttributedString *nameAttributedString = [[NSAttributedString alloc] initWithString:userName?[NSString stringWithFormat:@"%@: ", userName]:@" : " attributes:smallNameAttributes()];
    [attributedString appendAttributedString:nameAttributedString];
    [nameAttributedString release];
    
    NSAttributedString *contentAttributedString = [[NSAttributedString alloc] initWithString:content attributes:smallContentAttributes()];
    [attributedString appendAttributedString:contentAttributedString];
    [contentAttributedString release];
    
    NSAttributedString *timeAttributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", timeStr] attributes:smallTimeAttributes()];
    [attributedString appendAttributedString:timeAttributedString];
    [timeAttributedString release];
    
    return attributedString;
}

- (void)setTopic:(NSDictionary *)theTopic
{
    if (theTopic != topic) {
        [topic release];
        topic = [theTopic retain];
        
        for (NIAttributedLabel *label in _attributedLabels) {
            [label removeFromSuperview];
        }
        [_attributedLabels removeAllObjects];
        
        for (TKAsynImageView *imageView in _avatarImageViews) {
            [imageView removeFromSuperview];
        }
        [_avatarImageViews removeAllObjects];
        
        CGFloat height = 0.f;
        
        NSString *imageURL = [topic safeObjectForKey:@"ImgUrl"];
        NSString *videoURL = [topic safeObjectForKey:@"VideoUrl"];
        
        if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
            
            height += 5;
        }
        
        if (videoURL != nil && ![videoURL isEmptyOrWhitespace]) {
            
            height += 5;
        }
        
        NSString *content = [topic safeObjectForKey:@"MContent"];
        content = [content stringByTrimmingTrailingWhitespaceAndNewlineCharacters];
        BOOL flag = NO;
        ZSTGroupChatroomController *vc = (ZSTGroupChatroomController *)self.viewController;
        if (vc.circle != nil) {
            NSString *defaultCircle = [vc.circle safeObjectForKey:@"DefaultCircle"];
            if (defaultCircle!=nil && 1==[defaultCircle integerValue]) {
                flag = YES;
            }
        }
        if (content != nil && ![content isEmptyOrWhitespace]) {
            
            height += 10;
            
            NIAttributedLabel *label = [[[NIAttributedLabel alloc] init] autorelease];
            label.backgroundColor = [UIColor clearColor];
            
            if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {                
                NSAttributedString *attributedString = [ZSTChatBubble attributedStringFromComment:topic];
                CGSize size = [ZSTChatBubble suggestSizeForAttributedString:attributedString];
                label.frame = CGRectMake(15, height, kChatBubbleWidth + 46, size.height);
                label.attributedString = attributedString;
                
                height += size.height;
                
            } else if (videoURL != nil && ![videoURL isEmptyOrWhitespace]) {
                NSAttributedString *attributedString = [ZSTChatBubble attributedStringFromComment:topic];
                CGSize size = [ZSTChatBubble suggestSizeForAttributedString:attributedString];
                label.frame = CGRectMake(15, height, kChatBubbleWidth + 46, size.height);
                label.attributedString = attributedString;
                
                height += size.height;
                
            } else {
                NSAttributedString *attributedString = [ZSTChatBubble attributedStringFromComment:topic];
                CGSize size = [ZSTChatBubble suggestSizeForAttributedString:attributedString width:kTopicBubbleWidth];
                label.frame = CGRectMake(15, height, kTopicBubbleWidth, size.height);
                label.attributedString = attributedString;
                
                height += size.height;
            }
            [self addSubview:label];
            [_attributedLabels addObject:label];
        }
        
        NSArray *comments = [topic safeObjectForKey:@"Cmts"];
        for ( int i = 0; i< [comments count]; i++ ) {
            height += 10;
            NSDictionary *comment = [comments objectAtIndex:i];
            
            TKAsynImageView *avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(15, height, 36, 36)] autorelease];
            avtarImageView.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
            avtarImageView.defaultImage = ZSTModuleImage(@"module_snsa_default_avatar.png");
            avtarImageView.tag = i;
            [avtarImageView addTarget:self action:@selector(openUserInfo:) forControlEvents:UIControlEventTouchUpInside];
            avtarImageView.layer.borderWidth = 2;
            avtarImageView.layer.borderColor =[ [UIColor whiteColor] CGColor];

            NSString *fileKey = [comment safeObjectForKey:@"UAvatar"];
            if (fileKey == nil) {
                fileKey = @"";
            }
            
            NSURL *url = [NSURL URLWithString:fileKey];
            [avtarImageView setUrl:url];
            [avtarImageView loadImage];
            avtarImageView.adjustsImageWhenHighlighted = flag;
            
            [self addSubview:avtarImageView];
            [_avatarImageViews addObject:avtarImageView];
            
            NSAttributedString *attributedString = [ZSTChatBubble smallAttributedStringFromComment:comment];
            CGSize size = [ZSTChatBubble suggestSizeForAttributedString:attributedString];
            
            NIAttributedLabel *label = [[[NIAttributedLabel alloc] init] autorelease];
            label.frame = CGRectMake(61, height, kChatBubbleWidth, size.height);
            label.backgroundColor = [UIColor clearColor];

            label.attributedString = attributedString;
            [self addSubview:label];
            [_attributedLabels addObject:label];
            
            height += (size.height);
        }
                
        [self setNeedsDisplay];
    }
}

- (void)dealloc
{
    TKRELEASE(_attributedLabels);
    TKRELEASE(_avatarImageViews);
    self.topic = nil;
    self.font = nil;
    [super dealloc];
}

+(CGFloat) getWidthForAttributedString:(NSAttributedString *)attributedString {
    CFAttributedStringRef attributedStringRef = (CFAttributedStringRef)attributedString;
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attributedStringRef);
    
    CGSize newSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0),
                                                                  NULL, CGSizeMake(1000000, CGFLOAT_MAX), nil);
    if (nil != framesetter) {
        CFRelease(framesetter);
        framesetter = nil;
    }
    return newSize.width;
}

//计算评论
+ (CGSize)suggestSizeForCmtAttributedString:(NSAttributedString *)attributedString
{
    
    
    CFAttributedStringRef attributedStringRef = (CFAttributedStringRef)attributedString;
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attributedStringRef);
	
	CGSize newSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0),
                                                                  NULL, CGSizeMake(kCommentBubbleWidth, CGFLOAT_MAX), nil);
	if (nil != framesetter) {
        CFRelease(framesetter);
        framesetter = nil;
    }
    return CGSizeMake(kCommentBubbleWidth, MAX(ceil(newSize.height), 36));
}


- (void)openUserInfo:(TKAsynImageView *)avtarImageView
{
    ZSTGroupChatroomController *vc = (ZSTGroupChatroomController *)self.viewController;
    if (vc.circle != nil) {
        NSString *defaultCircle = [vc.circle safeObjectForKey:@"DefaultCircle"];
        if (defaultCircle!=nil && 1==[defaultCircle integerValue]) {
            return;
        }
    }
    ZSTContactDetailController *groupChatController = [[ZSTContactDetailController alloc] init];
    
    NSString *userID = nil;
    if (avtarImageView.tag < 0) {
        userID = [self.topic safeObjectForKey:@"UID"];
    } else {
        NSArray *comments = [topic safeObjectForKey:@"Cmts"];
        userID = [[comments objectAtIndex:avtarImageView.tag] safeObjectForKey:@"UID"];
    }
    
    groupChatController.userID = userID;
    [self.viewController.navigationController pushViewController:groupChatController animated:YES];
    [groupChatController release];
}

+ (CGSize)suggestSizeForAttributedString:(NSAttributedString *)attributedString;
{
    CFAttributedStringRef attributedStringRef = (CFAttributedStringRef)attributedString;
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attributedStringRef);
	
	CGSize newSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0),
                                                                  NULL, CGSizeMake(kChatBubbleWidth, CGFLOAT_MAX), nil);
	if (nil != framesetter) {
        CFRelease(framesetter);
        framesetter = nil;
    }
    return CGSizeMake(kChatBubbleWidth, MAX(ceil(newSize.height), 36));
}

+ (CGSize)suggestSizeForAttributedString:(NSAttributedString *)attributedString width:(CGFloat)width
{
    CFAttributedStringRef attributedStringRef = (CFAttributedStringRef)attributedString;
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attributedStringRef);
	
	CGSize newSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0),
                                                                  NULL, CGSizeMake(width, CGFLOAT_MAX), nil);
	if (nil != framesetter) {
        CFRelease(framesetter);
        framesetter = nil;
    }
    return CGSizeMake(width, MAX(ceil(newSize.height), 36));
}

+ (CGSize)suggestSizeForTopic:(NSMutableDictionary *)topic font:(UIFont *)font
{
    NSNumber *h = [topic safeObjectForKey:@"bubbleHeight"];
    if (!h) {
        CGFloat height = 0.f;
        
        NSString *imageURL = [topic safeObjectForKey:@"ImgUrl"];
        NSString *videoURL = [topic safeObjectForKey:@"VideoUrl"];
        if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
            height += 5;
        }
        
        if (videoURL != nil && ![videoURL isEmptyOrWhitespace]) {
            height += 5;
        }

        
        NSString *content = [topic safeObjectForKey:@"MContent"];
        content = [content stringByTrimmingTrailingWhitespaceAndNewlineCharacters];
        if (content != nil && ![content isEmptyOrWhitespace]) {
            
            height += 10;
            if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
                CGSize size = [ZSTChatBubble suggestSizeForAttributedString:[ZSTChatBubble attributedStringFromComment:topic]];
                height += size.height;
            } else if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
                CGSize size = [ZSTChatBubble suggestSizeForAttributedString:[ZSTChatBubble attributedStringFromComment:topic]];
                height += size.height;
            } else {
                CGSize size = [ZSTChatBubble suggestSizeForAttributedString:[ZSTChatBubble attributedStringFromComment:topic] width:kTopicBubbleWidth];
                height += size.height;
            }
        }
        
        NSArray *comments = [topic safeObjectForKey:@"Cmts"];
        for ( int i = 0; i< [comments count]; i++ ) {
            height += 10;
            NSDictionary *comment = [comments objectAtIndex:i];
            CGSize size = [ZSTChatBubble suggestSizeForAttributedString:[ZSTChatBubble smallAttributedStringFromComment:comment]];
            height += (size.height);
        }
        height += 10;
        [topic setObject:[NSNumber numberWithFloat:height] forKey:@"bubbleHeight"];
        return CGSizeMake(kTopicBubbleWidth, MAX(height, 46));
    }
    return CGSizeMake(kTopicBubbleWidth, MAX([h floatValue], 46));
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    NSString *imageURL = [topic safeObjectForKey:@"ImgUrl"];
    NSString *videoURL = [topic safeObjectForKey:@"VideoUrl"];
    if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
        [[ZSTModuleImage(@"module_snsa_feed-comment-bubble.png") stretchableImageWithLeftCapWidth:34 topCapHeight:15] drawInRect:CGRectMake(5, 0, self.width - 5, self.height)];
    } else if (videoURL != nil && ![videoURL isEmptyOrWhitespace]) {
        [[ZSTModuleImage(@"module_snsa_feed-comment-bubble.png") stretchableImageWithLeftCapWidth:34 topCapHeight:15] drawInRect:CGRectMake(5, 0, self.width - 5, self.height)];
    } else {
        [[ZSTModuleImage(@"module_snsa_feed-thought-bubble.png") stretchableImageWithLeftCapWidth:20 topCapHeight:25] drawInRect:self.bounds];
    }
    
    CGFloat height = 0.f;
    
    if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
        
        height += 5;
    }
    
    if (videoURL != nil && ![videoURL isEmptyOrWhitespace]) {
        
        height += 5;
    }
    
    NSString *content = [topic safeObjectForKey:@"MContent"];
    content = [content stringByTrimmingTrailingWhitespaceAndNewlineCharacters];
    if (content != nil && ![content isEmptyOrWhitespace]) {
        
        height += 10;
        
        if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
            CGSize size = [ZSTChatBubble suggestSizeForAttributedString:[ZSTChatBubble attributedStringFromComment:topic]];
            height += size.height;
            
        } else {
            
            CGSize size = [ZSTChatBubble suggestSizeForAttributedString:[ZSTChatBubble attributedStringFromComment:topic] width:kTopicBubbleWidth];
            height += size.height;
        }
    }
    
    NSArray *comments = [topic safeObjectForKey:@"Cmts"];
    for ( int i = 0; i< [comments count]; i++ ) {
        
        if (i != 0 || (content != nil && ![content isEmptyOrWhitespace])) {
            [[ZSTModuleImage(@"module_snsa_feed-comment-divider.png") stretchableImageWithLeftCapWidth:0 topCapHeight:1] drawInRect:CGRectMake(7, height + 4, self.width - 9, 2)];
        }
        
        height += 10;
        NSDictionary *comment = [comments objectAtIndex:i];
        
        NSAttributedString *attributedString = [ZSTChatBubble smallAttributedStringFromComment:comment];
        CGSize size = [ZSTChatBubble suggestSizeForAttributedString:attributedString];
        
        height += (size.height);
    }
}

- (void)commentSendFinished:(NSDictionary *)topic row:(NSInteger)row
{
    
}

@end
