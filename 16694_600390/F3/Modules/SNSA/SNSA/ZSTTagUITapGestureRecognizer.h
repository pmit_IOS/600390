//
//  TagUITapGestureRecognizer.h
//  YouYun
//
//  Created by admin on 12-10-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTTagUITapGestureRecognizer : UITapGestureRecognizer{
    NSInteger tag;
}
@property (nonatomic) NSInteger tag;
@property (nonatomic,retain) UIImageView *imageView;
@property (nonatomic,retain) NSString *fileName;
@end
