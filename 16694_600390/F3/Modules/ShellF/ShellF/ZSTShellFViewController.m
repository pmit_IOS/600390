//
//  ZSTShellFViewController.m
//  ShellF
//
//  Created by LiZhenQu on 14-4-9.
//  Copyright (c) 2014年 xxxxx. All rights reserved.
//

#import "ZSTShellFViewController.h"
#import "ElementParser.h"
#import "ZSTModuleManager.h"
#import "ZSTWebViewController.h"
#import "ZSTUtils.h"
#import "ZSTGlobal+ShellF.h"
#import "ZSTMineViewController.h"

@interface ZSTShellFViewController ()

@end

@implementation ZSTShellFViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self.engine getShellFAD];
//    [self.engine getShellFModuleAD];
    
    self.speedBarScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, (self.view.bounds.size.height - 20 - 49))];
    self.speedBarScrollView.delegate = self;
    self.speedBarScrollView.showsHorizontalScrollIndicator = NO;
    self.speedBarScrollView.showsVerticalScrollIndicator = NO;
    self.speedBarScrollView.backgroundColor = [UIColor clearColor];
    //pagingEnabled导致上下滚动的shellF很不好用。关掉
    self.speedBarScrollView.pagingEnabled = NO;
    [self.view addSubview:self.speedBarScrollView];
    
    self.shellfCarouselView = [[ZSTShellFCarouselView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
    self.shellfCarouselView.carouselDataSource = self;
    self.shellfCarouselView.carouselDelegate = self;
    [self.speedBarScrollView addSubview: self.shellfCarouselView];
    
    UIImageView *backgroundImg3 = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_shellf_mainView_background.png")];
    backgroundImg3.frame = CGRectMake(0, CGRectGetMaxY(self.shellfCarouselView.frame)-1, 320, 14);
    backgroundImg3.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:backgroundImg3];
    [backgroundImg3 release];
    
    [self imageButtonElementsForView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.rightBarButtonItem = [self initWithSubviews];
}

- (UIBarButtonItem *) initWithSubviews
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, WidthRate(55), WidthRate(55));
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(mineAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //btn.layer.cornerRadius = btn.frame.size.width / 2.0;
    btn.layer.cornerRadius = 4.0f;
    btn.layer.borderColor = [UIColor clearColor].CGColor;
    btn.layer.masksToBounds = YES;
    
    UIImage *image = nil;
    image = [UIImage imageNamed:@"module_personal_list_avater_defaut.png"];
    
    
    // 以下注释代码是用户登录后改变首页右上角头像(用户已上传头像)
//    NSString *path = nil;
//    UIImage *image = nil;
//    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
//    
//    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
//        
//        [ZSTF3Preferences shared].loginMsisdn = @"";
//    }
//    
//    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
//        
//        image = [UIImage imageNamed:@"module_personal_list_avater_defaut.png"];
//    } else {
//        
//        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
//            path = [dic safeObjectForKey:[ZSTF3Preferences shared].loginMsisdn];
//        }
//        
//        if (path && path.length > 0) {
//            
//            NSString *homePath = NSHomeDirectory();
//            NSString *imgTempPath = [path substringFromIndex:[path rangeOfString:@"tmp"].location];
//            NSString *finalPath = [NSString stringWithFormat:@"%@/%@",homePath,imgTempPath];
//            NSData *reader = [NSData dataWithContentsOfFile:finalPath];
//            image = [UIImage imageWithData:reader];
//            
//            if (!image) {
//                
//                image = [UIImage imageNamed:@"module_personal_list_avater_defaut.png"];
//            }
//            
//        } else {
//            
//            image = [UIImage imageNamed:@"module_personal_list_avater_defaut.png"];
//        }
//    }
    
    [btn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:btn.imageView.frame];
    iv.image = image;
    btn.contentMode = UIViewContentModeScaleAspectFit;
    [btn insertSubview:iv aboveSubview:btn.imageView];
    
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}

- (void)mineAction:(id)sender
{
    ZSTMineViewController *controller = [[ZSTMineViewController alloc] initWithNibName:@"ZSTMineViewController" bundle:nil];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void) showGuideView
{
    UIView *guideView = [[UIView alloc] initWithFrame:CGRectMake(0,0 , 320, self.view.bounds.size.height)];
    guideView.backgroundColor = [UIColor blackColor];
    guideView.tag = 1111;
    guideView.alpha = 0.7f;
    
    UIImageView *guideTextImgView = [[UIImageView alloc] initWithFrame:CGRectMake(113,270+self.view.bounds.size.height-480 , 93, 44)];
    guideTextImgView.image = ZSTModuleImage(@"module_shellf_BeginnersGuide.png");
    [guideView addSubview:guideTextImgView];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:FIRST_LAUNCH_ShellF];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(removeGuideView)];
    [guideView addGestureRecognizer:singleTap];
    
    [self.view addSubview:guideView];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void) removeGuideView
{
    [[[UIApplication sharedApplication].keyWindow viewWithTag:1111] removeFromSuperview];
}

- (void)imageButtonElementsForView
{
    NSInteger columnCount = 2;
    
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellf_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"ImageButton Item"];
    if (![[NSUserDefaults standardUserDefaults] valueForKey:FIRST_LAUNCH_ShellF]) {
//        [self showGuideView];
    }
    
    //水平间距
    float horizontalSpacing = (320.0 - 144.0 * columnCount)/(columnCount+1);
    //上下间距
    float verticalSpacing = 10;
    
    //SeedBar模块的宽高
    float speedBar_Width = 144.0f;
    float speedBar_Hight = 72.0f;
    
    //SeedBar模块内 按钮 文字的上下距离
    float speedBar_Iner_VertialSpacing = 0.f + iPhone5?2:0;
    
    
    // 计算PageCount
    NSUInteger rowCount = ([itemElements count] + 1) / columnCount;
    
    
    self.speedBarScrollView.contentSize = CGSizeMake(320, 160+(rowCount+1)*72+(rowCount-1)*10);
    for (NSInteger i = 0; i < [itemElements count]; i++) {
        Element *itemElement = [[itemElements objectAtIndex:i] retain];
        
        NSString *btnName = [[itemElement selectElement:@"Title"] contentsText];
        int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
        NSNumber *moduleType = [[itemElement selectElement:@"ModuleType"] contentsNumber];
        NSString *myTitleColor = [[itemElement selectElement:@"TitleColor"] contentsText];
        if (myTitleColor == nil || [myTitleColor length] == 0 || [myTitleColor isEqualToString:@""]) {
            myTitleColor = @"#000000";
        }
        
        NSDictionary *paramDic = [NSDictionary dictionary];
        if (moduleID == 12 || moduleID == 24) {
            NSString *interfaceUrl = [[itemElement selectElement:@"InterfaceUrl"] contentsText];
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",interfaceUrl,@"InterfaceUrl",myTitleColor,@"TitleColor", nil];
        }else{
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",myTitleColor,@"TitleColor", nil];
        }
        
        NSInteger currentPage = i / (rowCount * columnCount);
        NSInteger row = i / columnCount;
        row = row % rowCount;
        NSInteger col = i % columnCount;
        
        float x = 10 + horizontalSpacing * currentPage + (144 + horizontalSpacing)  *col;
        float y = 10 + (verticalSpacing + speedBar_Hight) * row + 160;
        
        ZSTShellFSpeedBar *bar = [[[ZSTShellFSpeedBar alloc] initWithFrame:CGRectMake(x, y, 144, 72)] autorelease];
        bar.backgroundColor = [UIColor clearColor];
        bar.speedBtn.tag = moduleID;
        bar.speedBtn.titleLabel.text = btnName;
        
        bar.delegate = self;
        NSString *normalImagePath = [NSString stringWithFormat:@"module_shellf_icon%@_n.png", @(i+1)];
        NSString *selectedImagePath = [NSString stringWithFormat:@"module_shellf_icon%@_p.png", @(i+1)];
        
        [bar configSpeedBarNormalImage:ZSTModuleImage(normalImagePath)
                         selectedImage:ZSTModuleImage(selectedImagePath)
                                 param:paramDic];
        
        
        bar.frame = CGRectMake(x, y, speedBar_Width, speedBar_Hight);
        [bar speedBarInerSpace:speedBar_Iner_VertialSpacing];
        
        [self.speedBarScrollView addSubview:bar];
    }
    [parser release];
}

#pragma mark ZSTShellDSpeedBarDelegate

- (void)ZSTShellFSpeedBar:(ZSTShellFSpeedBar *)speedBar withParam:(NSDictionary *)param
{
    NSMutableDictionary *moduleParams = [NSMutableDictionary dictionaryWithDictionary:param];
    
    ZSTModule *module = [[ZSTModuleManager shared] findModule:speedBar.speedBtn.tag];
    if (module) {
        UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:speedBar.speedBtn.tag withOptions:moduleParams];
        if (controller) {
            controller.hidesBottomBarWhenPushed = YES;
            controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:speedBar.titleLabel.text];
            controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

#pragma mark ZSTF3EngineShellDDelegate

- (void)getShellFADDidSucceed:(NSArray *)ADArray
{
    NSLog(@"ADArray====>%@",ADArray);
    
    self.shellFADArray = [ADArray retain];
    [self.shellfCarouselView reloadData];
}

- (void)getShellFADDidFailed:(int)resultCode
{
    [TKUIUtil alertInView:self.view withTitle:@"获取广告失败" withImage:nil];
}

- (void)getShellFModuleADDidSucceed:(NSArray *)ModuleADArray
{
#warning 这里有用的代码被注释掉了
//    self.shellFModuleADArray = [ModuleADArray retain];
//    for (NSDictionary *moduleDic in self.shellFModuleADArray) {
//        int location = [[moduleDic safeObjectForKey:@"location"] integerValue];
//        //ZSTShellDAsyImageView *view = (ZSTShellDAsyImageView *)[self.bgScrollView viewWithTag:location];
//        //[view clear];
//        // view.url = [NSURL URLWithString:[moduleDic objectForKey:@"fileurl"]];
//        // [view loadImage];
//        
//    }
}

- (void)getShellFModuleADDidFailed:(int)resultCode
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"获取功能图片失败", nil) withImage:nil];
}

#pragma mark ZSTShellDCarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(ZSTShellFCarouselView *)newsCarouselView
{
    return [self.shellFADArray count];
}

- (NSDictionary *)carouselView:(ZSTShellFCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    if ([self.shellFADArray  count] != 0) {
        return [self.shellFADArray  objectAtIndex:index];
    }
    return nil;
}

#pragma mark ZSTShellDCarouselViewDelegate

- (void)carouselView:(ZSTShellFCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    NSString *urlPath = [[self.shellFADArray objectAtIndex:index] safeObjectForKey:@"ad_linkurl"];
    if (urlPath == nil || [urlPath length] == 0 || [urlPath isEmptyOrWhitespace]) {
        return;
    }
    
    ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
    
    [webViewController setURL:urlPath];
     webViewController.type = self.moduleType;
    webViewController.hidesBottomBarWhenPushed = YES;
    webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    [self.navigationController pushViewController:webViewController animated:NO];
    [webViewController release];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
