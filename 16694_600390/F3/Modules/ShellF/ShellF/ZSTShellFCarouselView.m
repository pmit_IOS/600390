//
//  ZSTShellDCarouselView.m
//  ShellD
//
//  Created by xuhuijun on 13-6-18.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShellFCarouselView.h"
#import "TKAsynImageView.h"

#define maxItems 5

@implementation ZSTShellFCarouselView

@synthesize carouselDelegate = _carouselDelegate;
@synthesize carouselDataSource = _carouselDataSource;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (void)reloadData
{
    self.backgroundColor = [UIColor clearColor];
    CGRect frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    _scrollView = [[UIScrollView alloc] initWithFrame:frame];
    _scrollView.delegate = self;
    _scrollView.pagingEnabled = YES;
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:_scrollView];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height - 15, frame.size.width, 15)];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.opaque = NO;
    [self addSubview:titleView];
    [titleView release];
    
    _pageControl = [[TKPageControl alloc] initWithFrame:CGRectMake(0, 0, 80, titleView.frame.size.height)];
    _pageControl.type = TKPageControlTypeOnImageOffImage;
    _pageControl.numberOfPages = 0;
    _pageControl.dotSize = 7;
    _pageControl.onImage = ZSTModuleImage(@"module_shellf_carousel_pageControlOn.png");
    _pageControl.offImage = ZSTModuleImage(@"module_shellf_carousel_pageControlOff.png");
    _pageControl.backgroundColor = [UIColor clearColor];
    _pageControl.userInteractionEnabled = NO;
    [titleView addSubview:_pageControl];
    
    _curPage = 0;//轮播图得第一张
    
    [self startAnimation];
    
    _totalPages = [_carouselDataSource numberOfViewsInCarouselView:self];
    if (_totalPages == 0) {
        return;
    }
    if (_totalPages == 1) {
        [self stopAnimation];
        _scrollView.scrollEnabled = NO;
    }
    if (_totalPages > maxItems) {
        _totalPages = maxItems;
    }
    _pageControl.numberOfPages = _totalPages;
    _pageControl.hidden = NO;
    
    if (_totalPages == 1) {
        _pageControl.hidden = YES;
    }
    _scrollView.contentSize = CGSizeMake((self.frame.size.width) * 3, self.frame.size.height);
    
    [self loadData];
}

- (void)loadData
{
    _pageControl.currentPage = _curPage;
    
    if(_scrollView.subviews && _scrollView.subviews.count > 0) {
		[_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    [self getDisplayImagesWithCurpage:_curPage];
    
    if ([_curSource count]) {
        for (int i = 0; i < 3; i ++) {
            
            NSDictionary *dataDic = [_curSource objectAtIndex:i];
            
            TKAsynImageView *asynImageView = [[[TKAsynImageView alloc] initWithFrame:_scrollView.frame] autorelease];
            asynImageView.adjustsImageWhenHighlighted = NO;
            asynImageView.asynImageDelegate = self;
            asynImageView.adorn = ZSTModuleImage(@"module_shellf_scoll_pic.png");
            asynImageView.defaultImage = ZSTModuleImage(@"module_shellf_home_default_img.png");
            [asynImageView clear];
            asynImageView.url = [NSURL URLWithString:[dataDic safeObjectForKey:@"ad_imgurl"]];
            [asynImageView loadImage];
            asynImageView.frame = CGRectOffset(asynImageView.frame, asynImageView.frame.size.width * i, 0);
            [_scrollView addSubview:asynImageView];
            
        }
    }
    
    [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width, 0)];
    
}

- (void)getDisplayImagesWithCurpage:(NSInteger)page {
    
    NSInteger pre = [self validPageValue:_curPage-1];
    NSInteger last = [self validPageValue:_curPage+1];
    
    if (!_curSource) {
        _curSource = [[NSMutableArray alloc] init];
    }
    [_curSource removeAllObjects];
    
    id d = [self.carouselDataSource carouselView:self infoForViewAtIndex:pre];
    
    if (d == nil) {
        return;
    }
    [_curSource addObject:d];
    [_curSource addObject:[self.carouselDataSource carouselView:self infoForViewAtIndex:_curPage]];
    [_curSource addObject:[self.carouselDataSource carouselView:self infoForViewAtIndex:last]];
}


#pragma mark - TKAsynImageViewDelegate

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    //根据_pageControl.currentPage 打开响应的详细页
    
    if ([_carouselDelegate respondsToSelector:@selector(carouselView:infoForViewAtIndex:)]) {
        [_carouselDelegate carouselView:self didSelectedViewAtIndex:_pageControl.currentPage];
    }
    
}

#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [scrollView setContentOffset:CGPointMake(scrollView.size.width, 0) animated:YES];
}



- (NSInteger )validPageValue:(NSInteger)value {
    
    if(value == -1) value = _totalPages - 1; //最后一也
    if(value == _totalPages) value = 0; //第一页
    //中间页
    
    return value;
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    int x = aScrollView.contentOffset.x;
    if(x >= (2*aScrollView.size.width)) {
        _curPage = [self validPageValue:_curPage+1];
        [self loadData];
    }
    if(x <= 0) {
        _curPage = [self validPageValue:_curPage-1];
        [self loadData];
    }
    
}
//当页数变化时，改变scrollView的内容大小
- (void) pageTurn
{
    if (_carouselDataSource == nil) {
        return;
    }
    [_scrollView setContentOffset:CGPointMake((_scrollView.frame.size.width) * 2, 0.0f) animated:YES];
}

- (void)stopAnimation
{
    if ([_carouselTimer isValid]) {
        [_carouselTimer invalidate];
        TKRELEASE(_carouselTimer);
    }
}

- (void)startAnimation
{
    if (_carouselTimer == nil) {
        _carouselTimer = [[NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(pageTurn) userInfo:nil repeats:YES] retain];
    }
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    UIImage *image = ZSTModuleImage(@"module_shellf_scoll_pic.png") ;
    [image drawInRect:rect];
}

- (void)dealloc
{
    TKRELEASE(_carouselTimer);
    TKRELEASE(_describeLabel);
    TKRELEASE(_scrollView);
    TKRELEASE(_pageControl);
    TKRELEASE(_curSource);
    
    [super dealloc];
}

@end
