//
//  VideoViewController.m
//  VoiceChina
//
//  Created by xuhuijun on 13-2-22.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTVideoViewController.h"
#import "ZSTVideoMainCell.h"
#import "ZSTVideoPlayerController.h"
#import "MoreButton.h"
#import "ZSTDao+VideoB.h"

@interface ZSTVideoViewController ()
{
    int _pageIndex;
    BOOL _hasMore;
    MoreButton *_moreButton;
    BOOL  isRefresh;
    BOOL _loading;  //是否正在加载中
    
}

@property (retain,nonatomic) NSArray *videoArray;

@end

@implementation ZSTVideoViewController

- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao createTableIfNotExistForVIDEOBModule];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataArray = [[NSMutableArray alloc] initWithCapacity:10];
    self.videoArray = @[@"http://mod.pmit.cn/Video/1.mp4",@"http://mod.pmit.cn/Video/2.mp4",@"http://mod.pmit.cn/Video/3.mp4",@"http://mod.pmit.cn/Video/4.mp4",@"http://mod.pmit.cn/Video/5.mp4",@"http://mod.pmit.cn/Video/6.mp4",@"http://mod.pmit.cn/Video/7.mp4",@"http://mod.pmit.cn/Video/8.mp4"];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460+(iPhone5?88:0)+(IS_IOS_7?20:0)) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.tableView.separatorStyle =  UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    if (_refreshHeaderView == nil) {
        
       	EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height)];
		view.delegate = self;
		[self.tableView addSubview:view];
		_refreshHeaderView = view;
        [view release];
    }
    
     [_refreshHeaderView refreshLastUpdatedDate];
    
    UIImageView *shadow = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_videob_top_column_shadow.png")];
    shadow.frame = CGRectMake(0, 0, 320, -4);
    [self.tableView addSubview:shadow];
    [shadow release];
    
    NSArray *array = [[[NSArray alloc] initWithArray:[self.dao getVideosList]] autorelease];
    
    self.dataArray = [ZSTVideoBVO videoListLocalWithdic:array];

    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
    [self loadDataForPage:1];

}

- (void)loadDataForPage:(int)pageIndex
{
    if (pageIndex == 1) {
        _pageIndex = 1;        
        [self.engine getVideoListWithDelegete:self
                                    videoType:0
                                     pageSize:VIDEO_PAGE_SIZE
                                      sinceId:@"0"
                                     pageIdex:pageIndex
                                    orderType:SortType_Desc];
        
        isRefresh = YES;

    }else if (pageIndex > 1){
        ZSTVideoBVO *lastVideo = (ZSTVideoBVO *)[self.dataArray lastObject];
                
        [self.engine getVideoListWithDelegete:self
                                    videoType:0
                                     pageSize:VIDEO_PAGE_SIZE
                                      sinceId:lastVideo.videoid
                                     pageIdex:pageIndex
                                    orderType:SortType_Desc];
        
        isRefresh = NO;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_hasMore) {
        
        return self.dataArray.count + 1;
    }
    return self.dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *moreCellIdentifier = @"MoreCell";
    if (indexPath.row == self.dataArray.count && _hasMore)
    {
        UITableViewCell *moreCell = [tableView dequeueReusableCellWithIdentifier:moreCellIdentifier];
        if (moreCell == nil)
        {
            moreCell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:moreCellIdentifier] autorelease];
            _moreButton = [MoreButton button];
            [_moreButton addTarget:self action:@selector(loadMore) forControlEvents:UIControlEventTouchUpInside];
            [moreCell.contentView addSubview:_moreButton];
        }
        
        return moreCell;
    }
    
    static NSString *newsCellIdentifier = @"newsCellIdentifier";
    ZSTVideoMainCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
    
    if (cell==nil) {
        cell = [[[ZSTVideoMainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if ([self.dataArray count] != 0) {
        
        ZSTVideoBVO *videoListInfo = [self.dataArray objectAtIndex:indexPath.row];
        [cell configCell:videoListInfo];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   if (indexPath.row == self.dataArray.count) //更多按钮
        { 
            return 44;
        }
    
    return 78;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ZSTVideoBVO *videoInfo = [self.dataArray objectAtIndex:indexPath.row];
    
    ZSTVideoPlayerController *videoView = [[ZSTVideoPlayerController alloc] init];
    videoView.playUrl = videoInfo.fileurl;
    videoView.videoId = videoInfo.videoid;
    [self presentViewController:videoView animated:YES completion:nil];
    [videoView release];
}

#pragma mark - APIClinet Delegate

- (void)getVideoBListDidSucceed:(NSArray *)videoList hasMore:(BOOL)hasMore;
{
    [TKUIUtil hiddenHUD];
    
    _hasMore = hasMore;
    
    if (isRefresh) {
        _pageIndex = 1;
        [self insertData:videoList];
    } else {
        _pageIndex++;
        [self appendData:videoList];
    }
    
    [self doneLoadingData];
}

- (void)appendData:(NSArray*)dataarray
{
    [self.dataArray addObjectsFromArray:dataarray];
}

- (void)insertData:(NSArray*)dataarray
{
    self.dataArray = [NSMutableArray arrayWithArray:dataarray];
}


- (void) getVideoBListDidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"加载失败", nil) withImage:nil];
}

- (void)doneLoadingData
{
    _loading = NO;
    [_moreButton hideIndicator];
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

//顶上下拽刷新，开始加载第1页数据
- (void)reloadTableViewDataSource
{
    //加载第1页
	_loading = YES;
    _pageIndex = 1;
	[self loadDataForPage:_pageIndex];
}


- (void)loadMore
{
    if (_hasMore)
    {
        _loading = YES;
        [_moreButton displayIndicator];
        
        [self loadDataForPage:_pageIndex+1];
    }
}


//这个是处理数据条数不够一屏的情况
- (float)tableViewHeight
{
    if (self.tableView.contentSize.height < self.tableView.frame.size.height)
    {
        return self.tableView.frame.size.height;
    }
    else
    {
        return self.tableView.contentSize.height;
    }
}

//这个是处理数据条数不够一屏的情况
- (float)endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}

#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([self endOfTableView:scrollView] <= -65.0f && !_loading)
    {
        [self loadMore];
    }
    else
    {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
	[self reloadTableViewDataSource];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
	return _loading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
	return [NSDate date];
}

-(void)dealloc
{
    [self.tableView release];
    [self.dataArray release];
    [super dealloc];
}

@end
