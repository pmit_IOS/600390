//
//  ZSTDao+VideoB.m
//  VideoB
//
//  Created by lizhenqu on 13-3-28.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTDao+VideoB.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "TKUtil.h"

#define CREATE_TABLE_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [VideoB_List_%@] (\
                                                                    ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                    videoid  VARCHAR DEFAULT '', \
                                                                    videoname VARCHAR DEFAULT '', \
                                                                    videomemo VARCHAR DEFAULT '', \
                                                                    videotype INTEGER  DEFAULT 0, \
                                                                    iconurl VARCHAR  DEFAULT '',\
                                                                    fileurl VARCHAR  DEFAULT '',\
                                                                    duration INTEGER DEFAULT 0, \
                                                                    ordernum INTEGER  DEFAULT 0, \
                                                                    favoritecount INTEGER  DEFAULT 0, \
                                                                    supportcount INTEGER  DEFAULT 0, \
                                                                    tramplecount INTEGER  DEFAULT 0, \
                                                                    description VARCHAR  DEFAULT '', \
                                                                    addtime VARCHAR  DEFAULT '' \
                                                                );", @(moduleType)]

@implementation ZSTDao(VideoB)

- (void)createTableIfNotExistForVIDEOBModule
{
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD(self.moduleType)];
}

- (BOOL)addVideoID:(NSString*)videoid
         videoName:(NSString*)videoname
         videoMemo:(NSString*)videomemo
         videoType:(NSInteger)videotype
           iconUrl:(NSString*)iconurl
           fileUrl:(NSString*)fileurl
          duration:(NSInteger)duration
          orderNum:(NSInteger)ordernum
     favoriteCount:(NSInteger)favoritecount
      supportCount:(NSInteger)supportcount
      trampleCount:(NSInteger)tramplecount
       description:(NSString*)description
           addTime:(NSString*)addtime
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO VideoB_List_%@ (videoid, videoname, videomemo, videotype, iconurl, fileurl, duration, favoritecount, ordernum, supportcount,tramplecount,description,addtime) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", @(self.moduleType)],
                    
                    [TKUtil wrapNilObject:videoid],
                    [TKUtil wrapNilObject:videoname],
                    [TKUtil wrapNilObject:videomemo],
                    [NSNumber numberWithInteger:videotype],
                    [TKUtil wrapNilObject:iconurl],
                    [TKUtil wrapNilObject:fileurl],
                    [NSNumber numberWithInteger:duration],
                    [NSNumber numberWithInteger:ordernum],
                    [NSNumber numberWithInteger:favoritecount],
                    [NSNumber numberWithInteger:supportcount],
                    [NSNumber numberWithInteger:tramplecount],
                    [TKUtil wrapNilObject:description],
                    [TKUtil wrapNilObject:addtime]
                    ];
    return success;
}

- (BOOL)deleteAllVideos
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM VideoB_List_%@", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL]) {
        
        return NO;
    }
    return YES;
}

- (NSArray *)getVideosList
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, videoid, videoname, videomemo, videotype, iconurl, fileurl, duration, favoritecount, ordernum, supportcount,tramplecount,description,addtime FROM VideoB_List_%@", @(self.moduleType)];
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    return results;
}

- (BOOL)videoExist:(NSString*)videoID
{
     return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT COUNT(ID) as cnt FROM VideoB_List_%@ where videoid = ?", @(self.moduleType)], videoID];
}

- (BOOL)deleteVideo:(NSString*)videoID
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM VideoB_List_%@ WHERE videoid = ?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL, videoID]) {
        
        return NO;
    }
    return YES;
}

- (NSUInteger)videoCount
{
     return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT COUNT(ID) FROM VideoB_List_%@ ", @(self.moduleType)]];
}

- (NSUInteger) getMaxVideoId
{
    //不能用max(mblogid)，因为mblogid ascii排序有问题
	return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"select videoid from VideoB_List_%@", @(self.moduleType)]];
}

- (NSUInteger) getMinVideoId
{
    //不能用max(mblogid)，因为mblogid ascii排序有问题
	return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"select videoid from VideoB_List_%@", @(self.moduleType)]];
}

@end