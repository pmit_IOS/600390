//
//  ZSTVideoBVO.m
//  VideoB
//
//  Created by lizhenqu on 13-3-27.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTVideoBVO.h"

@implementation ZSTVideoBVO

- (id)initWithDic:(NSDictionary *)dic
{
    if (![dic isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    self = [super init];
    if (self) {
        
        self.notice = [dic safeObjectForKey:@"notice"];
        self.hasmore = [[dic safeObjectForKey:@"hasmore"] boolValue];
        self.videoid = [dic safeObjectForKey:@"videoid"];
        self.videotype = [[dic safeObjectForKey:@"videotype"] integerValue];
        self.videoname = [dic safeObjectForKey:@"videoname"];
        self.videomemo = [dic safeObjectForKey:@"videomemo"];
        self.iconurl = [dic safeObjectForKey:@"iconurl"];
        self.fileurl = [dic safeObjectForKey:@"fileurl"];
        self.duration = [[dic safeObjectForKey:@"duration"] integerValue];
        self.ordernum = [[dic safeObjectForKey:@"ordernum"] integerValue];
        self.favoritecount = [[dic safeObjectForKey:@"favoritecount"] integerValue];
        self.supportcount = [[dic safeObjectForKey:@"supportcount"] integerValue];
        self.tramplecount = [[dic safeObjectForKey:@"tramplecount"] integerValue];
        self.Description = [dic safeObjectForKey:@"description"];
        self.addtime = [dic safeObjectForKey:@"addtime"];
        
    }
    return self;
}

+ (ZSTVideoBVO*)videoListInfoWithdic:(NSDictionary*)dic
{
    if (!dic) {
        return nil;
    }
    ZSTVideoBVO* info = [[[ZSTVideoBVO alloc] initWithDic:dic] autorelease];
    return info;
}

+ (NSMutableArray*)videoListWithdic:(NSDictionary*)dic
{
    NSArray *array = [dic valueForKey:@"info"];
    if (![array isKindOfClass:[NSArray class]]) {
        return nil;
    }
    
    NSMutableArray *result = [NSMutableArray array];
    for (int i=0; i<array.count; i++) {
        NSDictionary *dictionary = [array objectAtIndex:i];
        if (![dictionary isKindOfClass:[NSDictionary class]]) {
            continue;
        }
        
        ZSTVideoBVO* info = [[[ZSTVideoBVO alloc] initWithDic:dictionary] autorelease];
        if (info != nil) {
            [result addObject:info];
        }
    }
    
    return result;
}

+ (NSMutableArray*)videoListLocalWithdic:(NSArray*)array
{
    if (![array isKindOfClass:[NSArray class]]) {
        return nil;
    }
    
    NSMutableArray *result = [NSMutableArray array];
    for (int i=0; i<array.count; i++) {
        NSDictionary *dictionary = [array objectAtIndex:i];
        if (![dictionary isKindOfClass:[NSDictionary class]]) {
            continue;
        }
        
        ZSTVideoBVO* info = [[[ZSTVideoBVO alloc] initWithDic:dictionary] autorelease];
        if (info != nil) {
            [result addObject:info];
        }
    }
    
    return result;
}

- (void) dealloc
{
    self.notice = nil;
    self.videoid = nil;
    self.videoname = nil;
    self.videomemo = nil;
    self.iconurl = nil;
    self.fileurl = nil;
    self.Description = nil;;
    self.addtime = nil;
   
    [super dealloc];
}

@end



