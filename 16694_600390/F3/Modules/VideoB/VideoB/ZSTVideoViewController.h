//
//  VideoViewController.h
//  VoiceChina
//
//  Created by xuhuijun on 13-2-22.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"
#import "ZSTF3Engine.h"
#import "ZSTF3Engine+VideoB.h"

@interface ZSTVideoViewController : ZSTModuleBaseViewController <UITableViewDelegate,UITableViewDataSource,ZSTF3EngineVideoBDelegate,EGORefreshTableHeaderDelegate>
{
     EGORefreshTableHeaderView *_refreshHeaderView;
}

@property (nonatomic, retain) UITableView *tableView;

@property (nonatomic, retain) NSMutableArray *dataArray;

@end
