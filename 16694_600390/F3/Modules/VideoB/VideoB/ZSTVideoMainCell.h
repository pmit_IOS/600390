//
//  NewsMainCell.h
//  F3
//
//  Created by admin on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTVideoBVO.h"
#import "TKAsynImageView.h"

@interface ZSTVideoMainCell : UITableViewCell {
//    TKAsynImageView *cellImageView;
    UIImageView *cellImageView;
    UILabel *title;
    UILabel *subTitle;
    NSInteger newsId;
    UILabel *addTime;
}

//@property (nonatomic,retain) TKAsynImageView *cellImageView;
@property (nonatomic,retain) UIImageView *cellImageView;
@property (nonatomic,retain) UILabel *title;
@property (nonatomic,retain) UILabel *subTitle;
@property (nonatomic,retain) UILabel *addTime;

- (void)configCell:(ZSTVideoBVO*)nvo;
@end

