//
//  ZSTGlobal+NewsB.h
//  NewsB
//
//  Created by xuhuijun on 13-7-18.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <Foundation/Foundation.h>

#define  NewsBPageSize 10

#define SortType_Desc @"Desc"               //降序 (刷新)
#define SortType_Asc @"Asc"                 //升序（更多）

#define NewsBBaseURL @"http://mod.pmit.cn/NewsB"//最新接口


#define GetNewsBCatagory NewsBBaseURL @"/GetCatagory"
#define GetNewsBCarousel NewsBBaseURL @"/GetCarousel"
#define GetNewsBListByPage NewsBBaseURL @"/GetNewsListByPage"
#define GetNewsBContent NewsBBaseURL @"/GetNewsContent"
#define InfoAddComment NewsBBaseURL @"/AddComment"//文章评论添加
#define GetInfoComment NewsBBaseURL @"/GetComment"//文章评论获取

