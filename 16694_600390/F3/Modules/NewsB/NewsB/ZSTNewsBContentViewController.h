//
//  NewsContentViewController.h
//  F3
//
//  Created by admin on 12-7-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTNewsBVO.h"
#import "MGTemplateEngine.h"
#import "ICUTemplateMatcher.h"
#import "TKHorizontalTableView.h"
#import "ZSTF3Engine+NewsB.h"
#import "ZSTNewsBContentVO.h"
#import "TKAsynImageView.h"
#import "MTZoomWindowDelegate.h"
#import <MessageUI/MessageUI.h>
#import "ZSTHHSinaShareController.h"
#import "ZSTNewsBFontView.h"
#import "ZSTLoginViewController.h"

typedef enum
{
    FontSize_Small = 0,
    FontSize_Normal = 1,
    FontSize_Middle = 2,                        
    FontSize_Large = 3
} ZSTNewsBFontSize;

@interface ZSTNewsBContentViewController : UIViewController <UIWebViewDelegate, MGTemplateEngineDelegate,
    TKHorizontalTableViewDelegate,TKHorizontalTableViewDataSource,MTZoomWindowDelegate,HJManagedImageVDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,MFMessageComposeViewControllerDelegate,ZSTHHSinaShareControllerDelegate,ZSTNewsBFontViewDelegate,LoginDelegate> {
    
    TKAsynImageView *selectedImageView;
    
    NSArray *newsArr;

    TKHorizontalTableView *hTableView;
    
    NSString *requestUrl;
        
    ZSTNewsBFontSize fontSize;
        
}
@property (nonatomic, retain) TKHorizontalTableView *hTableView;
@property (nonatomic,retain) NSArray *newsArr;
@property (nonatomic,retain) TKAsynImageView *selectedImageView;

@property (nonatomic, retain) ZSTNewsBVO *vo ;
@property (nonatomic ,retain) ZSTNewsBContentVO *contentVo;
@property (nonatomic, retain) ZSTNewsBFontView *actionView;

@property (nonatomic, assign) NSInteger selectedIndex;

- (CGSize)displayRectForImage:(CGSize)imageSize;
@end
