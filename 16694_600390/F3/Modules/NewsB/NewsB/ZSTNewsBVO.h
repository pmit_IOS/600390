//
//  NewsVO.h
//  F3Engine
//
//  Created by admin on 12-7-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTNewsBVO : NSObject

@property (nonatomic, assign) int msgid;
@property (nonatomic, assign) int categoryid;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *source;
@property (nonatomic, retain) NSString *iconurl;
@property (nonatomic, retain) NSString *addtime;
@property (nonatomic, assign) int ordernum;
@property (nonatomic, assign) int isread;
@property (nonatomic, retain) NSString *linkUrl;


+ (id)voWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;


@end
