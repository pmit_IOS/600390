//
//  ZSTInfoBEmptyTableCell.m
//  infob
//
//  Created by luobin on 11/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTNewsBEmptyTableCell.h"

@implementation ZSTNewsBEmptyTableCell

@synthesize introduceLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        introduceLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 240, 416)];
        introduceLabel.numberOfLines = 1;
        introduceLabel.textAlignment = NSTextAlignmentCenter;
        introduceLabel.textColor = RGBCOLOR(150, 150, 150);
        introduceLabel.backgroundColor = [UIColor clearColor];
        introduceLabel.font = [UIFont systemFontOfSize:18];
        [self.contentView addSubview:introduceLabel];
    }
    return self;
}

- (void)setIntroduce:(NSString *)introduce
{
    self.introduceLabel.text = introduce;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    CGContextRef cr = UIGraphicsGetCurrentContext();
    [RGBCOLOR(241, 241, 241) set];
    [[UIColor clearColor] set];
    CGContextFillRect(cr, self.bounds);
    
}
- (void)dealloc
{
    TKRELEASE(introduceLabel);
    [super dealloc];
}

@end
