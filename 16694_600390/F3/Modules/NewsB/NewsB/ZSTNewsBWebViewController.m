//
//  ZSTNewsBWebViewController.m
//  NewsB
//
//  Created by xuhuijun on 13-8-2.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTNewsBWebViewController.h"
#import "ZSTUtils.h"

@interface ZSTNewsBWebViewController ()

@end

@implementation ZSTNewsBWebViewController
@synthesize webUrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    [self setURL:[webUrl description]];
    self.type = self.moduleType;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
