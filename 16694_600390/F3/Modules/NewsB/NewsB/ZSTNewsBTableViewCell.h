//
//  ZSTNewsBTableViewCell.h
//  NewsB
//
//  Created by xuhuijun on 13-7-23.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarouselView.h"

@interface ZSTNewsBTableViewCell : UITableViewCell

@property (nonatomic,retain) UILabel *titleLabel;
@property (nonatomic,retain) UILabel *sourceLabel;
@property (nonatomic,retain) UILabel *timeLabel;
//@property (nonatomic,retain) TKAsynImageView *asynView;
@property (nonatomic,retain) UIImageView *asynView;

- (void)configCell:(NSDictionary *)dic index:(NSUInteger)index;

@end


@protocol ZSTNewsBCarouselViewCellDelegate <NSObject>

@optional
- (void)carouselViewCell:(CarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
@end

@interface ZSTNewsBCarouselViewCell : UITableViewCell<CarouselViewDataSource,CarouselViewDelegate>
{
    CarouselView *carouselView;
    NSArray *dataArry;
    id<ZSTNewsBCarouselViewCellDelegate>carouselViewCellDelegate;
}
@property (nonatomic, retain) CarouselView *carouselView;
@property (nonatomic ,retain) NSArray *dataArry;
@property (nonatomic ,retain) id<ZSTNewsBCarouselViewCellDelegate>carouselViewCellDelegate;

- (void)configCell:(NSArray*)array;

@end

