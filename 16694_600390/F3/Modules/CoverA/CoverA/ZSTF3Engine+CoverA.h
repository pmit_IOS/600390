//
//  ZSTF3Engine+CoverA.h
//  CoverA
//
//  Created by xuhuijun on 13-1-16.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine.h"

@protocol ZSTF3EngineCoverADelegate <ZSTF3EngineDelegate>

- (void)getCoverAMessageDidSucceed:(NSArray *)coverAArray;
- (void)getCoverAMessageDidFailed:(int)resultCode;

@end

@interface ZSTF3Engine (CoverA)

/**
 *	@brief	获取封面信息
 *
 */
- (void)getCoverAmessage;

@end
