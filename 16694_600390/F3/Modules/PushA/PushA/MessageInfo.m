//
//  NSMInfo.m
//  F3_UI
//
//  Created by huang austin on 11-6-17.
//  Copyright 2011 北航. All rights reserved.
//

#import "MessageInfo.h"


@implementation MessageInfo

@synthesize state;
@synthesize ID;
@synthesize MSGID;
@synthesize pushId;
@synthesize typeId;
@synthesize userID;
@synthesize userName;
@synthesize encryptKey;
@synthesize subject;
@synthesize time;
@synthesize forwarded;
@synthesize isReceived;
@synthesize isPrivate;
@synthesize isLocked;
@synthesize content;
@synthesize fileId;
@synthesize report;

//-(NSString *) description
//{
//    NSMutableString *desc = [[NSMutableString alloc] initWithFormat: 
//                             @"ID = %d, MSGID = %@, pushId = %@, typeId = %@, userID = %@, userName = %@, encryptKey = %@, subject = %@, time = %d, "
//                             "state = %@, forwarded = %@, isReceived=%d,isPrivate = %d, isLocked = %d, content = %@ ,fileId = %@", 
//                             ID, MSGID, pushId, typeId, userID, userName, encryptKey, subject, time, state, forwarded, isReceived, isPrivate, isLocked, content,fileId];
//	
//	return [desc autorelease];
//}

-(NSString *) sendDescription
{
	return [self description];
}

- (id)copyWithZone:(NSZone *)zone
{
    MessageInfo *messageInfo = [[MessageInfo allocWithZone:zone] init]; 
    messageInfo.ID = self.ID;
    messageInfo. state = self.state;
    messageInfo. MSGID = [[self.MSGID copy] autorelease];
    messageInfo. pushId = [[self.pushId copy] autorelease];
    messageInfo. typeId = [[self.typeId copy] autorelease];
    messageInfo. userID = [[self.userID copy] autorelease];
    messageInfo. userName = [[self.userName copy] autorelease];
    messageInfo. encryptKey = [[self.encryptKey copy] autorelease];
    messageInfo. subject = [[self.subject copy] autorelease];
    messageInfo. time = [[self.time copy] autorelease];
    messageInfo. forwarded = [[self.forwarded copy] autorelease];
    messageInfo. isReceived = self.isReceived;
    messageInfo. isPrivate = self.isPrivate;
    messageInfo. isLocked = self.isLocked;
    messageInfo. content = [[self.content copy] autorelease];
    messageInfo. fileId  = [[self.fileId copy] autorelease];
    messageInfo. report = self.report;
    return messageInfo;
}

-(void) dealloc
{
    self.MSGID = nil;
    self.pushId = nil;
    self.typeId = nil;
    self.userID = nil;
    self.userName = nil;
    self.encryptKey = nil;
    self.subject = nil;
    self.time = nil;
    self.forwarded = nil;
    self.content = nil;
    self.fileId = nil;
	[super dealloc];
}


@end
