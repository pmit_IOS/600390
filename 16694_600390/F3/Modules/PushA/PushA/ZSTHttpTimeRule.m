//
//  ZSTHttpTimeRule.m
//  
//
//  Created by xuhuijun on 12-4-13.
//  Copyright 2012年 掌上通. All rights reserved.
//

#import "ZSTHttpTimeRule.h"
#import "ZSTUtils.h"

@implementation ZSTTimeRange

@synthesize startTime;
@synthesize endTime;
@synthesize interval;

- (NSArray *)getTimePointsForDate:(NSDate *)date
{
    NSMutableArray *timePoints = [NSMutableArray array];
    
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    
    NSDateFormatter *formater = [[ NSDateFormatter alloc] init];
    [formater setDateFormat:@"HH':'mm':'ss"];
    
    //开始时间
    NSDate *startDate = [formater dateFromString:self.startTime];
    NSDateComponents *startComps =[gregorian components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:startDate];
    [startComps setYear:date.year];
    [startComps setMonth:date.month];
    [startComps setDay:date.day];
    startDate = [gregorian dateFromComponents:startComps];
    
    //结束时间
    NSDate *endDate = [formater dateFromString:self.endTime];
    NSDateComponents *endComps =[gregorian components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:endDate];
    [endComps setYear:date.year];
    [endComps setMonth:date.month];
    [endComps setDay:date.day];
    endDate = [gregorian dateFromComponents:endComps];
    
    while (![startDate isLaterThanDate:endDate]) {
        [timePoints addObject:startDate];
        startDate = [startDate dateByAddingTimeInterval:self.interval];
    }
    
    [formater release];
    
    return timePoints;
}

- (void)dealloc
{
    self.startTime = nil;
    self.endTime = nil;
    [super dealloc];
}

@end


@interface ZSTHttpTimeRule()

@property (nonatomic, retain, readwrite) NSArray *timeRanges;

@end

@implementation ZSTHttpTimeRule
@synthesize timeRanges;

- (id)initWithHTTPPollTimeRule:(NSString *)httpPollTimeRule
{
    self = [super init];
    
    if (self) {
        
        NSMutableArray *theTimeRanges = [NSMutableArray array];
        
        NSArray *HTTPPollTimeRuleArray = [httpPollTimeRule componentsSeparatedByString:@"|"];
        for (NSString *aHTTPPollTimeRule in HTTPPollTimeRuleArray) {
            NSArray *aHTTPPollTimeRuleArray = [aHTTPPollTimeRule componentsSeparatedByString:@","];
            if ([aHTTPPollTimeRuleArray count] >= 3) {
                ZSTTimeRange *item = [[[ZSTTimeRange alloc] init] autorelease];
                item.startTime = [ZSTUtils convertToStandardTime:[aHTTPPollTimeRuleArray objectAtIndex:0]];
                item.endTime = [ZSTUtils convertToStandardTime:[aHTTPPollTimeRuleArray objectAtIndex:1]];
                item.interval = [[aHTTPPollTimeRuleArray objectAtIndex:2] intValue]*60;
                [theTimeRanges addObject:item];
            }
        }
        self.timeRanges = theTimeRanges;
    }
    return self;
}

- (NSArray *)getTimePointsForDate:(NSDate *)date
{
    NSParameterAssert(date);
    
    NSMutableArray *timePoints = [NSMutableArray array];
    for (ZSTTimeRange *range in timeRanges) {
        
        [timePoints addObjectsFromArray:[range getTimePointsForDate:date]];
    }
    return timePoints;
}

- (void)dealloc
{
    self.timeRanges = nil;
    [super dealloc];
}


@end
