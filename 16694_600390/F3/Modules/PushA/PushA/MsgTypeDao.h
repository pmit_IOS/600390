//
//  ECECCDao.h
//  F3
//
//  Created by 9588 on 9/26/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MsgTypeInfo.h"

@interface MsgTypeDao : NSObject {
    
}

+ (MsgTypeDao *)shareMsgTypeDao;

//插入或替换一个消息类型
- (BOOL)insertOrReplaceMsgTypeInfo:(MsgTypeInfo *)info;

//插入消息类型
- (BOOL)insertMsgTypeInfo:(MsgTypeInfo *)info;

//获取所有消息类型
-(NSArray *) getMsgTypeInfos;

//获取消息类型的图标
- (MsgTypeInfo *)getMsgTypeInfoByTypeID:(NSString *)typeID;

//删除消息类型
-(void)deleteMsgTypeInfo:(NSString *)typeID;

//删除所有消息类型
-(void)deleteAllMsgTypeInfo;

// 删除不存在信息的分类
- (void)deleteMsgTypeInfoWhichNoMessage;

//修改消息类型的屏蔽状态
-(BOOL)setMsgTypeInfo:(NSString *)typeID shielded:(BOOL)isShielded;

-(NSData *)getIcon:(NSString *)typeId;

@end
