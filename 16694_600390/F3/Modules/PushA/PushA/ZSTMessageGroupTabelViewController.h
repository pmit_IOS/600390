//
//  MessageGroupTabelViewController.h
//  F3_UI
//
//  Created by mac on 11-7-2.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTMessagesViewController.h"
@class ZSTLauncherButtonView;
@class MsgTypeInfo;
@class ZSTEditView;
@class ZSTMessageTypeViewController;

@interface ZSTMessageGroupTabelViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>  
{
    NSMutableArray *_receiveArray;
    UITableView *_groupTaleView;
    UIButton *_newNMSButton;
    MsgTypeInfo *_info;
    UIImageView *_footerView;
    UIImageView *_backgroundView;
    
    BOOL _isLock;
    BOOL _editStateMark;
    BOOL _isEditing;
    
    ZSTEditView *_editView;
    ZSTEditView *_confirmDelView;
}

@property (retain, nonatomic) MsgTypeInfo *info;
@property (assign,nonatomic) ZSTMessageTypeViewController *typeDelegate;

- (void)onNMSUpdate:(NSNotification *) notification;


@end
