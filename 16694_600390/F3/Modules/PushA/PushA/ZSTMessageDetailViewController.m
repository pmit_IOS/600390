    //
//  ViewoutViewController.m
//  F3Client
//
//  Created by iPhone on 11-6-16.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "ZSTMessageDetailViewController.h"
#import "ZSTCreateNMSDialogViewController.h"
#import "MessageInfo.h"
#import "ZSTLogUtil.h"
#import "ZSTUtils.h"
#import "ZSTScrollToolBarView.h"
#import "EGOPhotoGlobal.h"
#import "ZSTEditView.h"

@implementation ZSTMessageDetailViewController

@synthesize filePathString = _filePathString;
@synthesize msgID = _msgID; 
@synthesize pushId = _pushId;
@synthesize userPhoneNumber = _userPhoneNumber;
@synthesize subject = _subject;
@synthesize iconImageView = _iconImageView;
@synthesize sendDate = _sendDate;
@synthesize inboxName = _inboxName;
@synthesize lazyLoad;
@synthesize curNmsInfo;
//@synthesize canEdit = _canEdit;

- (UIImage *)createBarButtonImage:(UIImage *)image withTitle:(NSString *)title
{
    CGSize targetSize =  CGSizeMake(150, 30);
    
    // Create a new context with the right size
    UIGraphicsBeginImageContext(targetSize);
    CGContextRef context =  UIGraphicsGetCurrentContext();
    
    CGContextSetAllowsAntialiasing(context, YES);
    
     // Draw the new tab bar image at the center
    
    [image drawInRect:CGRectMake((targetSize.width/2) - 25 - 10, 2, 25, 25)];
    
    [title drawInRect:CGRectMake(targetSize.width/2 , targetSize.height/2.0 - 20/2, 40, 20) withFont:[UIFont systemFontOfSize:14]];

    // Generate a new image
    UIImage* resultImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();

    
    return resultImage;
}

- (UIScrollView *)_getScrollView:(UIWebView *)webView
{
    for (id subview in webView.subviews){
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
        {
            return (UIScrollView *)subview;
        }
    }
    return nil;
}

-(void)setRightBarButtonItem
{
    UIButton *optionsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    optionsBtn.frame = CGRectMake(280, 7, 30, 30);
    optionsBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [optionsBtn setImage:[UIImage imageNamed:@"btn_webicon_forwardMessage.png"] forState:UIControlStateNormal];
    [optionsBtn setImage:[UIImage imageNamed:@"btn_webicon_forwardMessage_light.png"] forState:UIControlStateHighlighted];
    [optionsBtn addTarget:self action:@selector(forwardMessage) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:optionsBtn] autorelease];
}


#pragma mark -------ScrollToolBarView button selector ---------------------------

-(void)home
{
    [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath: _filePathString isDirectory:NO]]]; 
    
    [_scrollToolBarView reclock];
}

-(void) replyMessage
{
    [_scrollToolBarView reclock];

    ZSTCreateNMSDialogViewController * replyCreateNMSDialogViewController =[[ZSTCreateNMSDialogViewController alloc] init];
    replyCreateNMSDialogViewController.shouldSaveToDraft = NO;
    replyCreateNMSDialogViewController.isDraftEdit = NO;
    replyCreateNMSDialogViewController.hidesBottomBarWhenPushed = YES;
    [replyCreateNMSDialogViewController getReplyUserNumber:_userPhoneNumber userName:self.inboxName.text];
    [replyCreateNMSDialogViewController pushInNavigationViewController:self];  
    [replyCreateNMSDialogViewController  release];
    
}

-(void) forwardMessage
{
//    [_scrollToolBarView reclock];
    NSString *GP_Show_NewMessage = NSLocalizedString(@"GP_Show_NewMessage", nil);
    if ([GP_Show_NewMessage isEqualToString:@"0"]) {

    } else {
        ZSTCreateNMSDialogViewController * replyCreateNMSDialogViewController = [[ZSTCreateNMSDialogViewController alloc] init];
        replyCreateNMSDialogViewController.shouldSaveToDraft = NO;
        replyCreateNMSDialogViewController.isDraftEdit = NO;
        replyCreateNMSDialogViewController.subject = [NSString stringWithFormat:@"%@",_labelTitle.text];
        replyCreateNMSDialogViewController.pushId = self.pushId;
        replyCreateNMSDialogViewController.hidesBottomBarWhenPushed = YES;
        replyCreateNMSDialogViewController.textview.text = [NSString stringWithFormat:@"%@",_labelTitle.text];
        [replyCreateNMSDialogViewController pushInNavigationViewController:self];
        [replyCreateNMSDialogViewController  release];
    }
}

-(void)goBack
{
    [_webview goBack];
    [_scrollToolBarView reclock];
    
}
-(void)goForward
{
    [_webview goForward];
    [_scrollToolBarView reclock];
    
}

-(void)refresh
{
    [_webview reload];
    [_scrollToolBarView reclock];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        
    if (buttonIndex == 0) {
            
        Class messageClass = (NSClassFromString(@"MFMessageComposeViewController")); 
            
        if (messageClass != nil && [MFMessageComposeViewController canSendText]) {
                
            MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
                
            picker.body = [NSString stringWithFormat:NSLocalizedString(@"分享%@：%@,%@", nil),appDisplayName, _subject, @""];
                
            picker.messageComposeDelegate = self;
                
            [self presentModalViewController: picker animated:YES];
                
            [picker release];
        }
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // 直接检测服务器是否绑定成功
    if (result==MessageComposeResultSent) {
        [ZSTUtils showAlertTitle:nil message:NSLocalizedString(@"分享成功!" , nil)];
    }
}

#pragma mark ------------------------------------------------------------------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [ZSTUtils logoView];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self.navigationController selector:@selector(popViewController)];
    NSString *GP_Show_NewMessage = NSLocalizedString(@"GP_Show_NewMessage", nil);
    if (![GP_Show_NewMessage isEqualToString:@"0"]) {
        [self setRightBarButtonItem];
    }
    selectFromList = YES;
    
    UIImageView *imageview =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
	imageview.frame = CGRectMake(0, 0, 320, 480+(iPhone5?88:0));
	[self.view addSubview:imageview];
	[imageview release];
    
    _messageDescriptionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 55)];
    _messageDescriptionView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_messageDescriptionView];
    
    _iconImageView = [[TKAsynImageView alloc] initWithFrame: CGRectMake(5, 4, 50, 50)];
    _iconImageView.backgroundColor = [UIColor clearColor];
    _iconImageView.defaultImage = [UIImage imageNamed:@"icon.png"];  
    _iconImageView.adjustsImageWhenHighlighted = NO;
    [_messageDescriptionView addSubview: _iconImageView];
    [_iconImageView release];
    
    CALayer * iconImageViewLayer = [_iconImageView layer];
    [iconImageViewLayer setMasksToBounds:YES];
    [iconImageViewLayer setCornerRadius:6.0];
    [iconImageViewLayer setBorderWidth:1.0];
    [iconImageViewLayer setBorderColor:[[UIColor clearColor] CGColor]];
    
    _sendDate = [[UILabel alloc] initWithFrame: CGRectMake(170, 2, 145, 25)];
    _sendDate.font = [UIFont systemFontOfSize: 12];
    _sendDate.textAlignment = NSTextAlignmentRight;
    _sendDate.backgroundColor = [UIColor clearColor];
    [_messageDescriptionView addSubview: _sendDate];
    [_sendDate release];
    
    _inboxName = [[UILabel alloc] initWithFrame:CGRectMake(60, 2, 120, 25)];
    _inboxName.backgroundColor = [UIColor clearColor];
    _inboxName.font = [UIFont systemFontOfSize:14];
    [_messageDescriptionView addSubview: _inboxName];
    [_inboxName release];
    
    _labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, 30, 255, 25)];
	_labelTitle.text = @"";
    _labelTitle.backgroundColor = [UIColor clearColor];
    _labelTitle.textAlignment = NSTextAlignmentLeft;
    _labelTitle.font = [UIFont systemFontOfSize:14];
    _labelTitle.textColor = [UIColor whiteColor];
    _labelTitle.text = _subject;
	[_messageDescriptionView addSubview:_labelTitle];
	[_labelTitle release];
    
    _webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, 320, 355+(iPhone5?88:0))];
    _webview.scalesPageToFit =  YES;
    _webview.backgroundColor = [ZSTUtils getBGColor];

    _messageDescriptionView.hidden = YES;
    _webview.frame = CGRectMake(0, 1, 320, 355+59+(iPhone5?88:0));
    
    NSString *GP_Setting_Items = NSLocalizedString(@"GP_Setting_Items", nil);
    if ([GP_Setting_Items isEqualToString:@"GP_Setting_Items"]) {
        GP_Setting_Items = @"Register,Recommend, Suggest, Update, Help, About, Sound, Vibrate, Report, FullScreen, MessageClear,systemLog";
    }
    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSArray *settingItems = [GP_Setting_Items componentsSeparatedByString:@","];
    
    if (![ZSTF3Preferences shared].fullScreen && [settingItems containsObject:@"FullScreen"]) {
        _messageDescriptionView.hidden = NO;
        _webview.frame = CGRectMake(0, 60, 320, 355+(iPhone5?88:0));
    }
    
    
    CALayer * layer = [_webview layer];
    [layer setMasksToBounds:YES];
    [layer setCornerRadius:3.0];
    [layer setBorderWidth:1.0];
    [layer setBorderColor:[[UIColor clearColor] CGColor]];
    _webview.delegate = self;
    
    [self.view addSubview:_webview];
    [_webview release];
    
    UIScrollView *scrollView = [self _getScrollView:_webview];
    for (UIView *subView in [scrollView subviews]) 
    {
        if ([[subView class] isSubclassOfClass:[UIImageView class]]) 
            subView.hidden = YES;
    }
    
    UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [homeButton setImage:[UIImage imageNamed:@"btn_webicon_home.png"] forState:UIControlStateNormal];
    [homeButton setImage:[UIImage imageNamed:@"btn_webicon_home_light.png"] forState:UIControlStateHighlighted];
    [homeButton addTarget:self action:@selector(home) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *forwardMessageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [forwardMessageBtn setImage:[UIImage imageNamed:@"btn_webicon_forwardMessage.png"]  forState:UIControlStateNormal];
    [forwardMessageBtn setImage:[UIImage imageNamed:@"btn_webicon_forwardMessage_light.png"] forState:UIControlStateHighlighted];
    [forwardMessageBtn addTarget:self action:@selector(forwardMessage) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *replyMessageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [replyMessageBtn setImage:[UIImage imageNamed:@"btn_webicon_replyMessage.png"]  forState:UIControlStateNormal];
    [replyMessageBtn setImage:[UIImage imageNamed:@"btn_webicon_replyMessage_light.png"] forState:UIControlStateHighlighted];
    [replyMessageBtn addTarget:self action:@selector(replyMessage) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [refreshButton setImage:[UIImage imageNamed:@"btn_webicon_reload.png"]  forState:UIControlStateNormal];
    [refreshButton setImage:[UIImage imageNamed:@"btn_webicon_reload_light.png"] forState:UIControlStateHighlighted];
    [refreshButton addTarget:self action:@selector(refresh) forControlEvents:UIControlEventTouchUpInside];
    
    _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backButton setImage:[UIImage imageNamed:@"btn_webicon_backward.png"]  forState:UIControlStateNormal];
    [_backButton setImage:[UIImage imageNamed:@"btn_webicon_backward_light.png"] forState:UIControlStateHighlighted];
    [_backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    _forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_forwardButton setImage:[UIImage imageNamed:@"btn_webicon_forward.png"] forState:UIControlStateNormal];
    [_forwardButton setImage:[UIImage imageNamed:@"btn_webicon_forward_light.png"] forState:UIControlStateHighlighted];
    [_forwardButton addTarget:self action:@selector(goForward) forControlEvents:UIControlEventTouchUpInside];
    
    _scrollToolBarView = [[ZSTScrollToolBarView alloc] initWithFrame:CGRectMake(1+290, 480-105+(iPhone5?88:0), 325, 40) color:[UIColor blackColor] alpha:0.6];
    if ([GP_Show_NewMessage isEqualToString:@"0"]) {
        _scrollToolBarView.items = [NSArray arrayWithObjects:homeButton,refreshButton,_backButton,_forwardButton, nil];
        _scrollToolBarView.imageItems = [NSArray arrayWithObjects:@"btn_webicon_home.png",@"btn_webicon_reload.png",@"btn_webicon_backward.png",@"btn_webicon_forward.png",nil];
    } else {
        _scrollToolBarView.items = [NSArray arrayWithObjects:homeButton,forwardMessageBtn,replyMessageBtn,_backButton,_forwardButton, nil];
        _scrollToolBarView.imageItems = [NSArray arrayWithObjects:@"btn_webicon_home.png",@"btn_webicon_forwardMessage.png",@"btn_webicon_replyMessage.png",@"btn_webicon_backward.png",@"btn_webicon_forward.png",nil];
    }
    
    _scrollToolBarView.delegate = self;
    [self.view addSubview:_scrollToolBarView];
    
    NSString *minimumSystemVersion = @"3.2";
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    if ([systemVersion compare:minimumSystemVersion options:NSNumericSearch] != NSOrderedAscending)
    {
        UITapGestureRecognizer* doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        doubleTap.numberOfTapsRequired = 2;
        [_webview addGestureRecognizer:doubleTap];
        doubleTap.delegate = self;
        doubleTap.cancelsTouchesInView = YES;
        [doubleTap release]; 
    }
    
    [ZSTLogUtil logUserAction:NSStringFromClass([ZSTMessageDetailViewController class])];

}

#pragma mark -------- UIGestureRecognizerDelegate ----------------

- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{

    if ([animationID isEqualToString:@"WebFrameZoomIn"]) {
        _messageDescriptionView.hidden = YES;
    } else if([animationID isEqualToString:@"WebFrameZoomOut"])
    {
        _webview.frame = CGRectMake(0, 60, 320, 355+(iPhone5?88:0));
    }
}

-(void)handleDoubleTap:(UIGestureRecognizer *)sender
{
    if (_messageDescriptionView.frame.origin.y == 0) {
        [UIView beginAnimations:@"WebFrameZoomIn" context:nil];
        [UIView setAnimationDuration:0.4];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
        _messageDescriptionView.frame = CGRectMake(0, -60, 320, 55);
        _webview.frame = CGRectMake(0, 1, 320, 355+59+(iPhone5?88:0));
        
        [UIView commitAnimations];
    }else
    {
        CGRect webViewFrame = _webview.frame;
        webViewFrame.origin.y += 59;
        _messageDescriptionView.hidden = NO;
        [UIView beginAnimations:@"WebFrameZoomOut" context:nil];
        [UIView setAnimationDuration:0.4];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
        
        _messageDescriptionView.frame = CGRectMake(0, 0, 320, 55);
        _webview.frame = webViewFrame;
        
        [UIView commitAnimations];
    }

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)updateView:(MessageInfo *)nmsInfo
{
    self.navigationItem.rightBarButtonItem.enabled = YES;
    self.subject = nmsInfo.subject;
    _labelTitle.text = nmsInfo.subject;
    self.userPhoneNumber = nmsInfo.userID;
    
    NSString * str = [[ZSTUtils pathForECECC] stringByAppendingPathComponent:nmsInfo.pushId];
    NSString * displayString = [str stringByAppendingPathComponent:@"Index.htm"];

    [self setFilePathString:displayString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:NSLocalizedString(@"M月dd日 HH:mm", nil)];
    NSString *dateString = [formatter stringFromDate:nmsInfo.time];
    
    self.sendDate.text = dateString;
    [formatter release];
    
    self.inboxName.text = nmsInfo.userID;
    self.iconImageView.image = [UIImage imageNamed:@"personal.png"];
    
    [_activityIndicatorView stopAnimating];
}

- (void)onNMSDownload_Complete: (NSNotification *)notification
{
    MessageInfo *nmsInfo = [notification.userInfo objectForKey:NotificationNMSKey];
    if (nmsInfo != nil && [nmsInfo.pushId isEqualToString: _pushId]) {
        
        [self performSelectorOnMainThread:@selector(updateView:) withObject:nmsInfo waitUntilDone:NO];
    }
}

//判断路径是否是表情
- (BOOL)isEmoticon:(NSString *)path
{
    NSString *name = [path lastPathComponent];
    if ([name length] == 21 && [name hasPrefix:@"f0"] && [name hasSuffix:@".png"]) {
        NSString *s = [name substringWithRange:NSMakeRange(15, 2)];
        return [s isNumber]&&[s intValue] < 92;
    }
    return NO;
}

#pragma mark --------UIWebViewDelegate---------

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
{
    NSURL *requestURL =[request URL];
    
    BOOL flag = NO;
    if ([requestURL isFileURL]) {
        NSString *path = [requestURL path];
        
        NSRange range = [path rangeOfString:[NSString stringWithFormat:@"/Documents/%@/inbox/%@/", [ZSTF3Preferences shared].ECECCID, _pushId]];
        if (range.location != NSNotFound) {
        
            flag = YES;
            if (selectFromList) {
                [ZSTLogUtil logUserView:[NSString stringWithFormat:@"%d", 3] pushId:_pushId messageId:self.msgID];
                selectFromList = NO;
            }else{
                [ZSTLogUtil logUserView:[path substringFromIndex:range.location + range.length - 1] pushId:_pushId messageId:self.msgID];
            }

        }
    }
    if (!flag) {
        [ZSTLogUtil logUserView:[NSString stringWithFormat:@"%@",requestURL] pushId:_pushId messageId:self.msgID];
    }
    
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        
        if ([self isEmoticon:[requestURL path]]) {
            return NO;
        }
        
        if ([requestURL isFileURL] && [ZSTUtils isImageURL:requestURL]) {
            
            NSMutableArray *photos = [NSMutableArray array];
            
            EGOPhoto *photo = [[EGOPhoto alloc] initWithImageURL:requestURL];
            [photos addObject:photo];
            [photo release];
            
            NSString *docsDir = [[requestURL path] stringByDeletingLastPathComponent];
            NSFileManager *localFileManager = [[NSFileManager alloc] init];
            NSDirectoryEnumerator *dirEnum = [localFileManager enumeratorAtPath:docsDir];
            
            NSString *file;
            while ((file = [dirEnum nextObject]) != nil) {
                
                if ([ZSTUtils isImagePath:file] && ![file isEqualToString:[[requestURL absoluteString] lastPathComponent]] && ![self isEmoticon:file]) {
                    
                    NSString *filePath = [docsDir stringByAppendingPathComponent:file];
                    EGOPhoto *photo = [[EGOPhoto alloc] initWithImageURL:[NSURL fileURLWithPath:filePath]];
                    [photos addObject:photo];
                    [photo release];
                }
            }
            [localFileManager release];
            
            EGOPhotoSource *source = [[EGOPhotoSource alloc] initWithPhotos:photos];
            EGOPhotoViewController *photoController = [[EGOPhotoViewController alloc] initWithPhotoSource:source];
            
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:photoController];
            [self presentViewController:navController animated:YES completion:nil];
            
            photoController.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"返回", @"") style:UIBarButtonItemStylePlain target:navController action:@selector(dismissModalViewController)] autorelease];
            
            [navController release];
            [photoController release];
            [source release];
            
        } else {
                        
            return YES;
            
        }
        
        return NO;
    }

    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    _backButton.enabled = [webView canGoBack];
    _forwardButton.enabled = [webView canGoForward];
    
    webCanGoBack = [webView canGoBack];
    webCanGoForward = [webView canGoForward];
}

- (void)webViewDidFinishLoad:(UIWebView*)webView 
{
    _backButton.enabled = [webView canGoBack];
    _forwardButton.enabled = [webView canGoForward];
    
    webCanGoBack = [webView canGoBack];
    webCanGoForward = [webView canGoForward];

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [ZSTLogUtil logSysInfo:[NSString stringWithFormat:@"view message failure. error : %@", error.localizedDescription]];
}

#pragma mark ----------ScrollToolBarViewDelegate-----------


-(void)clickedPushOut
{
    [UIView beginAnimations:@"ScrollToolBarViewPushOut" context:nil];
    [UIView setAnimationDuration:0.6];
    
    CGRect scrollToolBarViewFrame = _scrollToolBarView.frame;
    scrollToolBarViewFrame.origin.x -=  290;
    _scrollToolBarView.frame = scrollToolBarViewFrame;
    
    [UIView commitAnimations];
    
    _backButton.enabled = webCanGoBack;
    _forwardButton.enabled = webCanGoForward;
}

-(void)clickedPushIn
{
    [UIView beginAnimations:@"ScrollToolBarViewPushOIn" context:nil];
    [UIView setAnimationDuration:0.6];
    
    CGRect scrollToolBarViewFrame = _scrollToolBarView.frame;
    scrollToolBarViewFrame.origin.x +=  290;
    _scrollToolBarView.frame = scrollToolBarViewFrame;
    
    [UIView commitAnimations];
}

-(void) setFilePathString:(NSString *) getfilePathString
{
    [_filePathString release];
	_filePathString = [getfilePathString retain];
    
    NSURL *url = [NSURL fileURLWithPath: _filePathString isDirectory:NO];
    
	[_webview loadRequest:[NSURLRequest requestWithURL: url]];
}


-(NSString *) filePathString
{
	return _filePathString;
}

- (void)setIconImageViewUrl:(NSString *)iconKey type:(NSString *)type
{
    if ([iconKey isEqualToString:PERSONAL_IMAGE] || [type isEqualToString:PERSONAL_MSGTYPE]) {
        _iconImageView.image = [UIImage imageNamed:@"personal.png"];
    }else {
        [_iconImageView clear];
        _iconImageView.url = [NSURL URLWithString:[NSString stringWithFormat:GETFILEIMAGE_PATH,iconKey]];
        [_iconImageView loadImage];
    } 
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [_activityIndicatorView release];
    [_subject release];
    [_filePathString release];
    [_userPhoneNumber release];
    [_msgID release];
    [_scrollToolBarView stopClock];
    [_scrollToolBarView release];
    [super dealloc];
}

@end
