//
//  my_f3.m
//  Net_Information
//
//  Created by huqinghe on 11-6-5.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "ZSTMessagesViewController.h"

#import "ZSTUtils.h"
#import "TKUIUtil.h"

#import "MessageInfo.h"
#import "MessageDao.h"
#import "ZSTLogUtil.h"

#import "MsgTypeDao.h"
#import "MessageInfo.h"
#import "ZSTDao+pusha.h"

#import "ZSTMessageTableViewCell.h"
#import "ZSTEditView.h"
#import "ZSTCreateNMSDialogViewController.h"
#import "ZSTMessageTypeViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "ZSTMessageDetailViewController.h"
#import "ZSTShell.h"

@implementation ZSTMessagesViewController

@synthesize tableView = _tableView;
@synthesize f3Engine;

+(void)initialize
{
    [ZSTDao createTableIfNotExistForPushaModule];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunching) name:UIApplicationDidFinishLaunchingNotification object:nil];
    if ([ZSTShell isModuleAvailable:-1]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceHttpPoll) name:UIApplicationWillEnterForegroundNotification object:nil];
    }

    if ([ZSTShell isModuleAvailable:22]) {

        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(forceHttpPoll)
                                                     name: NotificationName_ForceHttpPoll
                                                   object: nil];
    }
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(onUnreadNMSChanged:)
                                                 name: NotificationName_UnreadNMSChanged
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(cleanUpMessageAlert)
                                                 name: NotificationName_CleanupMessages
                                               object: nil];
    [self updateIconBadgeNumber];
}

+(void)onUnreadNMSChanged: (NSNotification *) notification
{
    [self performSelectorOnMainThread:@selector(updateIconBadgeNumber) withObject:nil waitUntilDone:NO];
}

+(void)updateIconBadgeNumber
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = [[MessageDao shareMessageDao] getCount:NMSStateNotRead] ;
}

+(void)cleanUpMessageAlert
{
    if ([[MessageDao shareMessageDao] getCount] >= 200) {
        
        UIAlertView * cleanupMessageAlert = [[UIAlertView alloc] initWithTitle:nil
                                                                       message:NSLocalizedString(@"为了节约存储空间，是否要自动清理旧消息?", nil)
                                                                      delegate:self
                                                             cancelButtonTitle:NSLocalizedString(@"清理", nil)
                                                             otherButtonTitles:NSLocalizedString(@"取消",@"取消"),nil];
        cleanupMessageAlert.tag = 1023;
        [cleanupMessageAlert show];
        [cleanupMessageAlert release];
    }
}

#pragma mark －－－－－－－－－－－－－－－－－UIAlertViewDelegate－－－－－－－－－－－－－－－－－－－－－－－－－－－

+(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 1023){
        
        if (0 == buttonIndex) {//清理符合条件的信息(根据时间进行清理，保留最近15天内的消息)
            
            [ZSTF3Engine deleteExpiredMessage];
            postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
            
        }
        
        [ZSTF3Preferences shared].cleanupDate = [NSDate date];
        [[ZSTF3Preferences shared] synchronize];
        
    }
}

+(void)applicationDidFinishLaunching
{
    if ([ZSTShell isModuleAvailable:-1] || [ZSTShell isModuleAvailable:22]) {
        
        if ([ZSTShell isModuleAvailable:-1]) {
            //开始http轮询
//            [ZSTF3Engine startHttpPoll];
        }
        //自动清理检查
        if ([ZSTF3Preferences shared].cleanUp && [[ZSTF3Preferences shared].cleanupDate timeIntervalSinceNow] <= -(15.0*24*60*60)) {
            [self cleanUpMessageAlert];
        }
        //信息类别每启动一次可更新一次
        [ZSTUtils setNeedReloadMessageType];
    }
}

+ (void)forceHttpPoll
{
    if ([ZSTShell isModuleAvailable:-1] || [ZSTShell isModuleAvailable:22]) {
        
        ZSTF3Engine *f3Engine = [[[ZSTF3Engine alloc] init] autorelease];
        [f3Engine enforceHttpPoll];
    }
}

- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
}

#pragma mark - ZSTF3EngineDelegate
- (void)enforceHttpPollResponse
{
    [_headerView egoRefreshScrollViewDataSourceDidFinishedLoading:_tableView];
    _loading = NO;
}

- (void)requestDidFail:(ZSTLegacyResponse *)response
{
    [_headerView egoRefreshScrollViewDataSourceDidFinishedLoading:_tableView];
    _loading = NO;
}

#pragma mark - EGORefreshTableHeaderViewDelegate

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    _loading = YES;
    [f3Engine enforceHttpPoll];
}


- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
    return _loading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
    return [NSDate date];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [_headerView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
    [_headerView egoRefreshScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [_headerView egoRefreshScrollViewDidEndDragging:scrollView];
}
-(UIImage *)compoundWithBackgoundImage:(UIImage *)bgImage frontImage:(UIImage *)frontImage positon:(CGPoint) point
{
    UIGraphicsBeginImageContext(bgImage.size);
    if (CGPointEqualToPoint(point, CGPointZero))point = CGPointMake((bgImage.size.width - frontImage.size.width)/2, (bgImage.size.height - frontImage.size.height)/2);
    [bgImage drawAtPoint:CGPointMake(0,0)];
    [frontImage drawAtPoint:point];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)updateFooterView
{
    if ([_nmsArray count] == 0) {
        _backgroundView.hidden = NO;
        UIImage * bgImage = [UIImage imageNamed:@"bg.png"];
        UIImage * frontImage = ZSTModuleImage(@"module_senda_no_message.png");
        _backgroundView.image = [self compoundWithBackgoundImage:bgImage frontImage:frontImage positon:CGPointZero];//[UIImage imageNamed:@"nullNMS.png"];

    } else {

        _backgroundView.hidden = NO;
        _backgroundView.image = [UIImage imageNamed:@"bg.png"];
    }
}

-(void) updateTableView
{   
    [_nmsArray release];
    _nmsArray = (NSMutableArray *)[[[MessageDao shareMessageDao] getMessages] retain];
    
    [self updateFooterView];
    if (_isEditing) {
        [self setEditing:NO animated:YES];
        [self setEditing:YES animated:YES];
    }
    [_tableView reloadData];
}


//表格的长按事件
//-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
//{
//    CGPoint point = [gestureRecognizer locationInView:_tableView];
//    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:point];
//}

#pragma mark UDP方面要集成进来
#pragma mark TODO: 使用子线程来获取 网信列表
// 这个函数，需要起一个子线程来处理。
// 子线程下载完数据后，将数据插入数据库中
// 然后发送一个通知消息，给主线程，说，有数据更新了。 (使用NSNotificationCenter来做会比较方便)
// 主线程接收到更新消息后，查询数据库，更新Array列表，更新UI。
// 这里查询数据库会牵扯到一个问题: 数据的损耗问题，因为插入数据库的数据我是知道的，现在又要将插入数据库的数据读取出来，然后再Update，造成资源浪费。
// 解决方案: 将增量到的数据，想办法直接给主线程使用。

-(void) createNMS
{  
    [_editView dismiss];
    [self setEditing:NO animated:YES];
    _isLock = NO;
    _editStateMark = NO;
    _isEditing = NO;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[ZSTUtils pathForTempFinderOfOutbox]]) {
        [[NSFileManager defaultManager] removeItemAtPath:[ZSTUtils pathForTempFinderOfOutbox] error:nil];
    }
    
    ZSTCreateNMSDialogViewController *creatNMSDialog = [[ZSTCreateNMSDialogViewController alloc] init];
    creatNMSDialog.shouldSaveToDraft = NO;
    creatNMSDialog.isDraftEdit = NO;
    BOOL isCoverEdit = [NSLocalizedString(@"GP_EditType",@"0") intValue] == 1?YES:NO;
    if (isCoverEdit) {
        ZSTF3Preferences *dataManager = [ZSTF3Preferences shared];
        [creatNMSDialog getReplyUserNumber:dataManager.ECECCID userName:dataManager.ECECCID];
    }
    creatNMSDialog.hidesBottomBarWhenPushed = YES;
    [creatNMSDialog pushInNavigationViewController:self];
    [creatNMSDialog release];
}

-(void)setRightBarButtonItem
{
    UIButton *optionsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    optionsBtn.frame = CGRectMake(280, 7, 30, 30);
    optionsBtn.imageEdgeInsets = UIEdgeInsetsMake(6, 6, 6, 6);
    [optionsBtn setImage:[UIImage imageNamed:@"icon_edit_n.png"] forState:UIControlStateNormal];
    [optionsBtn setImage:[UIImage imageNamed:@"icon_edit_p.png"] forState:UIControlStateHighlighted];
    [optionsBtn addTarget:self action:@selector(edit) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:optionsBtn] autorelease];
    
}

-(void) createNavigationItem
{                                                  
    [self setRightBarButtonItem];

}

- (void)viewDidLoad 
{
    [super viewDidLoad];
        
    self.view.backgroundColor = [UIColor clearColor];
    self.hidesBottomBarWhenPushed = NO;
    self.navigationItem.titleView = [ZSTUtils logoView];
    
     self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    _isLock = NO;
    _editStateMark = NO;
    _isEditing = NO;
    
    UIView *inboxView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.height)];
    inboxView.tag = 99;
    inboxView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:inboxView];
    [inboxView release];
    
    _tableView = [[UITableView alloc] initWithFrame: CGRectMake(0, 0, 320, self.view.height) style: UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorColor = [UIColor clearColor];
    
    _backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nullNMS.png"]] autorelease];
    _backgroundView.frame = CGRectMake(0, 0, 320, self.view.height);
    _backgroundView.hidden = YES;
    _backgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight;

    [inboxView addSubview:_backgroundView];//一定要注意顺序
    [inboxView addSubview: _tableView];
    
    _headerView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-250.0, self.view.frame.size.width, 250.0)];
    _headerView.delegate = self;
    [_headerView refreshLastUpdatedDate];
    [_tableView addSubview:_headerView];
    
    self.f3Engine = [[[ZSTF3Engine alloc] init] autorelease];
    f3Engine.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(onNMSUpdate:) 
                                                 name: NotificationName_NMSListUpdated 
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(onNMSUpdate:) 
                                                 name: NotificationName_GroupUnreadNMSChanged 
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(onNMSUpdate:) 
                                                 name: NotificationName_SwitchNMSListUpdated
                                               object: nil];  
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNMSUpdate:) 
                                                 name:NotificationName_OutBoxNMSListUpdated
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateProgress:) 
                                                 name:NotificationName_Progress
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(messageSendSuccess:) 
                                                 name: NotificationName_MessageSendSucessed
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(messageSendFailed:) 
                                                 name: NotificationName_MessageSendFaild
                                               object: nil];
    
//    [self performSelector:@selector(autoRefresh) withObject:nil afterDelay:0.6];
}

- (void)autoRefresh
{
    [_headerView autoRefreshOnScroll:self.tableView animated:YES];
}

- (void)messageSendFailed:(NSNotification *)notification
{   
    NSString *title = [notification object];  
    [TKUIUtil alertInView:self.view withTitle:title withImage:[UIImage imageNamed:@"icon_warning.png"]];
    
}

- (void)messageSendSuccess:(NSNotification *)notification
{  
    NSString *title = [notification object];  
    [TKUIUtil alertInView:self.view withTitle:title withImage:[UIImage imageNamed:@"icon_smile_face.png"]];
}

-(void)updateProgress:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification object];
    float progress = [[userInfo objectForKey:USERINFO_KEY_PROGRESSAMOUNT] floatValue];
    int ID = [[userInfo objectForKey:USERINFO_KEY_ID] intValue];
    
    ZSTMessageTableViewCell *cell = (ZSTMessageTableViewCell *)[_tableView viewWithTag:ID+100];
    if (cell != nil) {
        
        NSLog(@"updateProgress = %f",progress);
        
        if (cell.progressLabel.frame.size.height != 0.0f) {
            [UIView beginAnimations:@"progress" context:nil];
            [UIView setAnimationDuration:0.4];
            cell.progressLabel.frame = CGRectMake(0, 45*progress, 45, 45*(1-progress));
            NSString *percent = [[[NSString stringWithFormat:@"%f",progress*100] substringToIndex:2] stringByAppendingString:@"%"];
            cell.percentLabel.text = percent;
            cell.percentLabel.hidden = NO;
            cell.percentLabel.font = [UIFont systemFontOfSize:10];
            [UIView commitAnimations];
         if (progress == 1.0f) {
                cell.percentLabel.hidden = YES;
            }
        }  else {
            cell.progressLabel.frame = CGRectMake(0, 45*progress, 45, 45*(1-progress));
        }
    }
}

- (void)onNMSUpdate:(NSNotification *) notification
{
    [self updateTableView];
}

#pragma mark ---------------------  EditView ------------------------------------------

-(void)delSingleMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES];
    [self setEditing:YES animated:YES]; 
    _isEditing = YES;
    _isLock = NO;
    _editStateMark = NO;
    
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"完成", nil) target:self selector:@selector (endDelete)];
    [self updateTableView];
}

-(void)confirmDelAllMSGAction
{
    [ZSTF3Engine deleteAllMessage];
    
    postNotificationOnMainThreadNoWait(NotificationName_UnreadNMSChanged);
    
    [self updateFooterView];
    [self updateTableView];
    [_confirmDelView dismiss];
}

-(void)cancelDelAllMSGAction
{
    [_confirmDelView dismiss];
}

-(void)delAllMSGAction
{
   [_editView dismiss];
   [self setEditing:NO animated:YES]; 
    _isEditing = NO;

    UIImage *image = [UIImage imageNamed:@"confirmDelAll.png"];
    _confirmDelView  = [[ZSTEditView alloc] initWithImage:image];
    _confirmDelView.center = CGPointMake(320/2, (480-20-44)/2);
    
    UIButton *confirmDelMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmDelMSGBtn.frame = CGRectMake(6, 50, 98, 55);
    [confirmDelMSGBtn addTarget:self action:@selector(confirmDelAllMSGAction) forControlEvents:UIControlEventTouchUpInside];
    confirmDelMSGBtn.showsTouchWhenHighlighted = YES;
    confirmDelMSGBtn.backgroundColor = [UIColor clearColor];
    [_confirmDelView addSubview:confirmDelMSGBtn];
    
    UIButton *cancelDelAllMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelDelAllMSGBtn.frame = CGRectMake(106, 50, 98, 55);
    [cancelDelAllMSGBtn addTarget:self action:@selector(cancelDelAllMSGAction) forControlEvents:UIControlEventTouchUpInside];
    cancelDelAllMSGBtn.showsTouchWhenHighlighted = YES;
    cancelDelAllMSGBtn.backgroundColor = [UIColor clearColor];
    [_confirmDelView addSubview:cancelDelAllMSGBtn];
    
    [_confirmDelView show];
    [self updateTableView];
}

-(void)lockMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES]; 
    [self setEditing:YES animated:YES]; 
    _isEditing = YES;
    self.navigationItem.rightBarButtonItem = nil;


    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"完成", nil) target:self selector:@selector (endDelete)];
    
    //编辑锁定状态
    _isLock = YES;
    _editStateMark = NO;
    [self updateTableView];

}

-(void)statusMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES]; 
    [self setEditing:YES animated:YES]; 
    _isEditing = YES;
    self.navigationItem.rightBarButtonItem = nil;

    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"完成", nil) target:self selector:@selector (endDelete)];
    
    //编辑已读未读状态
    _editStateMark = YES;
    _isLock = NO;
    [self updateTableView];
}

-(void)endDelete
{
    [self setEditing:NO animated:YES];
    self.navigationItem.rightBarButtonItem = nil;
    
    [self setRightBarButtonItem];
    
    _isLock = NO;
    _editStateMark = NO;
    _isEditing = NO;
    [_tableView reloadData];
}

-(void)cancelMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES]; 
    [self setEditing:YES animated:YES];    
    _isEditing = YES;
    _isLock = NO;
    _editStateMark = NO;
    
    [self endDelete];
    [self updateTableView];

}

-(void)swithToMessageTypeViewAction
{
    [self cancelMSGAction];

    
    [self.navigationController popViewControllerAnimated:YES];

}

-(void) edit
{  
    UIImage *image = nil;
    
    NSString *GP_Show_NewMessage = NSLocalizedString(@"GP_Show_NewMessage", nil);
    if ([GP_Show_NewMessage isEqualToString:@"0"]) {
        image = [UIImage imageNamed:@"option_edit_right_cancel.png"];
        
    }else{
        image = [UIImage imageNamed:@"option_edit_right_new.png"];
    }
    _editView = [[ZSTEditView alloc] initWithImage:image];
    _editView.frame = CGRectMake(110, 51, image.size.width, image.size.height);
    
    UIButton *swithMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    swithMSGBtn.frame = CGRectMake(6, 20, 65, 55);
    [swithMSGBtn addTarget:self action:@selector(swithToMessageTypeViewAction) forControlEvents:UIControlEventTouchUpInside];
    swithMSGBtn.showsTouchWhenHighlighted = YES;
    swithMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:swithMSGBtn];
    
    
    UIButton *lockMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    lockMSGBtn.frame = CGRectMake(6+65, 20, 65, 55);
    [lockMSGBtn addTarget:self action:@selector(lockMSGAction) forControlEvents:UIControlEventTouchUpInside];
    lockMSGBtn.showsTouchWhenHighlighted = YES;
    lockMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:lockMSGBtn];
    
    UIButton *statusMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    statusMSGBtn.frame = CGRectMake(73+65, 20, 65, 55);
    [statusMSGBtn addTarget:self action:@selector(statusMSGAction) forControlEvents:UIControlEventTouchUpInside];
    statusMSGBtn.showsTouchWhenHighlighted = YES;
    statusMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:statusMSGBtn];
    
    
    UIButton *delAllMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    delAllMSGBtn.frame = CGRectMake(6, 76, 65, 55);
    [delAllMSGBtn addTarget:self action:@selector(delAllMSGAction) forControlEvents:UIControlEventTouchUpInside];
    delAllMSGBtn.showsTouchWhenHighlighted = YES;
    delAllMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:delAllMSGBtn];
    
    UIButton *createNMS_cancel_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    createNMS_cancel_Btn.frame = CGRectMake(73, 76, 65, 55);
    [createNMS_cancel_Btn addTarget:self action:@selector(cancelMSGAction) forControlEvents:UIControlEventTouchUpInside];    
    createNMS_cancel_Btn.showsTouchWhenHighlighted = YES;
    createNMS_cancel_Btn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:createNMS_cancel_Btn];
    
    
    if ([GP_Show_NewMessage isEqualToString:@"1"]) {
        
        UIButton *createMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        createMSGBtn.frame = CGRectMake(73+65, 76, 65, 55);
        [createMSGBtn addTarget:self action:@selector(createNMS) forControlEvents:UIControlEventTouchUpInside];
        createMSGBtn.showsTouchWhenHighlighted = YES;
        createMSGBtn.backgroundColor = [UIColor clearColor];
        [_editView addSubview:createMSGBtn];
        

    }
    
    [_editView show];
    
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[_tableView setEditing:editing animated:animated];

}

#pragma mark-----------------------UITableViewDataSource---------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CustomTableViewCell";
    
    ZSTMessageTableViewCell *cell = (ZSTMessageTableViewCell *)[tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    MessageInfo *nmsInfo = [_nmsArray objectAtIndex: indexPath.row];
    if (!cell)
    {
        cell = [[[ZSTMessageTableViewCell alloc] initWithStyle: UITableViewCellStyleDefault 
                                           reuseIdentifier: cellIdentifier] autorelease];
        cell.backgroundView = [[[UIView alloc] init] autorelease];
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
    }
    
    NSString *GP_Show_NewMessage = NSLocalizedString(@"GP_Show_NewMessage", nil);
    if ([GP_Show_NewMessage isEqualToString:@"0"]) {
        [cell reLayout];
    }
    cell.tag = nmsInfo.ID + 100;
    cell.isLock = _isLock;
    cell.editStateMark = _editStateMark;
    cell.inEditing = _isEditing;
   
    [cell setNMSInfo:nmsInfo isLock:_isLock editStateMark:_editStateMark];
    if (nmsInfo.state == NMSStateSending) {
     
        cell.progressLabel.hidden = NO;
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_nmsArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTMessageTableViewCell *cell = (ZSTMessageTableViewCell *)[tableView cellForRowAtIndexPath: indexPath];
    MessageInfo *nmsInfo = [_nmsArray objectAtIndex: indexPath.row];
    [self.typeDelegate refresh];
    if (cell.selectionStyle == UITableViewCellSelectionStyleNone || nmsInfo.state == NMSStateReceiving || nmsInfo.state == NMSStateSending)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }else if (nmsInfo.state == NMSStateReceivedFailed)
    {
        cell.labelIsRead.text = NSLocalizedString(@"[正在下载]" , nil);
        [self.f3Engine reReceiveMessage:nmsInfo.pushId];
        return;
    }else if (nmsInfo.state == NMSStateSentFailed)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        cell.labelIsRead.text = NSLocalizedString(@"[发送中]" , nil);
        cell.labelIsRead.textColor = [UIColor grayColor];
        [[MessageDao shareMessageDao] setOutMessage:nmsInfo.ID MSGID:nmsInfo.MSGID withState:NMSStateSending];
        [ZSTF3Engine sendF3:nmsInfo alertWhenFinish:YES];
        return;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BOOL shouldSendNotification = (nmsInfo.state == NMSStateNotRead);
    if (shouldSendNotification) {
        
        nmsInfo.state = NMSStateRead;
        if (nmsInfo.report) {
            [ZSTF3Engine reportState:nmsInfo];
        }
        nmsInfo.report = NO;
        [[MessageDao shareMessageDao] updateMessageById:nmsInfo];
        postNotificationOnMainThreadNoWait(NotificationName_UnreadNMSChanged);
        postNotificationOnMainThreadNoWait(NotificationName_InboxUnreadNMSChanged);
    }

    if (nmsInfo.state == NMSStateNotRead || nmsInfo.state == NMSStateRead) {
        
        NSString *inboxPath = [ZSTUtils pathForInbox];
        NSString * str = [inboxPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",nmsInfo.pushId]];
        
        NSString * displayString = [str stringByAppendingPathComponent:@"Index.htm"];
        
        ZSTMessageDetailViewController * messageViewController = [[ZSTMessageDetailViewController alloc] init];
        messageViewController.hidesBottomBarWhenPushed = YES;
        //    messageViewController.canEdit = YES;
        messageViewController.subject = nmsInfo.subject;
        [messageViewController view];
        messageViewController.userPhoneNumber = nmsInfo.userID;
        //    messageViewController.userName = nmsInfo.userName;
        [messageViewController  setFilePathString:displayString];
        messageViewController.msgID = nmsInfo.MSGID;
        messageViewController.pushId = nmsInfo.pushId;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy'-'MM'-'dd HH':'mm':'ss"];
        [formatter setDateFormat:NSLocalizedString(@"MM-dd HH:mm", nil)];
        NSString *dateString = [formatter stringFromDate:nmsInfo.time];
        
        messageViewController.sendDate.text = dateString;
        [formatter release];
        
        self.navigationController.navigationBarHidden = NO;
        messageViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:messageViewController animated:YES];
        
        [cell setNMSInfo: nmsInfo isLock:_isLock editStateMark:_editStateMark];
        
        messageViewController.inboxName.text = cell.labelName.text;
         MsgTypeInfo *msgInfo = [[MsgTypeDao shareMsgTypeDao] getMsgTypeInfoByTypeID:nmsInfo.typeId];
        [messageViewController setIconImageViewUrl:msgInfo.iconKey type:nmsInfo.typeId];
        [messageViewController release];
        
    }else if (nmsInfo.state == NMSStateSended || nmsInfo.state == NMSStateDraft 
              || nmsInfo.state == NMSStateHaveArrived || nmsInfo.state == NMSStateHaveRead)
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:[ZSTUtils pathForTempFinderOfOutbox]]) {
            [[NSFileManager defaultManager] removeItemAtPath:[ZSTUtils pathForTempFinderOfOutbox] error:nil];
        }
        ZSTCreateNMSDialogViewController * replyCreateNMSDialogViewController = [[ZSTCreateNMSDialogViewController alloc] init];
        replyCreateNMSDialogViewController.shouldSaveToDraft = NO;
        
        if (nmsInfo.state == NMSStateDraft) {
        replyCreateNMSDialogViewController.isDraftEdit = YES;
        }else{
            replyCreateNMSDialogViewController.isDraftEdit = NO;
        }
        replyCreateNMSDialogViewController.hidesBottomBarWhenPushed = YES;
        NSArray *phoneNumberArray = [nmsInfo.userID  componentsSeparatedByString:@","];
        [replyCreateNMSDialogViewController getOutContactNumber:phoneNumberArray];
        replyCreateNMSDialogViewController.textview.text = nmsInfo.content;
        replyCreateNMSDialogViewController.nmsID = [NSString stringWithFormat:@"%ld",(long)nmsInfo.ID];
        [replyCreateNMSDialogViewController pushInNavigationViewController:self];
        [replyCreateNMSDialogViewController  release];
        
    }

}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ZSTMessageTableViewCell *cell = (ZSTMessageTableViewCell *)[tableView cellForRowAtIndexPath: indexPath];
    MessageInfo *nmsInfo = [_nmsArray objectAtIndex: indexPath.row];


	if (editingStyle == UITableViewCellEditingStyleDelete && !_editStateMark && !_isLock) 
	{

        [ZSTF3Engine deleteMessage: nmsInfo];
		
		[_nmsArray removeObjectAtIndex:indexPath.row];
        
        [tableView setEditing:NO animated:YES];
		[tableView deleteRowsAtIndexPaths: [NSArray arrayWithObject: indexPath] withRowAnimation: UITableViewRowAnimationFade];
        
        [self updateTableView];
        
        postNotificationOnMainThreadNoWait(NotificationName_UnreadNMSChanged);
        
        return;
	}
	else if (editingStyle == UITableViewCellEditingStyleDelete && _editStateMark)
	{
        
        if (nmsInfo.state == NMSStateRead) {
            nmsInfo.state = NMSStateNotRead;
            
        }else if(nmsInfo.state == NMSStateNotRead) 
        {
            nmsInfo.state = NMSStateRead;
            
        }
        [[MessageDao shareMessageDao] updateMessageById:nmsInfo];
            
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_UnreadNMSChanged object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_InboxUnreadNMSChanged object:nil];
        
    
        [cell setNMSInfo: nmsInfo isLock:_isLock editStateMark:_editStateMark];

	}else if (editingStyle == UITableViewCellEditingStyleDelete && _isLock)
    {
                
        if (nmsInfo.isLocked == NO) {
            nmsInfo.isLocked = YES;
        }else if( nmsInfo.isLocked == YES) 
        {
            nmsInfo.isLocked = NO;
        }
        [[MessageDao shareMessageDao] updateMessageById:nmsInfo];
   
        [cell setNMSInfo: nmsInfo isLock:_isLock editStateMark:_editStateMark];

        postNotificationOnMainThreadNoWait(NotificationName_SwitchNMSListUpdated);
    }
    
    [UIView beginAnimations:@"endEditingCell" context:nil];
    [UIView setAnimationDuration:0.5];
    
    CGRect labelNameFrame = cell.labelName.frame;
    labelNameFrame.size.width += 85;
    cell.labelName.frame = labelNameFrame;
    
    CGRect labelDateFrame = cell.labelDate.frame;
       labelDateFrame.origin.x += 85;
    cell.labelDate.frame = labelDateFrame;
    
    CGRect labelTitleFrame = cell.labelTitle.frame;
    labelTitleFrame.size.width += 85;
    cell.labelTitle.frame = labelTitleFrame;
    
    [UIView commitAnimations];
    
    [tableView reloadData];

}

#pragma mark-----UITableViewDelegate-----------------

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageInfo *nmsInfo = [_nmsArray objectAtIndex: indexPath.row];

    if (!_isLock && !_editStateMark && nmsInfo.isLocked == YES) 
    {
        return UITableViewCellEditingStyleNone;
    }else if (!_isLock && _editStateMark && 
             (nmsInfo.state == NMSStateSended 
                  || nmsInfo.state == NMSStateSending 
                  || nmsInfo.state == NMSStateSentFailed 
                  || nmsInfo.state == NMSStateDraft
                  || nmsInfo.state == NMSStateHaveArrived
                  || nmsInfo.state == NMSStateHaveRead
                  || nmsInfo.state == NMSStateReceiving))
    {
        return UITableViewCellEditingStyleNone;
    }else if (!_isLock && !_editStateMark &&
              (nmsInfo.state == NMSStateSending
               || nmsInfo.state == NMSStateReceiving))
    {
        return UITableViewCellEditingStyleNone;
    }
    
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MessageInfo *nmsInfo = [_nmsArray objectAtIndex: indexPath.row];

    if (_editStateMark && nmsInfo.state == NMSStateNotRead) {
        
        return NSLocalizedString(@"设为已读" , nil);
    }else if(_editStateMark && nmsInfo.state == NMSStateRead)
    {
        return NSLocalizedString(@"设为未读" , nil);
    }else if(_isLock && nmsInfo.isLocked == NO)
    {
        return NSLocalizedString(@"锁定信息" , nil);
    }else if(_isLock && nmsInfo.isLocked == YES)
    {
        return NSLocalizedString(@"解除锁定" , nil);
    }else if(_editStateMark && (nmsInfo.state == NMSStateReceivedFailed || nmsInfo.state == NMSStateReceiving || nmsInfo.state == 0))
    {
        return NSLocalizedString(@"无法更改" , nil);
    }
      return NSLocalizedString(@"删除" , nil);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self createNavigationItem];
    [self setEditing:NO animated:YES];
    [self updateTableView];
    self.navigationItem.hidesBackButton = YES;
}

#pragma mark -

- (void)dealloc 
{
    self.f3Engine = nil;
    TKRELEASE(_headerView);
    [_tableView release];
    [_nmsArray release];
    [_editView release];
    [_confirmDelView release];
    [super dealloc];
}

@end
