//
//  ZSTGlobal+News.h
//  News
//
//  Created by luobin on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

// IP Push参数
#define IP_PUSH_SERVER_IP @"59.151.28.115"
#define IP_PUSH_SERVER_PORT 8080


#define PushBBaseURL @"http://mod.pmit.cn/pushb"//最新接口

#define PushABaseURL @"http://mod.pmit.cn/pusha"//最新接口


#define SendABaseURL @"http://mod.pmit.cn/senda"//最新接口


#define GETNMSLIST_URL_PATH PushBBaseURL @"/SearchNMSPushs"

#define SENDNMS_URL_PATH SendABaseURL @"/SendF3"

#define RECEIVE_NMS_URL_PATH PushBBaseURL @"/ReceiveNMS"

#define RECEIVE_NMS_CONTENT_URL_PATH PushBBaseURL @"/ReceiveNMSContent"

#define GETMSGTYPEINFO_URL_PATH PushBBaseURL @"/GetMsgTypeInfo"

#define MSGTYPESET_URL_PATH PushBBaseURL @"/MsgTypeSet"

#define SYNCMSGTYPE_URL_PATH PushBBaseURL@"/SyncMsgType"

#define DOWNLOADFILE_URL_PATH PushABaseURL @"/DownloadFile"

#define SUBMITSYSTEMLOG_URL_PATH PushABaseURL @"/SubmitSystemLog"

#define UPDATECLIENTTOKEN_URL_PATH PushABaseURL@"/UpdateClientToken"

#define SENDREPORT_URL_PATH PushABaseURL @"/sendreport"

#define GETUPLOADFILEINFO_PATH PushABaseURL @"/GetUploadFileInfo"

#define UPLOADFILEBREAKPOINT_PATH PushABaseURL @"/UploadFileBreakPoint"

#define GETFILEINFO_PATH PushBBaseURL @"/GetFile"

#define GETFILEIMAGE_PATH PushABaseURL @"/file?fileid=%@"

#define NMS_UPDATE_LIST_INTERVAL 30

#define REGISTER_STATUS_NO 0           //未绑定
#define REGISTER_STATUS_TEMP 1         //使用虚拟手机号
#define REGISTER_STATUS_OK 9           //使用真实手机号，绑定成功

#define LastPollingTimeKey @"TheLastPollingTime"


