//
//  ZSTWeiBoInfo.h
//  F3
//
//  Created by xuhuijun on 12-6-11.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTWeiBoInfo : NSObject

@property(nonatomic, copy) NSString *avatarString;
@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSDate *time;
@property(nonatomic, copy) NSString *content;
@property(nonatomic, copy) NSString *thumbnail_pic;
@property(nonatomic, copy) NSString *bmiddle_pic;
@property(nonatomic, copy) NSString *original_pic;
@property(nonatomic, copy) NSString *Platform;
@property(nonatomic, copy) NSString *forwardCount;
@property(nonatomic, copy) NSString *commentCount;
@property(nonatomic, retain) ZSTWeiBoInfo *attachmentInfo;
@property(nonatomic, copy) NSString *MID;
@property(nonatomic, assign) long long ID;
@property(nonatomic, assign)  BOOL bisforward;
@property(nonatomic, copy) NSString *boriginalcontent;
@property(nonatomic, copy) NSString *type;

@end
