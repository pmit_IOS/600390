/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "SDFileCache.h"
//#import "SDWebImageDecoder.h"
#import <CommonCrypto/CommonDigest.h>
//#import "SDWebImageDecoder.h"

static NSInteger cacheMaxCacheAge = 60*60*24*7; // 1 week

static SDFileCache *instance;

@implementation SDFileCache

#pragma mark NSObject

- (id)init
{
    if ((self = [super init]))
    {
        // Init the memory cache
        memCache = [[NSMutableDictionary alloc] init];
        
        // Init the disk cache
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        diskCachePath = SDWIReturnRetained([[paths objectAtIndex:0] stringByAppendingPathComponent:@"ImageCache"]);
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:diskCachePath])
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:diskCachePath
                                      withIntermediateDirectories:YES
                                                       attributes:nil
                                                            error:NULL];
        }
        
        // Init the operation queue
        cacheInQueue = [[NSOperationQueue alloc] init];
        cacheInQueue.maxConcurrentOperationCount = 1;
        cacheOutQueue = [[NSOperationQueue alloc] init];
        cacheOutQueue.maxConcurrentOperationCount = 1;
        
#if TARGET_OS_IPHONE
        // Subscribe to app events
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(clearMemory)
                                                     name:UIApplicationDidReceiveMemoryWarningNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cleanDisk)
                                                     name:UIApplicationWillTerminateNotification
                                                   object:nil];
        
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_4_0
        UIDevice *device = [UIDevice currentDevice];
        if ([device respondsToSelector:@selector(isMultitaskingSupported)] && device.multitaskingSupported)
        {
            // When in background, clean memory in order to have less chance to be killed
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(clearMemory)
                                                         name:UIApplicationDidEnterBackgroundNotification
                                                       object:nil];
        }
#endif
#endif
    }
    
    return self;
}

- (void)dealloc
{
    SDWISafeRelease(memCache);
    SDWISafeRelease(diskCachePath);
    SDWISafeRelease(cacheInQueue);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    SDWISuperDealoc;
}

#pragma mark SDFileCache (class methods)

+ (SDFileCache *)sharedFileCache
{
    if (instance == nil)
    {
        instance = [[SDFileCache alloc] init];
    }
    
    return instance;
}

- (NSString *)cachePathForKey:(NSString *)key
{
    const char *str = [key UTF8String];
    unsigned char r[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (CC_LONG)strlen(str), r);
    NSString *filename = [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                          r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15]];
    
    return [diskCachePath stringByAppendingPathComponent:filename];
}

#pragma mark SDFileCache (private)

- (void)storeKeyWithDataToDisk:(NSArray *)key_Data_Path
{
    // Can't use defaultManager another thread
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    NSString *key = [key_Data_Path objectAtIndex:0];
    NSData *data = [key_Data_Path count] > 1 ? [key_Data_Path objectAtIndex:1] : nil;
    NSString *filePath = [key_Data_Path count] > 2 ? [key_Data_Path objectAtIndex:2] : nil;
    
    if (data)
    {
        [fileManager createFileAtPath:filePath contents:data attributes:nil];
    }
    else
    {
        // If no data representation given, convert the UIImage in JPEG and store it
        // This trick is more CPU/memory intensive and doesn't preserve alpha channel
        NSData *FileData = SDWIReturnRetained([self fileFromKey:key fromDisk:YES withFilePath:filePath]); // be thread safe with no lock
        if (FileData)
        {
#if TARGET_OS_IPHONE
            [fileManager createFileAtPath:filePath contents:FileData attributes:nil];
            
#else
            //            NSArray*  representations  = [[UIImage imageWithData:FileData] representations];
            //            NSData* jpegData = [NSBitmapImageRep representationOfImageRepsInArray: representations usingType: NSJPEGFileType properties:nil];
            [fileManager createFileAtPath:filePath contents:FileData attributes:nil];
#endif
            //            SDWIRelease([UIImage imageWithData:FileData]);
        }
    }
    
    SDWIRelease(fileManager);
}

- (void)notifyDelegate:(NSDictionary *)arguments
{
    NSString *key = [arguments objectForKey:@"key"];
    NSString *filePath = [arguments objectForKey:@"filePath"];
    id <SDFileCacheDelegate> delegate = [arguments objectForKey:@"delegate"];
    NSDictionary *info = [arguments objectForKey:@"userInfo"];
    
    NSMutableDictionary *infos = [NSMutableDictionary dictionaryWithDictionary:info];
    [infos setObject:filePath forKey:@"filePath"];
    info = [NSDictionary dictionaryWithDictionary:infos];
    
    NSData *fileData = [arguments objectForKey:@"fileData"];
    
    if (fileData)
    {
        [memCache setObject:fileData forKey:key];
        
        if ([delegate respondsToSelector:@selector(fileCache:didFindFileData:forKey:userInfo:)])
        {
            [delegate fileCache:self didFindFileData:fileData forKey:key userInfo:info];
        }
    }
    else
    {
        if ([delegate respondsToSelector:@selector(fileCache:didNotFindFileDataForKey:userInfo:)])
        {
            [delegate fileCache:self didNotFindFileDataForKey:key userInfo:info];
        }
    }
}

- (void)queryDiskCacheOperation:(NSDictionary *)arguments {
    
    //    NSString *key = [arguments objectForKey:@"key"];
    NSString *filePath = [arguments objectForKey:@"filePath"];
    NSMutableDictionary *mutableArguments = SDWIReturnAutoreleased([arguments mutableCopy]);
    
    NSData *fileData =  [NSData dataWithContentsOfFile:filePath];
    
    if (fileData)
    {
        [mutableArguments setObject:fileData forKey:@"fileData"];
        [mutableArguments setObject:filePath forKey:@"filePath"];
    }
    
    [self performSelectorOnMainThread:@selector(notifyDelegate:) withObject:mutableArguments waitUntilDone:NO];
}

#pragma mark FileCache

- (void)storeFile:(NSData *)fileData forKey:(NSString *)key toDisk:(BOOL)toDisk withFilePath:(NSString *)filePath
{
    if (!fileData || !key)
    {
        return;
    }
    
    [memCache setObject:fileData forKey:key];
    
    if (toDisk)
    {
        NSArray *keyWithData;
        if (fileData)
        {
            keyWithData = [NSArray arrayWithObjects:key, fileData, filePath, nil];
        }
        else
        {
            keyWithData = [NSArray arrayWithObjects:key,filePath, nil];
        }
        
        NSInvocationOperation *operation = SDWIReturnAutoreleased([[NSInvocationOperation alloc] initWithTarget:self
                                                                                                       selector:@selector(storeKeyWithDataToDisk:)
                                                                                                         object:keyWithData]);
        [cacheInQueue addOperation:operation];
    }
}

- (void)storeFile:(NSData *)fileData forKey:(NSString *)key withFilePath:(NSString *)filePath
{
    [self storeFile:fileData forKey:key toDisk:YES withFilePath:filePath];
}




- (NSData *)fileFromKey:(NSString *)key withFilePath:(NSString *)filePath
{
    return [self fileFromKey:key fromDisk:YES withFilePath:filePath];
}

- (NSData *)fileFromKey:(NSString *)key fromDisk:(BOOL)fromDisk withFilePath:(NSString *)filePath

{
    if (key == nil)
    {
        return nil;
    }
    
    NSData *fileData = [memCache objectForKey:key];
    
    if (!fileData && fromDisk)
    {
        fileData = [NSData dataWithContentsOfFile:filePath];
        if (fileData)
        {
            [memCache setObject:fileData forKey:key];
        }
    }
    
    return fileData;
}

- (void)queryDiskCacheForKey:(NSString *)key delegate:(id <SDFileCacheDelegate>)delegate userInfo:(NSDictionary *)info
{
    if (!delegate)
    {
        return;
    }
    
    if (!key)
    {
        if ([delegate respondsToSelector:@selector(fileCache:didNotFindFileDataForKey:userInfo:)])
        {
            [delegate fileCache:self didNotFindFileDataForKey:key userInfo:info];
        }
        return;
    }
    
    // First check the in-memory cache...
    NSData *fileData = [memCache objectForKey:key];
    if (fileData)
    {
        // ...notify delegate immediately, no need to go async
        if ([delegate respondsToSelector:@selector(fileCache:didFindFileData:forKey:userInfo:)])
        {
            [delegate fileCache:self didFindFileData:fileData forKey:key userInfo:info];
        }
        return;
    }
    
    NSMutableDictionary *arguments = [NSMutableDictionary dictionaryWithCapacity:4];
    [arguments setObject:key forKey:@"key"];
    [arguments setObject:delegate forKey:@"delegate"];
    if (info)
    {
        [arguments setObject:info forKey:@"userInfo"];
        NSString *filePath = [info objectForKey:@"filePath"];
        [arguments setObject:filePath forKey:@"filePath"];
    }
    NSInvocationOperation *operation = SDWIReturnAutoreleased([[NSInvocationOperation alloc] initWithTarget:self
                                                                                                   selector:@selector(queryDiskCacheOperation:)
                                                                                                     object:arguments]);
    [cacheOutQueue addOperation:operation];
}

- (void)removeFileForKey:(NSString *)key withFilePath:(NSString *)filePath

{
    if (key == nil)
    {
        return;
    }
    
    [memCache removeObjectForKey:key];
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
}

- (void)clearMemory
{
    [cacheInQueue cancelAllOperations]; // won't be able to complete
    [memCache removeAllObjects];
}

- (void)clearDisk
{
    [cacheInQueue cancelAllOperations];
    [[NSFileManager defaultManager] removeItemAtPath:diskCachePath error:nil];
    [[NSFileManager defaultManager] createDirectoryAtPath:diskCachePath
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:NULL];
}

- (void)cleanDisk
{
    NSDate *expirationDate = [NSDate dateWithTimeIntervalSinceNow:-cacheMaxCacheAge];
    NSDirectoryEnumerator *fileEnumerator = [[NSFileManager defaultManager] enumeratorAtPath:diskCachePath];
    for (NSString *fileName in fileEnumerator)
    {
        NSString *filePath = [diskCachePath stringByAppendingPathComponent:fileName];
        NSDictionary *attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
        if ([[[attrs fileModificationDate] laterDate:expirationDate] isEqualToDate:expirationDate])
        {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
    }
}

-(int)getSize
{
    int size = 0;
    NSDirectoryEnumerator *fileEnumerator = [[NSFileManager defaultManager] enumeratorAtPath:diskCachePath];
    for (NSString *fileName in fileEnumerator)
    {
        NSString *filePath = [diskCachePath stringByAppendingPathComponent:fileName];
        NSDictionary *attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
        size += [attrs fileSize];
    }
    return size;
}

@end
