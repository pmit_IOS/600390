/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

@class SDWebFileManager;

@protocol SDWebFileManagerDelegate <NSObject>

@optional

- (void)webFileManager:(SDWebFileManager *)fileManager didFinishWithFileData:(NSData *)data;
- (void)webFileManager:(SDWebFileManager *)fileManager didFinishWithFileData:(NSData *)data forURL:(NSURL *)url;
- (void)webFileManager:(SDWebFileManager *)fileManager didFailWithError:(NSError *)error;
- (void)webFileManager:(SDWebFileManager *)fileManager didFailWithError:(NSError *)error forURL:(NSURL *)url;

@end
