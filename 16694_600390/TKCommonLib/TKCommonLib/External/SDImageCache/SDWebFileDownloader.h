/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import <Foundation/Foundation.h>
#import "SDWebFileDownloaderDelegate.h"
#import "SDWebImageCompat.h"

extern NSString *const SDWebFileDownloadStartNotification;
extern NSString *const SDWebFileDownloadStopNotification;

@interface SDWebFileDownloader : NSObject
{
@private
    NSURL *url;
    SDWIWeak id<SDWebFileDownloaderDelegate> delegate;
    NSURLConnection *connection;
    NSMutableData *fileData;
    id userInfo;
    BOOL lowPriority;
    
}

@property (nonatomic, retain) NSURL *url;
@property (nonatomic, assign) id<SDWebFileDownloaderDelegate> delegate;
@property (nonatomic, retain) NSMutableData *fileData;
@property (nonatomic, retain) id userInfo;
@property (nonatomic, readwrite) BOOL lowPriority;

+ (id)downloaderWithURL:(NSURL *)url delegate:(id<SDWebFileDownloaderDelegate>)delegate userInfo:(id)userInfo lowPriority:(BOOL)lowPriority;
+ (id)downloaderWithURL:(NSURL *)url delegate:(id<SDWebFileDownloaderDelegate>)delegate userInfo:(id)userInfo;
+ (id)downloaderWithURL:(NSURL *)url delegate:(id<SDWebFileDownloaderDelegate>)delegate;
- (void)start;
- (void)cancel;

// This method is now no-op and is deprecated
+ (void)setMaxConcurrentDownloads:(NSUInteger)max __attribute__((deprecated));

@end
