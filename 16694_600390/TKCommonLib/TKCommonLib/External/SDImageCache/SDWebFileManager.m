/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "SDWebFileManager.h"
#import "SDFileCache.h"
#import "SDWebFileDownloader.h"
#import <objc/message.h>

#if NS_BLOCKS_AVAILABLE
typedef void(^SuccessBlock)(NSData *data);
typedef void(^FailureBlock)(NSError *error);

@interface SDWebFileManager ()
@property (nonatomic, copy) SuccessBlock successBlock;
@property (nonatomic, copy) FailureBlock failureBlock;
@end
#endif

static SDWebFileManager *instance;

@implementation SDWebFileManager

//@synthesize filePathString;

#if NS_BLOCKS_AVAILABLE
@synthesize successBlock;
@synthesize failureBlock;
#endif

- (id)init
{
    if ((self = [super init]))
    {
        downloadDelegates = [[NSMutableArray alloc] init];
        downloaders = [[NSMutableArray alloc] init];
        cacheDelegates = [[NSMutableArray alloc] init];
        cacheURLs = [[NSMutableArray alloc] init];
        downloaderForURL = [[NSMutableDictionary alloc] init];
        failedURLs = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    SDWISafeRelease(downloadDelegates);
    SDWISafeRelease(downloaders);
    SDWISafeRelease(cacheDelegates);
    SDWISafeRelease(cacheURLs);
    SDWISafeRelease(downloaderForURL);
    SDWISafeRelease(failedURLs);
    SDWISuperDealoc;
}


+ (id)sharedManager
{
    if (instance == nil)
    {
        instance = [[SDWebFileManager alloc] init];
    }
    
    return instance;
}

/**
 * @deprecated
 */
- (NSData *)fileDataWithURL:(NSURL *)url
{
    SDWebFileDownloader *downloader = [downloaderForURL objectForKey:url];
    
    return [[SDFileCache sharedFileCache] fileFromKey:[url absoluteString] withFilePath:[downloader.userInfo objectForKey:@"filePath"]];
}

/**
 * @deprecated
 */
- (void)downloadWithURL:(NSURL *)url delegate:(id<SDWebFileManagerDelegate>)delegate withFilePath:(NSString *)filePath retryFailed:(BOOL)retryFailed 
{
    [self downloadWithURL:url delegate:delegate options:(retryFailed ? SDWebImageRetryFailed : 0) withFilePath:filePath];
}

/**
 * @deprecated
 */
- (void)downloadWithURL:(NSURL *)url delegate:(id<SDWebFileManagerDelegate>)delegate retryFailed:(BOOL)retryFailed withFilePath:(NSString *)filePath lowPriority:(BOOL)lowPriority 
{
    SDWebImageOptions options = 0;
    if (retryFailed) options |= SDWebImageRetryFailed;
    if (lowPriority) options |= SDWebImageLowPriority;
    [self downloadWithURL:url delegate:delegate options:options withFilePath:filePath];
}

- (void)downloadWithURL:(NSURL *)url delegate:(id<SDWebFileManagerDelegate>)delegate withFilePath:(NSString *)filePath
{
    [self downloadWithURL:url delegate:delegate options:0 withFilePath:filePath];
    
}

- (void)downloadWithURL:(NSURL *)url delegate:(id<SDWebFileManagerDelegate>)delegate options:(SDWebImageOptions)options withFilePath:(NSString *)filePath
{
    // Very common mistake is to send the URL using NSString object instead of NSURL. For some strange reason, XCode won't
    // throw any warning for this type mismatch. Here we failsafe this error by allowing URLs to be passed as NSString.
    if ([url isKindOfClass:NSString.class])
    {
        url = [NSURL URLWithString:(NSString *)url];
    }
    
    if (!url || !delegate || (!(options & SDWebImageRetryFailed) && [failedURLs containsObject:url]))
    {
        return;
    }
    
    // Check the on-disk cache async so we don't block the main thread
    [cacheDelegates addObject:delegate];
    [cacheURLs addObject:url];
    
    //    [filePathString release];
    //    filePathString = nil;
    //    self.filePathString = filePath;
    
    NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:delegate, @"delegate", url, @"url", [NSNumber numberWithInt:options], @"options",filePath,@"filePath", nil];
    [[SDFileCache sharedFileCache] queryDiskCacheForKey:[url absoluteString] delegate:self userInfo:info];
}

#if NS_BLOCKS_AVAILABLE
- (void)downloadWithURL:(NSURL *)url delegate:(id)delegate options:(SDWebImageOptions)options success:(void (^)(NSData *data))success failure:(void (^)(NSError *error))failure withFilePath:(NSString *)filePath
{
    self.successBlock = success;
    self.failureBlock = failure;
    [self downloadWithURL:url delegate:delegate options:options withFilePath:filePath];
}
#endif

- (void)cancelForDelegate:(id<SDWebFileManagerDelegate>)delegate
{
    NSUInteger idx;
    while ((idx = [cacheDelegates indexOfObjectIdenticalTo:delegate]) != NSNotFound)
    {
        [cacheDelegates removeObjectAtIndex:idx];
        [cacheURLs removeObjectAtIndex:idx];
    }
    
    while ((idx = [downloadDelegates indexOfObjectIdenticalTo:delegate]) != NSNotFound)
    {
        SDWebFileDownloader *downloader = SDWIReturnRetained([downloaders objectAtIndex:idx]);
        
        [downloadDelegates removeObjectAtIndex:idx];
        [downloaders removeObjectAtIndex:idx];
        
        if (![downloaders containsObject:downloader])
        {
            // No more delegate are waiting for this download, cancel it
            [downloader cancel];
            [downloaderForURL removeObjectForKey:downloader.url];
        }
        
        SDWIRelease(downloader);
    }
}

#pragma mark SDFileCacheDelegate

- (NSUInteger)indexOfDelegate:(id<SDWebFileManagerDelegate>)delegate waitingForURL:(NSURL *)url
{
    // Do a linear search, simple (even if inefficient)
    NSUInteger idx;
    for (idx = 0; idx < [cacheDelegates count]; idx++)
    {
        if ([cacheDelegates objectAtIndex:idx] == delegate && [[cacheURLs objectAtIndex:idx] isEqual:url])
        {
            return idx;
        }
    }
    return NSNotFound;
}

- (void)fileCache:(SDFileCache *)fileCache didFindFileData:(NSData *)data forKey:(NSString *)key userInfo:(NSDictionary *)info
{
    NSURL *url = [info objectForKey:@"url"];
    id<SDWebFileManagerDelegate> delegate = [info objectForKey:@"delegate"];
    
    NSUInteger idx = [self indexOfDelegate:delegate waitingForURL:url];
    if (idx == NSNotFound)
    {
        // Request has since been canceled
        return;
    }
    
    if ([delegate respondsToSelector:@selector(webFileManager:didFinishWithFileData:)])
    {
        [delegate performSelector:@selector(webFileManager:didFinishWithFileData:) withObject:self withObject:data];
    }
    if ([delegate respondsToSelector:@selector(webFileManager:didFinishWithFileData:forURL:)])
    {
        objc_msgSend(delegate, @selector(webFileManager:didFinishWithFileData:forURL:), self, data, url);
    }
#if NS_BLOCKS_AVAILABLE
    if (self.successBlock)
    {
        self.successBlock(data);
    }
#endif
    
    [cacheDelegates removeObjectAtIndex:idx];
    [cacheURLs removeObjectAtIndex:idx];
}

- (void)fileCache:(SDFileCache *)fileCache didNotFindFileDataForKey:(NSString *)key userInfo:(NSDictionary *)info
{
    NSURL *url = [info objectForKey:@"url"];
    id<SDWebFileManagerDelegate> delegate = [info objectForKey:@"delegate"];
    SDWebImageOptions options = [[info objectForKey:@"options"] intValue];
    
    NSUInteger idx = [self indexOfDelegate:delegate waitingForURL:url];
    if (idx == NSNotFound)
    {
        // Request has since been canceled
        return;
    }
    
    [cacheDelegates removeObjectAtIndex:idx];
    [cacheURLs removeObjectAtIndex:idx];
    
    // Share the same downloader for identical URLs so we don't download the same URL several times
    SDWebFileDownloader *downloader = [downloaderForURL objectForKey:url];
    
    if (!downloader)
    {
        downloader = [SDWebFileDownloader downloaderWithURL:url delegate:self userInfo:info lowPriority:(options & SDWebImageLowPriority)];
        [downloaderForURL setObject:downloader forKey:url];
    }
    else
    {
        // Reuse shared downloader
        downloader.userInfo = info;
        downloader.lowPriority = (options & SDWebImageLowPriority);
    }
    
    [downloadDelegates addObject:delegate];
    [downloaders addObject:downloader];
}

#pragma mark SDWebFileDownloaderDelegate

- (void)fileDownloader:(SDWebFileDownloader *)downloader didFinishWithFile:(NSData *)data 
{
    SDWIRetain(downloader);
    SDWebImageOptions options = [[downloader.userInfo objectForKey:@"options"] intValue];
    
    // Notify all the downloadDelegates with this downloader
    for (NSInteger idx = (NSInteger)[downloaders count] - 1; idx >= 0; idx--)
    {
        NSUInteger uidx = (NSUInteger)idx;
        SDWebFileDownloader *aDownloader = [downloaders objectAtIndex:uidx];
        if (aDownloader == downloader)
        {
            id<SDWebFileManagerDelegate> delegate = [downloadDelegates objectAtIndex:uidx];
            SDWIRetain(delegate);
            SDWIAutorelease(delegate);
            
            if (data)
            {
                if ([delegate respondsToSelector:@selector(webFileManager:didFinishWithFileData:)])
                {
                    [delegate performSelector:@selector(webFileManager:didFinishWithFileData:) withObject:self withObject:data];
                }
                if ([delegate respondsToSelector:@selector(webFileManager:didFinishWithFileData:forURL:)])
                {
                    objc_msgSend(delegate, @selector(webFileManager:didFinishWithFileData:forURL:), self, data, downloader.url);
                }
#if NS_BLOCKS_AVAILABLE
                if (self.successBlock)
                {
                    self.successBlock(data);
                }
#endif
            }
            else
            {
                if ([delegate respondsToSelector:@selector(webFileManager:didFailWithError:)])
                {
                    [delegate performSelector:@selector(webFileManager:didFailWithError:) withObject:self withObject:nil];
                }
                if ([delegate respondsToSelector:@selector(webFileManager:didFailWithError:forURL:)])
                {
                    objc_msgSend(delegate, @selector(webFileManager:didFailWithError:forURL:), self, nil, downloader.url);
                }
#if NS_BLOCKS_AVAILABLE
                if (self.failureBlock)
                {
                    self.failureBlock(nil);
                }
#endif
            }
            
            [downloaders removeObjectAtIndex:uidx];
            [downloadDelegates removeObjectAtIndex:uidx];
        }
    }
    
    if (data)
    {
        // Store the image in the cache
        [[SDFileCache sharedFileCache] storeFile:data 
                                          forKey:[downloader.url absoluteString] 
                                          toDisk:!(options & SDWebImageCacheMemoryOnly) 
                                    withFilePath:[downloader.userInfo objectForKey:@"filePath"]];
        
    }
    else if (!(options & SDWebImageRetryFailed))
    {
        // The image can't be downloaded from this URL, mark the URL as failed so we won't try and fail again and again
        // (do this only if SDWebImageRetryFailed isn't activated)
        [failedURLs addObject:downloader.url];
    }
    
    
    // Release the downloader
    [downloaderForURL removeObjectForKey:downloader.url];
    SDWIRelease(downloader);
}

- (void)fileDownloader:(SDWebFileDownloader *)downloader didFailWithError:(NSError *)error;
{
    SDWIRetain(downloader);
    
    // Notify all the downloadDelegates with this downloader
    for (NSInteger idx = (NSInteger)[downloaders count] - 1; idx >= 0; idx--)
    {
        NSUInteger uidx = (NSUInteger)idx;
        SDWebFileDownloader *aDownloader = [downloaders objectAtIndex:uidx];
        if (aDownloader == downloader)
        {
            id<SDWebFileManagerDelegate> delegate = [downloadDelegates objectAtIndex:uidx];
            SDWIRetain(delegate);
            SDWIAutorelease(delegate);
            
            if ([delegate respondsToSelector:@selector(webFileManager:didFailWithError:)])
            {
                [delegate performSelector:@selector(webFileManager:didFailWithError:) withObject:self withObject:error];
            }
            if ([delegate respondsToSelector:@selector(webFileManager:didFailWithError:forURL:)])
            {
                objc_msgSend(delegate, @selector(webFileManager:didFailWithError:forURL:), self, error, downloader.url);
            }
#if NS_BLOCKS_AVAILABLE
            if (self.failureBlock)
            {
                self.failureBlock(error);
            }
#endif
            
            [downloaders removeObjectAtIndex:uidx];
            [downloadDelegates removeObjectAtIndex:uidx];
        }
    }
    
    // Release the downloader
    [downloaderForURL removeObjectForKey:downloader.url];
    SDWIRelease(downloader);
}

@end
