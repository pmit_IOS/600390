//
//  AssetTablePicker.m
//
//  Created by Matt Tuzzolo on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import "ELCAssetTablePicker.h"
#import "ELCAssetCell.h"
#import "ELCAsset.h"
#import "ELCAlbumPickerController.h"
#import "UINavigationBar+Custom.h"
#import "TKUtil.h"


@implementation ELCAssetTablePicker

@synthesize parent;
@synthesize selectedAssetsLabel;
@synthesize assetGroup, elcAssets;

-(void)viewDidLoad {
        
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    [self.tableView setAllowsSelection:NO];

    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    self.elcAssets = tempArray;
    [tempArray release];
    
    UIButton *button = [self.navigationController.navigationBar buttonWithText:@"完成"
                                                                        target:self 
                                                                        action:@selector(doneAction:)];
    UIBarButtonItem *finishBar = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = finishBar;
    [finishBar release];
    [self.navigationItem setTitle:@"Loading..."];

    [self performSelectorInBackground:@selector(preparePhotos) withObject:nil];
    
    // Show partial while full list loads
    [self.tableView performSelector:@selector(reloadData) withObject:nil afterDelay:.5];
}

-(void)preparePhotos {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    
    NSLog(@"enumerating photos");
    [self.assetGroup enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) 
     {         
         if(result == nil) 
         {
             return;
         }
         
         ELCAsset *elcAsset = [[[ELCAsset alloc] initWithAsset:result] autorelease];
         [elcAsset setParent:self];
         [self.elcAssets addObject:elcAsset];
     }];    
    NSLog(@"done enumerating photos");
    
    [self.tableView reloadData];
    [self.navigationItem setTitle:@"选择图片"];
    
    [pool release];

}

- (void)selectedAssets:(id)activity {
    NSMutableArray *selectedAssetsImages = [[[NSMutableArray alloc] init] autorelease];
    for(ELCAsset *elcAsset in self.elcAssets) 
    {        
        if([elcAsset selected]) {
            
            [selectedAssetsImages addObject:[elcAsset asset]];
        }
    }
    
    [(ELCAlbumPickerController*)self.parent selectedAssets:selectedAssetsImages completion:^{
        [activity stopAnimating];
        [activity removeFromSuperview];
        self.view.alpha = 1.0;
        self.view.userInteractionEnabled = YES;
    }];
}

- (void) doneAction:(id)sender {
    UIActivityIndicatorView *activity = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
    activity.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    activity.center = self.view.center;
    [self.view addSubview:activity];
    self.view.alpha = 0.8;
    [activity startAnimating];
    UIImageView *kBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"refresh-background"]];
    kBackgroundView.frame = CGRectMake(0, 0, 320, 480);
    [[[UIApplication sharedApplication] keyWindow] addSubview:kBackgroundView];
    self.view.userInteractionEnabled = NO;
    [self performSelector:@selector(selectedAssets:) withObject:nil afterDelay:0.0];
}

#pragma mark UITableViewDataSource Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ceil([self.assetGroup numberOfAssets] / 4.0);
}

- (NSArray*)assetsForIndexPath:(NSIndexPath*)_indexPath {
    
    int index = (_indexPath.row*4);
    int maxIndex = (_indexPath.row*4+3);
    
    // NSLog(@"Getting assets for %d to %d with array count %d", index, maxIndex, [assets count]);
    
    if(maxIndex < [self.elcAssets count]) {
        
        return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                [self.elcAssets objectAtIndex:index+1],
                [self.elcAssets objectAtIndex:index+2],
                [self.elcAssets objectAtIndex:index+3],
                nil];
    }
    
    else if(maxIndex-1 < [self.elcAssets count]) {
        
        return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                [self.elcAssets objectAtIndex:index+1],
                [self.elcAssets objectAtIndex:index+2],
                nil];
    }
    
    else if(maxIndex-2 < [self.elcAssets count]) {
        
        return [NSArray arrayWithObjects:[self.elcAssets objectAtIndex:index],
                [self.elcAssets objectAtIndex:index+1],
                nil];
    }
    
    else if(maxIndex-3 < [self.elcAssets count]) {
        
        return [NSArray arrayWithObject:[self.elcAssets objectAtIndex:index]];
    }
    
    return nil;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
        
    ELCAssetCell *cell = (ELCAssetCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil) 
    {                
        cell = [[[ELCAssetCell alloc] initWithAssets:[self assetsForIndexPath:indexPath] reuseIdentifier:CellIdentifier] autorelease];
    }    
    else 
    {        
        [cell setAssets:[self assetsForIndexPath:indexPath]];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 79;
}

- (int)totalSelectedAssets {
    
    int count = 0;
    
    for(ELCAsset *asset in self.elcAssets) 
    {
        if([asset selected]) 
        {            
            count++;    
        }
    }
    
    return count;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)dealloc 
{
    [elcAssets release];
    [selectedAssetsLabel release];
    [super dealloc];    
}

@end
