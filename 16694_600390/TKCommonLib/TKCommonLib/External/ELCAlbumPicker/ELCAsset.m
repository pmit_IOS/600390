//
//  Asset.m
//
//  Created by Matt Tuzzolo on 2/15/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import "ELCAsset.h"
#import "ELCAssetTablePicker.h"

@implementation ELCAsset

@synthesize asset;
@synthesize parent;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

-(id)initWithAsset:(ALAsset*)_asset {
    self = [super initWithFrame:CGRectMake(0, 0, 0, 0)];
    if (self) {
        
        self.asset = _asset;
        
        CGRect viewFrames = CGRectMake(0, 0, 75, 75);
        
        UIImageView *assetImageView = [[UIImageView alloc] initWithFrame:viewFrames];
        [assetImageView setContentMode:UIViewContentModeScaleToFill];
        [assetImageView setImage:[UIImage imageWithCGImage:[self.asset thumbnail]]];
        [self addSubview:assetImageView];
        [assetImageView release];
        
        overlayView = [[UIImageView alloc] initWithFrame:viewFrames];
        [overlayView setImage:[UIImage imageNamed:@"Overlay.png"]];
        [overlayView setHidden:YES];
        [self addSubview:overlayView];
    }
    
    return self;    
}

-(void)toggleSelection {
    
    overlayView.hidden = !overlayView.hidden;
    
    if([(ELCAssetTablePicker*)self.parent totalSelectedAssets] > 9) {
        overlayView.hidden = !overlayView.hidden;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"照片太多了" message:@"请选择少于10张照片" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        [alert release];    
        
        //        [(ELCAssetTablePicker*)self.parent doneAction:nil];
    }
}

-(BOOL)selected {
    
    return !overlayView.hidden;
}

-(void)setSelected:(BOOL)_selected {
    
    [overlayView setHidden:!_selected];
}

- (void)dealloc 
{    
    self.asset = nil;
    [overlayView release];
    [super dealloc];
}

@end

