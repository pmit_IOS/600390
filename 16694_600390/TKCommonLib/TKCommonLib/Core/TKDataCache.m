
#import "TKDataCache.h"
#import "JSON.h"

@implementation TKDataCache

+ (id)getCacheDataByURLKey:(NSString *)key {
    id data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    return [data JSONValue];
}

+ (void)setCacheData:(id)data andURLKey:(NSString *)key {
    NSString *jsonValue = [data JSONRepresentation];
    [[NSUserDefaults standardUserDefaults] setObject:jsonValue forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)removeCacheDataByURLKey:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
}

@end
