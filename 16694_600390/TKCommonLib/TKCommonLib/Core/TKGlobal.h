
#import "TKDebug.h"

#define DEGREES_TO_RADIANS(__ANGLE) ((__ANGLE) / 180.0 * M_PI)

// ##################################  Release  ################################
#define TKRELEASE(property) { \
    [property release]; \
    property = nil; \
}

/**
 * from three20
 * Add this macro before each category implementation, so we don't have to use
 * -all_load or -force_load to load object files from static libraries that only contain
 * categories and no classes.
 * See http://developer.apple.com/library/mac/#qa/qa2006/qa1490.html for more info.
 */
#define TK_FIX_CATEGORY_BUG(name) @interface TK_FIX_CATEGORY_BUG_##name : NSObject @end \
@implementation TK_FIX_CATEGORY_BUG_##name @end

// ##############################  Notification  ###############################
// Notification name
#define kNotificationLoginSucsess @"kNotificationLoginSucsess"
#define kNotificationLogOutSuccess @"kNotificationLogOutSuccess"

#define kNotificationLoginCancel @"kNotificationLoginCancel"

#define kNotificationLoginDidCancel @"kNotificationLoginDidCancel"

// ################################  SINGLETON  ################################
// about the - (oneway void)release {}
//http://stackoverflow.com/questions/5494981/use-case-of-oneway-void-in-objective-c
#define SINGLETON_INTERFACE(CLASSNAME)  \
+ (CLASSNAME *)shared;\
- (void)forceRelease;

#define SINGLETON_IMPLEMENTATION(CLASSNAME)         \
\
static CLASSNAME* g_shared##CLASSNAME = nil;        \
\
+ (CLASSNAME*)shared                                \
{                                                   \
    if (g_shared##CLASSNAME != nil) {                   \
        return g_shared##CLASSNAME;                         \
    }                                                   \
\
    @synchronized(self) {                               \
        if (g_shared##CLASSNAME == nil) {                   \
            g_shared##CLASSNAME = [[self alloc] init];      \
        }                                                   \
    }                                                       \
\
    return g_shared##CLASSNAME;                             \
}                                                           \
\
+ (id)allocWithZone:(NSZone*)zone                           \
{                                                           \
    @synchronized(self) {                                   \
        if (g_shared##CLASSNAME == nil) {                   \
            g_shared##CLASSNAME = [super allocWithZone:zone];    \
            return g_shared##CLASSNAME;                         \
        }                                                   \
    }                                                   \
    NSAssert(NO, @ "[" #CLASSNAME                       \
        " alloc] explicitly called on singleton class.");   \
    return nil;                                         \
}                                                   \
\
- (id)copyWithZone:(NSZone*)zone                    \
{                                                   \
    return self;                                        \
}                                                   \
\
- (id)retain                                        \
{                                                   \
    return self;                                        \
}                                                   \
\
- (oneway void)release                                     \
{                                                   \
}                                                   \
\
- (void)forceRelease {                              \
    NSLog(@"Force release "#CLASSNAME"");               \
    @synchronized(self) {                               \
        if (g_shared##CLASSNAME != nil) {                   \
            g_shared##CLASSNAME = nil;                          \
        }                                                   \
    }                                                   \
    [super release];                                    \
}                                                   \
\
- (id)autorelease {                                                   \
    return self;                                        \
}

