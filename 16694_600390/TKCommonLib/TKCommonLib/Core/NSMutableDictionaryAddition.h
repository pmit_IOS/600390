//
//  NSMutableDictionaryAddition.h
//  TKCommonLib
//
//  Created by luobin on 4/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary(TKCategory)

/**
 *	@brief	安全的可变字典，当添加项为nil，自动转换为［NSNULL null］对象，防止程序崩溃
 *
 *	@param 	anObject 	The value for key. The object receives a retain message before being added to the dictionary. This value maybe be nil.
 *	@param 	aKey 	The key for value. The key is copied (using copyWithZone:; keys must conform to the NSCopying protocol). The key must not be nil.
 */
- (void)setSafeObject:(id)anObject forKey:(id)aKey;


@end

@interface NSDictionary(TKCategory)

/**
 *	@brief	安全的可变字典，当添加项为nil，自动转换为［NSNULL null］对象，防止程序崩溃
 *
 *	@param 	aKey 	The key for value. The key is copied (using copyWithZone:; keys must conform to the NSCopying protocol). The key must not be nil.
 */
- (id)safeObjectForKey:(id)aKey;


/**
 *	@brief	忽略大小写可变字典，防止获取失败
 *
 *	@param 	aKey 	The key for value. The key is copied (using copyWithZone:; keys must conform to the NSCopying protocol). The key must not be nil.
 */


- (id)insensitiveObjectForKey:(id)aKey;

@end
