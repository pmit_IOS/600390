/*
 Erica Sadun, http://ericasadun.com
 iPhone Developer's Cookbook, 3.0 Edition
 BSD License for anything not specifically marked as developed by a third party.
 Apple's code excluded.
 Use at your own risk
 */

#import <UIKit/UIKit.h>
@interface UIDevice (Reachability)
+ (BOOL) networkAvailable;
+ (BOOL) activeWLAN;
+ (BOOL) activeWWAN;
@end

@interface UIDevice (Platform)
+(NSString *) machine;
+(NSString *) platform;
+(NSString *)macaddress;
+(BOOL)isIPod;
+(BOOL)isIPhone;
+(BOOL)isIPad;
@end

@interface UIDevice (Identifier)
/*
 * @method uniqueDeviceIdentifier
 * @description use this method when you need a unique identifier in one app.
 * It generates a hash from the MAC-address in combination with the bundle identifier
 * of your app.
 */

+ (NSString *) uniqueDeviceIdentifier;
/*
 * @method uniqueGlobalDeviceIdentifier
 * @description use this method when you need a unique global identifier to track a device
 * with multiple apps. as example a advertising network will use this method to track the device
 * from different apps.
 * It generates a hash from the MAC-address only.
 */

+ (NSString *) uniqueGlobalDeviceIdentifier;

@end
