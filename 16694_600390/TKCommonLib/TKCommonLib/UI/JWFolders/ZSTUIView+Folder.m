//
//  UIView+Folder.m
//  F3
//
//  Created by luobin on 12-9-27.
//
//

#import <QuartzCore/QuartzCore.h>
#import "ZSTUIView+Folder.h"
#import "UIScreenAdditions.h"

@implementation UIView (Folder)

- (void)openFolderWithContentView:(UIView *)view
                         position:(CGPoint)position
                        openBlock:(JWFoldersOpenBlock)openBlock
                       closeBlock:(JWFoldersCloseBlock)closeBlock
                  completionBlock:(JWFoldersCompletionBlock)completionBlock
{
    ZSTJWFolders *folders = [[ZSTJWFolders alloc] init];
    [folders openFolderWithContentView:view
                              position:position
                         containerView:self
                             openBlock:openBlock
                            closeBlock:closeBlock
                       completionBlock:completionBlock
     ];
    [folders release];
}

@end

@implementation UITableView (Folder)

- (void)openFolderWithContentView:(UIView *)view
                        openBlock:(JWFoldersOpenBlock)openBlock
                       closeBlock:(JWFoldersCloseBlock)closeBlock
                  completionBlock:(JWFoldersCompletionBlock)completionBlock {
    CGRect rowRect = [self rectForRowAtIndexPath:[self indexPathForSelectedRow]];
    ZSTJWFolders *folders = [[ZSTJWFolders alloc] init];
    [folders openFolderWithContentView:view
                              position:CGPointMake(51, CGRectGetMaxY(rowRect) - self.contentOffset.y)
                         containerView:self
                             openBlock:^(UIView *contentView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction){
                                 self.scrollEnabled = NO;
                                 if (openBlock) {
                                     openBlock(contentView, duration, timingFunction);
                                 }
                             }
                            closeBlock:^(UIView *contentView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction){
                                self.scrollEnabled = YES;
                                if (closeBlock) {
                                    closeBlock(contentView, duration, timingFunction);
                                }
                            }
                       completionBlock:completionBlock
     ];
    [folders release];
}

- (void)openFollowWithContentView:(UIView *)view
                        openBlock:(JWFoldersOpenBlock)openBlock
                       closeBlock:(JWFoldersCloseBlock)closeBlock
                  completionBlock:(JWFoldersCompletionBlock)completionBlock {
    CGRect rowRect = [self rectForRowAtIndexPath:[self indexPathForSelectedRow]];
    ZSTJWFolders *folders = [[ZSTJWFolders alloc] init];
    [folders openFollowWithContentView:view
                              position:CGPointMake(51, CGRectGetMaxY(rowRect) - self.contentOffset.y)
                         containerView:self
                             openBlock:^(UIView *contentView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction){
                                 self.scrollEnabled = NO;
                                 if (openBlock) {
                                     openBlock(contentView, duration, timingFunction);
                                 }
                             }
                            closeBlock:^(UIView *contentView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction){
                                self.scrollEnabled = YES;
                                if (closeBlock) {
                                    closeBlock(contentView, duration, timingFunction);
                                }
                            }
                       completionBlock:completionBlock
     ];
    [folders release];
}


- (UIImage *)screenshot2 {
    CGFloat scale = [UIScreen screenScale];
	
    if(scale > 1.5) {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, scale);
    } else {
        UIGraphicsBeginImageContext(self.frame.size);
    }
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0, -self.contentOffset.y);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenshot;
}

- (UIImage *)screenshot {
    CGFloat scale = [UIScreen screenScale];
    
    CGRect rect = self.bounds;
    CGRect rowRect = [self rectForRowAtIndexPath:[self indexPathForSelectedRow]];
    UIImage *unvisibleScreenshot = nil;
    
    CGFloat diff = CGRectGetMaxY(rowRect) - CGRectGetMaxY(rect);
    if (diff > 0) {
        rect.size.height += diff;
        
        if(scale > 1.5) {
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(rect.size.width, diff), NO, scale);
        } else {
            UIGraphicsBeginImageContext(CGSizeMake(rect.size.width, diff));
        }
        CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0, diff - rowRect.size.height);
        [[self cellForRowAtIndexPath:self.indexPathForSelectedRow].layer renderInContext:UIGraphicsGetCurrentContext()];
        unvisibleScreenshot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    if(scale > 1.5) {
        UIGraphicsBeginImageContextWithOptions(rect.size, NO, scale);
    } else {
        UIGraphicsBeginImageContext(rect.size);
    }
    [[self screenshot2] drawInRect:CGRectMake(0, 0, rect.size.width, self.bounds.size.height)];
    if (diff > 0) {
        [unvisibleScreenshot drawInRect:CGRectMake(0, self.bounds.size.height, rect.size.width, diff)];
    }
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenshot;
}

@end