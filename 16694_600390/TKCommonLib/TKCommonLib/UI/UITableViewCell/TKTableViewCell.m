


#import "TKTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "TKDrawing.h"

#define shadow_margin           4
#define default_shadow_opacity  0.7

#define contentView_margin      2

#define default_radius          10

//#define default_border_color                    [UIColor colorWithHex:0xBCBCBC]
//#define default_separator_color                 [UIColor colorWithHex:0xCDCDCD]
#define default_border_color                    [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1]
#define default_separator_color                 [UIColor colorWithRed:225/255.0f green:225/255.0f blue:225/255.0f alpha:1]

#define default_selection_gradient_start_color  [UIColor colorWithHex:0x0089F9]
#define default_selection_gradient_end_color    [UIColor colorWithHex:0x0054EA]


typedef enum {
    CellBackgroundBehaviorNormal = 0,
    CellBackgroundBehaviorSelected,
} CellBackgroundBehavior;

typedef enum {
    CellBackgroundGradientNormal = 0,
    CellBackgroundGradientSelected,
} CellBackgroundGradient;



@interface TKTableViewCell (Private)

- (float) shadowMargin;
- (BOOL) tableViewIsGrouped;

@end

@implementation TKTableViewCell (Private)
- (BOOL) tableViewIsGrouped {
    return _tableViewStyle == UITableViewStyleGrouped;
}

- (float) shadowMargin {
    return [self tableViewIsGrouped] ? shadow_margin : 0;
}

@end



// http://www.raywenderlich.com/2033/core-graphics-101-lines-rectangles-and-gradients
// https://developer.apple.com/library/mac/documentation/graphicsimaging/reference/CGContext/Reference/reference.html

@interface TKTableViewCellBackground : UIView

@property (nonatomic, assign) TKTableViewCell *cell;
@property (nonatomic, assign) CellBackgroundBehavior behavior;

- (id) initWithFrame:(CGRect)frame behavior:(CellBackgroundBehavior)behavior;

@end

@implementation TKTableViewCellBackground
@synthesize cell;
@synthesize behavior;


- (CGPathRef) createRoundedPath:(CGRect)rect 
{
	if (!self.cell.cornerRadius) {
		return [UIBezierPath bezierPathWithRect:rect].CGPath;
	}
	
    UIRectCorner corners;

    switch (self.cell.position) {
        case TKTableViewCellPositionTop:
            corners = UIRectCornerTopLeft | UIRectCornerTopRight;
            break;
        case TKTableViewCellPositionMiddle:
            corners = 0;
            break;
        case TKTableViewCellPositionBottom:
            corners = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            break;
        default:
            corners = UIRectCornerAllCorners;
            break;
    }

    UIBezierPath *thePath = [UIBezierPath bezierPathWithRoundedRect:rect
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(self.cell.cornerRadius, self.cell.cornerRadius)];    
    return thePath.CGPath;
}


- (CGGradientRef) newGradientFromType:(CellBackgroundGradient)type
{
    switch (type) 
    {
        case CellBackgroundGradientSelected:
            return [self.cell newSelectionGradient];
        default:
            return [self.cell newNormalGradient];
    }
}

- (void) drawGradient:(CGRect)rect type:(CellBackgroundGradient)type 
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    CGPathRef path;
    path = [self createRoundedPath:rect];
    
    CGContextAddPath(ctx, path);
        
    CGGradientRef gradient = [self newGradientFromType:type];
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextClip(ctx);
    CGContextDrawLinearGradient(ctx, gradient, startPoint, endPoint, 0);
    CGGradientRelease(gradient);
    
    CGContextRestoreGState(ctx);
}

- (void) drawBackground:(CGRect)rect
{
    if (self.behavior == CellBackgroundBehaviorSelected 
        && self.cell.selectionStyle != UITableViewCellSelectionStyleNone)
    {
        [self drawGradient:rect type:CellBackgroundGradientSelected];
        return;
    }
    
    if (self.cell.gradientStartColor && self.cell.gradientEndColor)
    {
        [self drawGradient:rect type:CellBackgroundGradientNormal];
        return;
    }
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    
    // draws body
    CGPathRef path;
    path = [self createRoundedPath:rect];

    CGContextAddPath(ctx, path);

    CGContextSetFillColorWithColor(ctx, self.cell.backgroundColor.CGColor);
    CGContextFillPath(ctx);

    CGContextRestoreGState(ctx);
}

- (void) drawSingleLineSeparator:(CGRect)rect 
{
    switch (self.cell.position)
    {
        case TKTableViewCellPositionAlone:
            break;
        case TKTableViewCellPositionBottom:
            break;
        case TKTableViewCellPositionTop:
        case TKTableViewCellPositionMiddle:
            [TKDrawing drawLineAtHeight:CGRectGetMaxY(rect) - 0.5
                                   rect:rect
                                  color:self.cell.customSeparatorColor
                                  width:1];
            break;
    }
}

- (void) drawEtchedSeparator:(CGRect)rect
{
    switch (self.cell.position)
    {
        case TKTableViewCellPositionAlone:
            break;
        case TKTableViewCellPositionBottom:
            [TKDrawing drawLineAtHeight:0
                                   rect:rect
                                  color:[UIColor whiteColor]
                                  width:0.5];
            break;
        case TKTableViewCellPositionTop:
            [TKDrawing drawLineAtHeight:CGRectGetMaxY(rect)-0.5
                                   rect:rect
                                  color:self.cell.customSeparatorColor
                                  width:0.5];
            break;
        default:
            [TKDrawing drawLineAtHeight:0
                                   rect:rect
                                  color:[UIColor whiteColor]
                                  width:0.5];
            [TKDrawing drawLineAtHeight:CGRectGetMaxY(rect)-0.5
                                   rect:rect
                                  color:self.cell.customSeparatorColor
                                  width:0.5];
            break;
    }
}

- (void) drawLineSeparator:(CGRect)rect
{
    switch (self.cell.customSeparatorStyle) 
    {
        case UITableViewCellSeparatorStyleSingleLine:
            [self drawSingleLineSeparator:rect];
            break;
        case UITableViewCellSeparatorStyleSingleLineEtched:
            [self drawEtchedSeparator:rect];
        default:
            break;
    }
}

- (void) fixShadow:(CGContextRef)ctx rect:(CGRect)rect
{
    if (self.cell.position == TKTableViewCellPositionTop || self.cell.position == TKTableViewCellPositionAlone)
    {
        return;
    }
    
    CGContextSaveGState(ctx);
    CGContextMoveToPoint(ctx, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextAddLineToPoint(ctx, CGRectGetMaxX(rect), CGRectGetMinY(rect));
    CGContextSetStrokeColorWithColor(ctx, self.cell.borderColor.CGColor);
    CGContextSetLineWidth(ctx, 5);

    UIColor *shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:self.cell.shadowOpacity];
    CGContextSetShadowWithColor(ctx, CGSizeMake(0, -1), 3, shadowColor.CGColor);

    CGContextStrokePath(ctx);
    
    CGContextRestoreGState(ctx);

}

- (void) drawBorder:(CGRect)rect shadow:(BOOL)shadow 
{
    float shadowShift = 0.5 * self.cell.dropsShadow;
    
    CGRect innerRect = CGRectMake(rect.origin.x+shadowShift, rect.origin.y+shadowShift,
                                  rect.size.width-shadowShift*2, rect.size.height-shadowShift*2);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    if (shadow) 
    {
        [self fixShadow:ctx rect:innerRect];
    }

    
    CGContextSaveGState(ctx);

    // draws body
    
    CGPathRef path = [self createRoundedPath:innerRect];
    CGContextAddPath(ctx, path);

    if (shadow) {
        UIColor *shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:self.cell.shadowOpacity];
        CGContextSetShadowWithColor(ctx, CGSizeMake(0, 1), 3, shadowColor.CGColor);
    }   
    CGContextSetStrokeColorWithColor(ctx, self.cell.borderColor.CGColor);
    CGContextSetLineWidth(ctx, 2 - shadowShift);
    CGContextStrokePath(ctx);
    
    CGContextRestoreGState(ctx);
}

- (CGRect) innerFrame:(CGRect)frame 
{
    float topMargin = 0;
    float bottomMargin = 0;
    float shadowMargin = [self.cell shadowMargin];
    
    switch (self.cell.position) {
        case TKTableViewCellPositionTop:
            topMargin = shadowMargin;
        case TKTableViewCellPositionMiddle:
            // let the separator to be painted, but separator is only painted
            // in grouped table views
            //            bottomMargin = [self tableViewIsGrouped] ? 1 : 0;
            break;
        case TKTableViewCellPositionAlone:
            topMargin = shadowMargin;
            bottomMargin = shadowMargin;
            break;
        case TKTableViewCellPositionBottom:
            bottomMargin = shadowMargin;
            break;
        default:
            break;
    }
    
    return CGRectMake(frame.origin.x + shadowMargin,
                              frame.origin.y + topMargin,
                              frame.size.width - shadowMargin*2,
                              frame.size.height - topMargin - bottomMargin);
}

- (void) drawRect:(CGRect)initialRect 
{
    CGRect rect = [self innerFrame:self.bounds];
    
    [self drawBorder:rect shadow:self.cell.dropsShadow];
    
    [self drawBackground:rect];
    
    [self drawLineSeparator:rect];
}

- (void) dealloc 
{
    self.cell = nil;
    
    [super dealloc];
}

- (id) initWithFrame:(CGRect)frame behavior:(CellBackgroundBehavior)bbehavior 
{
    if (self = [super initWithFrame:frame]) 
    {
        self.contentMode = UIViewContentModeRedraw;
        self.behavior = bbehavior;
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

@end


@implementation TKTableViewCell
@synthesize position, dropsShadow, borderColor, tableViewBackgroundColor;
@synthesize customSeparatorColor, selectionGradientStartColor, selectionGradientEndColor;
@synthesize cornerRadius;
@synthesize customBackgroundColor, gradientStartColor, gradientEndColor;
@synthesize shadowOpacity, customSeparatorStyle;


- (void) dealloc
{
    [self.contentView removeObserver:self forKeyPath:@"frame"];
    self.borderColor = nil;
    self.tableViewBackgroundColor = nil;
    self.customSeparatorColor = nil;
    self.selectionGradientStartColor = nil;
    self.selectionGradientEndColor = nil;
    self.customBackgroundColor = nil;
    self.gradientStartColor = nil;
    self.gradientEndColor = nil;
    
    [super dealloc];
}

- (void)initializeVars
{
    // default values
    self.position = TKTableViewCellPositionMiddle;
    self.dropsShadow = NO;
    self.borderColor = default_border_color;
    self.tableViewBackgroundColor = [UIColor clearColor];
    self.customSeparatorColor = default_separator_color;
    self.selectionGradientStartColor = default_selection_gradient_start_color;
    self.selectionGradientEndColor = default_selection_gradient_end_color;
    self.cornerRadius = default_radius;
    self.shadowOpacity = default_shadow_opacity;
    self.customSeparatorStyle = UITableViewCellSeparatorStyleSingleLine;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionOld context:nil];

        
        TKTableViewCellBackground *bg = [[TKTableViewCellBackground alloc] initWithFrame:CGRectInset(self.frame, -4, -4)
                                                                                  behavior:CellBackgroundBehaviorNormal];
        bg.cell = self;
        self.backgroundView = bg;
        [bg release];
        
        bg = [[TKTableViewCellBackground alloc] initWithFrame:self.frame
                                                      behavior:CellBackgroundBehaviorSelected];
        bg.cell = self;
        self.selectedBackgroundView = bg;
        [bg release];
        
        [self initializeVars];
    }
    return self;
}


+ (TKTableViewCellPosition) positionForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView.dataSource tableView:tableView numberOfRowsInSection:indexPath.section] == 1) {
        return TKTableViewCellPositionAlone;
    }
    if (indexPath.row == 0) {
        return TKTableViewCellPositionTop;
    }
    if (indexPath.row+1 == [tableView.dataSource tableView:tableView numberOfRowsInSection:indexPath.section])
    {
        return TKTableViewCellPositionBottom;
    }

    return TKTableViewCellPositionMiddle;
}

+ (CGFloat) neededHeightForPosition:(TKTableViewCellPosition)position tableStyle:(UITableViewStyle)style 
{
    if (style == UITableViewStylePlain)
    {
        return 0;
    }
    
    switch (position) 
    {
        case TKTableViewCellPositionBottom:
        case TKTableViewCellPositionTop:
            return shadow_margin;
        case TKTableViewCellPositionAlone:
            return shadow_margin*2;
        default:
            return 0;
    }    
}

+ (CGFloat) tableView:(UITableView *)tableView neededHeightForIndexPath:(NSIndexPath *)indexPath
{
    TKTableViewCellPosition position = [TKTableViewCell positionForTableView:tableView indexPath:indexPath];
    return [TKTableViewCell neededHeightForPosition:position tableStyle:tableView.style];
}

- (void) prepareForTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath 
{
    _tableViewStyle = tableView.style;
    self.position = [TKTableViewCell positionForTableView:tableView indexPath:indexPath];
}

- (CGRect) backgroundFrame
{
    float topMargin = 0;
    float bottomMargin = 0;
    float shadowMargin = [self shadowMargin];
    
//    switch (self.position) {
//        case TKTableViewCellPositionTop:
//            topMargin = shadowMargin;
//        case TKTableViewCellPositionMiddle:
//            // let the separator to be painted, but separator is only painted
//            // in grouped table views
////            bottomMargin = [self tableViewIsGrouped] ? 1 : 0;
//            break;
//        case TKTableViewCellPositionAlone:
////            topMargin = shadowMargin;
////            bottomMargin = shadowMargin;
//            break;
//        case TKTableViewCellPositionBottom:
//            bottomMargin = shadowMargin;
//            break;
//        default:
//            break;
//    }
    
    CGRect frame = CGRectMake(9 - shadowMargin,
                              -topMargin,
                              302+shadowMargin*2,
                              self.frame.size.height+topMargin+bottomMargin);
    
    return frame;
}

// Avoids contentView's frame auto-updating. It calculates the best size, taking
// into account the cell's margin and so.
- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context 
{
    if ([keyPath isEqualToString:@"frame"]) 
    {
        self.backgroundView.frame = [self backgroundFrame];
        self.selectedBackgroundView.frame = self.backgroundView.frame;
    }
}

- (void) prepareForReuse 
{
    [super prepareForReuse];
    [self.backgroundView setNeedsDisplay];
    [self.selectedBackgroundView setNeedsDisplay];
}

- (void) setTableViewBackgroundColor:(UIColor *)aBackgroundColor 
{
    [aBackgroundColor retain];
    if (tableViewBackgroundColor != nil) {
        [tableViewBackgroundColor release];
    }
    tableViewBackgroundColor = aBackgroundColor;
    
    self.backgroundView.backgroundColor = aBackgroundColor;
    self.selectedBackgroundView.backgroundColor = aBackgroundColor;
}

- (CAShapeLayer *) mask 
{
    UIRectCorner corners = 0;
    
    switch (self.position)
    {
        case TKTableViewCellPositionTop:
            corners = UIRectCornerTopLeft | UIRectCornerTopRight;
            break;
        case TKTableViewCellPositionAlone:
            corners = UIRectCornerAllCorners;
            break;
        case TKTableViewCellPositionBottom:
            corners = UIRectCornerBottomLeft | UIRectCornerBottomRight;
            break;
        default:
            break;
    }
    
//    CGRect maskRect = CGRectMake(0, 0, 
//                                 self.innerFrame.size.width, 
//                                 self.innerFrame.size.height);
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                   byRoundingCorners:corners
                                                         cornerRadii:CGSizeMake(self.cornerRadius, self.cornerRadius)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = maskPath.CGPath;

    return [maskLayer autorelease];
}

- (BOOL) dropsShadow 
{
    return dropsShadow && [self tableViewIsGrouped];
}

- (float) cornerRadius
{
    return [self tableViewIsGrouped] ? cornerRadius : 0;
}

- (UIColor *) backgroundColor 
{
    return customBackgroundColor ? customBackgroundColor : [super backgroundColor];
}

- (CGGradientRef) newSelectionGradient
{
    CGFloat locations[] = { 0, 1 };    
    
    NSArray *colors = [NSArray arrayWithObjects:(id)self.selectionGradientStartColor.CGColor, (id)self.selectionGradientEndColor.CGColor, nil];
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, 
                                                        (CFArrayRef) colors, locations);
    CGColorSpaceRelease(colorSpace);
    
    return gradient;
}

- (CGGradientRef) newNormalGradient
{
    CGFloat locations[] = { 0, 1 };    
    
    NSArray *colors = [NSArray arrayWithObjects:(id)self.gradientStartColor.CGColor, (id)self.gradientEndColor.CGColor, nil];
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, 
                                                        (CFArrayRef) colors, locations);
    CGColorSpaceRelease(colorSpace);
    
    return gradient;
}

@end
