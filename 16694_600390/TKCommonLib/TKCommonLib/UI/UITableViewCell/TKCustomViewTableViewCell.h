


#import <UIKit/UIKit.h>
#import "TKTableViewCell.h"

/** `TKCustomViewTableViewCell` is a subclass of `TKTableViewCell`.
 
 `TKCustomViewTableViewCell` adds a custom view inside the cell (*not 
 inside the contentView!*), and masks it to the cell's shape. So you can add
 an image, for example, and it will get masked to the cell's shape, with the
 rounded corners.
*/

@interface TKCustomViewTableViewCell : TKTableViewCell

/** Holds a reference to the custom view. */
@property (nonatomic, retain) UIView *customView;

@end
