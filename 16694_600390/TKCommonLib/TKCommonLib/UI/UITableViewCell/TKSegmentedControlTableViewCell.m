


#import "TKSegmentedControlTableViewCell.h"


@implementation TKSegmentedControlTableViewCell
@synthesize selectedIndex;

- (void) updateButtonActions 
{
    /* a bit of hacking. In this case, we don't care if the user drags the finger
     or not, we still want te segment to be selected. */
    [(UIButton *)self.customView removeTarget:self.customView
                                       action:@selector(hardDeselect) 
                             forControlEvents:UIControlEventTouchDragInside | UIControlEventTouchDragOutside | UIControlEventTouchUpOutside];
    [(UIButton *)self.customView addTarget:self.customView action:@selector(fireButtonAction:event:) forControlEvents:UIControlEventTouchUpOutside];           
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        selectedIndex = -1;
        
        UIColor *tmp = [self.selectionGradientStartColor retain];
        self.selectionGradientStartColor = self.selectionGradientEndColor;
        self.selectionGradientEndColor = tmp;
        [tmp release];
        
        self.textLabel.shadowColor = [UIColor blackColor];
        self.textLabel.shadowOffset = CGSizeMake(0, 1);

        self.shadowOnlyOnSelected = YES;
        
        [self updateButtonActions];
        [self setActionBlock:nil]; // forces tracking the selected index        
    }
    return self;
}

- (void) _setSelectedIndex:(NSInteger)sselectedIndex notifySuper:(BOOL)notify
{
    selectedIndex = sselectedIndex;
    
    if (notify)
    {
        [super selectIndex:selectedIndex];
    }
}

- (void) restartSelectedIndex {
    [self _setSelectedIndex:0 notifySuper:YES];
}


- (void) setSelectedIndex:(NSInteger)sselectedIndex {
    [self _setSelectedIndex:sselectedIndex notifySuper:YES];
}


- (NSArray *) titles {
    return self.texts;
}

- (void) setTitles:(NSArray *)titles {
    self.numberOfElements = [titles count];
    for (int i = 0; i < self.numberOfElements; i++) {
        [self setText:[titles objectAtIndex:i] atIndex:i];
    }
    [self restartSelectedIndex];
}


- (void) setActionBlock:(void (^)(NSIndexPath *indexPath, NSInteger sselectedIndex))actionBlock {
    [super setActionBlock:^(NSIndexPath *indexPath, NSInteger sselectedIndex) {
        [self _setSelectedIndex:sselectedIndex notifySuper:NO];
        
        if (actionBlock)
        {
            actionBlock(indexPath, sselectedIndex);
        }
    }];
}


@end
