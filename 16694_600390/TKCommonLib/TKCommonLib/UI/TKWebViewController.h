
//
//  TKWebViewController.h
//  
//
//  Created by luo bin on 12-3-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

@protocol TKWebControllerDelegate;

@interface TKWebViewController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate>
{
@protected
    UIWebView*        _webView;
    
    UIToolbar*        _toolbar;
    
    UIView*           _headerView;
    
    UIBarButtonItem*  _backButton;
    UIBarButtonItem*  _forwardButton;
    UIBarButtonItem*  _refreshButton;
    UIBarButtonItem*  _stopButton;
    UIBarButtonItem*  _actionButton;
    
    NSURL*            _loadingURL;
    
    UIActionSheet*    _actionSheet;
    
    UIActivityIndicatorView* _spinner;
    
    id<TKWebControllerDelegate> _delegate;
}

/**
 * The current web view URL. If the web view is currently loading a URL, then the loading URL is
 * returned instead.
 */
@property (nonatomic, readonly) NSURL*  URL;

/**
 * A view that is inserted at the top of the web view, within the scroller.
 */
@property (nonatomic, retain)   UIView* headerView;

/**
 * The web controller delegate
 */
@property (nonatomic, assign)   id<TKWebControllerDelegate> delegate;

/**
 * Navigate to the given URL.
 */
- (void)openURL:(NSURL*)URL;

/**
 * Load the given request using UIWebView's loadRequest:.
 *
 * @param request  A URL request identifying the location of the content to load.
 */
- (void)openRequest:(NSURLRequest*)request;

@end

/**
 * The web controller delegate, similar to UIWebViewDelegate, but prefixed with controller
 */
@protocol TKWebControllerDelegate <NSObject>

@optional
- (BOOL)webController:(TKWebViewController *)controller webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
       navigationType:(UIWebViewNavigationType)navigationType;
- (void)webController:(TKWebViewController *)controller webViewDidStartLoad:(UIWebView *)webView;
- (void)webController:(TKWebViewController *)controller webViewDidFinishLoad:(UIWebView *)webView;
- (void)webController:(TKWebViewController *)controller webView:(UIWebView *)webView
 didFailLoadWithError:(NSError *)error;

@end



