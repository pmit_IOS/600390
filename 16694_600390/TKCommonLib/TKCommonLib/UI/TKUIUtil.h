//
//  TKUIUtil.h
//  
//
//  Created by luo bin on 12-3-4.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKUIUtil : NSObject

+ (void)promptInView:(UIView *)view withTitle:(NSString *)title withCenter:(CGPoint)center;

+ (void)promptInView:(UIView *)view withTitle:(NSString *)title withCenter:(CGPoint)center delegate:(id)delegate;

+ (void)clearPromptInView:(UIView *)view;

+ (BOOL)isActivity:(UIView *)view;

//////////////////////////////////////////////////////////////////////////////////////////////////////

+ (void)alertInView:(UIView *)view withTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles,...NS_REQUIRES_NIL_TERMINATION;

+ (void)alertInView:(UIView *)view withTitle:(NSString *)title withImage:(UIImage *)image;

+ (void)alertInView:(UIView *)view withTitle:(NSString *)title withImage:(UIImage *)image withCenter:(CGPoint)center;

+ (void)alertInWindow:(NSString *)title withImage:(UIImage *)image;

+ (void)alertInWindow:(NSString *)title withImage:(UIImage *)image withCenter:(CGPoint)center;

+ (void)showHUD:(UIView *)view;

+ (void)showHUDInView:(UIView *)view withCenter:(CGPoint)center;

+ (void)showHUD:(UIView *)view withText:(NSString*)text;

+ (void)showHUDInView:(UIView *)view withText:(NSString*)text withImage:(UIImage *)image;

+ (void)showHUDInView:(UIView *)view withText:(NSString*)text withCenter:(CGPoint)center;

+ (void)showHUDInView:(UIView *)view withText:(NSString*)text withImage:(UIImage *)image delegate:(id)delegate;

+ (void)showHUDInView:(UIView *)view withText:(NSString*)text withImage:(UIImage *)image withCenter:(CGPoint)center;

+ (void)showHUDInView:(UIView *)view withText:(NSString*)text withImage:(UIImage *)image withCenter:(CGPoint)center delegate:(id)delegate;

+ (void)hiddenHUD;

+ (void)hiddenHUDAfterDelay:(NSTimeInterval)delay;

+ (UIBarButtonItem *)itemForNavigationWithTitle:(NSString *)title withImage:(NSString *)imageURL target:(id)target selector:(SEL)selector;

+ (UIBarButtonItem *)itemForNavigationWithTitle:(NSString *)title target:(id)target selector:(SEL)selector;

+ (UIBarButtonItem *)backItemForNavigationWithTitle:(NSString *)title target:(id)target selector:(SEL)selector;

+ (UIView *)titleViewForNavigationWithTitle:(NSString *)title;

+ (UIView *)titleViewForNavigationWithTitle:(NSString *)title withSize:(CGSize)size;

+ (UIView *)titleViewForNavigationWithTitle:(NSString *)title 
                                   withSize:(CGSize)size 
                                  withLines:(NSUInteger)numbers
                                   withFont:(UIFont *)font;
@end
