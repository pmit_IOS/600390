//
//  TKActionSheet.m
//  TKCommonLib
//
//  Created by luobin on 5/2/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TKActionSheet2.h"

@implementation TKActionSheet2

@synthesize view;
@synthesize toolBar;

-(id)initWithHeight:(float)height WithSheetTitle:(NSString*)title{
	self = [super init];
    if (self) {
		int theight = height - 40;
		int btnnum = theight/50;
		for(int i=0; i<btnnum; i++){
			[self addButtonWithTitle:@" "];
		}
		toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
		toolBar.barStyle = UIBarStyleBlackOpaque;
		UIBarButtonItem *titleButton = [[UIBarButtonItem alloc] initWithTitle:title 
                                                                        style:UIBarButtonItemStylePlain 
                                                                       target:nil 
                                                                       action:nil];
		
		UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" 
                                                                        style:UIBarButtonItemStyleDone 
                                                                       target:self
                                                                       action:@selector(done)];
		
		UIBarButtonItem *leftButton  = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" 
                                                                        style:UIBarButtonItemStyleBordered 
                                                                       target:self 
                                                                       action:@selector(docancel)];
		
		UIBarButtonItem *fixedButton  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace 
                                                                                      target:nil 
                                                                                      action:nil];
		
		NSArray *array = [[NSArray alloc] initWithObjects:leftButton,fixedButton,titleButton,fixedButton,rightButton,nil];
		[toolBar setItems: array];
		[titleButton release];
		[leftButton  release];
		[rightButton release];
		[fixedButton release];
		[array       release];
		[self addSubview:toolBar];
		view = [[UIView alloc] initWithFrame:CGRectMake(0, 44, 320, height-44)];
		view.backgroundColor = [UIColor groupTableViewBackgroundColor];
		[self addSubview:view];
    }
    return self;
}

-(void)done{
	[self dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)docancel{
	[self dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)dealloc{
    self.toolBar = nil;
    self.view = nil;
	[super dealloc];
}

@end
