//
//  ZSTPageListController.h
//  YouYun
//
//  Created by luobin on 6/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKLoadMoreView.h"
#import "EGORefreshTableHeaderView.h"

@interface TKPageTableViewCell : UITableViewCell {
@private
    
}

@property (nonatomic, assign) NSUInteger row;
@property (nonatomic, retain) id object;

+ (CGFloat)tableView:(UITableView*)tableView rowHeightForObject:(id)object;

- (void)objectUpdate;


@end


/**
 *	@brief	抽象类，不能直接使用
 */

@interface TKPageTableViewController : UITableViewController<EGORefreshTableHeaderDelegate, TKLoadMoreViewDelegate>
{
    EGORefreshTableHeaderView *_refreshHeaderView;
    TKLoadMoreView *_loadMoreView;
    
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    
    NSMutableArray *_datas;
}

@property (nonatomic, retain) NSMutableArray *data;

- (void)fetchDataSuccess:(NSArray *)data hasMore:(BOOL)hasMore;

- (void)fetchDataFailed;

@end

//子类应该复写这些方法，默认实现为空
@interface TKPageTableViewController()

- (void)refreshNewestData;

- (void)loadMoreData;

- (Class)tableView:(UITableView*)tableView cellClassForObject:(id)object;
    
@end
