//
// Copyright 2009-2011 Facebook
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    hTKp://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 * TRUE if the screen is retina.
 */
BOOL TKIsRetina();

/**
 * @return the current runtime version of the iPhone OS.
 */
NSString *TKOSVersion();

/**
 * Checks if the run-time version of the OS is at least a certain version.
 */
BOOL TKRuntimeOSVersionIsAtLeast(NSString *version);

/**
 * Checks if the link-time version of the OS is at least a certain version.
 */
BOOL TKOSVersionIsAtLeast(float version);

/**
 * @return TRUE if the keyboard is visible.
 */
BOOL TKIsKeyboardVisible();

/**
 * @return TRUE if the device has phone capabilities.
 */
BOOL TKIsPhoneSupported();

/**
 * @return TRUE if the device supports backgrounding
 */
BOOL TKIsMultiTaskingSupported();

/**
 * @return TRUE if the device is iPad.
 */
BOOL TKIsPad();

/**
 * @return the current device orientation.
 */
UIDeviceOrientation TKDeviceOrientation();

/**
 * @return TRUE if the current device orientation is portrait or portrait upside down.
 */
BOOL TKDeviceOrientationIsPortrait();

/**
 * @return TRUE if the current device orientation is landscape left, or landscape right.
 */
BOOL TKDeviceOrientationIsLandscape();

/**
 * @return device full model name in human readable strings
 */
NSString* TKDeviceModelName();

/**
 * On iPhone/iPod touch
 * Checks if the orientation is portrait, landscape left, or landscape right.
 * This helps to ignore upside down and flat orientations.
 *
 * On iPad:
 * Always returns Yes.
 */
BOOL TKIsSupportedOrientation(UIInterfaceOrientation orientation);

/**
 * @return the rotation transform for a given orientation.
 */
CGAffineTransform TKRotateTransformForOrientation(UIInterfaceOrientation orientation);

/**
 * @return the application frame with no offset.
 *
 * From the Apple docs:
 * Frame of application screen area in points (i.e. entire screen minus status bar if visible)
 */
CGRect TKApplicationFrame();

/**
 * @return the space between the edge of the screen and a grouped table cell. Larger on iPad.
 */
CGFloat TKGroupedTableCellInset();

/**
 * A convenient way to show a UIAlertView with a message.
 */
void TKAlert(NSString* message);

/**
 * Same as TKAlert but the alert view has no title.
 */
void TKAlertNoTitle(NSString* message);

/**
 * Same as TKAlert but the alert view has app name title.
 */
void TKAlertAppNameTitle(NSString* message);

/**
 *	return	the current interface orientation.
 */
UIInterfaceOrientation TKInterfaceOrientation();

/**
 *	return	the bound of the current screen 
 */
CGRect TKScreenBounds();

/**
 *	return	the height of the status bar 
 */
CGFloat TKStatusHeight();

/**
 * @return the height of the keyboard for a given orientation.
 */
CGFloat TKKeyboardHeightForOrientation(UIInterfaceOrientation orientation);

/**
 *	return the height for the keyboard
 */
CGFloat TKKeyboardHeight();

/**
 * @return the toolbar height for a given orientation.
 *
 * The toolbar is slightly shorter in landscape.
 */
CGFloat TKToolbarHeightForOrientation(UIInterfaceOrientation orientation);


/**
 * @return the height of the area containing the status bar and navigation bar.
 */
CGFloat TKBarsHeight();


///////////////////////////////////////////////////////////////////////////////////////////////////
// Debug logging helpers

#define TKLOGRECT(rect) \
  TKDINFO(@"%s x=%f, y=%f, w=%f, h=%f", #rect, rect.origin.x, rect.origin.y, \
  rect.size.width, rect.size.height)

#define TKLOGPOINT(pt) \
  TKDINFO(@"%s x=%f, y=%f", #pt, pt.x, pt.y)

#define TKLOGSIZE(size) \
  TKDINFO(@"%s w=%f, h=%f", #size, size.width, size.height)

#define TKLOGEDGES(edges) \
  TKDINFO(@"%s left=%f, right=%f, top=%f, boTKom=%f", #edges, edges.left, edges.right, \
  edges.top, edges.boTKom)

#define TKLOGHSV(_COLOR) \
  TKDINFO(@"%s h=%f, s=%f, v=%f", #_COLOR, _COLOR.hue, _COLOR.saturation, _COLOR.value)

#define TKLOGVIEWS(_VIEW) \
  { for (UIView* view = _VIEW; view; view = view.superview) { TKDINFO(@"%@", view); } }


///////////////////////////////////////////////////////////////////////////////////////////////////
// Dimensions of common iPhone OS Views

/**
 * The standard height of a row in a table view controller.
 * @const 44 pixels
 */
extern const CGFloat TKkDefaultRowHeight;

/**
 * The standard height of a toolbar in portrait orientation.
 * @const 44 pixels
 */
extern const CGFloat TKkDefaultPortraiTKoolbarHeight;

/**
 * The standard height of a toolbar in landscape orientation.
 * @const 33 pixels
 */
extern const CGFloat TKkDefaultLandscapeToolbarHeight;

/**
 * The standard height of the keyboard in portrait orientation.
 * @const 216 pixels
 */
extern const CGFloat TKkDefaultPortraitKeyboardHeight;

/**
 * The standard height of the keyboard in landscape orientation.
 * @const 160 pixels
 */
extern const CGFloat TKkDefaultLandscapeKeyboardHeight;

/**
 * The space between the edge of the screen and the cell edge in grouped table views.
 * @const 10 pixels
 */
extern const CGFloat TKkGroupedTableCellInset;

/**
 * Deprecated macros for common constants.
 */
#define TK_ROW_HEIGHT                 TKkDefaultRowHeight
#define TK_TOOLBAR_HEIGHT             TKkDefaultPortraiTKoolbarHeight
#define TK_LANDSCAPE_TOOLBAR_HEIGHT   TKkDefaultLandscapeToolbarHeight

#define TK_KEYBOARD_HEIGHT                 TKkDefaultPortraitKeyboardHeight
#define TK_LANDSCAPE_KEYBOARD_HEIGHT       TKkDefaultLandscapeKeyboardHeight
#define TK_IPAD_KEYBOARD_HEIGHT            TKkDefaultPadPortraitKeyboardHeight
#define TK_IPAD_LANDSCAPE_KEYBOARD_HEIGHT  TKkDefaultPadLandscapeKeyboardHeight


///////////////////////////////////////////////////////////////////////////////////////////////////
// Animation

/**
 * The standard duration length for a transition.
 * @const 0.3 seconds
 */
extern const CGFloat TKkDefaulTKransitionDuration;

/**
 * The standard duration length for a fast transition.
 * @const 0.2 seconds
 */
extern const CGFloat TKkDefaultFasTKransitionDuration;

/**
 * The standard duration length for a flip transition.
 * @const 0.7 seconds
 */
extern const CGFloat TKkDefaultFlipTransitionDuration;

/**
 * Deprecated macros for common constants.
 */
#define TK_TRANSITION_DURATION      TKkDefaulTKransitionDuration
#define TK_FAST_TRANSITION_DURATION TKkDefaultFasTKransitionDuration
#define TK_FLIP_TRANSITION_DURATION TKkDefaultFlipTransitionDuration
