//
//  YIFullScreenScroll.m
//  YIFullScreenScroll
//
//  Created by Yasuhiro Inami on 12/06/03.
//  Copyright (c) 2012 Yasuhiro Inami. All rights reserved.
//

#import "YIFullScreenScroll.h"
#import "UIViewController+HeaderView.h"

#define IS_PORTRAIT         UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)
//#define STATUS_BAR_HEIGHT   (IS_PORTRAIT ? [UIApplication sharedApplication].statusBarFrame.size.height : [UIApplication sharedApplication].statusBarFrame.size.width)
#define STATUS_BAR_HEIGHT   0
#define MIN_SCROLL_DISTANCE_FOR_FULLSCREEN  44

@implementation YIFullScreenScroll

@synthesize viewController = _viewController;
@synthesize enabled = _enabled;
@synthesize shouldShowUIBarsOnScrollUp = _shouldShowUIBarsOnScrollUp;

- (id)initWithViewController:(UIViewController*)viewController
{
    self = [super init];
    if (self) {
        self.enabled = YES;
        self.shouldShowUIBarsOnScrollUp = YES;
        
//        viewController.navigationController.navigationBar.translucent = YES;
//        viewController.navigationController.toolbar.translucent = YES;
        _viewController = viewController;
    }
    return self;
}
 
- (void)_layoutWithScrollView:(UIScrollView*)scrollView deltaY:(CGFloat)deltaY
{
    if (!self.enabled) return;
    
    // headerView
    UIView *headerView =  _viewController.headerView;
    BOOL isNavBarExisting = headerView && headerView.superview && !headerView.hidden;
    if (isNavBarExisting) {
        headerView.top = MIN(MAX(headerView.top-deltaY, STATUS_BAR_HEIGHT-headerView.height), STATUS_BAR_HEIGHT);
    }

    // scrollIndicatorInsets
    UIEdgeInsets insets = scrollView.scrollIndicatorInsets;
    if (isNavBarExisting) {
        insets.top = headerView.bottom - STATUS_BAR_HEIGHT;
    }
    insets.bottom = 0;
    scrollView.scrollIndicatorInsets = insets;
}

#pragma mark -

- (void)layoutTabBarController
{
    if (_viewController.tabBarController) {
        UIView* tabBarTransitionView = [_viewController.tabBarController.view.subviews objectAtIndex:0];
        tabBarTransitionView.frame = _viewController.tabBarController.view.bounds;
    }
}

- (void)showUIBarsWithScrollView:(UIScrollView*)scrollView animated:(BOOL)animated
{
    [UIView animateWithDuration:(animated ? 0.1 : 0) animations:^{
        [self _layoutWithScrollView:scrollView deltaY:-50];
    }];
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    _prevContentOffsetY = scrollView.contentOffset.y;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ((scrollView.dragging || _isScrollingTop) && scrollView.contentOffset.y + scrollView.height < scrollView.contentSize.height) {
        CGFloat deltaY = scrollView.contentOffset.y-_prevContentOffsetY;
        _prevContentOffsetY = MAX(scrollView.contentOffset.y, -scrollView.contentInset.top);
        
        if (!self.shouldShowUIBarsOnScrollUp && deltaY < 0 && scrollView.contentOffset.y > 0 && !_isScrollingTop) {
            deltaY = fabs(deltaY);
        }
//        NSLog(@"_prevContentOffsetY == %f, deltaY == %f, scrollView.contentSize.height== %f", _prevContentOffsetY, deltaY, scrollView.contentSize.height);
        [self _layoutWithScrollView:scrollView deltaY:deltaY];
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    _prevContentOffsetY = scrollView.contentOffset.y;
    _isScrollingTop = YES;
    return YES;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView
{
    _isScrollingTop = NO;
}

@end
