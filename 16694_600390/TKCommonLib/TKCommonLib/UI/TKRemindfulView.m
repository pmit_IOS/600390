//
//  TKRemindfulView.m
//  UsingDemo
//
//  Created by Jey on 11-9-18.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "TKRemindfulView.h"


@implementation TKRemindfulView
@synthesize text = _text;
@synthesize textLabel = _textLabel;
@synthesize indicatorView = _indicatorView;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.windowLevel = UIWindowLevelStatusBar + 1.0f;
        self.backgroundColor = [UIColor blackColor];
        
        float width = MIN(frame.size.width, 200.0);
        _textLabel = [[UILabel alloc] initWithFrame:(CGRect){(frame.size.width-width)/2, 0.0f, width, frame.size.height}];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.textColor = RGBACOLOR(230.0, 230.0, 230.0, 1.0);
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.font = [UIFont boldSystemFontOfSize:12.0f];
        [self addSubview:_textLabel];
        
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _indicatorView.frame = CGRectMake(frame.size.width - 40.0,
                                          3.0f,
                                          frame.size.height-8,
                                          frame.size.height-8);
        _indicatorView.hidesWhenStopped = YES;
        [self addSubview:_indicatorView];
    }
    return self;
}

- (void)setText:(NSString *)text {
    if (text == _text) {
        return;
    }
    [_text release];
    _text = [text retain];
    _textLabel.text = _text;
    CGSize size = [_text sizeWithFont:_textLabel.font];
    _indicatorView.center = CGPointMake(_textLabel.center.x-size.width/2-_indicatorView.frame.size.height,
                                        _indicatorView.center.y);
    
    self.hidden = NO;
}

- (BOOL)isShow {
    return !self.hidden;
}

- (void)dealloc {
    [_textLabel release], _textLabel = nil;
    [_indicatorView release], _indicatorView = nil;
    [_text release], _text = nil;
    [self hide:NO];
    [super dealloc];
}

#pragma mark - Public
- (void)show:(BOOL)animated {
    [_indicatorView startAnimating];
    self.hidden = NO;
}

- (void)hide:(BOOL)animated {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [_indicatorView stopAnimating];
    self.hidden = YES;
}
@end
