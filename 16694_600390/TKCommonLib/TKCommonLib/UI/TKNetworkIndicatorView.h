//
//  TKNetworkIndicatorView.h
//  ECA
//
//  Created by luobin on 12-10-10.
//
//

#import <UIKit/UIKit.h>

@class TKNetworkIndicatorView;
typedef void (^TKRefreshLoadBlock)(TKNetworkIndicatorView *networkIndicatorView);

@interface TKNetworkIndicatorView : UIControl

@property (nonatomic, retain) UIActivityIndicatorView *indicatorView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UILabel *loadingLabel;
@property (nonatomic, copy) TKRefreshLoadBlock refreshLoadBlock;

+ (TKNetworkIndicatorView *)newNetworkIndicatorViewForView:(UIView *)addToView refreshLoadBlock:(TKRefreshLoadBlock)refreshLoadBlock;
- (void)refreshFailed;
- (void)removeView;

@end

@interface UIView (ZSTNetworkIndicatorView)

- (TKNetworkIndicatorView *)newNetworkIndicatorViewWithRefreshLoadBlock:(TKRefreshLoadBlock)refreshLoadBlock;
- (TKNetworkIndicatorView *)currentNetworkIndicatorView;
- (void)refreshFailed;
- (void)removeNetworkIndicatorView;

@end
