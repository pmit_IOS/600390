



#import "TKTabBarItem.h"
#import "ZSTUtils.h"

@interface TKTabBarItem()

@property (nonatomic, retain) UIImage *customSelectedImage;
@property (nonatomic, retain) UIImage *customUnselectedImage;

@end

@implementation TKTabBarItem

@synthesize customSelectedImage;
@synthesize customUnselectedImage;


#pragma mark -
#pragma mark Initialization and destruction

// Designated Initializer

- (id)initWithTitle:(NSString *)title unselectedImage:(UIImage *)anUnselectedImage selectedImage:(UIImage *)aSelectedImage textColor:(NSString *)hexColor tag:(NSInteger)tag
{
    [self setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[ZSTUtils colorFromHexColor:hexColor], UITextAttributeTextColor,nil] forState:UIControlStateNormal];
    
    return [self initWithTitle:title unselectedImage:anUnselectedImage selectedImage:aSelectedImage tag:tag];
}

- (id)initWithTitle:(NSString *)title unselectedImage:(UIImage *)anUnselectedImage selectedImage:(UIImage *)aSelectedImage tag:(NSInteger)tag {
	
	self.customSelectedImage = aSelectedImage;

	return [self initWithTitle:title image:anUnselectedImage tag:tag];
}

- (id)initWithTitle:(NSString *)title image:(UIImage *)image tag:(NSInteger)tag {

	if (self = [super initWithTitle:title image:image tag:tag]) {
		self.customUnselectedImage = image;
        
    }
	return self;
}

- (void)dealloc {
    [customSelectedImage release];
    [customUnselectedImage release];
    [super dealloc];
}

#pragma mark -
#pragma mark Overwritten methods

-(UIImage *)selectedImage {
    return self.customSelectedImage;
}

-(UIImage *)unselectedImage {
    return self.customUnselectedImage;
}


@end
