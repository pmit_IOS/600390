//
//  TKMoviePlayerViewController.h
//  VideoTest
//
//  Created by luobin on 12-9-20.
//  Copyright (c) 2012年 luobin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "TKPlaybackTimeSlider.h"

@class TKTransportControls;
@interface TKMoviePlayerViewController : UIViewController {
    
}

-(id)initWithContentURL:(NSURL *)url;

@property(nonatomic, retain ,readonly) NSURL *url;
@property(nonatomic, retain ,readonly) UIImageView *imgPreviewImage;
@property(nonatomic, retain ,readonly) TKTransportControls *transportControls;
@property(retain ,readonly) MPMoviePlayerController *moviePlayer;

- (void)resetTimer;
- (void)stop;

@end