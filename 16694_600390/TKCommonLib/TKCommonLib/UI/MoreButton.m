//
//  MoreButton.m
//  QingTing2
//
//  Created by Eric on 11-12-9.
//  Copyright (c) 2011年 TOTEM.ME All rights reserved.
//

#import "MoreButton.h"

#define kLoadMoreText NSLocalizedString(@"加载更多" , nil)
#define kLoadingText  NSLocalizedString(@"正在加载..." , nil)
//#define kLoadMoreText nil

@implementation MoreButton

- (void)displayIndicator
{
    if (_indicator == nil)
    {
        _indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((self.frame.size.width- self.frame.size.height) / 2 - 60, 0.0f,
                                                                               self.frame.size.height, self.frame.size.height)];
        _indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        _indicator.transform = CGAffineTransformScale(_indicator.transform, 0.75, 0.75);
        [self addSubview:_indicator];
    }
    self.enabled = NO;
    _indicator.hidden = NO;
    [self setTitle:kLoadingText forState:UIControlStateNormal];
    [_indicator startAnimating];
}


- (void)hideIndicator
{
    self.enabled = YES;
    [_indicator stopAnimating];
    _indicator.hidden = YES;
    [self setTitle:kLoadMoreText forState:UIControlStateNormal];
}

+ (id)button
{
    MoreButton *button = [MoreButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 320, 40);
    button.backgroundColor = [UIColor clearColor];
    [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [button setTitle:kLoadMoreText forState:UIControlStateNormal];
    
    return button;
}

@end
