//
//  UIImage+Compress.h
//  F3_UI
//
//  Created by 9588 9588 on 8/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImage_Compress)

- (UIImage *)compressedImageScaleFactor:(CGFloat)imagePix;

- (UIImage *)compressedImage;

- (CGFloat)compressionQuality;

- (NSData *)compressedData;

- (NSData *)compressedData:(CGFloat)compressionQuality;

@end
