//
//  UIViewController+HeaderView.h
//  TKCommonLib
//
//  Created by admin on 12-9-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (HeaderView)

@property (nonatomic, retain) UIView *headerView;

@end
