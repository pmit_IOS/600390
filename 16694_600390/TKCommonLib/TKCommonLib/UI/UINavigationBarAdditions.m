//
//  UINavigationBar+Custom.m
//  
//
//  Created by luobin on 5/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "UINavigationBarAdditions.h"
#import <objc/runtime.h>

TK_FIX_CATEGORY_BUG(UINavigationBarTKCategory)

static char backgroundImageKey;

static UIImage *backgroundImage;

@implementation UINavigationBar (TKCategory)

+ (void)initialize
{
    TKReplaceMethods([UINavigationBar class], @selector(tempDrawRect:), @selector(drawRect:));
    TKReplaceMethods([UINavigationBar class], @selector(drawRect:), @selector(doDrawRect:));
}

+ (void)setBackgroundImage:(UIImage *)theBackgroundImage
{
    if (backgroundImage != theBackgroundImage) {
        [backgroundImage release];
        backgroundImage = [theBackgroundImage retain];
        //set navigationbar background image compatible with ios5
        if ([UINavigationBar instancesRespondToSelector:@selector(setBackgroundImage:forBarMetrics:)]
            && [UINavigationBar respondsToSelector:@selector(appearance)]) {
            [[UINavigationBar appearance] setBackgroundImage:theBackgroundImage 
                                               forBarMetrics:UIBarMetricsDefault];
        }
    }
}

- (void)setBackgroundImage:(UIImage *)backgroundImage
{
    objc_setAssociatedObject(self, &backgroundImageKey, backgroundImage, OBJC_ASSOCIATION_RETAIN);
    
    //set navigationbar background image compatible with ios5
    if ([UINavigationBar instancesRespondToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
        [self setBackgroundImage:backgroundImage
                   forBarMetrics:UIBarMetricsDefault];
    }
}

- (void)applyDefaultStyle {
    // add the drop shadow
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(0.0, 2);
    self.layer.shadowOpacity = 0.2;
    self.layer.masksToBounds = NO;
    self.layer.shouldRasterize = YES;
}

-(void)willMoveToWindow:(UIWindow *)newWindow{
    [super willMoveToWindow:newWindow];
    [self applyDefaultStyle];
}



- (UIImage *)backgroundImage
{
    return objc_getAssociatedObject(self, &backgroundImageKey);
}

- (void)tempDrawRect:(CGRect)rect 
{
    
}

- (void)doDrawRect:(CGRect)rect 
{
    //解决ios4.3.3 使用MPMoviePlayerViewController 程序奔溃的情况
    if ([[[self class] description] caseInsensitiveCompare:@"MPCenteringNavigationBar"] == NSOrderedSame) {
        return;
    }
    UIImage *image = self.backgroundImage;
    if (image == nil) {
        image = backgroundImage;
    }
    if (image) {
        [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    } else {
        [self tempDrawRect:rect];
    }
}

- (UIButton *)buttonWithText:(NSString *)buttonText
                      target:(id)target
                      action:(SEL)action {
    UIImage* buttonImage = nil;
    UIImage* buttonPressedImage = nil;
    NSUInteger buttonWidth = 54.0;
    NSUInteger capWidth = 5.0;
    buttonImage = [[UIImage imageNamed:@"seg-button"] stretchableImageWithLeftCapWidth:capWidth topCapHeight:0.0];
    buttonPressedImage = [[UIImage imageNamed:@"seg-button-press"] stretchableImageWithLeftCapWidth:capWidth topCapHeight:0.0];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0.0, 0.0, buttonWidth, buttonImage.size.height);
    button.titleLabel.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
    button.titleLabel.textColor = [UIColor whiteColor];
    button.titleLabel.shadowOffset = CGSizeMake(0,-1);
    button.titleLabel.shadowColor = [UIColor darkGrayColor];
    
    [button setTitle:buttonText forState:UIControlStateNormal];
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:buttonPressedImage forState:UIControlStateHighlighted];
    [button setBackgroundImage:buttonPressedImage forState:UIControlStateSelected];
    button.adjustsImageWhenHighlighted = NO;
    
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (UIButton *)buttonWithImage:(UIImage *)image
             highlightedImage:(UIImage *)highImage
                       target:(id)target
                       action:(SEL)action {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *refreshImage = image;
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0.0, 0.0, refreshImage.size.width, refreshImage.size.height);
    [button setImage:refreshImage forState:UIControlStateNormal];
    if (highImage) {
        [button setImage:highImage forState:UIControlStateHighlighted];
    }
    return button;
}

- (UIButton *)buttonWithTitle:(NSString *)text
                    backImage:(UIImage *)image
         highlightedBackImage:(UIImage *)highImage
                       target:(id)target
                       action:(SEL)action {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *refreshImage = image;
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0.0, 0.0, refreshImage.size.width, refreshImage.size.height);
    [button setBackgroundImage:refreshImage forState:UIControlStateNormal];
    [button setTitle:text forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    button.titleLabel.textColor = [UIColor whiteColor];
    button.titleLabel.shadowOffset = CGSizeMake(0,-1);
    button.titleLabel.shadowColor = [UIColor darkGrayColor];
    if (highImage) {
        [button setBackgroundImage:highImage forState:UIControlStateHighlighted];
    }
    return button;
}
@end

