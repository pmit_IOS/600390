//
//  TKImageCacheManager.m
//  TKCommonLib
//
//  Created by luobin on 4/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TKImageCacheManager.h"
#import "HJObjManager.h"
#import "HJMOBigFileCache.h"

@implementation TKImageCacheManager

SINGLETON_IMPLEMENTATION(TKImageCacheManager)

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
        
        if (_objMan == nil) {
            // Create the object manager
            _objMan = [[HJObjManager alloc] initWithLoadingBufferSize:100 memCacheSize:80];
            _objMan.maxMemSingleFileSize = 30 * 1024;       //30k
            
            //if you are using for full screen images, you'll need a smaller memory cache than the defaults,
            //otherwise the cached images will get you out of memory quickly
            //objMan = [[HJObjManager alloc] initWithLoadingBufferSize:6 memCacheSize:1];
            
            // Create a file cache for the object manager to use
            // A real app might do this durring startup, allowing the object manager and cache to be shared by several screens
            NSString* cacheDirectory = [NSHomeDirectory() stringByAppendingString:@"/Library/Caches/imgcache/"] ;
            HJMOFileCache* fileCache = [[[HJMOFileCache alloc] initWithRootPath:cacheDirectory] autorelease];
            _objMan.fileCache = fileCache;
            
            // Have the file cache trim itself down to a size & age limit, so it doesn't grow forever
            fileCache.fileCountLimit = 5000;
            fileCache.fileAgeLimit = 60*60*24*7; //1 week
            [fileCache trimCacheUsingBackgroundThread];
        }
    }
    return self;
}

- (void)clearMemCache
{
    [_objMan clear];
}

-(BOOL) manage:(id<HJMOUser>)user
{
    return [_objMan manage:user];
}

- (void)dealloc
{
    TKRELEASE(_objMan);
    [super dealloc];
}

@end
