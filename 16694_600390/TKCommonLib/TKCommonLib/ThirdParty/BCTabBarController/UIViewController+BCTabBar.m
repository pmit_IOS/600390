//
//  UIViewController+BCTabBar.m
//  VoiceChina
//
//  Created by huijun xu on 13-2-18.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "UIViewController+BCTabBar.h"
#import <objc/runtime.h>

@implementation UIViewController (BCTabBar)


- (BCTabBarController *)bcTabbarController
{
    return (BCTabBarController *)objc_getAssociatedObject(self, kBCTabBarControllerKey);
}

@end
