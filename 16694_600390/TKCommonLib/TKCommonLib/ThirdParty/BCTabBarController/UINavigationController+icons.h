#import <Foundation/Foundation.h>


@interface UINavigationController (BCTabBarController)

- (NSString *)iconImageName;
- (NSString *)tabTitle ;
- (NSString *)titleColor;

@end
