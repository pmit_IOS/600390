//
//  ELBudgetImageView.m
//  Test
//
//  Created by  on 11-7-1.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "ZSTSignaView.h"

#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@implementation ZSTSignaView  

-(id) initWithImage:(UIImage *)image
{
//    UIImage *image1 = [image stretchableImageWithLeftCapWidth: 12 topCapHeight:12];
    self = [super initWithImage: image];
    if (self) 
    {
        _badgeLabel = [[UILabel alloc] initWithFrame: CGRectZero];
        _badgeLabel.text = @"0";
        _badgeLabel.textAlignment = NSTextAlignmentCenter;
        _badgeLabel.textColor = [UIColor whiteColor];
        _badgeLabel.font = [UIFont boldSystemFontOfSize:8];
        _badgeLabel.backgroundColor = [UIColor clearColor];
        [self addSubview: _badgeLabel];
        [_badgeLabel sizeToFit];
        _badgeLabel.center = CGPointMake(self.frame.size.width / 2-4, self.frame.size.height / 2-2);
        
    }
    return self;
}

-(NSString *) text
{
    return _badgeLabel.text;
}

-(void) setText: (NSString *) text
{
    _badgeLabel.text = text;
    [_badgeLabel sizeToFit];

    
    if ([_badgeLabel.text length] >= 3)
    {
        _badgeLabel.text = @"•••";
    }
    
    _badgeLabel.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2-2);
 }

- (void)dealloc
{
    [_badgeLabel release];
    [super dealloc];
}

@end
